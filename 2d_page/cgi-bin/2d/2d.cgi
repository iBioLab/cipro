#!/usr/bin/perl5.8.8
#@(#)2d.cgi
# Make2D-DB II tool
# This script is the main script of the WEB server scripts for the relational 2-DE database
#==========================================================================================

# /*  This script is part of the Make-2D DB II Tool                               */
# /*  Copyright to the SWISS INSTITUTE OF BIOINFORMATICS                          */
# /*  You can freely use this script without moving those copyright lines         */
# /*  Modifications should comply to the instructions stated in the licence terms */


$VERSION = 2.50;

$sub_VERSION = "1";

$SERVER::interface = 1;

#==========================================================================================

use lib "./inc";
# use lib qw{./inc};
# or:
# use FindBin;
# use lib $FindBin::Bin;
# unshift @INC, qw(./inc):

do "basic_include.pl" or die;
do "2d_util.pl" or die;

if ($SERVER::ExPASy) {
  require "foot.pl";
}

my $coreInterfacePrefix = $main::core_interface_prefix;

$SERVER::core_selector = ($coreInterfacePrefix and $0 =~ /\.$coreInterfacePrefix\.\w+$/)? 1 : 0;
if ($SERVER::core_selector) {
  undef($SERVER::apacheModeRewriteOn);
  undef($SERVER::remoteInterface);
  do "2d_util.core.pl" or die;
}


use strict;
use vars qw($VERSION $sub_VERSION);

use CGI qw (-no_xhtml :standard :html);
if ($_DEBUG_::ON) {
  use CGI::Carp q(fatalsToBrowser)
}
$CGI::POST_MAX = 1024 * 256;

my $co = new CGI;

my $link_style='text-decoration: none; font-family: sans-serif; font-size: 10pt;';
my $font_style='font-family: sans-serif; font-size: 10pt;';

#====================================#
#====================================#

# VIEW or UPLOAD A FILE, Display File and Exit:

if ($co->param('View File')) {

  my $fileField = $co->param('View File');

  my %fileFieldCorrespondance = (
    'display file (gels)' => 'hidden gels file',
    'display file (entries)' => 'hidden entries file',
    'display file (spots)' => 'hidden spots file',
  );
  $fileField = $fileFieldCorrespondance{$fileField} if $fileField and $fileFieldCorrespondance{$fileField};

  &VIEW_FILE($fileField);
}

if ($co->param('Upload File') and $SERVER::core_selector) {

  my $fileField = $co->param('Upload File');
  my $toFile;

  my %fileFieldCorrespondance = (
    'upload file (gels)' => 'uploaded hidden gels file',
    'upload file (entries)' => 'uploaded hidden entries file',
    'upload file (spots)' => 'uploaded hidden spots file',
    'upload file (subtitle)' => 'uploaded subtitle file',
  );
  my %toFileCorrespondance = (
    'upload file (subtitle)' => "$main::tmp_path/subtitle.html",
  );
  $toFileCorrespondance{'upload file (gels)'} = $co->param('hidden gels file');
  $toFileCorrespondance{'upload file (entries)'} = $co->param('hidden entries file');
  $toFileCorrespondance{'upload file (spots)'} = $co->param('hidden spots file');

  $toFile = $toFileCorrespondance{$fileField} if $toFileCorrespondance{$fileField};
  $fileField = $fileFieldCorrespondance{$fileField} if $fileFieldCorrespondance{$fileField};

  &UPLOAD_FILE($fileField, $toFile);
}

#====================================#
#====================================#

# MAIN DEFINITIONS

my $server_path          =  $main::server_path;
my $html_path            =  $main::html_path;
my $web_server_ref_name  =  $main::web_server_ref_name;
my $url_www2d            =  $main::url_www2d;
my $icons                =  $main::icons;
my $db_server_logo       = ($main::db_server_logo and ($main::db_server_logo =~ /^\// or -e "$server_path$main::db_server_logo"))?
                            $main::db_server_logo : undef;
my $db_server_logo_width = ($db_server_logo and $main::db_server_logo_width =~ /^\d+$/)? $main::db_server_logo_width : undef;
   $db_server_logo_width =  640 if $db_server_logo_width > 640;
my $back_logo            = ($db_server_logo and !$SERVER::core_selector and !$SERVER::ExPASy)? $db_server_logo : "$icons/back.gif";
my $tmp_path             =  $main::tmp_path;
my $name2d               =  $main::name2d;
my $email                =  $main::email;
my $bkgrd                =  $main::bkgrd;
my $basic_font_color     = ($main::basic_font_color)? $main::basic_font_color : 'black';
my $title_comment        =  $main::title_comment;
my $menu_graphic_mode    =  int($main::menu_graphic_mode);
   $menu_graphic_mode    =  1 if $menu_graphic_mode > 3;
my $sample_origin_URL; # defined later once $DB::CurrentDatabase is known
my $image_type;        # defined later once $DB::CurrentDatabase is known

   $SERVER::interfaceWebAddress
                         = $co->url(-base=>1).$co->url(-absolute=>1);
my $script_name          = $main::script_name = $co->url(-absolute=>1);
my $script_name_qmark    = $main::script_name_qmark = '?';
                        my $script_name_rel   = $co->url(-relative=>1);
                       (my $script_dir = $script_name) =~ s/$script_name_rel//;
   $main::map_viewer_cgi = $main::map_viewer;
   $main::map_viewer     =~ s/(\w+)$/$coreInterfacePrefix.$1/ if $SERVER::core_selector;
my $map_viewer           = "$script_dir$main::map_viewer";
my $map_viewer_qmark     = $main::map_viewer_qmark = '?';
if ($SERVER::apacheModeRewriteOn) {
   $script_name          = $main::script_name = "/".$main::web_server_ref_name."/";
   $script_name_qmark    = $main::script_name_qmark = undef;
   $map_viewer           = $main::map_viewer = "/".$main::web_server_ref_name."/viewer";
   $map_viewer_qmark     = $main::map_viewer_qmark = '&';
}
   $main::tissue_list_URL = (($main::tissue_list_URL =~ /^\//)? $main::expasy : '').$main::tissue_list_URL;

my $subTitleFile = undef;

(my $name2dAlphaNumeric = $name2d) =~ s/\W/_/g;

my $full_script_name     = $co->url(-full=>1);

my $show_hidden_entries  = $main::show_hidden_entries;
my $private_data_password = $main::private_data_password;
my $userPrivatePassword = undef;
if ($private_data_password) {
  $userPrivatePassword = _crypting_string_($co->cookie("2d_".$name2dAlphaNumeric."_private_password"), 1, 0);
  if ($userPrivatePassword eq $private_data_password) {
    $show_hidden_entries = 1;
  }
  else {
    undef($userPrivatePassword) 
  }
}

my $allInterfacesText = '[All Interfaces]';
   $SERVER::extractAllRemoteDatabaseNames = 1;


$SERVER::coreSchema   = 'core';
$SERVER::publicSchema = 'public';
$SERVER::commonSchema = 'common';
$SERVER::logSchema    = 'log';

my $unidentifiedProteinNickname = quotemeta($main::unidentifiedProteinNickname);

if ($SERVER::core_selector or $show_hidden_entries) {
  $SERVER::search_path = $SERVER::coreSchema.','.$SERVER::commonSchema.','.$SERVER::logSchema;
  if ($unidentifiedProteinNickname) {
    $SERVER::showFlagSQLEntry = "Entry.AC <> '$unidentifiedProteinNickname'";
    $SERVER::showFlagSQLViewMap = "ViewMapEntryList.AC <> '$unidentifiedProteinNickname'";  
  }
  else {
    $SERVER::showFlagSQLEntry = '1 = 1';
    $SERVER::showFlagSQLViewMap = '1 = 1';
  }
}
else {
  $SERVER::search_path = $SERVER::publicSchema.','.$SERVER::commonSchema;
  $SERVER::hidePrivate = 1;
  $SERVER::showFlagSQLEntry = 'Entry.showFlag IS TRUE';
  $SERVER::showFlagSQLViewMap = 'ViewMapEntryList.showFlag IS TRUE';
}

$SERVER::showFlagSQLEntry = '1 = 1'
  if (($SERVER::core_selector or $userPrivatePassword) and $main::privateSearchUnidentifiedProtein);


# DEFAULTS: define values or leave to  'default'
eval ("use Chart::ErrorBars");
$SERVER::Chart = 1 unless $@;
$main::table_border = 0;
my $dynamicExamples = ($main::swiss_2d_page)? 0 : 1;


 my ($command, $hidden_reference, $home_page, $form_on, $command_external, $string);

 my ($resultStart, $resultEnd) = ("\n\n<!-- main result start -->\n\n", "\n\n<!-- main result end -->\n\n\n");
 my ($resultTextStart, $resultTextEnd) = ("<PRE>\n<!-- text result start -->\n", "\n<!-- text result end -->\n</PRE>");
(my $resultStartqm, my $resultEndqm) = (quotemeta($resultStart), quotemeta($resultEnd));
(my $resultTextStartqm, my $resultTextEndqm) = (quotemeta($resultTextStart), quotemeta($resultTextEnd));


#if ($co->param('save_file' or 0)) { # add all save file parameters
#        $STDOUT_to_file = 1;
#        my $co = new CGI;
#        print "Content-type: application/octet-stream\r\n";
#        goto EXECUTE_COMMAND; # Jump over any html STDOUT to the SQL command bloc then terminates after saving file
#}

#====================================#

# DEFINE SEARCH KEYWORDS, ATTRIBUTE VALUES To '$Search_Argument' And '$DB_Argument', Either from SUBMISSION PARAMS(priority) or @ARGV
# (Use those 'Keys' in the Main Code - check also the sub-routines (FORMATTED_COMMAND))

my $Search_Argument;
my $DB_Argument;

my %Search_PARAM_keys = (
"ac"           => "AC",
"de"           => "DE",
"author"       => "Author",
"spot"         => "Spot",
"ident"        => "Ident",
"pi_mw"        => "pI",
"combined"     => "Combined",
"map"          => "Map",
"mapexpinfo"   => "MapExpInfo",
"index"        => "Index",
"maplist"      => "MapList",
"databaselist" => "DatabaseList",
"make2d"       => "Make2D",
);
#$Search_PARAM_keys{au} = $Search_PARAM_keys{author};
#$Search_PARAM_keys{sn} = $Search_PARAM_keys{spot}
my %Search_PARAM_values = reverse %Search_PARAM_keys;
foreach my $key (keys %Search_PARAM_keys) {
  $Search_PARAM_keys{$key."s"} = $Search_PARAM_keys{$key};
}

my %Search_Menu = (
"AC"         => "Search by ACCESSION NUMBER",
"DE"         => "Search by DESCRIPTION, ID or GENE NAME",
"Author"     => "Search by AUTHOR",
"Spot"       => "Search by SPOT ID / SERIAL NUMBER",
"Ident"      => "Search Spots by IDENTIFICATION Techniques",
"pI"         => "Search SPOTS in a pI/Mw Range",
"Combined"   => "Combining Queries on Differnet Fields",
"Map"        => "List Identified Proteins for a given Map",
"MapExpInfo" => "List Experimental Information for a given Map",
"MainUpdate" => "Manage Database and Update Operations",
);
my %Search_Menu_TextStyle = ();
if ($menu_graphic_mode == 0) {
  foreach my $Search_Menu (%Search_Menu) {
    $Search_Menu_TextStyle{$Search_Menu} = 'MenuLink';
  }
}

my %Search_Menu_icons;
%Search_Menu_icons = (
"AC"         => "$icons/2d_menu_AC.gif",
"DE"         => "$icons/2d_menu_DE.gif",
"Author"     => "$icons/2d_menu_author.gif",
"Spot"       => "$icons/2d_menu_spot.gif",
"Ident"      => "$icons/2d_menu_ident.jpg", 
"pI"         => "$icons/2d_menu_pi.gif",
"Combined"   => "$icons/2d_menu_combined.gif",
"MapExpInfo" => "$icons/2d_menu_map_exp_info.gif",
"Map"        => "$icons/2d_menu_protein_list.gif",
"MainUpdate" => "$icons/2d_menu_mainupdates.jpg",
) if $menu_graphic_mode > 0;
%Search_Menu_icons = (
"AC"         => "$icons/2d_menu15.gif",
"DE"         => "$icons/2d_menu20.gif",
"Author"     => "$icons/2d_menu25.gif",
"Spot"       => "$icons/2d_menu30.gif",
"Ident"      => "$icons/2d_menu30.gif",
"pI"         => "$icons/2d_menu35.gif",
"Combined"   => "$icons/2d_menu50.gif",
"Map"        => "$icons/2d_menu70.gif",
"MainUpdate" => "$icons/2d_menu_mainupdates.jpg",
) if $menu_graphic_mode == 3;

# Read 'GET' arguments:
my (%args, $key, $value);
if ($ENV{REQUEST_METHOD} eq 'GET') {
  foreach my $input (split("&",$ENV{QUERY_STRING})) {
    $input =~ s/\&//g;
    if ($input =~ /([^=]+)=?(.*)/) {
      my ($key,$value) = (lc($1), lc($2));
      $key = 'ac' if $key eq 'id';
      $key = 'de' if $key eq 'ge';
      $key = 'database' if $key eq 'db';
     ($key,$value) = ('view','image') if $key eq 'viewer';
      # replace "+" with " "
      $value =~ s/\+/ /g;
      # convert hex characters
      $value =~ s/%(..)/pack('c',hex($1))/eg;
      # add keyword/value pair to a list
      $args{$key} = $value if $key;
      undef($key); undef($value);
    }
  }
}


# Go directly to one of the various search pages:
my $Search_Argument_key = undef;
my @Several_GET_Databases = ();
my @Several_Search_Databases = ();

if (%args) {

  $args{author} = $args{au} if exists($args{au});
  $args{spot} = $args{sn} if exists($args{sn});
  $args{combined} = $args{ful} if exists($args{ful});   $args{combined} = $args{full} if exists($args{full});

  foreach my $arg (keys %args) {

    # Internally we use ',' as separator, but we do accept '+' (which is a space when interpreted from the URL)
    $args{$arg} =~ s/(\s|\+)+/,/mg;

    next if !$Search_PARAM_keys{$arg};

    # we only want 'map=all' to act as the '?map' with no argument value (to only display thumbs)
    if ($args{map} =~ /^(all|\*)$/i) {      
      $args{map} = '';
      $Search_Argument_key = $Search_PARAM_keys{map};
      $Search_Argument = $Search_Argument_key;
      $Search::displayAllMaps = 1;
      next; 
    }
    # if (defined($args{$arg})) {$Search_Argument_key = defined; next;}
    $Search_Argument_key = $Search_PARAM_keys{$arg};
    $Search_Argument = $Search_Argument_key;

  }

  if ($args{database} and ($args{database} =~ /.+,.+/ or $args{database} eq 'all')) {
   $args{database} = join ',', @main::DATABASES_Included if $args{database} =~ /(^|,)all(,|$)/;
   my $separator = ',';
   @Several_GET_Databases = split "$separator", $args{database};
  }
  if ($args{database} and !@Several_GET_Databases) {
    $DB_Argument = $args{database};
    $DB_Argument = -1 if lc($DB_Argument) eq 'all';
    if  ($DB_Argument =~ /^(\d+)$/ ) { $DB_Argument = $main::included_DATABASES[0] unless $main::database[$1] }
    # when the database argument is the DB name and not it's internal number (we work internally with the internal number)
    $DB_Argument = &get_DB_number($DB_Argument) if ($DB_Argument !~  /^(\d+)$/ and $DB_Argument != -1); # -1 for All
  }
  else {
    $DB_Argument = $main::included_DATABASES[0]; 
  }
  if ($Search::displayAllMaps and @Several_GET_Databases) {  
    @Several_Search_Databases = @Several_GET_Databases;
    @Several_GET_Databases = ();
  }
}


# Now, Check if POSTED 'clickedMenu' exists to RE-DEFINE Arguments:

my $new_Search_Argument = 0;
foreach my $Search_key (keys %Search_Menu) {
  # for submitted "names" without values: check for $co->param("someName.x")
  if ($co->param('clickedMenu') eq $Search_key) {
    $Search_Argument = $Search_key;
    $new_Search_Argument = 1;
    last;
  }

}
if ($co->param('clickedMenu') and !$Search_Argument) {
  $Search_Argument = 'AC';
  $new_Search_Argument = 1;
}
if ($co->param('refresh maps') and !$Search_Argument) {
  $new_Search_Argument = 1;
}

# For ExPASy, we want to detect if this is an initial access, limit headers otherwise
my $headersLimitation = undef;
if ($SERVER::ExPASy and (%args or $co->param)) {
  $headersLimitation = 1;
}


#====================================#
#====================================#

# Multi Databases? Call and Display potential Remote Interfaces?

$main::multi_DBs = 1 if scalar(@main::DATABASES_Included) > 1;

if ($co->param('norecall')  or $args{'norecall'}) {
  undef($SERVER::remoteInterface) unless $SERVER::PortalWebService;
}
$SERVER::remoteInterfaceDefined = 1 if keys %{$SERVER::remoteInterface};
my $noPermitedGet = 0;
if ($SERVER::onlyInterfaces) {
  delete ($args{extract}); # we only accept the 'GET version' as GET, with no extraction!
  $noPermitedGet = 1 if grep {$_ !~ /make2d|format/} keys %args;
}

#====================================#
#====================================#

# Submitted maps chosen by clicking on an image have precedence over the list maps
if ($co->param('map_pattern') =~ /(\w{2,})/) {
  my $map_pattern = uc($1);
  $co->param(-name=>'choose_map', -values=>[$map_pattern]);
}
my $choosen_map_image = (split '\0', $co->param('choose_map_img'))[0];
$co->param(-name=>'choose_map', -values=>[$choosen_map_image]) if ($co->param('choose_map_img') =~ /\w/) and !$co->param('Execute query');

#====================================#

# SAVE TO FILE: Save Result and Exit. ( with GET - first argument is __SAVE_TO_FILE__, 2nd is filename and 3d is what is to be saved)
# (the 'GET' method has been desactivated/deprecated)
&SAVE_FILE_TO_DISK if ($co->param('Save to file') or $Search_Argument eq '__SAVE_TO_FILE__');



# OTHERWISE, NORMAL PROCESSING (display):
#----------------------------------------#
#----------------------------------------#


#========================================#
# Write headers                          #
#========================================#

# for 'core' connections: read login info and update their cookies expiration, otherwise read cookies

if ($SERVER::core_selector) {

  if ($SERVER::ExPASy) {
   die "This script is intended to be executed from the master server!\n"
     unless $ENV{GL_hostname} and $ENV{GL_hostname} eq $ENV{GL_master_release};
  }

  &CORE_DATABASE_LOGIN_DATA;

  if ($co->param('Submit Divers')) {
  }

}

# for all non 'core' connections:

else {

  if ($SERVER::remoteInterfaceDefined) {

    my @remoteInterfaces = sort keys %{$SERVER::remoteInterface};
    foreach my $remoteInterface (@remoteInterfaces) {
      next unless $remoteInterface;
      my $remoteInterfaceSubDB = (@{$SERVER::remoteInterface}{$remoteInterface}->{database})?
        @{$SERVER::remoteInterface}{$remoteInterface}->{database} : [];
      $remoteInterfaceSubDB = [1] unless @$remoteInterfaceSubDB;
      foreach my $remoteInterfaceSubDBElement (@$remoteInterfaceSubDB) {
        my $cookie_name = "2d_remoteInterfaceMapList_".$remoteInterface.":".$remoteInterfaceSubDBElement;
        unless ($co->cookie($cookie_name)) {

          my $full_script_name_reconnectionURL = ${$SERVER::remoteInterface}{$remoteInterface}->{URL}."?".'maplist&database='.$remoteInterfaceSubDBElement.'&norecall=1';
          my $current_response = '';
          $current_response = ($current_response =~ /$resultTextStartqm(.+)$resultTextEndqm/s)? $1 : '';
        }
        else {
          $SERVER::remoteInterfaceMaps{$remoteInterface.":".$remoteInterfaceSubDBElement} = $co->cookie($cookie_name);
        }
        # this is a hash
        $SERVER::remoteInterfaceMaps{$remoteInterface.":".$remoteInterfaceSubDBElement} = undef
          unless keys %{$SERVER::remoteInterfaceMaps{$remoteInterface.":".$remoteInterfaceSubDBElement}};
      }
    }

  } # $SERVER::remoteInterfaceDefined

  if ($co->param('Submit Divers')) {

    if ($co->param('Submit Divers') eq 'Submit Private password') {
      if ($co->param('private_password')) {
        $userPrivatePassword = $co->param('private_password');
        if ($userPrivatePassword eq $private_data_password) {
          my $userPrivatePasswordEncrypted = _crypting_string_($userPrivatePassword,'',0);
          $userPrivatePasswordEncrypted = $co->cookie(-name=>"2d_".$name2dAlphaNumeric."_private_password", value=>$userPrivatePasswordEncrypted, -expires=>'+1M');
          push (@SERVER::headerCookies, $userPrivatePasswordEncrypted);
        }
        else {
          undef($userPrivatePassword);
        }
      }
    } # 'Submit Private password'

    elsif ($co->param('Submit Divers') eq 'Desactivate Private access') {
      my $userPrivatePasswordRemoval = $co->cookie(-name=>"2d_".$name2dAlphaNumeric."_private_password", value=>'', -expires=>'now');
      push (@SERVER::headerCookies, $userPrivatePasswordRemoval);
    }

  } # 'Submit Divers'

}

print $co->header(-cookie=>[@SERVER::headerCookies], -expires=>'12h');

print "\n";

my $display_version = ($VERSION =~ /\.\d{2}/)? $VERSION : ($VERSION =~ /^\d+$/)? $VERSION.".00" : $VERSION."0";
   $display_version.= ($sub_VERSION)? ".$sub_VERSION" : '';
my $link_version_to_readme = "ver. $display_version";


#======================================================================#
# Extract Part ('extract' argument, or several selected databases)     #
# 'Only Interfaces' can contact remote interfaces, but GET is excluded #
#======================================================================#


# chosen maps are a special case, with several databases they are in the format "[interface] *$database -- map" so we can limit to one db:
if (!$new_Search_Argument and $co->param('choose_map') =~ /^(?:\[[^\]]*\])?\s*\*(.*?)\s+\-\-\s+(.+)$/) {
  my $choose_map_databaseName = $1;
  my $choose_map = $2;
  $SERVER::limitToDatabase = $choose_map_databaseName;
  if ($co->param('choose_map') =~ /^\[([^\]]+)\]/) {
    $SERVER::limitToInterface = $1 if $SERVER::remoteInterface->{$1};
  }
  $choose_map_databaseName = $main::database[$choose_map_databaseName]{database_name} if $choose_map_databaseName =~ /^\d+$/;
  $co->param(-name=>'selected_databases', -values=>[$choose_map_databaseName]);
  $co->param(-name=>'choose_map', -values=>[$choose_map]);
}

# If norecall, delete all related remote interface parameters
if ($co->param('norecall')) {
  $co->delete('selected_remoteinterfaces');
  $co->delete('selected_remoteinterfaces_and_noLocalDBs');
}

my @Several_Posted_Databases =
 (($co->param('selected_remoteinterfaces_and_noLocalDBs') and $co->param('selected_remoteinterfaces'))
   or $SERVER::limitToInterface or $SERVER::onlyInterfaces)? () :
  ($co->param('selected_databases'))? $co->param('selected_databases') : ();
# 'non-multi' local database has to be addressed by 'POST' if a remote interface is selected
@Several_Posted_Databases = ($main::DATABASES_Included[0]) if
  ($co->param('selected_remoteinterfaces') and !@Several_Posted_Databases and !$SERVER::limitToInterface and
  (!$co->param('selected_remoteinterfaces_and_noLocalDBs') and !$SERVER::onlyInterfaces));

my $allInterfacesTextQM = quotemeta($allInterfacesText);
my @Several_Remote_Interfaces = ();
if ($co->param('selected_remoteinterfaces')) {
  if ($SERVER::limitToDatabase) {
    @Several_Remote_Interfaces = ($SERVER::limitToInterface)
      if grep {$_ =~ /^$SERVER::limitToInterface$/i} keys %{$SERVER::remoteInterface};
  } else {
  @Several_Remote_Interfaces = ($co->param('selected_remoteinterfaces'));
  }
}
@Several_Remote_Interfaces = sort keys %{$SERVER::remoteInterface} if grep {$_ =~ /$allInterfacesTextQM/} @Several_Remote_Interfaces;
if ($SERVER::onlyInterfaces and !@Several_Remote_Interfaces and !$co->param('portalWebService')) {
  if ($co->param('norecall')) { # when called, query all remote interfaces
    @Several_Remote_Interfaces = sort keys %{$SERVER::remoteInterface};
  }
  elsif ($SERVER::remoteInterfaceDefault) {
    @Several_Remote_Interfaces = ($SERVER::remoteInterfaceDefault);
    $SERVER::remoteInterfaceDefaultActivated = $SERVER::remoteInterfaceDefault;
  }
  else {
    foreach my $remoteInterface (sort keys %{$SERVER::remoteInterface}) {
      next if $remoteInterface =~ /$allInterfacesTextQM/ or
        !$SERVER::remoteInterface->{$remoteInterface}->{activated};
      @Several_Remote_Interfaces = ($remoteInterface);
    }
  }
}


my $allDatabases;
my $allDatabasesFromRemoteInterface;

if (grep { $_ =~ /^all$/i } @Several_Posted_Databases) {
  @Several_Posted_Databases = @main::DATABASES_Included;
  $allDatabases = 1;
  $allDatabasesFromRemoteInterface = 1 if $co->param('norecall');
  map {$_ = $main::database[$_]{database_name}} @Several_Posted_Databases;
  $co->param(-name=>'selected_databases', -values=>['All']);
}
elsif (scalar (@Several_Posted_Databases) > 1) {
  map {$_ = &get_DB_number($_) unless $_ =~ /^\d+$/} @Several_Posted_Databases;  
}

undef(@Several_Posted_Databases) unless scalar (@Several_Posted_Databases) > 1 or @Several_Remote_Interfaces;


if ( (defined($args{extract}) and !$args{norecall}) or
     (@Several_GET_Databases and !$args{norecall}) or
     (  (@Several_Posted_Databases or @Several_Remote_Interfaces) and 
        (!$co->param('norecall') or $allDatabasesFromRemoteInterface) and
        ($co->param('Execute query') or $co->param('hidden_reference')) and !$new_Search_Argument) or
        ($co->param('norecall') and $SERVER::PortalWebService and !$noPermitedGet)
    ) {
# add aslo several get databases (&database=all/1+2+..)

  my $GetString = $ENV{QUERY_STRING};
  $GetString =~ s/\&?extract\=?[^&]*//gi;
  $GetString .= '&norecall=1';

  my $full_script_name_reconnectionURL = $full_script_name;
  my $localhost_identification;
  $localhost_identification  = $SERVER::localHostID if $SERVER::localHostID;
  $localhost_identification .= ':'.$SERVER::localHostPass if $SERVER::localHostPass;  
  $full_script_name_reconnectionURL =~ s/^(http:\/\/)?/$1$localhost_identification\@/i if $localhost_identification;

  my ($response, $current_response, $anchorIndex, $initiatedByAnotherInterfaceMsg);
 (my $preStart, my $preEnd) = ($args{extract} eq 'pre' )? ('<PRE>','</PRE>') : ('','');

  if (@Several_GET_Databases) {

    foreach my $get_database (@Several_GET_Databases) {
      $GetString =~ s/\&(database|db)\=[^&]+/&database=$get_database/i;
      $full_script_name_reconnectionURL .= "?".$GetString;
      $current_response = LWP_AGENT('GET', $full_script_name_reconnectionURL);
      $current_response = ($current_response =~ /$resultTextStartqm(.+)$resultTextEndqm/s)? $preStart.$1.$preEnd :
                          ($current_response =~ /$resultStartqm(.+)$resultEndqm/s)? $1 : $current_response;
      $response .= $current_response."\n";
      $response .= $co->br.$co->br.$co->br unless ($args{format} eq 'raw');
    }
    $response =~ s/\n$//;
    if (defined($args{extract})) {
      print $response;
      exit;
    }

  }

  elsif (defined($args{extract})) {

    undef($co);

    $full_script_name_reconnectionURL .= "?".$GetString;

    $current_response = LWP_AGENT('GET', $full_script_name_reconnectionURL);
    $current_response = ($current_response =~ /$resultTextStartqm(.+)$resultTextEndqm/s)? $preStart.$1.$preEnd :
                        ($current_response =~ /$resultStartqm(.+)$resultEndqm/s)? $1 : $current_response;
    $response .= $current_response;
    print $response;
    exit;

  }

  elsif (@Several_Posted_Databases or @Several_Remote_Interfaces) {

    $co->param(-name=>'norecall', -values=>[1]);

    # use the following signal to prevent a portal when contacting another portal (or even itself)
    # that the later continues on the chain, resulting in a potentiel infinite cycle!
    $co->param(-name=>'portalWebService', -values=>[1]) if $SERVER::PortalWebService;

    # we propagate the initial querying interface (may be useful in the future, specially for PortalWebService)
    if ($co->param('initiatedByInterface')) {
      $co->param(-name=>'initiatedByAnotherInterface', -values=>[1])
    } else {
      $co->param(-name=>'initiatedByInterface', -values=>["$name2d"]);
    }

    # Modify some queries:
    # we will transform 'Search by accession number' into  Combined search to reduce display and redundancies
    my $remoteQueryModified = undef;
    if (@Several_Remote_Interfaces and $co->param('hidden_reference') eq 'AC' and $co->param('search_AC')) {
      $remoteQueryModified = 'AC->CombinedAC';
      my $search_AC = $co->param('search_AC');
      $co->param(-name=>'hidden_reference', -values=>['Combined']);
      $co->param(-name=>'combined_field1', -values=>[' Accession number']);
      $co->param(-name=>'include_field1', -values=>["$search_AC"]);
      $co->param(-name=>'Combined_exact', -values=>[1])
        if $co->param('AC_pattern') eq 'exact_AC'; # reserved for Web interfaces' queries
      $co->param(-name=>'inner_and_or_field1', -values=>['OR']);
      $co->param(-name=>'combined_output_fields', -values=>['Description']);
      $co->delete('search_AC');
    }

    my @Several_Posted_Databases_copy = @Several_Posted_Databases;

    if (@Several_Posted_Databases and @Several_Remote_Interfaces) {
      my $localDatabasesTtl = 'Local Database'.((scalar @Several_Posted_Databases > 1)? 's' : '');
      $anchorIndex .= $co->li({-class=>"compact"}, $co->a({href=>"#".'localDatabases'}, "$localDatabasesTtl"));
      $response.= $co->a({-name=>'localDatabases'},'').
        $co->h2({class=>'center'}, $co->strong({-class=>'H2Font'}, '\'').
        $co->strong({-class=>'H2Font underlined red'}, "$localDatabasesTtl").
        $co->strong({-class=>'H2Font'}, '\''));
    }

    # postgreSQL defined databases
    my $topBorderThin = '';

    while ( my $current_database = shift(@Several_Posted_Databases_copy) ) {

      next unless $current_database;
      $co->param(-name=>'selected_databases', -values=>[$current_database]);    
      my %current_params = $co->Vars;

      $current_response = LWP_AGENT('POST', $full_script_name_reconnectionURL, \%current_params); 
      $current_response = ($current_response =~ /$resultTextStartqm(.+)$resultTextEndqm/s)? $preStart.$1.$preEnd :
                          ($current_response =~ /$resultStartqm(.+)$resultEndqm/s)? $1 : $current_response;
      $response .= $co->br.$topBorderThin.$co->h3({-class=>'center underlined darkBlue'},$main::database[$current_database]{database_name}).$current_response if $current_response;
      $topBorderThin = $co->hr({-class=>'topBorderThin'});

    }

    # URL defined interfaces
    my @Several_Remote_Interfaces_copy = @Several_Remote_Interfaces;
    my $firstInterface = (@Several_Posted_Databases)? undef : 1;

    while ( my $current_interface = shift(@Several_Remote_Interfaces_copy) ) {

      next unless $current_interface and ${$SERVER::remoteInterface}{$current_interface}->{URL} and ${$SERVER::remoteInterface}{$current_interface}->{activated};
      next if $SERVER::limitToInterface and $current_interface ne $SERVER::limitToInterface;

      my $remoteInterfaceURL = ${$SERVER::remoteInterface}{$current_interface}->{URL};
      my $remoteInterfaceURI = (${$SERVER::remoteInterface}{$current_interface}->{URI})?
         ${$SERVER::remoteInterface}{$current_interface}->{URI} : $remoteInterfaceURL;
      my $portalSubTitle .= ($SERVER::PortalWebService and $co->param('norecall'))?
        $co->h3({class=>'center'}, $co->strong({-class=>'H3Font'}, "[contacted by: '$name2d']")) : '';
      $anchorIndex .= $co->li({-class=>"compact"}, $co->a({href=>"#"."$current_interface-$name2d"}, "Remote interface \'$current_interface\'"));
      if ($firstInterface) {
        if ($co->param('initiatedByAnotherInterface')) {
          my $initialInterface = $co->param('initiatedByInterface');
          $initiatedByAnotherInterfaceMsg = $co->h3({class=>'center'}, $co->strong({-class=>'H3Font'}, "[contacted by this interface: '$initialInterface']"));
        }
      } else {
        $response.= $co->br.$co->hr({-class=>'topBorder'});
      }
      $response.=
     (($anchorIndex and !$firstInterface)? $co->a({href=>"#".'ressourcesList-'."$name2d"},"[Back to '$name2d' list of ressources]").
         $co->a({-name=>"$current_interface-$name2d"},'') : '').
      $co->h2({class=>'center'}, $co->strong({-class=>'H2Font'}, 'Remote Interface \'').
      $co->a({-href=>"$remoteInterfaceURI", -target=>"_blank"},$co->strong({-class=>'H2Font red'}, $current_interface)).$co->strong({-class=>'H2Font'}, '\'')).$portalSubTitle;
      undef($firstInterface);
      my $remoteInterfaceDomain = ($remoteInterfaceURL =~ /^(http:\/\/[^\/]+)/i )? $1 : '';

      ${$SERVER::remoteInterface}{$current_interface}->{database} = [1] unless
        ${$SERVER::remoteInterface}{$current_interface}->{database};
      my $notSameMake2DVersion = '';
      my $get_remote_interface_version = '';
      my ($remote_interface_version, $remote_interface_subversion) = ();
      my @current_interfaceSubDatabases = ($SERVER::limitToDatabase)? ($SERVER::limitToDatabase) :
         @{${$SERVER::remoteInterface}{$current_interface}->{database}};
      foreach my $current_interfaceSubDatabase (@current_interfaceSubDatabases) {
        unless ($get_remote_interface_version) {
          $get_remote_interface_version =
            LWP_AGENT('GET', ${$SERVER::remoteInterface}{$current_interface}->{URL}.'?make2d&format=text&extract');
          $get_remote_interface_version = $1 if
            $get_remote_interface_version =~ /$resultTextStartqm(.+)$resultTextEndqm/s;
         ($remote_interface_version, $remote_interface_subversion) = ('1.xx','x');
         ($remote_interface_version, $remote_interface_subversion) = ($1,$2) if
            $get_remote_interface_version =~ /\bversion\:\s*(\S+).+\bsub\-version\:\s*(\S+)/i;
          $remote_interface_version = '1.xx' unless $remote_interface_version;
          $remote_interface_subversion = 'x' unless $remote_interface_subversion;
          if ($VERSION != $remote_interface_version) {
          # (or $sub_VERSION ne $remote_interface_subversion) -> no, generally sub-versions are compatible
            my $remote_display_version =
              ($remote_interface_version =~ /\.\d{2}/ or $remote_interface_version eq '1.xx')? $remote_interface_version :
                ($remote_interface_version =~ /^\d+$/)? $remote_interface_version.".00" : $remote_interface_version."0";
            $remote_display_version.= ($remote_interface_subversion)? ".$remote_interface_subversion" : '';
            $notSameMake2DVersion = $co->h4({class=>'center Comment'},
              'The Make2D-DB system running on this remote interface (ver.&nbsp;'.$remote_display_version.') '.
              'is different from the system running on this server (ver.&nbsp;'.$display_version.'). '.
              'Some queries may not be correctly interpreted on such a situation.').$co->br;
          }
        }
        $co->param(-name=>'selected_databases', -values=>[$current_interfaceSubDatabase]);
        my %current_params = $co->Vars;
        if ($remote_interface_version < $VERSION) {
          $current_params{'hidden_reference'} = 'Map' if $current_params{'hidden_reference'} eq 'MapExpInfo';
        }
        $current_response = LWP_AGENT('POST', ${$SERVER::remoteInterface}{$current_interface}->{URL}, \%current_params);
        $current_response = ($current_response =~ /$resultTextStartqm(.+)$resultTextEndqm/s)? $preStart.$1.$preEnd :
                            ($current_response =~ /$resultStartqm(.+)$resultEndqm/s)? $1 : $current_response;
        if ($current_response =~ /\n\<\!DOCTYPE html/) {
          $current_response = $co->h4({class=>'center'},"Could not extract data from ".$co->strong($current_interface)."!".
                              " This server may be using another version of Make2D-DB II".
                              " which does not directly interpret this query.");
        }
        elsif (!$current_response) {
          $current_response = $co->h4({class=>'center'},"Could not extract data from ".$co->strong($current_interface)."!".
                              " This server may be (temporarily) inaccessible, or your query was too".
                              " general, which may cause a time out on remote servers.");
        }
        if ($remoteInterfaceDomain) {
          # target to "$name2d - Remote Interfaces" (unique, but doesn't automatically pop-up) or _blank?
          $current_response =~ s/\b(href|src)\s*\=\s*\"\s*\//target\=\"_blank\" $1 = \"$remoteInterfaceDomain\//gi;
         (my $remoteInterfaceTruncURL = ${$SERVER::remoteInterface}{$current_interface}->{URL}) =~ s/[^\/]+$//;
          $current_response =~ s/\b(href)\s*\=\s*\"\s*(2d)/target\=\"_blank\" $1 = \"$remoteInterfaceTruncURL$2/gi;
          $current_response =~ s/http:\/\/(?:localhost|127\.0\.0\.1)/$remoteInterfaceDomain/gi;
        }
       (my $current_interfaceTr = '_'.$current_interface) =~ s/\W/_/g;
        $current_response =~ s/JavaScript:OpenViewerForSpot/JavaScript\:OpenViewerForSpot$current_interfaceTr/gi;
        my $localRemoteSubDatabase = $co->br.($SERVER::limitToDatabase)?
          $co->h3({-class=>'center underlined darkBlue'}, $SERVER::limitToDatabase) : '';
        $current_response = $co->h3('Sorry, the system running on this remote interface is not up-to-date. It cannot correctly run this specific query!') if $current_response =~ /Bad Query: ERROR:/;
        $response .= $localRemoteSubDatabase.$notSameMake2DVersion.$current_response if $current_response;
      }
    }

    $co->delete('norecall');
    $co->delete('portalWebService') if $SERVER::PortalWebService;

    if ($remoteQueryModified) {
      if ($remoteQueryModified eq 'AC->CombinedAC') {
        $co->param(-name=>'hidden_reference', -values=>['AC']);
        my $search_AC = $co->param('include_field1'); $co->param(-name=>'search_AC', -values=>["$search_AC"]);
        $co->delete('combined_field1'); $co->delete('include_field1');
        $co->delete('Combined_exact') if $co->param('Combined_exact');
        $co->delete('inner_and_or_field1'); $co->delete('combined_output_fields');
      }
    }

    if ($allDatabases) {
      $co->param(-name=>'selected_databases', -values=>['All']);
    }
    else {
      $co->param(-name=>'selected_databases', -values=>[@Several_Posted_Databases]);    
    }

  }

  $response = $co->a({-name=>'ressourcesList-'.$name2d},'').$co->strong("'$name2d' list of ressources:").
              $co->ul($anchorIndex).$co->hr({-class=>'topBorder'}).$response if $anchorIndex and $response;
  $response = $initiatedByAnotherInterfaceMsg.$response if $initiatedByAnotherInterfaceMsg;
  $main::SeveralResponse = $response if $response;

}

# undef(@Several_Posted_Databases); # still used later


#========================================#
# Start HTML                             #
#========================================#

my $formName = 'Form1';

my $style  = shared_definitions("style");

my $script = shared_definitions("script:cookies");
   $script.= shared_definitions("script:GetElementValue");
   $script.= shared_definitions("script:OpenViewerForSpot", "$map_viewer$map_viewer_qmark");
   $script.= shared_definitions("script:OpenGraphViewerForSpot");

   $script.= shared_definitions("script:ClickMenu", $formName);

if ($SERVER::remoteInterfaceDefined) {
  foreach my $remoteInterface (keys %{$SERVER::remoteInterface}) {
    next if $SERVER::limitToInterface and $remoteInterface ne $SERVER::limitToInterface;
    my $remoteInterface_URL = ${$SERVER::remoteInterface}{$remoteInterface}->{URL};
    my $remoteInterface_modeRewriteOn = (${$SERVER::remoteInterface}{$remoteInterface}->{URL} =~ /\w+\.\w+$/)? 0 : 1;
   (my $remoteInterface_higherURL = ${$SERVER::remoteInterface}{$remoteInterface}->{URL}) =~ s/\/?[^\/]*$//;
    my $remoteInterface_map_viewer = ($remoteInterface_modeRewriteOn)?
         ${$SERVER::remoteInterface}{$remoteInterface}->{URL}.'/viewer&'
       : $remoteInterface_higherURL.'/'.$main::map_viewer_cgi.'?';
   (my $remoteInterfaceTr = '_'.$remoteInterface) =~ s/\W/_/g;
    $script.= shared_definitions("script:OpenViewerForSpot", "$remoteInterface_map_viewer","$remoteInterfaceTr");    
  }

}

my ($httpEquiv,$content) = ($noPermitedGet)? ('refresh', "10; URL=$url_www2d" ) :
   ('Content-Type', 'text/html;charset=iso-8859-1');

unless ($SERVER::ExPASy) {

  print $co->start_html

  (
    -title   => "$name2d",
    -author  => "$email",
    -meta    => {'keywords'=>'2-D PAGE SDS Two-dimensional polyacrylamide gel electrophoresis Database'},
    -head    => [meta({'http-equiv'=>"$httpEquiv", 'content'=>"$content"})],
    -bgcolor => "$bkgrd",
    -link    => 'blue',
#    -style   => {'-code'=>$style},
    -script => "$script",
#    -script  => ["$script", 
#    {type=>"text/javascript", src=>"./js/dojo-release-1.3.1/dojo/dojo.js"}, 
#    {type=>"text/javascript", src=>"./js/cipro.js"}],
    -dtd     => '"-//W3C/DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"',
  ),"\n";

}

else {

  $style =  "\n\n<style type=\"text/css\">\n<!--\n\n".$style."\n-->\n</style>";
  $script = "\n\n<script language=\"JavaScript\" type=\"text/javascript\">\n<!-- Hide script\n\n".$script.
            "\n   // End script hiding -->\n</script>\n\n";

#  print_head_html("$name2d", $style.$script);
  print_head_html("$name2d", $script);

}


undef($style); undef($script);

FIN({'swiss-2dpage'=>1, no_mirror=>1}) if ($SERVER::ExPASy);

if ($SERVER::deactivated or ($SERVER::onlyInterfaces and !$SERVER::remoteInterfaceDefault) or
    $noPermitedGet ) {
  if ($SERVER::deactivated) {
    print $co->h3({-class=>'red'}, "Sorry, the '$name2d' search engine is temporarily deactivated!");
  } elsif ($SERVER::onlyInterfaces and !$SERVER::remoteInterfaceDefault) {
    print $co->h3({-class=>'red'}, "Sorry, this interface is defined to be a portal, ".
     "but no remote databases are currently activated within the configuration files!");
  } elsif ($noPermitedGet) {
    print $co->h3({-class=>'red'}, "Sorry, this interface is set up to be a portal with no local data.").
     $co->h3("It acts as a Web server, and can only be currently interogated either by POST methods or interactively!").
     $co->h3({-class=>"center"}, "If you are not redirected in 15 seconds please ".
       $co->a({-href=>"$url_www2d"}, "click here"));
  }

  print $co->h3({-class=>'Comment'}, "For more information, please contact $email.") if $email;
  FIN({'swiss-2dpage'=>1, noLinkFile=>1}) if ($SERVER::ExPASy);
  print $co->end_html;
  exit 0;
}

my ($print, $text_output);
# include file
if ($main::db_server_include_file) {
  my $db_server_include_file = $main::db_server_include_file;
  $db_server_include_file = "$html_path/$web_server_ref_name/data/$db_server_include_file" unless $db_server_include_file =~ /\//;
  if (-e $db_server_include_file) {
    my $FILE = new IO::File;
    $FILE->open("<$db_server_include_file");
    print (<$FILE>);
    $FILE->close;
  }
  else { # only if server is configured to accept includes!
    print "\n<!--#include file=\"$db_server_include_file\" -->\n";
  }
}

# if 'No script' on Browser
print "\n\n"."<noscript>"."\n".
      "<BR><H3><STRONG>!!This displayer contains script commands. You should enable JavaScripting within your browser preferences, or use another browser!!"."\n".
      "</STRONG></H3><BR><BR></noscript>\n\n" unless $SERVER::ExPASy;


print $co->p if $SERVER::ExPASy;

if ($db_server_logo) {
    print $co->start_table()."\n"
} else {
    print $co->start_table({align=>"center"})."\n";
}

print $co->start_Tr()."\n";

print $co->start_td."\n";
if ($db_server_logo and !$headersLimitation) {
  if ($main::home_url) {
    my $logo_ex = "". ( ($db_server_logo_width)?
             $co->img({-src=>"$db_server_logo", -align=>'left', -width=> $db_server_logo_width, -border=>0, -alt=>"$main::home_displayed_text"})
            :
             $co->img({-src=>"$db_server_logo", -align=>'left', -border=>0, -alt=>"$main::home_displayed_text"})
    );
#    print $co->a({-class=>'Transparent', href=>"$main::home_url"}, $logo_ex)."\n";
  } else {
    print "". ( ($db_server_logo_width)? 
             $co->img({-src=>"$db_server_logo", -align=>'left', -width=> $db_server_logo_width, -border=>0, -alt=>"$name2d image"})
            :
             $co->img({-src=>"$db_server_logo", -align=>'left', -border=>0, -alt=>"$name2d image"})
    )
  }
}
print $co->end_td."\n";


if ($SERVER::core_selector and $SERVER::set_dbname) {
  $DB::CurrentDatabaseName = $SERVER::set_dbname;
}
else {
  if ($co->param('selected_databases')) {
    my @selected_databases = $co->param('selected_databases');
    map {$_ = $main::database[$_]{database_name} if $_=~/^\d+$/ and $main::database[$_]{database_name}} @selected_databases;
    $DB::CurrentDatabaseName = join ' & ', @selected_databases;
  }
  else {
    $DB::CurrentDatabaseName = $main::database[$DB_Argument]{database_name};
  }
}


# Read Last database from Cookie, affect it if no database name has been submitted
my $cookie_2d_LastDatabase =  "2d_".$name2dAlphaNumeric."_LastDatabase";
$cookie_2d_LastDatabase =~ s/\s+/_/g;
unless ($DB::CurrentDatabaseName) {
  if (cookie("$cookie_2d_LastDatabase") and !$SERVER::need_login) { # Cookie stores the DB name and not it's number
    $DB::CurrentDatabaseName = cookie("$cookie_2d_LastDatabase");
    $DB::CurrentDatabase = &get_DB_number($DB::CurrentDatabaseName);
    $DB::CurrentDatabaseName = $main::database[$DB::CurrentDatabase]{database_name} if !$DB::CurrentDatabaseName or $DB::CurrentDatabaseName eq 'undefined';
  }
}
unless ($DB::CurrentDatabase) {
  $DB::CurrentDatabase = &get_DB_number($DB::CurrentDatabaseName); # default database is sent back if database_name is invalid
  $DB::CurrentDatabaseName = $main::database[$DB::CurrentDatabase]{database_name} if !$DB::CurrentDatabaseName or $DB::CurrentDatabaseName eq 'undefined';
}

($DB::CurrentDatabaseNameURI = lc($DB::CurrentDatabaseName)) =~ s/\s/_/g;
 $DB::ArgumentCurrentDatabaseNameURI =  $main::multi_DBs? "\&database=$DB::CurrentDatabaseNameURI" : '';

#--------------------------------------------------------------------------------#
# Now, $DB::CurrentDatabaseName is implicitly known as well as $DB::CurrentDatabase!!
# Now, we know which database is finally invoked!!
#--------------------------------------------------------------------------------#

$sample_origin_URL = ($main::database[$DB::CurrentDatabase]{sample_origin_URL})? $main::database[$DB::CurrentDatabase]{sample_origin_URL} : $main::default_sample_origin_URL;

$image_type = ($main::database[$DB::CurrentDatabase]{image_type})? $main::database[$DB::CurrentDatabase]{image_type} : $main::default_image_type;

my $currentDatabaseNameForTitle = $DB::CurrentDatabaseName;
my ($name2d_database_name, $title);
if ($SERVER::core_selector and $main::database[$DB::CurrentDatabase]{database_name}
  and $main::database[$DB::CurrentDatabase]{database_name} ne $currentDatabaseNameForTitle) {
  my $publicTitleDBname = "<br>(public name: ".$main::database[$DB::CurrentDatabase]{database_name}.")";
  $name2d_database_name = ($DB::CurrentDatabaseName and $main::multi_DBs)? $name2d." (".$currentDatabaseNameForTitle.")" : $name2d;
  $title = ($DB::CurrentDatabaseName and $main::add_database_name_to_title)? $name2d.": ".$currentDatabaseNameForTitle.$publicTitleDBname: $name2d;
} else {
   $currentDatabaseNameForTitle =~ s/_/ /g;
   $name2d_database_name = ($DB::CurrentDatabaseName and $main::multi_DBs)? $name2d." (".$currentDatabaseNameForTitle.")" : $name2d;
   $title = ($DB::CurrentDatabaseName and $main::add_database_name_to_title)? $name2d." (".$currentDatabaseNameForTitle.")": $name2d;
}

=pod
unless ($SERVER::ExPASy and $headersLimitation ) {
  print $co->start_td."\n";
  print $co->h1({class=>"left"}, "$title"),"\n";
  if ($SERVER::homeSubHeader and !$SERVER::core_selector) {
    if ($SERVER::ExPASy) {
      print $co->h3({class=>"left darkBlue"},$SERVER::homeSubHeader);
    } else {
    print $co->h3({class=>"subHeader left"},$SERVER::homeSubHeader);
    }
  }
  print $co->end_td."\n";
}

if ($SERVER::core_selector) {
  print $co->td.$co->td.$co->start_td({align=>'right'})."\n";
  &CORE_DATABASE_LOGIN_FORM;
  print $co->end_td."\n";
}

print $co->end_Tr."\n";
print $co->end_table."\n";

print $co->hr, "\n";
=cut
#============================================================================================#


# Called Without Form (a GET QUERRY):
#------------------------------------

# A valid get querry have a first argument beginning with 'search_'

# we consider not precised single argument as an AC/ID entry (with no other parameters)

$args{ac} = $1, $Search_Argument_key = defined if ($ARGV[0] =~ /^(\w+)$/) and !defined($Search_Argument_key);

my $parent_is_a_GET_QUERRY;
$parent_is_a_GET_QUERRY = 1 if defined($Search_Argument_key);

my $exact_search;

if ($args{ac}) {

  my ($search_ac, $spot_id);
  $search_ac = $args{ac};
  $spot_id = $args{spot} if $args{spot};
  $co->append(-name=>'Execute query', -values=>'true');
  $co->append(-name=>'hidden_reference', -values=>'AC');
  if ($args{format} =~ /^te?xt$/) {
    $co->append(-name=>'hidden_reference_text_entry', -values=>'AC')
  }
  elsif ($args{format} =~ /^raw$/) {
    $co->append(-name=>'hidden_reference_raw_entry', -values=>'AC')
  }
  $co->append(-name=>'search_AC', -values=>"$search_ac");
  $co->append(-name=>'search_SPOT', -values=>"$spot_id") if $spot_id;

}

elsif ($args{de} or $args{gn} or $args{kw}) {
  my ($search_de, $search_de_limited) = ($args{de})? ($args{de},'DE') : ($args{de})? ($args{gn},'GN') : ($args{kw},'KW');
  $co->append(-name=>'hidden_reference', -values=>'DE'); 
  $co->append(-name=>'search_description', -values=>"$search_de");
  $co->append(-name=>'search_description_limited', -values=>"$search_de_limited");
  $co->append(-name=>'include_external_data', -values=>1);
}

elsif ($args{map} or $args{spot}) {

  if ($args{map} =~ /^(all|\*)$/i) {
    $args{map} = '';
  }
  elsif ($args{spot}) {
    if ($args{spot} =~ s/(\S+)\:(\S+)//) {
      $args{map} = $1;
      $args{spot} = $2;
      $exact_search = 1;
    }
    my $search_map = ($args{map})? $args{map} : 'All Maps';
    my $search_spot = $args{spot};
    $co->append(-name=>'hidden_reference', -values=>'Spot');
    $co->append(-name=>'search_spot_number', -values=>"$search_spot");
    $co->append(-name=>'choose_map', -values=>"$search_map");
  }
  else { # $args{map} and !$args{spot}
    my $search_map = $args{map};
    my $mapHiddenReference = (exists $args{info})? 'MapExpInfo' : 'Map';
    $co->append(-name=>'hidden_reference', -values=>"$mapHiddenReference");
    $co->append(-name=>'choose_map', -values=>"$search_map");
  }

}

elsif ($args{author}) {
  my $search_author = $args{author};
  $co->append(-name=>'hidden_reference', -values=>'Author'); 
  $co->append(-name=>'authors_list', -values=>"$search_author");
}

elsif (length($args{combined})>2) {
  my $search_combined = $args{combined};
  $co->append(-name=>'hidden_reference', -values=>'Combined');
  $Combined::co = new CGI;
  $Combined::co->append(-name=>'combined_field1', -values=>' Full Entry');
  $Combined::co->append(-name=>'include_field1', -values=>"$search_combined");
  $Combined::co->append(-name=>'combined_output_fields', -values=>'Description');
}


# more arguments

elsif (defined($args{make2d})) {
  $co->append(-name=>'hidden_reference', -values=>'make2d');
}

elsif (defined($args{index})) {
  my $index = $args{index};
  $index = ($index =~ /^0|uniprot$/i or !$index)? defined : ($index =~ /^1|swiss-?prot$/i)? 1 : 
           ($index =~ /^2|trembl$/i)? 2 : ($index =~ /^3|swiss-?2dpage$/i)? 3 : undef;
  if (defined($index)) {
    $co->append(-name=>'hidden_reference', -values=>'index');
    $co->append(-name=>'hidden_reference_more_arguments', -values=>"$index");
  }
}

elsif (defined($args{maplist})) {
  $co->append(-name=>'hidden_reference', -values=>'maplist');
}

elsif (defined($args{databaselist})) {
  $co->append(-name=>'hidden_reference', -values=>'databaselist');
}

if ($DB_Argument =~ /^(-?\d+)$/) { # Database Number (-1 for 'All')
  # overwrite the cookie value #
  $DB::CurrentDatabase = $1; # KHM, reconsider the -1 => All
}

#============================================================================================#


# Write Elements:
#===============#

my ($expandSubSection, $expandSign, $buttonClass);


# Start Form

print $print = ($parent_is_a_GET_QUERRY or $SERVER::apacheModeRewriteOn)?
    $co->start_multipart_form(-method=>'POST', -action =>"$script_name", -name=>"$formName")."\n"
  : $co->start_multipart_form(-method=>'POST', -name=>"$formName")."\n" ;

# higlight selected search
(my $trunc_argument_part = $Search_Argument) =~ s/^(.*?)(\_\_)+.*$/$1/; # less greedy
my $currentQuery = ($trunc_argument_part)? $trunc_argument_part : ($co->param('hidden_reference'))? $co->param('hidden_reference') : '';
my $button_font_color = '8F4F4F';
my $button_length = 18;
my %triangle_image;
my $triangle_void = '&nbsp;'.$co->img({-src=>"$icons/2d_triangle_void.gif", -alt=>""}); 
my %fixedMenu;
if ($currentQuery) {
  $Search_Menu_icons{$currentQuery} =~ s/\.(gif|jpg)$/_on.$1/; # higlighted icons are named with a '_on' suffix
  $Search_Menu_TextStyle{$currentQuery} = 'MenuLinkOnTxt';
  $triangle_image{$currentQuery} =  '&nbsp;'.$co->img({-src=>"$icons/2d_triangle.gif", -alt=>"*"});
  $fixedMenu{$currentQuery} = 'FixedMenuSelected'; 
}
if ($menu_graphic_mode == 0 or $menu_graphic_mode == 2) {
  foreach my $arg (keys %Search_Menu) {
    next if $arg eq $currentQuery;
    $triangle_image{$arg} = $triangle_void;
    $fixedMenu{$arg} = 'FixedMenu'; 
  }
}

my ($script_DB_argument, $viewer_DB_argument);
if ($DB::CurrentDatabase and $main::multi_DBs) {
  $script_DB_argument = "+".$DB::CurrentDatabase; # used when we replace submit images buttons by the get buttons (OnClick)
  $viewer_DB_argument = ($SERVER::apacheModeRewriteOn)? "&database=" : "?+"; # if not, the DB will be the second argument for the viewer
  $viewer_DB_argument.= $DB::CurrentDatabaseNameURI;
}




my (%showed_DATABASES, @showed_DATABASES, $default_DATABASE);

# push @showed_DATABASES, 'All'; # KHM
$default_DATABASE = $DB::CurrentDatabaseName if $DB::CurrentDatabaseName;
foreach my $DB_number (@main::DATABASES_Included) {
  my $database_name = $main::database[$DB_number]{database_name};
  push @showed_DATABASES, $database_name;
  $showed_DATABASES{$database_name} = $DB_number;

  # make the first database the default one, check the defined value 'undefined'!
  if ((!$DB::CurrentDatabase or $DB::CurrentDatabase eq 'undefined') and !$default_DATABASE) {
    $default_DATABASE = $database_name;
  }
  elsif ($showed_DATABASES{$DB::CurrentDatabase} eq $database_name) { # database param is read as CurrentDatabase
    $default_DATABASE = $database_name;
  }
}
$showed_DATABASES{All} = -1; # the value '-1' will indicate all databases selected (deprecated)


my @selected_DATABASES_numbers;
if ($co->param('selected_databases')) {
  my @selected_databases = $co->param('selected_databases'); # ??
}
else {
  @selected_DATABASES_numbers = $co->param('selected_databases_numbers');
}
$co->append(-name => 'selected_databases_numbers', -value => [@selected_DATABASES_numbers]);
$DB::CurrentDatabase = $showed_DATABASES{$default_DATABASE} unless $DB::CurrentDatabase;

my @CurrentDatabases = (@Several_Posted_Databases)? @Several_Posted_Databases
                     : (@Several_Search_Databases)? @Several_Search_Databases
                     : ($DB::CurrentDatabase);

# Keep last Search_param to revok it if CurrentDatabase is not the LastDatabase
# if ($DB::CurrentDatabase ne $co->param('keep_LastDatabase')) {
  # $Search_Argument = $co->param('keep_Search_param') if $co->param('keep_Search_param');
#}
# Declare clickedMenu; Initial hidden fields are sticky! so we have to re-write them this way:
$co->param(-name=> 'clickedMenu', -value=>"");
print $co->hidden('clickedMenu')."\n";
$co->param(-name=>'keep_Search_param', -value=>"$Search_Argument");
print $co->hidden('keep_Search_param')."\n";
$co->param(-name=> 'keep_LastDatabase', -value=>"$DB::CurrentDatabase");
print $co->hidden('keep_LastDatabase')."\n";

# We make sure some other hidden fields are kept over
if ($co->param('last remote maps on') and !$co->param('refresh maps')) {
  my $lastRemoteMapsParameter = $co->param(-name=>'last remote maps on');
  $co->delete('last remote maps on');
  $co->param(-name=>'last remote maps on', -values=>[$lastRemoteMapsParameter]);
  print $co->hidden('last remote maps on');
}

# some example values from the database:
my ($default_example_ID_upper, $default_example_AC_upper, $default_example_spot_upper);
if ($dynamicExamples) {
  unless ($co->param('default_example_ID')) {
    my $firstCommandInScript = "SELECT ID, AC FROM ENTRY LIMIT 1";
    my ($ID_example, $AC_example) = EXECUTE_FAST_COMMAND("$firstCommandInScript");
    if (!$ID_example and $SERVER::core_selector) {
      print "\n<br><br><strong class='red'>Error: Cannot connect to the core schema! ".
            "Please, try a valid login or verify the connection parameters.\n</strong></body></html>\n";
      exit 0;
    }
    my ($spot_example) = EXECUTE_FAST_COMMAND("SELECT spotID FROM Spot LIMIT 1");
    print $co->hidden(-name=>'default_example_ID', -value=>"$ID_example")."\n";
    print $co->hidden(-name=>'default_example_AC', -value=>"$AC_example")."\n";
    print $co->hidden(-name=>'default_example_spot', -value=>"$spot_example")."\n";
  }
  $default_example_ID_upper = uc($co->param('default_example_ID'));
  $default_example_AC_upper = uc($co->param('default_example_AC'));
  $default_example_spot_upper = uc($co->param('default_example_spot'));
     $default_example_ID_upper = 'TCTP_HUMAN' unless $default_example_ID_upper;
     $default_example_AC_upper = 'P12345' unless $default_example_AC_upper;
     $default_example_spot_upper = '2D-0017PD' unless $default_example_AC_upper;
  print $co->hidden(-name=>'default_example_ID', -value=>"$default_example_ID_upper")."\n";
  print $co->hidden(-name=>'default_example_AC', -value=>"$default_example_AC_upper")."\n";
  print $co->hidden(-name=>'default_example_spot', -value=>"$default_example_spot_upper")."\n";
} else {
  $default_example_ID_upper = 'APOA1_HUMAN';
  $default_example_AC_upper = 'P04406';
  $default_example_spot_upper = '2D-0017PD';
}
my ($default_example_ID_lower, $default_example_AC_lower) = (lc($default_example_ID_upper), lc($default_example_AC_upper));
(my $default_example_ID_upper_trunc = $default_example_ID_upper) =~ s/^(\w{3}).+$/$1/;
my $default_example_ID_lower_trunc = lc($default_example_ID_upper_trunc);
(my $default_example_spot_upper_trunc = $default_example_spot_upper) =~ s/^(?:.+)?(.{4})$/$1/;




print $co->start_table."\n";

print $co->start_Tr."\n";

print $co->start_td({-width=>'9%', -valign=>'top'})."\n";


# Write Menu
#===========#

# We're using onMouseOver instead of OnCLick to avoid calling the input with a keyboard return (reserved for the submission)

my $menuTitleMessage = ($SERVER::core_selector)? $Search_Menu{MainUpdate} : 'Choose one of the search methods below!';

my $homeMenuText = "2D-PAGE TOP".(($SERVER::ExPASy and $main::home_url)? '&nbsp;(search engine)': '');
my $fixedMenu = $co->a({-href=>"$script_name",-class=>"MenuTitle",
                        -onMouseOver=>"window.status=\"$menuTitleMessage\";return true",
			-style=>$link_style
                       },"$homeMenuText&nbsp;").$co->p;
#my $fixedMenu = $co->a({-href=>"$script_name",-class=>"MenuTitle",
#                        -onMouseOver=>"window.status=\"$menuTitleMessage\";return true"
#                       },"$homeMenuText&nbsp;").$co->hr;

if ($SERVER::core_selector) {
  $Search_Menu_icons{MainUpdate} =~ s/\.(gif|jpg)$/_on.$1/ unless $Search_Argument;
  my $updateButtom = ($menu_graphic_mode)?
    $co->input({-type=>"image", -src=>"$Search_Menu_icons{MainUpdate}", 
                -alt=>"* $Search_Menu{MainUpdate}",
                -onMouseUp=>"javascript:ClickMenu('MainUpdate','$script_name')",
                -onMouseOver=>"window.status='$Search_Menu{MainUpdate}';return true"
               })."\n"
  :
    $co->br.$co->a({-href=>"$script_name",-class=>"$Search_Menu_TextStyle{MainUpdate}",
                    -onMouseOver=>"window.status=\"$menuTitleMessage\";return true"
                   }, '[Main updates]')."\n";

  $fixedMenu =  $co->a({-href=>"$script_name",-class=>"MenuTitle",
                        -onMouseOver=>"window.status=\"$menuTitleMessage\";return true"
                       },"$updateButtom\&nbsp;").$co->hr."\n";
  $fixedMenu = $updateButtom.$co->hr."\n";
}

my ($mapExpInfoMenu, $mapExpInfoMenuNoSpace) = ("experimental info", "experimental&nbsp;info");
my ($mapProteinListMenu, $mapProteinListMenuNoSpace) = ("protein list", "protein&nbsp;list");
my $graphicalViewerName = ($menu_graphic_mode == 1)? 'graphical viewer' : 'graphical interface';


$fixedMenu   .=          ($menu_graphic_mode == 0)?

                            $co->strong($co->span({-class=>"MenuTitle", -style=>$font_style}, 'Search&nbsp;by&nbsp;')).$co->br."\n".
                            $co->a({-href=>"javascript:ClickMenu('AC','$script_name')" , -class=>"$Search_Menu_TextStyle{AC}",
                                    -onMouseOver=>"window.status='$Search_Menu{AC}';return true", 
				    -style=>$link_style}, 
                                  "CIPRO&nbsp;ID").$triangle_image{AC}.$co->br({-class=>'breakCategory1'})."\n".
#                            $co->a({-href=>"javascript:ClickMenu('DE','$script_name')" , -class=>"$Search_Menu_TextStyle{DE}",
#                                    -onMouseOver=>"window.status='$Search_Menu{DE}';return true",
#				    -style=>$link_style},
#                                  "description").$triangle_image{DE}.$co->br({-class=>'breakCategory1'})."\n".
#                            $co->a({-href=>"javascript:ClickMenu('Author','$script_name')" , -class=>"$Search_Menu_TextStyle{Author}",
#                                    -onMouseOver=>"window.status='$Search_Menu{Author}';return true"},
#                                  "[author's&nbsp;name]").$triangle_image{Author}.$co->br({-class=>'breakCategory1'})."\n".
                            $co->a({-href=>"javascript:ClickMenu('Spot','$script_name')" , -class=>"$Search_Menu_TextStyle{Spot}",
                                    -onMouseOver=>"window.status='$Search_Menu{Spot}';return true",
				    -style=>$link_style},
                                  "spot&nbsp;ID").$triangle_image{Spot}.$co->br({-class=>'breakCategory1'})."\n".
                            $co->a({-href=>"javascript:ClickMenu('Ident','$script_name')" , -class=>"$Search_Menu_TextStyle{Ident}",
                                    -onMouseOver=>"window.status='$Search_Menu{Ident}';return true",
				    -style=>$link_style},
                                  "identification&nbsp;methods").$triangle_image{Ident}.$co->br({-class=>'breakCategory1'})."\n".
                            $co->a({-href=>"javascript:ClickMenu('pI','$script_name')" , -class=>"$Search_Menu_TextStyle{pI}",
                                    -onMouseOver=>"window.status='$Search_Menu{pI}';return true",
				    -style=>$link_style},
                                  "pI&nbsp;/&nbsp;Mw").$triangle_image{pI}.$co->br({-class=>'breakCategory1'})."\n".
                            $co->a({-href=>"javascript:ClickMenu('Combined','$script_name')" , -class=>"$Search_Menu_TextStyle{Combined}",
                                    -onMouseOver=>"window.status='$Search_Menu{Combined}';return true",
				    -style=>$link_style},
                                  "combined&nbsp;fields").$triangle_image{Combined}.$co->br({-class=>'breakCategory1'})."\n".
                            $co->p.$co->strong($co->span({-class=>"MenuTitle", -style=>$font_style}, 'Lists')).$co->br."\n".
                            $co->a({-href=>"javascript:ClickMenu('MapExpInfo','$script_name')" , -class=>"$Search_Menu_TextStyle{MapExpInfo}",
                                    -onMouseOver=>"window.status='$Search_Menu{MapExpInfo}';return true",
				    -style=>$link_style},
                                  "Experimental Information").$triangle_image{MapExpInfo}.$co->br({-class=>'breakCategory1'})."\n".
                            $co->a({-href=>"javascript:ClickMenu('Map','$script_name')" , -class=>"$Search_Menu_TextStyle{Map}",
                                    -onMouseOver=>"window.status='$Search_Menu{Map}';return true",
				    -style=>$link_style},
                                  "Protein List").$triangle_image{Map}.$co->br({-class=>'breakCategory1'})."\n".
                           ($SERVER::onlyInterfaces? '' :
#                             $co->a({-href=>"$map_viewer$viewer_DB_argument", -class=>'MenuLink',
#                                      -onMouseOver=>"window.status='NAVIGATE graphically through the MAPS';return true",
#				      -style=>$link_style},
#                                "2D-Gel Image")."\n".
#                            $co->br({-class=>'breakCategory1'})
			    '')."\n"

                         :

                         # Submit Button Alternative, maybe desactivate as not always NN compliant #
                         ($menu_graphic_mode == 2)?

                         $co->strong($co->span({-class=>"MenuTitle"}, 'Search&nbsp;by&nbsp;')).$co->br."\n".

                         $co->button({class=>"$fixedMenu{AC}", -value=>'accession number',
                                      -onClick=>"javascript:ClickMenu('AC','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{AC}';return true"}).
                         " ".$triangle_image{"AC"}.$co->br."\n".
                         $co->button({class=>"$fixedMenu{DE}", -value=>'DE, ID, KW or gene',
                                      -onClick=>"javascript:ClickMenu('DE','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{DE}';return true"}).
                         " ".$triangle_image{"DE"}.$co->br."\n".
                         $co->button({ class=>"$fixedMenu{Author}", -value=>'author',
                                      -onClick=>"javascript:ClickMenu('Author','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{Author}';return true"}).
                         " ".$triangle_image{"Author"}.$co->br."\n".
                         $co->button({ class=>"$fixedMenu{Spot}", -value=>'spot ID/serial#',
                                      -onClick=>"javascript:ClickMenu('Spot','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{Spot}';return true"}).
                         " ".$triangle_image{"Spot"}.$co->br."\n".
                         $co->button({ class=>"$fixedMenu{Ident}", -value=>'identification methods',
                                      -onClick=>"javascript:ClickMenu('Ident','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{Ident}';return true"}).
                         " ".$triangle_image{"Ident"}.$co->br."\n".
                         $co->button({ class=>"$fixedMenu{pI}", -value=>'pI/Mw range',
                                      -onClick=>"javascript:ClickMenu('pI','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{pI}';return true"}).
                         " ".$triangle_image{"pI"}.$co->br."\n".
                         $co->button({ class=>"$fixedMenu{Combined}", -value=>'combined fields',
                                      -onClick=>"javascript:ClickMenu('Combined','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{Combined}';return true"}).
                         " ".$triangle_image{"Combined"}.$co->br."\n".
                         $co->span({-class=>"MenuTitle"}, 'Maps&nbsp;').$co->br."\n".
                         $co->button({ class=>"$fixedMenu{MapExpInfo}", -value=>"$mapExpInfoMenu",
                                      -onClick=>"javascript:ClickMenu('MapExpInfo','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{MapExpInfo}';return true"}).
                         " ".$triangle_image{"MapExpInfo"}.$co->br."\n".
                         $co->button({ class=>"$fixedMenu{Map}", -value=>"$mapProteinListMenu",
                                      -onClick=>"javascript:ClickMenu('Map','$script_name')",
                                      -onMouseOver=>"window.status='$Search_Menu{Map}';return true"}).
                         " ".$triangle_image{"Map"}.$co->br."\n".
                        ($SERVER::onlyInterfaces? '' :
                           $co->button({ class=>'FixedMenu', -value=>"$graphicalViewerName",
                                        -onClick=>"window.location.href='$map_viewer$viewer_DB_argument'", 
                                        -onMouseOver=>"window.status='NAVIGATE graphically through the MAPS';return true"})." ".$triangle_void.$co->br).
                           $co->br."\n"

                         :

                         # Any other modes (1 / 3)

                         ( ($menu_graphic_mode == 3)?
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu01.gif", -alt=>""}).$co->br."\n".
                            $co->input({-type=>"image", -src=>"$icons/2d_menu05.gif", -alt=>'Top Page',
                                        -onMouseOver=>"window.status='Top Page';return true"}).$co->br."\n"
                            :
                            "\n".

                            $co->strong($co->span({-class=>"MenuTitle"}, 'Search&nbsp;by&nbsp;')).$co->br."\n"
                         ).


                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu08.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{AC}",
                                     -alt=>"* $Search_Menu{AC}",
                                     -onMouseUp=>"javascript:ClickMenu('AC','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{AC}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu17.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{DE}", 
                                     -alt=>"* $Search_Menu{DE}",
                                     -onMouseUp=>"javascript:ClickMenu('DE','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{DE}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu22.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{Author}", 
                                     -alt=>"* $Search_Menu{Author}",
                                     -onMouseUp=>"javascript:ClickMenu('Author','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{Author}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu27.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{Spot}", 
                                     -alt=>"* $Search_Menu{Spot}",
                                     -onMouseUp=>"javascript:ClickMenu('Spot','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{Spot}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu32.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{Ident}", 
                                     -alt=>"* $Search_Menu{Ident}",
                                     -onMouseUp=>"javascript:ClickMenu('Ident','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{Ident}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu32.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{pI}", 
                                     -alt=>"* $Search_Menu{pI}",
                                     -onMouseUp=>"javascript:ClickMenu('pI','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{pI}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu37.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{Combined}", 
                                     -alt=>"* $Search_Menu{Combined}",
                                     -onMouseUp=>"javascript:ClickMenu('Combined','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{Combined}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu52.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).
                         $co->hr.
                         $co->span({-class=>"MenuTitle"}, 'Maps&nbsp;')."\n".
                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{MapExpInfo}",
                                     -alt=>"* $Search_Menu{MapExpInfo}",
                                     -onMouseUp=>"javascript:ClickMenu('MapExpInfo','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{MapExpInfo}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu52.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).

                         $co->p({},
                         $co->input({-type=>"image", -src=>"$Search_Menu_icons{Map}",
                                     -alt=>"* $Search_Menu{Map}",
                                     -onMouseUp=>"javascript:ClickMenu('Map','$script_name')",
                                     -onMouseOver=>"window.status='$Search_Menu{Map}';return true"
                                     }))."\n".

                         ( ($menu_graphic_mode == 3)?
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu72.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         ).


                         ( ($SERVER::onlyInterfaces)? '' :
                         ( ($menu_graphic_mode == 3)?
                            $co->a({-href=>"$map_viewer$viewer_DB_argument"},
                                     $co->img({-border=>'0', -src=>"$icons/2d_menu90.gif", -alt=>'Navigate the Maps',
                                               -onMouseOver=>"window.status='NAVIGATE graphically through the MAPS';return true"})).
                            $co->br."\n"
                            : ($menu_graphic_mode == 1)?
                            $co->p({},
                            $co->a({-href=>"$map_viewer$viewer_DB_argument"},
                                     $co->img({-border=>'0', -src=>"$icons/2d_menu_graphical_viewer.gif", -alt=>'Graphical Viewer',
                                               -onMouseOver=>"window.status='NAVIGATE graphically through the MAPS';return true"})).
                            $co->br({-class=>'breakCategory1'}))."\n"
                            : 
                            $co->hr.
                            $co->a({-href=>"$map_viewer$viewer_DB_argument", 
                                    -onMouseOver=>"window.status='NAVIGATE graphically through the MAPS';return true"},
                                     $co->h3({class=>"MenuLink"}, "Navigate the Maps"))
                         )
                         ).

                         ( ($menu_graphic_mode == 3)? 
                            $co->img  ({-border=>'0', -src=>"$icons/2d_menu97.gif", -alt=>""}).$co->br."\n" :
                            "\n"
                         );


=pod
if (($main::multi_DBs or $SERVER::remoteInterfaceDefined) and !$SERVER::core_selector) {

  $fixedMenu.= $co->hr.$co->br."\n";
  $fixedMenu.= $co->start_table;
  $fixedMenu.= $co->start_Tr;
  $fixedMenu.= $co->td({-bgcolor=>'#99FFFF'}, '&nbsp;')."\n";
  $fixedMenu.= $co->td({-bgcolor=>"$bkgrd"}, '&nbsp;&nbsp;')."\n";
  $fixedMenu.= $co->start_td()."\n";

  if ($main::multi_DBs and !$SERVER::onlyInterfaces) {
    my $database_list_header = ($menu_graphic_mode)?
      $co->br.$co->img({-src=>"$icons/2d_menu_DB.gif", -align=>'top', -alt=>"Databases"}).$co->br."\n" :
      $co->br.$co->span({class=>'MenuTitle'}, 'Databases:').$co->br."\n";
    $fixedMenu.= $database_list_header;
    my ($list_size, @showed_DATABASES_visible);
    if (scalar (@showed_DATABASES) > 5) {
      $list_size = 5;
      @showed_DATABASES_visible = ('All', @showed_DATABASES);
    } else {
      $list_size = scalar (@showed_DATABASES) + 1;
      @showed_DATABASES_visible = (@showed_DATABASES, 'All');
    }
    $fixedMenu.= $co->scrolling_list 
          ( -name=> 'selected_databases',
            -values=>[@showed_DATABASES_visible],
            -defaults=>[$default_DATABASE],
            -multiple=>'true',
            -size=>$list_size,
            -onChange=>"DeleteCookie('$cookie_2d_LastDatabase'), SetCookie('$cookie_2d_LastDatabase', GetElementValue(document.$formName.selected_databases), 30)"
          )."\n";
    $fixedMenu.= "\n".$co->br.$co->br.$co->em($co->span({class=>'Estomped'},'use \'Ctrl\' to select several<br>databases, or choose \'All\''));
  }
  if ($SERVER::remoteInterfaceDefined) {
    $fixedMenu.= $co->br.$co->br if $main::multi_DBs and !$SERVER::onlyInterfaces;
    $fixedMenu.= $co->strong('Query Remote Interfaces:').$co->br;
    my @remoteInterfaces = sort keys %{$SERVER::remoteInterface};
    my $defaultInterface = ($SERVER::remoteInterfaceDefaultActivated)? $SERVER::remoteInterfaceDefaultActivated
                         : ($SERVER::onlyInterfaces)? $remoteInterfaces[0] : '';
    my @remoteInterfacesLinkLabels;
    foreach my $remoteInterface (@remoteInterfaces) {
      my $href = (${$SERVER::remoteInterface}{$remoteInterface}->{URI})?
         ${$SERVER::remoteInterface}{$remoteInterface}->{URI} :
         ${$SERVER::remoteInterface}{$remoteInterface}->{URL};
      push @remoteInterfacesLinkLabels, $co->a({href=>"$href", -target=>"_blank"}, $remoteInterface);
    }
    if (scalar (@remoteInterfaces) > 1) {
      unshift @remoteInterfaces, ($co->strong("$allInterfacesText"));
      unshift @remoteInterfacesLinkLabels, "$allInterfacesText";
    }
    $fixedMenu.= $co->checkbox_group(-name=>'selected_remoteinterfaces', -values=>[@remoteInterfaces],
                                     -columns=>2, -rows=>scalar @remoteInterfaces, -rowheaders=>[@remoteInterfacesLinkLabels],
                                     -nolabels=>'true', -default=>[$defaultInterface])."\n";
    unless ($SERVER::onlyInterfaces) {
      $fixedMenu.= $co->hr.$co->checkbox (-name=>'selected_remoteinterfaces_and_noLocalDBs', -label=>" Exclude local DBs", -checked=>0)."\n";
      $fixedMenu.= "\n".$co->br.$co->em($co->span({class=>'Estomped'},'has only effect if a remote<br>interface is selected'))."\n";
    }
    print $co->br."\n";
  }

  $fixedMenu.= $co->end_td."\n";
  $fixedMenu.= $co->end_Tr."\n";
  $fixedMenu.= $co->end_table."\n";
}
=cut
$fixedMenu = $co->td({-nowrap}, $fixedMenu);

print $co->table($co->caption($co->strong('') ), Tr({-align=>'left'}, ["\n".$fixedMenu]));

print $co->end_td."\n";

print $co->td('&nbsp;')."\n";
#print $co->td({-bgcolor=>'#99FFFF'}, '&nbsp;')."\n";
print $co->td({-bgcolor=>'#000000', -height => 800}, ' ')."\n";
print $co->td('&nbsp;')."\n";

print $co->start_td({-width=> '91%', -valign=>'top'})."\n";


#============================================================================================#
#============================================================================================#


#==============================#
# Execute a Command            #
#==============================#

# my $Execute_query = 1 if 
#  ($co->param('Execute query') or $co->param('hidden_reference') =~ /^AC|DE|Author|Spot|Ident|pI|Combined|Map$/);

my $resultStartDatabase = "\n\n  <!-- start database:\"$DB::CurrentDatabaseName\" interface:\"$name2d\" url:\"$SERVER::interfaceWebAddress\" -->\n\n";
my $resultEndDatabase = "\n\n  <!-- end database:\"$DB::CurrentDatabaseName\" interface:\"$name2d\" url:\"$SERVER::interfaceWebAddress\" -->\n\n";


if (($co->param('Execute query') or $co->param('hidden_reference')) and !$new_Search_Argument)  {

  $co->param(-name=> 'Execute query', -values=> 1) unless $co->param('Execute query');

  $command = $co->param('pgsql command');
  $hidden_reference = $co->param('hidden_reference');



  # Search by...
  #============#

  if ($hidden_reference) {

    my ($search_message, $search_argument_not_ok, $search_argument_not_found, $hide_no_result_message);

    print $resultStart;

    print $resultStartDatabase unless $main::SeveralResponse;

    # If we have already collected several search responses (from multi databases)

    if ($main::SeveralResponse) {

      print $main::SeveralResponse;

    }

    # Search by AC

    elsif ($hidden_reference eq 'AC') {


      my $sample_preparation_link = ($sample_origin_URL)?
        $co->p({class=>"center"},
                $co->a({href=>"$sample_origin_URL", target=>"_blank"}, "Sample Preparation and Post-separation Analysis")."\n"
#                .$co->br." (the sample data is in raw format for the moment, until the 'Proteomics Standardisation Project' is finalised)".$co->br."\n"
              ).$co->br.$co->hr : '';

      my $execute_value = $co->param('Execute query');

      if (($execute_value eq 'List all ACs' and !$co->param('search_AC')) or $co->param('search_AC') =~ /^(all|\*)$/i) {
        my $orderBy = ($co->param('AC_List_output') eq 'Protein ID')? 'Entry.ID' : 'Entry.AC';
        if ($co->param('hidden_reference_raw_entry')) {
          $command = " SELECT Entry.AC FROM ENTRY WHERE $SERVER::showFlagSQLEntry ORDER BY $orderBy";
          my (@ACs_text) = EXECUTE_FAST_COMMAND($command);
          for (my $ii = 0; $ii < scalar @ACs_text; $ii++) {
            $text_output.= "AC   ".$ACs_text[$ii]."\n";
          }
          $text_output =~ s/\n$//s;
          print $resultTextStart.$text_output.$resultTextEnd;
        }
        else {
          my $select_map_name_pattern = '';
          my $map_fullName = '';
          if ($co->param('map_name_pattern') =~ /\S/) {
            if ($co->param('map_name_pattern') !~ /^\s*\w+\s*$/) {
              &SEARCH_ARGUMENT_NOT_OK($co->param('map_name_pattern'), 'bad map pattern'); goto AC_END_SECTION
            }
            my $map_name_pattern = $co->param('map_name_pattern');
            $map_name_pattern =~ s/\s//g;
            $map_fullName = " || '<br>{&nbsp;' || Gel.fullName || '&nbsp;}'";
            $command = " SELECT DISTINCT Gel.gelID from Gel WHERE Gel.shortNAme ~* '$map_name_pattern' OR Gel.fullName ~* '$map_name_pattern'";
            my @matching_maps = &EXECUTE_FAST_COMMAND("$command");
            if (scalar @matching_maps) {
              foreach my $matching_map (@matching_maps) {
                next unless $matching_map =~ /^\d+$/;
                $select_map_name_pattern .= "Gel.gelID = $matching_map OR "
              }
              $select_map_name_pattern = "($select_map_name_pattern) AND " if ($select_map_name_pattern =~ s/OR\s+$//i);
            }
            else {
              &SEARCH_ARGUMENT_NOT_OK($co->param('map_name_pattern'), 'bad map pattern');  goto AC_END_SECTION
            }
          }
#          print $sample_preparation_link;
          $command = " SELECT '  __AC__'|| Entry.AC||'__AC__' as accession_nbsp_number, Entry.ID || '&nbsp;' as _nbsp__upper_I__upper_D, ".
                     "Entry.description, '&nbsp;' || Gel.shortName$map_fullName as _upper_Map".
                     " FROM Entry, EntryGelImage, Gel".
                     " WHERE $select_map_name_pattern $SERVER::showFlagSQLEntry AND Entry.AC = EntryGelImage.AC AND EntryGelImage.gelID = Gel.gelID".
                     " ORDER BY $orderBy, Gel.shortName";
          print $co->p.$co->br."\n".$co->h3({class=>'H3Font center'}, "List of accession numbers on $title")."\n";
          &EXECUTE_FORMATTED_COMMAND ({command=>$command, table_width=>'60%', group_rows=>1, group_rows_extra=>[2,3], table_alignment_general=>'center',
                                       table_alignment_text_class=>'left', no_table_caption=>2,
                                       show_number_rows_selected=>'Total number of entries: '});
        }
        goto AC_END_SECTION;
      }

      my ($search_AC, $search_AC_copy);

      $search_AC = $search_AC_copy = uc($co->param('search_AC'));
      if($search_AC =~ m/CIPRO[0-9]+\./) {
        $search_AC =~ s/CIPRO/CIPRO_/;
	$search_AC =~ s/\./_/g;
      } elsif ($search_AC =~ m/KH\./) {
        $search_AC =~ s/\./_/g;
      }
      $search_AC =~ s/^\s*(.+\S)\s*$/$1/;
      $search_AC =~ s/\s+/,/g;
      $search_argument_not_ok = 1 if !$search_AC;


      my (@search_AC, @search_AC_copy, %search_AC);


      # for a list of ACs, they should be joined by ',', otherwise just one element
      @search_AC_copy = split (',', $search_AC);

      grep {push @search_AC, $_ unless $search_AC{$_}; $search_AC{$_} = 1} @search_AC_copy;
      undef(%search_AC);

      my $sub_search_message = "matching: ";
      my $entryMessageNature = ($search_AC[1])? 'entries' : 'entry';

      if ($co->param('AC_pattern') eq 'partial_AC' and !$search_argument_not_ok) {
        $command = " SELECT Entry.AC FROM Entry WHERE (";
        foreach (@search_AC) {
        my $arg = quotemeta(quotemeta($_));
          $command .= "Entry.AC ~* '$arg' OR Entry.ID ~* '$arg' OR ";
        }
        $command =~ s/\s*OR\s*$//i;
        $command.= ") AND $SERVER::showFlagSQLEntry ";
        $command.= "UNION ( SELECT SecondaryAC.AC FROM SecondaryAC, entry WHERE (";
        foreach (@search_AC) {
          my $arg = quotemeta(quotemeta($_));
          $command .= "SecondaryAC.secondaryAC ~* '$arg' OR ";
        }	
        $command =~ s/\s*OR\s*$//i;
        $command.= ") AND $SERVER::showFlagSQLEntry )";
        @search_AC = EXECUTE_FAST_COMMAND($command);
        $search_argument_not_found = 1 unless @search_AC;
        $sub_search_message = "matching partially: ";
        $entryMessageNature = 'entries';
      }

#      print $sample_preparation_link if $sample_origin_URL && !$search_argument_not_ok;   
      $search_message = "Searching in '$name2d_database_name' for $entryMessageNature";
      print IntoCenteredColoredCell($co->span({style=>$font_style." font-weight: bold;"}, $search_message).'&nbsp;'.$co->span({style=>$font_style}, $sub_search_message).$co->span({-class=>'red', style=>$font_style." font-weight: bold;"},uc($co->param('search_AC'))),'','left')."\n";
#      print $co->br.$co->br.$co->br;
      print $co->br;

      if ($search_argument_not_ok) {
        &SEARCH_ARGUMENT_NOT_OK($search_AC_copy, 'bad AC');
      }
      elsif ($search_argument_not_found) {
        &SEARCH_ARGUMENT_NOT_OK($search_AC_copy, 'no AC pattern');
      }
      else {

        if ($co->param('AC_output') eq 'RAW' or $co->param('hidden_reference_raw_entry')) {
          undef($text_output);
          my $privateKeywordArgument = ($SERVER::hidePrivate and $main::privateKeyword)? $main::privateKeyword : '';
          foreach my $search_AC (@search_AC) {
            my @text_output = EXECUTE_FAST_COMMAND("SELECT make2db_ascii_entry('".$search_AC."', '78', '".$privateKeywordArgument."')");
            $text_output .= $text_output[0]."\n" if $text_output[0] =~ /\n.+/;
          }
          $text_output =~ s/\n+$//s;
          $text_output = _insert_entry_copyright_text_($text_output);
          print $resultTextStart.$text_output.$resultTextEnd;

        }
        elsif (scalar @search_AC > 5) { # we limit any formatted display to 5 entries
          $command = " SELECT '__AC__'||Entry.AC||'__AC__' as accession_number, Entry.ID as _upper_I_upper_D, ".
                     "Entry.description, Entry.geneNames as Gene_upper_Name FROM Entry WHERE";
          foreach my $search_AC (@search_AC) {
            $command .= " Entry.AC = '$search_AC' OR";
          }
          $command =~ s/\s*OR\s*$//i;
          $command .= " ORDER BY 1";
          &EXECUTE_FORMATTED_COMMAND({command=>$command});
        }
        elsif ($co->param('AC_output') eq 'TEXT' or $co->param('hidden_reference_text_entry')) {
          print (&PRINT_ENTRY($search_AC,\@search_AC));
        }
        else {
          my $spot_id = '';
          $spot_id = $co->param('search_SPOT') if $co->param('search_SPOT');
          $spot_id = $args{map}.':'.$spot_id if $args{map};
          grep {s/\\//g} @search_AC;
          my $expand_msms = ($args{expand} =~ /\bms\/?ms\b/)? 1 : undef;
          my $expand = ((exists($args{expand}) and !$args{expand}) or $expand_msms)? 1 : undef;

          print (&PRINT_NICE_ENTRY({entry_ac=>[@search_AC], entry_spot=>$spot_id, expand=>$expand, expand_msms=>$expand_msms}));
        }

      }

      AC_END_SECTION:
    }



    # Search by Description

    elsif ($hidden_reference eq 'DE') {

      my ($search_des, $search_de_limited, $meta_search_des, @meta_search_des);

      $search_des = $co->param('search_description');
      $search_des =~ s/^\s*(.+\S)\s*$/$1/;
      $search_des =~ s/\s{2,}/ /;
      $search_de_limited = $co->param('search_description_limited');

      $search_argument_not_ok = 1 if $search_des !~ /\S{2}/;

      @meta_search_des = split " ", $search_des;
      map { $_ = quotemeta(quotemeta($_)) } @meta_search_des;
      $meta_search_des = join "|", @meta_search_des;

      my ($sub_command, $sub_select, $sub_command_external, $sub_select_external, $sub_search_message);
      if ($search_de_limited eq 'ID') {
        $sub_command = " Entry.ID ~* '".$meta_search_des."'";
        $sub_select = "";
        $sub_command_external = " ExternalMainXrefData.uniProtID ~* '".$meta_search_des."' ";
        $sub_select_external = "";
        $sub_search_message = "entry name (ID).";
      }
      elsif ($search_de_limited eq 'DE') {
        $sub_command = " Entry.description ~* '".$meta_search_des."'";
        $sub_select = "Entry.description,";
        $sub_command_external = " ExternalMainXrefData.uniProtDescription ~* '".$meta_search_des."' ";
        $sub_select_external = " ExternalMainXrefData.uniProtDescription as _upper_Uni_upper_Prot_upper_K_upper_B_description,";
        $sub_search_message = "description (DE).";
      }
      elsif ($search_de_limited eq 'GN') {
        $sub_command = " Entry.geneNames ~* '".$meta_search_des."'";
        $sub_select = "'\{'||Entry.geneNames||'\}' as GENES,";
        $sub_command_external = " ExternalMainXrefData.uniProtGeneNames ~* '".$meta_search_des."' ";
        $sub_select_external = " '\{'||ExternalMainXrefData.uniProtGeneNames||'\}' as _upper_Uni_upper_Prot_upper_K_upper_B_genes,";
        $sub_search_message = "gene names (GN).";      
      }
      elsif ($search_de_limited eq 'KW') {
        $sub_command = " Entry.keywords ~* '".$meta_search_des."'";
        $sub_select = "Entry.keywords as Keywords,";
        $sub_command_external = " ExternalMainXrefData.uniProtCategoryKeywords ~* '".$meta_search_des."' ";
        $sub_select_external = " ExternalMainXrefData.uniProtCategoryKeywords as _upper_Uni_upper_Prot_upper_K_upper_B_keywords,";
        $sub_search_message = "UniProtKB/Swiss-Prot keywords (KW).";      
      }
      else {
        $sub_command = " Entry.ID ~* '".$meta_search_des."' or Entry.description ~* '".$meta_search_des."'".
                       " or Entry.geneNames ~* '".$meta_search_des."' or Entry.keywords ~* '".$meta_search_des."'";
        $sub_select = "Entry.description, '\{'||Entry.geneNames||'\}' as GENES, Entry.keywords as Keywords,";
        $sub_command_external = " ExternalMainXrefData.uniProtID ~* '".$meta_search_des."' or ExternalMainXrefData.uniProtDescription ~* '".$meta_search_des."'".
                                " or ExternalMainXrefData.uniProtGeneNames ~* '".$meta_search_des.
                                "' or ExternalMainXrefData.uniProtCategoryKeywords ~* '".$meta_search_des."'";
        $sub_select_external = " ExternalMainXrefData.uniProtDescription as _upper_Uni_upper_Prot_upper_K_upper_B_description,".
                               " '\{'||ExternalMainXrefData.uniProtGeneNames||'\}' as _upper_Uni_upper_Prot_upper_K_upper_B_genes,".
                               " ExternalMainXrefData.uniProtCategoryKeywords as _upper_Uni_upper_Prot_upper_K_upper_B_keywords,";
        $sub_search_message = "description (DE), entry name (ID), gene names (GN) or UniProtKB/Swiss-Prot keywords (KW).";
      }

      $search_message = "Searching in '$name2d_database_name' for entries matching ".(($search_des =~ /\s/)? "any of the keywords:" : "the keyword:");
      $search_message.= $co->br."'".$co->span({-class=>'red'}, $search_des)."'".$co->br;
      $search_message.= "in their ".$sub_search_message;
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";

      $command = " SELECT DISTINCT '__AC__'||Entry.AC||'__AC__' as accession_number, '__AC__'||Entry.ID||'__AC__' as _upper_I_upper_D,";
      $command.= " $sub_select Organism.organismSpecies as species";
      $command.= " FROM Entry, Organism ";
      $command.= " WHERE ($sub_command)";
      $command.= " AND Organism.organismID = Entry.organismID";
      $command.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries;
      my $order_by = ($co->param('order_by') eq 'Accession number')? 1 : ($co->param('order_by') eq 'Protein ID')? 2 : 'species';
      $command.= " ORDER BY $order_by";
      $hide_no_result_message = 1 if $co->param('include_external_data');
      $hide_no_result_message *=
      &EXECUTE_FORMATTED_COMMAND({command=>$command, hidden_reference=>$hidden_reference, hide_no_result_message=>$hide_no_result_message}) unless $search_argument_not_ok;

      if ($co->param('include_external_data')) {
        $command = " SELECT DISTINCT Entry.AC ";
        $command.= " FROM Entry";
        $command.= " WHERE ($sub_command)";
        $command.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries; 
        $command_external = " SELECT DISTINCT '__AC__'||Entry.AC||'__AC__' as accession_number__openP_internal_closeP_,";
        $command_external.= " ExternalMainXrefData.uniProtID as _upper_Uni_upper_Prot_upper_K_upper_B__upper_I_upper_D,";
        $command_external.= $sub_select_external. " Organism.organismSpecies as species";
        $command_external.= " FROM Entry, ExternalMainXrefData, Organism";
        $command_external.= " WHERE ($sub_command_external)";
        $command_external.= " AND Organism.organismID = Entry.organismID AND ExternalMainXrefData.AC = Entry.AC";
        $command_external.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries;   
        $command_external = $command_external." AND Entry.AC NOT IN (".$command.")";
        $command_external.= " ORDER BY $order_by";
        print $co->br;
        &EXECUTE_FORMATTED_COMMAND({caption=>'External UniProtKB data (additional matching):', command=>$command_external, hidden_reference=>$hidden_reference,
                                    hide_no_result_message=>$hide_no_result_message}) unless $search_argument_not_ok;    
      }

      &SEARCH_ARGUMENT_NOT_OK($search_des, 'bad description') if $search_argument_not_ok;
    }



    # Search by Authors

    elsif ($hidden_reference eq 'Author') {

      my ($search_author, $selected_author, $displayed_selected_author, $protein_id, $meta_selected_author, $meta_protein_id);

      $search_author = $co->param('search_author');
      $search_author =~ s/^\s*(.+\S)\s*$/$1/;
      $selected_author = $co->param('authors_list') if $co->param('authors_list') and $co->param('authors_list') !~  /^(?:all authors|\-+)/i;
      $selected_author = $1 if $search_author =~ /^(\S+)/ && !$selected_author;
      $displayed_selected_author = $selected_author;
     ($selected_author, $displayed_selected_author) = ('..','all authors') unless $selected_author;
      $protein_id = $co->param('search_author_byID');
      $protein_id =~ s/^\s*(.+\S)\s*$/$1/;
      $search_argument_not_ok = 1 if (($selected_author eq '.' or $selected_author !~ /\S{2}/) and  $protein_id !~ /\S{2}/);

      $search_message = "Searching in '$name2d_database_name' for authors containig: '";
      $search_message.= ($selected_author)? $co->span({-class=>'red'}, $displayed_selected_author) : "all authors";
      $search_message.= "' and protein names containing: '";
      $search_message.= ($protein_id)? $co->span({-class=>'red'}, $protein_id):"all names";
      $search_message.= "'.";  
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";

      $meta_selected_author = quotemeta(quotemeta($selected_author));
      $meta_protein_id = quotemeta(quotemeta($protein_id));

      $command = " SELECT DISTINCT __Author__.author, '__AC__'||ReferencedEntry.AC||'__AC__' as accession_number ,'__AC__'||Entry.ID||'__AC__' as _upper_I_upper_D";
      $command.= " FROM __Author__, ReferencedEntry, Entry";
      $command.= " WHERE __Author__.author ~* '".$selected_author."'";
      $command.= " AND ReferencedEntry.referenceID = __Author__.article AND Entry.AC = ReferencedEntry.AC";
      if ($protein_id) {
        $command.= " AND (Entry.AC ~* '".$protein_id."' OR Entry.ID ~* '".$protein_id."')";
      }
      my $order_by = ($co->param('order_by') eq 'Accession number')? 2 : 3;
      $command.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries;
      $command.= " ORDER BY 1, $order_by";

      # for authors we change the output format before it is printed in the SUB
      my @rows;
      if ($search_argument_not_ok) {
        &SEARCH_ARGUMENT_NOT_OK($search_author, 'bad author') 
      }
      else {
       (my $adaptedCommand = $command) =~ s/__Author__/Author/ig;
        @rows = &EXECUTE_FORMATTED_COMMAND({command=>$adaptedCommand, hidden_reference=>$hidden_reference,
                                            group_rows=>1, only_rows=>1, table_alignment_text_class=>'left'});
       ($adaptedCommand = $command) =~ s/__Author__/AuthorGroup/ig;
        unless ($rows[1]) {
          $command =~ s/Author\./AuthorGroup./ig;
          @rows = &EXECUTE_FORMATTED_COMMAND({command=>$adaptedCommand, hidden_reference=>$hidden_reference,
                                              group_rows=>1, only_rows=>1, table_alignment_text_class=>'left'});
        }
      }

      if ($rows[1]) {
        print $co->table({-cellspacing=>0, -align=>"center", -border=>$main::table_border, -width=>'70%'},
                caption({class=>'H3Font'}, $co->br.'List of authors:'.$co->br.$co->br), "\n",
                TR({-align=>'left', -valign=>'top'},\@rows)
               );
      }
      elsif (!$search_argument_not_ok) {
        &SEARCH_ARGUMENT_NOT_OK('','no result') if !$rows[1];
      }

    }



    # Search by Serial Number

    elsif ($hidden_reference eq 'Spot') {

      my ($map_to_search, $search_spot, $meta_search_spot);

      my $map_to_search_name = $co->param('choose_map');
      $map_to_search_name =~ s/^.+\s+\-\-\s+//;
      my $map_to_search_long_name = '';
      if ($map_to_search_name =~ s/\s*\{([^\}]*)\}\s*$//) {
        $map_to_search_long_name = " { ".$1." }";
      }
      if ($map_to_search_name !~ /All Maps/i) {
        $command = " SELECT gelID FROM Gel WHERE lower(shortName) = '".lc($map_to_search_name)."'";
        ($map_to_search) = EXECUTE_FAST_COMMAND($command);
        $map_to_search = defined unless $map_to_search;
      } else { undef($map_to_search) }

      $search_spot = $co->param('search_spot_number');
      $search_spot =~ s/^\s*(.*\S)\s*$/$1/;
      $search_argument_not_ok = 1 if $search_spot =~ /\s/
        or ($map_to_search_name =~ /All Maps/i and $search_spot !~ /\w{1}/) or (!$map_to_search and defined($map_to_search));
      $exact_search = 1 if length($search_spot) == 1;

      my $with_available_data = ($co->param('search_spot_ExpData') eq 'do_not_show_experimental_data')? 0 : 1;
      my ($spot_mapping_methods, $spot_pmf, $spot_msms, $spot_aacid, $experimental_methods);
      if ($with_available_data) {
        $spot_mapping_methods = ($co->param('search_spot_mapping_methods'))? 1 : 0;
        $spot_pmf = ($co->param('search_spot_pmf') or $args{data} eq 'pmf' or $args{data} eq 'all')? 1 : 0;
        $spot_msms = ($co->param('search_spot_msms') or $args{data} =~ /^(ms){1,2}$/ or $args{data} eq 'all')? 1 : 0;
        $spot_aacid = ($co->param('search_spot_aacid') or $args{data} eq 'aa' or $args{data} eq 'all')? 1 : 0;
        $with_available_data = ($spot_mapping_methods or $spot_pmf or $spot_msms or $spot_aacid);
        $experimental_methods = (($spot_pmf)? 'PMF, ' : '').
                                (($spot_msms)? 'Tandem MS, ' : '').
                                (($spot_aacid)? 'AA composition, ' : '').
                                (($spot_mapping_methods)? 'Mapping methods, ' : '');
        $experimental_methods = 'all' if $args{data} eq 'all';
        $experimental_methods =~ s/,\s*$//;
      }

      my $exact_message = ($exact_search)? ' (exactly)' : '';
      my $experimental_message = ($experimental_methods)? ' experimental data ('.$experimental_methods.')<br>' : '';
      $search_message = "Searching in '$name2d_database_name'$experimental_message for spot names matching$exact_message: '";
      $search_message.= ($map_to_search_name)? $co->span({-class=>'red'}, uc($map_to_search_name)).":"
                      : $co->em(" (All maps) ").":" unless $map_to_search;
      $search_message.= ($search_spot)? $co->span({-class=>'red'}, uc($search_spot)) : $co->em(" (All spots)");
      $search_message.= "'";
      if ($map_to_search) {
        my $map_to_search_name_href = $co->a({href=>"$main::script_name$main::script_name_qmark"."map=".lc($map_to_search_name).
                                                    "$DB::ArgumentCurrentDatabaseNameURI"}, uc($map_to_search_name).$map_to_search_long_name);
        my $chosen_map_species = ( $co->param('choose_map') =~ /^(.+)\s*\-\-\s+/)?  " <em>$1</em>" : '';
        $search_message.= $co->br."on the$chosen_map_species '".$co->span({-class=>'red'},$map_to_search_name_href)."' map";
      }
      $search_message.= ".";
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";

      $meta_search_spot = quotemeta(quotemeta($search_spot));

      my $cell_valign = ($with_available_data)? 'top' : 'middle';

      $command = " SELECT DISTINCT  '__SPOT__'||";
      $command.= "Gel.shortName||'\:'||";
      $command.= "SpotEntry.spotID||'__SPOT__' as spot__upper_i_upper_d,";
      $command.= " '__MAP__'||Gel.shortName||'__MAP__ {' || Gel.fullName || '}' as Map," if !$map_to_search;
      $command.= " '__AC__'||Entry.AC||'__AC__'||'<br>('||Entry.ID::TEXT||')' as  A_upper_C_openP__upper_I_upper_D_closeP_,";

      if ($with_available_data) {

        my $infoSpectra = "|| '[Spectra:' || SpotEntry.spotID || '&&' || Gel.shortname || '&&' || SpotEntry.AC || ";
        my $infoSpectraMSMS = $infoSpectra."'&&msms]' ";
        my $infoSpectraPMF = $infoSpectra."'&&pmf]' ";

        $command.= " '__EXPDATA__**' || common.make2db_entryspot_mapping_methods_string(SpotEntry.AC,SpotEntry.spotID,SpotEntry.gelID) || '**__EXPDATA__'".
                   " as Mapping__upper_Methods," if $spot_mapping_methods;
        $command.= " '__EXPDATA__**' || ViewSpotEntry.peptidemasses $infoSpectraPMF|| '**__EXPDATA__' as P_upper_M_upper_F," if $spot_pmf;
        $command.= " '__EXPDATA__**' || ViewSpotEntry.msms $infoSpectraMSMS|| '**__EXPDATA__' as Tandem__upper_M_upper_S," if $spot_msms;
        $command.= " '__EXPDATA__**' || ViewSpotEntry.peptidesequences || '**__EXPDATA__' as Identified_peptides," if $spot_msms;
        $command.= " '__EXPDATA__**' || ViewSpotEntry.aminoacidlist || '**__EXPDATA__' as Amino_acid_composition" if $spot_aacid;
        $command =~ s/,$//;
      }
      else {
        $command.= " Entry.description";
      }
      $command.= " FROM Entry, SpotEntry, Organism";
      $command.= " , ViewSpotEntry " if $with_available_data;
      $command.= " , Gel";
      $command.= " WHERE SpotEntry.AC = Entry.AC AND Organism.organismID = Entry.organismID";
      $command.= " AND SpotEntry.spotID ~* '".$meta_search_spot."'" if $search_spot and !$exact_search;
      $command.= " AND lower(SpotEntry.spotID) = '".lc($search_spot)."'" if $search_spot and $exact_search;
      $command.= " AND SpotEntry.gelID = '$map_to_search'" if $map_to_search;
      $command.= " AND SpotEntry.gelID = Gel.gelID";
      $command.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries;
      $command.= " AND SpotEntry.AC = ViewSpotEntry.AC AND SpotEntry.spotID = ViewSpotEntry.spotID AND SpotEntry.gelID = ViewSpotEntry.gelID"
        if $with_available_data;

      my $commandAccession = undef;
      if ($args{accession}) {
        $commandAccession = $command." AND Entry.AC = upper('".$args{accession}."')";
        $commandAccession.= ($map_to_search)? "" : " ORDER BY 2";
        $command .= " AND Entry.AC != upper('".$args{accession}."')";
      }

      $command.= ($map_to_search)? " ORDER BY 1":" ORDER BY 2,1";

      my $separate_by_blocks = ($map_to_search)? undef : '2:3';

      unless ($search_argument_not_ok) {
      my $mainCaption = 'Query Result:';
      my $notOK_note = '';
      if ($commandAccession) {
        &EXECUTE_FORMATTED_COMMAND({command=>$commandAccession, hidden_reference=>$hidden_reference, no_color_alternation=>1,
                                    group_rows=>1, general_table_border=>$with_available_data, no_table_caption=>2, caption=>'Searched entry',
                                    cell_valign=>$cell_valign, separate_by_blocks=>$separate_by_blocks, notOK_note=>$notOK_note, void_cell=>$co->em({class=>'smallComment'},'no data')});
        print $co->br."\n".$co->p;
        $mainCaption = 'Other entries';
        $notOK_note = '_nothing_';
      }
      &EXECUTE_FORMATTED_COMMAND({command=>$command, hidden_reference=>$hidden_reference, no_color_alternation=>1, group_rows=>1,
                                  general_table_border=>$with_available_data, caption=>$mainCaption,
                                  cell_valign=>$cell_valign, separate_by_blocks=>$separate_by_blocks, notOK_note=>$notOK_note, void_cell=>$co->em({class=>'smallComment'},'no data')});
      }
      if ( $search_argument_not_ok and (!$map_to_search and defined($map_to_search)) ) {
        &SEARCH_ARGUMENT_NOT_OK($map_to_search_name, 'bad map name');
      }
      elsif ($search_argument_not_ok) {
        &SEARCH_ARGUMENT_NOT_OK($search_spot, 'bad spot name');
      }

    }



    # Search by Identification Method

    elsif ($hidden_reference eq 'Ident') {

      my ($map_to_search, $mapNotFound, @ident_methods, $ident_boolean);

      @ident_methods = $co->param('search_ident_methods');
      @ident_methods = split '\0', $co->param('search_ident_methods') unless scalar (@ident_methods) >1;
      $ident_boolean = $co->param('search_ident_boolean')? $co->param('search_ident_boolean') : 'OR';

      my $map_to_search_name = $co->param('choose_map');
      $map_to_search_name =~ s/^.+\s+\-\-\s+//;
      $map_to_search_name =~ s/\s*\{([^\}]*)\}\s*$//;
      my $map_to_search_long_name = ($1)? " { ".$1." }" : '';
      if ($map_to_search_name !~ /All Maps/i) {
        $command = " SELECT gelID FROM Gel WHERE lower(shortName) = '".lc($map_to_search_name)."'";
        ($map_to_search) = EXECUTE_FAST_COMMAND($command);
        $mapNotFound = 1 unless $map_to_search;
        $map_to_search = defined unless $map_to_search;
      } else { undef($map_to_search) }

      $search_message = "Parsing in '$name2d_database_name' ";
      $search_message.= $co->br."for entries identified by: {".join (',', @ident_methods)."}";
      if ($map_to_search) {
        my $map_to_search_name_href = $co->a({href=>"$main::script_name$main::script_name_qmark"."map=".lc($map_to_search_name).
                                                    "$DB::ArgumentCurrentDatabaseNameURI"}, $map_to_search_name.$map_to_search_long_name);
        my $chosen_map_species = ( $co->param('choose_map') =~ /^(.+)\s*\-\-\s+/)?  " <em>$1</em>" : '';
        $search_message.= $co->br."on the$chosen_map_species '".$co->span({-class=>'red'},$map_to_search_name_href)."' map";
      }
      elsif ($mapNotFound) {
        $search_message.= br.br."Search limited to the '".$map_to_search_name."' map."
      }
      $search_message.= ".";
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";
      $search_argument_not_ok = 1 unless scalar (@ident_methods);

      my (%detected_ident_methods);
      my $cell_valign = 'middle';

      my $group_by_ac = ($co->param('search_ident_groupBy') eq 'Accession number')? 1 : 0;
      my $resultPattern = '@__AVAILABLE_RESULTS__@';
      my $ac_field = " '__AC__'|| Entry.AC || '__AC__' || '<br>('||Entry.ID::TEXT||')' as  A_upper_C_openP__upper_I_upper_D_closeP_,";
      $command = '';
      $command.= " SELECT DISTINCT";
      $command.= $ac_field if $group_by_ac;
      $command.= " '__SPOT__'|| Gel.shortName || '\:' || SpotEntry.spotID ||'__SPOT__' as spot__upper_i_upper_d,";
      $command.= " '__MAP__' || Gel.shortName || '__MAP__ {' || Gel.fullName || '}' as Map," if !$map_to_search;
      $command.= $ac_field unless $group_by_ac;

      # $command.= " '{' || array_to_string(SpotEntryMappingTopic.mappingTechnique,',') || '}' as Mapping_Techniques" ;
      $command.= " ViewMapEntryList.mappingtechniques || ".
                 "'$resultPattern' || Gel.shortName || '\:' ||SpotEntry.spotID || '__' || ViewMapEntryList.availableresults || '$resultPattern'".
                 " as Identification_Techniques";
      $command.= " FROM Entry, SpotEntry, Gel, ViewMapEntryList";
      $command.= " WHERE (";
      foreach my $ident_method (@ident_methods) {
        if ($ident_method eq 'PMF') {
          $command.= " (ViewMapEntryList.mappingtechniques ~ '{PMF}' OR ViewMapEntryList.availableresults ~ 'PMF') $ident_boolean";
          $detected_ident_methods{'PMF'} = 1;
          next;
        }
        if ($ident_method eq 'Aa') {
          $command.= " (ViewMapEntryList.mappingtechniques ~ '{Aa}' OR ViewMapEntryList.availableresults ~ '{Aa}') $ident_boolean";
          $detected_ident_methods{'Aa'} = 1;
          next;
        }
        if ($ident_method eq 'MS/MS') { 
          $command.= " (ViewMapEntryList.mappingtechniques ~ '{MS\/MS}' OR ViewMapEntryList.availableresults ~ 'MS\/MS') $ident_boolean";
          $detected_ident_methods{'MS/MS'} = 1;
          next;
        }
        $command.= " ViewMapEntryList.mappingtechniques ~ '{$ident_method}' $ident_boolean";
      }
      $command =~ s/$ident_boolean$//;
      $command.= ")";
      $command.= " AND SpotEntry.AC = Entry.AC";
      $command.= " AND SpotEntry.gelID = '$map_to_search'" if $map_to_search;
      $command.= " AND SpotEntry.gelID = Gel.gelID";
      $command.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries;
      $command.= " AND SpotEntry.AC = ViewMapEntryList.AC AND SpotEntry.spotID = ViewMapEntryList.spotID AND SpotEntry.gelID = ViewMapEntryList.gelID";
      $command.= ($map_to_search)? " ORDER BY 1" : " ORDER BY 2,1";

      my $separate_by_blocks = ($map_to_search)? undef : ($group_by_ac)? '3:2' : '2:2';

      if ($mapNotFound) {
        $command = "SELECT 'Cannot find a map named $map_to_search_name within this database!' as message";
        &EXECUTE_FORMATTED_COMMAND({command=>$command});
      }
      elsif (!$search_argument_not_ok) {
        my $identification_table =
          &EXECUTE_FORMATTED_COMMAND({command=>$command, hidden_reference=>$hidden_reference, no_color_alternation=>1, group_rows=>1,
                                      do_not_print=>1, cell_valign=>$cell_valign, separate_by_blocks=>$separate_by_blocks,
                                      void_cell=>$co->em({class=>'smallComment'},'no data')});
        $identification_table =~ s/([^>]+)$resultPattern([^@]+\:[^@]+)__([^_]*?)$resultPattern/&_href_available_results_($1,$2,$3)/ge;

        # ident legends
        my ($mapping_method_sub_command, $legend_command, @legend, $legend, $legend_row);
        while ($identification_table =~ /\{([^}]+)\}/g) { $detected_ident_methods{$1} = 1}
        if (%detected_ident_methods) {
          foreach my $key (keys %detected_ident_methods) {
            $mapping_method_sub_command.= "mappingTechnique = \'$key\' OR ";  
          }
          $mapping_method_sub_command =~ s/\s+OR\s*$//i;
          $mapping_method_sub_command = "AND (".$mapping_method_sub_command.")";  
          print $co->br."\n";  
          $legend_command = " SELECT DISTINCT mappingTechnique AS Mapping_methods, techniqueDescription AS Description".
                                    "  FROM MappingTopicDefinition WHERE mappingTechnique <> 'N/A' $mapping_method_sub_command ORDER BY 1";  
          @legend = &EXECUTE_FAST_COMMAND($legend_command);
          for (my $ii; $ii < (scalar @legend - 1); $ii+=2) {
            $legend.= $co->strong("{&nbsp;$legend[$ii]&nbsp;}")."&nbsp;=>&nbsp;\'$legend[$ii+1]\', ";
          }
          if ($legend) {
            $legend =~ s/,\s+$//;
          # $legend_row = $co->th({ -bgcolor => '#a52a2a'}, ['','',''] ).$co->th({ -bgcolor => "$main::bkgrd"}, ['','aa',''] )
            $legend_row = $co->Tr(td({-colspan=>3, -bgcolor=>'#a52a2a'},'')).$co->Tr(td({-colspan=>3, -bgcolor=>"$main::bkgrd"},'')) if $separate_by_blocks;
            $legend_row.= $co->Tr(td({-colspan=>3, -bgcolor=>'lightblue'},'')).
                          $co->Tr($co->td({-colspan=>3, -bgcolor=>'lightblue'},$co->strong({class=>'Estomped'}, 'Mapping legends: ').$legend));
            $identification_table =~ s/(\<\/table\>)$/$legend_row$1/i;
          }
        }

        if ($identification_table) {
          print $identification_table
        }
        else {
          &SEARCH_ARGUMENT_NOT_OK('','no result');
        }

      }

      &SEARCH_ARGUMENT_NOT_OK('', 'no_ident_methods') if $search_argument_not_ok;

    }



    # Search by pI/Mw

    elsif ($hidden_reference eq 'pI') {

      my ($map_to_search, $mapNotFound);

      my $map_to_search_name = $co->param('choose_map');
      $map_to_search_name =~ s/^.+\s+\-\-\s+//;
      $map_to_search_name =~ s/\s*\{([^\}]*)\}\s*$//;
      my $map_to_search_long_name = ($1)? " { ".$1." }" : '';
      if ($map_to_search_name !~ /All Maps/i) {
        $command = " SELECT gelID FROM Gel WHERE lower(shortName) = '".lc($map_to_search_name)."'";
       ($map_to_search) = EXECUTE_FAST_COMMAND($command);
        $mapNotFound = 1 unless $map_to_search;
      } else { undef($map_to_search) }

      my $order_by = $co->param('order_by');

      my $pi_min = $co->param('pi_min');
      my $pi_max = $co->param('pi_max');
      my $mw_min = $co->param('mw_min');
      my $mw_max = $co->param('mw_max');      
      map  {$_ =~ s/^\s*(.*\S)\s*$/$1/} ($pi_min, $pi_max, $mw_min, $mw_max);
      $search_argument_not_ok = grep {$_ !~ /^(\d*\.*\d+|\d+\.*\d*)$/ or $_ < 0} ($pi_min, $pi_max, $mw_min, $mw_max);
      $search_argument_not_ok = ($pi_min == $pi_max or $mw_min == $mw_max)? 1 : $search_argument_not_ok;

      ($pi_min, $pi_max) = sort {$a <=> $b} ($pi_min, $pi_max);
      ($mw_min, $mw_max) = sort {$a <=> $b} ($mw_min, $mw_max);
      $mw_min = int($mw_min*1000), $mw_max = int($mw_max*1000) unless $search_argument_not_ok;

      $search_message = "Searching in '$name2d_database_name' for proteins identified in the following range:".br;
      $search_message.= $co->em("&nbsp;&nbsp; pI")."&nbsp;[&nbsp;".$co->span({-class=>'brown'}, $pi_min)."&nbsp;-&nbsp;".$co->span({-class=>'brown'}, $pi_max)."&nbsp;]&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;&nbsp;";
      $search_message.= $co->em("Mw")."&nbsp;[&nbsp;".$co->span({-class=>'brown'}, $mw_min)."&nbsp;-&nbsp;".$co->span({-class=>'brown'}, $mw_max)."&nbsp;]";
      if ($map_to_search) {
        my $map_to_search_name_href = $co->a({href=>"$main::script_name$main::script_name_qmark"."map=".lc($map_to_search_name).
                                                    "$DB::ArgumentCurrentDatabaseNameURI"}, $map_to_search_name.$map_to_search_long_name);
        my $chosen_map_species = ( $co->param('choose_map') =~ /^(.+)\s*\-\-\s+/)?  " <em>$1</em>" : '';
        $search_message.= br.br."Search limited to the$chosen_map_species '".$map_to_search_name_href."' map."
      }
      elsif ($mapNotFound) {
        $search_message.= br.br."Search limited to the '".$map_to_search_name."' map."
      }
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";

      my ($order_by_field, $other_field) = ($co->param('order_by') eq 'Accession number')?
        ('Entry.AC', 'Entry.ID') : ('Entry.ID', 'Entry.AC');
      my $order_by_header = ($co->param('order_by') eq 'Accession number')?
        "Accession_upper__number_openP__upper_I_upper_D_closeP_" :  "I_upper_D_openP__upper_a_upper_c_closeP_";

      $command = " SELECT";
      $command.= " '__MAP__' || Gel.shortName || '__MAP__ {' || Gel.fullName || '}' as map," unless $map_to_search;
      $command.= " '__AC__'||".$order_by_field."||'__AC__'||'<br>('||".$other_field."::TEXT||')' as  $order_by_header,";
      $command.= " '__SPOT__'||Gel.shortName||'\:'||SpotEntry.spotID||";
      $command.= "'__SPOT__' as spot__upper_I_upper_D, Spot.pI as p_upper_I, Spot.mw as _upper_Mw";
      $command.= " FROM Gel, Entry JOIN (";
      $command.= "     SpotEntry JOIN Spot ON (";
      $command.= "         (SpotEntry.spotID = Spot.spotID AND SpotEntry.gelID = Spot.gelID)";
      $command.= "         AND ( Spot.pI IS NULL OR (Spot.pI >= '$pi_min' AND Spot.pI <= '$pi_max') )";
      $command.= "         AND   Spot.mw >= '$mw_min' AND Spot.mw <= '$mw_max'";
      $command.= "         AND Spot.gelID = '$map_to_search'" if $map_to_search;
      $command.= "     )";
      $command.= " ) ON ( Entry.AC = SpotEntry.AC ) WHERE Gel.gelID = Spot.gelID";
      $command.= " AND $SERVER::showFlagSQLEntry" unless $show_hidden_entries;
      $command.= " ORDER BY ". ( ($map_to_search)? "" : "map, " ) ."$order_by_field, $other_field";

      if ($mapNotFound) {
        $command = "SELECT 'Cannot find a map named $map_to_search_name within this database!' as message";
        &EXECUTE_FORMATTED_COMMAND({command=>$command});
      }
      elsif (!$search_argument_not_ok) { 
        my $execute_command = ($map_to_search)?
           &EXECUTE_FORMATTED_COMMAND({command=>$command, hidden_reference=>$hidden_reference, no_color_alternation=>1, group_rows=>1, void_cell=>$co->em({class=>'smallComment'},'not defined')}) :
	   &EXECUTE_FORMATTED_COMMAND({command=>$command, hidden_reference=>$hidden_reference, no_color_alternation=>1, group_rows=>1, , void_cell=>$co->em({class=>'smallComment'},'not defined'), separate_by_blocks=>'1:1'});
      }
      else {
        my @false_pi_mw = grep {$_ !~ /^(\d*\.*\d+|\d+\.*\d*)$/} ($pi_min, $pi_max, $mw_min, $mw_max);
        @false_pi_mw = ('__zero_range__', $pi_min, $pi_max, $mw_min, $mw_max) if @false_pi_mw == 0;
        map {$_ = "\'$_\'"} @false_pi_mw;
        &SEARCH_ARGUMENT_NOT_OK(join(' ',@false_pi_mw), 'bad pi_mw range') if $search_argument_not_ok;
      }

    }




    # Search by Combined queries

    elsif ($hidden_reference eq 'Combined') {

      $search_message = "Searching in '$name2d_database_name' for entries matching your specific query:";
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";

     (my $include_external_data, my $combinedFieldsMaxNumber) =
       ($co->param('include_external_data'), $co->param('CombinedMax'));
      my $query_status = &COMBINED_QUERY({include_external_data=>$include_external_data,
                                          combined_fieldsMaxNumber=>$combinedFieldsMaxNumber});


      if ($query_status == -1) { &SEARCH_ARGUMENT_NOT_OK('', 'bad_combined_keywords') }
      if ($query_status == -2) { &SEARCH_ARGUMENT_NOT_OK('', 'bad_combined_digits') }
      if ($query_status == -3) { &SEARCH_ARGUMENT_NOT_OK('', 'bad_combined_date') }
      if ($query_status == -4 
       or $query_status == -5) { my $note = ($co->param('combined_date') eq 'Creation Date')? 'created':'updated';
                                 if ($co->param('date_since')) {
                                   $note .= " since ".$co->strong($co->param('date_since')).
                                     (($co->param('date_before'))? ' and ' : '');
                                 }
                                 $note .= ' before '.$co->strong($co->param('date_before')) if ($co->param('date_before'));
                                 &SEARCH_ARGUMENT_NOT_OK($note, 'bad_combined_limit_date')
                               }

    }



    # Search and List proteins in a Map / Map Experimental info

    elsif ($hidden_reference eq 'Map' or $hidden_reference eq 'MapExpInfo')   {
      my $onlyExpInfo = ($hidden_reference eq 'MapExpInfo')? 1 : 0;
      my $mapResultFound = 0;
      my $print_map_list = my $image_src = my $image_link = '';

      my $map_name = $co->param('choose_map');
      $map_name =~ s/^.+\s+\-\-\s+//;
      $map_name =~ s/\s*\{([^\}]*)\}\s*$//;
      $command = " SELECT Gel.gelID, Gel.shortName, Gel.fullName, Gel.melanieGeneratedID, Organism.organismSpecies, Organism.taxonomyCode, Gel.dimension".
                 " FROM Gel, Organism WHERE lower(Gel.shortName) = '".lc($map_name)."'".
                 " AND Gel.organismID = Organism.organismID";
     (my $map, my $map_short_name, my $map_long_name, my $melanie_like_id, my $map_os, my $map_ox, my $map_dimension)
       = EXECUTE_FAST_COMMAND($command);
      my $sp_tissue;
      if ($map) {

       ($image_src) =  EXECUTE_FAST_COMMAND("SELECT GelImage.smallImageUrl FROM GelImage WHERE GelImage.gelID = $map");
        if (!$main::extract_image_URL_from_DB or !$image_src or !-e $image_src) {
          my $sgifs = ($main::database[$DB::CurrentDatabase]{small_image_url})?
            $main::database[$DB::CurrentDatabase]{small_image_url} : $main::default_small_image_url;
          my $small_image_type = ($main::database[$DB::CurrentDatabase]{small_image_type})?
            $main::database[$DB::CurrentDatabase]{small_image_type} : $main::default_small_image_type;
	  $image_src = "$sgifs/".uc($map_name).".$small_image_type";
	}
        $image_link = $co->img({align => "top", height => 30, hspace => 3, vspace => 3, src => "$image_src", alt => ""});
        $image_link = $co->a({href =>
          "$main::map_viewer$main::map_viewer_qmark".
          "map=$map_name\&ac=all$DB::ArgumentCurrentDatabaseNameURI"}, $image_link);

        $command = "SELECT TissueSP.tissueSPDisplayedName FROM GelTissueSP, TissueSP ".
                   "WHERE GelTissueSP.gelID = $map AND GelTissueSP.tissueSPName = TissueSP.tissueSPName";
       ($sp_tissue) = EXECUTE_FAST_COMMAND($command);
      }
      my $lien_newt = &get_href({database=>'NEWT', IDs=>[$map_ox]});
      my $map_os_ox_url = ($map_ox)? $co->a({href=>"$lien_newt", target=>"_blank"}, $map_os) : "$map_os";
      my $map_os_message = ($map_long_name !~ /$map_os/i)? $co->em(" {".$map_os_ox_url."}") : '';
      my $sp_tissue_message = ($sp_tissue)? $co->br.'tissue: '.ucfirst(lc($sp_tissue)) : '';

     #$command = " SELECT releaseNum, releaseDate FROM Release WHERE releaseNum =  make2db_last_release()";
      $command = "SELECT databaseRelease, databaseSubRelease, to_char(databaseReleaseDate, 'DD Month YYYY') FROM Database LIMIT 1";
      my ($release, $sub_release, $release_date) = EXECUTE_FAST_COMMAND($command);
      $melanie_like_id = ($melanie_like_id eq $map_name)? 'reference map' : $melanie_like_id;
      $search_message = (($onlyExpInfo)? 'Experimental information' : 'Protein list').' for '.
                       (($map_short_name)?
                        $co->a({href=>"$main::map_viewer$main::map_viewer_qmark"."map=$map_short_name\&ac=all$DB::ArgumentCurrentDatabaseNameURI", class=>'red'}, "$map_short_name").'&nbsp;'.(($image_src)? $image_link : '').
                        $co->br.$map_long_name."&nbsp;".$map_os_message.$sp_tissue_message :
                        "'unknown'".(($map_name)? " ($map_name)": ''));

#      $search_message.= $co->br.$co->span({class=>'smallComment'}, " (map ID: $melanie_like_id)") if $melanie_like_id;
#      $search_message.= $co->br.$co->span({class=>'smallComment'}, "Release: $release".(($sub_release)? ".$sub_release" : '')." / $release_date") if $release;
      $search_message = $co->h4($search_message);
      print IntoCenteredColoredCell($search_message)."\n";

      my ($gel_preparation_txt, $gel_preparation_uri, $gel_preparation_doc, $gel_informatics_txt,
          $gel_informatics_uri, $gel_informatics_doc, $gel_informatics_soft,
          $gel_strain, $gel_pi_start, $gel_pi_end, $gel_mw_start, $gel_mw_end, $gel_sp_tissue,
          $gel_number_identified_spots, $gel_number_proteins, $gel_number_spots);
      if ($map) {

        $command = " SELECT GelPreparation.preparationDescription, GelPreparation.URI, GelPreparation.preparationDocument,".
                   " GelInformatics.informaticsDescription, GelInformatics.URI, GelInformatics.informaticsDocument,  GelInformatics.soft,".
                   " Gel.organismStrain, Gel.startpI, Gel.endpI, Gel.startMW, Gel.endMW, TissueSP.tissueSPDisplayedName".
                   " FROM Gel LEFT JOIN GelPreparation ON (Gel.gelPreparationID = GelPreparation.gelPreparationID)".
                   " LEFT JOIN GelInformatics ON (Gel.gelInformaticsID = GelInformatics.gelInformaticsID)".
                   " LEFT JOIN GelTissueSP ON (Gel.gelID = GelTissueSP.gelID)".
                   " LEFT JOIN TissueSP ON (GelTissueSP.tissueSPName = TissueSP.tissueSPName)".
                   " WHERE Gel.gelID = $map";

       ($gel_preparation_txt, $gel_preparation_uri, $gel_preparation_doc, $gel_informatics_txt, $gel_informatics_uri, $gel_informatics_doc, $gel_informatics_soft, $gel_strain, $gel_pi_start, $gel_pi_end, $gel_mw_start, $gel_mw_end, $gel_sp_tissue)
          = EXECUTE_FAST_COMMAND("$command");
        my $excludeUnidentified = ($unidentifiedProteinNickname)? " AND AC <> '$unidentifiedProteinNickname'" : '';
        $command = "SELECT count(DISTINCT spotID) FROM ViewMapEntryList WHERE gelID = $map".$excludeUnidentified;
       ($gel_number_identified_spots) = EXECUTE_FAST_COMMAND("$command");
        $command = "SELECT count(DISTINCT AC) FROM ViewMapEntryList WHERE gelID = $map".$excludeUnidentified;
       ($gel_number_proteins) = EXECUTE_FAST_COMMAND("$command");
        $command = "SELECT count(*) FROM Spot WHERE gelID = $map";
       ($gel_number_spots) =  EXECUTE_FAST_COMMAND("$command");
        if ($gel_number_spots and $gel_number_spots <= $gel_number_identified_spots) {
          $command = "SELECT GelImage.detectedSpots FROM GelImage WHERE GelImage.gelID = $map";
         ($gel_number_spots) =  EXECUTE_FAST_COMMAND("$command");
        }

      }
      if ($sample_origin_URL) {
        my  $analysisMessage = ($gel_preparation_uri or $gel_preparation_doc or $gel_informatics_uri or $gel_informatics_doc)?
            '' : ' and Post-separation Analysis';
        print $co->p({class=>"center"},
              $co->a({href=>"$sample_origin_URL", target=>"_blank"}, "Sample Preparation$analysisMessage")
#              .$co->br."\n".$co->span({class=>'smallComment'}, " (the sample description may be free text, until the 'Proteomics Standards Initiative' is finalised)")
				.$co->br."\n"
            );
      }
      my $gelDocuments;
      if ($gel_preparation_uri) {
        my $gelDocMsg1 = ($gel_preparation_uri ne $gel_informatics_uri)? 'Gel Preparation' : 'Gel Related Documents';
        $gelDocuments = $co->a({href=>"$gel_preparation_uri", target=>"_blank"}, "$gelDocMsg1")."\n";
      }
      if ($gel_informatics_uri and $gel_informatics_uri ne $gel_preparation_uri) {
        $gelDocuments.= '&nbsp;/&nbsp;' if $gelDocuments;
        $gelDocuments.= $co->a({href=>"$gel_informatics_uri", target=>"_blank"}, 'Gel Informatics')."\n";
      }
      if ($gel_preparation_doc or $gel_informatics_doc) {
        my $currentDatabase = $main::database[$DB::CurrentDatabase]{dbname};
        my $docDir = "$main::url_www2d/data/doc/$currentDatabase/";
        $gelDocuments.= ' (local documents: ';
        if ($gel_preparation_doc) {
          $gelDocuments.= $co->a({href=>"$docDir$gel_preparation_doc", target=>"_blank"}, 'preparation')."\n";
        }
        if ($gel_informatics_doc) {
          $gelDocuments.= '&nbsp;/&nbsp;' if $gel_preparation_doc;
          $gelDocuments.= $co->a({href=>"$docDir$gel_informatics_doc", target=>"_blank"}, 'informatics')."\n";
        }
        $gelDocuments.= ')';
      }
      $gelDocuments.= $co->br if $gelDocuments;
      print $co->p({class=>"center"}, $gelDocuments.
#	  $co->span({class=>'smallComment'}, " (the gel protocols may be free text, until the 'Proteomics Standards Initiative' is finalised)").
	  $co->br."\n"
            ) if !$onlyExpInfo and ($gel_preparation_uri or $gel_informatics_uri or $gel_preparation_doc or $gel_informatics_doc);
      print $co->br.$co->hr."\n" if $sample_origin_URL or $gel_preparation_uri;

      my (@query, @query_copy);
      my (@query_GN, @query_DE, @query_spots, @query_AC, @query_ID, @query_pi, @query_mw, @query_volume, @query_od, @query_fragment, @query_topics, @query_mapping);
      my (@query_results, @query_ref);
      my $fields_number = 14;

      unless ($onlyExpInfo) {

        my $order_by = $co->param('order_by');
        $order_by = ($order_by eq 'Protein ID')? "id" : ($order_by eq 'Accession number')? "ac" : "genes";

        $command = " SELECT genes, description, spotID, ac, id, pI, mw, volumeRelative, odRelative, fragment, topicDescription, mappingTechniques,";
        $command.= " availableResults, referenceIDs FROM ViewMapEntryList WHERE gelID = $map AND $SERVER::showFlagSQLViewMap ORDER by $order_by";

        @query = @query_copy = EXECUTE_FAST_COMMAND($command) if $map;

        my $matchs = int(($#query+1)/$fields_number); # defining array length speeds up things
        $#query_GN = $#query_DE = $#query_spots = $#query_AC = $#query_ID = $#query_pi = $#query_mw = $#query_volume = $#query_od = $#query_fragment 
                   = $#query_topics = $#query_mapping = $#query_results = $#query_ref = $matchs -1;


        for (my $ii = 0; $ii < $matchs; $ii++) {
          my $jj = $ii * $fields_number;
          $query_GN[$ii]       = $query[$jj++];
          $query_DE[$ii]       = $query[$jj++];
          $query_spots[$ii]    = $query[$jj++];
          $query_AC[$ii]       = $query[$jj++];
          $query_ID[$ii]       = $query[$jj++];
          $query_pi[$ii]       = $query[$jj++];
          $query_mw[$ii]       = $query[$jj++]; 
          $query_volume[$ii]   = $query[$jj++];
          $query_od[$ii]       = $query[$jj++];
          $query_fragment[$ii] = $query[$jj++];
          $query_topics[$ii]   = $query[$jj++];
          $query_mapping[$ii]  = $query[$jj++];
          $query_results[$ii]  = $query[$jj++];
          $query_ref[$ii]      = $query[$jj];
        }

        if ($co->param('spot_volume')) {
          $MAP::include_spot_volume = 1;
        } else {
          @query_volume = @query_od = ();
        }

        undef(@query);
        undef(@query_pi) if $map_dimension == 1;
        map { $_=~ s/\./\'/} @query_mw; 

        if ($query_spots[0]) {

           $mapResultFound = 1;

           $print_map_list = &PRINT_MAP_LIST ($map_name, \@query_GN, \@query_DE, \@query_spots, \@query_AC, \@query_ID,
                                  \@query_pi, \@query_mw, \@query_volume, \@query_od, \@query_fragment,
                                  \@query_topics, \@query_mapping, \@query_results, \@query_ref, $order_by);

           print $print_map_list->{mapListTable};
           print "\n";
        }

      }

      my $proteinListMappingMethods = ($print_map_list and $print_map_list->{mappingMethods})? 1 : 0;

      # restrict to MapExpInfo in future versions
      my $showMapInfoTable =
       ($map_ox or $map_os or $gel_preparation_txt or $gel_informatics_txt or $gel_informatics_soft or $gel_strain
                or $gel_pi_start or $gel_pi_end or $gel_mw_start or $gel_mw_end or $gel_sp_tissue
                or $proteinListMappingMethods)? 1 : 0;

      if ($showMapInfoTable) {

        $mapResultFound = 1;
        my $caption = $co->caption({-class=>'H4Font'}, $co->span({style=>$font_style}, 'Gel&nbsp;experimental&nbsp;information'), $co->br.$co->br)."\n";
	my @rows;
#        my @rows = th({ -bgcolor => 'lightblue'}, ['Information Type','Data']);
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, 'Map&nbsp;name'),$co->span({style=>$font_style}, $map_short_name." {$map_long_name}")]));
#        push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Dimension'),$map_dimension.'-D']));
        if ($gelDocuments) {
          push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Related&nbsp;documents'),$gelDocuments]))
        }
        if ($gel_strain or $map_ox or $map_os) {
          my ($gel_related_species_title, $gel_related_species_data);
          if ($map_ox or $map_os) {
            $gel_related_species_title = 'species';
            if ($map_ox and $map_os) {
              $gel_related_species_data = $co->em($map_os).' / TaxID:'.$co->a({href=>"$lien_newt", target=>"_blank"}, $map_ox)
            } elsif ($map_ox) {
              $gel_related_species_data = 'TaxID:'.$co->a({href=>"$lien_newt", target=>"_blank"}, $map_ox)
            } elsif ($map_os) {
              $gel_related_species_data = $co->em($map_os)
            }
          }
          $gel_related_species_data = "[$gel_related_species_data]" if $gel_related_species_data and $gel_strain;
          $gel_related_species_title = 'Strain&nbsp;Comment ['.$gel_related_species_title.']' if $gel_strain;
#          push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong($gel_related_species_title), $gel_strain.' ' .$gel_related_species_data]));
        }
        if ($gel_sp_tissue) {
          $gel_sp_tissue = $co->a({href=>"$main::tissue_list_URL", target=>"_blank"}, $gel_sp_tissue) if $main::tissue_list_URL;
          push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Tissue (Swiss-Prot definition)'),$gel_sp_tissue]))
        }
        if ($gel_preparation_txt) {
          push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Gel&nbsp;Preparation'),$gel_preparation_txt]))
        }
        if ($gel_pi_start and !$gel_pi_end) {$gel_pi_end = 'undefined'}
        elsif (!$gel_pi_start and $gel_pi_end) {$gel_pi_start = 'undefined'}
        if ($gel_mw_start and !$gel_mw_end) {$gel_mw_end = 'undefined'}
        elsif (!$gel_mw_start and $gel_pi_end) {$gel_mw_start = 'undefined'}
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, 'pI'), $co->span({style=>$font_style}, "$gel_pi_start - $gel_pi_end")])) if $gel_pi_start and $map_dimension > 1;
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, 'Mw'),$co->span({style=>$font_style}, "$gel_mw_start - $gel_mw_end")])) if $gel_mw_start;
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Gel&nbsp;Informatics'),$gel_informatics_txt])) if $gel_informatics_txt;
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Informatics&nbsp;Software'),$gel_informatics_soft])) if $gel_informatics_soft;
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, '#detected&nbsp;spots'),$co->span({style=>$font_style}, $gel_number_spots)])) if $gel_number_spots;
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, '#identified&nbsp;spots'),$co->span({style=>$font_style}, $gel_number_identified_spots)])) if $gel_number_identified_spots;
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, '#identified&nbsp;proteins'),$co->span({style=>$font_style}, $gel_number_proteins)])) if $gel_number_proteins;
        my $mappindMethodData = '';
        if ($proteinListMappingMethods) {
          # $mappindMethodData = ${$print_map_list->{mappingMethods}};
        }
        else {
          $command = "SELECT DISTINCT SpotEntryMappingTopic.mappingtechnique".
                     " FROM SpotEntryMappingTopic WHERE SpotEntryMappingTopic.gelID = $map";
          my @gelMappingMethods = EXECUTE_FAST_COMMAND("$command");
          $command = "SELECT MappingTopicDefinition.mappingTechnique, MappingTopicDefinition.techniqueDescription".
                     " FROM MappingTopicDefinition";
          my @allMappingMethods = EXECUTE_FAST_COMMAND("$command");
          my %allMappingMethods = my %alreadyCitedMappingMethods = ();
          for (my $ii = 0; $ii < scalar(@allMappingMethods); $ii+=2) {
            $allMappingMethods{$allMappingMethods[$ii]} = $allMappingMethods[$ii+1];
          }
          my $gelMappingMethods = '';
#          for (my $ii = 0; $ii < scalar(@gelMappingMethods); $ii++) {
          for (my $ii = 0; $ii <= $#gelMappingMethods; $ii++) {
            $gelMappingMethods[$ii] =~ s/^{|}$//g;
            foreach my $gelMappingMethodsAtomic (split ",", $gelMappingMethods[$ii]->[0]) {
              next if $alreadyCitedMappingMethods{$gelMappingMethodsAtomic};
              $mappindMethodData .= $co->span({style=>$font_style}, $allMappingMethods{$gelMappingMethodsAtomic}).', ';
#              $mappindMethodData .= "{<strong>".$gelMappingMethodsAtomic."</strong>}&nbsp;".$allMappingMethods{$gelMappingMethodsAtomic}.', ';
              $alreadyCitedMappingMethods{$gelMappingMethodsAtomic} = 1;
            }
          }
        }
        if ($mappindMethodData) {
          $mappindMethodData =~ s/,\s+$//;
          push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, 'Mapping&nbsp;Methods'),$mappindMethodData])) if $mappindMethodData;
        }
        if ($main::swiss_2d_page) {
          my $map_reference = "/ch2d/ch2d-refs.html#$map_short_name";
          $map_reference = $main::expasy.$map_reference unless $SERVER::ExPASy;
          push (@rows, td({-bgcolor => '#fffafa'}, [$co->strong('Reference'),
                $co->a({href=>"$map_reference", target=>"_blank"},'Bibliographic reference')]));
        }
        if ($hidden_reference eq 'MapExpInfo') {
          my $map_protein_list_href = $co->a({href=>"$main::script_name$main::script_name_qmark"."map=".lc($map_name).
                                                    "$DB::ArgumentCurrentDatabaseNameURI", style=>$link_style}, 'Display list');
          push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, 'Protein list'), $map_protein_list_href]));
        }
        $image_link = $co->img({align => "top", height => 100, hspace => 3, vspace => 3, src => "$image_src", alt => "[Click for graphical interface]"});
        $image_link = $co->a({href =>
          "$main::map_viewer$main::map_viewer_qmark".
          "map=$map_name\&ac=all$DB::ArgumentCurrentDatabaseNameURI"}, $image_link);
        push (@rows, td({-bgcolor => '#fffafa'}, [$co->span({style=>$font_style." font-weight: bold;"}, 'Graphical interface'), $co->br.$image_link])) if $image_src;

        print $co->br.
          $co->table({-cellspacing=>0, -border=>0, -width=>"90%"},
                  $caption, TR({-align=>'left', -valign=>'middle'},\@rows)
               ).$co->br.$co->p."\n";
      }

      unless ($mapResultFound) {
        my $no_or_noMore_data = ($map_short_name)? 'more ' : '';
        print $co->h3({class=>"center"}, "Sorry, no ".$no_or_noMore_data."related data could be retrieved for the $map_name gel / reference map.")."\n";
      }

      if ($co->param('save_file') and $query_spots[0]) {
        my $data;
        for (my $ii = 0; $ii < $#query_copy; $ii+= $fields_number) {
          $data.= join ("\t", @query_copy[$ii..$ii+$fields_number-1])."\n";
        }
        &SAVE_FILE_DATA($data);
      }

    }


    # hidden_references_more_arguments

    # displaying the tool current technical data
    elsif ($hidden_reference eq 'make2d') {
      $command = " SELECT Make2DDBTool.version, Make2DDBTool.subVersion FROM Make2DDBTool ORDER BY version LIMIT 1";
      if ($args{format} eq 'html') {
        &EXECUTE_FORMATTED_COMMAND({command=>$command, table_width=>'80%', group_rows=>1,
                                    table_alignment_general=>'center', caption=>"Make2D-DB II Technical Data"})}
      else {
        my ($version, $subVersion) = &EXECUTE_FAST_COMMAND($command);
       ($version, $subVersion) = ($VERSION, $sub_VERSION) unless defined($version);
        $text_output = "version: $version\tsub-version: $subVersion";
        print $resultTextStart.$text_output.$resultTextEnd;
      }
    }

    # Displaying the index data for retrieval (http format also available)
    elsif ($hidden_reference eq 'index') {
      my $indexDBcode = my $caption_index = $co->param('hidden_reference_more_arguments');
      my $extra_order = ($indexDBcode)? '' : ', 2';
         $indexDBcode.= ($indexDBcode)? '' : ' 1 OR EntryXrefDB.XrefDBCode = 2';
      my $text_output;
      $command = " SELECT EntryXrefDB.AC || ' (' || Entry.ID || ')' as _upper_A_upper_C, XrefDB.XrefDBName as _upper_X_upper_Ref___upper_D_upper_B__name, ".
                 " EntryXrefDB.XrefPrimaryIdentifier as _upper_Primary__upper_Identifier FROM EntryXrefDB, ENTRY, XrefDB ".
                 " WHERE (EntryXrefDB.XrefDBCode = $indexDBcode) AND EntryXrefDB.AC = Entry.AC AND $SERVER::showFlagSQLEntry ".
                 " AND EntryXrefDB.XrefDBCode = XrefDB.XrefDBCode ORDER BY 1$extra_order";  
      if ($args{format} eq 'html') {&EXECUTE_FORMATTED_COMMAND({command=>$command, table_width=>'40%', group_rows=>1,
                                                                table_alignment_general=>'center', caption=>"List of index $caption_index"})}
      else {
        my (@index_list) = &EXECUTE_FAST_COMMAND($command);
        for (my $ii = 0; $ii < scalar @index_list; $ii+=3) {
          $text_output.= $index_list[$ii]." ".$index_list[($ii+1)]." ".$index_list[($ii+2)]."\n";
        }
        $text_output =~ s/\n$//s;
        print $resultTextStart.$text_output.$resultTextEnd;
      }
    }

    # Displaying the maplist data for retrieval (http format also available)
    elsif ($hidden_reference eq 'maplist') {
      my $text_output;
      my $speciesCode;
      $speciesCode = $1 if ($args{species} =~ /^(\d+)$/);

      $command = " SELECT Gel.shortname as Gel_short_name, Gel.fullName as Gel_full_name, Gel.melanieGeneratedID as Gel__upper_I_upper_D,".
                 " Organism.taxonomyCode as Species__upper_Code, Organism.organismSpecies as Species_common_name,".
                 " TissueSP.tissueSPDisplayedName as SwissProt__upper_Tissue,".
                 " GelImage.imageUrl, GelImage.smallImageUrl".
                 " FROM Organism, Gel LEFT JOIN GelTissueSP ON (Gel.gelID = GelTissueSP.gelID)".
                 " LEFT JOIN TissueSP ON (TissueSP.tissueSPName = GelTissueSP.tissueSPName)".
                 " LEFT JOIN GelImage ON (GelImage.GelID = Gel.gelID)".
                 " WHERE Gel.organismID = Organism.organismID".
               (($speciesCode)? " AND Organism.taxonomyCode = $speciesCode" : '');

      my $command_num_fields = 8;

      if ($args{format} eq 'html') {
        $speciesCode = "for secpies with code: $speciesCode" if $speciesCode;
        &EXECUTE_FORMATTED_COMMAND({command=>$command, table_width=>'80%', group_rows=>1,
                                    table_alignment_general=>'center', caption=>"List of maps $speciesCode"})}
      else {
        my (@map_list) = &EXECUTE_FAST_COMMAND($command);
        for (my $ii = 0; $ii < scalar @map_list; $ii+=$command_num_fields) {
          my $map_list_section = join "\t", @map_list[$ii..($ii+$command_num_fields-1)];
          $text_output.= "$map_list_section\n";
        }
        $text_output =~ s/\n$//s;
        print $resultTextStart.$text_output.$resultTextEnd;
      }
    }

    # Displaying the databases' interface names for retrieval
    elsif ($hidden_reference eq 'databaselist') {
      my @databaseList = ();
      foreach my $databaseNum (@main::DATABASES_Included) {
        push @databaseList, $main::database[$databaseNum]{database_name};
      }
      @databaseList = ($main::default_dbname) unless @databaseList;
      $text_output = join "\n", @databaseList;
      $text_output =~ s/\n$//s;
      print $resultTextStart.$text_output.$resultTextEnd;
    }

    print $resultEndDatabase unless $main::SeveralResponse;

    print $resultEnd;

  } # end if hidden_reference



  # Or Execute a General/Predefined pgsql Command (database administrators)
  #=======================================================================#

  elsif ($SERVER::core_selector) {
    &CORE_ExecuteBatchCommand;
  }

}


#============================================================================================#
#============================================================================================#

sub _href_available_results_ {
  my ($identification_techniques, $spot, $available_results) = @_;
  $identification_techniques =~ s/,/} {/g;
  $spot =~ s/^(\w+)/lc($1)/e;
  return $identification_techniques unless $spot and $available_results;
  my $a_href_pref = "<a href=\"$main::script_name$main::script_name_qmark"."spot=$spot$DB::ArgumentCurrentDatabaseNameURI\&data=";
  while ($available_results =~ s/(\w+(?:\/?(\w+))?)//) { # \\/
    my $available_result = lc($1);
   (my $available_result_url = $available_result) =~ s/\W//g;
    $identification_techniques =~
      s/\b($available_result)\b/$a_href_pref$available_result_url\">$1<\/a>/i;
  }
  return $identification_techniques;
}

#============================================================================================#
#============================================================================================#


#==============================#
# Get a Command                #
#==============================#

# Already Treated in the Execute Command section

if (($co->param('Execute query') or $co->param('hidden_reference')) and !$new_Search_Argument) {}

# Get a Command

elsif ($Search_Argument eq 'AC') {

  # hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"AC");
  print $co->hidden('hidden_reference');

  print $co->h2($name2d) if ($SERVER::ExPASy);
  print $co->h2($co->span({style=>$font_style}, 'Search by CIPRO ID')),"\n",
        $co->span({style=>$font_style}, 'Enter a CIPRO ID:&nbsp;'), $co->textfield({ -value=> '', -name=>'search_AC'}).$co->p."\n";

  my %labels = (
       'exact_AC'   => ' Exact Name',
       'partial_AC' => ' Partial Name'
  );

=pod
  print "AC Pattern:<br>",
  $co->radio_group
  (
      -name=>'AC_pattern', 
      -values=>['exact_AC','partial_AC'], 
      -default=>'exact_AC',  
      -labels=>\%labels
  ),p;
=cut	
  my %labels2 = (
       'NICE'   => ' Nice View',
       'TEXT' => ' Text View',
       'RAW' => ' Raw View'
  );

=pod
  print "Output:<br>",
  $co->radio_group
  (
      -name=>'AC_output', 
      -values=>['NICE','TEXT', 'RAW'], 
      -default=>'NICE',  
      -labels=>\%labels2
  );
=cut

=pod
  print $co->p.$co->hr.$co->br.$co->submit({-name=>'Execute query', -label =>'List all ACs'})."\n";
  #print "&nbsp;&nbsp;&nbsp;".
  #      $co->input({-type=>"button", -name=>'Execute query', -value=>'List all ACs', onMouseUp=>"submit(this)"})."\n".p."\n";

  print '&nbsp;&nbsp;only for maps containing:&nbsp', $co->textfield({ -value=> '', -name=>'map_name_pattern'}).$co->p."\n";
  print "Sort by:<br>",
  $co->radio_group
  (
      -name=>'AC_List_output', 
      -values=>['Accession number','Protein ID'], 
      -default=>'Accession number',
      -labels=>\%labels2
  ).$co->p."\n";
=cut
  print $co->hr,p,"\n";

  print $co->span({style=>$font_style}, "Please give the accession number or an entry name. For example, you may type "). 
        $co->span({style=>$font_style." font-weight: bold;"}, "CIPRO100.11.1").
	$co->span({style=>$font_style}, " or ").
	$co->span({style=>$font_style." font-weight: bold;"}, "KH.C5.375").
	$co->span({style=>$font_style}, ".").$co->p;

}


# Search by description

elsif ($Search_Argument eq 'DE') {


  # hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"DE");
  print $co->hidden('hidden_reference');
  
  my ($includeUniProt, $includeKW);
  $includeUniProt = 1 if ($main::show_external_data and EXECUTE_FAST_COMMAND("SELECT AC FROM ExternalMainXrefData LIMIT 1"));
  $includeKW = 1 if $includeUniProt or EXECUTE_FAST_COMMAND("SELECT AC FROM Entry WHERE keywords IS NOT NULL LIMIT 1");

  print $co->h2($name2d) if ($SERVER::ExPASy);
  my $message = 'Search by description (DE), entry name (ID), gene name (GN)'.(($includeKW)? ' or UniProtKB/Swiss-Prot keywords (KW)' : '');
  print $co->h2($message),
        'Enter search keywords:&nbsp;', $co->textfield({ -value=> '', -name=>'search_description'}).p."\n";
  my @values = ('All fields', 'DE', 'ID', 'GN');
  push @values ,'KW' if $includeKW;
  print $co->br.'Limit to: '."\n".
    $co->radio_group
    ( -name=>'search_description_limited', 
      -values=>[@values],
      -default=>'All fields',
    ).p."\n";
  if ($includeUniProt) {
    print br.$co->checkbox (-name=>'include_external_data', -label=>" Include external UniProtKB data in search", -checked=>1).p."\n";
  }
  print $co->br.'Sort by: '."\n".
    $co->radio_group 
    ( -name=>'order_by', 
      -values=>['Accession number', 'Protein ID', 'Gene name'],
      -default=>'Accession number',
    ).$co->p."\n";

  print $co->hr.$co->p."\n";

  print 
         "
         Please enter a keyword. This may be any word or partial word  
         appearing in the entry identifier (ID), the description (DE), the gene names (GN) or a UniProtKB/Swiss-Prot keyword (KW). 
         For example, you may type <em>$default_example_ID_lower</em>, or just
         <em>$default_example_ID_lower_trunc</em>, or <em>$default_example_ID_upper_trunc</em> or <em>$default_example_ID_upper</em>.<P>\n
         If you give more than one keyword, entries 
         having <strong>any</strong> keyword will be listed. Please do <strong>NOT</strong> use any boolean operators (and, or, etc.),
          nor quotes (\").<P>\n";

}


# Search by authors

elsif ($Search_Argument eq 'Author') {

  my @author_list = EXECUTE_FAST_COMMAND("select distinct author from Author order by 1");
  my @group_list  = EXECUTE_FAST_COMMAND("select distinct author from AuthorGroup order by 1");

  print $co->h2($name2d) if ($SERVER::ExPASy);
  print $co->h2('Search by author');

  if ((@author_list or @group_list) or $SERVER::remoteInterface) {
  # hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"Author");
  print $co->hidden('hidden_reference');
  print $co->span({class=>'Result'},'Please, note that there are currently no authors nor author groups inside the selected '.
                                    'local database. Only queries on remote interfaces may give some result!')
    if (!@author_list and !@group_list and !$SERVER::onlyInterfaces) and $SERVER::remoteInterface;

  if (!$co->param('Execute query')) {
    print p.$co->submit({-name=>'Execute query', -label=>'Execute query'}),
    "&nbsp;&nbsp;&nbsp",
    $co->reset,p,p,$co->br;
  }

  print 'Enter an author search keyword:&nbsp;', $co->textfield({ -value=> '', -name=>'search_author'}),"&nbsp;&nbsp;&nbsp;";

  print $co->p;
  if (@group_list) {
    map {$_ =~ s/^(.{30}).{5,}/$1.../} @group_list;
    unshift(@group_list, '-----') if @author_list;
    push @author_list, @group_list;
  }
  my $author_ex_long;
  my @author_list_copy = @author_list; # the following action with $author_ex =~ s/... acts like a grep on @list_copy!
  foreach my $author_ex (@author_list_copy) {
    $author_ex =~ s/^(\w+).+$/$1/;
    $author_ex_long = $author_ex if length($author_ex) > length($author_ex_long) and length($author_ex) > 2;
  }
  undef (@author_list_copy);
  my ($author_ex_upper, $author_ex_lower, $author_ex_trunc_upper, $author_ex_trunc_lower) = ('BAIROCH', 'Bairoch', 'Hochstra', 'hoschstra');
  if ($author_ex_long and $dynamicExamples) {
   ($author_ex_upper, $author_ex_trunc_upper) = (uc($author_ex_long), uc(substr($author_ex_long,0,(length($author_ex_long)-2))));
   ($author_ex_lower, $author_ex_trunc_lower) = (lc($author_ex_upper), lc($author_ex_trunc_upper));
  }
  print '-- you can also limit your search to one of the following authors ',"\n".
    $co->scrolling_list
      (
       'authors_list', 
       ['all authors',@author_list],
       '',
       1,  # number of items that appears at once
       )."\n".$co->p unless $SERVER::onlyInterfaces;
  print 'Limit to entry identifier (', $co->strong("AC"), ' or ', $co->strong("ID"), ') containing:&nbsp;',
  $co->textfield({ -value=> '', -name=>'search_author_byID'}),p;

  print $co->br.'Sort by: '."\n".
    $co->radio_group 
    ( -name=>'order_by', 
      -values=>['Accession number', 'Protein ID'],
      -default=>'Accession number',
    ),p,"\n";

  print $co->hr,p,"\n";

  print 
          "
          Please give an author search keyword. This may be any author name or partial 
          name. For example, you may type <em>$author_ex_lower</em>, or 
          <em>$author_ex_upper</em>, or just <em>$author_ex_trunc_upper</em> or even <em>$author_ex_trunc_lower</em>.<P>\n 
          <H2>List all authors for a given ID or AC </H2>
          If you give an ID or an AC, you will get all authors referenced in the
          given entry. For example, you may type <em>$default_example_ID_lower</em>,
          or <em>$default_example_ID_upper</em> to find out about all authors 
          referenced in the $default_example_ID_upper entry, or just
          <em>TCTP</em> to find out about authors who 
          have been working on the TCTP.<P>\n
          <H2>Search by author and ID or AC</H2>
          If you give an author name (or partial author name) 
          and <strong>then</strong> an AC or ID (or partial AC or ID), you will get all the authors 
          matching the author keyword who have been working on 
          all entries matching the ID or the AC keyword.
          For example, you may type <em>bair</em> and then <em>human</em>.<br>\n
          To force the ID search to match the beginning, use '<em>^</em>'. To force it to match the end, use '<em>\$</em>'. For
          example, use <em>^$default_example_ID_upper_trunc</em> to list proteins ID beginning with $default_example_ID_upper_trunc.
          <BR>To list all entries for a given author, leave the entries field blank.<P>\n";
  } else {
    print $co->span({class=>'Result'},'Sorry, there are currently no authors nor author groups inside this selected database!');
    $SERVER::no_general_submit_button = $SERVER::no_general_reset_button = 1;
  }

  print $co->br;

}


# Search by serial number

elsif ($Search_Argument eq 'Spot') {

# hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"Spot");
  print $co->hidden('hidden_reference');

  print $co->h2($name2d) if ($SERVER::ExPASy);
  print $co->h2($co->span({style=>$font_style}, 'Search by spot ID')),"\n",
        $co->span({style=>$font_style}, 'Enter a spot search keyword:&nbsp;'), $co->textfield({ -value=> '', -name=>'search_spot_number'}),p,"\n";

  #my %labels = ('show_experimental_data' => 'show', 'do_not_show_experimental_data' =>'do not show');
  #print $co->p.$co->a("&nbsp;Experimental data ").
  #      $co->radio_group (
  #       -name=>'search_spot_ExpData', 
  #       -values=>['show_experimental_data','do_not_show_experimental_data'], 
  #       -default=>'show_experimental_data',  
  #       -labels=>\%labels
  #),p;


  #print $co->a('&nbsp;&nbsp;(&nbsp;').
  #      $co->checkbox(-name=>"search_spot_mapping_methods", -checked=>"checked", -label=>"").$co->a('mapping&nbsp;methods&nbsp;&nbsp;').
  #      $co->checkbox(-name=>"search_spot_pmf", -checked=>"checked", -label=>"").$co->a('PMF&nbsp;&nbsp;').
  #      $co->checkbox(-name=>"search_spot_msms", -checked=>"checked", -label=>"").$co->a('MS/MS&nbsp;&nbsp;').
  #      $co->checkbox(-name=>"search_spot_aacid", -checked=>"checked", -label=>"").$co->a('AA&nbsp;composition&nbsp;').
  #      $co->a(')').$co->p.$co->br;

  &DISPLAY_MAP_LIST_SELECTION({databases=>[@CurrentDatabases], remoteInterfaces=>[@Several_Remote_Interfaces], clickedMenu=>$Search_Argument});


  print  $co->span({style=>$font_style}, "Please give a spot ID. For example, you may type A1603 or OV8508.").$co->p;

=pod
  print  " or just <em>$default_example_spot_upper_trunc</em>" if $default_example_spot_upper ne $default_example_spot_upper_trunc;
  print	 ".<br>
         You can list all spots for a <strong>specific map</strong> by leaving the search keyword blank and selecting a map.<P>\n
         <strong>Warning: you may give only one spot ID at a time.</strong><P>\n";
=cut
}


# Search by identification methods

elsif ($Search_Argument eq 'Ident') {

# hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"Ident");
  print $co->hidden('hidden_reference');

  print $co->h2($name2d) if ($SERVER::ExPASy);
  print $co->h2($co->span({style=>$font_style}, 'Search by identification methods')),"\n";

  if ($SERVER::Chart and !$SERVER::onlyInterfaces) {
    my $chartGetURL = $co->url(-base=>1).$main::cgi_2ddb.'/'.$main::gd_chart_viewer.
      "?\&data=msms\&database=$DB::CurrentDatabaseName";
    my $spectraButton = $co->button({ class=>'colored', -value=>'Spectra navigator',
        -onClick=>"window.open(\"$chartGetURL\",'subWind','scrollbars,Height=880,Width=880,dependent')",
        -onMouseOut=>"this.className='colored'",
        -onMouseOver=>"this.className='colored upperCased';window.status='Display PMF and MS/MS peak lists and data';return true"}).
       '&nbsp;&nbsp;'.$co->span({-class=>'small'},'[Click to launch the MS spectra viewer]').$co->br.$co->p."\n";
    print $spectraButton;
  }

  my (@ident_methods_values, %ident_methods_labels);
  if ($SERVER::read_mapping_methods_description_list) {
    my (@mappingTopicDefinition) = EXECUTE_FAST_COMMAND("SELECT mappingTechnique, techniqueDescription FROM MappingTopicDefinition ORDER BY 1");
    while (my $mappingTechnique = shift @mappingTopicDefinition and my $techniqueDescription = shift @mappingTopicDefinition) {
      next if $mappingTechnique eq 'N/A';
      push @ident_methods_values, $mappingTechnique;
      $ident_methods_labels{$mappingTechnique} = "$mappingTechnique ($techniqueDescription)"
    }
  }
  else {
    @ident_methods_values = ('PMF','MS/MS','Aa','Mi','Gm','Co','Im');
    %ident_methods_labels = ('PMF'=>'PMF', 'MS/MS'=>'Tandem MS','Aa'=>'AA Composition', 'Mi' =>'Micro-Sequencing / Tagging',
                             'Gm'=>'Gel Matching', 'Co'=>'Comigration', 'Im'=>'Immunobloting');
  }
  print $co->span({style=>$font_style}, $co->checkbox_group(-name=>'search_ident_methods', -values=>[@ident_methods_values],
                            -labels=>{%ident_methods_labels},
                            -linebreak=>'true', -default=>['PMF','MS/MS'])).$co->p;

=pod
  my %labels = ('OR' => 'any of the selected methods', 'AND' =>'all the selected methods');
  print $co->p.'&nbsp;&nbsp;--&nbsp;Identified&nbsp;by:&nbsp;&nbsp;'.
        $co->radio_group (
         -name=>'search_ident_boolean', 
         -values=>['OR','AND'], 
         -default=>'OR',  
         -labels=>\%labels
        ).$co->p;
  
  print $co->p.'&nbsp;&nbsp;--&nbsp;Group&nbsp;by:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.
        $co->radio_group (
         -name=>'search_ident_groupBy', 
         -values=>['Spot','Accession number'], 
         -default=>'Spot', 
        ).$co->p.$co->br;
=cut
  &DISPLAY_MAP_LIST_SELECTION({databases=>[@CurrentDatabases], remoteInterfaces=>[@Several_Remote_Interfaces], clickedMenu=>$Search_Argument});
  
  print  $co->span({style=>$font_style}, "Please, choose one or several identification methods, then make sure to select the appropriate map.").$co->p;

}


# Search by pI/MW

elsif ($Search_Argument eq 'pI') {

  # hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"pI");
  print $co->hidden('hidden_reference');

  print $co->h2($name2d) if ($SERVER::ExPASy);
  print $co->h2($co->span({style=>$font_style}, 'Search proteins by pI/Mw range')),"\n",
        $co->span({style=>$font_style." font-weight: bold;"}, ('&nbsp;pI Range:')),$co->p,
        $co->span({style=>$font_style}, '&nbsp;&nbsp;min.&nbsp;&nbsp;'), $co->textfield({ -value=> '0', -name=>'pi_min', -size=>4, -maxlength=>4 }),
        $co->span({style=>$font_style}, '&nbsp;max.&nbsp;&nbsp;'), $co->textfield({ -value=> '14', -name=>'pi_max', -size=>4, -maxlength=>4}),
        '&nbsp;&nbsp;'.$co->span({class=>"Comment", style=>$font_style}, '(has no effect on SDS/1-D maps)'),
        $co->p,
        $co->span({style=>$font_style." font-weight: bold;"}, '&nbsp;Mw Range (kDaltons):'), $co->p,
        $co->span({style=>$font_style}, '&nbsp;&nbsp;min.&nbsp;&nbsp;'), $co->textfield({ -value=> '0', -name=>'mw_min', -size=>6, -maxlength=>6}),
        $co->span({style=>$font_style}, '&nbsp;max.&nbsp;&nbsp;'), $co->textfield({ -value=> '250', -name=>'mw_max', -size=>6, -maxlength=>6}), p;

  &DISPLAY_MAP_LIST_SELECTION({databases=>[@CurrentDatabases], remoteInterfaces=>[@Several_Remote_Interfaces], clickedMenu=>$Search_Argument});

  my %labels;
  $labels{'Protein ID'} = ' Protein ID ';
  $labels{'Map'} = ' Map ';
=pod
  print $co->br.'Sort by: '."\n".
    $co->radio_group 
    ( -name=>'order_by', 
      -values=>['Accession number', 'Protein ID'],
      -default=>'Accession number',
      -labels=>\%labels
    ),p,"\n";
  print $co->hr,p,"\n";
=cut
  print $co->span({style=>$font_style}, "Please give a ").
  	$co->span({style=>$font_style." font-weight: bold"}, "pI").
	$co->span({style=>$font_style}, " range for your spots. For example, you may type ").
	$co->span({style=>"font-style: italic;"}, "7.25 or just 7.\n ").
        $co->span({style=>$font_style}, "Give also a range for ").
	$co->span({style=>$font_style." font-weight: bold;"}, "Mw").
	$co->span({style=>$font_style}, " in ").
	$co->span({style=>$font_style." font-weight: bold;"}, "kDa").
	$co->span({style=>$font_style}, ". You may type, for example, ").
	$co->span({style=>"font-style: italic;"}, "90.550 or 110.\n ").
        $co->span({style=>$font_style}, "The search will include all maps, except when you limit your search to a particular map.\n").$co->p;

}


# Search by combined queries

elsif ($Search_Argument eq 'Combined') {

  # hidden fields are sticky! so we have to re-write them this way:
  $co->param(-name=>'hidden_reference', -value=>"Combined");
  print $co->hidden('hidden_reference');

  print $co->h2($name2d) if ($SERVER::ExPASy);
  print $co->h2($co->span({style=>$font_style}, 'Search entries by combining different search keywords'));

  print "Enter keywords in the form below. Keywords may be any word or partial word of at least 2 characters long.\n
         Use of wildcards (\"*\" or  \"?\") is authorized. For each separate field, you can choose to interpret <strong>spaces</strong> as \n
         '<strong><em>or</em></strong>' or '<strong><em>and</em></strong>' operators. When using more than one field, you have the choice to define\n
         '<em>or</em>' or '<em>and</em>' operators releative to the other fields.&nbsp;&nbsp;<br><br>\n\n";

  if ($main::show_external_data and EXECUTE_FAST_COMMAND("SELECT AC FROM ExternalMainXrefData LIMIT 1")) {
    print $co->br.$co->checkbox (-name=>'include_external_data', -label=>" Include external UniProtKB data in search (fields marked with +)",
                                 -checked=>1).$co->p."\n";
  }
  print $co->strong('--&nbsp;Date Fields:&nbsp;').$co->a({href=>"#date"},'[Click here]').'&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
  print $co->strong('--&nbsp;Output Fields:&nbsp;').$co->a({href=>"#output"},'[Click here]').$co->p."\n";
  if (!$co->param('Execute query')) {
    print p.$co->submit({-name=>'Execute query', -label=>'Execute query'}),
    "&nbsp;&nbsp;&nbsp",
    $co->reset,p,p;
  }


  my %labels_fields        = &COMBINED_QUERY({callFor=>'labels_fields'});
  my %labels_output_fields = &COMBINED_QUERY({callFor=>'labels_output_fields'});
  my @labels_fields        = sort(keys(%labels_fields));
  grep { s/^(\d+\.)+// } @labels_fields;
  my @labels_output_fields = sort(keys(%labels_output_fields));
  my %labels_and_or;
  $labels_and_or{'AND'}    = ' AND ';
  $labels_and_or{'OR'}     = ' OR ';

  my (@rows, $last_row, $field_number);

 # print $co->br.$co->p."\n";

  # Define here number of Blocks, and number of fields by block! Fixed to 2 x 2 by default

  $co->param(-name=>'CombinedMax', -value=>"0") unless $co->param('CombinedMax');
  print $co->hidden('CombinedMax');

  my ($more_fields_value, $more_fields_help, $more_fields_CombinedMax);
  if ($co->param('CombinedMax')) {
    $more_fields_value = "Reduce input fields";
    $more_fields_help = "Restore default input fields number";
    $more_fields_CombinedMax = 0;
  }
  else {
    $more_fields_value = "Extend input Fields";
    $more_fields_help = "Get more input fields";
    $more_fields_CombinedMax = 1;
  }
  my $more_fields_button =
        $co->input({-type=>"button", -name=>"more fields", value=>"$more_fields_value",
                    -onMouseOver=>"window.status='$more_fields_help';return true",
                    -onMouseUp=>"javascript:document.$formName.CombinedMax.value='$more_fields_CombinedMax';ClickMenu('Combined','$script_name')"}
                   );

  my $blocks_number = ($co->param('CombinedMax'))? 4 : 2;
  my $fields_per_block = ($co->param('CombinedMax'))? 2 : 2;
  my @labels_fields_defaults = ($labels_fields[0],$labels_fields[2],$labels_fields[3],$labels_fields[4],
                                $labels_fields[5],$labels_fields[6],$labels_fields[8],$labels_fields[19]);

  for (my $i_block = 1; $i_block <= $blocks_number; $i_block++) {

    my $block_bgcolor = ($i_block % 2)? '#f0e68c':'#f8f8ff';
    my $last_row_table;

    for (my $i_field = 1; $i_field <= $fields_per_block; $i_field++) {  

      $field_number++;

      $last_row_table.=  $co->p.$co->strong("Field $field_number");
      $last_row_table.= '&nbsp;&nbsp;Interpret spaces between keywords as:&nbsp;'."\n".
                   $co->radio_group 
                   ( -name=>'inner_and_or_field'.$field_number,
                     -values=>['OR','AND','adjacent'],
                     -default=>'OR',
                     -labels=>\%labels_and_or
                    ).$co->br.$co->br.'&nbsp;'."\n";
      $last_row_table.=  $co->scrolling_list 
                   ( -name=>'combined_field'.$field_number,
                     -values=>[@labels_fields],
                     -default=>$labels_fields_defaults[$field_number-1],
                     -size=>1,
                    )."\n";
      $last_row_table.=  'field contains:&nbsp;';
      $last_row_table.=  $co->textfield({ -name=>"include_field$field_number", -value=> ''})."\n";
      $last_row_table.=  ' and not:&nbsp;';
      $last_row_table.=  $co->textfield({ -name=>"exclude_field$field_number", -value=> '', -size=>15}).'&nbsp;'.$co->br."\n";

      if ($i_field < $fields_per_block) {
        $last_row_table.=  $co->p.'----------------'."\n".
                     $co->radio_group 
                     ( -name=>'outer_and_or_field'.$field_number.($field_number+1), 
                       -values=>['OR','AND'],
                       -default=>'OR',
                       -labels=>\%labels_and_or
                      ).'----------------'."\n";
      }
      else {
        $last_row_table = $co->table({-bgcolor=>"$block_bgcolor", -border=>1 },
              $co->Tr({-align=>'center', -valign=>'middle'},$co->td($last_row_table))
              )."\n";
        push (@rows, td({-nowrap}, $last_row_table)."\n");
        last;
      }

    } # end i_field

    if ($i_block < $blocks_number) {

      $last_row =  $co->p.'============================'."\n".
                   $co->radio_group 
                   ( -name=>'outer_and_or_block'.$i_block.($i_block+1), 
                     -values=>['OR','AND'],
                     -default=>'OR',
                     -labels=>\%labels_and_or
                    ).'============================'."\n";
      push (@rows, td({-nowrap}, $last_row)."\n");
      $last_row =  $co->p; push (@rows, td({-nowrap}, $last_row)."\n");
    }

  } # end i_block

  $last_row =  $co->br.$more_fields_button;

  $last_row.=  $co->p.$co->hr.$co->p;
  $last_row.=  $co->a({name=>'date'},"").$co->strong('Date Field').$co->br."\n";
  $last_row.=  $co->span({class=>'smallComment'},"(Use exact date format, e.g. 31-1-2005 or 31-Jan-2005 or 31/1/2005)").$co->br.
               $co->span({class=>'smallComment'},"(You may also specifiy a month/year, or simplay a year, e.g. Dec/2005 or 2005)").$co->p."\n";
  $last_row.=  $co->scrolling_list
               ( -name=>'combined_date',
                 -values=>['Creation Date', 'Last 2D modification', 'Last general modification'],
                 -default=>'Creation Date',
                 -size=>1,
                )."\n";
  $last_row.=  'Since:&nbsp;';
  $last_row.=  $co->textfield({ -name=>'date_since', -value=> '', -size=>18})."\n";
  $last_row.=  '&nbsp;Before:&nbsp;';
  $last_row.=  $co->textfield({ -name=>'date_before', -value=> '', -size=>18})."\n";

  push (@rows, td({-nowrap}, $last_row)."\n");

  print  $co->table({-align=>'center', -border=>0, -width=>'90%'},
              TR({-align=>'center', -valign=>'middle'},\@rows)
              ),"\n";


  print hr,p,"\n";

  print $co->p({class=>"center"},
        $co->a({name=>'output'},""),
        $co->strong('Output Fields: '),' (',$co->strong($co->em('Accession numbers')),' and ',$co->strong($co->em('IDs')), ' are displayed by default)',$co->br,"\n",
        $co->span({class=>'smallComment'}, "(hold on 'CTRL' key for multiple selections)"),$co->br,"&nbsp;",$co->br,"\n",
        $co->scrolling_list 
         ( -name=> 'combined_output_fields',
          -values=>[@labels_output_fields],
          -defaults=>['Description'],
          -size=>5,
          -multiple=>'true'
         ),"\n",
        ),"\n";


  print hr,p,"\n";
  print &SAVE_FILE_FORM('Combined');

}


# Search and List proteins in a map

elsif ($Search_Argument eq 'Map' or $Search_Argument eq 'MapExpInfo') {

  my $onlyExpInfo = ($Search_Argument eq 'MapExpInfo')? 1 : 0;

  $SERVER::no_general_submit_button = 1;

  # hidden fields are sticky! so we have to re-write them this way:
  my $hiddenReferenceValue = ($onlyExpInfo)? 'MapExpInfo' : 'Map';
  $co->param(-name=>'hidden_reference', -value=>"$hiddenReferenceValue");
  print $co->hidden('hidden_reference');

  if ($Search_Argument eq 'MapExpInfo') {
    print $co->h2($name2d) if ($SERVER::ExPASy);
    print $co->h2($co->span({style=>$font_style}, 'Display experimental information for a reference map')),"\n",
          $co->span({style=>$font_style." font-weight: bold;"}, "'$mapExpInfoMenu'"),"\n",
	  $co->span({style=>$font_style}, ' allows you to display '),
	  $co->span({style=>$font_style." font-weight: bold;"}, "experimental information"),
          $co->span({style=>$font_style}, ' for a given map.')."\n";
  }
  else {
    print $co->h2($name2d) if ($SERVER::ExPASy);
    print $co->h2('Retrieve protein list for a reference map'),"\n",
          $co->strong("'$mapProteinListMenu'"),"\n",' allows you to list in a table all the '.
          $co->strong('protein entries').' identified for a given reference map, with related 2-DE information on spot ID, experimental '.
          $co->em('pI').' and '.$co->em('Mw').' (read from gel), mapping procedures, references, etc..',"\n";
  }

  print $co->br.$co->br.$co->a({href=>"$sample_origin_URL", target=>"_blank"},
    "Sample Preparation and Post-separation Analysis")."\n"
#    ." (the sample data is in raw format for the moment, until the 'Proteomics Standardisation Project' is finalised)"
    if $sample_origin_URL;

  print $co->p."\n";

  unless ($onlyExpInfo) {  
    my %labels;
    $labels{'Protein ID'} = ' Protein ID ';
    $labels{'Accession number'} = ' Accession number ';
    $labels{'gene'} = ' Gene name ';
    print 'Sort by: '."\n".
      $co->radio_group
      (
        -name=>'order_by', 
        -values=>['Accession number', 'Protein ID', 'Gene name'],
        -default=>'Accession number',  
        -labels=>\%labels
      ).$co->p."\n";
    print $co->checkbox (-name=>'spot_volume', -label=>" Include an estimation for the spots\' expression intensity (in volume (ppm) and %od)");
  }

  print $co->p.$co->span({style=>$font_style." font-weight: bold;"}, 'Choose&nbsp;a&nbsp;map&nbsp;(or&nbsp;click&nbsp;on&nbsp;the&nbsp;images&nbsp;below):').$co->p."\n";

  &DISPLAY_MAP_LIST_SELECTION({databases=>[@CurrentDatabases], noAll=>1, remoteInterfaces=>[@Several_Remote_Interfaces], clickedMenu=>$Search_Argument});

  undef($SERVER::no_general_submit_button);

  if (scalar @CurrentDatabases > 1) {
    foreach my $eachdb (@CurrentDatabases) {
      $main::displayedAfter.= &DISPLAY_MAP_TABLE_SELECTION({database=>$eachdb, submitName=>'choose_map_img', doNotPrint=>1}); 
    }
  }
  else {
    my @all_maps = &all_maps_full_name({species=>1});
    $main::displayedAfter.=  &DISPLAY_MAP_TABLE_SELECTION({maps=>\@all_maps, submitName=>'choose_map_img', doNotPrint=>1});
  }

  print &SAVE_FILE_FORM('List_Map').$co->p unless $onlyExpInfo;

}


elsif ($co->param('Submit Divers')) {

  if ($co->param('Submit Divers') eq 'Private data') {
    my $privateCookie = _crypting_string_($co->cookie("2d_".$name2dAlphaNumeric."_private_password"), 1,0);
    undef($privateCookie) unless $privateCookie eq $private_data_password;
    if ($privateCookie) {
      print $co->strong({class=>'red'}, 'You are currently allowed to access private data!' ).$co->p."\n";
      print $co->submit({-name=>'Submit Divers', -label =>'Desactivate Private access'})."\n";
      $SERVER::no_general_reset_button = 1;
    }
    else {
      print $co->strong({class=>'red'}, 'You are currently not allowed to access private data!' ).$co->p."\n";
      print 'Please, enter the required password to access private data &nbsp;&nbsp;'.
        $co->password_field (-name=>'private_password', -default=>'', -size=>8).$co->p."\n";
      print $co->submit({-name=>'Submit Divers', -label =>'Submit Private password'})."\n";
    }
    $SERVER::no_general_submit_button = 1;
  }

  elsif ($co->param('Submit Divers') eq 'Submit Private password') {
    my $submittedPrivatePasswordStatus = ($userPrivatePassword)? 'Correct password to access private data.'
       : 'Uncorrect password to access private data!';
    print $co->strong({class=>'red'}, $submittedPrivatePasswordStatus).$co->p."\n";
    print $string = ($userPrivatePassword)? 'Your search queries will now include data marked hidden for public access.'
       :  ($email)? 'Please, contact '.$co->a({href=>"mailto:$email"}, $email).' for more information.' : '';
    $SERVER::no_general_submit_button = $SERVER::no_general_reset_button = 1;
  }
  elsif ($co->param('Submit Divers') eq 'Desactivate Private access') {
    print $co->strong({class=>'red'}, 'Access to private data has been desactivated!').$co->p."\n";
    $SERVER::no_general_submit_button = $SERVER::no_general_reset_button = 1;
  }
}

# MAIN (Home) PAGE & General Command

else {

  $home_page = 1;

  my ($mssg1, $mssg2) = ($SERVER::core_selector)? ('adminstrator', 'update') : ('main', 'query');
=pod
  print
  $co->em({class=>"Comment"}, "This is the $mssg1 interface of the " .$co->a({-href=>"$main::expasy/ch2d/make2ddb/"}, "Make2D-DB II tool").
  " to $mssg2 $name2d.").$co->br."\n" if $SERVER::displayToolComments;

  print $co->h1("$name2d");
=cut

  my $subTitleFileBig = 0;
  unless ($SERVER::core_selector) {
    if ($title_comment) {
#      print $co->strong("$title_comment").$co->br.$co->br."\n";
    }
    $subTitleFile =  (-s "$tmp_path/subtitle.html")? "$tmp_path/subtitle.html" :
     (-s "$html_path/$web_server_ref_name/data/subtitle.original.html")? "$html_path/$web_server_ref_name/data/subtitle.original.html"
       : undef;

    if ($subTitleFile) {
=pod
      my $FILE = new IO::File;
      $FILE->open("<$subTitleFile");
      print (<$FILE>);
      print $co->br.$co->br."\n";
      $FILE->close;
      print "\n";
      $subTitleFileBig = 1 if -s $subTitleFile > 80;
=cut
      print $co->div( 
        $co->span({-style=>"font-weight: bold; font-family: sans-serif, font-size: 10pt;"}, "CIPRO 2D-PAGE DB").
    	$co->span({-style=>$font_style}, " provides graphic profiles of protein expression with 2D gels in ").
	$co->span({-style=>"font-style: italic; font-size: 10pt;"}, "Ciona").
	$co->span({-style=>$font_style}, " embryo at several developmental stages and adult tissues. Each spot directly links to basic protein information, gene expression profiles, 3D images of embryos, protein localization and transgenic resources.")).
	$co->br;
    }

  }

  if ($SERVER::core_selector) {
    print $co->strong({class=>'red'}, "You are accessing the inner (core) data schema. Your login ID and password are required to perform any operation!")."&nbsp;&nbsp;\n";
    print $co->submit({class=>'smallComment', -name=>'logout_submit', -value=>'logout',})."\n";
  }

  else {

    my $expandFactSubSection = 'Expand: Facts and statistics';
    unless ($SERVER::onlyInterfaces) {
      my $databasesFactButtons; my $keepFactsOpened = 0;
      if (1) {
        foreach my $databaseNum (@main::DATABASES_Included) {
          my $databaseNam = $main::database[$databaseNum]{database_name};
          $databaseNum = $main::default_dbname unless $databaseNum;
          my $expandFactSubSectionBis = 'Expand: Facts and statistics('.$databaseNum.')';
          my ($expandFactSignBis, $buttonFactClassBis) = ($co->param($expandFactSubSectionBis) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
          $databasesFactButtons.= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.
            $co->submit({-class=>$buttonFactClassBis, -name=>$expandFactSubSectionBis, -label => $expandFactSignBis }).
            "&nbsp;&nbsp;&nbsp".$co->strong("$databaseNam").$co->p if $main::multi_DBs;
          if ( $co->param('Expand: Facts and statistics('.$databaseNum.')') eq '+'
               or (!$main::multi_DBs and $co->param($expandFactSubSection) eq '+') ) {
            $DB::CurrentDatabase = $databaseNum;
            $command = "SELECT databaseName, databaseDescription, databaseRelease, databaseSubRelease, to_char(databaseReleaseDate, 'DD Month YYYY'), ".
                       "databaseReleaseNotes, databaseMainSummary, copyright, contact FROM common.Database LIMIT 1";
            my ($databaseName, $databaseDescription, $databaseRelease, $databaseSubRelease, $databaseReleaseDate,
                $databaseReleaseNotes, $databaseMainSummary, $copyright, $contact) = EXECUTE_FAST_COMMAND("$command");
            my $DBfacts = '';
            $DBfacts = $co->start_ul();
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Database&nbsp;Public&nbsp;Name:&nbsp;').$co->strong("$databaseName"));
            $databaseReleaseDate = "$3/$2/$1" if ($databaseReleaseDate =~ /^(\d+)\-(\d+)\-(\d+)$/);
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Description:&nbsp;').$databaseDescription) if $databaseDescription;
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Current&nbsp;Release:&nbsp;').
                         $co->strong("$databaseRelease.$databaseSubRelease").
                       '&nbsp;('.$co->strong($databaseReleaseDate).')');
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Release&nbsp;Notes:&nbsp;').$databaseReleaseNotes) if $databaseReleaseNotes;
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Copyright:&nbsp;').$copyright) if $copyright;
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Contact:&nbsp;').$contact) if $contact;
            $DBfacts.= $co->end_ul.$co->start_ul;
            $databaseMainSummary = $co->em({-class=>'gray'}, '[No statistics have been prepared. Please, contact the database administrator to generate automatic database statistcs!]') unless $databaseMainSummary;
            $DBfacts.= $co->li($co->em({-class=>'brown'}, 'Current&nbsp;Content&nbsp;and&nbsp;Statistics:&nbsp;').$databaseMainSummary);
            my $factsClass = ($main::multi_DBs)? 'leftBorder2' : '';
            $DBfacts = $co->div({-class=>$factsClass}, $DBfacts)."\n";
            $databasesFactButtons.= $DBfacts.$co->p."\n";
            $keepFactsOpened = 1;
            undef($databaseMainSummary); undef($databaseReleaseNotes);
          }
        }
        my ($expandFactSign, $buttonFactClass) = ($co->param($expandFactSubSection) eq '+' or $keepFactsOpened)? ('-', 'expandableOff') : ('+', 'expandableOn');
=pod	
        print $co->submit({-class=>$buttonFactClass, -name=>$expandFactSubSection, -label => $expandFactSign })."&nbsp;&nbsp;&nbsp".
              $co->strong('[Facts and statistics]').$co->p;
        print $databasesFactButtons.$co->br if ($co->param($expandFactSubSection) eq '+' or $keepFactsOpened);
        $co->param(-name=>'keepFactsOpened', -values=>[$keepFactsOpened]);
        print $co->hidden('keepFactsOpened')."\n";
=cut
      }

    }

    $expandSubSection = 'Expand: Help';
    if ($subTitleFileBig) {
     ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
=pod
      print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
            $co->strong('[How to use this interface]').$co->p;
=cut
    }
    if (!$subTitleFileBig or $co->param($expandSubSection) eq '+') {
      if ($SERVER::onlyInterfaces) {
        my $message = 'This is a Web portal to remote World-Wide 2D-PAGE databases. '.
        'To gain full access to any of the listed Make2D-DB II remote interfaces/databases, click on its name from the '.
        '\'Remote Interfaces\' list.';
        print $co->strong($message).
        $co->br.$co->br;
      }
=pod      
      print
      $co->strong($co->em({class=>"underlined"},"Search by")).$co->br."You can perform simple search queries:".$co->br."\n".
      $co->start_ul."\n".
      $co->li( $co->strong("by accession number").": search $name2d by accession number or by entry name (AC or ID)")."\n".
      $co->li( $co->strong("by description, ID or gene name").": search by description (DE), entry name (ID), gene name (GN) or UniProtKB/Swiss-Prot keywords (KW)")."\n".
      $co->li( $co->strong("by author").": search authors list by giving an AC or an ID, or list all entries for a given author")."\n".
      $co->li( $co->strong("by spot id / serial number").": search by spot ID / serial numbers")."\n".
      $co->li( $co->strong("by identification methods").": search by identification methods")."\n".
      $co->li( $co->strong("by pI/Mw range").": search for spots located within a pI/Mw range. You can limit your search to a specific map")."\n".
      $co->end_ul."\n".
      "You can also perform much more complex queries using the ".$co->strong("'combined fields'")." link."."\n".
             "This option lets you construct your own query by combining various keywords and boolean operators (SRS-like interface)."."\n".
             "You can choose the output fields to show and decide to save results as a text file on your own computer.".$co->br.$co->br."\n".
      $co->strong($co->em({class=>"underlined"},"Maps")).$co->br."Several actions can be performed for the maps:".$co->br."\n".
      $co->start_ul."\n".
      $co->li($co->strong("'$mapExpInfoMenu'")." allows you to display ".$co->strong("experimental information")."\n".
                    " for a given map.")."\n".
      $co->li($co->strong("'$mapProteinListMenu'")." allows you to list in a table all the ".
                    $co->strong("protein entries")." identified for a given reference map, with related"."\n".
                    " 2-DE information on spot ID, experimental pI and Mw (read from map), mapping procedures, references, etc.."."\n".
                    " You can export the table in text format to your computer.")."\n".
      $co->li($co->strong("'$graphicalViewerName'")." is the viewer for the maps. ".
             "This viewer lets you navigate graphically and zoom between the different maps. ".
             "You can display all spots or just show those of particular interest."."\n".
             "By dragging your mouse over the different spots, available experimental information will be dynamically displayed.".
       (($SERVER::onlyInterfaces)? ' '.$co->strong({class=>'underlined'}, "Warning!").' with this portal interface without local data, this feature is disabled at the root level. '.
       'You should perform \'Maps\' queries, or click directly on the interface of interest (from the Remote Interfaces\' list) to access remotely its '.$graphicalViewerName.'. You may also click on any listed spot to display its location on the map.' : ''))."\n".
=cut
      $co->end_ul."\n";

      if ($main::multi_DBs or $SERVER::remoteInterfaceDefined) {
#        print $co->br.$co->hr;
        my $also = undef;
        if ($main::multi_DBs and !$SERVER::onlyInterfaces) {
=pod
          print
                 $co->strong($co->em({class=>"underlined"},"Databases")." (or distinct projects)").$co->br."\n".
                 "This interface has been configured to access ".$co->strong("several databases (distinct projects)").".".
                 " You may select one individual database, or select, simultaneously, several databases for your queries.".
                 " To select more than one database, use the 'Ctrl' button while clicking on the database name.".$co->p."\n";
=cut
          $also = ' also';
        }
        if ($SERVER::remoteInterfaceDefined) {
=pod
          print
                 $co->strong($co->em({class=>"underlined"},"Query Remote Interfaces")).$co->br."\n".
                 "This interface has$also been configured to automatically access other similar ".$co->strong("remote interfaces").".".
                 " You may select one or several interfaces for your queries.".
                 " Please, note that there are some few limitations on queries with remote interfaces".
                 " (e.g. the 'graphical interface' does not directly access remote databases).".$co->br."\n";
          unless ($SERVER::onlyInterfaces) {
            print
                   "Note also that, by default, there is at least one selected 'local' database.".
                   " If you want to exclude 'local' databases from your queries (restricting queries to remote interfaces),".
                   " turn on the '".$co->em("Exclude local DBs")."' box.".$co->br."\n";
          }
          print
                   "Finally, you should be aware that selecting many remote databases simultaneously".
                   " will cause a slow down of your queries' response.".$co->p."\n";
=cut
        }
      }
    }
  }

  my @stages = ('EGG', '16CELL', 'GASTRULA', 'NEURULA', 'LARVA');
  my @tissues = ('OVARY', 'NC', 'TS');
  my @pIs = ('4-7', '6-11');
  my ($stage_content, $tissue_content);

  my $stage_title = Tr(th({align=>"center", width=>"20%"}, span({-style=>$font_style}, 'Stage')), 
  			th({align=>"center"}, span({-style=>$font_style}, 'pI 4-7')), 
			th({align=>"center"}, span({-style=>$font_style}, 'pI 6-11')));
  my $tissue_title = Tr(th({align=>"center", width=>"20%"}, span({-style=>$font_style}, 'Tissue')), 
  			th({align=>"center"}, span({-style=>$font_style}, 'pI 4-7')), 
			th({align=>"center"}, span({-style=>$font_style}, 'pI 6-11')));

  for my $stage (@stages) {
    my $col_elms = th({align=>"center"}, span({-style=>$font_style}, $stage));
    for my $pI (@pIs){
      $col_elms .= td({align=>"center"}, $co->a({href=>"2d_view_map.cgi?map=".$stage.$pI."&ac=all spots"}, img({src=>'../../html/2d/data/small-gifs/sds/'.$stage.$pI.'.png', alt=>$stage.$pI, width => 50, height => 60})));
    }
    $stage_content .= Tr($col_elms);
  }

  for my $tissue (@tissues) {
    my $col_elms = th({align=>"center"}, span({-style=>$font_style}, $tissue));
    for my $pI (@pIs){
      $col_elms .= td({align=>"center"}, $co->a({href=>"2d_view_map.cgi?map=".$tissue.$pI."&ac=all spots"}, img({src=>'../../html/2d/data/small-gifs/sds/'.$tissue.$pI.'.png', alt=>$tissue.$pI, width => 50, height => 60})));
    }
    $tissue_content .= Tr($col_elms);
  }
  
  print $co->table({style => "position: absolute; left: 280px; top: 40px;", width => 400, cellpadding => 20}, 
  		Tr(td({valign => "top"}, $co->table({width => 200}, 
			Tr(td({colspan => 3, align => "center"}, 
				img({src => "sds1.jpg", alt => "Developmental Stages", width => 210, height => 35}))),
			$stage_title, $stage_content)),
		   td({valign => "top"}, $co->table({width => 200}, 
		   	Tr(td({colspan => 3, align => "center"},
				img({src => "sds2.jpg", alt => "Adult Tissues", width => 140, height => 35})),
			$tissue_title, $tissue_content)))));

#  print $co->table({align=>"center", width=>500, cellpadding=>5}, Tr(td({colspan=>3, align=>"center"}, img({src=>"sds1.jpg", alt=>"Developmental Stages", width=>420, height=>70}))), 
#  $stage_title, $stage_content);
#  print $co->table({align=>"center", width=>500, cellpadding=>5}, Tr(td({colspan=>3, align=>"center"}, img({src=>"sds2.jpg", alt=>"Adult Tissues", width=>280, height=>70}))), 
#  $tissue_title, $tissue_content);
      print 
      $co->div({style => "position: absolute; top: 500px; font-family: sans-serif; font-size: 10pt;"}, 
      $co->strong($co->em({class=>"underlined"},"Search by")).$co->br."You can perform simple search queries:".$co->br."\n".
      $co->start_ul."\n".
      $co->li( $co->strong("CIPRO ID").": search by CIPRO ID")."\n".
      $co->li( $co->strong("by spot ID").": search by spot ID")."\n".
      $co->li( $co->strong("by identification methods").": search by identification methods")."\n".
      $co->li( $co->strong("by pI/Mw range").": search for spots located within a pI/Mw range. You can limit your search to a specific map")."\n".
      $co->end_ul."\n".
      "You can also perform much more complex queries using the ".$co->strong("'combined fields'")." link."."\n".
             "This option lets you construct your own query by combining various keywords and boolean operators (SRS-like interface)."."\n".
             "You can choose the output fields to show and decide to save results as a text file on your own computer.".$co->br.$co->br."\n".
      $co->strong($co->em({class=>"underlined"},"Maps")).$co->br."Several actions can be performed for the maps:".$co->br."\n".
      $co->start_ul."\n".
      $co->li($co->strong("'$mapExpInfoMenu'")." allows you to display ".$co->strong("experimental information")."\n".
                    " for a given map.")."\n".
      $co->li($co->strong("'$mapProteinListMenu'")." allows you to list in a table all the ".
                    $co->strong("protein entries")." identified for a given reference map, with related"."\n".
                    " 2-DE information on spot ID, experimental pI and Mw (read from map), mapping procedures, references, etc.."."\n".
                    " You can export the table in text format to your computer.")."\n");
}




# End All Forms by an EXECEUTE

if ( (!$co->param('Execute query') and !$home_page) or ($SERVER::core_selector and $home_page) ) {

  # More btach commands: activated only for core

  &CORE_FormBatchCommand if ($SERVER::core_selector and $home_page);

  # Free pgSQL command: activated only for core

  &CORE_FormFreeCommand if ($SERVER::core_selector and $home_page);

  # General submission for all Forms

  print $co->submit({-name=>'Execute query', -label => 'Execute query' }).
    "&nbsp;&nbsp;&nbsp" unless $SERVER::no_general_submit_button;
  print $co->reset.$co->br."\n" unless $SERVER::no_general_reset_button;

  print $co->br.$main::displayedAfter if $main::displayedAfter;

  print $co->hr."\n";


}



#============================================================================================#


# End Document with footer #

# if ($_DEBUG_::ON) {foreach my $env (keys %ENV) {$_DEBUG_::message .= "* $env=> ".$ENV{$env}."<br>"};}
# my @prms = $co->param(); foreach my $prm (sort @prms) {print "-- $prm -> ".$co->param($prm).$co->br;}

print $_DEBUG_::message;
undef($_DEBUG_::message);

my $script_argument = ($hidden_reference)? "?$Search_PARAM_values{$hidden_reference}":'';
my $name2d_level = $name2d;
$name2d_level .= " ($hidden_reference)" if $hidden_reference;
print $co->p;
print $co->hr if $hidden_reference ; #and !$SERVER::ExPASy;

if ($home_page) {
  my $endTable;
#  print $co->hr;
  # Additional submission form (Private data management at document end)
  if ($private_data_password and !$SERVER::core_selector and !$SERVER::onlyInterfaces) {
    $endTable.= $co->start_Tr."\n";
    $endTable.= $co->td($co->submit({-class=> 'colored', -name=>'Submit Divers', -label =>'Private data'}))."\n";
    $endTable.= $co->td($co->span({class=>'smallComment'}, '&nbsp;&nbsp;--&nbsp;Check your access to private data'))."\n";
    $endTable.= $co->end_Tr."\n";
  }
  # Go to the core interface
=pod
  if ($coreInterfacePrefix and !$SERVER::core_selector and $SERVER::showAdminLoginButton and !$SERVER::onlyInterfaces) {
   (my $coreInterfaceWebAddress = $SERVER::interfaceWebAddress) =~ s/(\.\w+)$/\.$coreInterfacePrefix$1/;
    $endTable.= $co->start_Tr."\n";
    $endTable.= $co->td($co->button({ -class=>'colored', -value=>'Admin login',
         -onClick=>"window.location.href='$coreInterfaceWebAddress'", 
         -onMouseOver=>"window.status='Login to the administration interface';return true"}))."\n";
    $endTable.= $co->td($co->span({class=>'smallComment'}, '&nbsp;&nbsp;--&nbsp;Login for administrators'))."\n";
    $endTable.= $co->end_Tr."\n";
  }
=cut
  # Go to the public interface
  if ($SERVER::core_selector) {
   (my $mainInterfaceWebAddress = $SERVER::interfaceWebAddress) =~ s/\.$coreInterfacePrefix(\.\w+)$/$1/;
    $endTable.= $co->start_Tr."\n";
    $endTable.= $co->td($co->button({ -class=>'colored', -value=>'Public interface',
         -onClick=>"window.location.href='$mainInterfaceWebAddress'", 
         -onMouseOver=>"window.status='Return to the public interface';return true"}))."\n";
    $endTable.= $co->td($co->span({class=>'smallComment'}, '&nbsp;&nbsp;--&nbsp;Back to the public interfave'))."\n";
    $endTable.= $co->end_Tr."\n";
  }
  print $co->p.$co->start_table."\n".$endTable.$co->end_table."\n" if ($endTable);
}

=pod
print $string = ($db_server_logo and !$SERVER::core_selector and !$SERVER::ExPASy)?
      $co->p({class=>"center"}, $co->a({href=>"$script_name$script_argument", -class=>'Transparent'},$co->img({-src=>"$back_logo", -width=>'100', -border=>0, -alt=>"$name2d image"})))
      : (!$SERVER::ExPASy)?
      $co->p({class=>"center"}, $co->a({href=>"$script_name$script_argument", -class=>'Transparent'},$co->img({-src=>"$back_logo", -border=>0, -alt=>"$name2d image"}))).
      $co->h3("$name2d_level") : '';
=cut

print "\n".$co->end_td.$co->end_Tr."\n";
print $co->end_table."\n";


# End Form
print $co->end_form."\n";

if (-s "$main::html_path/$main::web_server_ref_name") {
  $link_version_to_readme = $co->a({href=>"$url_www2d/data/1.Readme_main.html", target=>"_blank"}, "$link_version_to_readme");
}

my $add_maintainer = ($main::maintainer && $email)? " and maintained by ".$co->a({href=>"mailto:$email"}, $main::maintainer).","
                   : ($main::maintainer)? " and maintained by $main::maintainer," : undef;

=pod
print 
      $co->hr.$co->h3("Gateways to other related servers")."\n".
      $co->start_ul."\n".
      $co->li($co->a({href=>"http://www.expasy.org/ch2d/"}, "SWISS-2DPAGE")." - The two-dimensional polyacrylamide gel electrophoresis database of the ".
              $co->a({href=>"http://www.isb-sib.ch/"}, "Swiss Institute of Bioinformatics")." in Geneva")."\n".
      $co->li($co->a({href=>"http://www.expasy.org/ch2d/2d-index.html"}, "WORLD-2DPAGE")." - Index to other Federated 2-D PAGE databases")."\n".
      $co->li($co->a({href=>"http://www.expasy.org/"}, "ExPASy")." - The proteomics web server of the ".
              $co->a({href=>"http://www.isb-sib.ch/"}, "Swiss Institute of Bioinformatics").", in Geneva")."\n".
      $co->end_ul."\n"
  if $SERVER::displayToolComments > 1;

my $databaseOrProtalMssg = ($SERVER::onlyInterfaces)? 'portal' : 'database';
print
      $co->hr.
      $co->em("This $databaseOrProtalMssg was constructed$add_maintainer using the ".$co->a({href=>"http://www.expasy.org/ch2d/make2ddb/"}, "Make2D-DB II")."\n".
              " package (".$link_version_to_readme.") ".
              "from the ".$co->a({href=>"http://www.expasy.org/ch2d/2d-index.html"}, "WORLD-2DPAGE").
              " of the ".$co->a({href=>"http://www.expasy.org/"}, "ExPASy web server"))."\n".
      (($home_page and $SERVER::displayToolComments > 1)? "&nbsp;&nbsp;"._html_valid_icon_() : '') 
  if $SERVER::displayToolComments;

print $co->hr.$co->hr.$co->h4({class=>"center"}, "[".$co->a({href=>"$main::home_url"}, "$main::home_displayed_text")."]").$co->br."\n"
  if $SERVER::displayToolComments and $main::home_url and !$SERVER::ExPASy;
=cut

if ($SERVER::ExPASy) {
  FIN({'swiss-2dpage'=>1, redirectFileTo=>'/swiss-2dpage/'});
  &print_end_html;
}
else {
  print $co->end_html;
}

exit 0;


##======================================================================##
##======================================================================##
##======================================================================##

sub IntoCenteredColoredCell {
  my $text = $_[0];
  my $color = ($_[1])? $_[1] : '#f0e68c';
  my $table_align = ($_[2])? $_[2] : 'center';
  my $co = new CGI;
  my $return_text =
    $co->start_table({-cellpadding=>'4px', -align=>"$table_align", -bgcolor=>"$color", -border=>2}).$co->start_Tr({-align=>'center'}).
    $co->start_td(-nowrap).$text.$co->end_td.
    $co->end_Tr.$co->end_table.$co->br."\n";

}

##======================================================================##
##======================================================================##
##======================================================================##


# SEARCH ARGUMENT IS NOT OK

sub SEARCH_ARGUMENT_NOT_OK {

  my $co = new CGI;
  my $argument = $co->strong($_[0]);
     $argument = $co->strong('\'void argument\'') unless defined($_[0]);
  my $note     = $_[1];
  my $message;

  my $this_database = $DB::CurrentDatabaseName || 'this database';

  if ($note eq 'no result') {
    $message = $co->span({class=>'Result'}, "Sorry, your query gives no result. Please, try again!");
  }
  elsif ($note eq 'bad map name') {
    $message = "There is actually no map name called '$argument' within $this_database, please check!";
  }
  elsif ($note eq 'bad map pattern') {
    $message = "There is actually no maps matching '$argument' within $this_database.<br>";
    $message.= "Please, make sure your argument is a valid single word with no special characters nor spaces!"
      unless $_[0] =~ /^\s*\w+\s*$/;
  }
  elsif ($note eq 'bad spot name') {
    $message = "Your search argument for spot number ($argument) is not a valid argument.<br>Please, check that it does not conatin ".
               "inner spaces and that it is at least 1 character long!";
  }
  elsif ($note eq 'bad description') {
    $message = "Your search argument for description ($argument) is not a valid argument.<br>Please, check that it is at least two characters long!";
  }
  elsif ($note eq 'bad_combined_keywords') {
    $message = "Sorry, your search keywords are not valid <br>Please, check that they are at least two characters long!";
  }
  elsif ($note eq 'bad_combined_digits') {
    $message = "Sorry, your search keywords for TaxID or PubMed/Medline are not valid.<br>Please, type only digits for those fields!";
  }
  elsif ($note eq 'bad_combined_date') {
    $message = "Sorry, your 'DATE' fields are not in correct format. Use standard date format, e.g: 31/10/2002!";
  }
  elsif ($note eq 'bad_combined_limit_date') {
    $message = $co->strong("Sorry, no entry has been $argument.");
  }
  elsif ($note eq 'bad AC') {
    $message = "Your search argument ($argument) is not a valid accession number.<br>Please, check that you have entered a non void valid pattern!";
  }
  elsif ($note eq 'no AC pattern') {
    $message = "Your search argument ($argument) could not match any entry.<br>Please, try to be less specific!";
  }  
  elsif ($note eq 'bad author') {
    $message = "Your author search argument ($argument) is too short to be searched. Please be more specific or specify at least ".
               " a two characters long entry identifier!";
  }
  elsif ($note eq 'bad pi_mw range') {
    $message = "Your given pI/Mw value(s) ";
    $message.= ($argument =~ s/\'__zero_range__\'//)? "$argument are mistyped or of range zero.":"$argument is (are) not valid.";
    $message.= " Please, enter valid values.";
  }
  elsif ($note eq 'no_ident_methods') {
    $message = "No identification method has been selected! Please, select at least one identification method!";
  }
  print $co->start_table({-width=>'50%', -align=>'center', -bgcolor=>$bkgrd}).
        $co->Tr($co->td({-class=>'red', -align=>'center'}, $message)).
        $co->end_table."\n";

}


##======================================================================##

##======================================================================##


sub _ignore_used_only_once_
{

  return;

  () if (
    $SERVER::interface or
    $SERVER::read_mapping_methods_description_list or
    $SERVER::showAdminLoginButton or
    $SERVER::need_login or
    $SERVER::deactivated or
    $SERVER::extractAllRemoteDatabaseNames or
    $main::core_interface_prefix or
    $main::cgi_2ddb or
    $main::default_sample_origin_URL or
    $main::gd_chart_viewer or
    $main::show_hidden_entries or
    $main::private_data_password or
    $main::unidentifiedProteinNickname or
    $main::privateSearchUnidentifiedProtein or
    $main::url_www2d or
    $main::email or
    $main::bkgrd or
    $main::title_comment or
    $main::name2d or!p
    $MAP::include_spot_volume or
    $main::icons or
    $main::default_image_type or
    $main::menu_graphic_mode or
    $main::server_path or
    $main::add_database_name_to_title or
    $main::extract_image_URL_from_DB or
    $main::default_small_image_url or
    $main::default_small_image_type or
    $_DEBUG_::ON
  )

}


