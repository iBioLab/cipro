#!/usr/bin/perl

#use strict;
#use warnings;
use CGI qw /:all/;
use DBI;
use LWP::Simple;

my $id = param('id');
my $gel = param('gel');
my $popupid = param('popupid');
my $ac = param('ac') || $id;

(my $url = url())=~s/[^\/]+$//;

my $dbh = DBI->connect("dbi:Pg:dbname = sds; host = cipro5.ibio.jp", "postgres", "", {AutoCommit => 0});
my $sth = $dbh->prepare("SELECT DISTINCT ac FROM spotentry WHERE gelid = (SELECT gelid FROM gel WHERE shortname = '$gel') AND ac != 'UNIDENTIFIED'");
$sth->execute;
my @acs = ('all spots');
while(my @array = $sth->fetchrow_array) {
    push(@acs, $array[0]);
}

$sth = $dbh->prepare("SELECT xpixelsize, ypixelsize FROM gelimage WHERE gelid = (SELECT gelid FROM gel WHERE shortname = '$gel')");
$sth->execute;
my $sizes = $sth->fetch;

if($id eq 'all') {
    $id = 'all spots';
}

$sth->finish;
$dbh->disconnect;

sub content {
    my $content = get($url."2d_view_map.cgi?map=".$gel."&ac=".$ac."&scale=0.33");
    $content =~ s/\<\!DOCTYPE.+?\<meta.+?\>//s;
    $content =~ s/\<input type="hidden" name="keep_LastDatabase.+?value\=\"show_msms_spots\" \>\<\/div\>\<\/form\>//s;
    if($id ne 'all spots') {
        $content =~ s/alt="" width="9"/alt="" width="12"/g;
    }
    return $content;
}

sub links {
    return start_form(),
           hidden('gel', $gel), hidden('id', $id),
           div({style => "position: absolute; top: ".($$sizes[1]*0.33+110)."px; left: ".($$sizes[0]*0.33/2-128.5)."px; font-family: Arial;"}, 
    	       "View: ", 
	       scrolling_list(-name => 'ac',
	                      -values => \@acs,
	   		      -default => $id, 
			      -size => 1,
			      -onchange => "submit(this)",
			      -style => "font-family: Arial;")),
	   end_form();
}


$JSCRIPT=<<END;
function resizeIframe() {
    var iframe = parent.document.getElementById("sdsi$popupid");
    var height = document.documentElement.scrollHeight || document.getElementById("sds$popupid").offsetHeight;
    iframe.style.height = height + 20 + "px";    
    iframe.style.width = height + 50 + "px";    
}
END
#var iframe = parent.document.getElementById("sds$popupid");
#var height = document.documentElement.scrollHeight || document.getElementById("sds$popupid").offsetHeight;
#var width = document.documentElement.scrollWidth || document.getElementById("sds$popupid").offsetWidth;
#iframe.style.height = height + 20 + "px";
#iframe.style.width = width + 20 + "px";

print start_html(-title=>"CIPRO 2D-PAGE",
		 #-script=>[$JSCRIPT],
		 -script=>$JSCRIPT,
		 -onload=>"resizeIframe();"
    );

#print start_html({-title => "CIPRO 2D-PAGE"});
print content(), links(), end_html();
