#!/usr/bin/perl5.8.8
#@(#)2d_view_map.cgi
# Make2D-DB II tool
# This script is the GD chart viewer of the WEB server for the relational 2-DE database
#======================================================================================

# /*  This script is part of the Make-2D DB II Tool                               */
# /*  Copyright to the SWISS INSTITUTE OF BIOINFORMATICS                          */
# /*  You can freely use this script without moving those copyright lines         */
# /*  Modifications should comply to the instructions stated in the licence terms */


$VERSION = 2.50;

$SERVER::GDChart = 1;

# - GD graphics library: http://www.boutell.com/gd/
# - zlib compression library: http://www.zlib.net/
# - libpng PNG reference library: http://www.libpng.org/pub/png/libpng.html
# - perl module GD: http://search.cpan.org/~mverb/GDGraph-1.43/Graph.pm)
# - perl module Chart: http://www.cpan.org/modules/by-module/Chart/
# (see: # http://search.cpan.org/src/CHARTGRP/Chart-2.4.1/Documentation.pdf)

#=============================================================================

use strict;
use vars qw($VERSION $sub_VERSION);
use strict;
use vars qw($VERSION $sub_VERSION);


use lib "./inc";
do "basic_include.pl" or die;
do "2d_util.pl" or die;

if ($SERVER::ExPASy) {
  require "foot.pl";
}

use CGI qw (-no_xhtml :standard :html);
$CGI::POST_MAX = 1024 * 256;

#=============================================================================

# Initialisation #

my $co = new CGI;

my $name2d               = $main::name2d;
(my $name2dAlphaNumeric = $name2d) =~ s/\W/_/g;

my $full_script_name     = $co->url(-full=>1);

my $show_hidden_entries  = $main::show_hidden_entries;
my $private_data_password = $main::private_data_password;
my $userPrivatePassword = undef;
if ($private_data_password) {
  $userPrivatePassword = _crypting_string_($co->cookie("2d_".$name2dAlphaNumeric."_private_password"), 1, 0);
  if ($userPrivatePassword eq $private_data_password) {
    $show_hidden_entries = 1;
  }
  else {
    undef($userPrivatePassword) 
  }
}

$SERVER::coreSchema   = 'core';
$SERVER::publicSchema = 'public';
$SERVER::commonSchema = 'common';
$SERVER::logSchema    = 'log';

my $unidentifiedProteinNickname = quotemeta($main::unidentifiedProteinNickname);
if ($show_hidden_entries) {
  $SERVER::search_path = $SERVER::coreSchema.','.$SERVER::commonSchema.','.$SERVER::logSchema;
  if ($unidentifiedProteinNickname) {
    $SERVER::showFlagSQLEntry = "Entry.AC <> '$unidentifiedProteinNickname'";
  }
  else {
    $SERVER::showFlagSQLEntry = '1 = 1';
  }
}
else {
  $SERVER::search_path = $SERVER::publicSchema.','.$SERVER::commonSchema;
  $SERVER::hidePrivate = 1;
  $SERVER::showFlagSQLEntry = 'Entry.showFlag IS TRUE';
}


#=============================================================================

# Read arguments

my (%args, $key, $value);
if ($ENV{REQUEST_METHOD} eq 'GET') {
  foreach my $input (split("&",$ENV{QUERY_STRING})) {
    $input =~ s/\&//g;
    if ($input =~ /([^=]+)=?(.*)/) {
      my ($key,$value) = (lc($1), $2);
      $value = uc($value) unless $key eq 'database';
      # replace "+" with " "
      $value =~ s/\+/ /g;
      # convert hex characters
      $value =~ s/%(..)/pack('c',hex($1))/eg;
      # add keyword/value pair to a list
      $args{$key} = $value if $key;
      undef($key); undef($value);
    }
  }
}

my $map = ($co->param('map'))? uc($co->param('map')) : $args{map};
my $spot = ($co->param('spot'))? uc($co->param('spot')) : $args{spot};
my $data = ($co->param('data'))? lc($co->param('data')) : ($args{data})? $args{data} : 'msms';
   $data = 'msms' if $data eq 'ms';
my $msms = ($data eq 'msms')? 1 : 0;
my $ac = ($co->param('ac'))? uc($co->param('ac')) : $args{ac};
my $database = ($co->param('database'))? $co->param('database') : $args{database};
   unless ($database) {
     $database = get_DB_number();
   }
   if ($database =~ /^\d+$/) {
     $database = ($main::database[$database]{database_name})? $main::database[$database]{database_name} : $main::default_dbname;
   }
my $argumentsNotOK = my $argumentMissingOnlyAC = 0;
$argumentsNotOK = 1 if (
    !$map or !$spot or ($data ne 'pmf' and $data ne 'msms')
    or (exists($args{spectra}) and !$args{expid})
  );
if (!$ac and !$argumentsNotOK) {
  $argumentsNotOK = 1;
  $argumentMissingOnlyAC = 1;
}

$co->param(-name=>'map', -values=>[$map]);
$co->param(-name=>'spot', -values=>[$spot]);
$co->param(-name=>'data', -values=>[$data]);
$co->param(-name=>'ac', -values=>[$ac]);
$co->param(-name=>'database', -values=>[$database]);


#=============================================================================

# Some definitions

$DB::CurrentDatabase = &get_DB_number($database);
$DB::CurrentDatabaseName = ($main::database[$DB::CurrentDatabase]{database_name})?
  $main::database[$DB::CurrentDatabase]{database_name} : $main::default_dbname;

my $multi_DBs = (scalar(@main::DATABASES_Included) > 1)? 1 : 0;

my $table = {};
$table->{ident}->{pmf} = 'SpotIdentificationPeptMassF';
$table->{ident}->{msms}  = 'SpotIdentificationTandemMS';
$table->{data}->{pmf}  = 'SpotDataPeptMassF';
$table->{data}->{msms}   = 'SpotDataTandemMS';


my $commandIdent = "SELECT ".(($msms)? "ionMasses" : "peptideMasses").", dataExpID FROM ".$table->{ident}->{$data}.
                   " WHERE identificationID = __identExpID__";
my $commandData = "SELECT ".(($msms)? "ionMasses" : "peptideMasses, enzyme")." FROM ".$table->{data}->{$data}.
             " WHERE dataExpID = __dataExpID__";

#=============================================================================

#==============#
#= main part =#
#============#


if ($argumentsNotOK or !exists($args{spectra})) {

  my ($command, $counter, $message);
  my $result = [];
  my $formData = {};

  my $style  = shared_definitions("style");

  print $co->header();

  print $co->start_html

  (
    -title   => "$name2d - GDchart Peak List Viewer",
    -author  => "$main::email",
    -head    => [meta({'http-equiv'=>'Content-Type', 'content'=>'text/html;charset=iso-8859-1'})],
    -bgcolor => "$main::bkgrd",
    -link    => 'blue',
    -style   => {'-code'=>$style},
    -dtd     => '"-//W3C/DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"',
  ),"\n";

  my $title = "$DB::CurrentDatabaseName: Experimental spectra viewer (".(($msms)? 'MS/MS' : 'PMF').")";
  print $co->h2({-class=>'underlined'}, $title);
  my $subTitle = $co->span({-class=>'H3Font'},"Spot: ").$co->span({-class=>'H3Font red'}, $spot).
    $co->span({-class=>'H3Font'}," ($map), AC: ").$co->span({-class=>'H3Font red'}, "$ac");
  print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$subTitle.$co->br unless $argumentsNotOK;

  my $identTable = $table->{ident}->{$data};
  my $dataTable = $table->{data}->{$data};
  $command = "SELECT $identTable.AC, $identTable.identificationID,".
             " $identTable.humanIdentifier, $identTable.version".
             (($msms)? ", $dataTable.dataExpID, $dataTable.humanIdentifier, $dataTable.parentMass, $dataTable.parentCharge": '').
             " FROM Gel, $identTable".
             (($msms)? ", $dataTable" : ''). 
             " WHERE Gel.shortname = '$map' AND $identTable.gelID = Gel.gelID ".
             " AND upper($identTable.spotID) = '$spot' AND $identTable.AC = '$ac'".
             (($msms)?
               " AND $dataTable.dataExpID = $identTable.dataExpID AND $dataTable.spotID = $identTable.spotID AND $dataTable.gelID = $identTable.gelID"
               : '').
             " ORDER BY ". # ."$identTable.humanIdentifier, $dataTable.humanIdentifier, ".
             (($msms)? "$dataTable.parentMass, " : '')."$identTable.version";

  my $voidPeakList = 0;
  unless ($argumentsNotOK) {
    $result = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
    $voidPeakList = 1 if (!$result and $co->param('Show spectra'));
  }

  my $numResult = (scalar @{$result}) -1;
  foreach my $row (@{$result}) {
    my ($row_ac, $row_identID, $row_ident_hID, $row_version, $row_dataID, $row_data_hID, $row_parentMass, $row_parentCharge)  = @{$row};
    push @{$formData->{$row_ac}->{identID}}, $row_identID;
    push @{$formData->{$row_ac}->{dataID}}, $row_dataID;
    push @{$formData->{$row_ac}->{identHumanID}}, $row_ident_hID if $row_ident_hID;
    push @{$formData->{$row_ac}->{dataHumanID}}, $row_data_hID if $row_data_hID;
    push @{$formData->{$row_ac}->{version}}, $row_version;
    if ($msms) {
      $row_parentCharge = ($row_parentCharge > 0)? "$row_parentCharge+" : ($row_parentCharge<0)? "$row_parentCharge-" :
                          (!$row_parentCharge)? "'undefined charge'" : $row_parentCharge;
      if (!$row_parentMass) {
        $row_parentMass = "'undefined parent mass' (".$row_identID.")"
      }
      push @{$formData->{$row_ac}->{parentMass}}, $row_parentMass;
      push @{$formData->{$row_ac}->{parentCharge}}, $row_parentCharge;
    }
  }

  _terminate_html_('nothing matches!') unless $formData;

  my $formName = 'Form1';
  print $co->start_form(-method=>'POST', -action =>"$full_script_name", -name=>"$formName")."\n";

  my @listSpectra = (); my %labels = ();
  if ($msms) {
    for ($counter = 0; $counter <= $numResult; $counter++) {
      $listSpectra[$counter] = ${$formData->{$ac}->{identID}}[$counter];
      my $hID;
      $hID = ${$formData->{$ac}->{identHumanID}}[$counter] if ${$formData->{$ac}->{identHumanID}}[$counter];
      $hID .= '/' if $hID;
      $hID = ${$formData->{$ac}->{dataHumanID}}[$counter] if ${$formData->{$ac}->{dataHumanID}}[$counter];
      $hID = " {$hID}" if $hID;
      $labels{$listSpectra[$counter]} = "[ identification n. ".${$formData->{$ac}->{identID}}[$counter].
      "$hID => ".${$formData->{$ac}->{parentMass}}[$counter]." : ".${$formData->{$ac}->{parentCharge}}[$counter]." ]";
    } 
  } else {
    for ($counter =0; $counter <= $numResult; $counter++) {
      $listSpectra[$counter] = ${$formData->{$ac}->{identID}}[$counter];
      $labels{$listSpectra[$counter]} = "[ identification n. ".${$formData->{$ac}->{identID}}[$counter]." ]";
    } 
  }

  if (@listSpectra or $voidPeakList) {

    print $co->h4({-class=>'red left'}, "Sorry, no peak list values are given for this spectra!").$co->p if $voidPeakList;

    print $co->br.$co->p.'&nbsp;'.$co->strong('Available experiments:').$co->br.$co->p;
    print '&nbsp;&nbsp;&nbsp;'.$co->scrolling_list (
      -name=> 'selected_spectra',
      -values=>[@listSpectra],
      -default=>[$listSpectra[0]],
      -labels=>\%labels,
      -size=>3
    )."\n";

    print '&nbsp;&nbsp;--&nbsp;&nbsp;range&nbsp;(zoom)&nbsp;from:&nbsp;'.
    $co->textfield (
      -name=> 'range_from',
      -size => 4
    )."\n";
    print "&nbsp;&nbsp;to:&nbsp;".
    $co->textfield (
      -name=> 'range_to',
      -size=> 4
    )."\n";
    print '&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;'.$co->checkbox(-name=>'list values', -checked=>1);

    print $co->p;
    print $co->submit({-name=>'Show spectra', -label =>'Show spectra'})."\n";
    print '&nbsp;&nbsp;&nbsp;'.
          $co->button({ -value=>'reset data', -onClick=>"window.location.href='$full_script_name'"});
    print $co->hidden('map')."\n";
    print $co->hidden('spot')."\n";
    print $co->hidden('data')."\n";
    print $co->hidden('ac')."\n";
    print $co->hidden('database')."\n";

  } else {
    if (!$argumentsNotOK) {
      $message = 'Cannot find any '.(($msms)? 'MS/MS' : 'PMF').' identification data for entry \''.$ac.'\' related to spot \''.$spot.'\' ('.$map.')!';
    } else {
      $message = ($co->param('Submit data') and !$argumentMissingOnlyAC)? 'Missing arguments!' : '';
    }
    my @maps = EXECUTE_FAST_COMMAND("SELECT Gel.shortname FROM Gel");
    my (@all_databases, $databases_field);
    if ($multi_DBs) {
      foreach my $database_number (@main::DATABASES_Included) {
        my $database_name = $main::database[$database_number]{database_name};
        push @all_databases, $database_name;
      }
      $databases_field = '&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;database&nbsp;'.
          $co->scrolling_list( -name =>'database', -values=>[@all_databases], -default=>[$database], size=>1);
    }
    print $co->p;
    print $co->h4({-class=>'left'}, "Please, enter data to be searched for:").$co->p;
    print 'spot:&nbsp;'.$co->textfield(-name=>'spot', -default=>$spot);
    print '&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;map:&nbsp'.
          $co->scrolling_list(-name=>'map', -values=>[@maps], -default=>[$map], -size=>1);
    if ($argumentMissingOnlyAC) {
      my $identTable = $table->{ident}->{$data};
      $command = "SELECT DISTINCT $identTable.AC FROM $identTable, Gel ".
                 "WHERE $identTable.spotID = '$spot' AND Gel.shortname = '$map' AND Gel.gelID = $identTable.gelID";
      my @foundACs = EXECUTE_FAST_COMMAND("$command");
      if (@foundACs) {
        print '&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;identified&nbsp;AC:&nbsp;'.
              $co->scrolling_list(-name=>'ac', -values=>[@foundACs], -default=>[$ac], -size=>1);
      } else {
        $message = "Sorry, there are no entries identified by ".uc($data)." for this spot!";
      }
    }
    print $co->p.'identification&nbsp;method&nbsp;'.
          $co->radio_group(-name=>'data', -values=>['msms', 'pmf'], -default=>[$data], -labels=>{'msms'=>'MS/MS', 'pmf'=>'PMF'});
    print $databases_field;
    print $co->p.'&nbsp;&nbsp;&nbsp;&nbsp;';
    print $co->submit({-name=>'Submit data', -label =>'Submit data'})."\n";
    print '&nbsp;&nbsp;&nbsp;'.
          $co->button({ -value=>'reset data', -onClick=>"window.location.href='$full_script_name'"});
    print $co->br.$co->h4({-class=>'red left'}, "$message");
  }

  print $co->end_form."\n";
  print $co->p;

  if ($co->param('selected_spectra')) {
    my $selectedIdentID = $co->param('selected_spectra');
    my $range_from = $co->param('range_from');
    my $range_to = $co->param('range_to');
    my $range = (( $range_from =~ /(\d+\.?\d*)/)? $1 : '').'-'.
                (( $range_to =~ /(\d+\.?\d*)/)? $1 : '');
    undef($range) if $range eq '-';
    my $rangeURL = ($range)? "\&range=$range" : '';
    my $full_script_name_spectra_args = $full_script_name."?\&map=$map\&spot=$spot\&data=$data".
      "\&ac=$ac\&database=".$DB::CurrentDatabaseName.
      "\&expid=$selectedIdentID\&identid\&spectra$rangeURL";
    print $co->br.$co->img({-border=>'1px', -src=>"$full_script_name_spectra_args", -alt=>"[spectra cannot be displayed]"});

    if ($co->param('list values')) {
     ($command = $commandIdent) =~ s/__identExpID__/$selectedIdentID/;
      my ($identIonMasses, $dataExpID) = EXECUTE_FAST_COMMAND($command);
      my ($identIonMassesMz, $identIonMassesIntensity, $identIonMassesNotRealIntensity) = _READ_PGSQL_NUMERIC_ARRAY_($identIonMasses);
      undef($identIonMasses);
     ($command = $commandData) =~ s/__dataExpID__/$dataExpID/;
      my ($dataIonMasses, $pmfEnzyme) = EXECUTE_FAST_COMMAND($command);
      my ($dataIonMassesMz, $dataIonMassesIntensity, $dataIonMassesNotRealIntensity) = _READ_PGSQL_NUMERIC_ARRAY_($dataIonMasses);
      undef($dataIonMasses);
      my $dataTurn = 0;
      print $co->br.$co->p;
      foreach my $ionMassesMz ($identIonMassesMz, $dataIonMassesMz) {
        my $ionMassesIntensity = ($dataTurn)? $dataIonMassesIntensity : $identIonMassesIntensity;
        my $tableCaption = ($dataTurn)? 'List of remaining peaks' : 'List of retained / identified peaks';
        print $co->br."\n";
        if ($ionMassesMz) {
          my $listTable;
          my $tableNotEmpty = 0;
          my $innerCounter;
          if ($range) {
            my $rangeTable = $range; $rangeTable = "0$range" if $range =~ /^\-/; $rangeTable .= "all" if $range =~ /\-$/;
            $tableCaption .= " (range: $rangeTable)";
          }
          $listTable = $co->start_table().$co->caption($co->strong($tableCaption)).$co->start_Tr.$co->start_td({-valign=>'top'})."\n";
          my $innerTableStart = $co->start_table({-border=>1, -bgcolor=>'#eeeeee'}).
             $co->th({-bgcolor=>'#7777ee'},['m/Z','intensity'])."\n";
          $listTable .= $innerTableStart;
          while (my $ionMasse = shift @{$ionMassesMz}) {
             next if $dataTurn and $identIonMassesIntensity->{$ionMasse};
             next if $range_from and $ionMasse < $range_from;
             next if $range_to and $ionMasse > $range_to;
            if ($innerCounter >10 ) {
              $innerCounter = 0;
              $listTable .= $co->end_table.$co->end_td.$co->start_td({-valign=>'top'}).$innerTableStart;
            }
            $innerCounter++;
            $listTable .= $co->Tr($co->td({-class=>'small', -align=>'right'}, $ionMasse).
                  $co->td({-class=>'small gray', -align=>'right'}, $ionMassesIntensity->{$ionMasse}))."\n";
            $tableNotEmpty = 1;
          }
          $listTable .= $co->end_table.$co->end_td.$co->end_Tr.$co->end_table."\n";
          if ($tableNotEmpty) {
            print $listTable;
          } else {
              my $noValuesMessage = "$tableCaption: no ".(($dataTurn)? 'other ':'').'peak list values are available'.
                 (($range)? ' in this range':'').'!';
              print $co->span({-class=>'Comment'}, $noValuesMessage).$co->br;
          }
        }
        else {
          print $co->span({-class=>'Comment'}, "$tableCaption: no peak list values could be retrieved!").$co->br;
        }
        $dataTurn = 1;
      }

    }

  }

  print $co->end_html."\n";

}



#=============================================================================

#=======================#
#= spectra generation =#
#=====================#

else {

  eval ("use Chart::ErrorBars");
  exit(1) if $@;

  my ($command);
  my $identExpID = $args{expid};
  my ($range_from, $range_to) = (undef, undef);
  if ($args{range}) {
    $range_from = $1 if $args{range} =~ /^(\d+\.?\d*)/;
    $range_to = $1 if $args{range} =~ /\-(\d+\.?\d*)$/;
    ($range_from, $range_to) =  ($range_to, $range_from) if  $range_from > $range_to and $range_to;
  }

  my $tmp_path = $main::tmp_path;

 ($command = $commandIdent) =~ s/__identExpID__/$identExpID/;
  my ($identIonMasses, $dataExpID) = EXECUTE_FAST_COMMAND($command);

  my ($identIonMassesMz, $identIonMassesIntensity, $identIonMassesNotRealIntensity) = _READ_PGSQL_NUMERIC_ARRAY_($identIonMasses);
  undef($identIonMasses);

 ($command = $commandData) =~ s/__dataExpID__/$dataExpID/;
  my ($dataIonMasses, $pmfEnzyme) = EXECUTE_FAST_COMMAND($command);

  my ($dataIonMassesMz, $dataIonMassesIntensity, $dataIonMassesNotRealIntensity) = _READ_PGSQL_NUMERIC_ARRAY_($dataIonMasses);
  undef($dataIonMasses);

  my $argumentsNotOKTitle = ($identIonMassesNotRealIntensity or $dataIonMassesNotRealIntensity)?
    ' (intensity is not explicitly given!)' : '';

  my (@mzValues, @identValues, @dataValues);
  my @mzValuesInit = (@{$identIonMassesMz}, @{$dataIonMassesMz});
  @mzValuesInit = sort (@mzValuesInit);
  my $mzValuePrevious = undef;
  foreach my $mzValue (@mzValuesInit) {
    next if $mzValue == $mzValuePrevious;
    next if $range_from and $mzValue < $range_from;
    next if $range_to and $mzValue > $range_to;
    push @mzValues, $mzValue;
    if ($identIonMassesIntensity->{$mzValue}) {
      push @identValues, $identIonMassesIntensity->{$mzValue};
    } else {
      push @identValues, 0;
    }
    if ($dataIonMassesIntensity->{$mzValue} and !$identIonMassesIntensity->{$mzValue}) {
      push @dataValues, $dataIonMassesIntensity->{$mzValue};
    } else {
      push @dataValues, 0;
    }
    $mzValuePrevious = $mzValue;
  }

  my $onlyOnesIntensity = 1;
  $onlyOnesIntensity = 0 if grep {$_ and $_ != 1} @identValues or grep {$_ and $_ != 1} @dataValues ;

  my ($dimensionX, $dimensionY) = (800,400);
 ($dimensionX, $dimensionY) = (800,100) unless @identValues;

  my @upErrors;
  foreach (@mzValues) {
    push @upErrors, 0;
  }

  my $obj = Chart::ErrorBars->new ($dimensionX, $dimensionY);

  $obj->add_dataset(@mzValues);

  $obj->add_dataset(@identValues);
  $obj->add_dataset(@upErrors);
  $obj->add_dataset(@identValues); # down errors

  $obj->add_dataset(@dataValues);
  $obj->add_dataset(@upErrors);
  $obj->add_dataset(@dataValues); # down errors

  my @yValuesSorted = sort(@identValues, @dataValues);
  my $max_val = int($yValuesSorted[scalar @yValuesSorted -1] * 1.1);
  undef(@yValuesSorted);

  my $title = "Data=".uc($data).(($pmfEnzyme)? "($pmfEnzyme)": '').", Database=$database, Spot=$spot($map), AC=$ac";
  my $subtitle =  "[ Identification exp.ID = '$identExpID', Data exp.ID = '$dataExpID' ]$argumentsNotOKTitle";
  my $identifiedLabel = 'retained / identified peaks';
  my $dataLabel = 'non identified / other peaks'. ((grep {$_} @dataValues)? '' : ' (no additional data given)');
  my @labels = ($identifiedLabel,$dataLabel);
  my %attributes = (
    'title' => "$title",
    'sub_title' => "$subtitle",
    'legend_labels' => \@labels, # must be an Array reference!!
    'legend' => 'bottom',
    'xy_plot' => 'true',
    'x_label' => 'm/Z',
    'y_label' => 'relative intensity',
    'precision'=>3,
    'min_val' => 0,
    #'max_val' => $max_val,
    'min_x_ticks' => 25,
    'max_x_ticks' => 40,
    'min_y_ticks' => 6  ,
    'max_y_ticks' => 12,
    'x_ticks' => 'vertical',
    'brush_size' => 1,
    'pt_size'=> 0,
    'sort'=>'true',
    'transparent' => 'true',
    'colors' => {'background'=>[255,255,200], 'title'=>[255,20,20], 'text'=>[0,0,0], 'dataset0'=>[255,0,0], 'dataset1'=>[120,120,120]}
  );
  if ($onlyOnesIntensity) {
    $attributes{'max_val'} = 1.5;
    $attributes{'y_label'} = 'not given intensities';
    $attributes{'y_axes'} = 'right';
  }

  unless (@identValues) {
    $subtitle = ($args{range})? '[No available data in this range]' : '[No available Data]';
    %attributes = (
      'title' => "$title",
      'sub_title' => "$subtitle",
      'precision'=>0,
      #'transparent' => 'true',
      'colors' => {'background'=>[255,255,200], 'title'=>[255,20,20], 'text'=>[0,0,0]}
    );
  }

  $obj->set(%attributes);
  #$obj->set('colors' => {'background' =>[200,200,200]});
  $obj->cgi_png;

} 


#=============================================================================

exit(0);

#=============================================================================


sub _terminate_html_ {

  my $message = $_[0];
  my $co_terminate = new CGI;
  print $co_terminate->h4($message) if $message;
  print $co_terminate->end_form."\n";
  exit(1);
  
}

#=============================================================================


sub _READ_PGSQL_NUMERIC_ARRAY_ {
  my $array = $_[0];
  my $mz = [];
  my $intensity = {};# = {};
  my ($current_mz, $current_intensity);
  my $notRealIntensity = 0;
  while ($array =~ /\{\s*(\d+\.?\d*)(?:\s*,?\s*(\d+\.?\d*))?\s*\}/g) {
    $current_mz = $1;
    if ($2) {
      $current_intensity = $2;
    } else {
      $current_intensity =  1;
      $notRealIntensity = 1;
    }
    push @{$mz}, $current_mz;
    $intensity->{$current_mz} = $current_intensity;
  }

  return ($mz, $intensity, $notRealIntensity);
}

#=============================================================================

exit(0);


##======================================================================##

##======================================================================##


sub _ignore_used_only_once_
{

  return;

  () if (
    $SERVER::ExPASy or
    $SERVER::GDChart or
    $SERVER::hidePrivate or
    $main::email or
    $main::unidentifiedProteinNickname or
    $main::tmp_path or
    $main::name2d or
    $main::private_data_password or
    $main::show_hidden_entries or
    $main::bkgrd
  )

}


