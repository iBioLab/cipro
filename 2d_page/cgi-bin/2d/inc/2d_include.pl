#/usr/bin/perl -w
##@(#)2d_include.pl         version: 2.50

#=================================================================================================#
# Make2D-DB II tool                                                                               #
# 2d_include.pl                                                                                   #
# This is the configuration file for the WEB server scripts querying the relationel 2-DE database #
#=================================================================================================#

#-------------------------------------------------------------------------------------------------#


use constant CONFIG_FILE => 'conf/cipro.ini';
use Config::Simple;
my $cfg = new Config::Simple(CONFIG_FILE);


#---------------------------------------------------#
# START TO SET YOUR CONFIGURATION STARTING FROM HERE
#---------------------------------------------------#


# GENERAL BEHAVIOOUR #
#--------------------#


# URL Redirection (Apache mod_rewrite) #

# Activate the redirection of URLs using the Apache mod_rewrite module (1 for yes, 0 for no)?
# (if activated, make sure after your installation to include the generated redirection rules into your Apache configuration file)
# (see the 'Readme: interface' page for more details)
#
$modeRewriteOn    = 0;


#--------------------#


# TITLE NAME of the SERVER and some other server settings #

# Title Name to display on the server:
#
$name2d           = "CIPRO 2D-PAGE";
#$name2d = $cfg->param('2dpage.name2d');

# Sub-Title:
# You may type any text to be displayed (including HTML tags),
# Note: In addition, the tool will look for a file that is called:
# * subtitle.html *
# that you may provide within your data directory to make it included into the interface home page
# The *subtitle.html* file will be copied into your server_path/html/your_database/data (tmp) directory
#
$title_comment = "Home page of the 2D-PAGE server to query 'CIPRO 2D-PAGE'";

# Do you wish to add the running database name to the displayed title (1 for yes, 0 for no)?
# If you are running several databases at the same time, this option will be automatically activated
#
$add_database_name_to_title = 1;

# The name of Person/Institution that maintains the database (optional)
# This name will be displayed on the bottom of the web pages as a contact address
#
$maintainer       = '';

# Database Author/contact e-mail address (optional): !! Do not omit the *backslah* before the '@' sign !!
#
#$email            = 'yonezawa\@ibio.jp';
$email            = 'cipro_admin@ibio.jp';

# Background colour of the pages (use standard HTML defined colours, default: lightyellow):
#
$bkgrd            = "#ffffff";

# Main font color (use standard HTML defined colours):
#
$basic_font_color = "black";

# Menu graphic modes: type '1' or '2' to choose one of the 2 graphic menus, type '0' for a simple textual menu
#
$menu_graphic_mode = 0;

# If buttons menu is selected, choose a color for the buttons (default: khaki):
#
$menu_button_color = "#f0e68c";

# If buttons menu is selected, choose a color for the selected active button (default: brown):
#
$menu_selected_button_color = "#a52a2a";

# If buttons menu is selected, either use the default font color for the button text (0),
# or the reverse color between active and non active buttons (1):
#
$menu_reverse_font_color    = 1;

# The viewer displays spot inforamtion inside a small box. Give a size (in pixel) for the width of the box:
#
$ballon_size      = "280";

# Any private data, marked 'hidden', is not displayed to public users, except if this value is set to (1):
#
$show_hidden_entries     = 0;

# A user may access private data using the following password
# No access to private data will be allowed to users if no password is defined here
#
$private_data_password = "";

# Include in your viewer non identified spots (0 / 1):
#
$include_not_identified_spots = 1;

# Show the external data gathered when displaying an entry / include this data within the search engine (0 / 1):
#
$show_external_data   = 1;

#--------------#

# EXPASY SERVER URL: choose any of the following: #
#
#  $expasy =  "http://www.expasy.org"; #
#  $expasy =  "http://cn.expasy.org";  #
#  $expasy =  "http://bo.expasy.org";  #
#  $expasy =  "http://ca.expasy.org";  #
#  $expasy =  "http://au.expasy.org";  #
#  $expasy =  "http://br.expasy.org";  #

$expasy = "http://www.expasy.org";


#--------------#



# PATHS OF DATA IN THE SERVER #

# main path to the server (Apache Document Root):
#
$server_path              = "../../";

# reference name for the directories containing your data in the server:
# (this name is the one you would have already provided in the main configuration file)
#
$web_server_ref_name      = "2d";

# path of the parent html subdirectoy in the server: 
#
$html_path                = "../../html";

# path for a directory to store temporary files in the server:
#
$tmp_path                 = "../../html/2d/data/tmp";



#--------------#



# SERVER ABSOLUTE/RELATIVE URLs #

# relative URL for the main 2D html page:
# (in some systems you may need to precede this value by '/html' or '/html_public'!)
#
$url_www2d                = "../../html/2d";

# relative URL for the running CGI server scripts:
#
$cgi_2ddb                 = "../../cgi-bin/2d";

# name of the main script to query the database:
#
$main_script_file         = "2d.cgi";


# relative URL for the small icons and logos on the server:
#
$icons                    = "../../icons";

# name of the HTML top page file to include at the top of the server pages (leave blank if no file is needed):
# the file is assumed to be in server_path/html/your_database/data directory, otherwise, give a complete path to look in another direcroty
#
$db_server_include_file   = "../../html/2d/data";

# shift the initial display position of the maps within the viewer by: (in pixels)
#
$increase_map_display_shift_down = 0;

# name of the logo image you have provided (e.g. "logo.gif")
# or its relative URL on your site (e.g. "/images/logos/logo.gif")
#
$db_server_logo_name      = "../../icons/make2DBLogo.gif";

# display the logo image in the viewer (1 for yes, 0 for no):
#
$display_db_server_logo_in_viewer = 0;

# rescale your logo image to a fixed width (in pixels), type 0 to skip redimension:
#
$db_server_logo_width     = 0;

# Home link: text to display for the link
#
$home_displayed_text      = "Home";

# Home link: URL ('/' means the Document Root)
#
$home_url                 = "/";
#--------------#

# DATABASES TO INCLUDE #

# Several local or remote databases/projects can be included within the same interface.
# You will be asked to define the connection parameters for each of them separately.
# To decide which databases/projects are to be included in the query interface,
# type between brackets the databases IDs (only numbers) you would like to assign to them, separated by commas:
# e.g. (1,2,3).
# You will be asked later to define the specific parameters for each of these databases.
#
# For a single database installation, keep the default value (1).
#
@DATABASES_Included = (1,2);


#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#


# DEFAULT DATABASE PARAMETERS #

# Each added database will have its own specific parameters (they will be set later) #
# Whenever those parameters are missing, the following default ones will be taken into consideration #


# Address of the running postgreSQL server.
# If the postgreSQL server is running on the same machine where the Apache server is running, then use 'localhost'
#
$default_host     = "localhost";

# default port (usually 5432):
#
$default_port     = 5432;

# default options (leave blank):
#
$default_options  = "";

# default tty (leave blank):
#
$default_tty      = "";

# Name of the postgreSQL database to be used by default:
# (during installation, you may leave 'sds' to let the tool automatically insert the name of the new database you are about to create)
#
$default_dbname   = "sds0";

# PostgreSQL select user (user with no write permissions to query the database):
#
$default_select_user = "select2d";

# PostgreSQL select user password, by default, the password is set up identical to the select user name:
#
$default_select_user_password = "select2d";

# extract the image absolute URL from the database [type 1]) / or read it from the specific DB parameters [type 0] (0 or 1)?
# (note 1: if you type 0 and no specific DB parameters are found, then default values - see after - will be used)
# (note 2: if you have built other databases for the same interface without any server configuration file, then type also 0)
#
$extract_image_URL_from_DB = 1;

# absolute URL for the map images:
#
$default_image_url        = "../../html/2d/data/gifs/sds";

# graphical map image type (gif, jpg, png, tif, etc..)?
#
$default_image_type       = "png";

# absolute URL for the small map images:
#
$default_small_image_url  = "../../html/2d/data/small-gifs/sds";

# graphical small map image type (gif, jpg, png, tif, etc..)?
#
$default_small_image_type = "png";

# an URL link to describe sample origin and preparation (leave blank if you do not have any)
#
$default_sample_origin_URL = "";
 
# default copyright message (to be displayed within each entry):
# !! do not press <return> before finishing your message!!
# write your message in just one line, or use the special escaped character '\n' - backslah & n - to impose new lines.
# You can use HTML tags (e.g. <BR>), as well as single-quotes (not double quotes).
#
$default_copyright_text   = "";



#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#


# DATABASES SPECIFIC PARAMETERS #
#===============================#


##__SERVER_DB_PARAMS_BEGIN__##


$database[1] =
{

# the 'database_name' is the name to *display* on the server, not the postgreSQL database name
database_name => "sds",
main_index => "SWISS-PROT",
# do not change (specific to SWISS-2DPAGE)
swiss_2d_page => 0,
# an URL link to describe sample origin and preparation (leave blank if you do not have any) 
sample_origin_URL => "",

# connection parameters ('dbname' is the *postgreSQL* database name)
dbname => "sds",
host => "localhost",
port => "5433",
options => "",
tty => "",
select_user => "select2d",
select_user_password => "select2d",
#dbname  => $cfg->param("2dpage.dbname"),
#host    => $cfg->param('2dpage.host'),
#port    => $cfg->param('2dpage.port'),
#options => $cfg->param('2dpage.options'),
#tty     => $cfg->param('2dpage.tty'),
#select_user => $cfg->param('2dpage.select_user'),
#select_user_password  => $cfg->param('2dpage.select_user_password'),

# map viewer: image type & shifting
image_url => "../../html/2d/data/gifs/sds",
image_type => "png",
small_image_url => "../../html/2d/data/small-gifs/sds",
small_image_type => "png",
# shift the map's x position to the left (pixels): default 0
map_shift_left => 0,
# shift the map's y position down (pixels): default 0
map_shift_down => 0,

# COPYRIGHT message:
# To deactivate copyright message on full entry view (or to use the default text), leave copyright_text empty,
# otherwise, type your text between the quotes.
copyright_text => "",

};


$database[2] =
{

# the 'database_name' is the name to *display* on the server, not the postgreSQL database name
database_name => "sds0",
main_index => "SWISS-PROT",
# do not change (specific to SWISS-2DPAGE)
swiss_2d_page => 0,
# an URL link to describe sample origin and preparation (leave blank if you do not have any) 
sample_origin_URL => "",

# connection parameters ('dbname' is the *postgreSQL* database name)
dbname => "sds0",
host => "localhost",
port => "5432",
options => "",
tty => "",
select_user => "select2d",
select_user_password => "select2d",

# map viewer: image type & shifting
image_url => "../../html/2d/data/gifs/sds",
image_type => "png",
small_image_url => "../../html/2d/data/small-gifs/sds",
small_image_type => "png",
# shift the map's x position to the left (pixels): default 0
map_shift_left => 0,
# shift the map's y position down (pixels): default 0
map_shift_down => 0,

# COPYRIGHT message:
# To deactivate copyright message on full entry view (or to use the default text), leave copyright_text empty,
# otherwise, type your text between the quotes.
copyright_text => "",

};


##__SERVER_DB_PARAMS_END__##
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#
1;
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#
