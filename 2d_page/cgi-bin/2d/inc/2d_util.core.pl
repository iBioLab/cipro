#@(#)2d_util.core.pl
# version 2.50
# Make2D-DB II tool
# This Sub-Routines collection is used by the WEB server scripts for the relational 2-DE database
#------------------------------------------------------------------------------------------------#

# /*  This script is part of the Make-2D DB II Tool                               */
# /*  Copyright to the SWISS INSTITUTE OF BIOINFORMATICS                          */
# /*  You can freely use this script without moving those copyright lines         */
# /*  Modifications should comply to the instructions stated in the licence terms */


#=============================================================================================================================#

# sub CORE_DATABASE_LOGIN_FORM                           login form for 'core'
# sub CORE_DATABASE_LOGIN_DATA                           read login info for 'core' and update their cookies expiration, otherwise read cookies

# sub CORE_FormFreeCommand                               display the Core free command form
# sub CORE_FormBatchCommand                              display the Core Batch commands form

# sub CORE_ExecuteBatchCommand                           execute the Core Batch commands

# sub CORE_UpdateExternalUniProtData                     update UniProt related data on the core schema
# sub CORE_UpdateExternalMake2DDBData                    update Make2DDB II (2D) related data on the core schema

# sub UPLOAD_FILE                                        upload a text file given by it's path

#=============================================================================================================================#

exit unless $SERVER::core_selector; 

use strict;


$CORE::nullInsertValue = undef;
$CORE::maxElementsToHide = 20000;

#=============================================================================================================================#
#=============================================================================================================================#


# Login form for 'core'

sub CORE_DATABASE_LOGIN_FORM {

  use CGI qw (-no_xhtml);
  my $co = new CGI;

  my $loginFormName = 'loginForm';

  print 

    $co->start_form(-method=>'POST', -name=>"$loginFormName")."\n".

    $co->start_table()."\n".
    $co->start_Tr()."\n".
    $co->td('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')."\n".
    $co->td({-bgcolor=>'#99FFFF'}, '&nbsp;')."\n".
    $co->start_td().
    $co->em({class=>'smallComment'},"&nbsp;&nbsp;&nbsp;ID ").
    $co->textfield(-name=>'login_id', -size=>6).
    $co->end_td."\n".
    $co->start_td().
    $co->em({class=>'smallComment'}," pass ").
    $co->password_field (-name=>'login_password', -default=>'', -size=>4).
    $co->end_td."\n";

  print 
    $co->start_td().
    $co->em({class=>'smallComment'}," database ").
    $co->textfield(-name=>'login_database', -default=>'', -size=>10).
    $co->end_td."\n" if $main::multi_DBs;

  print
    $co->start_td()."&nbsp;&nbsp;".
    $co->submit({class=>'smallComment', -name=>'login_submit', -value=>'login' })."\n".
    $co->end_td."\n".
    $co->end_Tr."\n".
    $co->end_table."\n".

    $co->end_form."\n";

  if ($SERVER::need_login) {
    print $co->br.$co->br.$co->strong("You will need to login to access the core schemas!").$co->br.$co->br.
          $co->em("Your login information will be encrypted and will definitely expire after 30 minutes of non activity.")."\n";
    print $co->end_td.$co->end_Tr.$co->end_table."\n";
    print $co->end_html."\n";
    exit 0;
  }

  undef($co);

  1;
}


# Read login info for 'core' and update their cookies expiration, otherwise read cookies

sub CORE_DATABASE_LOGIN_DATA {

  my $co = new CGI;

  if ($co->param('logout_submit')) {
    push (@SERVER::headerCookies, $co->cookie(-name=>'2d_core_login_i_',-value=>0, -expires=>'now'));
    push (@SERVER::headerCookies, $co->cookie(-name=>'2d_core_login_p_',-value=>0, -expires=>'now'));
    push (@SERVER::headerCookies, $co->cookie(-name=>'2d_core_login_d_',-value=>0, -expires=>'now'));
    $co->delete('logout_submit'); $co->delete('login_id'); $co->delete('login_password'); $co->delete('login_database'); 
    $SERVER::need_login = 1; sleep 1;
  }
  elsif ($co->param('login_id')) {
    $SERVER::set_select_user = ($co->param('login_id'));
    $SERVER::set_select_user_password = ($co->param('login_password'));
    $SERVER::set_dbname = ($co->param('login_database'));
    undef($SERVER::set_select_user) if !$SERVER::set_dbname and $main::multi_DBs;
  }
  elsif ( my $cookie_login_id = cookie('2d_core_login_i_') ) {
    my $cookie_login_password = cookie('2d_core_login_p_');
    my $cookie_login_database = cookie('2d_core_login_d_');
    $SERVER::set_select_user  = _crypting_string_($cookie_login_id, 1);
    $SERVER::set_select_user_password = _crypting_string_($cookie_login_password, 1);
    $SERVER::set_dbname = _crypting_string_($cookie_login_database, 1);
    $SERVER::need_login = 1 unless $SERVER::set_select_user;
    $SERVER::need_login = 1 if !$SERVER::set_dbname and $main::multi_DBs;
  }
  else {
    $SERVER::need_login = 1;
  }

  if ($SERVER::set_select_user) {
    my $cookie_login_id =
       $co->cookie(
                   -name=>'2d_core_login_i_',
                   -value=>_crypting_string_($SERVER::set_select_user),
                   -expires=>'+30m'
                  );
    my $cookie_login_password = 
       $co->cookie(
                   -name=>'2d_core_login_p_',
                   -value=>_crypting_string_($SERVER::set_select_user_password),
                   -expires=>'+30m'
                  );
    my $cookie_login_database = 
       $co->cookie(
                   -name=>'2d_core_login_d_',
                   -value=>_crypting_string_($SERVER::set_dbname),
                   -expires=>'+30m'
                  );
    push (@SERVER::headerCookies, ($cookie_login_id, $cookie_login_password, $cookie_login_database));

  }

  1;
}


#=============================================================================================================================#
#=============================================================================================================================#


# Display the Core free command form

sub CORE_FormFreeCommand {

  my $co = new CGI;

  print 
  $co->p.$co->hr.$co->h2("Executing a free 'pgSQL' command").
  $co->em('To be able to correctly verify your queries\' results, <u>execute only one command at a time</u>!').$co->p.
  $co->textarea
  (
      -name=>'pgsql command',
      -default=>"", 
      -rows=>"3", 
      -columns=>80
  ).
  $co->p."\n";

  my $DBSchema = "$main::url_www2d/data/database_schema";
  print $co->a({href=>"$DBSchema/all.html", target=>"_blank"}, "Database schema (all)")."&nbsp;-&nbsp;\n";
  print $co->a({href=>"$DBSchema/core_schema.html", target=>"_blank"}, "Database core schema")."&nbsp;-&nbsp;\n";
  print $co->a({href=>"$DBSchema/common_schema.html", target=>"_blank"}, "Database common schema")."&nbsp;-&nbsp;\n";
  print $co->a({href=>"$DBSchema/public_schema.html", target=>"_blank"}, "Database public schema")."\n";
  print $co->p;

  1;
}

#=============================================================================================================================#
#=============================================================================================================================#


# Display the Core Batch commands form

sub CORE_FormBatchCommand {

  my $co = new CGI;

  my ($expandSubSection, $expandSign, $buttonClass, $expandSubSectionBis, $expandSignBis, $buttonClassBis);

 (my $currentDatabaseNameFile = $DB::CurrentDatabaseName) =~ s/\s+/_/g;

  print $co->p.$co->br.$co->hr.$co->hr."\n";
  print $co->h2($co->u("Administration Commands")).$co->p."\n";
 
  $expandSubSection = 'Expand Update: Views';
 ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
  print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
        $co->strong('Managing Views, Statistics and Subtitle').$co->p;
  if ($co->param($expandSubSection) eq '+') {
    print $co->p._get_some_text_('Expand Update: Views',1).$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Update views on core schema',
      -onMouseOver=>"window.status='This will reflect any changes on the core schema to the different entries and maps views';return true"}).
      '&nbsp;&nbsp;&nbsp';
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Export to public schema',
          -onMouseOver=>"window.status='Export all changes from the core schema to the public schema';return true"}).
          '&nbsp;&nbsp;&nbsp';
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Update all and export all', 
          -onMouseOver=>"window.status='Combine the 2 previous operations -> full views update on the core schema, followed by an export to the public schema';return true"});
    print $co->p.'&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;update interface URI'.
          $co->checkbox({-name=>"update_interface_table", -checked=>"checked", -label=>"",
          -onMouseOver=>"window.status='Update the DynamicRemoteMake2DDBInterface table to contain current URIs';return true"})."\n";

    print $co->p.$co->br._get_some_text_('Expand Update: Views (2)',1)."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Update Statistics', 
          -onMouseOver=>"window.status='Update statistics to reflect your database content';return true"})."\n";

    print $co->p.$co->br._get_some_text_('Expand Update: Views (3)',1)."\n";
    my $subTitleFile =  (-s "$main::tmp_path/subtitle.html")? "$main::tmp_path/subtitle.html" :
     (-s "$main::html_path/$main::web_server_ref_name/data/subtitle.original.html")? "$main::html_path/$main::web_server_ref_name/data/subtitle.original.html"
       : undef;
    my $subTitleFileContent;
    if ($subTitleFile and open SubTitleFile , "<$subTitleFile") {
      while (<SubTitleFile>) {
        $subTitleFileContent .= $_;
      }
      close $subTitleFileContent;
    }
    print $co->submit({-class=>'coloredFixed', -name=>'Execute query', -label => 'Update Subtitle',
          -onMouseOver=>"window.status='Update the interface subtitle page \'subtitle.html\'';return true"}).$co->br.$co->br;
    print $co->textarea (-name=>'subtitle file', -default=>"$subTitleFileContent", -rows=>"3", -columns=>80).$co->br.$co->p;
    print '&nbsp;&nbsp;&nbsp;--&nbsp;you&nbsp;may&nbsp;upload&nbsp;a&nbsp;file&nbsp;from&nbsp;your&nbsp;computer:&nbsp;'."\n";
    print $co->filefield({ -name=>'uploaded subtitle file',  -size=>16, -default=>''}).'&nbsp;&nbsp;'.
          $co->submit({-name=>'Upload File', -value=> 'upload file (subtitle)',
          -onMouseOver=>"window.status='upload \'a simple HTML or text\' file to be displayed as the interface subtitle';return true"});
  }
  print $co->hr.$co->br."\n";

  $expandSubSection = 'Expand Update: External';
 ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
  print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
        $co->strong('Managing External Data').$co->p;
  if ($co->param($expandSubSection) eq '+') {
    print $co->p._get_some_text_('Expand Update: External',1).$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Update external UniProtKB data', -onMouseOver=>
          "window.status='Contact the ExPASy server to update any external data for entries cross-referencing Swiss-Prot or TrEMBL (may take quite some time)';return true"}).
          '&nbsp;&nbsp;&nbsp;'.
          $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Update external 2D data', -onMouseOver=>
          "window.status='Contact the ExPASy server to update various external 2D data and cross-references';return true"}).
          '&nbsp;&nbsp;'.$co->em({class=>'gray'}, '(this may take quite some time!)&nbsp;&nbsp;&nbsp;').$co->p;
    print '&nbsp;&nbsp;--&nbsp;&nbsp;inner data replacement&nbsp;&nbsp;'.
          $co->radio_group({-name=>'update_core_schema_argument', -values=>['low','partial', 'full'], -default=>'full',
          -onMouseOver=>"window.status='Replace your inner data by the external data (full will also force ACs, IDs and descriptions to be updated!)';return true"}).$co->p;
    print '&nbsp;&nbsp;--&nbsp;&nbsp;increment entry version on change&nbsp;'.$co->checkbox(-name=>"update_entry_version", -checked=>"checked", -label=>"");
    print '&nbsp;&nbsp;--&nbsp;&nbsp;export to public schema&nbsp;'.$co->checkbox(-name=>"update_public_schema_argument", -checked=>"checked", -label=>"");
    print $co->br;
    print $co->br.$co->em({class=>'gray'}, '* for personal cross-references: \'full\' inner data replacement will deactivate (hide) redundant cross-references, '.
                                                  'and \'low\' will re-activate them back!') if $EXTERNAL::update_internal_data;
    print $co->br.$co->em({class=>'gray'}, '* the \'$EXTERNAL::update_internal_data\' variable is deactivated in \'basic_include.pl\'.'.
                                          ' No internal protein data will be replaced!') unless $EXTERNAL::update_internal_data;
  }
  print $co->hr.$co->br."\n";

  $expandSubSection    = 'Expand Update: Update Version';
  $expandSubSectionBis = 'Expand Update: Update Version DB details';
 ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+' or $co->param($expandSubSectionBis) eq '+')?
    ('-', 'expandableOff') : ('+', 'expandableOn');
 ($expandSignBis, $buttonClassBis) = ($co->param($expandSubSectionBis) eq '+')? () : ('+', 'expandableOn');
  print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
        $co->strong('Update Entry Versions / Annotate and Publish a Database Release').$co->p;
  if ($co->param($expandSubSection) eq '+' or $co->param($expandSubSectionBis) eq '+') {
    print $co->p._get_some_text_('Expand Update: Update Entry Version',1).$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Update Entry Versions',
          -onMouseOver=>"window.status='Update your entry versions (if they have been modified)!';return true"}).$co->p."\n";
    print $co->p.$co->br._get_some_text_('Expand Update: Annotate and Publish a Database Release',1).$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Annotate and Publish a Database Release',
          -onMouseOver=>"window.status='Increase your database version number, and publish it as a new release for public access!';return true"}).'&nbsp;&nbsp;as&nbsp;a&nbsp;new&nbsp;'."\n";
    print $co->radio_group(-name=>'increment_database_release_or_sub_release', -values=>['Full release','Sub-release'],-default=>'Sub-release').$co->p."\n";
    print "&nbsp;&nbsp;&nbsp&nbsp;".(($co->param($expandSubSection) eq '+')? $co->submit({-class=>$buttonClassBis, -name=>$expandSubSectionBis, -label => $expandSignBis }) : '')."&nbsp;&nbsp;&nbsp;".$co->strong("Database Release details");
    print $co->p;
    my ($previousDBdescription, $previousDBreleaseDate, $currentDate, $previousDBreleaseNotes, $previousDBcopyright, $previousDBcontact) =
    &EXECUTE_FAST_COMMAND(
     "SELECT databaseDescription, databaseReleaseDate, CURRENT_DATE, databaseReleaseNotes, copyright, contact FROM ".
      $SERVER::commonSchema.".Database");
    my $databaseURIText = $SERVER::interfaceWebAddress;
    if ($main::core_interface_prefix) {
      $databaseURIText =~ s/\.$main::core_interface_prefix\.(\w+)$/.$1/;
    }
    if ($co->param($expandSubSectionBis) eq '+') {
      print $co->start_table()."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;release&nbsp;increment:&nbsp;");
      print $co->td($co->radio_group( -name=>'database release increment', -values=>['increase', 'decrease', 'no modification'], -default=>'increase')).$co->end_Tr."\n";
      print $co->Tr($co->td('&nbsp;').$co->td('&nbsp;'))."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;new&nbsp;release&nbsp;date:&nbsp;");
      print $co->td($co->textfield({ -name=>'database release date',  -size=>10, -default=>$currentDate})).$co->end_Tr."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;database&nbsp;release&nbsp;notes:&nbsp;");
      print $co->td($co->textarea({ -name=>'database release notes',  -rows=>2, -columns=>64, -default=>$previousDBreleaseNotes})).$co->end_Tr."\n".$co->p."\n";
      print $co->Tr($co->td('&nbsp;').$co->td('&nbsp;'))."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;contact:&nbsp;");
      print $co->td($co->textfield({ -name=>'database contact',  -size=>32, -default=>$previousDBcontact})).$co->end_Tr."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;database&nbsp;description".$co->sup('1').":&nbsp;"); 
      print $co->td($co->textarea({ -name=>'database description',  -rows=>2, -columns=>64, -default=>$previousDBdescription})).$co->end_Tr."\n".$co->p."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;database&nbsp;copyright".$co->sup('2').":&nbsp;");
      print $co->td($co->textarea({ -name=>'database copyright',  -rows=>2, -columns=>64, -default=>$previousDBcopyright})).$co->end_Tr."\n".$co->p."\n";
      print $co->start_Tr.$co->td("&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp&nbsp;--&nbsp;database&nbsp;interface&nbsp;URL:&nbsp;");
      my $databaseURIText = $SERVER::interfaceWebAddress;
      if ($main::core_interface_prefix) {
        $databaseURIText =~ s/\.$main::core_interface_prefix\.(\w+)$/.$1/;
      }
      my $databaseURITextlength = (length($databaseURIText) > 16)? length($databaseURIText) : 16;
      print $co->td($co->textfield({ -name=>'database interface URI',  size=>$databaseURITextlength, -default=>$databaseURIText})).$co->end_Tr."\n".$co->p."\n";
      print $co->end_table."\n";
      print $co->br.$co->em({class=>'gray'}, $co->sup('1').' cf. also section [1], the HTML Subtitle file ').
            $co->strong('\''.$main::html_path.'/'.$main::web_server_ref_name.'/data/tmp/subtitle.html\'').
            $co->em({class=>'gray'}, ' to be displayed (included) on the interface home page.')."\n";
      print $co->br.$co->em({class=>'gray'}, $co->sup('2').' you may also use HTML tags.')."\n";

    }
    else {
      $co->param(-name=>'database description', -values=>[$previousDBdescription]); print $co->hidden('database description');
      $co->param(-name=>'database release date', -values=>[$currentDate]); print $co->hidden('database release date');
      $co->param(-name=>'database release notes', -values=>[$previousDBreleaseNotes]); print $co->hidden('database release notes');
      $co->param(-name=>'database interface URI', -values=>[$databaseURIText]); print $co->hidden('database interface URI');
      $co->param(-name=>'database contact', -values=>[$previousDBcontact]); print $co->hidden('database contact');
      $co->param(-name=>'database copyright', -values=>[$previousDBcopyright]); print $co->hidden('database copyright');
    }
  }
  print $co->hr.$co->br."\n";

  $expandSubSection = 'Expand Update: Export Data';
 ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
  print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
        $co->strong('Export/Backup Data').$co->p;
  if ($co->param($expandSubSection) eq '+') {
    print $co->p._get_some_text_('Expand Update: Export Data',1).$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Export flat file',
          -onMouseOver=>"window.status='Export your entries annotations sequentially into a text flat file!';return true"})."\n";
    print '--&nbsp;&nbsp;to&nbsp;file&nbsp;name&nbsp;(do&nbsp;not&nbsp;include&nbsp;any&nbsp;path)&nbsp;&nbsp;'.
          $co->textfield({ -name=>'export to flat file',  -size=>24, -default=>$currentDatabaseNameFile.'.dat'}).
          '&nbsp;&nbsp;--&nbsp;&nbsp;separate&nbsp;spot&nbsp;data&nbsp;'.$co->checkbox(-name=>"separate flat SPOT_DATA", -checked=>"checked", -label=>"").
          '&nbsp;&nbsp;--&nbsp;&nbsp;'.
          $co->radio_group(-name=>'file_type', -values=>['Unix','Windows/DOS'],-default=>'Unix').$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Export/Backup all database',
          -onMouseOver=>"window.status='Export or backup all of your database into a postgreSQL dump file!';return true"})."\n";
    print '--&nbsp;&nbsp;to&nbsp;file&nbsp;name&nbsp;(do&nbsp;not&nbsp;include&nbsp;any&nbsp;path)&nbsp;&nbsp;'.
          $co->textfield({ -name=>'export to dump file',  -size=>24, -default=>$currentDatabaseNameFile.'.dump'})."\n";
  }
  print $co->hr.$co->br."\n";

  $expandSubSection = 'Expand Update: Hide Data';
 ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
  print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
        $co->strong('Hide/Show Data').$co->p;
  if ($co->param($expandSubSection) eq '+') {
    my $hiddenGelsFileDefault = (-r $main::tmp_path."/$DB::CurrentDatabaseName/".$main::hiddenGelsFile)? $main::tmp_path."/$DB::CurrentDatabaseName/".$main::hiddenGelsFile : '';
    my $hiddenGelsFileDefaultTrunc = ($hiddenGelsFileDefault =~ /([^\/]+)$/)? $1 : '';
    my $hiddenEntriesFileDefault = (-r $main::tmp_path."/$DB::CurrentDatabaseName/".$main::hiddenEntriesFile)? $main::tmp_path."/$DB::CurrentDatabaseName/".$main::hiddenEntriesFile : '';
    my $hiddenEntriesFileDefaultTrunc = ($hiddenEntriesFileDefault =~ /([^\/]+)$/)? $1 : '';
    my $hiddenSpotsFileDefault = (-r $main::tmp_path."/$DB::CurrentDatabaseName/".$main::hiddenSpotsFile)? $main::tmp_path."/$DB::CurrentDatabaseName/".$main::hiddenSpotsFile : '';
    my $hiddenSpotsFileDefaultTrunc = ($hiddenSpotsFileDefault =~ /([^\/]+)$/)? $1 : '';
    print $co->p._get_some_text_('Expand Update: Hide Data',1).$co->p.$co->br."\n";
    print '&nbsp;&nbsp;'.$co->span({-class=>'underlined'}, 'Hide&nbsp;gels&nbsp;read&nbsp;from&nbsp;file&nbsp;(include&nbsp;path),'.
                '&nbsp;or&nbsp;leave&nbsp;empty&nbsp;to&nbsp;unhide&nbsp;all')."\n";
    print $co->start_table({-border=>0}).$co->start_Tr()."\n";
    print $co->td($co->submit({-class=>'coloredFixed', -name=>'Execute query', -label => 'Hide Gels',
          -onMouseOver=>"window.status='Hide entire gels from public access!';return true"}));
    print $co->td('&nbsp;&nbsp;&nbsp;--&nbsp;file&nbsp;path:&nbsp;');
    print $co->td($co->textfield({ -name=>'hidden gels file',  -size=>56, -default=>$hiddenGelsFileDefault})).
          $co->td($co->submit({-name=>'View File', -value=> 'display file (gels)',
          -onMouseOver=>"window.status='view \'hide gels\' file';return true"}));
    print $co->end_Tr.$co->end_table.$co->p."\n";
    print $co->start_table({-border=>0}).$co->start_Tr();
    print $co->td('&nbsp;&nbsp;&nbsp;--&nbsp;you&nbsp;may&nbsp;also&nbsp;upload&nbsp;a&nbsp;file&nbsp;from&nbsp;your&nbsp;computer:&nbsp;')."\n";
    print $co->td($co->filefield({ -name=>'uploaded hidden gels file',  -size=>16, -default=>''})).
          $co->td($co->submit({-name=>'Upload File', -value=> 'upload file (gels)',
          -onMouseOver=>"window.status='upload \'hide gels\' file';return true"}));
    print $co->end_Tr.$co->end_table.$co->br.$co->p."\n";
    print '&nbsp;&nbsp;'.$co->span({-class=>'underlined'}, 'Hide&nbsp;entries&nbsp;read&nbsp;from&nbsp;file&nbsp;(include&nbsp;path),'.
                '&nbsp;or&nbsp;leave&nbsp;empty&nbsp;to&nbsp;unhide&nbsp;all')."\n";
    print $co->start_table({-border=>0}).$co->start_Tr()."\n";
    print $co->td('&nbsp;'.$co->submit({-class=>'coloredFixed', -name=>'Execute query', -label => 'Hide Entries',
          -onMouseOver=>"window.status='Hide some entries from public access!';return true"}));
    print $co->td('&nbsp;&nbsp;&nbsp;--&nbsp;file&nbsp;path:&nbsp;');
    print $co->td($co->textfield({ -name=>'hidden entries file',  -size=>56, -default=>$hiddenEntriesFileDefault})).
          $co->td('&nbsp;'.$co->submit({-name=>'View File', -value=> 'display file (entries)',
          -onMouseOver=>"window.status='view \'hide entries\' file';return true"}));
    print $co->end_Tr.$co->end_table.$co->p."\n";
    print $co->start_table({-border=>0}).$co->start_Tr();
    print $co->td('&nbsp;&nbsp;&nbsp;--&nbsp;you&nbsp;may&nbsp;also&nbsp;upload&nbsp;a&nbsp;file&nbsp;from&nbsp;your&nbsp;computer:&nbsp;')."\n";
    print $co->td($co->filefield({ -name=>'uploaded hidden entries file',  -size=>16, -default=>''})).
          $co->td($co->submit({-name=>'Upload File', -value=> 'upload file (entries)',
          -onMouseOver=>"window.status='upload \'hide entries\' file';return true"}));
    print $co->end_Tr.$co->end_table.$co->br.$co->p."\n";
    print '&nbsp;&nbsp;'.$co->span({-class=>'underlined'}, 'Hide&nbsp;spot&nbsp;data&nbsp;from&nbsp;file&nbsp;(include&nbsp;path),'.
                '&nbsp;or&nbsp;leave&nbsp;empty&nbsp;to&nbsp;unhide&nbsp;all')."\n";
    print $co->start_table({-border=>0}).$co->start_Tr()."\n";
    print $co->td($co->submit({-class=>'coloredFixed', -name=>'Execute query', -label => 'Hide Spots',
          -onMouseOver=>"window.status='Hide spot related data from public access!';return true"}));
    print $co->td('&nbsp;&nbsp;&nbsp;--&nbsp;file&nbsp;path:&nbsp;');
    print $co->td($co->textfield({ -name=>'hidden spots file',  -size=>56, -default=>$hiddenSpotsFileDefault})).
          $co->td('&nbsp;'.$co->submit({-name=>'View File', -value=> 'display file (spots)',
          -onMouseOver=>"window.status='view \'hide spots\' file';return true"}));
    print $co->end_Tr.$co->end_table.$co->p."\n";
    print $co->start_table({-border=>0}).$co->start_Tr();
    print $co->td('&nbsp;&nbsp;&nbsp;--&nbsp;you&nbsp;may&nbsp;also&nbsp;upload&nbsp;a&nbsp;file&nbsp;from&nbsp;your&nbsp;computer:&nbsp;')."\n";
    print $co->td($co->filefield({ -name=>'uploaded hidden spots file',  -size=>16, -default=>''})).
          $co->td($co->submit({-name=>'Upload File', -value=> 'upload file (spots)',
          -onMouseOver=>"window.status='upload \'hide spots\' file';return true"}));
    print $co->end_Tr.$co->end_table.$co->br.$co->p."\n";
  }
  print $co->hr.$co->br."\n";

  $expandSubSection = 'Expand Update: Clean DB';
 ($expandSign, $buttonClass) = ($co->param($expandSubSection) eq '+')? ('-', 'expandableOff') : ('+', 'expandableOn');
  print $co->submit({-class=>$buttonClass, -name=>$expandSubSection, -label => $expandSign })."&nbsp;&nbsp;&nbsp".
        $co->strong('Clean Database').$co->p;
  if ($co->param($expandSubSection) eq '+') {
    print $co->p._get_some_text_('Expand Update: Clean DB',1).$co->p."\n";
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Vacuum Database',
          -onMouseOver=>"window.status='Perform a VACUUM FULL ANALYZE on your database to optimize performance';return true"}).
          '&nbsp;'.$co->em({class=>'gray'}, '(this may take quite some time!)&nbsp;&nbsp;&nbsp;').$co->p;
    print $co->submit({-class=>'colored', -name=>'Execute query', -label => 'Empty the Backup (log) schema',
          -onMouseOver=>"window.status='Initialize the Backup (log) schema from all previous modifications / export old modifications to a dump file';return true"}).
          '&nbsp;&nbsp;&nbsp;--&nbsp;&nbsp;dump&nbsp;to&nbsp;file&nbsp;(do&nbsp;not&nbsp;include&nbsp;any&nbsp;path)&nbsp;&nbsp;'.
          $co->textfield({ -name=>'export log to file', -size=>24, -default=>$currentDatabaseNameFile.'_log_schema.log'});
  }

  print "\n";
  1;
}

#=============================================================================================================================#
#=============================================================================================================================#


# Execute the Core Batch commands

# It is frequent to execute some update commands more than once,
# as they would not finish before subsequent commands are launched!

sub CORE_ExecuteBatchCommand {

  my $co = new CGI;
  my $command;

  # passed from the 'core' script

  my $backgroundUpdateMsgCmd = "SELECT 'update is being performed in the background' AS Database_update";

  if ( $co->param('update_interface_table') and ($co->param('Execute query') eq 'Update views on core schema' or
       $co->param('Execute query') eq 'Export to public schema' or $co->param('Execute query') eq 'Update all and export all') ) {
    my $core_interface_prefix = $main::core_interface_prefix;
   (my $interfaceURI = $ENV{SCRIPT_URI}) =~ s/\.$core_interface_prefix(\.\w+)$/$1/i;
    &EXECUTE_FAST_COMMAND("UPDATE ".$SERVER::coreSchema.".DynamicRemoteMake2DDBInterface SET interfaceURI = '$interfaceURI', ".
                          "DBNumber = $DB::CurrentDatabase, DBNAme = '$DB::CurrentDatabaseName' WHERE interfaceID = 0") if $interfaceURI; # interfaceID = 0 is the local interface
  }

  if ($co->param('Execute query') eq 'Update views on core schema') {
    $command = 'SELECT '.$SERVER::coreSchema.'.make2db_update(1,0)';
  }

  elsif ($co->param('Execute query') eq 'Export to public schema') {
    $command = 'SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)';
  }

  elsif ($co->param('Execute query') eq 'Update all and export all') {
    $command = 'SELECT '.$SERVER::coreSchema.'.make2db_update(2,1)'; 
  }

  elsif ($co->param('Execute query') eq 'Update Statistics') {
    my $statistics = &GENERATE_STATISTICS_HTML();
    $statistics =~ s/'/\\'/gs;
    $command = "UPDATE Common.Database SET databaseMainSummary = '$statistics'";
    &EXECUTE_FAST_COMMAND("$command");
    print $co->h3("Statistics update has been performed!");
    undef($command);
  }

  elsif ($co->param('Execute query') eq 'Update Subtitle') {
    my $subTitleContent = $co->param('subtitle file');
    my $subTitleFile = "$main::tmp_path/subtitle.html";
    my $fileMoved;
    if (-e "$subTitleFile") {
      rename ($subTitleFile, $subTitleFile.'.old');
      $fileMoved =  " (previous version moved to '$subTitleFile.old')";
    }
    if ($subTitleFile) {
      open SubTitle, ">$subTitleFile";
      print SubTitle $subTitleContent;
      close SubTitle;
      print $co->h3("Sub-title file: 'subtitle.html' has been replaced!$fileMoved");
    } else {
      print $co->h3("Sub-title file: 'subtitle.html' has been emptied!$fileMoved");
    }
  }

  elsif ($co->param('Execute query') eq 'Update external UniProtKB data'
      or $co->param('Execute query') eq 'Update external 2D data') {
    my $updateUniProt = ($co->param('Execute query') eq 'Update external UniProtKB data')? 1 : 0;
    my $update2D = ($co->param('Execute query') eq 'Update external 2D data')? 1 : 0;
    my $externalUpdated = 0;
    my $update_core_schema_argument = ($co->param('update_core_schema_argument') eq 'low')? 1 :
      ($co->param('update_core_schema_argument') eq 'partial')? 2 : ($co->param('update_core_schema_argument') eq 'full')? 3 : 2;
    $update_core_schema_argument = 1 unless $EXTERNAL::update_internal_data;
    my $update_public_schema_argument = $co->param('update_public_schema_argument')? 1 : 0;
    $externalUpdated = &CORE_UpdateExternalUniProtData()
      if $updateUniProt and $EXTERNAL::update_from_SwissProt;
    $externalUpdated+= &CORE_UpdateExternalMake2DDBData({updateCoreSchema=>$update_core_schema_argument})
      if $update2D and $EXTERNAL::update_from_Make2DDB;
    if ($externalUpdated) {
      if ($co->param('update_entry_version')) {
        sleep 3;
        my ($updateEntryVersionsStatus) = &EXECUTE_FAST_COMMAND("SELECT ".$SERVER::coreSchema.".make2db_update_version(TRUE)");
        $updateEntryVersionsStatus = ($updateEntryVersionsStatus)? 'update performed with success' : 'no modification';
        print "- Updating entry versions: $updateEntryVersionsStatus!<br>\n" 
      }
      sleep 3;
      $command = "SELECT ".$SERVER::coreSchema.".make2db_update($update_core_schema_argument, $update_public_schema_argument)";
      # changing ACs in cascade takes time.. and is *NOT* synchrnoized with the rest!! -> launch several times in background, or!!..
      &EXECUTE_FAST_COMMAND("DO: SELECT ".$SERVER::coreSchema.".make2db_update_internal_primaryAC (1)"); sleep 3;
      print $co->p;
      &EXECUTE_FORMATTED_COMMAND({command=>$backgroundUpdateMsgCmd, show_number_rows_affected=>1}) if $backgroundUpdateMsgCmd;
    }
    # update cross-references URLs
    my $response = ($updateUniProt)? LWP_AGENT('GET', "$main::expasy_GL_txt_reader") : undef;
    if (defined($response)) {
      $response =~ s/^\W[^\n]*\n?//mg;
      $response =~ s/http:\/\/expasy\.org/$main::expasy/mg if $main::expasy;
      open DR_TXT, (">$main::tmp_path/links.txt.uniprot");
      print DR_TXT $response;
      close DR_TXT;
      use File::Copy;
      copy("$main::tmp_path/links.txt.uniprot", "./inc/links.txt.uniprot");
    }
    print $co->br.$co->p;
  }

  elsif ($co->param('Execute query') eq 'Update Entry Versions' or $co->param('Execute query') eq 'Annotate and Publish a Database Release') {
    $command = "set search_path to core,common,public; SELECT ".$SERVER::coreSchema.".make2db_update_version(TRUE)";
    my ($updateEntryVersionsStatus) = &EXECUTE_FAST_COMMAND("$command");
    my $updateVersionMessage = 'Updating entry versions: '.(($updateEntryVersionsStatus)? 'update performed with success' : 'no modifications').'!';
    if ($co->param('Execute query') eq 'Annotate and Publish a Database Release') {
      sleep 3;
      my $databaseTable = $SERVER::commonSchema.'.database';
      my ($databaseDescription, $databaseReleaseDate, $databaseReleaseNotes, $databaseInterfaceURI, $databaseContact, $databaseCopyright) =
       (quotemeta($co->param('database description')), $co->param('database release date'),
        quotemeta($co->param('database release notes')), quotemeta($co->param('database interface URI')),
        quotemeta($co->param('database contact')), quotemeta($co->param('database copyright')));
      $command = "UPDATE $databaseTable SET".
        " databaseDescription = '$databaseDescription', databaseReleaseDate = '$databaseReleaseDate', databaseReleaseNotes = '$databaseReleaseNotes',".
        " contact = '$databaseContact', databaseInterfaceURI = '$databaseInterfaceURI', copyright = '$databaseCopyright'";
      my ($updateDatabaseVersionStatus) = &EXECUTE_FAST_COMMAND("$command");
      $updateVersionMessage.= '<br>Updating database general information: '.(($updateDatabaseVersionStatus)? 'update performed with success' : 'no modifications').'!';
      my $databaseIncrement = 1;
         $databaseIncrement = ($co->param('database release increment') eq 'increase')? 1 :
                              ($co->param('database release increment') eq 'decrease')? -1 :
                              ($co->param('database release increment') eq 'no modification')? 0 :  $databaseIncrement;
      if ($databaseIncrement) {
        my $releaseOrSubReleaseColumn = my $resetSubRelease = '';
        if (($co->param('increment_database_release_or_sub_release') eq 'Full release')) {
          $releaseOrSubReleaseColumn = 'databaseRelease';
          $resetSubRelease = ', databaseSubRelease = 0';
        } else {
          $releaseOrSubReleaseColumn =  'databaseSubRelease';
        }
        $command = "UPDATE $databaseTable SET $releaseOrSubReleaseColumn = $releaseOrSubReleaseColumn + 1".$resetSubRelease;
       ($updateDatabaseVersionStatus) = &EXECUTE_FAST_COMMAND("$command");
        $updateVersionMessage.= '<br>Updating database '.$co->param('increment_database_release_or_sub_release').': '.
         (($updateDatabaseVersionStatus)? 'modified' : 'no modifications').'!';
        my $statistics = &GENERATE_STATISTICS_HTML();
        $statistics =~ s/'/\\'/gs;
        $command = "UPDATE Common.Database SET databaseMainSummary = '$statistics'";
        &EXECUTE_FAST_COMMAND("$command");
        $updateVersionMessage.='<br>Statistics update has been performed!';
        undef($command);
      }
    }
    print $co->h3("$updateVersionMessage");
    undef($command);
  }

  elsif ($co->param('Execute query') eq 'Export flat file' and $co->param('export to flat file')) {
    # SELECT core.make2db_export_ascii_entries ('all', 75, '$file_psql.tmp', '') is not used due to permissions conflicts between pg anf http users!
    my $failed;
    my $file = $co->param('export to flat file');
       $file =~ s/.+\///; # truncate any path before file name /
       $file = 'last_flat_file.dat' unless $file;
       $file = "$main::tmp_path/$file";
    rename ("$file", "$file.old") if -s $file;
    my $windows = ($co->param('file_type') =~ /Windows|DOS/i)? 1 : 0;
    $command = 'SELECT AC FROM Entry WHERE showFlag = \'true\' ORDER BY ID';
    my @ACs = &EXECUTE_FAST_COMMAND("$command");
    open (DAT, ">$file") or $failed = 1;
    unless ($failed) {
      my %month = ( "1" => "January", "2" => "February", "3" => "March", 
                    "4" => "April",   "5" => "May",      "6" => "June", 
                    "7" => "July",    "8" => "August",   "9" => "September", 
                    "10" => "October", "11" => "November", "12" => "December" );
      my $lineF = ($windows)? "\r\n" : "\n"; 
      $command = "SET DATESTYLE TO 'POSTGRES, EUROPEAN'; ".
                 "SELECT databaseName, databaseRelease, databaseSubRelease, ".
                 "date_part('month',databaseReleaseDate), date_part('year',databaseReleaseDate) ".
                 "FROM common.Database LIMIT 1";
      my ($dbName, $dbRel, $dbSubRel, $dbDateMonth, $dbDateYear) = &EXECUTE_FAST_COMMAND("$command");
      $dbDateMonth = $month{$dbDateMonth} if exists($month{$dbDateMonth});
      my $header = "$dbName Release $dbRel.$dbSubRel$lineF$dbDateMonth $dbDateYear$lineF$lineF";
      print DAT $header;
      foreach my $ac (@ACs) {
        $command = "SELECT ".$SERVER::commonSchema.".make2db_ascii_entry(\'$ac\', 75, '')";
       (my $entry) = &EXECUTE_FAST_COMMAND("$command");
        next unless $entry;
        $entry = _insert_entry_copyright_text_($entry, 1);
        $entry =~ s/\n/\r\n/g if $windows;
        print DAT $entry."$lineF";
      }
    }
    my $returnChar = ($windows)? "\r\n" : "\n";
    my $fileSpotData;
    if ($co->param('separate flat SPOT_DATA')) {
      close DAT;
      $fileSpotData = $file;
      if ($fileSpotData =~ /\.[^\.]+$/) {
        $fileSpotData =~ s/\.([^\.]+)$/.spot_data.$1/;
      } else {
        $fileSpotData.= '.spot_data'
      }
      rename ("$fileSpotData", "$fileSpotData.old") if -s $fileSpotData;
      open (DAT, ">>$fileSpotData") or $failed = 1;
    }
    print DAT "__SPOT_DATA__(SYNTAX: Master Spot X Y pI Mw \%Od \%Vol)$returnChar";
    $command = 'SELECT Gel.shortName, Spot.spotID, Spot.xCoordinate, Spot.yCoordinate, Spot.pI, Spot.mw, '.
               'Spot.odRelative, Spot.volumeRelative FROM Spot, Gel WHERE Gel.gelID = Spot.gelID ORDER BY 1,2';
    my @spotData = &EXECUTE_FAST_COMMAND("$command");
    for (my $ii = 0; $ii <= (scalar(@spotData) - 8); $ii += 8) {
      my $spotDataLine = '';
      for (my $jj = 0; $jj < 8; $jj++) {
        $spotDataLine .= $spotData[$ii+$jj]."\t";
      }
      $spotDataLine =~ s/\t$/$returnChar/;
      print DAT $spotDataLine;
    }
    print DAT "__END_SPOT_DATA__$returnChar";
    close DAT;
    my $OKmessage = "Flat file exported to \'$file\'".(($fileSpotData)? " and to \'$fileSpotData\'" : '').'!';
    print my $string = ($failed)? $co->h3("Could not export flat file to \'$file\'!") : $co->h3($OKmessage);

    undef($command);
  }

  elsif ($co->param('Execute query') eq 'Export/Backup all database' and $co->param('export to flat file')) {
    my $failed;
    my $file = $co->param('export to dump file');
       $file =~ s/.+\///; # truncate any path before file name /
       $file = "$main::tmp_path/$file";
    rename ($file, "$file.old") if -s $file;
    unless (do "include.cfg") {
      print $co->h3("Could not read include.cfg to determine which 'pg_dump' is to be used - the default 'pg_dump' will be used!");
    }
    $POSTGRES::bin .= "/" unless !$POSTGRES::bin or $POSTGRES::bin =~ m!/$!;
    &IMPORT_DATABASE_SPECIFIC_PARAMETER unless $DB::set_select_user;
    open (DUMP_WRITE, ">$file") or $failed = 1;
    unless ($failed) {
      open (DUMP_READ, "$POSTGRES::bin"."pg_dump --create $DB::CurrentDatabaseName |") or $failed = 1;
      unless ($failed) {
        while (<DUMP_READ>) {
          print DUMP_WRITE $_;
        }
        close DUMP_READ;
      }
      close DUMP_WRITE;
    }
    print my $string = ($failed)? $co->h3("Could not dump database to \'$file\'!") : $co->h3("Database content dumped and exported to \'$file\'!");
  }

  # Hidden Data Treatment
  #---------------------#
  # Hidden Gels
  elsif ($co->param('Execute query') eq 'Hide Gels') {
    my (%hiddenData, $proceedUnhideData, $proceedHideData);
    my $hideDataFile = $co->param('hidden gels file');
    if (!$hideDataFile) {
      $proceedUnhideData = 1;
    }
    elsif (open (HiddenData, "<$hideDataFile")) {
      while (my $hLine = <HiddenData>) {
        next if $hLine =~ /^\s*\#/;
        $hLine =~ s/\r|\n|\t/ /g;
        $hLine =~ s/\s+/ /g;
        grep {$hiddenData{uc($_)} = 1} split ' ', $hLine;
      }
      close HiddenData;
      if (scalar (keys %hiddenData) > 1000) {
        print $co->h3("Hiding Gels: the file $hideDataFile contains too many data.".
                      "<br>Please, check the file content.<br>Operation aborted!");
      }
      else {
        $proceedHideData = 1;
      }
    }
    elsif (-e $hideDataFile) {
      print $co->h3("Hiding Gels: Could not read $hideDataFile.<br>Operation aborted!");
    }
    elsif (!-e $hideDataFile) {
      print $co->h3("Hiding Gels: Could not find $hideDataFile (no such file).<br>Operation aborted!");
    }

    my $unhideCommand = "UPDATE Gel SET showFlag = TRUE";
    if ($proceedUnhideData) {
      &EXECUTE_FAST_COMMAND("$unhideCommand");
      print $co->h3("Unhiding Gels: All your gels are now publicly accessible!");
    }
    elsif ($proceedHideData) {
      $command = "UPDATE Gel SET showFlag = FALSE WHERE shortName = ";
      if ($SERVER::noDBIplaceholders) {
        &EXECUTE_FAST_COMMAND("$unhideCommand"); sleep 3;
        foreach my $hiddenElement (keys %hiddenData) {
          $hiddenElement = quotemeta($hiddenElement);
          &EXECUTE_FAST_COMMAND($command."'$hiddenElement'");
        }
        copy ($hideDataFile, $hideDataFile.'.lastProcessed') if (-e $hideDataFile);
      }
      else {
        &PG_OPEN_CONNECTION({command=>'open'});
        my ($sth, $sth1, $sth2);
        eval {
          $sth = $DBI::openedConn->prepare("$unhideCommand");
          $sth ->execute(); sleep 3;
          $sth1 = $DBI::openedConn->prepare_cached($command."?");
          foreach my $hiddenElement (keys %hiddenData) {
            $hiddenElement = quotemeta($hiddenElement);
              $sth1->bind_param(1, $hiddenElement);
              $sth1->execute();
          }
          # because is *NOT* performed in the final command??!! -> included here in the eval as an extra execution
          $sth2 = $DBI::openedConn->prepare('SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)');
          $sth2->execute();
          $DBI::openedConn->commit;
        };
        if ($@) {
          print $co->h3("Hiding Gels: A problem occured, operation aborted!");
          $DBI::openedConn->rollback;
        }
        else {
          print $co->h3("Hiding Gels: Operation correctly performed!");
          use File::Copy;
          copy ($hideDataFile, $hideDataFile.'.lastProcessed') if (-e $hideDataFile);
        }
        $sth-> finish() if $sth;
        $sth1->finish() if $sth1;
        $sth2->finish() if $sth2;
      }
    }
    undef($command); 
    sleep 3;
    # export to public schema
    $command = 'SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)' if $proceedUnhideData or $proceedHideData;
  }
  # Hidden Entries
  elsif ($co->param('Execute query') eq 'Hide Entries') {
    my (%hiddenData, $proceedUnhideData, $proceedHideData);
    my $hideDataFile = $co->param('hidden entries file');
    if (!$hideDataFile) {
      $proceedUnhideData = 1;
    }
    elsif (open (HiddenData, "<$hideDataFile")) {
      while (my $hLine = <HiddenData>) {
        next if $hLine =~ /^\s*\#/;
        $hLine =~ s/\r|\n|\t/ /g;
        $hLine =~ s/\s+/ /g;
        grep {$hiddenData{uc($_)} = 1} split ' ', $hLine;
      }
      close HiddenData;
      if (scalar (keys %hiddenData) > $CORE::maxElementsToHide) {
        print $co->h3("Hiding Entries: the file $hideDataFile contains too many data.".
                      "<br>Please, check the file content.<br>Operation aborted!");      
      }
      else {
        $proceedHideData = 1;
      }
    }
    elsif (-e $hideDataFile) {
      print $co->h3("Hiding Entries: Could not read $hideDataFile.<br>Operation aborted!");
    }
    elsif (!-e $hideDataFile) {
      print $co->h3("Hiding Entries: Could not find $hideDataFile (no such file).<br>Operation aborted!");
    }

    my $unhideCommand = "UPDATE Entry SET showFlag = TRUE";     
    if ($proceedUnhideData) {
      &EXECUTE_FAST_COMMAND("$unhideCommand");
      print $co->h3("Unhiding Entries: All your entries are now publicly accessible!");
    }
    elsif ($proceedHideData) {
      $command = "UPDATE Entry SET showFlag = FALSE WHERE AC = ".$SERVER::commonSchema.".make2db_primary_accession(";
      if ($SERVER::noDBIplaceholders) {
        &EXECUTE_FAST_COMMAND("$unhideCommand"); sleep 3;
        foreach my $hiddenElement (keys %hiddenData) {
          $hiddenElement = quotemeta($hiddenElement);
          &EXECUTE_FAST_COMMAND($command."'$hiddenElement')");
        }
        print $co->h3("Hiding Entries: Operation performed! (no return status)");
        copy ($hideDataFile, $hideDataFile.'.lastProcessed') if (-e $hideDataFile);
      }
      else {
        &PG_OPEN_CONNECTION({command=>'open'});
        my ($sth, $sth1, $sth2);
        eval {
          $sth = $DBI::openedConn->prepare("$unhideCommand");
          $sth ->execute(); sleep 3;
          $sth1 = $DBI::openedConn->prepare_cached($command."?)");
          foreach my $hiddenElement (keys %hiddenData) {
            $hiddenElement = quotemeta($hiddenElement);
              $sth1->bind_param(1, $hiddenElement);
              $sth1->execute();
          }
          # because is *NOT* performed in the final command??!! -> included here in the eval as an extra execution
          $sth2 = $DBI::openedConn->prepare('SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)');
          $sth2->execute();
          $DBI::openedConn->commit;
        };
        if ($@) {
          print $co->h3("Hiding Entries: A problem occured, operation aborted!");
          $DBI::openedConn->rollback;
        }
        else {
          print $co->h3("Hiding Entries: Operation correctly performed!");
          copy ($hideDataFile, $hideDataFile.'.lastProcessed') if (-e $hideDataFile);
        }
        $sth-> finish() if $sth;
        $sth1->finish() if $sth1;
        $sth2->finish() if $sth2;
      }
    }
    undef($command); 
    sleep 3;
    # export to public schema
    $command = 'SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)' if $proceedUnhideData or $proceedHideData;
  }
  # Hidden Spots
  elsif ($co->param('Execute query') eq 'Hide Spots') {
    my (%hiddenData, $hiddenData, $proceedUnhideData, $proceedHideData);
    my $hideDataFile = $co->param('hidden spots file');
    if (!$hideDataFile) {
      $proceedUnhideData = 1;
    }
    elsif (open (HiddenData, "<$hideDataFile")) {
      while (my $hLine = <HiddenData>) {
        /()/;
        next if $hLine =~ /^\s*\#/ or $hLine !~ /^\s*(\S+)\s+(\S+)\s+(\S+)(?:\s+(\S+))?/;
        my ($hAC, $hGel, $hSpot, $hIdent) = (uc($1),uc($2),$3,uc($4));
        my $hSpotGel = $hGel."::".$hSpot;
        if ($hIdent) { # hide identifications
          next unless $hIdent =~ /^PMF|MS(?:\W?MS)?|AA$/i;
          $hIdent = 'MS/MS' if $hIdent =~ /^MS/i;
          $hiddenData->{Identification}->{$hSpotGel}->{$hAC}->{$hIdent} = 1;
        }
        elsif ($hSpot eq '*') { # hide all spots on gel for entry
          $hiddenData->{SpotsEntry}->{$hGel}->{$hAC} = 1;
        }
        else { # hide a spot for entry
          $hiddenData->{SpotEntry}->{$hSpotGel}->{$hAC} = 1
        }
      }
      close HiddenData;
      if (scalar (keys %hiddenData) > ($CORE::maxElementsToHide*5)) {
        print $co->h3("Hiding Spots: the file $hideDataFile contains too many data.".
                      "<br>Please, check the file content.<br>Operation aborted!");      
      }
      else {
        $proceedHideData = 1;
      }
    }
    elsif (-e $hideDataFile) {
      print $co->h3("Hiding Spots: Could not read $hideDataFile.<br>Operation aborted!");
    }
    elsif (!-e $hideDataFile) {
      print $co->h3("Hiding Spots: Could not find $hideDataFile (no such file).<br>Operation aborted!");
    }

    my $unhideCommand = "UPDATE SpotEntry SET showFlag = TRUE; ".
                        "UPDATE EntryGelImage SET showFlag = TRUE; UPDATE EntryGelMaster SET showFlag = TRUE;".
                        "UPDATE SpotDataParent* SET showFlag = TRUE; UPDATE SpotIdentificationParent* SET showFlag = TRUE;";
    if ($proceedUnhideData) {
      &EXECUTE_FAST_COMMAND("$unhideCommand");
      print $co->h3("Unhiding Spots: All your spots data are now publicly accessible!");
    }
    elsif ($proceedHideData) {
      if ($SERVER::noDBIplaceholders) {
        print $co->h3("Hiding Spots: DBI Place Holders should not be deactivated in your server configuration file!<br>Operation aborted!");
        # &EXECUTE_FAST_COMMAND("$unhideCommand"); sleep 3;
        $proceedHideData = 0;
      }
      else {
        my (%gelID, @gelIDs);
        (@gelIDs) = &EXECUTE_FAST_COMMAND("SELECT upper(shortName), gelID FROM Gel");
        for (my $ii=0; $ii < scalar @gelIDs; $ii+=2) {
          $gelID{$gelIDs[$ii]} = $gelIDs[$ii+1];
        }
        &PG_OPEN_CONNECTION({command=>'open'});
        my ($sth, $sth1, $sth2, $sth3, $sth4);
        eval {
          $sth = $DBI::openedConn->prepare("$unhideCommand");
          $sth ->execute(); sleep 3;
          $command = "UPDATE EntryGelImage SET showFlag = FALSE WHERE gelID = ? ".
                     "AND AC = ". $SERVER::commonSchema.".make2db_primary_accession(?); ";
          $command.= "UPDATE EntryGelMaster SET showFlag = FALSE WHERE gelID = ? ".
                     "AND AC = ". $SERVER::commonSchema.".make2db_primary_accession(?); ";
          $command.= "UPDATE SpotEntry SET showFlag = FALSE WHERE gelID = ? ".
                     "AND AC = ". $SERVER::commonSchema.".make2db_primary_accession(?)";
          $sth1 = $DBI::openedConn->prepare_cached($command);
          foreach my $gel (keys %{$hiddenData->{SpotsEntry}}){
            foreach my $ac (keys %{$hiddenData->{SpotsEntry}->{$gel}}) {;
              my $ac = quotemeta($ac);
              $sth1->bind_param(1, $gelID{$gel}); $sth1->bind_param(3, $gelID{$gel}); $sth1->bind_param(5, $gelID{$gel});
              $sth1->bind_param(2, $ac); $sth1->bind_param(4, $ac); $sth1->bind_param(6, $ac);
              $sth1->execute();
            }
          }
          $command = "UPDATE SpotEntry SET showFlag = FALSE WHERE gelID = ? ".
                     "AND spotID = ? AND AC = ". $SERVER::commonSchema.".make2db_primary_accession(?)";
          $sth2 = $DBI::openedConn->prepare_cached($command);
          foreach my $spot (keys %{$hiddenData->{SpotEntry}}){
            foreach my $ac (keys %{$hiddenData->{SpotEntry}->{$spot}}) {
              next unless $spot =~ /^(\S+)\:\:(\S+)$/;             
             (my $gel, my $spot, my $ac) = ($1, quotemeta($2), quotemeta($ac));
              $sth2->bind_param(1, $gelID{$gel});
              $sth2->bind_param(2, $spot);
              $sth2->bind_param(3, $ac);
              $sth2->execute();
            }
          }
          $command = "SET showFlag = FALSE WHERE gelID = ? ".
                     "AND (spotID = ? OR 1 = ?) ".
                     "AND (AC = ". $SERVER::commonSchema.".make2db_primary_accession(?) OR 1 = ?)";
          my %sth_ident;
          $sth_ident{'AA'} = $DBI::openedConn->prepare_cached("UPDATE SpotIdentificationAAcid ".$command);
          $sth_ident{'PMF'} = $DBI::openedConn->prepare_cached("UPDATE SpotIdentificationPeptMassF ".$command);
          $sth_ident{'MS/MS'} = $DBI::openedConn->prepare_cached("UPDATE SpotIdentificationTandemMS ".$command);
          foreach my $spot (keys %{$hiddenData->{Identification}}){
            foreach my $ac (keys %{$hiddenData->{Identification}->{$spot}}) {
              foreach my $ident (keys %{$hiddenData->{Identification}->{$spot}->{$ac}}) {
                next unless $spot =~ /^(\S+)\:\:(\S+)$/;
               (my $gel, my $spot, my $ac, my $ident) = ($1, quotemeta($2), quotemeta($ac), $ident);
                my $sth_ident = \$sth_ident{$ident} and $sth_ident{$ident} or next;
                my $spotAll = ($spot eq quotemeta('*'))? 1 : 0;
                my $acAll = ($ac eq quotemeta('*'))? 1 : 0;
                $$sth_ident->bind_param(1, $gelID{$gel});
                $$sth_ident->bind_param(2, $spot);
                $$sth_ident->bind_param(3, $spotAll);
                $$sth_ident->bind_param(4, $ac);
                $$sth_ident->bind_param(5, $acAll);
                $$sth_ident->execute();
              }
            }
          }
          # because is *NOT* performed in the final command??!! -> included here in the eval as an extra execution
          $sth4 = $DBI::openedConn->prepare('SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)');
          $sth4->execute();
          $DBI::openedConn->commit;
        };
        if ($@) {
          print $co->h3("Hiding Spots: A problem occured, operation aborted! ($@)");
          $DBI::openedConn->rollback;
        }
        else {
          print $co->h3("Hiding Spots: Operation correctly performed!");
          copy ($hideDataFile, $hideDataFile.'.lastProcessed') if (-e $hideDataFile);   
        }
        $sth-> finish() if $sth;
        $sth1->finish() if $sth1;
        $sth2->finish() if $sth2;
        $sth3->finish() if $sth3;
        $sth4->finish() if $sth4;
      }
    }
    undef($command); 
    sleep 3;
    # export to public schema
    $command = 'SELECT '.$SERVER::coreSchema.'.make2db_update(0,1)' if $proceedUnhideData or $proceedHideData;
  }
  # End Hidden Data Treatment


  elsif ($co->param('Execute query') eq 'Vacuum Database') { 
    print $co->h3("'Vacuum Analyze' Performed!");
    $command = "VACUUM FULL ANALYZE; select now()";
  }

  elsif ($co->param('Execute query') eq 'Empty the Backup (log) schema') {
    my $failed;
    if ($co->param('export log to file')) {
      my $file = $co->param('export log to file');
      $file =~ s/.+\///; # truncate any path before file name /
      $file = "$main::tmp_path/$file";
      rename ("$main::tmp_path/$file", "$main::tmp_path/$file.old") if -s "$main::tmp_path/$file";
      unless ($failed) {
        unless (do "include.cfg") {
          print $co->h3("Could not read include.cfg to determine which 'pg_dump' is to be used - the default 'pg_dump' will be used!");
        }
        $POSTGRES::bin .= "/" unless !$POSTGRES::bin or $POSTGRES::bin =~ m!/$!;
        &IMPORT_DATABASE_SPECIFIC_PARAMETER unless $DB::set_select_user;
        rename($file, "$file.old") if -s $file;
        open (LOGDUMP_WRITE, ">$file") or $failed = 1;
        if ($failed) {
          print $co->h3("Could not export the Backup (log) schema to \'$file\'! operation aborted!");
        }
        else {
          # my $logDumpCommand = "$POSTGRES::bin"."pg_dump --host localhost --schema=log --no-owner --compress=0 $DB::CurrentDatabaseName";
          # my $logDump = `$logDumpCommand`;
          open (LOGDUMP_READ, "$POSTGRES::bin"."pg_dump --host localhost --schema=log --no-owner --compress=0 $DB::CurrentDatabaseName |");
          while (<LOGDUMP_READ>) {
            print LOGDUMP_WRITE $_;
          }
          # undef($logDump);
          close LOGDUMP_WRITE;
          close LOGDUMP_WRITE;
          print $co->h3("The Backup (log) schema was exported to \'$file\'!");
        }
      } 
    }
    unless ($failed) {
      $command = "SELECT tablename FROM pg_tables WHERE schemaname = 'log'";
      my @log_tables = &EXECUTE_FAST_COMMAND("$command");
      $failed = 1 unless @log_tables;
      foreach my $log_table (@log_tables) {
        $command = "DELETE FROM ".$SERVER::logSchema.".$log_table";
        $command = "TRUNCATE TABLE ".$SERVER::logSchema.".$log_table";
        &EXECUTE_FAST_COMMAND("DO: $command");
      }
    }
    print my $string = ($failed)? $co->h3("The Backup (log) schema has not been initialized!") : $co->h3("The Backup (log) schema has been initialized!");

    undef($command);
  }

  else {
    $command = $co->param('pgsql command');
  }

  &EXECUTE_FORMATTED_COMMAND({command=>$command, show_number_rows_affected=>1}) if $command;

  1;
}

#=============================================================================================================================#
#=============================================================================================================================#


# Update UniProt related data on the core schema

sub CORE_UpdateExternalUniProtData {

  &PG_OPEN_CONNECTION({command=>'open'});

  my ($sth, $sth1, $sth2, $command, %command, $sp_cross_ref_ACs, $srs_response, $srs_response_error, $external_table_updated);

  eval {
    
    $sth = $DBI::openedConn->prepare(qq{
      SELECT AC, XrefPrimaryIdentifier FROM EntryXrefDB WHERE XrefDBCode = 1 OR XrefDBCode = 2;});
    $sth->execute();

    my %ACsRef = ();
    my $last_row_ref = ();
    while ( $last_row_ref = $sth->fetchrow_arrayref) {
      $ACsRef{${$last_row_ref}[0]} = ${$last_row_ref}[1]
    }
    $sth->finish() if $sth;

  
    # Query ExPASy and Update the External UniProt Table

    my @sp_cross_ref_ACs = sort values %ACsRef;
    my @sp_cross_ref_fields = ('AC', 'ID', 'DT', 'DE', 'GN', 'OG', 'KW', 'DR');


    if (@sp_cross_ref_ACs) {

      $srs_response = EXTERNAL_RETRIEVAL({key_identifiers=>[@sp_cross_ref_ACs], fields=>[@sp_cross_ref_fields], srs=>1});

      print "- SRS Retrieval terminated!<br>\n";

      # we expect here a hash of hashes: e.g. %{$srs_response->{P05090}}->{DR}
      if (defined($srs_response) and ref($srs_response) ne 'HASH') {
        undef($srs_response);
        print $srs_response_error = 
          "{The data type returned by the SRS mapper was not of the expected Swiss-Prot type! No SRS data will be processed}";
        return 0;
      }

      if (!defined($srs_response)) {
        print $srs_response_error = 
          "{Some errors have been encountered retrieving data by SRS. You may launch an automatic update later}";
        return 0;
      }

      else {
        my (%sp_SRS_details, $sp_SRS_details_number_of_fields);
        my $sp_extraction_date = 'now()'; # 'CURRENT_DATE' seems to be no longer supported;
        # Current Swiss-Prot/TrEMBL releases, Swiss-Prot is default
        my $sp_current_version = LWP_AGENT('GET', "$main::expasy_GL_txt_reader?Swiss-Prot:version");
        $sp_current_version = ($sp_current_version =~ /(\d+\.\d+)/mg)? $1 : '0.0';
        my $trembl_current_version = LWP_AGENT('GET', "$main::expasy_GL_txt_reader?TrEMBL:version");
        $trembl_current_version = ($trembl_current_version =~ /(\d+\.\d+)/mg)? $1 : '0.0';

        foreach my $sp_cross_ref_AC (@sp_cross_ref_ACs) {
        # ACs
         (my $sp_all_ACs = $srs_response->{$sp_cross_ref_AC}->{AC}) =~ s/\n//mg;
          my @sp_all_ACs = split ';', $srs_response->{$sp_cross_ref_AC}->{AC};
         (my $sp_AC = shift @sp_all_ACs) =~ s/\s//g;
          map { $_ =~ s/\s//g; $_ =~ s/\"//g } @sp_all_ACs; 
          my $sp_secondary_ACs = "{".(join ',', @sp_all_ACs)."}"; # we use later '{...}' in inserting arrrays
        # ID
         (my $sp_ID = $srs_response->{$sp_cross_ref_AC}->{ID}) =~ s/^(\w+)\s.*$/$1/;
          $sp_ID =~ s/\s//g;
        # DTs (detecting also if Swiss-Prot or TrEMBL)
          my ($sp_DT_entry_incorporated_date, $sp_DT_sequence_update_date, $sp_DT_sequence_update_version, $sp_DT_entry_update_date, $sp_DT_entry_update_version);
          my $SP_or_TrEMBL = ($srs_response->{$sp_cross_ref_AC}->{DT} =~ /TrEMBL/i)? 'false' : 'true';
          my $sp_trembl_current_version = ($SP_or_TrEMBL eq 'false')? $trembl_current_version : $sp_current_version;
          foreach my $dt_line (split '\n', $srs_response->{$sp_cross_ref_AC}->{DT}) {
            if ($dt_line =~ /^(\S+).+(?:incorporated|created)/i) {
              $sp_DT_entry_incorporated_date = $1;
             }
            elsif ($dt_line =~ /^(\S+).+sequence/i) {
              $sp_DT_sequence_update_date = $1;
              $sp_DT_sequence_update_version = $1 if $dt_line =~ /(?:version|Rel\.)\s*(\d+)/i;
            }
            elsif ($dt_line =~ /^(\S+).+(?:entry|annotation)/i) {
              $sp_DT_entry_update_date = $1;
              $sp_DT_entry_update_version = $1 if $dt_line =~ /(?:version|Rel\.)\s*(\d+)/i;
            }
          }
        # DE
         (my $sp_DE = $srs_response->{$sp_cross_ref_AC}->{DE}) =~ s/\n|\t/ /mg;
             $sp_DE =~ s/\.$//;
        # EC (from DE)
          my $sp_EC = ($sp_DE =~ /EC+\s*(\d{1,3}(\.(\d{1,3}|\-))+)/i)? $1 : '';	  
        # GN
         (my $sp_GN = $srs_response->{$sp_cross_ref_AC}->{GN}) =~ s/\n|\t/ /mg;
             $sp_GN =~ s/;$//;
        # KW
         (my $sp_OG = $srs_response->{$sp_cross_ref_AC}->{OG}) =~ s/\n|\t/ /mg;
        # KW
         (my $sp_KW = $srs_response->{$sp_cross_ref_AC}->{KW}) =~ s/\n|\t/ /mg;
        # DRs
          my $sp_DR = "{";
          foreach my $dr_line (split '\n', $srs_response->{$sp_cross_ref_AC}->{DR}) {
            $dr_line =~ s/\s*\.?\s*$//;
            $dr_line =~ s/\s*;\s*/;/g;
            $dr_line =~ s/\t/ /g;
            my @dr_components =  split ';', $dr_line;
            $dr_components[$#dr_components] =~ s/\.$//;
            my ($base, $dr_ac, $dr_id_desc) = @dr_components[0..2];
            my $dr_desc3 = $dr_components[3];
            my $dr_desc4 = @dr_components[4..$#dr_components] if $#dr_components >= 4; # any extra components are concateneted to $dr_desc4
            if ($base and $dr_ac) {
              $sp_DR .= "{\"$base\",\"$dr_ac\",\"$dr_id_desc\",\"$dr_desc3\",\"$dr_desc4\"},";
             #Prepare for GO retrieval
             #if ($EXTERNAL::update_from_GO and $base =~ /^GO$/i) {
              # $main::go_cross_ref_ACs{$sp_cross_ref_AC} = $dr_ac;
             #}
            }
          }
          $sp_DR =~ s/\,$//;
          $sp_DR .= "}";

	  # consolidate values for all the SRS data of the current Swiss-Prot AC(s) (unique SP AC in theory, but not prevented)
          unless ($sp_SRS_details{$sp_AC}) {
                   
            $sp_SRS_details{$sp_AC} = [
              $sp_extraction_date, $sp_AC, $sp_secondary_ACs, $sp_ID, $sp_trembl_current_version,
              $sp_DT_entry_incorporated_date, $sp_DT_sequence_update_date, $sp_DT_sequence_update_version,
              $sp_DT_entry_update_date, $sp_DT_entry_update_version,
              $sp_DE, $sp_EC, $sp_GN, $sp_OG, $sp_KW, $sp_DR, $SP_or_TrEMBL ];
            $sp_SRS_details_number_of_fields = scalar @{$sp_SRS_details{$sp_AC}} unless $sp_SRS_details_number_of_fields;
            # void fields should hold a null
            map {$_ = $CORE::nullInsertValue unless length($_)} @{$sp_SRS_details{$sp_AC}};
            
            
            foreach my $sp_secondary_ACs (@sp_all_ACs) {
              $sp_SRS_details{$sp_secondary_ACs} = $sp_SRS_details{$sp_AC};
            }
          }
        } # end foreach (@sp_cross_ref_ACs)

        # INSERTS!! (A fast alternative would be to write into a file and then COPY FROM)
        $command{1} = "DELETE FROM ExternalMainXrefData WHERE AC = ";
        $command{2} = "INSERT INTO ExternalMainXrefData VALUES ";
        unless ($SERVER::noDBIplaceholders) {
          $sth1 = $DBI::openedConn->prepare($command{1}."?");
          my $placeholders = join(', ', ('?') x ($sp_SRS_details_number_of_fields + 1));
          $sth2 = $DBI::openedConn->prepare($command{2}."(".$placeholders.", USER, now())");
        }
        foreach my $ac (sort keys %ACsRef) {
          my $sp_dr_ac = $ACsRef{$ac};
          next unless $sp_SRS_details{$sp_dr_ac};
          unless ($SERVER::noDBIplaceholders) {
            $sth1->bind_param(1, $ac);
            $sth1->execute();
            $sth2->execute($ac, @{$sp_SRS_details{$sp_dr_ac}});
          }
          else {
            my $command_values = $command{1}."'$ac'";
            $sth1 = $DBI::openedConn->prepare("$command_values"); $sth1->execute();
            $command_values = $command{2}."('$ac',";
            foreach my $sp_SRS_detail (@{$sp_SRS_details{$sp_dr_ac}}) {
              $sp_SRS_detail =~ s/'/\\'/g;
              $command_values.= ($sp_SRS_detail)? "'$sp_SRS_detail'," : "NULL,";
            }
            $command_values.= "USER, now())";
            $sth2 = $DBI::openedConn->prepare("$command_values"); $sth2->execute();
          }
          $external_table_updated = 1;
        }
        undef %sp_SRS_details;

      }
    } # end if (@sp_cross_ref_ACs)
 
    else {
      print "{There are currently no entries with cross-references to UniProt (Swiss-Prot or TrEMBL); no update has been performed}";
      $sth->finish()  if  $sth;
      $sth1->finish() if $sth1;
      return 0;
    }
    $DBI::openedConn->commit;
  }; # end eval

  if ($@ or !$external_table_updated) {
    $DBI::openedConn->rollback;
    die $@ if $@;
  } 
  print "- External UniProt Data updated!<br>\n" if $external_table_updated; 

  $sth-> finish() if  $sth;
  $sth1->finish() if $sth1;
  return 1;

}

#=============================================================================================================================#
#=============================================================================================================================#

# Update Make2DDB II (2D) related data on the core schema

sub CORE_UpdateExternalMake2DDBData {

  my $updateCoreSchema = $_[0]->{updateCoreSchema};

  &PG_OPEN_CONNECTION({command=>'open'});

  my ($command, %command, $sth, $sth1, $sth2, $sth3, $sth4, @key_identifiers, @fields);
  my $dynamic_2d_cross_refs_updated = my $compute_map_links_updated = 0;


  # update dynamic cross-references to other Make2D-DB II databases
  # always performed
  if (1) {

    @key_identifiers = ('_VOID_');
    @fields = ('_VOID_');
    my $dynamic_2d_cross_refs = EXTERNAL_RETRIEVAL({
      database=>'mak2ddbXref::lwp',
      key=>$DB::CurrentDatabaseName,
      key_identifiers=>[@key_identifiers],
      fields=>[@fields],
      srs=>0,
      returnScalar=>1,
      callerForm =>'html'
    });

    if ($dynamic_2d_cross_refs) {

      eval {
        $command = "DELETE FROM XrefDBDynamic; DELETE FROM EntryXrefDBDynamic";
        $sth = $DBI::openedConn->prepare("$command"); $sth->execute();
        my ($xrefDB, $xref1ID, $xref2ID, $indexDB, $indexID, %xrefEncounteredDB, %xrefEncounteredNonDynamicDB);
        my $xrefSerialDynamicDBCode = 10000;
        $sth = $DBI::openedConn->prepare("SELECT AC, XrefPrimaryIdentifier FROM EntryXrefDB WHERE XrefDBCode < 3");
        $sth->execute();
        my ($localIndexedAC, $localIndexIdentifier, %localIndexIdentifiers);
        $sth->bind_columns( undef, \$localIndexedAC, \$localIndexIdentifier);
        while($sth->fetch()) {
          push @{$localIndexIdentifiers{$localIndexIdentifier}}, $localIndexedAC;
        }
        $command{1} = "SELECT * FROM XrefDB WHERE lower(XrefDBName) = ";
        $command{2} = "INSERT INTO XrefDBDynamic (XrefDBCode, XrefDBName) VALUES ";
        $command{3} = "INSERT INTO EntryXrefDBDynamic (AC, XrefDBCode, XrefPrimaryIdentifier, XrefSecondaryIdentifier) VALUES ";
        $command{4} = "UPDATE EntryXrefDB SET activated = ";
        unless ($SERVER::noDBIplaceholders) {
          $sth1 = $DBI::openedConn->prepare_cached($command{1}." ?");
          $sth2 = $DBI::openedConn->prepare($command{2}." (?, ?)");
          $sth3 = $DBI::openedConn->prepare_cached($command{3}."(?, ?, ?, ?)");
          $sth4 = $DBI::openedConn->prepare_cached($command{4}." ? WHERE AC = ? AND XrefDBCode = ?");
        }
   
        my @dynamic_2d_cross_refs = split ("\n", $dynamic_2d_cross_refs);
        foreach my $dynamic_2d_cross_refs_line (@dynamic_2d_cross_refs) {
          next unless $dynamic_2d_cross_refs_line =~ /^\s*(\S+)\s+(\S+)\s+\((\S+)\)\s+(\S+)\s+(\S+)\s*$/;
         ($xrefDB, $xref1ID, $xref2ID, $indexDB, $indexID) = ($1,$2,$3,$4,$5);
          my $xrefSerialDBCode = undef;    
          unless ($xrefEncounteredDB{$xrefDB}) {
            unless ($SERVER::noDBIplaceholders) {
              $sth1->bind_param(1, lc($xrefDB));
              $sth1->execute();
            }
            else {
              my $command_values = $command{1}."'".quotemeta(lc($xrefDB))."'";
              $sth1 = $DBI::openedConn->prepare("$command_values"); $sth1->execute();
            }
            my @xrefDBfields = $sth1->fetchrow_array;
            if (@xrefDBfields) {
              $xrefEncounteredNonDynamicDB{$xrefDB} = $xrefDBfields[0];
              $xrefSerialDBCode = $xrefDBfields[0];
              map { $_ = '\''.quotemeta($_).'\''} @xrefDBfields;
              my $xrefDBfields = join (',', @xrefDBfields);
              $command = "INSERT INTO XrefDBDynamic VALUES ($xrefDBfields)";
              $sth = $DBI::openedConn->prepare("$command"); $sth->execute();
            }
            else {
              $xrefSerialDynamicDBCode++;
              unless ($SERVER::noDBIplaceholders) {
                $sth2->bind_param(1, $xrefSerialDynamicDBCode); $sth2->bind_param(2, $xrefDB);
                $sth2->execute();
              }
              else {
                my $command_values = $command{2}."($xrefSerialDynamicDBCode,'".quotemeta($xrefDB)."')";
                $sth2 = $DBI::openedConn->prepare("$command_values"); $sth2->execute();
              }
            }
            $xrefEncounteredDB{$xrefDB} = ($xrefSerialDBCode)? $xrefSerialDBCode : $xrefSerialDynamicDBCode;
          }
          
          if ($localIndexIdentifiers{$indexID}) {
            foreach my $localIndexIdentifier (@{$localIndexIdentifiers{$indexID}}) {
              if (($updateCoreSchema == 1 or $updateCoreSchema == 3) and $xrefEncounteredNonDynamicDB{$xrefDB}) {
                my $activated = ($updateCoreSchema == 1)? 'TRUE' : 'FALSE';
                unless ($SERVER::noDBIplaceholders) {
                  $sth4->bind_param(1, $activated);
                  $sth4->bind_param(2, $localIndexIdentifier);
                  $sth4->bind_param(3, $xrefEncounteredNonDynamicDB{$xrefDB});
                  $sth4->execute();
                }
                else {
                  my $command_values = $command{4}."$activated WHERE AC = '$localIndexIdentifier'".
                                       " AND XrefDBCode = $xrefEncounteredNonDynamicDB{$xrefDB}";
                  $sth4 = $DBI::openedConn->prepare("$command_values"); $sth4->execute();
                }
              }
              unless ($SERVER::noDBIplaceholders) {
                $sth3->bind_param(1, $localIndexIdentifier);
                $sth3->bind_param(2, $xrefEncounteredDB{$xrefDB});
                $sth3->bind_param(3, $xref1ID);
                $sth3->bind_param(4, $xref2ID);
                $sth3->execute();
              }
              else {
                my $command_values = $command{3}."('$localIndexIdentifier','".$xrefEncounteredDB{$xrefDB}."',".
                                                 "'".quotemeta($xref1ID)."',".(($xref2ID)? "'".quotemeta($xref2ID)."'" : "NULL").")";
                $sth3 = $DBI::openedConn->prepare("$command_values"); $sth3->execute();
              }
              $dynamic_2d_cross_refs_updated = 1;

            }          
          }
        }

        $DBI::openedConn->commit;
      }; # end eval

      if ($@ or !$dynamic_2d_cross_refs_updated) {
        $DBI::openedConn->rollback;
        die $@ if $@;
      }
      print "- Dynamic Cross-references to other 2-DE Databases updated!<br>\n" if $dynamic_2d_cross_refs_updated;
    }
  } # end always performed


  # include links to computable maps
  if ($EXTERNAL::include_links_to_compute_map) {

    @key_identifiers = ('_VOID_');
    @fields = ('_VOID_');
    my $compute_map_links = EXTERNAL_RETRIEVAL({
      database=>'computablemaps::postgres',
      key=>$DB::CurrentDatabaseName,
      key_identifiers=>[@key_identifiers],
      fields=>[@fields],
      srs=>0,
      returnScalar=>1
    });

    if ($compute_map_links) {
      eval {
        $command = "DELETE FROM GelComputableDynamic WHERE lower(databaseName) != '".lc($DB::CurrentDatabaseName)."'";
        $sth = $DBI::openedConn->prepare("$command");
        $sth->execute();
        $command = "INSERT INTO GelComputableDynamic (databaseName, gelShortName, gelFullName, organismSpecies, taxonomyCode, gelComputableUrl) ".
                   "VALUES ";
        $sth1 = $DBI::openedConn->prepare($command."(?, ?, ?, ?, ?, ?)") unless $SERVER::noDBIplaceholders; 
        while ($compute_map_links =~ s/(?:^|\n)([^\t]+)\t([^\t]+)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]+)\n//) {
          my ($field1,$field2,$field3,$field4,$field5,$field6) = ($1,$2,$3,$4,$5,$6);
          $field4 =~ s/\'//mg; # in some installations, the ' from common name is not escaped!?
          unless ($SERVER::noDBIplaceholders) {
            $sth1->bind_param(1, $field1); $sth1->bind_param(2, $field2); $sth1->bind_param(3, $field3);
            $sth1->bind_param(4, $field4); $sth1->bind_param(5, $field5); $sth1->bind_param(6, $field6);
            $sth1->execute();
          }
          else {
            my $command_values = $command ."('".quotemeta($field1)."','".quotemeta($field2)."','".quotemeta($field3)."','".
                                                quotemeta($field4)."',$field5,'".quotemeta($field6)."')";
            $sth1 = $DBI::openedConn->prepare("$command_values"); $sth1->execute();
          }
          $compute_map_links_updated = 1;
        }
        $DBI::openedConn->commit;
      }; # end eval

      if ($@ or !$compute_map_links_updated) {
        $DBI::openedConn->rollback;
        die $@ if $@;
      }
      print "- External Computable Maps updated!<br>\n" if $compute_map_links_updated;
    }
  } # end include links to computable maps


  $sth-> finish() if $sth;
  $sth1->finish() if $sth1;
  $sth2->finish() if $sth2;
  $sth3->finish() if $sth3;

  return ($dynamic_2d_cross_refs_updated + $compute_map_links_updated);
}



##======================================================================##


# Upload a text file given by it's path (and exit)

sub UPLOAD_FILE {

  exit 0 unless $SERVER::core_selector;

  use IO::File;
  my $maxLength = 100*1024;
  my $co_uploadFile = new CGI;

  my $fileField = $_[0];
  my $file  = $co_uploadFile->param($fileField);

  my $toFile = $_[1];
  unless ($toFile) {
   ($toFile = $fileField) =~ s/^uploaded\s+//;
    $toFile = ($co_uploadFile->param($toFile))? $co_uploadFile->param($toFile) :
      $main::tmp_path."/$DB::CurrentDatabaseName/"."lastUploadedFile.txt";
  }

  print $co_uploadFile->header."\n";
  print $co_uploadFile->start_html(-title=>"$fileField")."\n"; print "***$_[0] -> $_[1] ***<br>";

  # for binary files (not needed yet):
  #open (OUTFILE,">>$somefile");
  #while ($bytesread=read($filename,$buffer,1024)) {
  #  print OUTFILE $buffer;
  #}

  if (!$file) {
    print $co_uploadFile->h2('Sorry!');
    print $co_uploadFile->h3('Your file could not be retrieved, or no data could be read from it!');
  }
  elsif ($toFile !~ /^$main::tmp_path/) {
    print $co_uploadFile->h3("Sorry, you can only upload files to '$main::tmp_path' and sub-directories!");
    print $co_uploadFile->h4("Your are not allowed to write to '$toFile'");
  }
  else {

    # $/ = ""; my $fileHandler = new IO::File; fileHandler->open($file); $fileHandler->seek(0,0); print $co_viewFile->start_pre."\n";
    # while(my $line = <$fileHandler>) { print $line; }
    # print $co_viewFile->end_pre."\n"; $fileHandler->close; $/ = "";

    print $co_uploadFile->h3("...File uploaded: '$file'");
    print $co_uploadFile->h3("...File to copy to on the server: '$toFile'");


    if (-s $toFile) {
      use File::Copy;
      copy ($toFile, $toFile.'.old');
    }
    unless (open ToFile, ">$toFile") {
      print $co_uploadFile->h3($toFile.'...Could not write to the specified file. No upload is performed!');
    }
    else {
      my ($length, $largeFile, $allLines);
      while(my $line = <$file>) { 
        $allLines .= $line;
        print ToFile $line;
        $length = length($line);
        if ($length > $maxLength) {
          $largeFile = 1;
          last;
        }
      }
      close ToFile;
      if ($largeFile) {
        $length = int($length/1024);
        print $co_uploadFile->h3("...File too large (>$length Kbyte). Operation aborted!");
        rename $toFile.'.old', $toFile;
      }
      else {
        print $co_uploadFile->h3('...Operation completed.');
        print $co_uploadFile->br.$co_uploadFile->h3('File content:').$co_uploadFile->br;
        print $co_uploadFile->start_pre."\n";
        print $allLines;
        print $co_uploadFile->end_pre."\n";
      }
    }
  }

  print $co_uploadFile->end_html;

  exit 1;
  1;
}

##======================================================================##


sub _get_some_text_ {

  my $ident = $_[0];
  my $htmlFormat = $_[1];
  my $text;
  my $co_text = new CGI;

  if ($ident eq 'Expand Update: Views') {
    $text = "- ".$co_text->strong("Views")." ".
    "are collections of data grouped together to form a specific view for users (e.g. a protein entry or a map list). ".
    "Views on Make2D-DB II are chosen not to be dynamic, they are built using some special functions. ".
    "When some of your data have been changed or altered, you will need to reflect those changes on the views by using the following commands. ". 
    "You may apply views' update to the ".$co_text->strong("core schema")." (schema including private data, first button), ".
    "to the ".$co_text->strong("public schema")." (data displayed to public users, second button) ".
    "or perform both operations simultaneously (third button).\n".
    "The '".$co_text->strong("update interface URI")."' is intended to keep data relative to your interface location (or even to remote interfaces) up-to-date.";
  }
  elsif ($ident eq 'Expand Update: Views (2)') {
    $text =
    "- ".$co_text->strong("Statistics")." are statistical data that are automatically generated based on your database content. ".
    "They reflect your database content and are visible for public users. ".
    "You should always perform at least one update after having installed your database.\n\n";
  }
  elsif ($ident eq 'Expand Update: Views (3)') {
    $text =
    "- From this section, you can also edit the interface ".$co_text->strong("subtitle")." file (subtitle.html) that is displayed within ".
    "your interface homepage. You may also upload any valid HTML taged file (with no header nor html/body containers), ".
    "as well as any simple text file from your computer to replace the initial file.\n".
    $co_text->span({class=>'Comment'}, "(to annotate the current specific database, e.g. release notes or contact, go to section 'Annotate and Publish a Database Release')")."\n\n";
  }
  elsif ($ident eq 'Expand Update: External') {
    $text =
    "External data are the data collected over the net to be integrated within your own data. ".
    "Make2D-DB II rely on a distributed net of remote 2D databases, ".
    "as well as data related to the identified proteins and collected from the UniProt Knowledge database via the ExPASy server.\n".
    "There are actually ".$co_text->strong('3 levels for data replacement').". ".
    "The 'low' level will barely alter your older data. ".
    "The 'partial' level is a good compromise between data replacement and keeping personal annotations. ".
    "Finally, the 'full' level (default) will always replace inner data whenever an up-to-date is available*.\n".
    "<br>With the '".$co_text->strong('increment entry version on change')."' checked on, a change on data will cause an automatic version increment on all modified entries ".
    "(sometime, you will still need to explicitly go to the 'Update Entry Versions' section and perform the version update from there).\n".
    "Updates are instantly visible to public users if the '".$co_text->strong('export to public schema')."' remains selected."
  }
  elsif ($ident eq 'Expand Update: Update Entry Version') {
    $text =
    "Entries may have been modified, either directly or due to an external data import. To make any recent change increment the ".
    "protein corresponding versions (for general or 2D annotations), execute the '".$co_text->strong("Update Entry Versions")."'.\n";
  }
  elsif ($ident eq 'Expand Update: Annotate and Publish a Database Release') {
    $text =
    "The '".$co_text->strong("Annotate and Publish a Database Release")."' will consolidate all recent changes on core data and entry versions. ".
    "It will then, upon your choice, increment or not the database release or sub-release by 1, ".
    "and export it as a new release for public access.\n".
    "The '".$co_text->strong("Database Release details")."' lets you enter or edit the database annotations and details related to the current or the new release.\n";
  }
  elsif ($ident eq 'Expand Update: Export Data') {
    $text =
    "Currently, you can export your database entries into a SWISS-2DPAGE-like ".$co_text->strong("flat file")." format.\n".
    "You can also export all of your database structure and content into a dump file for <strong>backup</strong> purpose, ".
    "or to build it on another installation.\n".
    "Other formats (XML, spot based views) should be available in the future."
  }
  elsif ($ident eq 'Expand Update: Hide Data') {
    $text =
    "From here you control which data is to be hidden from public users. Data can be hidden at three levels (cf. documentation): ".
    "the ".$co_text->strong("gel level").", the ".$co_text->strong("protein level")." and the ".$co_text->strong("spot level").". ".
    "For each of those levels, you may use your default file (the one displayed by default in the file field), ".
    "or indicate a new file to be used instead. Any new file should allow read permission to the HTTP server.\n\n".
    "You may also ".$co_text->strong("upload")." a file from you computer by giving its path or by browsing your computer ".
    "using the 'Browse...' button. The file will then replace on the server directories the file name given ".
    "by default in the '".$co_text->strong("file path")."' field. ".
    "For security reasons, uploaded files are limited to a maximum 100 KByte. ".
    "Once your file has been uploaded, you should return back to this section and press the corresponding 'Hide' button ".
    "for the file to be processed.\n\n".
    "To unhide all data, simply do not give any file name (a blank field) in the corresponding 'file path' area.\n\n".
    "Your last processed files are located (by default) in ' /var/www/html/2d_test/data/tmp/test_2dpage/$DB::CurrentDatabaseName '";
  }
  elsif ($ident eq 'Expand Update: Clean DB') {
    $text =
    "It is useful to '".$co_text->strong("clean")."' your database from time to time. Specially if you have performed heavy update operations on your data. ".
    "It is recommended to apply the cleaning once per month, or - depending on your update activity - ".
    "whenever you feel a slowing down of your queries execution.\n".
    "The first button 'Vacum database' force your database to get *effectively* (physically) rid of any old and useless data. ".
    "The second button helps to empty the log schema from its content. ".
    "The log schema contains all modified or erased data (throughout the database history). ".
    "When it becomes overloaded, it is recommended to empty the schema (all content will then be redirected to a dump/text file that you may keep or erase).";
  }

  $text =~ s/\n/<br>\n/mg if $htmlFormat;
  return $text;

}


##======================================================================##

##======================================================================##



sub _ignore_used_only_once_
{

  return;

  () if (
    $SERVER::core_selector or
    $SERVER::need_login or
    $SERVER::logSchema or
    $SERVER::interfaceWebAddress or
    @SERVER::headerCookies or
    $DBI::openedConn or
    $main::html_path or
    $main::web_server_ref_name or
    $main::url_www2d or
    $main::multi_DBs or
    $DB::CurrentDatabase or
    $DB::set_select_user or
    $EXTERNAL::update_from_SwissProt or
    $EXTERNAL::update_from_Make2DDB or
    $EXTERNAL::include_links_to_compute_map
  )
  
}

#================================================================================================#


1;

