#@(#)make2db_util.pl
#  version 2.50
#  Make2D-DB II tool
#  Checking flat file format and syntax, and then building files to be copied into the DB
# *Called by make2db.pl (Converting Proteomic 2D-Gel databases into postgres relational format)*
# *Added also to the 2d_util.pl collection on the server side*
#==============================================================================================#

# /*  This script is part of the Make-2D DB II Tool                               */
# /*  Copyright to the SWISS INSTITUTE OF BIOINFORMATICS                          */
# /*  You can freely use this script without moving those copyright lines         */
# /*  Modifications should comply to the instructions stated in the licence terms */

#=============================================================================================================================#

# sub LWP_AGENT                    use of LWP::UserAgent for GET and POST methods

# sub EXTERNAL_RETRIEVAL           retrieval using the 'getz_syntax_mapper' script on the ExPASy server

# sub get_href                     get URLs for DbCrossRefs and other servers

# sub GENERATE_STATISTICS_HTML     perform the statistics generation HTML to be displayed on the server home interface

# sub GENERATE_FLAT_FILE           generate a flat file from a collection of tabulated report files

#=============================================================================================================================#

use strict;

use FindBin;
use lib $FindBin::Bin."/lib2d";

my $font_style = "font-family: sans-serif;";
my $link_style = "text-decoration: none; font-family: sans-serif;";
#=============================================================================================================================#
#=============================================================================================================================#

# called by: LWP_AGENT('GET', "URL") or LWP_AGENT('POST', "URL", {param_1=>"value_1", .., param_n=>"value_n"}) ;

sub LWP_AGENT {

  my $method = ($_[0] eq 'POST')? 'POST' : 'GET';
  my $url = $_[1];
  my %post_params = ($_[2] and %{$_[2]})? %{$_[2]} : () ; # hash

  my ($request, $response);

  eval ("use LWP::UserAgent; use HTTP::Request::Common");
  if ($@) {
    warn "\n{Error encountered while connecting to \"$url\": (Perl modules LWP::UserAgent and/or HTTP::Request::Common not present)}\n";
    return undef;
  }

#use LWP::UserAgent;
 use HTTP::Request::Common;

  my $LWP_Agent = LWP::UserAgent->new;
  $LWP_Agent->timeout(60);

  if ($method eq 'POST') {    
    $request = POST "$url", [%post_params];
  }
  else {
    $request = GET "$url";
  }

  $response = $LWP_Agent->request($request);

 #$response->{_protocol|_content|_rc}|_msg|_request} #

  if($response->is_error()) {
    warn "\n{Error encountered while connecting to \"$url\": (".$response->status_line.")}\n";
    return undef;
  }

  return $response->{_content};

}



#=============================================================================================================================#
#=============================================================================================================================#

# Called by: EXTERNAL_RETRIEVAL({key_identifiers=>[@required_keys], fields=>[@required_fields], 
#                                key=>'optionel', database=>'optionel', mapper_url=>'optionel',
#                                user=>'optionel', http=>'optionel',  srs=>'optionel']});

# returns a reference to a hash of hashes if $http is not defined (default) or *getz* is invoked
# (primary key: AC, secondries: 2 letters keywords), e.g. %{$srs_response->{P05090}}->{DR}
# otherwise, returns a scalar (also when the ExPASy syntax mapper is not invoked/defined)

sub EXTERNAL_RETRIEVAL {

  my $parameters = $_[0];

# by default, contact the getz SRS syntax mapper on ExPASy and query 'swiss_prot' #
  my $ExPASy_getz_syntax_mapper = "$main::expasy_getz_syntax_mapper";
  my $default_database = 'swiss_prot trembl::getz';


  my @SRS_key_identifiers = @{$parameters->{key_identifiers}};
  my @SRS_fields          = @{$parameters->{fields}};
  my $SRS_key             =  ($parameters->{key})? $parameters->{key} : 'AC';
  my $database            =  ($parameters->{database})? $parameters->{database} : $default_database;
  my $SRS_mapper_URL      =  ($parameters->{mapper_url}) ? $parameters->{mapper_url} : $ExPASy_getz_syntax_mapper;  
  my $user                =  ($parameters->{user})? $parameters->{user}     : 'Make2D-DB II';
  my $http                =  ($parameters->{http})? $parameters->{http}     : undef;
  my $srs                 =  ($parameters->{srs})? $parameters->{srs}       : undef;
  my $returnScalar        =  ($parameters->{returnScalar})? $parameters->{returnScalar} : undef;

  my $callerForm          =  ($parameters->{callerForm})? $parameters->{callerForm} : undef;

  my $make2d_version      = $main::VERSION;
  my $method = ($database =~ /\:\:(\w+)$/)? $1 : undef;
  undef $http if $method eq 'getz';


  $SRS_mapper_URL = $1 if $SRS_mapper_URL =~ /^(\S+)\?/;

  return undef unless $SRS_mapper_URL and @SRS_key_identifiers;

  if ($srs) {
    # for SRS entries: 'ID' and 'AC' will be used as entry separators:
    unshift (@SRS_fields, 'ID') unless grep { $_ eq 'ID' or $_ eq 'ALL' } @SRS_fields;
    unshift (@SRS_fields, 'AC') unless grep { $_ eq 'AC' or $_ eq 'ALL' } @SRS_fields;   
  }

  if ($srs and !$ExPASy_getz_syntax_mapper) {   

    my %SRS_fields = (
      AC  => 'acc',
      ID  => 'id',
      DT  => 'date',
      DE  => 'des',
      GN  => 'gen',
      KW  => 'keywords',
      DR  => 'dr',
      DBN => 'dbn',
      ALL => 'e'
    );
    map { $_ = 'AC' unless $SRS_fields{uc($_)}; $_ = $SRS_fields{uc($_)} if $SRS_fields{uc($_)} } @SRS_fields;
    $SRS_key = $SRS_fields{uc($SRS_key)} if $SRS_fields{uc($SRS_key)};
  }



  my $response;

  # Splice the AC array to limit the requests length to 100 entries max. (lv is redefined on the ExPASy script)
  my $lv_max = 100;
  my @SRS_key_identifiers_portion;
  while (@SRS_key_identifiers_portion[0..($lv_max-1)] = splice(@SRS_key_identifiers, 0, $lv_max)) {

    # ex on ExPASy: wgetz?-f+acc+-f+id+-f+date+-f+des+-f+keywords+-f+gen+-f+dr+[{swiss_prot}-acc:P13942|P05090]
    #             : getz -f acc -f id ... -f dr '[swiss_prot-acc:P13942|P05090]'
    #             : getz -f acc -f id ... -f dr '[swiss_prot-acc:P13942|P05090] & [swiss_prot:txi=96068]'
    my $partial_response = 
      LWP_AGENT ( 'POST', $SRS_mapper_URL,
                  {user=> $user, version=> $make2d_version,
		   database=>$database,
		   key=> $SRS_key, key_identifiers=>[@SRS_key_identifiers_portion], fields=>[@SRS_fields],
		   http=> $http
		  }
	        );
    if (!$partial_response or $partial_response =~ /(?:^|\n)(?:\->)?\s*Error/) {
      $partial_response = (defined($partial_response))? $partial_response : "Error: No response returned from the SRS mapper ($database)!";
      $partial_response = ($partial_response)? $partial_response : "Warning: the SRS mapper ($database) returned a void response!";
      print $partial_response. (($callerForm eq 'html')? "<br>\n" : "\n\n");
      return undef ;
    }

    $response .= $partial_response;

  }

  # Return as sent by the mapper from ExPASy if http is required
  return $response if $http or !$ExPASy_getz_syntax_mapper;

  # Also, return scalar as sent by the mapper from ExPASy if returnScalar is required 
  return $response if $returnScalar;

  my (%entry_buffer, %response_entries);
 
  # For SRS output, we expect a format "ID   VALUE\nAC   VALUE\n...", ID as identifier 
  if ($srs) {
    my ($several_demerged_response_entries);
    $response = "\n".$response; # do not split with "ID   " (too general), but with "\nID   "
    my $first_key_pattern = "\nID   ";
    foreach my $entry (split ("$first_key_pattern", $response)) {
      next unless $entry; # skip first void array element
      $entry = $first_key_pattern.$entry;
      next unless $entry =~ /\nAC\s+(.+)/mg;
      my @lines = split "\n", $entry;

      my %entry_buffer;
      foreach my $line (@lines) {
        next unless $line =~ /^(\w+)\s+(.+)$/;
        $entry_buffer{$1} .= ($entry_buffer{$1})? "\n".$2 : $2;
      }

      next unless $entry_buffer{AC};
     (my $entry_ACs = $entry_buffer{AC}) =~ s/\s|\n//mg;
      foreach my $entry_AC (split (';', $entry_ACs)) {
        $response_entries{$entry_AC} = \%entry_buffer;
        my $primaryEntryAC = ($response_entries{$entry_AC}->{AC} =~ /^(\w+)/)? $1 : '';
        if ($Make2D::namespace and $primaryEntryAC and $entry_AC ne  $primaryEntryAC) {
          $response_entries{$primaryEntryAC} = \%entry_buffer;
          push @{$ASCII::primarySwissProtAC->{$entry_AC}}, $primaryEntryAC;
        }
      }
    }

  } # end if $srs  

  # For non SRS output, we expect a format "Key   VALUE..\n"... with the first keyword as identifier
  else {
    my ($first_key, $first_key_pattern);
    if ($response =~ /^(\w+)\s+\w+/) { # we only keep the \w+ part from a first key value
      $first_key = $1;
      $first_key_pattern = "\n$first_key   ";
    }
    return $response unless $first_key;
    $response = "\n".$response; # do not split with "Key   " (too general), but with "\nKey   "
    foreach my $entry (split ("$first_key_pattern", $response)) {
      my $first_key_value;
      $first_key_value = $1 if $entry =~ /^(\w+)/;
      next unless $first_key_value;  # skipping also first void array element
      $entry = $first_key_pattern.$entry;
      my @lines = split "\n", $entry;

      my %entry_buffer;
      foreach my $line (@lines) {
        next unless $line =~ /^(\w+)\s+(.+)$/;
        $entry_buffer{$1} .= ($entry_buffer{$1})? "\n".$2 : $2;
      }

      next unless $entry_buffer{$first_key};
      $response_entries{$first_key_value} = \%entry_buffer;
    }
  } # end else (!$srs)  

  return \%response_entries;

}

#================================================================================================#
#================================================================================================#


# in XrefDB: Swiss-Prot->1st ref, TrEMBL->2nd ref, SWISS-2DPAGE->3rd ref, Swiss-Prot:SRS->4th ref, GO->5th ref #
# Call by: get_href({database=>'DB_name', IDs=>[param_1, .. , param_n], UniProt=>[0|1]});
# To read from an alternative link file, call by: get_href({database=>'DB_name::link_file_path', IDs=>[param_1, .. , param_n]});

sub get_href {

    my $parameters = $_[0];
    my $db = $parameters->{database};
    my @IDs = @{$parameters->{IDs}};
    my $dynamic = ($parameters->{dynamic} and $SERVER::interface)? 1 : 0; # for dynamic cross-references
    my $uniprot = ($parameters->{UniProt} and $SERVER::interface)? 1 : 0; # for UniProt external data cross-references
    my ($links_txt, $links_txt_uniprot) = ('links.txt', 'links.txt.uniprot');
    $get_href::last_link_file = "inc/$links_txt" if $SERVER::interface;
    my ($link_file, $same_link_file);
    undef($main::lien); $main::lien = '';

    if ($db =~/^(\w+)\:\:((?:\w|\/)+)$/) {
      $db = $1;
      $link_file = $2;
    }
    $db =~ tr/ /_/;

    if ($link_file and -e $link_file) {
      $get_href::last_link_file = $link_file;    
    }
    else {
      $same_link_file = 1;
    }


    unless (%main::links and $same_link_file) {
      use DbCrossRefs;
      my $DB_links = new DbCrossRefs;    
      $DB_links ->open("$get_href::last_link_file");
      %main::links = $DB_links->get_all();
      my ($db_key, $db_key_lc, $db_key_uc);
      foreach $db_key (keys %main::links) {
        $db_key_lc = lc($db_key);
        $main::links{$db_key_lc} = $main::links{$db_key} unless $main::links{$db_key_lc};
        $main::links{$db_key_uc} = $main::links{$db_key} unless $main::links{$db_key_uc};
      }
      warn "No Cross-Refernce links can be read from '$get_href::last_link_file'" unless %main::links;
      $main::links{'defined'} = 'N/A';
      $DB_links = undef;
    }

    if ($uniprot and !%main::uniprot_links) {
      my $uniprot_links_file = "$main::tmp_path/$links_txt_uniprot";
         $uniprot_links_file = "$get_href::last_link_file".'.uniprot' unless -e "$uniprot_links_file" ;
      if (-e "$uniprot_links_file") {
        use DbCrossRefs;
        my $DB_links = new DbCrossRefs;    
        $DB_links ->open("$uniprot_links_file");
        %main::links_uniprot = $DB_links->get_all();
        my ($db_key, $db_key_lc, $db_key_uc);
        foreach $db_key (keys %main::links_uniprot) {
          $db_key_lc = lc($db_key);
          $main::links_uniprot{$db_key_lc} = $main::links_uniprot{$db_key} unless $main::links_uniprot{$db_key_lc};
          $main::links_uniprot{$db_key_uc} = $main::links_uniprot{$db_key} unless $main::links_uniprot{$db_key_uc};
        }
        $main::links_uniprot{'defined'} = 'N/A';
        $DB_links = undef;
      }
      else {
        %main::uniprot_links = %main::links;
      } 
    }

    if ($dynamic and !%main::links_dynamic) {
      my ($command, $dbName, $displayer_url, $displayer_parameters, $url);
      $command = "SELECT XrefDBName, displayerURL, displayerParameters, url FROM XrefDBDynamic LIMIT 512";
      my @dynamic_fields = EXECUTE_FAST_COMMAND($command);
      for (my $ii = 0; $ii < scalar @dynamic_fields; $ii+=4) {
       ($dbName, $displayer_url, $displayer_parameters, $url) = @dynamic_fields[$ii..($ii+4)];
        $displayer_url .= "?".$displayer_parameters if $displayer_url and $displayer_parameters;
        $main::links_dynamic{$dbName} = ($displayer_url)? $displayer_url : $url;
      }
      $main::links_dynamic{'defined'} = 'N/A';

    }


    return '' unless $db;

    my $db_uc = uc($db); my $db_lc = lc($db); my $db_ucfirst = ucfirst($db); my $db_lc_ucfirst = ucfirst(lc($db));


    # In fact, there is no need for case checking on the 'dynamic' cross-references
    if ($uniprot) {
      $main::lien = ($main::links_uniprot{$db})? $main::links_uniprot{$db} :
                    ($main::links_uniprot{$db_ucfirst})? $main::links_uniprot{$db_ucfirst} : 
                    ($main::links_uniprot{$db_lc_ucfirst})? $main::links_uniprot{$db_lc_ucfirst} :
                    ($main::links_uniprot{$db_lc})? $main::links_uniprot{$db_lc} :
                    ($main::links_uniprot{$db_uc})? $main::links_uniprot{$db_uc} : '';
    }
    else {
      $main::lien = ($main::links{$db})? $main::links{$db} :
                    ($main::links{$db_ucfirst})? $main::links{$db_ucfirst} : 
                    ($main::links{$db_lc_ucfirst})? $main::links{$db_lc_ucfirst} :
                    ($main::links{$db_lc})? $main::links{$db_lc} :
                    ($main::links{$db_uc})? $main::links{$db_uc} : '';
    }

    # last: if not found, retrieve form database XrefDB (displayerURL, displayerParameters)
    if (!$main::lien) {
      my ($command, $displayer_url, $displayer_parameters, $url);
      $command = "SELECT displayerURL, displayerParameters, url FROM XrefDB WHERE XrefDBName = '$db'";
     ($displayer_url, $displayer_parameters, $url) = EXECUTE_FAST_COMMAND($command);
      $command = "SELECT displayerURL, displayerParameters, url FROM XrefDB WHERE XrefDBName ~* '$db' LIMIT 1";
     ($displayer_url, $displayer_parameters, $url) = EXECUTE_FAST_COMMAND($command) unless $displayer_url or $url;
      $displayer_url .= "?".$displayer_parameters if $displayer_url and $displayer_parameters;
      $main::lien = ($displayer_url)? $displayer_url : $url;
    }

    if (!$main::lien and $db =~ /^Swiss.?Prot|TrEMBL/i) { # future displayer for UniProt resources
      $main::lien = ($main::links{UniProt})? $main::links{UniProt} :  ($main::links_uniprot{UniProt})? $main::links_uniprot{UniProt} : '';
    }

    # replace any ExPASy domain by the defined $expasy address: 
    $main::lien =~ s/^http\:\/\/(?:\w+\.)?expasy\.org/$main::expasy/i if $main::expasy;


    my $counter = -1;
    foreach my $ID (@IDs) {
      $counter++;
      next unless $ID =~ /\w/;
      $ID =~ s/\s*\.\s*$//;
      $main::lien =~ s/\{$counter\}/$ID/;
    }

    return $main::lien;

}


#=============================================================================================================================#
#=============================================================================================================================#

# Perform the statistics generation HTML to be displayed on the server home interface


sub GENERATE_STATISTICS_HTML {

  my $stats;
  use CGI;
  my $co = new CGI;
  my ($command, $counter, $comment);
  my $gelData = {};
  my $speciesGels = {};
  my $unidentifiedProteinNickname = $main::unidentifiedProteinNickname;

  $stats = $co->p.$co->start_table();
  $stats.= $co->start_Tr().$co->start_td({-align=>'left'});

  $command = "SELECT Gel.gelID, Gel.shortName, Gel.fullName, Gel.dimension, Gel.organismID, ".
             "Organism.organismSpecies, Gel.organismStrain ".
             "FROM Gel, Organism WHERE Gel.organismID = Organism.organismID  ".
             "ORDER BY Organism.organismSpecies, Gel.shortName, Gel.dimension";
  my $gels = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $gelsNum = scalar @{$gels};
  my (%speciesCounter);
  for ($counter = 0; $counter <= ($gelsNum-1); $counter++) {
    my $gelID = ${$gels}[$counter][0];
    my $organismSpecies = ${$gels}[$counter][5];
    $gelData->{$gelID}->{shortname} = ${$gels}[$counter][1];
    $gelData->{$gelID}->{fullname} = ${$gels}[$counter][2];
    $gelData->{$gelID}->{dimension} = ${$gels}[$counter][3];
    $gelData->{$gelID}->{organismSpecies} = $organismSpecies;
    $gelData->{$gelID}->{organismStrain} = ${$gels}[$counter][6];
    push @{$speciesGels->{$organismSpecies}}, $gelID;
    my $speciesID = ${$gels}[$counter][4];
    $speciesCounter{$speciesID} = 1;
  }
  my $speciesNum = scalar (keys %speciesCounter);
  $command = "SELECT GelTissueSP.gelID, TissueSP.tissueSPDisplayedName FROM GelTissueSP, TissueSP ".
             "WHERE GelTissueSP.tissueSPname = TissueSP.tissueSPName";
  my $gelsTissue = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $gelsTissueNum = scalar @{$gelsTissue};
  for ($counter = 0; $counter <= ($gelsTissueNum-1); $counter++) {
    my $gelID = ${$gelsTissue}[$counter][0];
    $gelData->{$gelID}->{tissueSP} = ${$gelsTissue}[$counter][1];
  }

  $command = "SELECT DISTINCT SpotEntry.spotID, SpotEntry.gelID FROM SpotEntry ".
             " WHERE SpotEntry.AC <> '$unidentifiedProteinNickname'";
  my $spotsNum = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
     $spotsNum = scalar @{$spotsNum};

 (my $entriesNum) = EXECUTE_FAST_COMMAND("SELECT count(*) FROM Entry WHERE AC <> '$unidentifiedProteinNickname'");
 (my $referencesNum) = EXECUTE_FAST_COMMAND("SELECT count(*) FROM Reference");

  $stats.= $co->br.$co->start_table({-border=>1, -align=>'top'}).
    $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'General facts')).
    $co->th({-align=>'center', -bgcolor=>'#99FFFF'}, ['Species','Maps',
          'Identified&nbsp;spots', 'Identified&nbsp;proteins&nbsp;(entries)',
          'References'])."\n";
  $stats.= $co->start_Tr({-align=>'center'}).$co->td($speciesNum).$co->td($gelsNum).$co->td($spotsNum).$co->td($entriesNum).
           $co->td($referencesNum).$co->end_Tr.$co->end_table."\n";
  $stats.= $co->end_td.$co->end_Tr.$co->start_Tr().$co->start_td({-align=>'left'})."\n";

  $stats.= $co->br.$co->start_table({-border=>1, -align=>'top'}).
    $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'Species / Maps')).
    $co->th({-align=>'center', -bgcolor=>'#99FFFF'}, ['Species','Strain','Tissue&nbsp;(Swiss-Prot)','Map&nbsp;short&nbsp;name',
             'Map&nbsp;full&nbsp;name','Dimension']);
  my ($organismLast, $strainLast);
  foreach my $organismSpecies (sort keys %{$speciesGels}) {
    foreach my $gelID (sort @{$speciesGels->{$organismSpecies}}) {
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      my $organism = my $organismCopy = $co->strong($co->em($organismSpecies));
      $organism = ',,' if $organism eq $organismLast; $organismLast = $organismCopy;
      my $strain = my $strainCopy = $co->em($gelData->{$gelID}->{organismStrain});
      $strain = '--' unless $gelData->{$gelID}->{organismStrain};
      $strain = ',,' if $strain ne '--' and $organism eq ',,' and $strain eq $strainLast; $strainLast = $strainCopy;
      my $tissueSP = $gelData->{$gelID}->{tissueSP};
      $tissueSP = '--' unless $gelData->{$gelID}->{tissueSP};
      $stats.= $co->td($organism).$co->td($strain).$co->td($tissueSP).$co->td($gelData->{$gelID}->{shortname}).
               $co->td($gelData->{$gelID}->{fullname}).$co->td($gelData->{$gelID}->{dimension}.'-D');
      $stats.= $co->end_Tr."\n";
    }
  }
  $stats.= $co->end_table."\n";
  $stats.= $co->end_td.$co->end_Tr.$co->start_Tr().$co->start_td({-align=>'left'})."\n";

  $command = "SELECT Gel.gelID, count(DISTINCT SpotEntry.spotID), count(DISTINCT SpotEntry.AC) FROM SpotEntry, Gel, Organism ".
             "WHERE Gel.gelID = SpotEntry.GelID AND Gel.organismID = Organism.organismID ".
             "AND SpotEntry.AC <> '$unidentifiedProteinNickname' ".
             "GROUP BY Organism.organismSpecies, Gel.gelID, Gel.shortName ORDER BY Organism.organismSpecies, Gel.shortName";
  my $spotsMaps = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  $command = "SELECT Gel.gelID, count(DISTINCT Spot.spotID) FROM Spot, Gel WHERE Gel.gelID = Spot.GelID ".
             "GROUP BY Gel.gelID, Gel.shortName ORDER BY Gel.shortName";
  my @allSpotsMaps = EXECUTE_FAST_COMMAND($command);
  $command = "SELECT GelImage.gelID, GelImage.detectedSpots FROM GelImage, Gel ".
             "WHERE GelImage.gelID = Gel.gelID ORDER BY Gel.shortName";
  my @allSpotsMapsGelImage = EXECUTE_FAST_COMMAND($command);
  my ($allSpotsMaps, $allSpotsMapsGelImage);
  for ($counter = 0; $counter < scalar @allSpotsMaps; $counter+=2) {
    $allSpotsMaps->{$allSpotsMaps[$counter]} = $allSpotsMaps[$counter+1];
    $allSpotsMapsGelImage->{$allSpotsMapsGelImage[$counter]} = $allSpotsMapsGelImage[$counter+1];
  }

  my $spotsMapsNum = scalar @{$spotsMaps};
  if ($spotsMapsNum) {
    $stats.= $co->br.$co->start_table({-border=>1, -align=>'top'}).
      $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'},'Spots and Proteins Distribution'));
    $stats.= $co->th({-align=>'center', -bgcolor=>'#99FFFF'},
        ['Species','Map','Identified&nbsp;spots', 'Detected&nbsp;spots',
         'Identified&nbsp;proteins&nbsp;(entries)']);
    $organismLast = '';
    for ($counter = 0; $counter <= ($spotsMapsNum-1); $counter++) {
      my $gelID = ${$spotsMaps}[$counter][0];
      my $organism = my $organismCopy = $co->strong($co->em($gelData->{$gelID}->{organismSpecies}));
      $organism = ',,' if $organism eq $organismLast; $organismLast = $organismCopy;
      my $detectedSpots = ($allSpotsMaps->{$gelID} > ${$spotsMaps}[$counter][1])? $allSpotsMaps->{$gelID}
                        :  $allSpotsMapsGelImage->{$gelID};
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      $stats.= $co->td($organism).
               $co->td({-align=>'left'}, '&nbsp;'.$gelData->{$gelID}->{shortname}.' ('.$gelData->{$gelID}->{fullname}.')').
               $co->td(${$spotsMaps}[$counter][1]).$co->td($detectedSpots).$co->td(${$spotsMaps}[$counter][2]);
      $stats.= $co->end_Tr."\n";
    }
    $stats.= $co->end_table.$co->br."\n";
  }

  $command = "SELECT Organism.organismSpecies, count(DISTINCT SpotEntry.AC) ".
             "FROM Gel, Organism, SpotEntry WHERE Gel.organismID = Organism.organismID ".
             " AND Gel.gelID = SpotEntry.gelID AND SpotEntry.AC <> '$unidentifiedProteinNickname' ".
             "GROUP BY Organism.organismSpecies ORDER BY Organism.organismSpecies";

  my $speciesEntries = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $speciesEntriesNum = scalar @{$speciesEntries};
  if ($speciesEntriesNum) {
    $stats.= $co->br.$co->start_table({-border=>1, -align=>'top'}).
      $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'Species Distribution of Entries'));
    $stats.= $co->th({-align=>'center', -bgcolor=>'#99FFFF'}, ['Species','Number&nbsp;of&nbsp;entries', 'Frequency&nbsp(%)']);
    for ($counter = 0; $counter <= ($speciesEntriesNum-1); $counter++) {
      my $entriesRatio = ($entriesNum)? sprintf "%.2f", ${$speciesEntries}[$counter][1]*100/($entriesNum) : 'not defined';
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      $stats.= $co->td({-align=>'left'}, $co->strong(${$speciesEntries}[$counter][0])).
               $co->td(${$speciesEntries}[$counter][1]).$co->td($entriesRatio.'%');
      $stats.= $co->end_Tr."\n";
    }
    $stats.= $co->end_table.$co->br."\n";
  }

  $command = 
    "SELECT MappingTopicDefinition.mappingTechnique, MappingTopicDefinition.techniqueDescription, count(SpotEntryMappingTopic.*) ".
    "FROM SpotEntryMappingTopic, MappingTopicDefinition WHERE SpotEntryMappingTopic.AC <> '$unidentifiedProteinNickname' ".
    " AND MappingTopicDefinition.mappingTechnique = ANY (SpotEntryMappingTopic.mappingTechnique) ".
    "GROUP BY MappingTopicDefinition.mappingTechnique, MappingTopicDefinition.techniqueDescription ".
    "ORDER BY  MappingTopicDefinition.techniqueDescription";
  my $spotsMapping = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $spotsMappingNum = scalar @{$spotsMapping};
  if ($spotsMappingNum) {
    $stats.= $co->br.$co->start_table({-border=>1, -align=>'top'}).
      $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'Identification Methods Distribution (Mapping)'));
    $stats.= $co->th({-align=>'center', -bgcolor=>'#99FFFF'},
        ['Identification&nbsp;method','Abbreviation', 'Number&nbsp;of&nbsp;spots', 'Frequency&nbsp(%)']);
    my $spotsRatioSum;
    for ($counter = 0; $counter <= ($spotsMappingNum-1); $counter++) {
      my $spotsRatio = ($spotsNum)? sprintf "%.2f", ${$spotsMapping}[$counter][2]*100/($spotsNum) : 'not defined';
      $spotsRatioSum += $spotsRatio;
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      $stats.= $co->td({-align=>'left'}, $co->strong(${$spotsMapping}[$counter][1])).
               $co->td(${$spotsMapping}[$counter][0]).
               $co->td(${$spotsMapping}[$counter][2]).$co->td($spotsRatio.'%');
      $stats.= $co->end_Tr."\n";
    }
    $comment = ($spotsRatioSum > 100)? 'Total exceeds 100% as some of the spots have been identified by several methods.' : '';
    $stats.= $co->end_table.$co->span({class=>'Comment'}, $comment).$co->br."\n";
  }

  my $maxSubRelease = EXECUTE_FAST_COMMAND("SELECT max(subRelease) FROM Release");
  my $lengthSubRelease = length($maxSubRelease);
  $command = "SELECT Release.releaseNum, Release.subRelease, ".
             "to_char(Release.releaseDate, 'DD MonthYYYY') as formattedDate, count(Entry.AC) ".
             "FROM Entry, Release WHERE Entry.AC <> '$unidentifiedProteinNickname' ".
             "AND Entry.releaseCreation = Release.releaseNum AND Entry.subReleaseCreation = Release.subRelease ".
             "GROUP BY Release.releaseNum, Release.subRelease, formattedDate ORDER BY Release.releaseNum DESC, Release.subRelease DESC";
  my $entriesCreation = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $entriesCreationNum = scalar @{$entriesCreation};
  if ($entriesCreationNum) {
    $stats.= $co->br.$co->start_table({-border=>1, -align=>'top'}).
      $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'Entries: Created entries per Release'));
    $stats.= $co->th({-align=>'center', -bgcolor=>'#99FFFF'},
        ['Release','Release&nbsp;Date&nbsp', 'Number&nbsp;of&nbsp;entries']);
    for ($counter = 0; $counter <= ($entriesCreationNum-1); $counter++) {
      my $subRelease = ${$entriesCreation}[$counter][1];
      $subRelease = "0"x($lengthSubRelease - length($subRelease)).$subRelease;
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      $stats.= $co->td({-align=>'left'}, '&nbsp;'.${$entriesCreation}[$counter][0].'.'.$subRelease).
               $co->td({-align=>'left'}, '&nbsp;'.${$entriesCreation}[$counter][2]).
               $co->td(${$entriesCreation}[$counter][3]);
      $stats.= $co->end_Tr."\n";
    }
    $stats.= $co->end_table.$co->br."\n";
  }

  $stats.= $co->start_table().$co->start_Tr().$co->start_td({-align=>'left', -valign=>'top'})."\n";
  my $releaseModifFound = 0;
  $command = "SELECT releaseNum, subRelease, to_char(releaseDate, 'DD MonthYYYY') as formattedDate ".
             "FROM Release ORDER BY releaseNum DESC, subRelease DESC";
  my $releases = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $releasesNum = scalar @{$releases};
  my $releaseData;
  for ($counter = 0; $counter <= ($releasesNum-1); $counter++) {
    my $releaseNum = ${$releases}[$counter][0];
    my $releaseSub = ${$releases}[$counter][1];
    $releaseData->{$releaseNum}->{$releaseSub}->{date} = ${$releases}[$counter][2];
  }
  $command = "SELECT common.make2db_release_of_date(EntryVersion2D.versionDate, FALSE, TRUE) AS verDist, count(EntryVersion2D.AC) ".
             "FROM EntryVersion2D WHERE EntryVersion2D.AC <> '$unidentifiedProteinNickname' GROUP BY verDist ORDER BY verDist DESC";
  my $entries2DVersions = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $entries2DVersionsNum = scalar @{$entries2DVersions};
  if ($entries2DVersionsNum) {
    $stats.= $co->start_table({-border=>1}).
      $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'Entries: Last 2D Annotation Modification'));
    $stats.= $co->th({-align=>'center', -bgcolor=>'#99FFFF'},
        ['Release', 'Release&nbsp;Date', 'Number&nbsp;of&nbsp;modified&nbsp;entries']);
    for ($counter = 0; $counter <= ($entries2DVersionsNum-1); $counter++) {
      my $releaseNumSub = ${$entries2DVersions}[$counter][0];
      $releaseNumSub .= '.0' unless $releaseNumSub =~ /\.\d+$/;
      next unless $releaseNumSub =~ /(\d+)\.(\d+)/;
      my ($releaseNum, $releaseSub) = ($1,$2);
      my $releaseSubTrunc = ($releaseSub =~ /([1-9]\d*)$/)? $1 : 0;
      $releaseSub = "0"x($lengthSubRelease - length($releaseSub)).$releaseSub;
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      $stats.= $co->td({-align=>'left'}, '&nbsp;'.$releaseNum.'.'.$releaseSub).
               $co->td('&nbsp;'.$releaseData->{$releaseNum}->{$releaseSubTrunc}->{date}).
               $co->td( ${$entries2DVersions}[$counter][1]);
    }
    $stats.= $co->end_table."\n";
    $releaseModifFound = 1;
  }
  $command = "SELECT common.make2db_release_of_date(EntryVersionGeneral.versionDate, FALSE, TRUE) AS verDist, ".
             "count(EntryVersionGeneral.AC) ".
             "FROM EntryVersionGeneral WHERE EntryVersionGeneral.AC <> '$unidentifiedProteinNickname' GROUP BY verDist ORDER BY verDist DESC";
  my $entriesGeneralVersions = EXECUTE_FAST_COMMAND($command, {fetch_method=>'fetchall_arrayref'});
  my $entriesGeneralVersionsNum = scalar @{$entriesGeneralVersions};
  if ($entriesGeneralVersionsNum) {
    $stats.= $co->end_td.$co->td('&nbsp;&nbsp;&nbsp;').$co->start_td(-valign=>'top')."\n" if ($releaseModifFound);
    $stats.= $co->start_table({-border=>1}).
      $co->caption({-class=>"preHR left"}, $co->strong({-class=>'brown'}, 'Entries: Last General Annotation Modification'));
    $stats.= $co->th({-align=>'center', -bgcolor=>'#99FFFF'},
        ['Release', 'Release&nbsp;Date', 'Number&nbsp;of&nbsp;modified&nbsp;entries']);
    for ($counter = 0; $counter <= ($entriesGeneralVersionsNum-1); $counter++) {
      my $releaseNumSub = ${$entriesGeneralVersions}[$counter][0];
      $releaseNumSub .= '.0' unless $releaseNumSub =~ /\.\d+$/;
      next unless $releaseNumSub =~ /(\d+)\.(\d+)/;
      my ($releaseNum, $releaseSub) = ($1,$2);
      my $releaseSubTrunc = ($releaseSub =~ /([1-9]\d*)$/)? $1 : 0;
      $releaseSub = "0"x($lengthSubRelease - length($releaseSub)).$releaseSub;
      $stats.= $co->start_Tr({-align=>'center'})."\n";
      $stats.= $co->td({-align=>'left'}, '&nbsp;'.$releaseNum.'.'.$releaseSub).
               $co->td('&nbsp;'.$releaseData->{$releaseNum}->{$releaseSubTrunc}->{date}).
               $co->td( ${$entriesGeneralVersions}[$counter][1]);
    }
    $stats.= $co->end_table."\n";
    $releaseModifFound = 1;
  }
  $comment = ($releaseModifFound)?
    'Any modifications occuring after the current database release update are temporarily assigned to this current release.' : '';
  $stats.= $co->end_td.$co->end_Tr.$co->end_table."\n";
  $stats.= $co->span({class=>'Comment'}, $comment).$co->br."\n";


  undef($gels);
  undef($gelsTissue);
  undef ($spotsMaps);
  undef($allSpotsMaps);
  undef($speciesEntries);
  undef($spotsMapping);
  undef($entriesCreation);
  undef($releases);
  undef($entries2DVersions);
  undef($entriesGeneralVersions);


  $stats.= $co->end_td.$co->end_Tr."\n";
  $stats.= $co->end_table.$co->p."\n";

  return $stats;

}



#=============================================================================================================================#
#=============================================================================================================================#

# Generate a minimal flat file from a collection of tabulated report files
# The generated file has a first line: '__GENERATED_FLAT_FILE__'

# Returns, in addition of physically writing a flat file,
# a structure holding the flat: $returnGeneratedFlatFile
# and a structure holding the spots data: $returnSpotData

# Accepted formats:


# With headers:

# Spot \t ... (list all recognized headers) -> cf documentation

# Method is free-text, or from list!
# By default, FREE Methods are considered 2D topics (generating the "2D" lines),
# Except if prefixed by the word "COMMENT: ", which make them free text comments (generating the "CC" lines)

# Without headers (deprecated) : Two formats:

#1 => Spot \t X \t Y \t pI \t Mw \t [AC1] \t [IdentMethod1,IdentMethod2],.. \t [PMF] \t [MS/MS] \t [AMINO ACID COMPOSITION] \t [%od] \t [%vol]
#2 => Spot \t X \t Y \t pI \t Mw \t [AC1 AC2 AC3]

sub GENERATE_FLAT_FILE {

  my $returnGeneratedFlatFile;
  my $returnSpotData;
  $returnGeneratedFlatFile->{statusOK} = 0;
  $returnGeneratedFlatFile->{statusErrorMessage} = undef;

  my $parameters = $_[0];
  my %mapFiles = %{$parameters->{mapFiles}};
  my $outputFile = $parameters->{outputFile};
  my $noSpotData = ($parameters->{noSpotData})? 1 : 0;
  my %mapsMappingMethods = %{$parameters->{mapsMappingMethods}};

  use IO::File;
  my $outputFileHandle = new IO::File;
  $outputFileHandle->open(">$outputFile") or return undef;

  my (%generatedDat, %generatedDatExp, $generatedDat, %generatedMappingMethods, %acceptedUnknownMappingMethod);
  my (%generatedFree2D, $generatedFree2D, %generatedCommentBlock, %generatedReference, $commentBlock, %generatedXRefs);
  my $privateKeyword = ($main::privateKeyword)? $main::privateKeyword : ' {private}';
  my $defaultDataDirectory = ($main::data_dir)? $main::data_dir : '.';

  my $generatedFlatFilePattern = ($main::generatedFlatFilePattern)? $main::generatedFlatFilePattern : '__GENERATED_FLAT_FILE__';
  my $spotDataSectionPattern = ($main::spotDataSectionPattern)? $main::spotDataSectionPattern : '__SPOT_DATA__';
  my %headerRank;
  my %verifyMapDimension;

  my $srcDir = ($Make2D::src_dir)? $Make2D::src_dir : '../src';
  my $tempDir = ($Make2D::temp_dir)? $Make2D::temp_dir : '.';

  my $inSilicoDir = "$srcDir/InSilicoSpectro";
  my $inSilicoConverter = "$inSilicoDir/convertSpectra.pl";
  my $inSilicoConverterChecked = my $noInSilicoConverter = 0;

  my $referenceFile = $defaultDataDirectory."/". (($main::referenceFile)? $main::referenceFile : 'reference.txt');

  my $msmsFileType = my $msmsFileTypeDefault = 'mgf';
  my @msmsFileType = ('mgf','dta', 'pkl','btdx', 'peptMatches','mzdata','mzxml');
  my $pmfFileTypeDefault = 'dta';
  my $pmfDefaultEnzyme = 'TRYPSIN';
  my $msFileMaxLineLength = ($main::msFileMaxLineLength > 400 and $main::msFileMaxLineLength < 100000)?
     $main::msFileMaxLineLength : 20000;
  my ($pmfEnzyme, %unacceptedFileTypes);

  my $warnBuffer;


  my %reference; my $currentRefNumber = 0; my $referenceStatusOK = 0; my $referenceStatusWarn = 0;
  my %replacedMappingMethodKey;
  if (-e $referenceFile) {
    if (open REF, "<$referenceFile") {
      $referenceStatusOK = 1;
      while (my $refLine = <REF>) {
        chomp($refLine);
        next unless $refLine =~ /\w/;
        if ($refLine =~ /^(?:RN)?\s*\[(\d+)\]/) {
          $currentRefNumber = $1;
        }
        else {
          $reference{$currentRefNumber} .= $refLine."\n";
        }
      }

      close REF;
    }
  }
  my $RNcounter = {}; my $acReferenceCorrespondingRN = {}; my $acMappingMethodMapCorrespondingRN = {};

  foreach my $map (keys %mapFiles) {
    my $map_handle = new IO::File;
    my $previousSpotHeaders;
    unless ($map_handle->open($mapFiles{$map})) {
      $returnGeneratedFlatFile->{statusErrorMessage} =
        "Could not find ".$mapFiles{$map}." to read the map data - Check the file exists and is correctly cased!";
      return ($returnGeneratedFlatFile, undef);
    }
    my $headersDefined = undef;
    my $firstLine = 1;
    while (my $line = <$map_handle>) {
      chomp($line); $line =~ s/\r//g; $line =~ s/\n/ /g;
      # get rid of any surrounding single or double quotes around columns
      /()/; $line =~ s/(^|\t)(\"|\')([^\t]+)\2(\t|$)/$1$3$4/g; # or $line =~ s/(?:(^|\t)(?:\"|\')|(?:\"|\')(\t|$))/$1$2/g; 
      next unless $line =~ /\w/;
      if ($firstLine) {
        undef($firstLine);
        my ($counter, $rank);
        # some spreadsheets export columns surrounded by quotes
        $line =~ s/\"//g; $line =~ s/(\'\t|\t\')/\t/g; $line =~ s/(^\'|\'$)//g;
        $line =~ s/^ *//; $line =~ s/ *$//; $line =~ s/ *\t */\t/mg;
        if ($line =~ /^SPOTS?\t/i) {
          undef(%headerRank);
          $headersDefined = 1;
          my (@headers, $extraHeaders);
          my @mandatoryHeaders   = ('SPOT','X','Y','MW');
          my @preDefinedHeaders  = ('PI', 'AC', 'MAPPING METHODS', 'AMINO ACID', 'OD', 'VOLUME', 'REFERENCE');
          push @preDefinedHeaders, ('MASS SPECTROMETRY', 'PEPTIDES', 'MS FILE', 'MS URI', 'MS IDENT-FILE', 'MS IDENT-URI');
          push @preDefinedHeaders, ('PMF', 'PMF FILE', 'PMF URI', 'PMF IDENT-FILE', 'PMF IDENT-URI', 'XREF');
          $line = uc($line);
          @headers = split "\t", $line;
          grep {$_ =~ s/(^\s+|\s+$)//g; $_ =~ s/^(?:SPOTS|BANDS?)$/SPOT/} @headers;
          grep {$_ =~ s/^REFERENCES$/REFERENCE/} @headers;
          grep {$_ =~ s/^(?:CROSS-?REFERENCES?|DR)$/XREF/} @headers;
          grep {$_ =~ s/^MS(?:\/?MS)?$/MASS SPECTROMETRY/; s/^MS\/?MS\s+/MS /; $_ =~ s/^TANDEM MASS SPECTROMETRY$/MASS SPECTROMETRY/} @headers;
          grep {$_ =~ s/^PEPTIDE$/PEPTIDES/} @headers;
          grep {$_ =~ s/^\%OD$/OD/; $_ =~ s/^\%?VOL(UME)?$/VOLUME/; $_ =~ s/^COMMENT(?:\s*\:\s*)(\S+)/COMMENT\: $1/} @headers;

          foreach my $mandatoryHeader (@mandatoryHeaders) {
           ($counter, $rank) = (0, undef); # Do not set counter to '-1' otherwise grep fails with counter++ !
            my $mandatoryHeaderFound = grep {$counter++; $rank = $counter if $_ eq $mandatoryHeader } @headers;
            unless ($mandatoryHeaderFound) {
              $returnGeneratedFlatFile->{statusErrorMessage} = "Could not find within your map spreadsheet report headers (in ".
                $mapFiles{$map}.") a mandatory header:\'$mandatoryHeader\'!\n\nPlease, define this column header first!\n";
              return ($returnGeneratedFlatFile, undef);
            }
            $headerRank{$mandatoryHeader} = { category=>'mandatory', rank=>($rank-1) };
          }
          foreach my $preDefinedHeader (@preDefinedHeaders) {
           ($counter, $rank) = (0, undef);
            my $preDefinedHeaderFound = grep {$counter++; $rank = $counter if $_ eq $preDefinedHeader } @headers;
            $headerRank{$preDefinedHeader} = { category=>'predefined', rank=>($rank-1) } if $preDefinedHeaderFound;
          }
          $rank = -1;
          foreach my $header (@headers) {
            $rank++;
            next if $headerRank{$header} or $header !~ /\w/;
            if ($header =~ /^COMMENT\: /) {
             (my $headerDisplayed = $header) =~ s/^COMMENT\:\s+//;
              next unless $headerDisplayed =~ /\w/;
              $headerRank{$header} = { category=>'comment', rank=>$rank, displayed=>$headerDisplayed };
            }
            else {
              $headerRank{$header} = { category=>'free', rank=>$rank };
            }
          }
          if ($headerRank{REFERENCE} and !$referenceStatusOK and !$referenceStatusWarn) {
            warn "\nWarning: could not open the expected reference file at: '$referenceFile'.".
                 " No specific references will be attached to your entries.\n";
            $referenceStatusWarn = 1;
            $warnBuffer = 1;
          }
          next;
        }
      }
      /()/;
      my ($ex_spot, $ex_x, $ex_y, $ex_pi, $ex_mw, $ex_acs, $mappingMethods, $aa, $od, $vol, $references, $xrefs);
      my ($msms, $peptSeq, $msmsFile, $msmsURI, $msmsIdentFile, $msmsIdentURI, @msmsURI, @msmsIdentURI, @msmsIdentFile);
      my ($pmf, $pmfFile, $pmfURI, $pmfIdentFile, $pmfIdentURI, @pmfURI, @pmfIdentURI, @pmfIdentFile);
      $commentBlock = $generatedFree2D = '';
      if ($headersDefined) {
        my @lineValues = split "\t", $line;
        grep {$_ =~ s/(?:^\s+|\s+$)//g} @lineValues;

        $ex_spot        = $lineValues[$headerRank{'SPOT'}->{rank}];
        $ex_x           = $lineValues[$headerRank{'X'}->{rank}];
        $ex_x = $previousSpotHeaders->{$ex_spot}->{'X'} unless length($ex_x);
        $ex_y           = $lineValues[$headerRank{'Y'}->{rank}];
        $ex_y = $previousSpotHeaders->{$ex_spot}->{'Y'} unless length($ex_y);
        $ex_mw          = $lineValues[$headerRank{'MW'}->{rank}];
        $ex_mw = $previousSpotHeaders->{$ex_spot}->{'MW'} unless length($ex_mw);
        unless (length($ex_spot) and length($ex_x) and length($ex_y) and length($ex_mw)) { # accept '0's
          print "\n  ignoring a report line for $map (some mandatory fields are absent):\n   $line\n";
          next;
        }
        $ex_pi          = $lineValues[$headerRank{'PI'}->{rank}] if $headerRank{'PI'} =~ /\w/;
        $ex_pi = $previousSpotHeaders->{$ex_spot}->{'PI'} unless length($ex_pi);
        $previousSpotHeaders->{$ex_spot}->{'X'} = $ex_x;
        $previousSpotHeaders->{$ex_spot}->{'Y'} = $ex_y;
        $previousSpotHeaders->{$ex_spot}->{'MW'} = $ex_mw;
        $previousSpotHeaders->{$ex_spot}->{'PI'} = $ex_pi;
        $ex_acs         = uc($lineValues[$headerRank{'AC'}->{rank}]) if $headerRank{'AC'} =~ /\w/;
        $mappingMethods = $lineValues[$headerRank{'MAPPING METHODS'}->{rank}] if $headerRank{'MAPPING METHODS'} =~ /\w/;
        $pmf            = $lineValues[$headerRank{'PMF'}->{rank}] if $headerRank{'PMF'} =~ /\w/;
        $msms           = $lineValues[$headerRank{'MASS SPECTROMETRY'}->{rank}] if $headerRank{'MASS SPECTROMETRY'} =~ /\w/;
        $peptSeq        = $lineValues[$headerRank{'PEPTIDES'}->{rank}] if $headerRank{'PEPTIDES'} =~ /\w/;
        $aa             = $lineValues[$headerRank{'AMINO ACID'}->{rank}] if $headerRank{'AMINO ACID'} =~ /\w/;
        $od             = $lineValues[$headerRank{'OD'}->{rank}] if $headerRank{'OD'} =~ /\w/;
        $vol            = $lineValues[$headerRank{'VOLUME'}->{rank}] if $headerRank{'VOLUME'} =~ /\w/;
        $references     = $lineValues[$headerRank{'REFERENCE'}->{rank}] if $headerRank{'REFERENCE'} =~ /\w/;
        $msmsFile       = $lineValues[$headerRank{'MS FILE'}->{rank}] if $headerRank{'MS FILE'} =~ /\w/;
        $msmsURI        = $lineValues[$headerRank{'MS URI'}->{rank}] if $headerRank{'MS URI'} =~ /\w/;
        $msmsIdentFile  = $lineValues[$headerRank{'MS IDENT-FILE'}->{rank}] if $headerRank{'MS IDENT-FILE'} =~ /\w/;
        $msmsIdentURI   = $lineValues[$headerRank{'MS IDENT-URI'}->{rank}] if $headerRank{'MS IDENT-URI'} =~ /\w/;
        $pmfFile        = $lineValues[$headerRank{'PMF FILE'}->{rank}] if $headerRank{'PMF FILE'} =~ /\w/;
        $pmfURI         = $lineValues[$headerRank{'PMF URI'}->{rank}] if $headerRank{'PMF URI'} =~ /\w/;
        $pmfIdentFile   = $lineValues[$headerRank{'PMF Ident-FILE'}->{rank}] if $headerRank{'PMF Ident-FILE'} =~ /\w/;
        $pmfIdentURI    = $lineValues[$headerRank{'PMF Ident-URI'}->{rank}] if $headerRank{'PMF Ident-URI'} =~ /\w/;
        $xrefs          = $lineValues[$headerRank{'XREF'}->{rank}] if $headerRank{'XREF'} =~ /\w/;

        # If some URI references are given without any MS values nor files, we force by convention void values to make data 'defined'
        if ((!$msms and !$msmsFile) and ($msmsURI =~ /\w/ or $msmsIdentURI =~ /\w/)) {
          $msms = "0.0";
        }
        if ((!$pmf and !$pmfFile) and ($pmfURI =~ /\w/ or $pmfIdentURI =~ /\w/)) {
          $pmf = "0.0; Undefined";
        }

        $msmsURI =~ s/\s+/ /g; $msmsIdentURI =~ s/\s+/ /g; $msmsIdentFile =~ s/\s+/ /g; 
        $pmfURI =~ s/\s+/ /g; $pmfIdentURI =~ s/\s+/ /g;  $pmfIdentFile =~ s/\s+/ /g;
        @msmsURI = split ' ', $msmsURI; @msmsIdentURI = split ' ', $msmsIdentURI; @msmsIdentFile = split ' ', $msmsIdentFile;
        @pmfURI = split ' ', $pmfURI;  @pmfIdentURI = split ' ', $pmfIdentURI; @pmfIdentFile = split ' ', $pmfIdentFile;


        foreach my $header (keys %headerRank) {
          if ($headerRank{$header}->{category} eq 'comment' and $lineValues[$headerRank{$header}->{rank}]) {
           (my $freeCommentLine = "CC   -!- ".$headerRank{$header}->{displayed}.": ".$lineValues[$headerRank{$header}->{rank}]."\n")
            =~ s/\.*\s*\n$/\.\n/;
            my $freeCommentLineQM = quotemeta($freeCommentLine);
            $commentBlock .= $freeCommentLine unless $commentBlock =~ /\b$freeCommentLineQM\b/;
          }
          if ($headerRank{$header}->{category} eq 'free' and $lineValues[$headerRank{$header}->{rank}]) {
            my $dimension = ($ex_pi > 0)? 2 : 1;
            my $free2DLine = $lineValues[$headerRank{$header}->{rank}];
            if ($free2DLine !~ s/^\s*\*\s*// and $header !~ s/^\s*\*\s*//) {
              my $specificSpot = (($dimension == 2)? 'SPOT' : 'BAND')." $ex_spot: ";
              $free2DLine =~ s/(?:^\s*;?\s*)|(?:\s*;?\s*$)//g;
              $free2DLine =~ s/;\W*;/;/g;
              $free2DLine =~ s/;\s*/; $specificSpot/g;
              $free2DLine = $specificSpot.$free2DLine;
            }
           (my $headerDisplayed = $header) =~ s/^\s*\*\s*//;
           ($free2DLine = $dimension."D   -!-   ".$headerDisplayed.": ".$free2DLine."\n") =~ s/\.*\s*\n$/\.\n/;
            my $free2DLineQM = quotemeta($free2DLine);
            $generatedFree2D .= $free2DLine unless $generatedFree2D =~ /\b$free2DLineQM\b/;
          }
        }

      }
      # old format: deprecated
      else {
        unless ($line =~ /^\s*(\S+)\t(\d+)\t(\d+)\t(\S*)\t(\S+)(?:\t([^\t]*))?(?:\t([^\t]*))?(?:\t([^\t]*))?(?:\t([^\t]*))?(?:\t([^\t]*))?(?:\t(\d?\.?\d+))?(?:\t(\d?\.?\d+))?/) {
          print "\n  ignoring a report line for $map:\n   $line\n";
          next;
        }
        ($ex_spot, $ex_x, $ex_y, $ex_pi, $ex_mw, $ex_acs, $mappingMethods, $pmf, $msms, $aa, $od, $vol) =
          ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);
      }

      $ex_acs = $main::unidentifiedProteinNickname if !$ex_acs and ($main::include_not_identified_spots) and $main::unidentifiedProteinNickname;
      $ex_mw = int($ex_mw);
      $ex_spot = $ex_x."-".$ex_y if ($ex_spot eq '-1'); # some exports (Melanie) has spot = -1

      $returnSpotData->{$map}->{$ex_spot}->{spotID} = $ex_spot;
      $returnSpotData->{$map}->{$ex_spot}->{xPosition} = $ex_x;
      $returnSpotData->{$map}->{$ex_spot}->{yPosition} = $ex_y;
      $returnSpotData->{$map}->{$ex_spot}->{pI} = $ex_pi;
      $returnSpotData->{$map}->{$ex_spot}->{Mw} = $ex_mw;
      $returnSpotData->{$map}->{$ex_spot}->{relativeOd} = $od;
      $returnSpotData->{$map}->{$ex_spot}->{relativeVolume} = $vol;

      # several files or URIs may be separated by spaces
     ($returnSpotData->{$map}->{$ex_spot}->{msmsFiles} .= $msmsFile) =~ s/\S+\://g;
      $returnSpotData->{$map}->{$ex_spot}->{msmsURI} .= $msmsURI;
     ($returnSpotData->{$map}->{$ex_spot}->{msmsIdentFiles} .= $msmsIdentFile) =~ s/\S+\://g;
      $returnSpotData->{$map}->{$ex_spot}->{msmsIdentURI} .= $msmsIdentURI;
     ($returnSpotData->{$map}->{$ex_spot}->{pmfFiles} .= $pmfFile) =~ s/\S+\://g;
      $returnSpotData->{$map}->{$ex_spot}->{pmfURI} .= $pmfURI;
     ($returnSpotData->{$map}->{$ex_spot}->{pmfIdentFiles} .= $pmfIdentFile) =~ s/\S+\://g;
      $returnSpotData->{$map}->{$ex_spot}->{pmfIdentURI} .= $pmfIdentURI;

      my $dimension = ($ex_pi > 0)? 2 : 1;
      if ($verifyMapDimension{$map} and $verifyMapDimension{$map} != $dimension) {
        $returnGeneratedFlatFile->{statusErrorMessage} =
          "A fatal error reading the $map report: No consitency in the given pI values!\n".
          "Either you define a pI value for *each* spot (for 2D gels), or you do not give *any* value (for 1D gels).";
          return ($returnGeneratedFlatFile, undef);
      }
      $verifyMapDimension{$map} = $dimension;

      if ($pmf) {
        $pmf =~ s/(\W)?\s*$// unless $pmf =~ /$privateKeyword\s*$/;
        $pmf .= '; TRYPSIN' if $pmf =~ /\d$/;
      }
      for my $refIdentification ($pmf, $msms, $peptSeq, $aa) {
        my $hiddenIdentification = ($refIdentification  =~ s/\s*$privateKeyword\s*//gi)?
          $privateKeyword : undef;
        $refIdentification .= $hiddenIdentification;
      }


      foreach my $ex_ac (split ' ', $ex_acs) {
        if ($commentBlock) {
          $commentBlock =~ s/\n$//;
          my @commentLines = split "\n", $commentBlock;
          foreach my $commentLine (@commentLines) {
            next unless $commentLine  =~ /\w/;
            my $commentLineQM = quotemeta($commentLine);
            $generatedCommentBlock{$ex_ac} .= $commentLine."\n"
              unless $generatedCommentBlock{$ex_ac} =~ /(^|\n)$commentLineQM\n/i;
          }
          undef($commentBlock);
        }
        if ($xrefs and $ex_ac) {
          my @xrefs = split '&', $xrefs;
          foreach my $xref (@xrefs) {
            unless ($xref =~ /^\s*(\S+)\s+(.+)$/) {
              warn "\nAmbigious cross-reference syntax ('$xref') for entry '$ex_ac' ($map)!\n".
                   "The correct syntax is: Database1 ID1; ID2;.. IDn & Database2 ID1...\n".
                   "Discard this cross-reference and continue anyway? (type 'y' to accept)\n";
              chomp(my $input = readline(*STDIN));
              unless ($input =~ /^y(?:es)?$/i) {
                $returnGeneratedFlatFile->{statusErrorMessage} =
                  "'$xref' (incorrect cross-reference syntax!) for '$ex_ac' ($map): cannot generate a flat file!";
                return ($returnGeneratedFlatFile, undef);
              }
              next;
            }
            my ($xrefDB, $xrefID) = ($1, $2);
            $xrefID =~ s/(?:\s|;)$//g;
            my ($xrefDBQM, $xrefIDQM) = (quotemeta($xrefDB), quotemeta($xrefID));
            next if ($generatedXRefs{$ex_ac}) =~ /\bDR\s+$xrefDBQM; $xrefIDQM.\n/;
            $generatedXRefs{$ex_ac} .= "DR   $xrefDB; $xrefID.\n";
          }
        }
        unless ($generatedDat{$ex_ac}->{$map}) {
          $generatedDat{$ex_ac}->{$map} = $dimension."D   -!- MASTER: $map;\n";
        }
        my $newLinepIMwSpot = ($dimension > 1)? $dimension."D   -!-   PI/MW: SPOT $ex_spot=$ex_pi/$ex_mw\n"
                                                         : $dimension."D   -!-   MW: BAND $ex_spot=$ex_mw\n";
        # only the first occurence (per entry!) of the physical properities of a spot is retained
        my $newLinepIMwSpotQM = quotemeta($newLinepIMwSpot);
        $generatedDat{$ex_ac}->{$map} .= $newLinepIMwSpot unless $generatedDat{$ex_ac}->{$map} =~ /\b$newLinepIMwSpotQM/;

        my $spot_or_band = ($dimension > 1)? 'SPOT' : 'BAND';

        my $msmsOn = 0;
        if (!$inSilicoConverterChecked and ($pmfFile or $msmsFile)) {
          print "\n";
          system("cd $inSilicoDir; echo; perl $inSilicoConverter --showinputformats > /dev/null");
          if (!-e $inSilicoConverter or $?) {
            undef($inSilicoConverter); undef($inSilicoDir);
            warn "Warning: the InSilicoSpectro MS converter cannot be correctly run! No MS conversion will be processed!\n";
            sleep 1;
            $warnBuffer = 1;
          }
          $inSilicoConverterChecked = 1;
        }
        foreach my $msFile ($pmfFile, $msmsFile) {
          last unless $inSilicoConverter;
          unless ($inSilicoConverter) {
            unless ($noInSilicoConverter) {
              warn "Warning: the InSilicoSpectro MS converter cannot be correctly run! No MS conversion will be processed!\n";
              $warnBuffer = 1;
              sleep 1;
            }
            $noInSilicoConverter = 1;
            last;
          }
          if ($msFile=~/\w/) {
            unlink("$tempDir/".$$.".ms.m2d") if -e "$tempDir/".$$.".ms.m2d";
            $msFile =~ s/\s+/ /g;
            my @msFilesList = split ' ', $msFile;
            foreach my $msFileList (@msFilesList) {
              if ($msmsOn and $msFileList =~ s/^(\w+)\://) {
                $msmsFileType = lc($1);
                unless (grep {$_ eq $msmsFileType} @msmsFileType) {
                  unless (defined($unacceptedFileTypes{$msmsFileType})) {
                    warn "warning: $msmsFileType is not a known MS file type, the default type will be used instead.\n"; $warnBuffer = 1;
                    $unacceptedFileTypes{$msmsFileType} = defined;
                  }
                  $msmsFileType = $msmsFileTypeDefault;
                }
              }
              elsif ($msmsOn) {
                my $extension = ($msFileList =~ /\.([^.]+)$/)? lc($1) : '';
                $msmsFileType = ($extension and (grep {$_ eq $extension} @msmsFileType))? $extension : $msmsFileTypeDefault;

              }
              else {
                $pmfEnzyme = ($msFileList =~ s/^(\w+)\://)? $1 : $pmfDefaultEnzyme;
              }
              my $currentMSFileType = ($msmsOn)? $msmsFileType : $pmfFileTypeDefault;

              $msFileList = "$defaultDataDirectory/$msFileList" unless $msFileList =~ /^\//;
              unless (-r $msFileList) {
                warn "warning: cannot read the MS file \'$msFileList\' for spot $ex_spot on the $map map.\n";
                $warnBuffer = 1;
                next;
              }
              my $skipCommand = '--skip='. (($msmsOn)? 'pmf' : 'msms');
              undef($skipCommand); # ! doesn't work properly
              my $command = "cd $inSilicoDir; perl $inSilicoConverter $skipCommand ".
                            "--in=$currentMSFileType:$msFileList --out=m2d:$tempDir/".$$.".ms.m2d";
              system($command);
              if ($? or !-e "$tempDir/".$$.".ms.m2d") {
                warn "warning: cannot convert the MS file \'$msFileList\' for spot $ex_spot on the $map map.\n";
                unlink("$tempDir/".$$.".ms.m2d");  $warnBuffer = 1;
                next;
              }
              elsif (!-s "$tempDir/".$$.".ms.m2d") {
                warn "warning: the MS file \'$msFileList\' for spot $ex_spot on the $map map has generated a void converted file.\n";
                $warnBuffer = 1;
                next;
              }
              my $m2dFile = new IO::File;
              unless ($m2dFile->open("$tempDir/".$$.".ms.m2d")) {
                warn "warning: cannot open a converted m2d file from the MS file \'$msFileList\'.\n"; $warnBuffer = 1;
                next;
              }
              while (my $msLine = <$m2dFile>) {
                chomp($msLine); next unless $msLine;
                if (length($msLine) > $msFileMaxLineLength) {
                  warn "warning: too much data found in the converted m2d file of the MS file \'$msFileList\', some ".
                       "data will be not directly displayed (only the raw file will still contain all data).\n";
                  $warnBuffer = 1;
                  $msLine = ( ($msLine =~ /^(\[\S+\])?/)? $1.' ' : '') . '0.0'
                }
               (my $msFileListTrunc = $msFileList) =~ s/^.+\/([^\/]+)$/$1/;
                if ($msmsOn) {
                  $msms.= ' + ' if $msms;
                  $msms.= $msLine;
                  $msms.= " file:$msFileListTrunc";
                }
                else {
                  $msLine =~ s/^\s*\[\S+\]\s*//;
                  $msLine .= " $pmfEnzyme";
                  $pmf.= ' + ' if $pmf;
                  $pmf.= $msLine;
                  $pmf.= " file:$msFileListTrunc";
                }
              }
              $m2dFile->close;
            }
          }
          $msmsOn = 1;
        }

        $pmf =~ s/\s*\.*\s*$//; $msms =~ s/\s*\.*\s*$//; $peptSeq =~ s/\s*\.*\s*$//; $aa =~ s/\s*\;*\s*$//;
        if ($pmf=~/\w/) {
          foreach my $pmfSub (split ' \+ ', $pmf) {
            $pmfSub =~ s/^\s+|\s+$//;
            my $movePrivateKeywordAfterDocs = ($pmfSub =~ s/\s*$privateKeyword$//i)? 1 : 0;
            my $pmfElementURI = shift @pmfURI or undef;
            my $pmfElementIdentFile = shift @pmfIdentFile or undef;
               $pmfElementIdentFile =~ s/^.+\/([^\/]+)$/$1/; # truncate
            my $pmfElementIdentURI = shift @pmfIdentURI or undef;
            my $pmfElementOtherDocs = (($pmfElementURI)? " uri:$pmfElementURI" : '').
               (($pmfElementIdentFile)? " ident-file:$pmfElementIdentFile" : '').
               (($pmfElementIdentURI)? " ident-uri:$pmfElementIdentURI" : '').
               (($movePrivateKeywordAfterDocs)? $privateKeyword : '');
            $pmfSub =~ s/(.{60}\;)\s*/$1."\n2D"." "x9/eg;
            $generatedDatExp{$ex_ac}->{$map} .= $dimension."D   -!-   PEPTIDE MASSES: $spot_or_band $ex_spot: $pmfSub$pmfElementOtherDocs.\n";
          }
          $mappingMethods .= ',PMF' unless $mappingMethods =~ /\bPMF\b/;
        }
        if ($msms=~/\w/ or $peptSeq=~/\w/) {
          if ($msms=~/\w/) {
            foreach my $msmsSub (split ' \+ ', $msms) {
              $msmsSub =~ s/^\s+|\s+$//;
              my $movePrivateKeywordAfterDocs = ($msmsSub =~ s/\s*$privateKeyword$//i)? 1 : 0;
              my $msmsElementURI = shift @msmsURI or undef;
              my $msmsElementIdentFile = shift @msmsIdentFile or undef;
                 $msmsElementIdentFile =~ s/^.+\/([^\/]+)$/$1/; # truncate
              my $msmsElementIdentURI = shift @msmsIdentURI or undef;
              my $msmsElementOtherDocs = (($msmsElementURI)? " uri:$msmsElementURI" : '').
                 (($msmsElementIdentFile)? " ident-file:$msmsElementIdentFile" : '').
                 (($msmsElementIdentURI)? " ident-uri:$msmsElementIdentURI" : '').
                 (($movePrivateKeywordAfterDocs)? $privateKeyword : '');
              $msmsSub =~ s/(.{60}\;)\s*/$1."\n2D"." "x9/eg;
              $generatedDatExp{$ex_ac}->{$map} .= $dimension."D   -!-   TANDEM MASS SPECTROMETRY: $spot_or_band $ex_spot: $msmsSub$msmsElementOtherDocs.\n";
            }
          }
          if ($peptSeq=~/\w/) {
            $generatedDatExp{$ex_ac}->{$map} .= $dimension."D   -!-   PEPTIDE SEQUENCES: $spot_or_band $ex_spot: $peptSeq.\n";
          }
          $mappingMethods .= ',MS/MS' unless $mappingMethods =~ /\bMS\/MS\b/;
        }
        if ($aa=~/\w/) {
          $generatedDatExp{$ex_ac}->{$map} .= $dimension."D   -!-   AMINO ACID COMPOSITION: $spot_or_band $ex_spot: $aa;\n";
          $mappingMethods .= ',Aa' unless $mappingMethods =~ /\bAa\b/;
        }
        $mappingMethods .= ','.$mapsMappingMethods{$map} if $mapsMappingMethods{$map};

        if ($references) {
          foreach my $reference (split ',', $references) {
            if ($reference =~ /(\d+)/ and $reference{$1}) {
              my $referenceNumber = $1;
             # my $referenceQM = quotemeta($reference{$referenceNumber}); next if $generatedReference{$ex_ac} =~ /$referenceQM/;
              next if $acReferenceCorrespondingRN->{$ex_ac}->{$referenceNumber};
              $RNcounter->{$ex_ac}++;
              $generatedReference{$ex_ac}.= "RN   [".$RNcounter->{$ex_ac}."]\n".$reference{$referenceNumber};
              $acReferenceCorrespondingRN->{$ex_ac}->{$referenceNumber} = $RNcounter->{$ex_ac};
            }
          }
        }
        $mappingMethods =~ s/\s//g;
        while ($mappingMethods =~ s/([^,]+)\[((?:\d+,?)+)\]/$1/) {
          my $currentMappingMethod = $1;
          my $currentMappingMethodsReferences = $2;
          foreach my $currentMappingMethodsReference (split ',', $currentMappingMethodsReferences) {
            next unless $reference{$currentMappingMethodsReference};
            unless ($acReferenceCorrespondingRN->{$ex_ac}->{$currentMappingMethodsReference}) {
              $RNcounter->{$ex_ac}++;
              $generatedReference{$ex_ac}.= "RN   [".$RNcounter->{$ex_ac}."]\n".$reference{$currentMappingMethodsReference};
              $acReferenceCorrespondingRN->{$ex_ac}->{$currentMappingMethodsReference} = $RNcounter->{$ex_ac};
            }
            push @{$acMappingMethodMapCorrespondingRN->{$ex_ac}->{$currentMappingMethod}->{$map}},
              $acReferenceCorrespondingRN->{$ex_ac}->{$currentMappingMethodsReference}
              unless grep {$_ == $acReferenceCorrespondingRN->{$ex_ac}->{$currentMappingMethodsReference}}
                @{$acMappingMethodMapCorrespondingRN->{$ex_ac}->{$currentMappingMethod}->{$map}};
          }
        }
        my @mappingMethods = split ',', $mappingMethods;
        foreach my $mappingMethod (@mappingMethods) { # for case-insensitivity
          $mappingMethod = $replacedMappingMethodKey{$mappingMethod} if $replacedMappingMethodKey{$mappingMethod};
          next if $main::mapping_methods_description{$mappingMethod};
          foreach my $mapping_methods_description_key (keys %main::mapping_methods_description) {
            if (uc($mapping_methods_description_key) eq uc($mappingMethod)) {
              $mappingMethod = $replacedMappingMethodKey{$mappingMethod} = $mapping_methods_description_key;
              last;
            }
          }
        }
        foreach my $mappingMethod (@mappingMethods) {
          $mappingMethod =~ s/\s//g;
          next unless $mappingMethod;
          my $mappingMethodRNs = ($acMappingMethodMapCorrespondingRN->{$ex_ac}->{$mappingMethod}->{$map})?
             " [".(join ",", @{$acMappingMethodMapCorrespondingRN->{$ex_ac}->{$mappingMethod}->{$map}})."]" : '';
          unless ($main::mapping_methods_description{$mappingMethod} or $acceptedUnknownMappingMethod{$mappingMethod}) {
            warn "\nNo definition has been given for the mapping method '$mappingMethod' listed for spot '$ex_spot' ($map) on entry '$ex_ac'!\n".
                 "You might need to give a definition for this mapping technique in your configuration file (basic_include.pl).\n".
                 "Discard this method and continue anyway? (type 'y' to accept)\n";
            chomp(my $input = readline(*STDIN));
            unless ($input =~ /^y(?:es)?$/i) {
              $returnGeneratedFlatFile->{statusErrorMessage} =
                "'$mappingMethod' (not a defined method!) in '$ex_ac' ($map): cannot generate a flat file";
              return ($returnGeneratedFlatFile, undef);
            }
            print "continuing...\n\n";
            $acceptedUnknownMappingMethod{$mappingMethod} = 1;
            next;
          }
          my $newLineSpotMappingMethod = $dimension."D   -!-   MAPPING: $spot_or_band $ex_spot: ".
                                 $main::mapping_methods_description{$mappingMethod}.$mappingMethodRNs.".\n";
          my $newLineSpotMappingMethodQM = quotemeta($newLineSpotMappingMethod);
          $generatedMappingMethods{$ex_ac}->{$map} .= $newLineSpotMappingMethod
            unless $generatedMappingMethods{$ex_ac}->{$map} =~ /$newLineSpotMappingMethodQM/;
        }
        if ($generatedFree2D) {
          $generatedFree2D =~ s/\n$//;
          my @generatedFree2DLines = split "\n", $generatedFree2D;
          foreach my $generatedFree2DLine (@generatedFree2DLines) {
            next unless $generatedFree2DLine  =~ /\w/;
            my $generatedFree2DLineQM = quotemeta($generatedFree2DLine);
            $generatedFree2D{$ex_ac}->{$map} .= $generatedFree2DLine."\n"
              unless $generatedFree2D{$ex_ac}->{$map} =~ /(^|\n)$generatedFree2DLineQM\n/i;
          }
          undef($generatedFree2D);
        }
      }
    }
    $map_handle->close;
  }

  foreach my $ex_ac (sort keys %generatedDat) {
    $generatedDat .= "AC   $ex_ac;\n";
    $generatedDat .= $generatedReference{$ex_ac};
    $generatedDat .= $generatedCommentBlock{$ex_ac};
    foreach my $map (sort keys %mapFiles) {
      next unless $generatedDat{$ex_ac}->{$map};
      $generatedDat .= $generatedDat{$ex_ac}->{$map};
      $generatedDat .= $generatedDatExp{$ex_ac}->{$map}
        if $generatedDat{$ex_ac}->{$map} and $generatedDatExp{$ex_ac}->{$map};
      $generatedDat .= $generatedMappingMethods{$ex_ac}->{$map}
        if $generatedDat{$ex_ac}->{$map} and $generatedMappingMethods{$ex_ac}->{$map};
      $generatedDat .= $generatedFree2D{$ex_ac}->{$map}
        if $generatedDat{$ex_ac}->{$map} and $generatedFree2D{$ex_ac}->{$map};
    }
    $generatedDat .= $generatedXRefs{$ex_ac};
    $generatedDat .= "//\n";
  }
  if (!$generatedDat) {
    $outputFileHandle->close; 
    return undef;
  }

  $generatedDat = $generatedFlatFilePattern."(DO NOT REMOVE THIS LINE)\n\n".$generatedDat;

  unless ($noSpotData) {

    my $generatedSpotData = $spotDataSectionPattern."(SYNTAX: Master Spot X Y pI Mw \%Od \%Vol)"."\n";
    foreach my $map (sort keys %{$returnSpotData}) {
      foreach my $spot (sort keys %{$returnSpotData->{$map}}) {
        $generatedSpotData.= 
          $map."\t".$spot."\t".
          $returnSpotData->{$map}->{$spot}->{xPosition}."\t".
          $returnSpotData->{$map}->{$spot}->{yPosition}."\t".
          $returnSpotData->{$map}->{$spot}->{pI}."\t".
          $returnSpotData->{$map}->{$spot}->{Mw}."\t".
          $returnSpotData->{$map}->{$spot}->{relativeOd}."\t".
          $returnSpotData->{$map}->{$spot}->{relativeVolume}."\n";
      }
    }

    $generatedSpotData.= "__END_SPOT_DATA__"."\n";
    $generatedDat.= "\n".$generatedSpotData."\n";
  }

  $outputFileHandle->print($generatedDat);
  $outputFileHandle->close;

  $returnGeneratedFlatFile->{flatFile} = $generatedDat;

  undef($generatedDat);

  $returnGeneratedFlatFile->{statusOK} = 1;

  sleep 1 if $warnBuffer; # needs time to flush warn buffer
  return ($returnGeneratedFlatFile, $returnSpotData);
}


#=============================================================================================================================#
#=============================================================================================================================#

sub SortNumericThenAlpha {
  my $firstDigits;
  if ($a =~ /^(\d+)/ and $firstDigits = $1 and $b =~ /^(\d+)/ and $firstDigits != $1) {
    $a <=> $b}
  else {
  $a cmp $b}
}

#=============================================================================================================================#
#=============================================================================================================================#

sub _ignore_used_only_once_more_
{

  return;

  () if (
  $Make2D::namespace or 
  $ASCII::primarySwissProtAC or
  $main::tmp_path or
  $main::expasy_getz_syntax_mapper or
  $main::include_not_identified_spots
  )

}


1;



#================================================================================================#
#================================================================================================#
#================================================================================================#
#================================================================================================#




#================================================================================================#
#================================================================================================#
#================================================================================================#
#================================================================================================#


#@(#)2d_util.pl
# version 2.50
# Make2D-DB II tool
# This Sub-Routines collection is used by the WEB server scripts for the relational 2-DE database
#------------------------------------------------------------------------------------------------#

# /*  This script is part of the Make-2D DB II Tool                               */
# /*  Copyright to the SWISS INSTITUTE OF BIOINFORMATICS                          */
# /*  You can freely use this script without moving those copyright lines         */
# /*  Modifications should comply to the instructions stated in the licence terms */


#=============================================================================================================================#

# sub shared_definitions                    share some definitions between the scripts

# sub chomp2                                chomp for both LINUX and DOS systems (\r?\n$/ pattern)

# sub URI_ENCODE:                           URL encode data string -> string

# sub CREATE_PARAM_HASH:                    create a hash containing the CGI parameters

# sub _crypting_string_                     encrypt/decrypt string (internal)

# sub get_mapindex:                         get maps full name from table -> hash of full names

# sub all_maps_full_name                    retrun array with map names (shortName {fullName})
# sub one_map_full_name                     return a map long name (shortName {fullName}) for a given short name map
# sub list_colgroup_maps_by_species         return a species colgroup CGI list of the passed maps

# sub DISPLAY_MAP_TABLE_SELECTION           dispaly a table with map selection form
# sub DISPLAY_MAP_LIST_SELECTION            dispaly a list with map selection form

# sub get_DB_number                         return the given DB internal number, return 1 (default) if DB does not exist

# sub IMPORT_DATABASE_SPECIFIC_PARAMETER    import the specific parameters of the database to query and define $ENV{PGxxx} 

# sub date_human                            convert a PostgreSQL standard ISO DATE(YYYY-MM-DD) into DD-MMM-YYYY

# sub PG_OPEN_CONNECTION                   open a permanent postgres connection $DBI::openedConn with 'AutoCommit' set off

# sub EXECUTE_FAST_COOMAND:                 postgres query -> simple array
# sub EXPLAIN_FAST_COMMAND                  postgres explain query -> simple value

# sub EXECUTE_FORMATTED_COMMAND:            postgres query -> print formated table

# sub SAVE_FILE_FORM                        add a save file form
# sub SAVE_FILE_DATA:                       prepare data to be saved in a text file
# sub SAVE_FILE_TO_DISK:                    save file data to user disk

# sub VIEW_FILE                             view a text file given by it's path

# sub PRINT_ENTRY:                          transform text entry into basic html formatted one -> string

# sub PRINT_NICE_ENTRY:                     transform text entry into nice formatted one -> string

# sub PRINT_MAP_LIST:                       edit and print map_protein_list entries

# sub COMBINED_QUERY:                       define, Construct, Execute and Display tables on queries for the combined different field


# ++ make2db_util.pl content

#=============================================================================================================================#

use strict;

use FindBin;
use lib $FindBin::Bin;

#=============================================================================================================================#
#=============================================================================================================================#

# Chomp for both LINUX and DOS systems (\r?\n$/ pattern)

sub shared_definitions {

  my $main_arg = $_[0];


  # Styles #
  #========#

  if ($main_arg eq "style") {

    # javascript style, need to define style in start_html as 'text/javascript'
    # my $style = " tags.A.textdecoration = \"none\" 
    #               with (tags.P) { fontsize = \"14pt\" marginleft = \"2em\" }";

    # !! IE: OK, NN: not BODY and all font settings !!

    my $basic_font_color = $main::basic_font_color;
       $basic_font_color = 'black' unless  $basic_font_color;
    my $menu_button_color = $main::menu_button_color;
       $menu_button_color = '#f0e68c' unless $menu_button_color; # (khaki: #f0e68c)
    my $menu_selected_button_color = $main::menu_selected_button_color;
       $menu_selected_button_color = '#a52a2a' unless $menu_selected_button_color; # (brown: #a52a2a)
    my $menu_font_color = ($main::menu_reverse_font_color)? $menu_selected_button_color : $basic_font_color;
    my $menu_selected_font_color = ($main::menu_reverse_font_color)? $menu_button_color : $basic_font_color;
    my $aLinkColor = ($SERVER::ExPASy)? '#0000dd' : '#ff2500';
    my $aVisitedColor = ($SERVER::ExPASy)? '#660099' : 'blue';
    my $menuLinkColor = ($SERVER::ExPASy)? $aLinkColor : '#a52a2a';
    my $basicFontFamily = 'sans-serif ariel';

    return "

     .center                                                                                             { text-align:center }
     .right                                                                                               { text-align:right }
     .left                                                                                                 { text-align:left }
     .underlined                                                                                { text-decoration: underline }
     .red,A.red:link,A.red:visited                                                                               { color:red }
     .blue                                                                                                      { color:blue }
     .expasyVisited                                                                                          { color:#660099 }
     .darkBlue                                                                                               { color:#00008b }
     .brown                                                                                                  { color:#a52a2a }
     .gray,.specialList                                                                                         { color:gray }
     .small                                                                                                { font-size:12px; }
     .smallComment                                                                              { font-size:11px; color:gray }
     .smallCommentBrown                                                                      { font-size:11px; color:#a52a2a }
     .bold                                                                                                { font-weight:bold }
     .breakCategory1                                                                                    { margin-bottom:20px }
     .upperCased                                                                                  { text-transform:uppercase }
     .leftBorder                                                 { border-left:20px solid #99FFFF; margin:10px; padding:10px }
     .leftBorder2                                                 { border-left:15px solid #99FFFF; margin:20px; padding:5px }
     .topBorder                                                             { border-top:10px solid #99FFFF; margin-top:10px }
     .topBorderThin                                      { border-top:3px solid #99FFFF; margin-top:15px; margin-bottom:25px; margin-left:50px; margin-right:50px;}
     .Comment                                                                                { font-style:italic; color:gray }
     .Transparent,A.Transparent:link,A.Transparent:visited                        { text-decoration:none; color:$main::bkgrd }
     .H2FontPlus                                                                          { font-size:28px; font-weight:bold }
     .H2Font                                                                              { font-size:24px; font-weight:bold }
     .H3Font                                                                              { font-size:18px; font-weight:bold }
     .H4Font                                                                              { font-size:16px; font-weight:bold }
      BODY,SPAN                { font-family:$basicFontFamily; font-size:13px; text-decoration:none; color:$basic_font_color }
      A,P,Select               { font-family:$basicFontFamily; font-size:13px; text-decoration:none; color:$basic_font_color }
      A:hover                                                                                                               {}
      EM    { font-style:italic; font-family:$basicFontFamily; font-size:13px; text-decoration:none; color:$basic_font_color }
      Strong { font-weight:bold ;font-family:$basicFontFamily; font-size:13px; text-decoration:none; color:$basic_font_color }
      A:link                                                                       { color:$aLinkColor; text-decoration:none }
      A:visited                                                                  { color:$aVisitedColor;text-decoration:none }
      A:link:hover,A:visited:hover                                                               { text-decoration:underline }
      A.MenuLink:link,A.MenuLink:visited      { color:$menuLinkColor; font-size:14px; font-weight:bold; text-decoration:none }
      A.MenuLinkOnTxt:link,A.MenuLinkOnTxt:visited { color:$menuLinkColor; font-size:14px; font-weight:bold; text-decoration:underline }
      A.MenuLink:hover                                                                                           { color:red }
      SPAN.Estomped,A.Estomped,Strong.Estomped                                                  { font-size:12px; color:gray }
      SPAN.MenuTitle, A.MenuTitle,A.MenuTitle:link,A.MenuTitle:visited,A.MenuTitle:hover { font-weight:bold; font-size:18px; color:black; text-decoration:none }
      A.MenuTitleUnderlined                     { text-decoration:underline; font-weight:bold; font-size:18px; color:#00008b }
      A.LightColumn,Strong.LightColumn,EM.LightColumn                                                          { color:white }
      A.Warn,Strong.Warn,I.Warn,EM.Warn                                                         { text-align:left; color:red }
      A.SubmitText,Strong.SubmitText                                                            { font-size:13px; color:blue }
      H1                                                                                                      {color:#00008b }
      H2                                                                           { font-size:24px; color:$basic_font_color }
      H3.Menu                                  { margin-top:0px; margin-bottom:0px; text-align:left; color:$basic_font_color }
      H3                                    { margin-top:8px; margin-bottom:14px; text-align:center; color:$basic_font_color }
      H3.subHeader                                   {font-size: large; font-weight: bold; font-style: italic; color:#00008b }
      H3.MenuLink                                                                             { text-align:center; color:red }
      H3.Result                               { margin-top:8px; margin-bottom:10px; text-align:left; color:$basic_font_color }
      H4                     { font-size:14px; margin-top:6px; margin-bottom:8px; text-align:center; color:$basic_font_color }
      H4.Result                               { margin-top:8px; margin-bottom:10px; text-align:left; color:$basic_font_color }
      SPAN.Result,A.Result                                                                                   { color:#a52a2a }
      TABLE                                                     { font-size:16px; color:$basic_font_color; empty-cells: show }
      TH                                                                                                   { text-align:left }
      TR,DT,LI                                                                                              { font-size:13px }
      TD.NoWrap                                                                                         { white-space:nowrap }
      UL.compact                                                                           { margin:0 0 0 0; padding:0 0 0 0 }
      LI.compact                                                                           { margin:0 0 0 0; padding:0 0 0 0 }
      DIV.borderContainer                                                    { border:2px solid #99FFFF; margin:0; padding:0 }
      Caption.preHR                                                           { border-top:2px solid gray; padding-top: 10px }
      Input.FixedMenu { background-color:$menu_button_color; font-style:italic; font-size:13px; font-family:monospace; color:$menu_font_color; width:188px; height:24px }
      Input.FixedMenuSelected { background-color:$menu_selected_button_color; font-style:italic; font-size:13px; font-family:monospace; color:$menu_selected_font_color; width:188px; height:24px }
      Input.small { color:red; background-color:#87cefa; width:30px; height:20px; padding:1px; margin-top:12px; text-align:center; font-size:11px }
      Input.colored { background-color:$menu_button_color; font-size:12px }
      Input.coloredFixed { background-color:$menu_button_color; width:120px; font-size:12px }
      Input.expandableOn { color:red; background-color:$menu_button_color; width:20px; height:24px; font-size:14px; text-align:center }
      Input.expandableOff { color:$menu_button_color; background-color:red; width:20px; height:24px; font-size:14px; text-align:center }
      Input.niceViewButtonHead { border:0; border-color:#aaffff; color:red; height:18px; background-color:#0000dd; font-size:13px; text-align:center; font-weight:bold }
      Input.niceViewButton { border:0; border-color:#aaffff; color:$aLinkColor; height:18px; background-color:#bbffff; font-size:11px; text-align:center }
      Input.small { border:0; border-color:#eeeeee; color:gray; height:16px; background-color:$menu_button_color; font-size:11px; text-align:center }
      Input.mapSelection  { margin:3px 3px 3px 3px; height:100px }

    "

  }



  # Scripts #
  #=========#

  elsif ($main_arg eq "script:cookies") {
    return " 

    function isNumeric(val) {return(parseFloat(val,10)==(val*1))}


    function SetCookie(name, value, days) {
        // store info on cookie   
        var expire = new Date ();
        expire.setTime (expire.getTime() + (24 * 60 * 60) * days); 
        document.cookie = name + \"=\" + escape(value) + \"; expires=\" + expire.toGMTString(); 
    }

    function GetCookie(name) {
        // retrieve info on cookie 
        var startIndex = document.cookie.indexOf(name);
        if (startIndex != -1) {
            var endIndex = document.cookie.indexOf(\";\", startIndex); 
            if (endIndex == -1) endIndex = document.cookie.length;
            return unescape(document.cookie.substring(startIndex+name.length+1, endIndex)); 
        }
        else {
            return null;
        }
    }

    function DeleteCookie(name) {
       // delete info on cookie
       var expire = new Date ();
       expire.setTime (expire.getTime() - (24 * 60 * 60 * 30)); 
       document.cookie = name + \"=; expires=\" + expire.toGMTString(); 
    }

    "

  }


  elsif ($main_arg eq "script:GetElementValue") {

   return " 

    function GetElementValue(obj) {
        // Get the very last selected element value
        var txt = obj.options[obj.selectedIndex].text;
        return txt;
    }

   "

  }


  elsif ($main_arg eq "script:ClickMenu") {

    my $formName = $_[1];

    return "

    function ClickMenu(fieldValue, toScriptName) {
        // change a hidden field value and submit
        document.$formName.clickedMenu.value=fieldValue;
        //document.$formName.action=toScriptName;
        document.$formName.submit();
        //this.submit();
    }

    "

  }


  elsif ($main_arg eq "script:OpenViewerForSpot") {

    my $map_viewer = $_[1];
    my $remote_database = ($_[2])? $_[2] : undef;

    if ($#main::DATABASES_Included > 0 or $remote_database) {

      return "

    function OpenViewerForSpot$remote_database(SpotNumber, MapName, DatabaseName) {

        window.open(\"$map_viewer"."map=\" + MapName.toLowerCase() + \"\&spot=\" + SpotNumber +
                    \"\&database=\" + DatabaseName.toLowerCase(),'subWind','scrollbars,Height=600,Width=780,dependent')

    }

    function OpenViewerForSpotEntry$remote_database(AC, SpotNumber, MapName, DatabaseName) {

        window.open(\"$map_viewer"."map=\" + MapName.toLowerCase() + \"\&ac=\" + AC + \"\&spot=\" + SpotNumber +
                    \"\&database=\" + DatabaseName.toLowerCase(),'subWind','scrollbars,Height=600,Width=780,dependent')

    }

    "
    }
    else {

      return "

    function OpenViewerForSpot$remote_database(SpotNumber, MapName, DatabaseName) {

        window.open(\"$map_viewer"."map=\" + MapName.toLowerCase() + \"\&spot=\" +
                    SpotNumber,'subWind','scrollbars,Height=600,Width=780,dependent')

    }

    function OpenViewerForSpotEntry$remote_database(AC, SpotNumber, MapName, DatabaseName) {

        window.open(\"$map_viewer"."map=\" + MapName.toLowerCase() + \"\&ac=\" + AC + \"\&spot=\" +
                    SpotNumber,'subWind','scrollbars,Height=600,Width=780,dependent')

    }
    "

    }

  }


  elsif ($main_arg eq "script:OpenGraphViewerForSpot") {

  # newWindow.document.write(\'<applet codebase=\".\" code=\"grapher.graphApplet\" archive=\"graphApplet.jar\" codetype=\"application/octet-stream\" width=\"100%\" height=\"100%\">\');

  return "

    function GraphApplet(masses_string, intensities_string, name) {

      if (!intensities_string.length) {
          intensities_string = masses_string;
        }
      var newWindow = window.open(\'\', \'_blank\', \'menubar=yes,resizable=yes,height=300,width=650\');

      var ba = '/'; // silly var, but needed for HTML validation

      newWindow.document.write(\'<HTML><HEAD><TITLE>Quick spectrum watch<\'+ba+\'TITLE><\'+ba+\'HEAD><BODY BGCOLOR=WHITE>\');

      newWindow.document.write(\'<!--[if !IE]> Firefox and others will use outer object -->\');
      newWindow.document.write(\'<object classid=\"java:grapher.GraphApplet\" archive=\"$main::url_www2d/data/GraphApplet.jar\" type=\"application/x-java-applet\" height=\"100%\" width=\"100%\" >\');
      newWindow.document.write(\'<param name=\"masses\"  value=\"\' + masses_string + \'\">\');
      newWindow.document.write(\'<param name=\"intensities\"  value=\"\' + intensities_string + \'\">\');
      newWindow.document.write(\'<param name=\"title\" value=\"\' + name + \'\">\');
      newWindow.document.write(\'<!-- <![endif]-->\');

      newWindow.document.write(\'<!-- MSIE (Microsoft Internet Explorer) will use inner object -->\');
      newWindow.document.write(\'<object classid=\"clsid:8AD9C840-044E-11D1-B3E9-00805F499D93\" codebase=\"http://java.sun.com/update/1.5.0/jinstall-1_5_0-windows-i586.cab\" height=\"100%\" width=\"100%\" >\');
      newWindow.document.write(\'<param name=\"code\" value=\"grapher.GraphApplet\">\');
      newWindow.document.write(\'<param name=\"archive\" value=\"$main::url_www2d/data/GraphApplet.jar\">\');
      newWindow.document.write(\'<param name=\"type\" value=\"application/x-java-applet\">\');

      newWindow.document.write(\'<param name=\"masses\"  value=\"\' + masses_string + \'\">\');
      newWindow.document.write(\'<param name=\"intensities\"  value=\"\' + intensities_string + \'\">\');
      newWindow.document.write(\'<param name=\"title\" value=\"\' + name + \'\">\');

      newWindow.document.write(\'<strong>This browser does not have a Java Plug-in.<br>\');
      newWindow.document.write(\'<a href=\"http://java.sun.com/products/plugin/downloads/index.html\">\');
      newWindow.document.write(\'Get the latest Java Plug-in here.<\'+ba+\'a><\'+ba+\'strong>\');
      newWindow.document.write(\'<\'+ba+\'object>\');

      newWindow.document.write(\'<!--[if !IE]> close outer object -->\');
      newWindow.document.write(\'<\'+ba+\'object>\');
      newWindow.document.write(\'<!--<![endif]-->\');

      newWindow.document.write(\'<\'+ba+\'BODY><\'+ba+\'HTML>\');
      newWindow.document.close();
      newWindow.focus();

    }

    "

  }

  return 0;

}


#=============================================================================================================================#
#=============================================================================================================================#

# Chomp for both LINUX and DOS systems (\r?\n$/ pattern)

sub chomp2 {

  my $chomped = $_[0];
  $chomped =~ s/\r?\n$//;
  return $chomped;

}


#=============================================================================================================================#
#=============================================================================================================================#


sub URI_ENCODE {

  use URI::Escape;

  # URL Encode and Decode argument depending on mode

  my $argument = $_[0];
  my $mode     = $_[1];

  if ($mode != -1) {

   $argument = uri_escape($argument);

   # $argument =~ s/\t/\%09/mg;
   # $argument =~ s/\n/\%10/mg;
   # $argument =~ s/\r/\%13/mg;

   # $argument =~ s/\s/\%20/mg;
   # $argument =~ s/\#/\%23/mg;
   # $argument =~ s/\%/\%25/mg;
   # $argument =~ s/\+/\%2B/mg;
   # $argument =~ s/\=/\%3D/mg;

   # $argument =~ s/\(/\%28/mg;
   # $argument =~ s/\)/\%29/mg;
   # $argument =~ s/\{/\%7b/mg;
   # $argument =~ s/\}/\%7d/mg;

  }
 
  else {

   $argument = uri_unescape($argument);

   # $argument =~ s/\%09/\t/mg;
   # $argument =~ s/\%10/\n/mg;
   # $argument =~ s/\%13/\r/mg;

   # $argument =~ s/\%20/ /mg;
   # $argument =~ s/\%23/\#/mg;
   # $argument =~ s/\%25/\%/mg;
   # $argument =~ s/\%2B/\+/mg;
   # $argument =~ s/\%3D/\=/mg;

   # $argument =~ s/\%28/\(/mg;
   # $argument =~ s/\%29/\)/mg;
   # $argument =~ s/\%7b/\{/mg;
   # $argument =~ s/\%7d/\}/mg;

  }

  return $argument;

}


#================================================================================================#
#================================================================================================#


sub _crypting_string_ {

  my $string = $_[0];
  my $cut_non_alphanumeric_chars   = $_[1];
  my $cryptingLevel = (defined($_[2]))? $_[2] : 2;
  my $key = (($cryptingLevel>2)? `date +%H-%D` : ($cryptingLevel>1)? `date +%D` : '').(($cryptingLevel>0)? `uname -n -r` : '').'Make2D-DB II';
  my $return_value = $string ^ $key;
  $return_value =~ s/\W+$// if $cut_non_alphanumeric_chars;
  return $return_value;
  
}


#================================================================================================#
#================================================================================================#


sub CREATE_PARAM_HASH {

  # CGI Parameters are passed to a HASH

  my $co_param = new CGI;
  my (%param_hash);

  my @params = $co_param->param;

  foreach my $key (@params) {

    $param_hash{$key} = $co_param->param($key); 

    # !! IF any parameter returns a list, we can read it by:
    # @param_list = split ("\0", $param_hash{$key});
  };

  my $output = join("__#__", $co_param->param('combined_output_fields'));
  $param_hash{'combined_output_fields'} = $output;

  undef($co_param);

  return %param_hash;

  # to pass the hash to a sub:
  # -> &My_SUB(\%my_hash); sub My_SUB { $my_params = $_[0]; foreach $key (keys %{$c}){print "${$c}{$key}";} }

}


#================================================================================================#
#================================================================================================#


sub get_mapindex {

    # when argument 'species' is used, returned maps keys are prefixed with species + "__SPECIES__"

    my (%image_name, $exmaps, $map, $fullname, $with_species);
    $with_species = 1 if $_[0] eq 'species';

    my $command = " SELECT shortName, fullName, organismID FROM Gel ORDER BY 3";
    if ($with_species) {
      $command = " SELECT Gel.shortName, Gel.fullName, Organism.organismSpecies FROM Gel, Organism".
                 " WHERE Gel.organismID = Organism.organismID ORDER BY 3";
    }
    my @names = EXECUTE_FAST_COMMAND($command);

    for (my $ii = 0; $ii < $#names; $ii += 3) {
      $map = $names[$ii];
      $map = $names[$ii+2]."__SPECIES__".$map if $with_species;
      $fullname = $names[$ii+1];
      $fullname =~ s/_/ /g;
      $fullname = $map unless $fullname;
      $image_name{$map} = $fullname;
    }
    return %image_name;


    #-Not used any more-#
    my $cgi_2ddb_inc =  "$main::cgi_2ddb/inc/";
    $cgi_2ddb_inc = "/".$cgi_2ddb_inc if $cgi_2ddb_inc !~ /^\//;
    $exmaps = "$main::server_path"."$cgi_2ddb_inc".$main::set_dbname.".maps";
    %image_name=();
 
    open (MAPS,$exmaps) or return;
    <MAPS>; #shift first line
    while (<MAPS>) {
        ($map,$fullname) = split;
        $fullname = $map unless $fullname;
        $fullname =~ s/_/ /g;
        $image_name{$map} = $fullname;
    }
    close(MAPS);
    %image_name;
   #-------------------#

}



##======================================================================##
##======================================================================##


# Retrun array with map names (shortName {fullName}). Restrict to a specified AC if given
# USE: $map_name =~ s/(?:^[^-]+\-\-)?\s*(\S+)\s*(?:\{[^\}]*\}\s*)$/$1/ to extract the short name

sub all_maps_full_name {

  my $ac = ($_[0]->{ac})? $_[0]->{ac} : '' ;
  my $attach_species = ($_[0]->{species} > 0)? 1 : 0 ;
  $DB::CurrentDatabase = $_[0]->{database} if $_[0]->{database} =~ /^\d+$/;

  my ($command, $designated_ac_command);
  
  $designated_ac_command = " JOIN EntryGelImage ON (Gel.gelID = EntryGelImage.gelID AND".
                           " lower(EntryGelImage.AC) = '".lc($ac)."')" if $ac;

  if ($attach_species) {
    $command = " SELECT Organism.organismSpecies||' -- '||shortName FROM Organism, Gel".$designated_ac_command.
               " WHERE Gel.organismID = Organism.organismID ORDER by Gel.organismID, shortName";
  }
  else {
    $command = " SELECT shortName FROM Gel".$designated_ac_command." ORDER by organismID, shortName";
  }
  
  my @all_maps = (EXECUTE_FAST_COMMAND($command));

  $command = " SELECT fullName FROM Gel".$designated_ac_command." ORDER by organismID, shortName";
  my @all_maps_full_name = (EXECUTE_FAST_COMMAND($command));

  map {$_ =~ s/^\s+$//, $_=~ s/\<\/?.\>//mg, $_ =~ s/_/ /mg } @all_maps_full_name; #/
  for (my $ii=0; $ii < scalar @all_maps; $ii++) {
    $all_maps[$ii] .= " { $all_maps_full_name[$ii] }" if $all_maps_full_name[$ii];
  }

  return @all_maps;

}


# Return a map long name (shortName {fullName}) for a given short name map

sub one_map_full_name {

  my $map = $_[0]; # short name
  my $attach_species = $_[1];
  my ($command, $map_full_name, $species);
  return 0 unless $map;

  if ($attach_species) {
    $command = " SELECT shortname, fullname, Organism.organismSpecies FROM Gel, Organism".
               " WHERE lower(Gel.shortName) = '".lc($map)."' AND Gel.organismID = Organism.organismID LIMIT 1";
  }
  else {
    $command = "SELECT shortname, fullName FROM Gel WHERE lower(shortName) = '".lc($map)."' LIMIT 1";  
  }
 ($map, $map_full_name, $species) = (EXECUTE_FAST_COMMAND($command));
  $map_full_name =~ s/^\s+$//; $map_full_name =~ s/\<\/?.\>//mg; $map_full_name =~ s/_/ /mg; #/
   
  $map = $_[0] unless $map; 
  $map .= " { $map_full_name }" if $map_full_name;
  $map  = $species." -- ".$map if $attach_species and $species;

  return $map;

}

# Return a species colgroup CGI list of the passed maps

sub list_colgroup_maps_by_species {

  my $all_maps = $_[0]; # we need not affect the reference itself as we are sorting it
  $all_maps = [(&all_maps_full_name({species=>1}))] unless $all_maps;
  return unless $all_maps;
  my (%all_maps, $list_species);
  foreach my $value (sort (@{$all_maps})) {
    my $label = $value;
   ($list_species, $value) = ($1, $2) if $label =~ /(.+)\s+--\s+(.+)/;
    $all_maps{$list_species}->{$label} = $value;
  }
  my $map_colgroup_list = "\n";

  my ($currentInterface, $lastInterface, $interfaceGroupeOn, %activatedInterfaceGroupe);
  my $allowNestedColGroups = 1;
  foreach my $list_species (sort keys %all_maps) {

    # my $coMapList = new CGI;
    # $obj->CGI::optgroup is discarded, being a recent CGI.pm method (absent from some older versions)
    #  $map_colgroup_list .=  $coMapList->optgroup(-name=>"$list_species",-values=>{%{$all_maps{$list_species}}});
    ## -onChange=>"DeleteCookie('2d_view_map'), SetCookie('2d_view_map', GetElementValue(document.Form1.change_map), 30), submit(this)"
    # undef($coMapList);
    my $optgroupSpecies = $list_species;
    my $decallage = '';
    if ($allowNestedColGroups and $optgroupSpecies =~ s/^(\[[^\]]+\])\s*//) {
      $currentInterface = $1;
      unless ($activatedInterfaceGroupe{$currentInterface}) {
        $interfaceGroupeOn = 1;
        $map_colgroup_list.= "<optgroup label=\"$currentInterface\">";
        $decallage = '  ';
      }
      $activatedInterfaceGroupe{$currentInterface} = 1;
    }
    $map_colgroup_list.= "<optgroup label=\"$decallage$optgroupSpecies\">";
    foreach my $map (sort keys %{$all_maps{$list_species}}) {
      $map_colgroup_list.= "<option value=\"$map\">".$all_maps{$list_species}->{$map}."</option>\n";
    }
    $map_colgroup_list.= "</optgroup>\n";
    if ($interfaceGroupeOn and $currentInterface ne $lastInterface) {
      $map_colgroup_list.= "</optgroup>\n";
      $interfaceGroupeOn = 0;
    }
    $lastInterface = $currentInterface;
  }
  $map_colgroup_list.= "</optgroup>\n" if $interfaceGroupeOn;

  return $map_colgroup_list;

}


#================================================================================================#
#================================================================================================#


# dispaly a table with map selection form

sub DISPLAY_MAP_TABLE_SELECTION {

  return undef if $SERVER::onlyInterfaces;

  my $co_map_table = new CGI;
  my $mapTable;

  my $parameters = $_[0];
  my $submitName = $parameters->{submitName};
  my @maps = ($parameters->{maps})? @{$parameters->{maps}} : ();
  my $onlyReal = $parameters->{onlyReal};
  my $mapCellColor = ($parameters->{cellColor})? $parameters->{cellColor} : '#f0e68c';
  my $doNotPrint = ($parameters->{doNotPrint})? 1 : 0;

  my $database = ($parameters->{database})? $parameters->{database} : $DB::CurrentDatabase;
  $database = &get_DB_number($database) unless $database =~ /^\d+$/;

  $DB::CurrentDatabase = $database;
  @maps = &all_maps_full_name({species=>1}) unless @maps;

  unless ($DISPLAY_MAP_TABLE_SELECTION::tissuesDone) {
    $DISPLAY_MAP_TABLE_SELECTION::tissuesDone = 1;
    my $command = "SELECT Gel.shortName, TissueSP.tissueSPDisplayedName FROM Gel, GelTissueSP, TissueSP ".
                  "WHERE Gel.gelID = GelTissueSP.gelID AND TissueSP.tissueSPName = GelTissueSP.tissueSPName";
    my @tissues = EXECUTE_FAST_COMMAND($command);
    for (my $ii = 0; $ii < scalar (@tissues); $ii+=2) {
      my ($tissuesGelShortName, $tissuesSPDisplayedName) = ($tissues[$ii] ,$tissues[$ii+1]);
      $DISPLAY_MAP_TABLE_SELECTION::tissues{$tissuesGelShortName} = $tissuesSPDisplayedName;
    }
  }

  my ($map_displayed_name, $map_short_name, $displayed_species, $last_displayed_species);
  my $cell = 0;
  my $cell_per_line = 4;
  my $sgifs = ($main::database[$database]{small_image_url})?
    $main::database[$database]{small_image_url} : $main::default_small_image_url;
  my $small_image_type = ($main::database[$database]{small_image_type})?
    $main::database[$database]{small_image_type} : $main::default_small_image_type;
  my $database_name = $main::database[$database]{database_name};
  my $pre_hr_class = ($SERVER::interface)? "preHR" : "";

  $mapTable.= "<input type='hidden' ID='$submitName' name='$submitName' value=''>\n";
  $mapTable.= "\n".$co_map_table->start_table().$co_map_table->caption({class=>"$pre_hr_class"},$co_map_table->span({class=>"MenuTitle"},$database_name))."\n";
  $mapTable.= $co_map_table->start_Tr()."\n";
 #print $co_map_table->th({-rowspan=>10})."\n";

  my $cellBorder = 2;
  my $mapListClass = 'mapList';
  my $cluster = (scalar (@maps) > 8)? 1 : 0;
  if ($cluster) {
    $mapCellColor = $main::bkgrd;
    $cellBorder = 0;
    $mapListClass = "";
  }

  foreach my $map (sort @maps) {
    $cell++;
    $map_short_name = $map_displayed_name = $map;
    if ($map_short_name =~ s/(^.+\s+\-\-\s+)?\s*(\S+)\s*(\{[^\}]*\}\s*)$/$2/) {
      $last_displayed_species = $displayed_species;
      my $map_shortname = $co_map_table->strong($2).$co_map_table->br;
      my $map_fullname = ($3)? $3.$co_map_table->br : '';
      my $tissue = ($DISPLAY_MAP_TABLE_SELECTION::tissues{$2})? 'Tissue: '.$DISPLAY_MAP_TABLE_SELECTION::tissues{$2} : '';
     ($displayed_species = $1) =~ s/\s+\-+\s+$//;
      $displayed_species = $co_map_table->em($displayed_species).$co_map_table->br if $displayed_species;
      $map_displayed_name = $map_shortname.$map_fullname.((!$cluster)? $displayed_species : '').$tissue;
      if ($cluster and $displayed_species and $last_displayed_species ne $displayed_species) {
        $mapTable.= $co_map_table->end_Tr."\n".
#            $co_map_table->start_Tr().
#            $co_map_table->td($co_map_table->h3($co_map_table->em({class=>'gray'}, $co_map_table->hr."$displayed_species"))).
#            $co_map_table->end_Tr."\n".
            $co_map_table->start_Tr()."\n";
        $cell = 1;
      }
    }
    if ($onlyReal and $map_short_name =~ /^-/) {
      $cell--;
      next;
    }
    my $chosen_map = ($parameters->{database})? "*".$DB::CurrentDatabase." -- ".$map : $map;
    $mapTable.= $co_map_table->end_Tr().$co_map_table->start_Tr() if $cell % $cell_per_line == 1 && $cell > 2;
    $mapTable.= $co_map_table->start_td({-align=>'center'});
    $mapTable.= $co_map_table->start_table({-class=>"$mapListClass", -border=>$cellBorder}).
      $co_map_table->Tr().$co_map_table->start_td({-bgcolor=>"$mapCellColor", -align=>'center'});
    $mapTable.= $map_displayed_name.$co_map_table->br."\n";
   (my $map_short_name_no_space = $map_short_name) =~ s/\s.+//;

    $mapTable.= $co_map_table->input({ class=>"mapSelection",
                  -type=>"image", -name=>"$submitName",
                  -src=>"$sgifs/$map_short_name_no_space.$small_image_type",
                  -alt=>"Click below for: $map",
                  -onMouseOver=>"window.status=\"$map\";return true",
                  -onClick=>"document.getElementById('$submitName').value=\"$chosen_map\""
                }).$co_map_table->br."\n";

    $mapTable.= $co_map_table->end_td.$co_map_table->end_Tr.$co_map_table->end_table;
    $mapTable.= $co_map_table->end_td;
  }
  while ($cell % $cell_per_line > 0 and $cell > $cell_per_line) {
    $mapTable.= $co_map_table->td('&nbsp;');
    $cell++;
  }
  $mapTable.= $co_map_table->end_Tr."\n";
  $mapTable.= $co_map_table->end_table."\n".$co_map_table->p.$co_map_table->br;

  undef($co_map_table);

  if ($doNotPrint) {
    return $mapTable;
  }
  else {
    print $mapTable;
    return 1;
  }

  1;
}


# dispaly a list with map selection form

sub DISPLAY_MAP_LIST_SELECTION {

  my $parameters = $_[0];
  my @severalDatabases = ($parameters->{databases})? @{$parameters->{databases}} : ($DB::CurrentDatabase);
  my $noAll = ($parameters->{noAll})? 1 : 0;
  my @remoteInterfaces = ($parameters->{remoteInterfaces})? @{$parameters->{remoteInterfaces}} : ();
  my $clickedMenu = $parameters->{clickedMenu};

  my $co_map_list = new CGI;
  my @all_maps;

  if ((scalar @severalDatabases > 1 or @remoteInterfaces) and !$SERVER::onlyInterfaces) {
    my $localInterface = ($co_map_list->param('remote maps on') and @remoteInterfaces)? "[ Local ] ": '';
    foreach my $current_database (@severalDatabases) {
      $DB::CurrentDatabase = ($current_database =~ /^\d+$/)? $current_database : &get_DB_number($current_database);
      $current_database = ($current_database =~ /^\d+$/)?
        $main::database[$current_database]{database_name} : $current_database;
      my @maps = ($noAll)? &all_maps_full_name({species=>1}) : ('All Maps', &all_maps_full_name({species=>1}));
      map {$_ = "$localInterface*$current_database -- $_"} @maps;
      push @all_maps, @maps;
    }
  }
  else {
    @all_maps = (&all_maps_full_name({species=>1}));
  }

  if (@remoteInterfaces) {

   # get arguments: ?maplist&extract&format=raw&database=all
   # my @Several_Remote_Interfaces = ($co->param('selected_remoteinterfaces'));
   # @Several_Remote_Interfaces = keys %{$SERVER::remoteInterface} if grep {/^$allInterfacesText$/} @Several_Remote_Interfaces;

    if ($co_map_list->param('last remote maps on') and !$co_map_list->param('refresh maps')) {
      my $lastRemoteMapsParameter = $co_map_list->param(-name=>'last remote maps on');
      if ($noAll) {
        foreach my $lastMapsLine ((split '&&', $lastRemoteMapsParameter)) {
          next if $lastMapsLine =~ /\-\-\s+All Maps$/i;
          push @all_maps, $lastMapsLine;
        }
      } else {
        push @all_maps, (split '&&', $lastRemoteMapsParameter);
      }
    }

    #elsif ( ($co_map_list->param('remote maps on') and $co_map_list->param('refresh maps'))
    #     or ($SERVER::onlyInterfaces and $noAll) ) {
    elsif (@remoteInterfaces) {
      my $lastRemoteMapsParameter = '';
      foreach my $remoteInterface (@remoteInterfaces) {
        my $remoteURL = ${$SERVER::remoteInterface}{$remoteInterface}->{URL};
        next unless $remoteURL;
        my @remoteDatabasesBasicInclude = @{${$SERVER::remoteInterface}{$remoteInterface}->{database}};
        my @remoteDatabases = ();
        my $allInterfaceDatabases = 0;
        if (grep {$_ =~ /^All$/i} @remoteDatabasesBasicInclude) {
          $allInterfaceDatabases = 1;
          if ($SERVER::extractAllRemoteDatabaseNames) {
            @remoteDatabases = _fileTmp_remoteDatabaseList_({'remoteInterface'=>"$remoteInterface", 'remoteURL'=>"$remoteURL"});
          }
        }
        @remoteDatabases = @remoteDatabasesBasicInclude unless @remoteDatabases;
        my $onlyOneRemoteDBwithALL = (scalar @remoteDatabases == 1 and $remoteDatabases[0] =~ /^All$/i)? 1 : 0;
        foreach my $remoteDatabase (@remoteDatabases) {
          (my $mapsList, my $lastRemoteMapsParameterDB) =
          _fileTmp_RemoteMapList_({'remoteInterface'=>"$remoteInterface", 'remoteURL'=>"$remoteURL",
            'remoteDatabase'=>"$remoteDatabase", 'noAll'=>"$noAll", 'onlyOneRemoteDBwithALL'=>"$onlyOneRemoteDBwithALL"});
          push @all_maps, (split "\n", $mapsList);
          $lastRemoteMapsParameter .= $lastRemoteMapsParameterDB;
        }
      }
      if ($lastRemoteMapsParameter =~ s/\&\&$//) {
        $co_map_list->delete('last remote maps on');
        $co_map_list->param(-name=>'last remote maps on', -values=>[$lastRemoteMapsParameter]);
        print $co_map_list->hidden('last remote maps on');
      }
    }
  }

  my $map_colgroup_list = &list_colgroup_maps_by_species(\@all_maps);
  print span({-style=>$font_style}, '&nbsp;&nbsp;Limit to Map:&nbsp;'),"\n";
  print "<select name=\"choose_map\" size=\"1\" >".
        (($noAll)? '' : "<option selected=\"selected\" value=\"All Maps\">All Maps</option>").
        $map_colgroup_list."</select><p/>\n";
  # perl CGI bug? the colgroup values are lowercased and '_' transformed into '-'?!, so code the list in hard!
  # print $co_map_list->scrolling_list ( -name=>'choose_map',-values=>['All Maps'],-size=>1,-default=>"All Maps",
  #                             -attributes=>{'All Maps'=>{'class'=>'specialList'}}, $map_colgroup_list),p,"\n";
  my $mapPatternMsg = (1 or $noAll)? $co_map_list->span({-style=>$font_style}, 'or type a map ').$co_map_list->span({style=>$font_style."font-weight: bold;"}, 'exact').$co_map_list->span({-style=>$font_style}, ' name') : 'or only maps containing';
  print $co_map_list->span({style=>$font_style}, '&nbsp;&nbsp;--&nbsp;').$mapPatternMsg.':&nbsp', $co_map_list->textfield({ -value=> '', -name=>'map_pattern', -size=>16}).
    $co_map_list->p."\n";
  if ($clickedMenu and $SERVER::remoteInterfaceDefined) {
=pod
    my $remoteMapsChecked = ($SERVER::onlyInterfaces)? 1 : 0;
    print $co_map_list->span({style=>$font_style}, '&nbsp;&nbsp;--&nbsp;').$co_map_list->checkbox (-name=>'remote maps on', -label=>"", -checked=>$remoteMapsChecked);
        '&nbsp;display&nbsp;maps\'&nbsp;names&nbsp;&nbsp;from&nbsp;currently&nbsp;'.$co_map_list->strong('selected').
          '&nbsp;remote&nbsp;interfaces&nbsp;'."\n";

    print $co_map_list->submit({-name=>"refresh maps", -value=>"refresh maps",
                       -onClick=>"javascript:ClickMenu('$clickedMenu','$main::script_name')",
                       -onMouseOver=>"window.status='Include maps dynamically from currently selected remote interfaces';return true"}).$co_map_list->br.
    '&nbsp;&nbsp;'.$co_map_list->em({class=>'Comment'}, '* time consuming / deactivated for remote interfaces on remote portals (e.g. \'World-2DPAGE\')').
          $co_map_list->p."\n"; # or -class=>"refreshMaps"
=cut
  } 
  
  print $co_map_list->hr.$co_map_list->p."\n";
  
  undef($co_map_list); 
  return 1;
  
}

sub _fileTmp_remoteDatabaseList_ {

  my $parameters = $_[0];
 (my $remoteInterface = $parameters->{remoteInterface}) =~ s/\s/_/g;
  my $remoteURL = $parameters->{remoteURL};
  my $limitNumberDBs = 25;
  return () unless $remoteInterface;
  my $file = "$main::tmp_path/remoteInterface.$remoteInterface.databases.txt";
  my @databaseList = ();
  if (-e $file and -M $file < 3) {
    open FILE, "<$file";
    while (my $line = <FILE>) {
      chomp($line);
      push @databaseList, $1 if $line =~ /(.+\w+.*)/ and scalar (@databaseList) < $limitNumberDBs;
    }
    close FILE;
  }
  elsif ($remoteURL) {
    my $getDBs = LWP_AGENT('GET', $remoteURL.'?databaselist&extract&format=raw');
    foreach my $getDBsLine (split "\n", $getDBs) {
      # we don't want to LWP some scam
      if ($getDBsLine =~ /\<|\>/) {
        undef(@databaseList);
        last;
      }
      next unless $getDBsLine =~ /\w/;
      push @databaseList, $getDBsLine;
    }
    undef(@databaseList) if scalar @databaseList > $limitNumberDBs;
    open FILE, ">$file";
    print FILE join "\n", @databaseList; # even if void, to note that it has been recently contacted
    close FILE;
  }

  return @databaseList;

}

sub _fileTmp_RemoteMapList_ {

  my $parameters = $_[0];
  my $remoteInterface = $parameters->{remoteInterface};
 (my $remoteInterfaceNoSpace = $remoteInterface) =~ s/\s/_/g;
  my $remoteDatabase = $parameters->{remoteDatabase};
 (my $remoteDatabaseNoSpace = $remoteDatabase) =~ s/\s/_/g;
  my $remoteURL = $parameters->{remoteURL};
  my $noAll = $parameters->{noAll};
  my $onlyOneRemoteDBwithALL = $parameters->{onlyOneRemoteDBwithALL};
  my $limitNumberMaps = 100;
  return () unless $remoteInterface and $remoteDatabase;
  my $file = "$main::tmp_path/remoteInterface.$remoteInterfaceNoSpace.$remoteDatabaseNoSpace.maps.txt";
  my @mapsList = ();
  if (-e "$file.bak" or (-e $file and -M $file < 3)) {
    $file = $file.".bak" if -e "$file.bak";
    open FILE, "<$file";
    while (my $line = <FILE>) {
      chomp($line);
      push @mapsList, $1 if $line =~ /(.+\w+.*)/ and scalar (@mapsList) < $limitNumberMaps;
    }
    close FILE;
  }
  elsif ($remoteURL) {
    my $getMaps = LWP_AGENT('GET', $remoteURL.'?maplist&extract&format=raw&database='.$remoteDatabaseNoSpace);
    my @getMaps = split "\n", $getMaps;
    undef(@getMaps) if $getMaps[0] =~ /\<\!DOCTYPE html/ or $getMaps[1] =~ /\<\!DOCTYPE html/ or scalar (@getMaps) > $limitNumberMaps;
    @getMaps = ("All Maps", @getMaps);
    foreach my $getMapsLine (@getMaps) {
      if ($getMapsLine =~ /(?:^|\n)([^\t]+)\t([^\t]*)\t([^\t]*)\t(\d*)\t([^\t]*)\t([^\t]*)\t?(.*)/ or $getMapsLine =~ /^(All Maps)$/i) {
        my ($remoteMapShortName, $remoteMapFullName, $remoteMapID, $remoteMapTaxID, $remoteMapSpecies, $remoteMapTissue,$remoteMapOther)
          = ($1, $2, $3, $4, $5, $6, $7);
        if ($remoteMapOther) { # since version 2.50.1
          my ($imageUrl,$imageUrlSmall) = ($1,$2) if $remoteMapOther =~ /^(\S+)\t(\S+)/;
        }
        $remoteMapFullName =~ s/\<\/?.\>//mg; $remoteMapFullName =~ s/_/ /mg;
        my $remoteMap = (($remoteMapSpecies)? $remoteMapSpecies.(($remoteMapTissue)? " / $remoteMapTissue": '').' -- ' : '').
          $remoteMapShortName.(($remoteMapFullName)? " { $remoteMapFullName } ": '');
        my $displayedRemoteDatabase = ($onlyOneRemoteDBwithALL)? $remoteInterface : $remoteDatabase;
        $remoteMap = "[$remoteInterface] *$displayedRemoteDatabase -- $remoteMap";
        push @mapsList, $remoteMap;
      }
    }
    open FILE, ">$file";
    print FILE join "\n", @mapsList; # even if void, we write 'All Maps' to note that it has been recently contacted
    close FILE;
  }

  my $mapsList = my $lastRemoteMapsParameterDB = '';
  foreach my $mapsLine (@mapsList) {
    $lastRemoteMapsParameterDB .= $mapsLine.'&&';
    next if $noAll and $mapsLine =~ /\-\-\s+All Maps$/i;
    $mapsList.= $mapsLine."\n";
  }

  return ($mapsList, $lastRemoteMapsParameterDB);

}

#================================================================================================#
#================================================================================================#



sub get_DB_number {

  my $DB_Argument = $_[0];
  my $DB_found = 0;
  return $main::DATABASES_Included[0] unless $DB_Argument or $SERVER::core_selector;

  foreach my $key ('database_name', 'dbname') {
    for (my $ii = 1; $ii <99; $ii++) {
      my $db_name = $main::database[$ii]{$key};
     (my $db_name_no_spaces = $db_name) =~ s/\s/_/g;
      if ($db_name and (uc($db_name) eq uc($DB_Argument) or uc($db_name_no_spaces) eq uc($DB_Argument))) {
        $DB_Argument = $ii;
        $DB_found = 1;
        last;
      }
    }
    last if $DB_found;
  }

  $DB_Argument = $main::DATABASES_Included[0] unless $DB_Argument =~ /^(\d+)$/; # use 1st DB if DB name is not found
  return $DB_Argument;

}



#================================================================================================#
#================================================================================================#



sub IMPORT_DATABASE_SPECIFIC_PARAMETER {

  # Import database specific parameters and define $ENV{PGxxx} #

  my @database = @main::database;
  my $DB_number = $DB::CurrentDatabase;

  $DB::set_host     = ($database[$DB_number]{host})?     $database[$DB_number]{host}:      $main::default_host;
  $DB::set_port     = ($database[$DB_number]{port})?     $database[$DB_number]{port} :     $main::default_port;
  $DB::set_options  = ($database[$DB_number]{options})?  $database[$DB_number]{options} :  $main::default_options;
  $DB::set_tty      = ($database[$DB_number]{tty})?      $database[$DB_number]{tty} :      $main::default_tty;
  $DB::set_dbname   = ($database[$DB_number]{dbname})?   $database[$DB_number]{dbname} :   $main::default_dbname;
 
  if ($SERVER::core_selector) {
    $DB::set_select_user = $SERVER::set_select_user;
    $DB::set_select_user_password = $SERVER::set_select_user_password;
    $DB::set_dbname = $SERVER::set_dbname if $SERVER::set_dbname;
  }

  else {
    $DB::set_select_user  = ($database[$DB_number]{select_user})? $database[$DB_number]{select_user} : $main::default_select_user;
    $DB::set_select_user_password = ($database[$DB_number]{select_user_password})?
     $database[$DB_number]{select_user_password} : $main::default_select_user_password;
    $DB::set_select_user_password = $DB::set_select_user unless $DB::set_select_user_password;
  }

  # DBI and psql program are case-sensitive, while pgsql DBs are always lower-cased
  $DB::set_dbname = lc($DB::set_dbname);

  $ENV{PGHOST}     = $DB::set_host;
  $ENV{PGDATABASE} = $DB::set_dbname;
  $ENV{PGUSER}     = $DB::set_select_user;
  $ENV{PGPASSWORD} = $DB::set_select_user_password;
  $ENV{PGPORT}     = $DB::set_port;
  $ENV{PGOPTION}   = $DB::set_options;
  $ENV{PGTTY}      = $DB::set_tty;

  $IMPORT_DATABASE_SPECIFIC_PARAMETER::activated = 1;

  1;
}



#================================================================================================#
#================================================================================================#


sub date_human {

  # convert a PostgreSQL standard ISO DATE(YYYY-MM-DD) into DD-MMM-YYYY

  my $date = $_[0];
  return $date unless $date =~ /^(\d{4})\-0?(\d{1,2})\-(\d{1,2})$/;
  my ($year, $month, $day) = ($1, $2, $3);

  my %month = (  1 => "JAN",  2 => "FEB",  3 => "MAR", 
                 4 => "APR",  5 => "MAY",  6 => "JUN", 
                 7 => "JUL",  8 => "AUG",  9 => "SEP", 
                10 => "OCT", 11 => "NOV", 12 => "DEC");
  return $date unless $month{$month};

  return "$day-$month{$month}-$year";


}



#================================================================================================#
#================================================================================================#

sub PG_OPEN_CONNECTION {

 # open a permanent postgres connection $DBI::openedConn with 'AutoCommit' set off

  use DBI qw(:sql_types);
 #use DBD::Pg qw(:pg_types);

  my $parameters = $_[0];
  
  my $action = ($parameters->{action} eq 'close')? 'close' : 'open';
  
  &IMPORT_DATABASE_SPECIFIC_PARAMETER
    unless $IMPORT_DATABASE_SPECIFIC_PARAMETER::activated;

  my $set_host                 = $DB::set_host;
  my $set_port                 = $DB::set_port;
  my $set_options              = $DB::set_options;
  my $set_tty                  = $DB::set_tty;
  my $set_dbname               = $DB::set_dbname;

  my $set_select_user          = $DB::set_select_user;
  my $set_select_user_password = $DB::set_select_user_password;
  

  my $dbi_source   = "dbi:Pg:dbname=$set_dbname";

  $SIG {PIPE} = sub { print "broken pipe \n" };
 
  if ($action eq 'open' and (!$DBI::openedConn or $DBI::openedConnLastCachedDatabase ne $DB::CurrentDatabase)) {
    $DBI::openedConnLastCachedDatabase = $DB::CurrentDatabase;
    $DBI::openedConn = DBI->connect_cached($dbi_source, $set_select_user, "$set_select_user_password");
    if ($DBI::err) {
      print "<br>ERR: Could not open a permanent connection to $set_dbname:<br>".$DBI::errstr."<br>";
      undef($DBI::openedConn);
      return 0;
    }
    $DBI::openedConn->do("SET search_path to $SERVER::search_path");
    $DBI::openedConn->{AutoCommit} = 0;  # enable transactions, if possible
    $DBI::openedConn->{RaiseError} = 1;
    return 1;
  }
  elsif ($action eq 'close' and $DBI::openedConn) {
    $DBI::openedConn->disconnect;
    undef($DBI::openedConn);
    return 1;
  }

  return 0;

}


#================================================================================================#
#================================================================================================#


sub EXECUTE_FAST_COMMAND {

  # Request a list of values #

  my $command = $_[0];
  my $fetchall_arrayref = ($_[1]->{fetch_method} and $_[1]->{fetch_method} eq 'fetchall_arrayref')? 1 : 0;
  my $doOnly = ($command =~ s/^DO\:\s+//i)? 1: 0;

  if ($SERVER::onlyInterfaces) { return ($fetchall_arrayref)? '' : ()};

  use DBI qw(:sql_types);
 #use DBD::Pg qw(:pg_types);

  if (length($command) > 1024 and $command !~ /^\s*EXPLAIN /i) {
    my $max_postgreSQL_query_cost = ($main::max_postgreSQL_query_cost > 1000)?  $main::max_postgreSQL_query_cost : 150000;
    my $cost = EXPLAIN_FAST_COMMAND("$command");
    if ($cost > $max_postgreSQL_query_cost) {
      print "<br><br><center><font color='red'><strong>Process Halted!<br>Querry highly time-consuming (cost = $cost). Please, make your query shorter!".
            "</strong></font></center><br><br>" unless $EXECUTE_FAST_COMMAND::query_too_long;
      $EXECUTE_FAST_COMMAND::query_too_long = 1;
      return ();
    }
  }

  # Importing parameters for connection, parameters are set in the include configuration file #

  &IMPORT_DATABASE_SPECIFIC_PARAMETER
    unless $IMPORT_DATABASE_SPECIFIC_PARAMETER::activated;

  my $set_host                 = $DB::set_host;
  my $set_port                 = $DB::set_port;
  my $set_options              = $DB::set_options;
  my $set_tty                  = $DB::set_tty;
  my $set_dbname               = $DB::set_dbname;

  my $set_select_user          = $DB::set_select_user;
  my $set_select_user_password = $DB::set_select_user_password;


  my $dbi_source   = "dbi:Pg:dbname=$set_dbname";

  $SIG {PIPE} = sub { print "broken pipe \n" }; 

  # Connect (cached / through TCP/IP)
  unless ($DBI::conn and $DBI::connLastCachedDatabase eq $DB::CurrentDatabase) {
    $DBI::connLastCachedDatabase = $DB::CurrentDatabase;
    $DBI::conn = DBI->connect_cached($dbi_source, $set_select_user, "$set_select_user_password");

    if ($DBI::err) {
      undef($DBI::conn);
      if ($SERVER::core_selector) {
        if ($SERVER::viewer) {
          my $co_redirect = new CGI;
          print $co_redirect->redirect("$main::main_script");
          exit 0;
        }
        print "\n<br><br><strong class='red'>Error: Cannot connect to the core schema! Please, try a valid login or verify the connection parameters.\n</strong></body></html>";
        exit 0;
      }
      print "<br><strong>ERR: Could not establish connection to the '$set_dbname' database server!</strong><br><br>(message: ".$DBI::errstr.")<br>";
      if ($main::email) {
        $main::email =~ s/\\\@/@/;
        print "<br>You may report this incident to: <a href=\"mailto:$main::email\">$main::email</a><br>"
      }
      print "\n</FORM></BODY></HTML>";
      exit 0;
    }
  }

  $DBI::conn->do("SET search_path to $SERVER::search_path");

  if ($doOnly) {
    $DBI::conn->do("$command");
    return ();
  }

  my $result = $DBI::conn->prepare_cached($command);

  my $result_return = $result->execute;

  if ( $result->err ) {
    my $error = $result->errstr;
    print "<br><span class='gray' >Bad Query: $error<br>$command</span><br>";
    $result->finish if $result;
    return ();
  }

  if ($command !~ /\bselect\b/i) { # return number of rows affected on non-select operations
    $result->finish if $result;
    return ($result_return)
  }

  # some bug fetching arrays with some recent DBI versions (e.g. 1.40), 1.48 OK!
  my @list = ();
  my @last_row = ();
  my $list_fetchall_arrayref = [];

  if ($fetchall_arrayref) {
    $list_fetchall_arrayref = $result->fetchall_arrayref;
  } else {
    while ( @last_row = $result->fetchrow_array) {
      push (@list, @last_row);
    }
  }

  $result->finish if $result;

 # if ($DBI::conn) {$DBI::conn->disconnect; undef($DBI::conn)};

  if ($fetchall_arrayref) {
    return $list_fetchall_arrayref;
  } else {
    return @list;
  }

}


sub EXPLAIN_FAST_COMMAND {

  # For postgreSQL 7.3 or higher - returns an estimated cost value, otherwise -1

  my $command = $_[0];
  my ($psql_version, $psql_subversion);

  (my $postgresql_version) = EXECUTE_FAST_COMMAND("SELECT version()"); # PostgreSQL < 7.3 do not send back rows on EXPLAIN commands

  if ($postgresql_version =~ /^\w+\s+(\d+)\.(\d+)/ ) {
    $psql_version = $1;
    $psql_subversion = $2;
  }

  return -1 if $psql_version < 7 or ( $psql_version == 7 and  $psql_subversion < 3);

  $command = "EXPLAIN ".$command unless $command =~ /^explain /i; 
  my @explain = EXECUTE_FAST_COMMAND($command);
  my $explain = join "\t", @explain;
  my $cost;
  while ($explain =~ s/cost\=\d+\.\d+\.+(\d+\.?\d*)//) { $cost += $1};
  return $cost;

}


#================================================================================================#
#================================================================================================#


sub EXECUTE_FORMATTED_COMMAND {

  # Request -> FORMATTED TABLES

  use DBI qw(:sql_types);
 #use DBD::Pg qw(:pg_types);

  my $co = new CGI;

  my $parameters = $_[0];

  my $command = ($parameters->{command})? $parameters->{command} : 'select version()';
  my $hidden_reference = $parameters->{hidden_reference};
  my $table_width = $parameters->{table_width};
  my $caption = $parameters->{caption};
  my $no_table_caption = $parameters->{no_table_caption};
  my $no_color_alternation = $parameters->{no_color_alternation};
  my $table_alignment_general = $parameters->{table_alignment_general};
  my $general_table_border = (defined($parameters->{general_table_border}))?
       $parameters->{general_table_border} : $main::table_border;
  my $table_alignment_text_class = $parameters->{table_alignment_text_class};
  my $cell_valign = ($parameters->{cell_valign})? $parameters->{cell_valign} : 'middle';
  my $void_cell = ($parameters->{void_cell})? $parameters->{void_cell} : '';
  my $group_rows = $parameters->{group_rows};
  my $group_rows_extra = $parameters->{group_rows_extra}; undef $group_rows_extra if grep {$_ !~/^[2-9]\d*$/} @{$group_rows_extra};
  my $only_rows = $parameters->{only_rows};
  my $do_not_print = $parameters->{do_not_print};
  my $hide_no_result_message = $parameters->{hide_no_result_message};
  my $show_number_rows_selected = $parameters->{show_number_rows_selected};
  my $show_number_rows_affected = $parameters->{show_number_rows_affected};
  my $notOK_note = ($parameters->{notOK_note})? $parameters->{notOK_note} : 'no result';
  my ($separate_by_blocks, $separate_by_blocks_title_position) =
     ($parameters->{separate_by_blocks} =~ /^(\d+)\:?(\d+)?$/)? (($1 -1), ($2 -1)) : undef;

  $table_alignment_text_class = 'center' unless ($table_alignment_text_class =~ /^center|left|right$/i);

  # replace this by an explain querry for postgreSQL >= 7.3 # KHM
  if (length($command) > 2048) {
    print "<br><br><center><font color='red'><strong>Process Halted!<br>Querry too long. Please, make your query shorter!".
          "</strong></font></center><br><br>" unless $EXECUTE_FAST_COMMAND::query_too_long;
    $EXECUTE_FAST_COMMAND::query_too_long = 1;
    return 0;
  }

  $table_width = "90%" unless $table_width;
  $table_alignment_general = 'center' unless $table_alignment_general;
  undef($table_alignment_general) unless $table_alignment_general eq 'center'; # (KHM)

  # Importing parameters for connection, parameters are set in the include configuration file #

  &IMPORT_DATABASE_SPECIFIC_PARAMETER
    unless $IMPORT_DATABASE_SPECIFIC_PARAMETER::activated;

  my $set_host                 = $DB::set_host;
  my $set_port                 = $DB::set_port;
  my $set_options              = $DB::set_options;
  my $set_tty                  = $DB::set_tty;
  my $set_dbname               = $DB::set_dbname;

  my $set_select_user          = $DB::set_select_user;
  my $set_select_user_password = $DB::set_select_user_password;


  my $dbi_source   = "dbi:Pg:dbname=$set_dbname";

  $SIG {PIPE} = sub { print "broken pipe \n" };

  # Connect (cached / through TCP/IP)
  unless ($DBI::conn and $DBI::connLastCachedDatabase eq $DB::CurrentDatabase) {
    $DBI::connLastCachedDatabase = $DB::CurrentDatabase;
    $DBI::conn = DBI->connect_cached($dbi_source, $set_select_user, "$set_select_user_password");

    if ($DBI::err) {
      print "<br>ERR: Could not establish connection to $set_dbname:<br>".$DBI::errstr."<br>";
      undef($co);
      undef($DBI::conn);
      return ();
    }
  }

  # returns the process-id of the backend process
  # my $pid = $conn->backendPID; # was in use with Pg.pm
  # $DBI;;conn-> do("\H");

  # my $result = $DBI::conn->prepare_cached($command);
  # $result = $DBI::conn->selectall_arrayref($command);
  # combines 'prepare', 'execute' and 'fetchall_arrayref'
  # my $num_fields = $#{${$result}[0]} + 1;
  # my $num_rows = $#{$result} + 1;
  # for (@{$result}) { print $_->[0] - $_[1] }

  $DBI::conn->do("SET search_path to $SERVER::search_path");

  my $result = $DBI::conn->prepare_cached($command);

  my $result_return = $result->execute;

  if ( $DBI::err ) {
    my $error = $DBI::errstr;
    print $co->br.$co->span({class=>'gray'}, "Bad Query: $error".$co->br."$command")."\n";
    undef($co);
    return 0;
  }


  if ($show_number_rows_affected and $command !~ /^\s*(?:\bexplain\b\s+)?\bselect\b/i) { # generally used on non-select operations

    my $result_return_message = ($result_return >  1)? "Your command has affected *$result_return* rows!" : 
                                ($result_return == 1)? "Your command has affected *$result_return* row!"  : 
                                ($result_return == 0)? "Your command has not affected any row!"           :
                                                       "Could not determine the number of rows affected!" ;
    $result->finish if $result;
    print $co->h3($result_return_message);
    undef($co);
    return $result_return;
  }

  # number of fields
  my $num_fields = $result->{NUM_OF_FIELDS};
  
  my ($num_rows, $matchs_num_rows);  
  
  my ($last_row, $last_value, $last_value_2, $old_value, $old_value_2, $row_counter, $row_counter_grouped, $row_bgcolor);
  my (@head, $head_name, @rows, @last_row, @nbsp_cell); 

  for (my $i = 0; $i <= $num_fields - 1; $i++) {
    $head_name = $result->{NAME}->[$i];
    $head_name =~ s/_upper_(.)/uc($1)/gie;
    $head_name =~ s/_nbsp_/\&nbsp\;/gi;
    $head_name =~ s/_openP_/<br>\(/gi;
    $head_name =~ s/_closeP_/\)/gi;
    $head_name =~ s/_nbsp_/\&nbsp;/gi;
    $head_name =~ s/_/ /g;
    $head[$i] = (length($head_name)>2)? $co->strong(ucfirst($head_name)):$co->strong($head_name);
    $nbsp_cell[$i] = '&nbsp;';
  }

  splice (@head, $separate_by_blocks, 1) if defined($separate_by_blocks);
  @rows = th({-class=>"$table_alignment_text_class", -bgcolor => 'lightblue' }, \@head) unless defined($separate_by_blocks);

  my ($last_separate_field, @separate_by_blocks_head, $head_length, $group_rows_separate_now, $group_rows_first_separation_skipped, @void_head_th);
  $void_head_th[($#head)] = '';
  if (defined($separate_by_blocks)) {
    $separate_by_blocks_head[$#head] = $co->br;
    $head_length = ($separate_by_blocks_title_position >= 0)? $separate_by_blocks_title_position : int($#head/2);
  }
  FETCHED_ROWS:

  while ( @last_row = $result->fetchrow_array) {

   map {$_ = $void_cell unless $_} @last_row if $void_cell;

    my  $separate_by_blocks_move_sub_header;

    if (defined($separate_by_blocks)) {
      if ($last_separate_field ne $last_row[$separate_by_blocks]) {
        $row_counter = 0;
        $separate_by_blocks_move_sub_header = 1;
        push (@rows, th({ -bgcolor => '#fffafa', -class=>"$table_alignment_text_class"}, \@separate_by_blocks_head)) if @rows;
        $separate_by_blocks_head[$head_length] = $co->br.$co->span({style=>$font_style." font-weight: bold;"}, $last_row[$separate_by_blocks]).$co->br.$co->br;
#        push (@rows, th({ -bgcolor => '#a52a2a'}, \@void_head_th ));
        push (@rows, th({ -bgcolor => "$main::bkgrd"}, \@void_head_th ));
#        push (@rows, th({-align=>'center'}, \@separate_by_blocks_head ));
        push (@rows, th({ -bgcolor => 'lightblue', -align=>'left' }, \@separate_by_blocks_head ));
#        push (@rows, th({ -bgcolor => "$main::bkgrd"}, \@void_head_th ));
        push (@rows, th({ -bgcolor => 'lightblue', -class=>"$table_alignment_text_class"}, \@head ));
        $separate_by_blocks_head[$head_length] = '';
        undef($group_rows_first_separation_skipped);
      }
      $last_separate_field = $last_row[$separate_by_blocks];
      splice(@last_row, $separate_by_blocks, 1);
    }


    $num_rows++;

    # Separate by rows groups for some output: e.g. Author & pI / (we use specific different outputs)
    if ($group_rows) {
      $group_rows_separate_now = 1;
      $last_value   = $last_row[0];
      $last_value_2 = $last_row[1];
      $last_row[1]  = $last_row[2] = '&nbsp;' if ($hidden_reference eq 'pI' and $last_row[0] eq $old_value and $last_row[1] eq $old_value_2);
      if ($last_row[0] eq $old_value) {
        $last_row[0]  = $co->center("&nbsp;"); my $a;
        foreach my $group_row_extra (@{$group_rows_extra}) {
          $last_row[$group_row_extra-1]  = $co->center("&nbsp;"); $a++; last if $a > 100;
        }
        $row_counter++;
        undef($group_rows_separate_now);
      }
      else {
        $row_counter_grouped++;
      }
      #if ((!defined($separate_by_blocks) or !$separate_by_blocks_move_sub_header) and $old_value and $old_value ne $last_value) { 
        # push (@rows, td({-bgcolor=>'#DBEEFF'}, [@nbsp_cell[0..$#head]]));
      #}
      $old_value   = $last_value;
      $old_value_2 = $last_value_2;
    }

    if ($no_color_alternation) {
      if ($group_rows and $group_rows_separate_now) {
        push (@rows, th({-bgcolor => '#f0e68c', -border=>0, -class=>"$table_alignment_text_class"}, \@void_head_th)) if $group_rows_first_separation_skipped;
        $group_rows_first_separation_skipped = 1;     
      }
      push (@rows, td({valign=>$cell_valign},[@last_row]));
    }
    else {
      $row_bgcolor = ($row_counter % 2)? '#f0e68c':'#fffafa';
      $row_counter++;
      push (@rows, td({-bgcolor=>"$row_bgcolor", -bordercolor=>"$row_bgcolor", -border=>0, -valign=>$cell_valign},[@last_row]));
    }

    #push (@rows,"\n");

  }

  # Formatting Table:
  # adding &nbsp to any cell prevents it from remaining void (can not always apply formatting on a void cell)
  map {$_ =~ s/(<TD[^>]*>)<\/TD>/$1&nbsp<\/TD>/mgi } @rows;

  # AC's and ID's are surrounded by '__AC__', and so on.. -> we replace marks by HREF's
  my $privateKeyword = ($main::privateKeyword)? $main::privateKeyword : ' {private}';
  map { $_ =~ s/__AC__(\S+)__AC__/$co->a({href=>"$main::script_name$main::script_name_qmark"."ac=$1$DB::ArgumentCurrentDatabaseNameURI"}, $1)/emgi;
        $_ =~ s/__MAP__(\S+)__MAP__/$co->a({href=>"$main::script_name$main::script_name_qmark"."map=$1$DB::ArgumentCurrentDatabaseNameURI"}, $1)/emgi;
        $_ =~ s/__SPOT__(\S+)\:(\S+)__SPOT__/$co->a({href=>"JavaScript:OpenViewerForSpot('$2', '$1', '$DB::CurrentDatabase')"}, $2)/emgi; 
        $_ =~ s/__SPOT__(\S+)__SPOT__/$co->a({href=>"JavaScript:OpenViewerForSpot('$1', '', '$DB::CurrentDatabase')"}, $1)/emgi;
        $_ =~ s/__EXPDATA__\*\*([^\*]+$privateKeyword\s*)\*\*__EXPDATA__/$co->span({-class=>'smallComment'},&_format_experimental_data_('no data',1))/emgi if $SERVER::hidePrivate;
        $_ =~ s/__EXPDATA__\*\*([^\*]+)\*\*__EXPDATA__/$co->strong({-class=>'smallCommentBrown'},&_format_experimental_data_($1,1))/emgi;
        $_ =~ s/__EXPDATA__\*\*\*\*__EXPDATA__/$void_cell/mgi; 
      } @rows;

  if(defined $rows[3] and $rows[3] =~ m/<strong.+?>([0-9\.\,]+)<\/strong>/) {
    my @elm = split(/\,/, $1);
    my @tmp;

    for my $i (0..$#elm) {
      push(@tmp, $co->span({style=>"font-family: sans-serif;"}, $elm[$i]));
      if(($i+1) % 10 == 0 and $i < $#elm) {
        push(@tmp, "<br>");
      }
    }
    my $str = join(",", @tmp);
    $str =~ s/<br>\,/<br>/g;
    $str .= $co->span({style=>$font_style}, ", TRYPSIN (both matched and unmatched)");
    $rows[3] =~ s/<strong.+?>[0-9\.\,]+<\/strong>/$str/;
  }

  sub _format_experimental_data_ {

    my $experimental_data = $_[0];
    my $spectraButton;

    if ($experimental_data =~ s/\[Spectra\:([^\]]+)\]//i and $` and $main::gd_chart_viewer) {
      my $co_spct = new CGI;
      my ($spectraSpot, $spectraMap, $spectraAC, $spectraData) = split '&&', $1;
      if ($SERVER::Chart and $spectraSpot and $spectraMap and $spectraAC and $spectraData) {
        if ($SERVER::Chart) {
          my $chartGetURL = $co_spct->url(-base=>1).$main::cgi_2ddb.'/'.$main::gd_chart_viewer.
           "?spot=$spectraSpot\&map=$spectraMap\&ac=$spectraAC\&data=$spectraData\&database=$DB::CurrentDatabaseName";
          $spectraButton = "\n".$co_spct->br.$co_spct->button({ class=>'colored', -value=>'Spectra navigator',
              -onClick=>"window.open(\"$chartGetURL\",'subWind','scrollbars,Height=880,Width=880,dependent')",
              -onMouseOut=>"this.className='colored'",
              -onMouseOver=>"this.className='colored upperCased';window.status='NAVIGATE graphically through the MAPS';return true"}).$co_spct->br.$co_spct->p."\n";
        }
      }
      if ($SERVER::javaApplets) {
        my $appletTitle = uc($spectraData)." spectra on Spot/Band: $spectraSpot ($spectraMap)";
        my @experimental_data = split "\n", $experimental_data;
        foreach my $dataLine (@experimental_data) {
         (my $dataLineCopy = $dataLine) =~ s/\[[^\]+]\]//;
          my ($masses_string, $intensities_string);
          while ($dataLineCopy =~ s/(\d+(?:\.\d*)?)\s*(\(\d+(?:\.\d*)?\))?;//) {
            my $read_mass = $1;
           (my $read_intensity = $2) =~ s/\(|\)//g;
            $read_intensity = '1.0' unless $read_intensity;
            $masses_string .= $read_mass." ";
            $intensities_string .= $read_intensity." ";
          }
          my $localSpectraButton;
          if ($masses_string and $intensities_string) {
            $localSpectraButton = $co_spct->button({
              -class=> "small",
              -value=>"view",
              -onClick=>"GraphApplet('$masses_string','$intensities_string', '$appletTitle')",
              -onMouseOut=>"this.className='small'",
              -onMouseOver=>"this.className='small upperCased';"}).$co_spct->br; # !! do not add any '\n' !!
          }
          $dataLine =  $localSpectraButton.$dataLine;
        }
        $experimental_data = join "\n", @experimental_data;
      }
    }

    $experimental_data =~ s/\n/ <br>+++<br> /mg;
    $experimental_data =~ s/(\bAll Experiment Data\:\s*)/<br><em class=\'smallComment\'>$1<\/em>/gi;
    my $currentDatabase = $main::database[$DB::CurrentDatabase]{dbname};
    $currentDatabase = $main::default_dbname unless $currentDatabase;
    $experimental_data =~ s/(\[Documents\])/$1<br>/;
    $experimental_data =~ s/(\bData Document URI\:\s*)(\S+)/<em>$1<\/em><a target=\"_blank\" href=\"$2\">$2<\/a><br>/gi;
    $experimental_data =~ s/(\bLocal Data Document\:\s*)(\S+)/<em>$1<\/em><a target=\"_blank\" href=\"$main::url_www2d\/data\/ms\/$currentDatabase\/$2\">$2<\/a><br>/gi;
    $experimental_data =~ s/(\bIdentification Document URI\:\s*)(\S+)/<em>$1<\/em><a target=\"_blank\" href=\"$2\">$2<\/a><br>/gi;
    $experimental_data =~ s/(\bLocal Identification Document\:\s*)(\S+)/<em>$1<\/em><a target=\"_blank\" href=\"$main::url_www2d\/data\/ms\/$currentDatabase\/$2\">$2<\/a><br>/gi;
    return $spectraButton.$experimental_data;
  }


  $result->finish if $result;
 #if ($DBI::conn) {$DBI::conn->disconnect; undef($DBI::conn);}


  $matchs_num_rows = ($num_rows > 1)? ($num_rows)." matchs":($num_rows)." match" if !defined($matchs_num_rows);

  if ($only_rows) {
    undef($co);
    return @rows;
  }

  my $final_table;

  if ($show_number_rows_selected) {
    $caption .= "<br>" if $caption;
    my $extra_caption .= $show_number_rows_selected." ".(($row_counter_grouped)? $row_counter_grouped : $row_counter);
    $extra_caption = $co->span({class=>'H4Font'}, $extra_caption);
    $caption .= $extra_caption;
  }

  if ($rows[1]) {

    $caption = $co->span({style=>$font_style}, 'Query Result:') unless $caption;
    if (!$no_table_caption) {
      $caption = caption({class=>'left'}, $co->br.$co->span({-class=>'H3Font underlined'}, $caption).$co->span({-class=>'Result bold', style=>$font_style}, " $matchs_num_rows").$co->br.$co->br)."\n";
    }
    elsif ($no_table_caption == 2) {
      $caption = caption({class=>'left'}, $co->br.$co->span({-class=>'H3Font underlined', style=>$font_style, align=>"center"}, $caption).$co->br.$co->br)."\n";
    }
    elsif ($no_table_caption == 1) {
      undef($caption);
    }

    $final_table =
      $co->table({-cellspacing=>0, -border=>$general_table_border, -width=>"$table_width",
                  -align=>"$table_alignment_general", -bgcolor=>'#fffafa'}, $caption,
                   TR({-class=>$table_alignment_text_class, -valign=>'middle'},\@rows)
                );
    if ($do_not_print) {
      undef($co);
      return $final_table
    }
    print $final_table;

  }

  else {
    undef($co);
    return 0 if $do_not_print;
    &SEARCH_ARGUMENT_NOT_OK('',$notOK_note) unless $hide_no_result_message;
    return 0;
  }

  undef($co);
  return 1;

}


#================================================================================================#
#================================================================================================#


# add a save file form

sub SAVE_FILE_FORM {

  my $file_name = $_[0];
     $file_name.= '.'.$$.'.txt' unless $file_name =~ /\.txt$/i;
  my $checked = ($_[1])? 'CHECKED=1' : '';

  return qq (

    <INPUT TYPE="checkbox" NAME="save_file" VALUE="Save" $checked> Save 
    -- To save the output to a text file, give a file name here:&nbsp;
    <INPUT TYPE="text" NAME="file_name" VALUE="$file_name" SIZE=20 MAXLENGTH=20>
    <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;file&nbsp;type:&nbsp;
    <INPUT TYPE="radio" NAME="file_type" VALUE="Unix">Unix
    <INPUT TYPE="radio" NAME="file_type" VALUE="Windows/DOS" "CHECKED">Windows/DOS
    &nbsp;&nbsp;&nbsp;&nbsp;--&nbsp;CSV&nbsp;format:&nbsp;
    <INPUT TYPE="checkbox" NAME="file_tabulated" value="on" CHECKED>
    <P>
    <HR><P>
    <em>If you choose to save your file in </em><strong>text</strong><em> format, make sure you check the right file type for your system. Otherwise, to keep a copy in </em>
    <strong>HTML</strong><em> format, you will simply need to save the result page directly from your browser.</em><P>

  )

}


#================================================================================================#
#================================================================================================#


# Prepare data to be saved in a text file

sub SAVE_FILE_DATA {

  use CGI qw (-no_xhtml);
  my $co_save_data = new CGI;

  my $data = $_[0];

  my $file_name = ($co_save_data->param('save_file'))? $co_save_data->param('file_name'):'tmp2D_'.$$.'.txt';
  $file_name = 'use_shorter_file_name.txt' if length($file_name) > 64;
  my $file_type = $co_save_data->param('file_type');
  $data =~ s/\<(?:[^\>]+href[^\>]+|\/a)\>//gi; # eliminate all hrefs
  $data =~ s/\<br\>/ /gi; # spaces instead of breakes
  $data =~ s/\n/\n\r/mg if $co_save_data->param('file_type') =~ /Windows|DOS/i;

  print $co_save_data->p,$co_save_data->p,$co_save_data->hr;

  print $co_save_data->start_form,"\n";
  print $co_save_data->hidden( -name => 'data to save', -value => $data);
  print $co_save_data->hidden( -name => 'file name', -value => $file_name);

  print $co_save_data->p({class=>"center"});
  print $co_save_data->submit({-name=>'Save to file', -value=>'Save to file' }),"&nbsp;&nbsp;&nbsp;";

  if (0 and length($data) < 2048) { # deactivated/deprecated
  $data = &URI_ENCODE("$data",'');
  my $URL_argument = "__SAVE_TO_FILE__+".$file_name."+".$data;
  print $co_save_data->button ( -name=>'Save_to_File',
                                -value=>'Save to File (GET method))',
                              # -target=>'newWindow',
                              # -OnCLick=>"window.open('$main::script_name$main::script_name_qmark$URL_argument','subWind','scrollbars,Height=600,Width=600,dependant')"
                                -OnCLick=>"window.top.location = '$main::script_name$main::script_name_qmark$URL_argument'"
                              ),"\n";
  }

  print $co_save_data->p;
  print $co_save_data->end_form,"\n";
  
  undef($co_save_data);
  1;
}


#================================================================================================#
#================================================================================================#


# Save file data to user disk (and exit)

sub SAVE_FILE_TO_DISK {

  use IO::File;
  use IO::Seekable;

  my $co_save_file_disk = new CGI;
  my ($size, $buff, $tmp_filehandle);
  my ($file_name, $data);

  if (0 and $ARGV[0] eq '__SAVE_TO_FILE__') { # 'GET' method desactivated/deprecated
   ($file_name, $data) = ($ARGV[1], $ARGV[2]);
    $data = &URI_ENCODE("$data", -1);
  }
  else { # post method
    $file_name = $co_save_file_disk->param('file name');
    $data = $co_save_file_disk->param('data to save');
  }

  unless (length($file_name)and length($data)) {

    print $co_save_file_disk->header;
    print $co_save_file_disk->start_html;
    print $co_save_file_disk->h2('Sorry!');
    print $co_save_file_disk->h3('No file name has been specified!') unless length($file_name) > 0;
    print $co_save_file_disk->h3('No data has been found!') unless length($data) > 0;
    print $co_save_file_disk->end_html;
    exit 0;

  }
  
  eval { $tmp_filehandle = IO::File->new_tmpfile() };
  if ($@) {
    print $co_save_file_disk->header; 
    print $co_save_file_disk->h3('Sorry, the server encountered some problem.<br>Please, try again!');
    exit 1
  }


  $tmp_filehandle->print("$data");
  $size = $tmp_filehandle->tell;
  $tmp_filehandle->seek(0,0);
  binmode $tmp_filehandle;
  binmode STDOUT;

  # print "Content-type: application/octet-stream\r\n";
  # print "Content-Length: $size\r\n";
  # print "Content-disposition: attachment;filename=$file_name\r\n\r\n";

  print $co_save_file_disk->header( -type=>"application/octet-stream", "Content-Length"=>"$size", "Content-Disposition"=>"attachment;filename=$file_name" );

  while ($tmp_filehandle->read($buff, 1024)) {
    print $buff;
  }

  $tmp_filehandle->close;

  exit 1;
  1;
}

#================================================================================================#
#================================================================================================#


# View a text file given by it's path (and exit)

sub VIEW_FILE {

  use IO::File;

  my $co_viewFile = new CGI;
  my $fileField = $_[0];
  my $file  = $co_viewFile->param($fileField);

  print $co_viewFile->header."\n";
  print $co_viewFile->start_html(-title=>"$fileField")."\n";

  unless (-r $file) {
    print $co_viewFile->h2('Sorry!');
    unless ($file) {
        print $co_viewFile->h3('No file name has been specified!');
    } elsif (-e $file) {
        print $co_viewFile->h3('The HTTP Server does not owe read permission for this file!');
    } else {
        print $co_viewFile->h3('This file does not exist!');
    }
    print $co_viewFile->h3("Submitted file: $file") if $file;    
  }
  else {
    my $fileHandler = new IO::File;
    $fileHandler->open($file);
    print $co_viewFile->start_pre."\n";
    while(my $line = <$fileHandler>) {
      print $line;
    }
    print $co_viewFile->end_pre."\n";
    $fileHandler->close;
  }

  print $co_viewFile->end_html;

  exit 1;
  1;
}


#================================================================================================#
#================================================================================================#


# Format entry with hyperlinks and maps for display

sub PRINT_ENTRY {

  use CGI qw (-no_xhtml);
  my $co = new CGI;

  my $name             = $_[0];
  my $arguments        = $_[1];
  my @names            = @$arguments;
     grep {s/\\//g} @names;
  my $name2d           = $main::name2d;
  my $bkgrd            = $main::bkgrd;

  my $main_index       = ($main::database[$DB::CurrentDatabase]{main_index})? $main::database[$DB::CurrentDatabase]{main_index} : $main::main_index;
  my $swiss_2d_page    = (defined($main::database[$DB::CurrentDatabase]{swiss_2d_page}))? $main::database[$DB::CurrentDatabase]{swiss_2d_page} : $main::swiss_2d_page;

  my $show_hidden_entries = $main::show_hidden_entries;

  # if $main::extract_image_URL_from_DB is activated, then we retrieve the images from within the database
  my $sgifs            =($main::database[$DB::CurrentDatabase]{small_image_url})? $main::database[$DB::CurrentDatabase]{small_image_url} : $main::default_small_image_url;
  my $small_image_type =($main::database[$DB::CurrentDatabase]{small_image_type})? $main::database[$DB::CurrentDatabase]{small_image_type} : $main::default_small_image_type;

  my %links            = %main::links;
  my ($print_entry, $allnames, @lines, $line, $anz, $nb_prot, $firstac, $pre, $rest);
  my ($AC, @ac, @images, @im, $lien, $newt, $medline, $pubmed, @master_spots, $last_master, %image_name, $cell);
  my ($image_src, $stoked);

  my $privateKeywordArgument = ($SERVER::hidePrivate and $main::privateKeyword)? $main::privateKeyword : '';

  my $cross_species = "CROSS-SPECIES IDENTIFICATION";


  $print_entry = "";
  $allnames = $co->strong({class=>'underlined'}, $name2d). (($#main::DATABASES_Included > 0)? 
              $co->strong(" (".$main::database[$DB::CurrentDatabase]{database_name}.")") : '');
  $allnames = "$allnames: ";
  foreach my $ac (@names) {
      $allnames .= $co->a({href=>"#".$ac}, $ac)." & \n";
  }
  $allnames =~ s/\s*\&\s*$//;
  if (length($allnames) > 512) {
    print "<br><br><center><font color='red'><strong>Process Halted!<br>Querry too long. Please, make your query shorter!".
          "</strong></font></center><br><br>" unless $EXECUTE_FAST_COMMAND::query_too_long;
    $EXECUTE_FAST_COMMAND::query_too_long = 1;
    return 0;
  }
  $print_entry .= $allnames."\n";

  # by clicking on a spot: KH tmp
  unless ( $ENV{'PATH_INFO'} eq "/def" || !($ENV{'PATH_INFO'})) {
    if (($#names == 0) && ($name ne "UNKNOWN")) {
	$nb_prot .= "1 protein has";
    }
    else {
	$anz = $#names + 1;
	$nb_prot .= "$anz proteins have"; 
    }
    if ($name eq "UNKNOWN") {
	$print_entry .= "The selected spot does not currently correspond \n ";
	$print_entry .= "to any known protein. Please try again at a later \n";
	$print_entry .= "stage or select another spot.\n";
    }
    else {
	$print_entry .= $co->p . $co->hr . $co->h2("$nb_prot been found: ") . '\n';
    } #else
  } # unless for spots

  EACH_ENTRY:
  foreach $name (@names) {
    unless ($name eq "UNKNOWN") {
	$print_entry .= $co->p . $co->br . $co->hr . $co->a({-name=>"$name"},'') . $co->h2 ("$name: ") . "\n\n";
        $print_entry .= "<!-- IF_".$name."_NICE_START -->"."\n";
        $print_entry .= $co->a({-href=>"$main::script_name$main::script_name_qmark"."ac=$name$DB::ArgumentCurrentDatabaseNameURI"}, 'Nice View').' - a user-friendly view of this entry'."\n";
        $print_entry .= "<!-- IF_".$name."_NICE_END -->"."\n";
        $print_entry .= $co->br."\n". "<!-- HH -->";
        $print_entry .= "<!--__PI_TOOL_REF_FOR_".$name."__-->";
	$print_entry .= "\n<PRE>\n";
    }
    my $print_entry_copy = $print_entry;

    if (1) {

        # make2db_ascii_entry takes AC(prim/sec) or ID
        my $command_ascii = "SELECT common.make2db_ascii_entry('".$name."','78', '".$privateKeywordArgument."')";
        my @entry = EXECUTE_FAST_COMMAND($command_ascii);

        @lines = split('\n', $entry[0]);
        map { $_ .= "\n" } @lines;
	pop(@lines);
	$firstac=1;
	foreach $line (@lines) {
            if ($line =~ /^[A-Z]{2}...[A-Z]/) {
                ($pre, $rest) = unpack("a2xxxa*", $line);
            }
            else {
                $pre=substr($line,0,2);
                $rest=substr($line,5);
            }

            # operations on all lines

	    if (($pre eq "DE" || $pre eq "CC" ) && $rest =~ /EC\s+(\d\.\d{1,2}\.\d{1,2}\.\d{1,3})/) {
		# cross-references to ENZYME:
		$line =~ /EC\s+(\d\.\d{1,2}\.\d{1,2}\.\d{1,3})/;
		$lien = &get_href({database=>'ENZYME', IDs=>[$1]});
		$line = "$`EC " . $co->a({href=>"$lien"}, $1) . "$'";
	    }

            # operations on different lines

	    if ($pre eq "AC") {
		$print_entry .= $line;
                @images=();
	    	if ($firstac) {
			@ac=split(/\;/,$rest);
			$AC=$ac[0];
			$firstac=0;
                        unless ($main::show_hidden_entries) {
                          (my $showable_ac) = EXECUTE_FAST_COMMAND("SELECT AC FROM Entry where AC = '$AC' AND $SERVER::showFlagSQLEntry");
                           @lines=(), $print_entry = $print_entry_copy unless $showable_ac;
                        }
		}
	    }
            elsif ($pre eq "OX") {
	        if ($line =~ /NCBI_TaxID\=(\d+)/i) {
                  $newt = $1;
                  $lien =  &get_href({database=>'NEWT', IDs=>[$newt]});
                  $line =~ s/$newt/$co->a({href=>"$lien"}, $newt)/e;
                  $print_entry .= $line;
                }
            }
	    elsif ($pre eq "IM") {
		$print_entry .= $line;
		@im = split(/\s+/, $rest);
		chop(@im);
                push(@images,@im);
	    }
            elsif ($pre eq "RN") {
                (my $rn = $line) =~ s/^.+(\[\d+\])[.|\n]*$/$1/;
                $print_entry .= "<A NAME=\"$rn\">";
                $print_entry .= "</A>".$line;
            }
	    elsif ($pre eq "RX") {
	        $line =~ s/((?:^|\;)\s*DOI=)([^;]+)/$1.$co->a({href=>&get_href({database=>"DOI", IDs=>[$2]})}, $2)/ei;
		if ($line =~ /(?:MEDLINE|PubMed)=/i) {
	          $medline = $1 if ($line =~ /MEDLINE=(\d+);/i);
		  $pubmed = $1 if ($line =~ /PubMed=(\d+);/i);
                  chomp($line);
		  # $line =~ s/;$//;
                  $print_entry .= $line;
                  $print_entry .= (" [");
		  # Medline is now an obsolete sub-set of PubMed
		  my $pubmed_or_medline = ($pubmed)? $pubmed : ($medline)? $medline : '';
	          $lien = &get_href({database=>'MEDLINE', IDs=>[$pubmed_or_medline]}),
                   $print_entry .= $co->a({href=>"$lien"}, 'NCBI').', ' if $pubmed_or_medline;
                  $lien = &get_href({database=>'MEDLINE_ExPASy', IDs=>[$pubmed]}),
                   $print_entry .= $co->a({href=>"$lien"}, 'ExPASy').', ' if $pubmed and $swiss_2d_page;
                  $lien = &get_href({database=>'MEDLINE_EBI', IDs=>[$pubmed_or_medline]}),
                   $print_entry .= $co->a({href=>"$lien"}, 'EBI').', ' if $pubmed_or_medline;
                  $lien = &get_href({database=>'MEDLINE_Israel', IDs=>[$pubmed_or_medline]}),
                   $print_entry .= $co->a({href=>"$lien"}, 'Israel').', ' if $pubmed_or_medline;
                  $lien = &get_href({database=>'MEDLINE_Japan', IDs=>[$pubmed_or_medline]}),
                   $print_entry .= $co->a({href=>"$lien"}, 'Japan').', ' if $pubmed_or_medline;
                  $print_entry =~ s/,\s*$//;
                  $print_entry .= ("]\n");
		}
		else { $print_entry .= $line;}

    	    }
	    elsif ($pre eq "DR") {
	        chomp($rest);
		$rest =~ /^([^;]+);\s*([^;]+);?\s*([^;]+)?;?\s*([^;]+)?;?\s*([^;]+)?/;
		my $dr_db  = $1;
                my $dr_1id = $2;
                my $dr_2id = $3;
                my $dr_3id = $4;
		my $dr_4id = $5;
                my $dr_remains;
		   $dr_remains  = "; $dr_2id" if $dr_2id;
                   $dr_remains .= "; $dr_3id" if $dr_3id;
		   $dr_remains .= "; $dr_4id" if $dr_4id;
                   $dr_remains = '.' unless $dr_remains;
                $dr_1id =~ s/\s*\.\s*$//;
                my $lien = &get_href({database=>$dr_db, IDs=>[$dr_1id], UniProt=>$EXTERNAL::update_from_Make2DDB});
		if ($lien) {
		    $print_entry .= $pre . '   '. $dr_db . "; " . $co->a({href=>"$lien"}, $dr_1id) . "$dr_remains\n";
		}
		else {
		    $print_entry .= $line;
		}
                if (uc($dr_db) eq uc($main_index)) {
                  my $pi_tool_ref_pattern = "<!--__PI_TOOL_REF_FOR_".quotemeta($name)."__-->";
                  my $pi_tool_lien = &get_href({database=>'ExPASy_pI_tool', IDs=>[$dr_1id]});
                  my $pi_tool_ref = $co->a({-href=>"$pi_tool_lien"}, 'Compute the theoretical pI/Mw');
                  $print_entry =~ s/$pi_tool_ref_pattern/$pi_tool_ref/e;
                }
	    }


            elsif($pre eq "2D" || $pre eq "1D") {
	        if ($line =~ /\-\!\-\s*MASTER\:\s*(\S+)/i) {
                  $last_master = $1;
                  $last_master =~ s/\s*;?\s*$//;
                  undef(@master_spots);
                }
                $line =~ s/\[((\d+,?)+)\]/_insert_references_alink_($1)/eg;
		$line =~ s/((?:BAND|SPOT)\s+(\S+)\: )(Tandem mass spectrometry)/$1.
                           $co->a({href=>"$main::script_name$main::script_name_qmark".
                          "spot=$last_master:$2\&accession=$AC\&data=msms$DB::ArgumentCurrentDatabaseNameURI"},$3)/gie;
		$line =~ s/((?:BAND|SPOT)\s+(\S+)\: )(Peptide mass fingerprinting)/$1.
                           $co->a({href=>"$main::script_name$main::script_name_qmark".
                          "spot=$last_master:$2\&accession=$AC\&data=pmf$DB::ArgumentCurrentDatabaseNameURI"},$3)/gie;
		$line =~ s/((?:BAND|SPOT)\s+(\S+)\: )(Amino acid composition)/$1.
                           $co->a({href=>"$main::script_name$main::script_name_qmark".
                          "spot=$last_master:$2\&accession=$AC\&data=aa$DB::ArgumentCurrentDatabaseNameURI"},$3)/gie;
                $line =~ s/((?:\:|;)\s*(?:BAND|SPOT)\s+)((?:\w|\-)+)/$1.$co->a({href=>"JavaScript:OpenViewerForSpotEntry('$AC', '$2', '$last_master', '$DB::CurrentDatabase')"},$2)/eig;
                push (@master_spots, $2);
                # foreach my $master_spot (@master_spots) { # !!
                  # last if $line =~ /Mw\:/;
                  # $line =~ s/$master_spot([^)]{2})/$co->a({href=>"JavaScript:OpenViewerForSpot('$master_spot','$DB::CurrentDatabase')"}, $master_spot).$1/e;
                # }
                if ($swiss_2d_page && $line =~ /$cross_species\s*\(\s*(?:$main_index);\s*(([^;]+);\s*([^;]+)?;?\s*([^;]+)?);?\)/i) {
                  my $lien =  &get_href({database=>$main_index, IDs=>[$2]});
                  my $cross_link = $co->span({-class=>'red'}, "$cross_species ".$co->span({-class=>'blue'}, "(".$co->a({-href=>$lien}, $1).")"));
                  $line =~ s/$cross_species[^\n]+/$cross_link/;
                }
              $print_entry .= $line;
            }
	    else {
		$print_entry .= $line;
	    }

	} # foreach $line

	{ 

        if ($lines[0] eq '') {
          my $no_nice_view = "<!-- IF_".$name."_NICE_START -->(?:\\s|\\S|\\n)+<!-- IF_".$name."_NICE_END -->";
          $print_entry =~ s/$no_nice_view//img;
          my $subset = (@main::DATABASES_Included)? " ($DB::CurrentDatabaseName)" : '';
          $print_entry .= "</PRE>\n<BODY bgcolor=$bkgrd>\nThis protein does not exist in the current release of $name2d$subset<P>\n";
          next;
        }


      # $print_entry =~ s/\n(1|2)D\s{6}/\n2D   -!-/g;
	$print_entry .= "//";
	%image_name = &get_mapindex;
	$print_entry .= "\n". ("</PRE>\n") . $co->hr . "\n";;
       (my $caption_map_table = "The following 2-D map") .= (@images >1)? "s are":" is";
	$caption_map_table .= (" available for this protein:\n");
        $caption_map_table = $co->strong($caption_map_table);
        $print_entry .= ("<BR>");

        $cell = 0 ;
        $print_entry .= ("<TABLE border=3><caption>$caption_map_table</caption><TR><TH rowspan=10></TH>\n");

	foreach my $image (@images) {
            undef($image_src);
            $cell++;
            $print_entry .= ("</TR><TR>\n") if $cell % 3 == 1 && $cell > 2;          
	    $print_entry .= ("<TD>$image_name{$image}\n<BR>");
	    if ($main::extract_image_URL_from_DB) {
              ($image_src) = EXECUTE_FAST_COMMAND("SELECT GelImage.smallImageUrl FROM GelImage, GEL WHERE lower(Gel.shortName) = '".lc($image)."' and Gel.gelID = GelImage.gelID");
            }
            if (!$image_src or !-e $image_src) {
              $image_src = "$sgifs/$image.$small_image_type";
            }
	    
            $stoked = $co->img({align => "top", height => 100, hspace => 3, vspace => 3, src => "$image_src", alt => "$image"});
            $print_entry .= $co->a({href => "$main::map_viewer$main::map_viewer_qmark"."map=$image\&ac=$AC$DB::ArgumentCurrentDatabaseNameURI"}, $stoked);
            $print_entry .= ("</TD>\n");
	}
        while ($cell % 3 > 0 and $cell > 3) {
          $print_entry .= ("<TD>\&nbsp;</TD>");
          $cell++;
        }
	$print_entry .= ("</TR></TABLE><P>\n");

      }
    } 

  } #foreach	


  $print_entry = _insert_entry_copyright_text_($print_entry);
  $print_entry.= "<P>\n";

  undef($co);
  return $print_entry;

}


sub _insert_entry_copyright_text_ {
  my $entry = $_[0];
  my $noHtml = $_[1]? 1 : 0;
  my $copyright_text   = ($main::database[$DB::CurrentDatabase]{copyright_text})? $main::database[$DB::CurrentDatabase]{copyright_text} : $main::default_copyright_text;
  $copyright_text =~ s/\<[^>]+\>//sg if $noHtml;
  if ($copyright_text =~ /./) {
    chomp($copyright_text);
    $copyright_text = "\n".$copyright_text unless $copyright_text =~ /^\n/;
    $copyright_text =~ s/^\s+//;
    $copyright_text =~ s/\n(.)/\nCC   $1/g;
    my $cc_copyright = "CC   "."-"x75;
    $cc_copyright    = $cc_copyright."\nCC   ".$copyright_text."\n".$cc_copyright;
    $cc_copyright =~ s/(\n|\s)+$//;
    $entry =~ s/(\n[^C]{2}\s{3}[^\n]*)\n(DR\s{3}|\/\/|$)/$1\n$cc_copyright\n$2/;
    return $entry;
  }
}

sub _insert_references_alink_ {
 (my $references = $_[0]) =~ s/\s//g;
  my $alinks;
  my $co_ref_alink = new CGI;
  foreach my $reference (split ',', $references) {
    $alinks.= (($reference =~ /(\d+)/)? $co_ref_alink->a({href=>"#[$1]"}, $1) : $reference).',';
  }
  $alinks =~ s/,$//;
  return "[$alinks]";
}

#================================================================================================#
#================================================================================================#


# Format entry in a nice view with hyperlinks and maps for display

sub PRINT_NICE_ENTRY {

  use CGI qw (-no_xhtml);
  my $co = new CGI;

  my $parameters = $_[0];
  my @entry_ac = ($parameters->{entry_ac})? @{$parameters->{entry_ac}} : undef;
     grep {s/\\//g} @entry_ac;
  my $entry_spot = ($parameters->{entry_spot})? $parameters->{entry_spot} : undef;
  my $expand = ($parameters->{expand})? $parameters->{expand} : undef;
  my $expand_msms = ($parameters->{expand_msms})? $parameters->{expand_msms} : undef;
  my $entry_master;
  if ($entry_spot =~ /(.+)\:(.+)/) {
    $entry_master = $1;
    $entry_spot = $2;
  }

  my ($nice_print, $all_acs, $ac);

  my $name2d = $main::name2d. (($#main::DATABASES_Included > 0)? 
               " (".$main::database[$DB::CurrentDatabase]{database_name}.")" : '');
  my $name2d_searchTitle = $co->span({class=>'underlined'}, $main::name2d). (($#main::DATABASES_Included > 0)? 
                           " (".$main::database[$DB::CurrentDatabase]{database_name}.")" : '');
  my %image_name       =  &get_mapindex;
  my $sgifs            = ($main::database[$DB::CurrentDatabase]{small_image_url})? $main::database[$DB::CurrentDatabase]{small_image_url} : $main::default_small_image_url;
  my $small_image_type = ($main::database[$DB::CurrentDatabase]{small_image_type})? $main::database[$DB::CurrentDatabase]{small_image_type} : $main::default_small_image_type;
  my $main_index       = ($main::database[$DB::CurrentDatabase]{main_index})? $main::database[$DB::CurrentDatabase]{main_index} : $main::main_index;
  my $swiss_2d_page    = (defined($main::database[$DB::CurrentDatabase]{swiss_2d_page}))? $main::database[$DB::CurrentDatabase]{swiss_2d_page} : $main::swiss_2d_page;
  my $copyright_text_normal = ($main::database[$DB::CurrentDatabase]{copyright_text})? $main::database[$DB::CurrentDatabase]{copyright_text} : $main::default_copyright_text;
  my ($copyright_text, $sp_copyright_text_external);
  my $SP_or_TrEMBL_external_index = 'UniProtKB';
  
  my $privateKeywordArgument = ($SERVER::hidePrivate and $main::privateKeyword)? $main::privateKeyword : '';

  my %month = ( "JAN" => "January", "FEB" => "February", "MAR" => "March", 
                "APR" => "April",   "MAY" => "May",      "JUN" => "June", 
                "JUL" => "July",    "AUG" => "August",   "SEP" => "September", 
                "OCT" => "October", "NOV" => "November", "DEC" => "December" );

 (my $nbspCurrentDatabaseName = $DB::CurrentDatabaseName) =~ s/\s/\&nbsp;/g;
  my %upd_info = ( #"CREATED" => "Entered in $DB::CurrentDatabaseName in ",
                   "LAST MAP UPDATE" => "2D&nbsp;Annotations&nbsp;were&nbsp;last&nbsp;modified&nbsp;on",
		   "LAST UPDATE" => "General&nbsp;Annotations&nbsp;were&nbsp;last&nbsp;modified&nbsp;on&nbsp;",
                   "LAST SEQUENCE UPDATE" => "Last modified in ",
                   "INTEGRATED" => "integrated&nbsp;into&nbsp;$nbspCurrentDatabaseName&nbsp;on&nbsp;",
		   # External data from SP, 'Swiss-Prot' will be substituted by 'TrEMBL' if necessary
		   "SP CREATED" => "Entered in Swiss-Prot in ",
		   "SP LAST ANNOTATION UPDATE" => "Annotations&nbsp;were&nbsp;last&nbsp;modified&nbsp;on&nbsp;",
		   "SP LAST SEQUENCE UPDATE" => "Sequence&nbsp;was&nbsp;last&nbsp;modified&nbsp;on&nbsp;" );
		   # the new announced SP DT format
		   $upd_info{"INCORPORATED"} = $upd_info{"INTEGRATED"};
		   $upd_info{"GENERAL ANNOTATION VERSION"} = $upd_info{"LAST UPDATE"};
		   $upd_info{"2D ANNOTATION VERSION"} = $upd_info{"LAST MAP UPDATE"};
		   $upd_info{"SP INCORPORATED"} = $upd_info{"SP CREATED"};
		   $upd_info{"SP SEQUENCE VERSION"} = $upd_info{"SP LAST SEQUENCE UPDATE"};
		   $upd_info{"SP ENTRY VERSION"} = $upd_info{"SP LAST ANNOTATION UPDATE"};


  my $cross_species      = "CROSS-SPECIES IDENTIFICATION";
  my $two_d_MAIN_TOPICS  = ($SERVER::two_d_MAIN_TOPICS)? $SERVER::two_d_MAIN_TOPICS :
                           "MAPPING|NORMAL LEVEL|PATHOLOGICAL LEVEL|(NORMAL |DISEASE )*POSITIONAL VARIANTS|".
                           "EXPRESSION|(?:TANDEM )MASS SPECTROMETRY|PEPTIDE MASSES|PEPTIDE SEQUENCES|AMINO ACID COMPOSITION";
  my $ch2d_man_page      = "$main::expasy/ch2d/manch2d.html";
  my $ch2d_expl_gif_page = "$main::expasy/ch2d/expl-gifs.html";
  my $ch2d_compute_map   = "$main::expasy/cgi-bin/ch2d-compute-map?";

  my $tandemMassSpectrometryPattern = ($main::mapping_methods_containing{'MS/MS'})?
     $main::mapping_methods_containing{'MS/MS'} : 'Tandem mass spectrometry';
  my $PMFSpectrometryPattern = 'Peptide mass fingerprinting';
  my $AminoAcidPattern = 'Amino acid composition';

  my $titcol    = "#0000DD";
  my $tablecol  = "#BBFFFF";
  my $cellcol_normal = "#BBFFFF";
  my $cellcol_b_normal = "#6AFFFF"; $cellcol_b_normal = "#ADD8E6";
  my $cellcol_contrasted_normal  = "#AAFFFF";
  my $tablecol_external = "#F5F5D6";  
  my $cellcol_external = "#F5F5D6";
  my $cellcol_b_external = "#F5F5D6";
  my $cellcol_contrasted_external  = "#F0F0D1"; $cellcol_contrasted_external  = "#F0F0C9";
  my ($cellcol, $cellcol_b, $cellcol_contrasted);

  my $visitedBlueClass = ($SERVER::ExPASy)? 'expasyVisited' : 'blue';


  if ($main::show_external_data) {
    $sp_copyright_text_external =
      "Copyrighted by the UniProt Consortium, see <A class=\"small\" href = \"http://www.uniprot.org/terms\">".
      "http://www.uniprot.org/terms</A>. ".
      "Distributed under the Creative Commons Attribution-NoDerivs License";
    # $sp_copyright_text_external =~ s/\n/<br>/mg;
  }

  my $self_url;
  my $serach_AC_Pattern = '__SEARCHED_AC__';
  if ($co->param('hidden_reference')) {
    my $search_AC = $co->param('serach_AC'); $search_AC = $entry_ac[0];
    my $selected_database = $co->param('selected_database'); $selected_database = $DB::CurrentDatabase;
    my $withQueryString = ($ENV{QUERY_STRING})? $ENV{QUERY_STRING} : "ac=$serach_AC_Pattern\&database=$selected_database";
    $self_url = $co->url(-base=>1).$main::script_name.$main::script_name_qmark.$withQueryString;
  } else {
    # $self_url = $co->self_url; $self_url =~ s/;/\&/g; # self_url returns ';' instead of '&'?!!
    $self_url = $co->url(-base=>1).$main::script_name.$main::script_name_qmark.$ENV{QUERY_STRING};
  }

  my $self_url_expand = my $self_url_expand_msms = $self_url;

  my $expandButtonValue;
  if ($expand or $expand_msms) {
    $self_url_expand =~ s/\&expand[^\&]*$//; #/
    $expandButtonValue = '[Click here to reduce identification/mapping data]';
  } else {
    $self_url_expand .= '&expand' unless $self_url_expand =~ /\&expand[^\&]*$/;
    $expandButtonValue = '[Click here to expand identification/mapping data]';
  }
  my $two_d_topic_ExpandButton_General = $co->button({ -class=> "niceViewButtonHead", -value=>$expandButtonValue,
      -onClick=>"window.location.href='$self_url_expand'",
      -onMouseOut=>"this.className='niceViewButtonHead'",
      -onMouseOver=>"this.className='niceViewButtonHead upperCased';window.status='Expand/Reduce identifcation/mapping data values';return true"})."\n";
  my $expandMSMSbuttonValue;
  if ($expand_msms) {
    $self_url_expand_msms =~ s/\&expand\=ms\/?ms$//; #/
    $expandMSMSbuttonValue = '[Reduce MS/MS peak list data]';
  } else {
    $self_url_expand_msms =~ s/\&expand[^\&]*$//; # expanding msms implies expanding all general identifications
    $self_url_expand_msms .= '&expand=msms';
    $expandMSMSbuttonValue = '[Expand MS/MS peak list data]';
  }
  my $two_d_topic_ExpandMSMSButton_General = $co->button({ -class=> "niceViewButton", -value=>$expandMSMSbuttonValue,
      -onClick=>"window.location.href='$self_url_expand_msms'",
      -onMouseOut=>"this.className='niceViewButton'",
      -onMouseOver=>"this.className='niceViewButton upperCased';window.status='Expand/Reduce MS/MS displayed peak list values';return true"})."\n";
  my ($expandMSMSbuttonShowed, $msms_many_values);

  $all_acs = $co->strong("$name2d_searchTitle:&nbsp;&nbsp;");
  foreach $ac (@entry_ac) {
   $all_acs .= $co->a({href=>"#".$ac}, $ac)." & \n";
  }
  $all_acs =~ s/\s*\&\s*\n?$//;
#  $nice_print .= $all_acs.$co->p;


  # by clicking on a spot: develop this part later
  # {Include: number of proteins found in the spot, highligt spot in text}


  my ($pre, $rest);
  my ($swiss_prot_ref, $de, $gn, $os, $oc, $ox, $cc, $two_d_cc, $two_d, $two_d_all, $og, $kw, $dr, @images, $first_ref, $first_mt, $first_cc, $first_dr);
  my ($first_master, $last_master, $masters_header, $master, $image_link, @master_spots, $two_d_last_topic, $li_on);
  my ($next_is_new_2d_master, $spotLocationSection, %lastMasterSpotLocation, %lastMasterSpotLocationData, %lastMasterSpotLocationDataFound);
  my $insertLastMasterSpotLocationPattern = '__INSERT_lastMasterSpotLocation__';
  my ($incomm2d, %comm2d, $topicomm2d);
  my ($map_long_name, $map_species, $map_expInfo, $map_protein_th_location, $map_tissue, $image_src, $last_internal_ac);
  my (%spots, %mws, %methods);



 NICE_ENTRY_AC:

  foreach my $entry_ac (@entry_ac) {

    my ($add_external_table, $adding_external_table, $AChasMainIndex, $adding_external_2d, $adding_external_ch2d_maps);  
    my $nice_print_copy = $nice_print;
    undef($last_internal_ac);

   (my $two_d_topic_ExpandButton = $two_d_topic_ExpandButton_General) =~ s/$serach_AC_Pattern/$entry_ac/;
   (my $two_d_topic_ExpandMSMSButton = $two_d_topic_ExpandMSMSButton_General) =~ s/$serach_AC_Pattern/$entry_ac/;

  ADD_EXTERNAL_TABLE:

    my ($entry_AC_number, $command, $lines, $sp_version);

    unless ($add_external_table) {

      $cellcol = $cellcol_normal;
      $cellcol_b = $cellcol_normal;
      $cellcol_contrasted = $cellcol_contrasted_normal;
      $copyright_text = $copyright_text_normal;

      $entry_AC_number = $entry_ac;
      $command = "SELECT common.make2db_ascii_entry('".$entry_ac."','25000', '".$privateKeywordArgument."')";
      ($lines) = EXECUTE_FAST_COMMAND($command);

    }

    else {

      undef($add_external_table);
      $adding_external_table = 1;

      $cellcol = $cellcol_external;
      $cellcol_b =$cellcol_external;
      $cellcol_contrasted = $cellcol_contrasted_external;

      if ($adding_external_2d) {

      	my $tablecol_applied = $tablecol_external;
        $nice_print .= $co->br.$co->start_table({-border=>0, -bgcolor=>"$tablecol_applied", -width=>"100%"})."\n";
        $nice_print .= $adding_external_2d;
        $nice_print .= $co->end_table."\n";
        $nice_print .= $co->br. $co->br;

      }

      my $last_internal_ac = ($last_internal_ac)? $last_internal_ac : $entry_ac;
      $command = "SELECT uniProtAC, uniProtSecondaryAC, uniProtID, uniProtVersion, uniProtEntryIncorporatedDate,
                         uniProtSeqUpDate, uniProtSeqUpVersion, uniProtEntryUpDate, uniProtEntryUpVersion,
                         uniProtDescription, uniProtGeneNames, uniProtOrganellePlasmid, uniProtCategoryKeywords,
                         uniProtXrefs, SPorTrEMBL ".
                 "FROM ExternalMainXrefData WHERE AC = '".$last_internal_ac."'";

      my @lines = EXECUTE_FAST_COMMAND($command);
      next unless @lines;

      my ($sp_AC, $sp_AC_not_first, $sp_ID, $sp_version_local, $sp_entry_incorporated_date,
          $sp_sequence_update_date, $sp_sequence_update_version, $sp_entry_update_date, $sp_entry_update_version,
          $sp_description, $sp_geneNames, $sp_organellePlasmid, $sp_function_keywords, $sp_DRs, $SP_or_TrEMBL) = @lines;

      $SP_or_TrEMBL_external_index .= ($SP_or_TrEMBL)? '/Swiss-Prot' : '/TrEMBL';  
      $copyright_text = ($SP_or_TrEMBL_external_index =~ /Swiss.?Prot$/i)? $sp_copyright_text_external : '';

      $sp_version = $sp_version_local;

      $sp_AC_not_first =~ s/\W*(\w+)\W*/$1; /mg;
      $sp_AC_not_first = "; ".$sp_AC_not_first;
      undef($sp_AC_not_first) unless $sp_AC_not_first =~ /\w/;

      my $sp_DT = "DT   ".&date_human($sp_entry_incorporated_date)." (UniProtKB/Swiss-Prot, incorporated)\n".
                  "DT   ".&date_human($sp_sequence_update_date)." (UniProtKB/Swiss-Prot, sequence version $sp_sequence_update_version)\n".
                  "DT   ".&date_human($sp_entry_update_date)." (UniProtKB/Swiss-Prot, entry version $sp_entry_update_version)\n";

      my $sp_DR_block;
      while ($sp_DRs =~ s/\{+([^\}]+)\}//) {
        my $sp_dr_line = $1.".\n";
           $sp_dr_line =~ s/,\"{2}//mg;
           $sp_dr_line =~ s/,/; /mg;
           $sp_dr_line =~ s/\"//mg;
           $sp_DR_block  .= "DR   $sp_dr_line";
      }

      $lines = "ID   $sp_ID\nAC   $sp_AC$sp_AC_not_first\n$sp_DT\nDE   $sp_description\n".
               "GN   $sp_geneNames\nOG   $sp_organellePlasmid\nKW   $sp_function_keywords\n$sp_DR_block//\n";

    }



    if ($lines !~ /^(?:ID|AC)/) {
      $nice_print .= $co->a({-name=>$entry_ac},'').$co->hr.$co->h2($entry_ac) if $#entry_ac;
      $nice_print .= "This protein does not exist in the current release of $name2d".$co->br."\n";
      next;
    }
    if ($lines =~/\nDR\s+$main_index;\s*([^\n]+)\n/i) { # to link to 'pi_tool'
      $swiss_prot_ref = $1;
      $swiss_prot_ref = $1 if $swiss_prot_ref =~ /([^;]+);/;
      $swiss_prot_ref =~ s/\s+$//;
    }    

    my @lines = split "\n", $lines;
    pop(@lines);

    my ($masters_concat);
     #= ('XX', undef);
    
    my $prev_table;

    for (my $ii = 0; $ii <= $#lines; $ii++) {
      my $line = $lines[$ii];
      my ($next_line_keyword, $next_line);
      $next_line_keyword = $1, $next_line = $2 if $lines[$ii+1] =~ /^(\w{2})\s+(.*)/;
      #my $next_line_keyword, my $next_line = ('XX', 'ggg') unless $next_line_keyword;
      if ($line =~ /[A-Z]{2}...[A-Z]/) {
        ($pre, $rest) = unpack("a2xxxa*", $line);
      }
      else {
        $pre  = substr($line,0,2);
        $rest = substr($line,5);
      }
      next if !$rest;
      
      undef($next_is_new_2d_master);
      if  ($next_line_keyword =~ /^\dD$/ and $next_line =~ /^\-\!\-\s+MASTER\:\s*(\S+)/) {
        $next_is_new_2d_master = 1
      }
      
      if ($pre eq "ID") {
        # initialize vars beginning of each entry
        $de = $gn = $og = $os = $oc = $ox = $cc = $kw = $dr = $two_d_all = $incomm2d = $topicomm2d = undef;
        undef(%comm2d);
        undef(@images);
        $first_ref = $first_mt = $first_cc = $first_dr = $first_master = 1;


        my $entry_name = $1 if $rest =~ /^\s*(\S*\w)/;
	$entry_name =~ s/CIPRO_/CIPRO/;
	if($entry_name =~ m/CIPRO|KH/) {
	  $entry_name =~s /_/./g;
	}
        unless ($adding_external_table) {
#          $nice_print .= $co->hr."\n";# if $#entry_ac;
          $nice_print .= $co->a({-name=>$entry_ac},'')."\n";
          #$nice_print .= $co->h2($entry_ac)."\n";
	  (my $cipro_id = $entry_ac) =~ s/(CIPRO)_/$1/;
	  $cipro_id =~ s/_/./g if($cipro_id =~ /^CIPRO|^KH/);
	  if($cipro_id =~ m/CIPRO|PROCITS/) {
	    (my $link_id = $cipro_id) =~s /CIPRO//;
            $nice_print .= $co->h2($co->a({href=>"http://cipro5.ibio.jp/2.1/index.cgi?id=".$link_id, target=>'_blank', style=>$link_style}, $cipro_id))."\n";
	  } elsif($cipro_id =~ m/KH/) {
            $nice_print .= $co->h2($co->a({href=>"http://cipro5.ibio.jp/2.1/index.cgi?locus=".$cipro_id, target=>'_blank', style=>$link_style}, $cipro_id))."\n";
	  } else {
            $nice_print .= $co->h2($co->span({style=>$font_style}, $cipro_id))."\n";
          }
        }
        my $tablecol_applied = ($adding_external_table)? $tablecol_external : $tablecol;
#        $nice_print .= $co->br.$co->start_table({-border=>0, -bgcolor=>"$tablecol_applied", -width=>"100%"})."\n";
        $nice_print .= $co->br.$co->start_table({-border=>0, -width=>"100%"})."\n";
        my $SP_or_TrEMBL_external_indexURL = $co->a({-class=>"LightColumn", -href=>"$main::expasy/uniprot"}, $SP_or_TrEMBL_external_index);
        my $tr_header = ($adding_external_table)? "External data extracted from $SP_or_TrEMBL_external_indexURL": 'General information about the entry';
#        $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({-class=>"LightColumn"}, $tr_header)))."\n";
        $nice_print .= $co->Tr($co->td({-colspan=>4}, $co->span({-class=>"LightColumn", -style=>$font_style." font-weight: bold;"}, $tr_header)))."\n";
        my $tr_header2 = ($adding_external_table)? 
          'Extracted from '.$co->strong("$SP_or_TrEMBL_external_index").', release: '.$co->strong($sp_version) :
           $co->a({href=>"$main::script_name$main::script_name_qmark"."ac=$entry_ac\&format=text$DB::ArgumentCurrentDatabaseNameURI"}, 
                   "View entry in simple text format");
#        $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$cellcol_b"}, $tr_header2))."\n";
        $nice_print .= $co->Tr($co->td({-width=>"20%"}, $co->span({style=>$font_style}, 'Entry name')).
                               $co->td({-colspan=>3}, $co->span({style=>$font_style}, "$entry_name")))."\n";
#        $nice_print .= $co->Tr($co->td({-width=>"20%", -bgcolor=>"$cellcol_b"}, 'Entry name').
#                               $co->td({-bgcolor=>"$cellcol_b", -colspan=>3}, b("$entry_name")))."\n";
      }

      elsif ($pre eq "AC") {
        my @acs = split ";",$rest;
        my $ac  = $entry_AC_number = shift(@acs);
        unless ($main::show_hidden_entries or $adding_external_table) {
          (my $showable_ac) = EXECUTE_FAST_COMMAND("SELECT AC FROM Entry where AC = '$ac' AND $SERVER::showFlagSQLEntry"); 
          unless ($showable_ac) {
            $nice_print = $nice_print_copy;
            $nice_print .= $co->a({-name=>$entry_ac},'').$co->hr.$co->h2($entry_ac) if $#entry_ac;
            $nice_print .= "This protein does not exist in the current release of $name2d".$co->br."\n";
            next NICE_ENTRY_AC;
          }
          $last_internal_ac = $ac;
        }
        my $print_ac_field = ($adding_external_table)? $co->a({-href=>&get_href({database=>'Swiss-Prot', IDs=>[$ac],
           UniProt=>$adding_external_table})}, "$ac") : $ac;
	$print_ac_field =~ s/CIPRO_/CIPRO/;
	if($print_ac_field =~ m/CIPRO|KH/) {
	  $print_ac_field =~ s/_/./g;
	}
        $nice_print .= $co->Tr($co->td($co->span({style=>$font_style}, 'Accession number')).
                               $co->td({-colspan=>3}, $co->span({style=>$font_style}, $print_ac_field)))."\n";
#        $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol_b"}, 'Primary accession number').
#                               $co->td({-bgcolor=>"$cellcol_b", -colspan=>3}, b("$print_ac_field")))."\n";
        $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol_b"}, 'Secondary accession number(s)').
                               $co->td({-bgcolor=>"$cellcol_b", -colspan=>3}, "@acs"))."\n" if (@acs);
      }

      elsif ($pre eq "DT") {
        if ($rest =~ /0?(\d+)\-([A-Z]{3})\-(\d+).*?release.*?(\d+)/i) {
         (my $day, my $month, my $year, my $relno) = ($1, $2, $3, $4);
          my $upd_info = "INTEGRATED";
          $upd_info = "SP ".$upd_info if $adding_external_table;
#          $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, $upd_info{uc($upd_info)}).
#                                 $co->td({-bgcolor=>"$cellcol", colspan=>3}, $month{uc($month)}." $day, $year"." (release $relno)"))."\n";
        }
        # the new format
        elsif ($rest =~ /0?(\d+)\-([A-Z]{3})\-(\d+).*?,\s+(.+ version)\s*(\d*)/i) {


         (my $day, my $month, my $year, my $upd_info, my $relno) = ($1, $2, $3, $4, $5);
          $upd_info =~ s/\s*$//;
          if ($adding_external_table) {
          $upd_info = "SP $upd_info";
          }
          my $add_version;
          $upd_info{uc($upd_info)} =~ s/Swiss.?Prot/TrEMBL/i if $SP_or_TrEMBL_external_index =~ /TrEMBL/i and $SP_or_TrEMBL_external_index !~ /Swiss\-?Prot/i;
          $add_version = " (version $relno)" if defined($relno);
#          $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, $upd_info{uc($upd_info)}).
#                                 $co->td({-bgcolor=>"$cellcol", colspan=>3}, $month{uc($month)}." $day, ".$year.$add_version))."\n";

        }
      }

      elsif ($pre eq "DE") {
        $de.= $rest;
        if ($next_line_keyword ne 'DE') {
	  $nice_print .= $co->Tr($co->td({-colspan=>4}, '&nbsp;'))."\n";
          $nice_print .= $co->Tr($co->td({-colspan=>4}, $co->span({class=>"LightColumn", style=>$font_style." font-weight: bold;"},'Name and origin of the protein')))."\n";
#          $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({class=>"LightColumn"},'Name and origin of the protein')))."\n";
          if ($de =~ /EC (\d\.\d{1,2}\.\d{1,2}\.\d{1,3})/) {
            my $de_buffer = $de;
            while ($de_buffer =~ s/EC (\d\.\d{1,2}\.\d{1,2}\.\d{1,3})//) {
              my $enzyme = $1;
              my $enzyme_link = &get_href({database=>'ENZYME', IDs=>[$enzyme], UniProt=>$adding_external_table});
              $de =~ s/EC ($enzyme)/"EC ".$co->a({href=>"$enzyme_link"}, "$1")/eg;
            }
	  }
	  $de =~ s/\.$//;
          $nice_print .= $co->Tr($co->td($co->span({style=>$font_style}, 'Description')).
                                 $co->td({-colspan=>3}, $co->span({style=>$font_style}, $de)))."\n";
#          $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, 'Description').
#                                 $co->td({-bgcolor=>"$cellcol", -colspan=>3}, $co->strong("$de")))."\n";
        }
        else { $de.= " " }
      }

      elsif ($pre eq "GN") {
        $gn.= $rest;
        if ($next_line_keyword ne 'GN') {
          $gn =~ s/\.\s*$//;
	  $gn =~ s/;\s*/<br>/g;
	  $gn =~ s/\band\b/&nbsp;&nbsp;&nbsp;and<br>/gi;
	  $gn =~ s/(Name=\w+)/$co->strong("$1")/egi;
          $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, 'Gene name').
                                 $co->td({-bgcolor=>"$cellcol", -colspan=>3}, "$gn"))."\n";
        }
        else { $gn.= " " }
      }

      elsif ($pre eq "OS") {
       ($os.= $rest) =~ s/\s*\.?\s*$//;
        if ($next_line_keyword eq 'OS') { $os.= " " }
      }

      elsif ($pre eq "OC") {
        $oc.= $rest;

        if ($next_line_keyword eq 'OC') { $oc.= " " }
      }

      elsif ($pre eq "OX") {
        $ox = ($rest =~ /(\d+)/)? $1 : 'undefined';
        my $tax_id_link = &get_href({database=>'NEWT', IDs=>[$ox], UniProt=>$adding_external_table});
#        $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, 'Annotated species').
#                               $co->td({-bgcolor=>"$cellcol", -colspan=>3}, $os." [TaxID: ".$co->a({href=>"$tax_id_link"}, "$ox")."]"))."\n";
        $nice_print .= $co->Tr($co->td($co->span({style=>$font_style}, 'Taxonomy')).
                               $co->td({-colspan=>3}, $co->span({style=>$font_style." font-size: small;"}, $oc)))."\n";
	$nice_print .= $co->Tr($co->td({-colspan=>4}, '&nbsp;'))."\n";
#        $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, 'Taxonomy').
#                               $co->td({-bgcolor=>"$cellcol", -colspan=>3}, "$oc"))."\n";

      }

      elsif ($pre eq "IM") {
        my @im = split(/\s+/, $rest);
        chop(@im);
        push(@images,@im);
      }
=pod
      elsif ($pre eq "RN") {
        if ($first_ref) {
          undef($first_ref);
#          $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({class=>"LightColumn"}, 'References')))."\n";
          $nice_print .= $co->start_Tr()."\n";
          $nice_print .= $co->start_td({-colspan=>4, -bgcolor=>"$tablecol"})."\n";
          $nice_print .= $co->start_table({-border=>0, -bgcolor=>"$tablecol", -width=>"100%"})."\n";
        }
        else {
          $nice_print .= $co->end_td;
          $nice_print .= $co->end_Tr;
        }
        $nice_print .= $co->start_Tr({-bgcolor=>"$cellcol"})."\n";
        $nice_print .= $co->td({-valign=>"top"}, $co->a({-name=>"$rest"}, "$rest &nbsp;"))."\n";
        $nice_print .= $co->start_td;
      }

      elsif  ($pre eq "RP" || $pre eq "RG" || $pre eq "RA" || $pre eq "RT" || $pre eq "RL") {
        chop($rest);
        $nice_print .= $rest;
        $nice_print .= $co->br if $pre ne $next_line_keyword;
      }
      elsif ($pre eq "RX") {
        $rest =~ s/((?:^|\;)\s*DOI=)([^;]+)/$1.$co->a({href=>&get_href({database=>"DOI", IDs=>[$2], UniProt=>$adding_external_table})}, $2)/ei;
        if ($rest =~ /(?:MEDLINE|PubMed)=/i) {
          my $medline = $1 if ($rest =~ /MEDLINE=(\d+);/i);
          my $pubmed = $1 if ($rest =~ /PubMed=(\d+);/i);
          chomp($rest);
	  # $rest =~ s/;$//;
          $nice_print .= $rest;
          $nice_print .= (" ["); 
	  # Medline is now an obsolete sub-set of PubMed
	  my $pubmed_or_medline = ($pubmed)? $pubmed : ($medline)? $medline : '';
	  my $lien;
          $lien = &get_href({database=>'MEDLINE', IDs=>[$pubmed_or_medline], UniProt=>$adding_external_table}),
           $nice_print .= $co->a({href=>"$lien"}, 'NCBI').', ' if $pubmed_or_medline;
          $lien = &get_href({database=>'MEDLINE_ExPASy', IDs=>[$pubmed], UniProt=>$adding_external_table}),
           $nice_print .= $co->a({href=>"$lien"}, 'ExPASy').', ' if $pubmed and $swiss_2d_page;
          $lien = &get_href({database=>'MEDLINE_EBI', IDs=>[$pubmed_or_medline], UniProt=>$adding_external_table}),
           $nice_print .= $co->a({href=>"$lien"}, 'EBI').', ' if $pubmed_or_medline;
          $lien = &get_href({database=>'MEDLINE_Israel', IDs=>[$pubmed_or_medline], UniProt=>$adding_external_table}),
           $nice_print .= $co->a({href=>"$lien"}, 'Israel').', ' if $pubmed_or_medline;
          $lien = &get_href({database=>'MEDLINE_Japan', IDs=>[$pubmed_or_medline], UniProt=>$adding_external_table}),
           $nice_print .= $co->a({href=>"$lien"}, 'Japan').', ' if $pubmed_or_medline;
          $nice_print =~ s/,\s*$//;
          $nice_print .= ("]\n").$co->br;
        }
        else { $nice_print .= $rest.$co->br;}
      }
      if ($pre =~ /^R/ and $next_line_keyword !~ /^R/) {
        $nice_print .= $co->end_td;
        $nice_print .= $co->end_Tr;
        $nice_print .= $co->end_table."\n";
      }

      elsif ($pre eq "CC") {
        if ($rest =~ s/^\-\!\-\s*([^:]+)\://) {
          $rest.= $co->br unless $first_cc;
          $rest = $co->li($co->strong($1).": ".$rest);
        }
        $cc = $co->start_ul, undef($first_cc) if $first_cc;
        $cc.= $rest;
        $cc.= $co->end_ul if $next_line_keyword ne "CC";
        if ($next_line_keyword ne 'CC') {
          $cc =~ s/\.\s*$//;
          $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({class=>"LightColumn"}, 'Comments')))."\n";
          $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol", -colspan=>4}, "$cc"))."\n";
        }
        else { $cc.= " " }
      }
=cut
      elsif ($pre =~ /^\dD/) {
        if ($rest =~ /^\-\!\-\s+MASTER\:\s*(\S+)/) {
          undef($first_master);
          $last_master = $1;
          $last_master =~ s/\s*;?\s*$//;
        }
        if (!$masters_header) {
          $masters_header = 1;
          $nice_print .= $co->Tr($co->td({-colspan=>4}, $co->span({class=>"LightColumn", style=>$font_style." font-weight: bold;"}, '2D PAGE maps for identified proteins')))."\n";
	  $nice_print .= "<!-- cutable -->"."\n";
#          $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({class=>"LightColumn"}, '2D PAGE maps for identified proteins '.$two_d_topic_ExpandButton)))."\n";
        }
        if ($first_master && $rest =~ s/^\-\!\-\s*([^:]+)\://) {
          $rest = $co->strong($1).": ".$rest;
#          $two_d_cc .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$cellcol"}, "$rest"))."\n"; 
        }
        if (!$first_master) {
          if ($rest =~ /^\-\!\-\s+MASTER\:\s*(\S*[^;])/) {
            $master = $1;
            $masters_concat .= "__".$master."__";
            undef($two_d_last_topic);
            undef(@master_spots);
           ($image_src, $map_species, $map_tissue) = 
	       EXECUTE_FAST_COMMAND(
	         "SELECT GelImage.smallImageUrl, Organism.organismSpecies, TissueSP.tissueSPDisplayedName ".
                 "FROM GelImage, Organism, Gel ".
                 "LEFT JOIN GelTissueSP ON (Gel.gelID = GelTissueSP.gelID) ".
                 "LEFT JOIN TissueSP ON (TissueSP.tissueSPName = GelTissueSP.tissueSPName) ".
	         "WHERE GelImage.GelID = Gel.GelID AND Gel.shortName = '$master' AND Organism.organismID = Gel.organismID LIMIT 1 "
               );
            if (!$main::extract_image_URL_from_DB or !$image_src or !-e $image_src) {
	      $image_src = "$sgifs/$master.$small_image_type";
	    }
	    my $cipro_id = $entry_ac;
	    if($cipro_id =~ m/CIPRO/){
	      $cipro_id =~ s/CIPRO_/CIPRO/;
	      $cipro_id =~ s/_/./g;
              $image_link = $co->img({align => "top", height=>120, hspace => 3, vspace => 3, src => "http://cipro5.ibio.jp/~yonezawa/displayGelImage.cgi?id=".$cipro_id."&gel=".$master, alt => "$master"});
	    } elsif($cipro_id =~ m/KH/) {
	      $cipro_id =~ s/_/./g;
              
	      $image_link = $co->img({align => "top", height=>120, hspace => 3, vspace => 3, src => "http://cipro5.ibio.jp/~yonezawa/displayGelImage.cgi?id=".$cipro_id."&gel=".$master, alt => "$master"});
	    } elsif($cipro_id =~ m/PROCITS/) {
              $image_link = $co->img({align => "top", height => 100, hspace => 3, vspace => 3, src => "http://cipro5.ibio.jp/~yonezawa/displayGelImage.cgi?id=".$cipro_id."&gel=".$master, alt => "$master"});
	    } else{
              $image_link = $co->img({align => "top", height => 100, hspace => 3, vspace => 3, src => "$image_src", alt => "$master"});
	    }
#            $image_link = $co->img({align => "top", height => 100, hspace => 3, vspace => 3, src => "$image_src", alt => "$master"});
            $image_link    = $co->a({href => "$main::map_viewer$main::map_viewer_qmark"."map=$master\&ac=$entry_AC_number$DB::ArgumentCurrentDatabaseNameURI"}, $image_link);
            $map_long_name = &one_map_full_name($master);
            $map_long_name = ($map_long_name =~ /\{\s*(.+?)\s*\}/)? $1 : '';
            $map_long_name = $image_name{$master} if (!$map_long_name and $image_name{$master} ne $master);
	    $map_species   = ($map_species)? $co->br.$co->em($map_species) : '';
            $map_tissue    = ($map_tissue)? $co->br."Tissue: $map_tissue" : '';
            $map_expInfo   = $co->br.'&nbsp;&nbsp;'.
              $co->a({-href=>"$main::script_name$main::script_name_qmark".'map='.lc($master).'&info'}, $co->span({-class=>"small $visitedBlueClass"}, 'map experimental info'));
            $map_protein_th_location = ($swiss_2d_page and $ch2d_compute_map)? $co->br.'&nbsp;&nbsp;'.
              $co->a({-href=>"$ch2d_compute_map".uc($master).",$entry_ac"},  $co->span({-class=>"small $visitedBlueClass"}, 'protein estimated location')) : '';
            my $master_link = $co->a({-href=>"$main::script_name$main::script_name_qmark".'map='.lc($master), style=>$link_style}, $master);
            $image_link    = $master_link.(($map_long_name)? $co->span({style=>$font_style." font-size: small;"}, " {$map_long_name}") : '').
                             $co->br.$image_link."\n".$map_protein_th_location."\n";
#                             $map_species."\n".$map_tissue.$co->br.$image_link."\n".$map_expInfo.$map_protein_th_location."\n";
            if ($two_d) {
              $two_d     .=  "\n".$co->end_td.$co->end_Tr;
              $two_d_all .= $two_d;
	    }
            $two_d  = $co->start_Tr;
#            $two_d  = $co->start_Tr({-bgcolor=>"$cellcol"});
            $two_d .= $co->td({-valign => "top", -width=>"180"}, $co->br."$image_link")."\n";
            $two_d .= $co->td({-width=>"10"}, "&nbsp;")."\n";
            $two_d .= $co->start_td({-colspan=>3});
            $two_d .= $co->br.$co->span({class=>'underlined', style=>$font_style." font-weight: bold;"}, "$master").$co->br.$co->br.$co->span({style=>$font_style." font-weight: bold;"}, 'DETAIL');
#            $two_d .= $co->br.$co->span({class=>'underlined', style=>$font_style." font-weight: bold;"}, "$master").$co->br.$co->br.$co->strong('MAP LOCATIONS:');
            $two_d .= $insertLastMasterSpotLocationPattern;
            undef($spotLocationSection); undef(%lastMasterSpotLocation);
            undef(%lastMasterSpotLocationData); undef(%lastMasterSpotLocationDataFound);
          }
          else {
            if ($rest =~ /^\-\!\-\s*(?:PI\/)?MW:\s*(SPOT|BAND)\s*(\S+)=(\S+);/i) {
#              my $spot_kind = $1;
              my $spot_kind = 'SPOT';
              my $spot_name = $2;
              my $spotIdentData;
              my $pi_mw = $3;
              push (@master_spots, $spot_name);
              if ($pi_mw =~ /(\S+)\/(\S+)/) { $pi_mw = "pI=$1; Mw=$2"}
#              else                          { $pi_mw = "Mw=$pi_mw"}
	      if (lc($spot_name) eq lc($entry_spot) and (lc($last_master) eq lc($entry_master) or !$entry_master)){
                $pi_mw = $co->a({-name=>"spot"}, $co->span({-style=>$font_style."color: #FF0000;"}, $pi_mw));
	      } else {
                $pi_mw = $co->a({-name=>"spot"}, $co->span({-style=>$font_style}, $pi_mw));
	      }
	      my $i=0;
	      while(defined $mws{$master}{$i}){
	        $i++;
	      }
	      $mws{$master}{$i} = $pi_mw;
	      $spots{$master}{$i} = $co->span({style=>$font_style}, $spot_name);
#              $pi_mw = $co->a({-name=>"spot"}, $co->span({-class=>'red'}, "(the searched spot)")).$co->br.$pi_mw
#                if lc($spot_name) eq lc($entry_spot) and (lc($last_master) eq lc($entry_master) or !$entry_master);
=pod
              my $spot_nameView =
                $co->a({href=>"JavaScript:OpenViewerForSpotEntry('$entry_AC_number', '$spot_name', '$master','$DB::CurrentDatabase')"}, uc($spot_kind).' '.$spot_name);
              $spotIdentData = $co->a({href=>"$main::script_name$main::script_name_qmark".
                "spot=$master:$spot_name\&accession=$last_internal_ac\&data=all$DB::ArgumentCurrentDatabaseNameURI"},'[identification data]');
              $rest = '';
              $lastMasterSpotLocation{$spot_name} =
                $co->td({-width=>"150"}, $co->ul({-class=>"compact"},$co->li({-class=>"compact"},$spot_nameView.': '))).
                $co->td($pi_mw);
              $lastMasterSpotLocationData{$spot_name} = $co->td({-width=>"200", -align=>"left"}, '&nbsp;&nbsp;'.$spotIdentData);
=cut
            }
            elsif ($rest =~ s/^\-\!\-\s*($two_d_MAIN_TOPICS)\s*\://si) {
	      my $two_d_topic;
#              my $two_d_topic = uc($1);
              my $two_d_topic_details = $';
#              $two_d_topic_details =~ s/BAND/SPOT/g;
#              $two_d_topic = "MAPPING (identification)" if $two_d_topic eq "MAPPING";
              my $two_d_new_ident_topic = '';
	      my @elm=split(/\;/, $two_d_topic_details);
	      for my $e (@elm) {
	        my ($spot, $m) = split(/\:\s/, $e);
		$spot =~ s/\sBAND\s//;
		$m =~ s/\[1\]//;
		$m =~ s/\.//;
		$m =~ s/\s+$//;
		$methods{$master}{$spot} = $co->span({style=>$font_style}, $m);
	      }
              if ($two_d_last_topic ne $two_d_topic) {
                undef($li_on);
                $rest .= $co->br;
                if ($two_d_topic eq 'AMINO ACID COMPOSITION') {
                  $li_on = 'aa';
                  $two_d_new_ident_topic = $co->br.$co->a({-href=>$ch2d_man_page.'#AAC'}, $co->strong('AMINO ACID COMPOSITION:'))
                } elsif ($two_d_topic eq 'PEPTIDE MASSES') {
                  $li_on = 'pmf';
                  $two_d_new_ident_topic = $co->br.$co->a({-href=>$ch2d_man_page.'#MS'}, $co->strong('PEPTIDE MASSES:'));
                } elsif ($two_d_topic =~ /(?:TANDEM )MASS SPECTROMETRY/) {
                  $li_on = 'msms';
                  $two_d_new_ident_topic = $co->br.$co->a({-href=>$ch2d_man_page.'#MSMS'}, $co->strong('TANDEM MASS SPECTROMETRY:'));
                  if ($li_on eq 'msms' and length($two_d_topic_details)>400) {
                    $two_d_new_ident_topic .= $two_d_topic_ExpandMSMSButton;
                    $expandMSMSbuttonShowed = 1;
                  }
                } elsif ($two_d_topic eq 'PEPTIDE SEQUENCES') {
                  $li_on = 'pepseq';
                  $two_d_new_ident_topic = $co->br.$co->a({-href=>$ch2d_man_page.'#MSMS'}, $co->strong('PEPTIDE SEQUENCES:'))
                } else {
                  $two_d .= $co->br.$co->strong($two_d_topic.':').$co->br;
                 #$two_d_topic_details =~ s/\s*(\b(?:SPOT|BAND)\s+\S+\s*\:)((.+\1[^;]+;?)+|[^;]+;?)/$1$2<br>/gi;
                 #$two_d_topic_details =~ s/<br>$//;
                  while ($two_d_topic_details =~ /\s*(\b(?:SPOT|BAND)\s+\S+\s*\:).+?\1/) {
                    $two_d_topic_details =~ s/\s*(\b(?:SPOT|BAND)\s+\S+\s*\:)(.+?)\1/$1$2/gi;
                  }
                  $two_d_topic_details =~ s/\s*(\b(?:SPOT|BAND)\s+\S+\s*\:)/<br>$1/gi;
                  $two_d_topic_details =~ s/^<br>//;
                  $two_d_topic_details = $co->table($co->Tr($co->td($two_d_topic_details)));

                }
              }
              if ($li_on and $two_d_topic_details =~ /(?:^|\s*\b)(SPOTS?|BANDS?)\s+(\S+)\s*\:\s*/i) {
		my $spot_name = $2;
                $lastMasterSpotLocationDataFound{$spot_name} = 1
                 if ($li_on eq 'aa' or $li_on eq 'pmf' or $li_on eq 'msms' or $li_on eq 'pepseq');
              }
              if ($li_on and !$expand) {
                $two_d_new_ident_topic = $two_d_topic_details = $rest = '';
              }
              $two_d .= $two_d_new_ident_topic if $two_d_new_ident_topic;

              if ($two_d_topic_details =~ /(?:^|\s*\b)(SPOTS?|BANDS?)\s+(\S+)\s*\:\s*/i) {
                my $spot_kind = $1;
		my $spot_name = $2;
                
                my ($masses_string, $intensities_string) = ();
                if (($li_on eq 'pmf' or $li_on eq 'msms') and $SERVER::javaApplets) {
		  my $two_d_topic_details_copy = $two_d_topic_details;
		  while ($two_d_topic_details_copy =~ s/(\d+(?:\.\d*)?)\s*(\(\d+(?:\.\d*)?\))?;//) {
		    my $read_mass = $1;
		   (my $read_intensity = $2) =~ s/\(|\)//g;
		    $read_intensity = '1.0' unless $read_intensity;
		    $masses_string .= $read_mass." ";
		    $intensities_string .= $read_intensity." ";
		  }
		}
                if ($li_on eq 'msms' and !$expand_msms and length($two_d_topic_details)>400) {
                  $two_d_topic_details = substr($two_d_topic_details,0, 75);
                  $two_d_topic_details =~ s/\S+$//;
                  $two_d_topic_details.= $co->span({class=>'smallComment'}, '...and many other values...');
                  $msms_many_values = 1;
                }
                if ($masses_string and $intensities_string) {
                  $two_d_topic_details .= $co->button({
                    -class=> "niceViewButton",
                    -onMouseOut=>"this.className='niceViewButton'",
                    -onMouseOver=>"this.className='niceViewButton upperCased';",
                    -onClick=>"GraphApplet('$masses_string','$intensities_string', '$spot_kind: $spot_name')",
                    -value=>"[view]"});
                }

                @master_spots = sort {&SortNumericThenAlpha or $a cmp $b} @master_spots;

                foreach my $master_spot (@master_spots) {
                  my $break_after_spot = '';
                  my $spotDataPresent;
		  # my $break_after_spot = ($li_on)? $co->br : '';
                  $two_d_topic_details =~ 
		    s/($spot_kind\s+$master_spot\: )(.*?)($tandemMassSpectrometryPattern)/$1.$2.
                     $co->a({href=>"$main::script_name$main::script_name_qmark".
                       "spot=$master:$master_spot\&accession=$last_internal_ac\&data=msms$DB::ArgumentCurrentDatabaseNameURI"},$3)/gie
                    and $spotDataPresent = 1;
                  $two_d_topic_details =~ 
		    s/($spot_kind\s+$master_spot\: )(.*?)($PMFSpectrometryPattern)/$1.$2.
                     $co->a({href=>"$main::script_name$main::script_name_qmark".
                       "spot=$master:$master_spot\&accession=$last_internal_ac\&data=pmf$DB::ArgumentCurrentDatabaseNameURI"},$3)/gie
                    and $spotDataPresent = 1;
                  $two_d_topic_details =~ 
		    s/($spot_kind\s+$master_spot\: )(.*?)($AminoAcidPattern)/$1.$2.
                     $co->a({href=>"$main::script_name$main::script_name_qmark".
                       "spot=$master:$master_spot\&accession=$last_internal_ac\&data=aa$DB::ArgumentCurrentDatabaseNameURI"},$3)/gie
                    and $spotDataPresent = 1;
                  $two_d_topic_details =~ 
		    s/($spot_kind\s+)\b$master_spot\b\s*\:?/$1.$co->a({href=>"JavaScript:OpenViewerForSpot('$master_spot', '$master', '$DB::CurrentDatabase')"}, $master_spot).":$break_after_spot"/gie;

                  $lastMasterSpotLocationDataFound{$master_spot} = 1 if ($spotDataPresent);
                }

		# $two_d_topic_details .= $co->br;
                $two_d_topic_details = $co->table($co->Tr(($co->td($co->ul({-class=>"compact"},$co->li({-class=>"compact"}, $two_d_topic_details)))))) if ($li_on);
                $rest = $two_d_topic_details."\n";

              }
              $two_d_last_topic = $two_d_topic;
            }
            else {
              $rest =~ s/^\-\!\-\s*//;
              $rest =~ s/^([^:]+\:)/$co->strong("$1")/e;
              $rest =~ s/;/$co->br/em;
              $rest = $co->br.$rest;
            }
            $rest =~ s/\[((\d+,?)+)\]/_insert_references_alink_($1)/eg;
#            $two_d .= $rest."\n";
          }
        }
        my ($two_d_table, %appear);
        my $table_index = $co->Tr($co->td({style=>$font_style, align=>"center"}, "SPOT"), $co->td({style=>$font_style, align=>"center"}, '&nbsp;&nbsp;'."MW"), $co->td({style=>$font_style, align=>"center"}, "Method"));
        my $i = 0;
	while(defined $spots{$master}{$i}){
	  $spots{$master}{$i} =~ m/([A-Z]+[0-9]+)/;
	  if(defined $methods{$master}{$&} and (!defined $appear{$spots{$master}{$i}})){
	    my $dbh = DBI->connect("dbi:Pg:dbname=sds", "postgres", "", {AutoCommit => 0});
	    my $sth = $dbh->prepare("SELECT one_d_blocks FROM viewentry WHERE accession_number = '$entry_ac'");
	    $sth->execute;
	    my $c = $sth->fetch;
	    $sth->finish;
	    $dbh->disconnect;
	    my $print_method = $methods{$master}{$&};
	    $print_method =~ s/Matching/matching/;
	    if($master =~ m/NC/){
	      $spots{$master}{$i} =~ m/(NC[0-9]+)/;
	      $print_method = $co->a({href=>"2d.cgi?spot=".$1."&accession=".$entry_ac."&data=pmf", style=>$link_style}, $print_method);
	    }
	    $spots{$master}{$i} =~ m/([A-Z]+[0-9]+)/;
            $two_d_table .= $co->Tr($co->td({width=>50, align=>"left"}, $spots{$master}{$i}), $co->td({width=>70, align=>"right"}, $mws{$master}{$i}), $co->td({width=>200, align=>'center'}, $print_method)) if $$c[0] =~ m/$&/;
	    $appear{$spots{$master}{$i}} = 1;
	  }
	  $i++;
	}
        $two_d .= $co->table($table_index, $two_d_table) if defined $two_d_table;
	$two_d =~ s/DETAIL\<\/span>.+?\<table/DETAIL\<\/span><table/;
	$two_d .= $co->br({-class=>'breakCategory1'})."\n".$co->br if ($next_is_new_2d_master);
        if ( ($next_is_new_2d_master or $next_line_keyword !~ /^\dD$/ or !$next_line_keyword)
             and $two_d =~ /$insertLastMasterSpotLocationPattern/ ) {
          foreach my $master_spot (@master_spots) {
            $spotLocationSection .= $co->start_table ({-border=>0}).$co->start_Tr();
            $spotLocationSection .= $lastMasterSpotLocation{$master_spot};
            $spotLocationSection .= $lastMasterSpotLocationData{$master_spot} if $lastMasterSpotLocationDataFound{$master_spot};
            $spotLocationSection .= $co->end_Tr.$co->end_table;
          }
          $two_d =~ s/$insertLastMasterSpotLocationPattern/$spotLocationSection/;
          undef($spotLocationSection);
          undef(%lastMasterSpotLocationDataFound);
        }
        
        if ($next_line_keyword !~ /^\dD/) {
          if ($msms_many_values and !$expandMSMSbuttonShowed) {
            $two_d .= $two_d_topic_ExpandMSMSButton.$co->br;
            $msms_many_values = 0;
          }
          if ($swiss_2d_page && $two_d_cc =~ /$cross_species\s*\(\s*(?:$main_index);\s*(([^;]+);\s*([^;]+)?;?\s*([^;]+)?);?\)/i) {
            my $lien =  &get_href({database=>$main_index, IDs=>[$2], UniProt=>$adding_external_table});
            my $cross_link = $co->span({-class=>'red'}, "$cross_species ".$co->span({-class=>'blue'}, "(".$co->a({-href=>$lien}, $1).")"));
            $two_d_cc =~ s/$cross_species[^\n]+/$cross_link/;
          }
          $nice_print .= $two_d_cc."\n";
#          $nice_print .= $co->Tr({-bgcolor=>"$cellcol"}).$co->start_td({-colspan=>4})."\n";
          my $pi_tool_lien = &get_href({database=>'ExPASy_pI_tool', IDs=>[$swiss_prot_ref], UniProt=>$adding_external_table});
#          $nice_print .= $co->a({-href=>"$pi_tool_lien"}, 'Compute the theoretical pI/Mw').$co->br."\n" if $swiss_prot_ref;
#          $nice_print .= $co->a({-href=>"$main::expasy/ch2d/expl-gifs.html"}, 'How to interpret a protein')."\n".$co->end_td.$co->end_Tr."\n";
          $two_d      .= "\n".$co->end_td.$co->end_Tr;
#          $two_d      .= "\n".$co->br.$co->end_td.$co->end_Tr;
          $two_d_all  .= $two_d;
          # $two_d_all   = $co->end_table."\n".$co->start_table({-border=>0, -bgcolor=>"$tablecol", -width=>"100%"})."\n".$two_d_all."\n";
          $two_d_cc = $two_d = $masters_header = undef;
        }

      }

      elsif ($pre eq "OG") {
        $og = $rest;
        $og =~ s/\W\s*$//;
        $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, 'Encoded on').
                               $co->td({-bgcolor=>"$cellcol", -colspan=>3}, "$og"))."\n";
      }

      elsif ($pre eq "KW") {
        $kw.= $rest;
        if ($next_line_keyword ne 'KW') {
          $kw =~ s/\.\s*$//;
          my @kw = split ';', $kw;
          undef($kw);
          map { $_ =~ s/(?:(?:^\s+|\s+$))//g } @kw;
          foreach my $kw_function (@kw) {
          $kw .= $co->a({-href=>"$main::expasy/cgi-bin/get-entries?KW=$kw_function"}, $kw_function).'; '
          }
          $kw =~ s/;\s*$/./;
          $nice_print .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, 'Keywords').
                                 $co->td({-bgcolor=>"$cellcol", -colspan=>3}, "$kw"))."\n";
        }
        else { $kw.= " " }
      }

      elsif ($pre eq "DR") {
        if ($first_dr) {
          undef($first_dr);
          $dr = $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({class=>"LightColumn"}, 'Cross-references')))."\n";
        }
        $rest =~ /^([^;]+);\s*([^;]+);?\s*([^;]+)?;?\s*([^;]+)?;?\s*([^;]+)?/;
        my $dr_db  = $1;
        my $dr_1id = $2;
        my $dr_2id = $3;
        my $dr_3id = $4;
        my $dr_4id = $5;
        my $dr_links = $dr_1id;
           $dr_links .= "; $dr_2id" if $dr_2id;
        my $dr_remains;
           $dr_remains  = "; $dr_3id" if $dr_3id;
           $dr_remains .= "; $dr_4id" if $dr_4id;

        my $lien = &get_href({database=>$dr_db, IDs=>[$dr_1id, $dr_2id, $dr_3id],
                              UniProt=>($adding_external_table or $EXTERNAL::update_from_Make2DDB)});

        $dr_links = $co->a({-href=>"$lien"}, "$dr_links") if $lien;
        $dr .= $co->Tr($co->td({-bgcolor=>"$cellcol"}, "$dr_db").
                               $co->td({-bgcolor=>"$cellcol", -colspan=>3}, $dr_links.$dr_remains))."\n";
        $AChasMainIndex = $dr_1id if $dr_db =~ /(?:UniProt(?:KB)?)|Swiss-?Prot|TrEMBL/i;
        $AChasMainIndex = $dr_1id if $dr_db =~ /SWISS-?2DPAGE/i and !$AChasMainIndex;
      }


    # 1/2D, Copyright and blocks are only concatenated at the end of each entry

    } # end each $ii ($line)


    $nice_print .= $two_d_all;
    if ($copyright_text =~ /./) {
      chomp($copyright_text);
      $copyright_text =~ s/\n/ /mg; # not a <br>
      $copyright_text =~ s/^\s+//;
      $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"}, $co->strong({class=>"LightColumn"}, 'Copyright')))."\n";
      i $copyright_text =~ s/\n(.)/\nCC   $1/g;
      $nice_print .= $co->Tr($co->td({-colspan=>4, -bgcolor=>"$cellcol"}, $co->span({class=>'small'},"$copyright_text")))."\n";
    }
    $nice_print .= $dr;


    # External or SWISS-2DPAGE links to compute map
    while (($main::show_external_data and $EXTERNAL::include_links_to_compute_map and $AChasMainIndex) or $swiss_2d_page) {
      last if defined($adding_external_ch2d_maps);
      $adding_external_ch2d_maps  = defined;
      my @gelComputedDynamic;
      unless ($swiss_2d_page) {
        $cellcol = $cellcol_external;
        $cellcol_b =$cellcol_external;
        $cellcol_contrasted = $cellcol_contrasted_external;
      }
      my $tr_header = ($swiss_2d_page)? '2D PAGE maps for unidentified proteins' : 
                                        'External 2D data: Estimated location of the protein on various 2D PAGE maps';
      $adding_external_ch2d_maps  = $co->Tr($co->td({-colspan=>4, -bgcolor=>"$titcol"},
        $co->strong({class=>"LightColumn"}, $tr_header)))."\n";     
      $adding_external_ch2d_maps .= 
                             $co->Tr($co->td({-colspan=>4, -bgcolor=>"$cellcol"},
                             $co->ul(
                             $co->li($co->a({-href=>"$ch2d_expl_gif_page"}, 'How to interpret a protein map'))."\n".
                             $co->li('You may obtain an estimated location of the protein on various 2D PAGE maps, '.
                                            'provided the whole amino acid sequence is known. The estimation '.
                                            'is obtained according to the computed protein\'s pI and Mw.')."\n".
                             $co->li($co->strong('Warning 1').': the displayed region reflects '."\n".
                                    'an area around the theoretical pI and molecular weight of the protein and is only '.
                                    'provided for the user\'s information. '."\n".
                                    'It should be used with caution, as the experimental and theoretical positions of a '.
                                    'protein may differ significantly. ')."\n".
                             $co->li($co->strong(' Warning 2').': the 2D PAGE map is built on demand. This may take some '.
                                            'few seconds to be computed.')
                             )))."\n";

      my ($other_masters, $cell, $last_species, $not_first_species);
      if (!$swiss_2d_page) {
        @gelComputedDynamic = EXECUTE_FAST_COMMAND("SELECT databaseName, gelShortName, gelFullName, organismSpecies,".
                                                   "gelComputableUrl FROM GelComputableDynamic ".
                                                   "ORDER BY databaseName, organismSpecies");
        unless (scalar @gelComputedDynamic) {
          $adding_external_ch2d_maps = defined;
          last;
        }
        for (my $ii = 0; $ii < scalar (@gelComputedDynamic); $ii+=5) {
          my $databaseName = $gelComputedDynamic[$ii];
          my $gelShortName = $gelComputedDynamic[($ii+1)];
          my $gelFullName = $gelComputedDynamic[($ii+2)];
          my $species = $gelComputedDynamic[($ii+3)];
          my $gelComputedUrl = $gelComputedDynamic[($ii+4)];
          $gelComputedUrl =~ s/^http\:\/\/(\w+\.)?expasy\.org/$main::expasy/i;
          $gelComputedUrl =~ s/__MAP__/$gelShortName/;
          $gelComputedUrl =~ s/__AC__/$AChasMainIndex/;
          if ($species ne $last_species) {
           (my $species_br = $species) =~ s/\s+\(/<br>\(/;
            $other_masters .= $co->end_Tr.$co->end_table.$co->end_td.$co->end_Tr."\n" if $not_first_species;
            $other_masters .= $co->start_Tr({-bgcolor=>"$cellcol"}).$co->td({class=>"center"},
              $co->br.$co->strong({class=>'underlined'}, $databaseName).$co->strong(':').$co->br.$co->strong($co->em("$species_br")))."\n";
            $other_masters .= $co->start_td({-colspan=>3}).$co->start_table({-width=>"100%",
              -bgcolor=>"$cellcol_contrasted"}).$co->start_Tr()."\n";
            $last_species = $species;
            $not_first_species = 1;
            $cell = 0;
          }
          my $gelName = ($gelFullName =~ /\w/)? $gelFullName : $gelShortName;
          $gelName =~ s/(?:\<\/?i\>)//gi; #/
          $image_link = $co->ul({class=>'compact'}, $co->li($co->a({href => "$gelComputedUrl", style=>$link_style}, $gelName)))."\n";
#          $image_link = $co->ul({class=>'compact'}, $co->li($co->a({href => "$gelComputedUrl"}, $gelName)))."\n";
          $cell++;
          $other_masters .= $co->end_Tr.$co->start_Tr()."\n" if $cell % 3 == 1 && $cell > 2;
          $other_masters .= $co->start_td({-align=>'left'}).$image_link.$co->end_td."\n";
        }
      }
      else { # for the moment, only SWISS-2DPAGE related compute-map program is available
        my %image_names = &get_mapindex("species");
        unless (scalar %image_names) {
          $adding_external_ch2d_maps = defined;
          last;
        }
        foreach my $image (sort(keys %image_names)) {
         (my $image_no_species = $image) =~ s/^(.+)__SPECIES__//i;
          my $species = $1;
          next if $masters_concat =~ /__$image_no_species\__/;
          if ($species ne $last_species) {
           (my $species_br = $species) =~ s/\s+\(/<br>\(/;
            $other_masters .= $co->end_Tr.$co->end_table.$co->end_td.$co->end_Tr."\n" if $not_first_species;
            $other_masters .= $co->start_Tr({-bgcolor=>"$cellcol"}).$co->td({class=>"center"},
              $co->strong($co->em("$species_br")))."\n";
            $other_masters .= $co->start_td({-colspan=>3}).$co->start_table({-width=>"100%",
              -bgcolor=>"$cellcol_contrasted"}).$co->start_Tr()."\n";
            $last_species = $species;
            $not_first_species = 1;
            $cell = 0;
          }
          $image_src = "$sgifs/$image_no_species.$small_image_type";
          if ($main::extract_image_URL_from_DB) {
           ($image_src) = EXECUTE_FAST_COMMAND(
            "SELECT GelImage.smallImageUrl FROM GelImage, Gel ".
            "WHERE GelImage.gelID = Gel.gelID AND lower(Gel.shortName) = '".lc($image_no_species)."' LIMIT 1 "
            );
          }
          my $cipro_id = $entry_ac;
	  if($cipro_id =~ m/CIPRO|KH|PROCITS/){
	    if($cipro_id =~ m/CIPRO/){
	      $cipro_id =~ s/CIPRO_//;
	      $cipro_id =~ s/_/./g;
	      $image_link = $co->img({align=>"top" ,height=>100 , hspace=>3, vspace=>3,
                src =>"http://cipro5.ibio.jp/~yonezawa/displayGelImage.cgi?id=".$cipro_id."&gel=".$master, alt=>"$image_no_species"});
	    } elsif($cipro_id =~ m/KH/) {
	      $cipro_id =~ s/_/./g;
	      $image_link = $co->img({align=>"top" ,height=>100 , hspace=>3, vspace=>3,
                src =>"http://cipro5.ibio.jp/~yonezawa/displayGelImage.cgi?id=".$cipro_id."&gel=".uc(param('map')), alt=>"$image_no_species"});
	    } else {
	      $image_link = $co->img({align=>"top" ,height=>100 , hspace=>3, vspace=>3,
                src =>"http://cipro5.ibio.jp/~yonezawa/displayGelImage.cgi?id=".$cipro_id."&gel=".$master, alt=>"$image_no_species"});              
	    }
	  } else {
	    $image_link = $co->img({align=>"top" ,height=>100 , hspace=>3, vspace=>3,
              src =>"$image_src", alt=>"$image_no_species"});
	  }
          $image_link = $co->a({href => "$ch2d_compute_map$image_no_species,$entry_ac"}, $image_link);
          $image_link = $image_names{$image}."\n".$co->br.$image_link."\n";
          $cell++;
          $other_masters .= $co->end_Tr.$co->start_Tr()."\n" if $cell % 3 == 1 && $cell > 2;
          $other_masters .= $co->start_td.$image_link.$co->end_td."\n";
        }
      }

      $other_masters .= $co->end_Tr.$co->end_table.$co->end_td.$co->end_Tr."\n" if $last_species;
      $adding_external_ch2d_maps .= $other_masters;

      if ($swiss_2d_page) {
        $nice_print .= $adding_external_ch2d_maps;
        $adding_external_ch2d_maps = defined;
      }

      $cellcol = $cellcol_normal;
      $cellcol_b =$cellcol_normal;
      $cellcol_contrasted = $cellcol_contrasted_normal;

      last;
    }


    $nice_print .= $co->end_table."\n";

    $nice_print .= $co->br. $co->br;



    if ($main::show_external_data and !$adding_external_table) {

      $add_external_table = 1;

      if ($adding_external_ch2d_maps and !$swiss_2d_page) {
        $adding_external_2d .= $adding_external_ch2d_maps;
        $adding_external_ch2d_maps = defined;
      }

      # Add here any other related external 2D blocks:
      #if (($main::show_external_data and $EXTERNAL::include_links_to_some_2d_adds and $AChasMainIndex) {
      #  if ($adding_external_some_2d_adds) {
      #    $adding_external_2d .= $adding_external_some_2d_adds;
      #    $adding_external_some_2d_adds = defined;
      #  }
      #}

      goto ADD_EXTERNAL_TABLE unless $adding_external_table ;
    }

    undef($adding_external_table);

  } # end each $ac


  undef($co);
  return $nice_print;

}


#================================================================================================#
#================================================================================================#


# Format map_protein_list for display

sub PRINT_MAP_LIST {

  use CGI qw (-no_xhtml);
  my $co = new CGI;

  my $print_map_list;

  my ($map_name, $query_GN, $query_DE, $query_spots, $query_AC, $query_ID, $query_pi, $query_mw, $query_volume, $query_od,
      $query_fragment, $query_topics, $query_mapping, $query_results, $query_ref, $order_by) = @_;
  undef $query_pi unless @{$query_pi};
  my $query_AC_ID;
  my $query_mapping_raw_copy = $query_mapping;

  my (@head, @rows, @last_row, @spots_block, @pi_block, @mw_block, @volume_block, @od_block, @mapping_block, @topics_block, @results_block, @ref_block);

  my $matchs_num_rows = @{$query_spots};

  my ($matchs_num_spot, %encounteredSpot);
  foreach my $spot (@{$query_spots}) {
    $matchs_num_spot++ unless defined($encounteredSpot{$spot});
    $encounteredSpot{$spot} = defined
  } undef(%encounteredSpot);

  my ($block_counter, $row_bgcolor, @refs, %refs, $ref_id, $refs_counter);

  my ($spot_ref, %add_legend_result_techniques);
  for (my $ii = 0; $ii < scalar @{$query_mapping}; $ii++) {
    $spot_ref = ${$query_spots}[$ii];
    if (${$query_mapping}[$ii] =~ /\bPMF\b/i and ${$query_results}[$ii] =~ /\bPMF\b/i) {
      ${$query_mapping}[$ii] =~ s/(\bPMF\b)/$co->a({href=>"$main::script_name$main::script_name_qmark".
         "spot=$map_name:$spot_ref\&data=pmf$DB::ArgumentCurrentDatabaseNameURI"},$1)/ei; #\//;
      $add_legend_result_techniques{'PMF'} = 1;
    }
    if (${$query_mapping}[$ii] =~ /\bMS\/MS\b/i and ${$query_results}[$ii] =~ /\bMS\/MS\b/i) {
      ${$query_mapping}[$ii] =~ s/(\bMS\/MS\b)/$co->a({href=>"$main::script_name$main::script_name_qmark".
         "spot=$map_name:$spot_ref\&data=msms$DB::ArgumentCurrentDatabaseNameURI"},$1)/ei; #\//;
      $add_legend_result_techniques{'MS/MS'} = 1;
    }
    if (${$query_mapping}[$ii] =~ /\bAa\b/i and ${$query_results}[$ii] =~ /\bAa\b/i) {
      ${$query_mapping}[$ii] =~ s/(\bAa\b)/$co->a({href=>"$main::script_name$main::script_name_qmark".
         "spot=$map_name:$spot_ref\&data=aa$DB::ArgumentCurrentDatabaseNameURI"},$1)/ei; #\//;
      $add_legend_result_techniques{'Aa'} = 1;
    }
  }
  map { $_ =~ s/\s*\}\s*\{\s*/, /g } @{$query_mapping};  
  map { $_ =~ s/(?:\W*\s+.+|\W+)$//; $_ =~ s/^\s*\W+// } @{$query_GN};
  map { $_ =~ s/NAME\=//i } @{$query_GN};

  my @query_topics_copy = @{$query_topics};
  map { $_ =~ s/(?:,\s*)?MAPPING//ig } @query_topics_copy;
  my $query_topics_not_void = grep { $_ =~ /\w/ } @query_topics_copy;

  for (my $i=0; $i<=$matchs_num_rows; $i++) { 

    ${$query_spots}[$i]  = $co->a({href=>"JavaScript:OpenViewerForSpot('${$query_spots}[$i]', '$map_name', '$DB::CurrentDatabase')"}, ${$query_spots}[$i]);

    if (${$query_topics}[$i]) {
      ${$query_topics}[$i] =~ s/(?:,\s*)?MAPPING//ig;
      ${$query_topics}[$i] = ucfirst(lc(${$query_topics}[$i]));
    }
    unless (length(${$query_topics}[$i])) {# (${$query_topics}[$i])
      ${$query_topics}[$i] = "-";
    }

    ${$query_pi}[$i]     =  $co->span({-class=>'brown'}, ${$query_pi}[$i]) if defined($query_pi);
    ${$query_mw}[$i]     =  $co->span({-class=>'brown'}, ${$query_mw}[$i]);
    ${$query_mw}[$i]    .= " (F)" if ${$query_fragment}[$i] eq 't';

    if (${$query_volume}[$i] and ${$query_volume}[$i] =~/^\d*\.?\d*$/) {
      ${$query_volume}[$i] = sprintf "%3.4f%%", ${$query_volume}[$i];
    }
    else {
      ${$query_volume}[$i] =  "N/A";
    }
    ${$query_volume}[$i] =  $co->span({-class=>'brown'}, ${$query_volume}[$i]);

    if (${$query_od}[$i] and ${$query_od}[$i] =~/^\d*\.?\d*$/) {
      ${$query_od}[$i] = sprintf "%3.4f%%", ${$query_od}[$i];
    }
    else {
      ${$query_od}[$i] =  "N/A";
    }
    ${$query_od}[$i] =  $co->span({-class=>'brown'}, ${$query_od}[$i]);

    @refs = sort(split ", ", ${$query_ref}[$i]);
    undef(${$query_ref}[$i]);
    foreach $ref_id (@refs) {
      unless($refs{$ref_id}) {
        $refs{$ref_id} = ++$refs_counter;
      }
      ${$query_ref}[$i] .= $co->a({href=>"#$refs{$ref_id}"}, $refs{$ref_id}).", ";
    }
    ${$query_ref}[$i] =~ s/, $//;

    $row_bgcolor = ($block_counter % 2)? '#f0e68c':'#fffafa';
    push ( @spots_block,   td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_spots}[$i])])."\n" );
    push ( @pi_block,      td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_pi}[$i])])."\n" ) if defined($query_pi);
    push ( @mw_block,      td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_mw}[$i])])."\n" );
    push ( @volume_block,  td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_volume}[$i])])."\n" );
    push ( @od_block,      td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_od}[$i])])."\n" );
    push ( @mapping_block, td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_mapping}[$i])])."\n" );
    push ( @topics_block,  td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_topics}[$i])])."\n" );
    push ( @results_block, td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_results}[$i])])."\n" );
    push ( @ref_block,     td({-bgcolor=>"$row_bgcolor", -nowrap},[(${$query_ref}[$i])])."\n" );

    next if ${$query_AC}[$i+1] eq  ${$query_AC}[$i];
    $block_counter++;

    ${$query_AC}[$i] = $co->a({href=>"$main::script_name$main::script_name_qmark"."ac=${$query_AC}[$i]$DB::ArgumentCurrentDatabaseNameURI"}, ${$query_AC}[$i]);
#    ${$query_ID}[$i] = $co->a({href=>"$main::script_name$main::script_name_qmark"."ac=${$query_ID}[$i]$DB::ArgumentCurrentDatabaseNameURI"}, ${$query_ID}[$i]);


    ${$query_spots}[$i]   = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@spots_block)
                                 )."\n";
    ${$query_pi}[$i]      = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@pi_block)
                                 )."\n" if defined($query_pi);
    ${$query_mw}[$i]      = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@mw_block)
                                 )."\n";
    ${$query_volume}[$i]  = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@volume_block)
                                 )."\n" if $MAP::include_spot_volume;
    ${$query_od}[$i]      = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@od_block)
                                 )."\n" if $MAP::include_spot_volume;
    ${$query_mapping}[$i] = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@mapping_block)
                                 )."\n";
    ${$query_topics}[$i]  = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@topics_block)
                                 )."\n";
    ${$query_results}[$i] = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@results_block)
                                 )."\n";
    ${$query_ref}[$i]     = $co->table({-border=>$main::table_border, -align=>'center'},
                                 TR({class=>'center', -valign=>'top'},\@ref_block)
                                 )."\n";
    ${$query_GN}[$i]      = '-' if !${$query_GN}[$i]; #eq '';

    $query_AC_ID = ${$query_AC}[$i];
#    $query_AC_ID = ${$query_AC}[$i].$co->br."(".${$query_ID}[$i].")";



    @last_row = (${$query_GN}[$i], ${$query_ID}[$i], $query_AC_ID, ${$query_spots}[$i]);
    push @last_row,  ${$query_pi}[$i] if defined($query_pi);
    push @last_row, (${$query_mw}[$i], ${$query_mapping}[$i]);
    push @last_row,  ${$query_topics}[$i] if $query_topics_not_void;
    push @last_row,  ${$query_volume}[$i] if $MAP::include_spot_volume;
    push @last_row,  ${$query_od}[$i] if $MAP::include_spot_volume;
    push @last_row,  ${$query_ref}[$i];


    push (@rows, td({-bgcolor=>"$row_bgcolor"},[@last_row])."\n");

    @spots_block = @pi_block = @mw_block = @volume_block = @od_block = @topics_block = @mapping_block = @results_block = @ref_block = ();

  }


  # Add headers
  my   @headers = ('Gene Name', 'Description', 'Accession&nbsp;n&#176;', 'Spot ID');
  push @headers,  ('Exp <em>pI</em>') if defined($query_pi);
  push @headers,  ('Exp <em>Mw</em>', 'Mapping Methods');
  push @headers,  ('Comment Topics') if $query_topics_not_void;
  push @headers,  ('%Vol', '%od') if $MAP::include_spot_volume;
  push @headers,  ('References');

  for (my $i = 0; $i < @headers; $i++) {
    $head[$i] = $co->strong($headers[$i]);
  }

  unshift @rows, th({-class=>'center', -bgcolor => 'lightblue' }, \@head);  


  # Query Mapping Legends

  my $mappindMethodData = '';
  my $mappingLegendCommand = 
    "SELECT DISTINCT SpotEntryMappingTopic.mappingtechnique".
    " FROM SpotEntryMappingTopic, Gel WHERE lower(Gel.shortName) = '".lc($map_name)."' AND SpotEntryMappingTopic.gelid = Gel.gelID";
  my @gelMappingMethods = EXECUTE_FAST_COMMAND("$mappingLegendCommand");
  $mappingLegendCommand = "SELECT MappingTopicDefinition.mappingTechnique, MappingTopicDefinition.techniqueDescription".
    " FROM MappingTopicDefinition";
  my @allMappingMethods = EXECUTE_FAST_COMMAND("$mappingLegendCommand");
  my %allMappingMethods = my %alreadyCitedMappingMethods = ();
  for (my $ii = 0; $ii < scalar(@allMappingMethods); $ii+=2) {
    $allMappingMethods{$allMappingMethods[$ii]} = $allMappingMethods[$ii+1];
  }
  my $gelMappingMethods = '';
  for (my $ii = 0; $ii < scalar(@gelMappingMethods); $ii++) {
    $gelMappingMethods[$ii]->[0] =~ s/^{|}$//g;
    foreach my $gelMappingMethodsAtomic (split ",", $gelMappingMethods[$ii]->[0]) {
      next if $alreadyCitedMappingMethods{$gelMappingMethodsAtomic};
      $mappindMethodData .= $co->strong('{&nbsp;'.$gelMappingMethodsAtomic.'&nbsp;}').'&nbsp;=>&nbsp;\''.$allMappingMethods{$gelMappingMethodsAtomic}.'\', ';
      $alreadyCitedMappingMethods{$gelMappingMethodsAtomic} = 1;
    }
  }
  if ($mappindMethodData) {
      ($mappindMethodData = $co->strong({class=>'Estomped'}, 'Mapping legends: ').$mappindMethodData) =~ s/,\s+$//;
      push (@rows, $co->td({-colspan=>$#head+1, -bgcolor=>'lightblue'},''));
      push (@rows, $co->td({-colspan=>$#head+1, -bgcolor=>'lightblue'},$mappindMethodData));
  }


  # Print main Map table
  my $displaySpots = ($matchs_num_spot > 1)? 'spots' : 'spot';
  my $displayEntries = ($block_counter > 1)? 'entries' : 'entry';
  $print_map_list.= $co->br."\n";
  $print_map_list.= $co->table({-border=>($main::table_border*0)+15, -width=>'90%', -bgcolor=>'#fffafa'},
                         caption($co->span({-class=>'underlined H3Font'}, "Protein list for $map_name"), $co->span({-class=>'Result H4Font'}, "&nbsp;&nbsp;$matchs_num_spot"),
                           $co->span({-class=>'H4Font'}, " identified $displaySpots / "), $co->span({-class=>'Result H4Font'},
                           "$block_counter"), $co->span({-class=>'H4Font'}, " protein $displayEntries"), $co->br, $co->br ), "\n",
                         TR({class=>'center', -valign=>'middle'},\@rows)
                         ).$co->br."\n";


  # Print a separated Legend table - desactivated

  if (0 and $mappindMethodData) {
    $print_map_list.= &EXECUTE_FORMATTED_COMMAND(
      {command=>$mappingLegendCommand, table_width=>'40%', no_color_alternation=>1, no_table_caption=>1, table_alignment_general=>'left', do_not_print=>1}
     );
  }

  # Extract References

  my ($command, $ref_number, $ref_medline, $ref_authors, $ref_location, $lien);

  return $print_map_list unless %refs;

  $command = " SELECT referenceID, crossReferences, authors, referenceLocation FROM ViewRef WHERE";
  foreach $ref_id (keys %refs) {
    $command.= " referenceID = $ref_id OR";
  }
  $command =~ s/OR$//;

  my @list = EXECUTE_FAST_COMMAND($command);

  @headers = ('#', 'PubMed/Medline', 'Authors', 'Location');
  undef(@head);
  undef(@rows);

  my (%ref_row);

  for (my $i=0; $i < @headers; $i++) {
    $head[$i] = $co->strong($headers[$i]);
  }

  @rows = th({ -bgcolor => 'lightblue', class=>'center' }, \@head);

  while ( ($ref_id, $ref_medline, $ref_authors, $ref_location, @list) = @list ) {

    $ref_number = $co->a({-name=>"$refs{$ref_id}"},'') ."[".$refs{$ref_id}."]";
    if ($ref_medline =~ /PubMed\=(\d+)/i) {
      $ref_medline = $1;
      $lien = &get_href({database=>'MEDLINE', IDs=>[$ref_medline]});
      $ref_medline = $co->a({href=>"$lien"}, $ref_medline);
    }
    elsif ($ref_medline =~ /MEDLINE\=(\d+)/) {
      $ref_medline = $1;
      $lien = &get_href({database=>'MEDLINE', IDs=>[$ref_medline]});
      $ref_medline = $co->a({href=>"$lien"}, $ref_medline);
    }
    else {
      $ref_medline = '-';
    }

    $ref_row{$refs{$ref_id}} = [$ref_number, $ref_medline, $ref_authors, $ref_location];
  }

  # pushing references into table sorted
  my $row_counter;
  for (my $ii = 1; $ii <= $refs_counter; $ii++) {
    $row_bgcolor = ($row_counter % 2)? '#f0e68c':'#fffafa';
    $row_counter++;
    push (@rows, td({-class => 'center', -bgcolor=>"$row_bgcolor"}, [@{$ref_row{$ii}}])."\n");
  }


  $print_map_list.= $co->br.$co->br."\n";
  $print_map_list.= $co->table({-cellspacing=>0, -border=>$main::table_border, -width=>'90%', -bgcolor=>'#fffafa'},
                         $co->caption({-class=>'H4Font'}, 'Cited&nbsp;references', $co->br, $co->br),"\n",
                         TR({-align=>'left', -valign=>'middle'},\@rows)
                         ).$co->br."\n";

  undef($co);

  # Final Impression

  return {mapListTable=>$print_map_list, mappingMethods=>\$mappindMethodData};

}


#================================================================================================#
#================================================================================================#


# Define, Construct, Execute and Display tables on queries for the combined different field

sub COMBINED_QUERY {

  my $call_for = $_[0]->{callFor};

  my $include_external_data = $_[0]->{include_external_data};

  my $combinedFieldsMaxNumber = $_[0]->{combined_fieldsMaxNumber};

  # define corresponding fields for combined researchs, use '__#__' to separate columns

  my %labels_fields = 
    (
     '1.0. Full Entry' => 'ViewEntry.identifier__#__ViewEntry.id_method__#__ViewEntry.accession_number__#__'.
         'ViewEntry.secondary_identifiers__#__ViewEntry.creation__#__ViewEntry.version_2d__#__ViewEntry.version_general__#__ViewEntry.description__#__'.
         'ViewEntry.genes__#__ViewEntry.organism__#__ViewEntry.organism_classification__#__ViewEntry.taxonomy_cross_reference__#__'.
         'ViewEntry.masters__#__ViewEntry.images__#__ViewEntry.free_comments__#__ViewEntry.reference_lines__#__'.
         'ViewEntry.one_d_comments__#__ViewEntry.one_d_blocks__#__ViewEntry.two_d_comments__#__ViewEntry.two_d_blocks__#__'.
         'ViewEntry.database_cross_reference',
     '2.1. Accession number' => 'Entry.AC', # we also include not primary ACs     
     '2.2. ID +' => 'Entry.ID__#__ExternalMainXrefData.uniProtID',
     '2.3. Description +' => 'Entry.description__#__ExternalMainXrefData.uniProtDescription',
     '2.4. Gene Names +' => 'Entry.geneNames__#__ExternalMainXrefData.uniProtGeneNames', 
     '2.5. ID, Description & Genes +' => 'Entry.ID__#__Entry.description__#__Entry.geneNames__#__'.
                                       'ExternalMainXrefData.uniProtID__#__ExternalMainXrefData.uniProtDescription__#__ExternalMainXrefData.uniProtGeneNames',
     '2.6. EC Code +' => 'EnzymeNomenclature.enzymeCode__#__ExternalMainXrefData.uniProtEnzymeCode',
     '2.5. UniProtKB/Swiss-Prot Keywords +' => 'ExternalMainXrefData.uniProtCategoryKeywords',
     '3.1. Organism Name / Classification (Map)' => 'Organism.organismSpecies__#__Organism.organismClassification',
     '3.2. Organism NCBI TaxId (Map)' => 'Organism.taxonomyCode',
     '4.1. Map Names' => 'ViewEntry.images',
     '4.2. Tissue (Swiss-Prot)' => 'GelTissueSP.tissueSPName', # we don't query directly Tissue table
     '5.1. Authors' => 'ViewRef.authors', # 'authors.author',
     '5.2. Ref. Work' => 'ViewRef.referenceWorkDescription',
     '5.3. Ref. Title' => 'ViewRef.referencetitle',
     '5.4. Ref. Citation/Location' => 'ViewRef.referenceLocation',
     '5.5. PubMed/Medline Number' => 'ViewRef.crossReferences',
     '6.1. Free Comments' => 'ViewEntry.free_comments',
     '7.1. Spot ID' => 'SpotEntry.spotID',
     '7.2. Spot Identification Methods/Results' => 'ViewEntry.one_d_blocks__#__ViewEntry.two_d_blocks',
     '7.3. Spot related Comments' => 'ViewEntry.one_d_comments__#__ViewEntry.two_d_comments',
     '8.1. Xref Database Name +' => 'XrefDBParent.XrefDBName',
     '8.2. Xref AC / Identifier +' => 'EntryXrefDBParent.XrefPrimaryIdentifier__#__EntryXrefDBParent.XrefSecondaryIdentifier'
    );
  # keys with the special '__#__' separator for adds will include extra conditions using AC as extra equality conditions
  # between tables! (if the tables are different, " AND table1.AC = table2.AC ..." will be inserted), cf. 'extra conditions'
  # redefine tables (simple) if external data is to be excluded
   unless ($include_external_data) {
     $labels_fields{'2.2. ID +'} = 'Entry.ID';
     $labels_fields{'2.3. Description +'} = 'Entry.description';
     $labels_fields{'2.4. Gene Names +'} = 'Entry.geneNames'; 
     $labels_fields{'2.5. ID, Description & Genes +'} = 'Entry.ID__#__Entry.description__#__Entry.geneNames';
     $labels_fields{'2.6. EC Code +'} = 'EnzymeNomenclature.enzymeCode'; 
  }

  my %labels_fields_recall_digits;
  foreach my $key (keys %labels_fields) { #!! keys will start with a space..
   (my $key_no_digit = $key) =~ s/^(\d+\.)+//;
    $labels_fields_recall_digits{$key_no_digit} = $key;
  }

  my %labels_output_fields = 
    (
     # full_entries part
     #'Accession number' => 'ViewEntry.accession_number',
     #'ID' => 'ViewEntry.identifier',
     'Description' => 'ViewEntry.description',
     'Gene names' => 'ViewEntry.genes', 
     'Organism common name (entry)' => 'ViewEntry.organism',
     'Organism classification (entry)' => 'ViewEntry.organism_classification',
     'Organism NCBI TaxID (entry)' => 'ViewEntry.taxonomy_cross_reference',
     'Map Names' => 'ViewEntry.images',
     'Free Comments' => 'ViewEntry.free_comments',
     'Spot Identification Methods/Results' => 'ViewEntry.one_d_blocks',
     'Spot related Comments' => 'ViewEntry.one_d_comments',
     'Xref DBs' => 'ViewEntry.database_cross_reference',
     # references Part
     'Authors' => 'ViewRef.authors',
     'Ref. Work' => 'ViewRef.referenceWorkDescription',
     'Ref. Title' => 'ViewRef.referencetitle',
     'Ref. Citation/Location' => 'ViewRef.referenceLocation',
     'PubMed/Medline number' => 'ViewRef.crossReferences',
     # tissue part
     'Tissue (Swiss-Prot)' => 'GelTissueSP.tissueSPName',
     # keywords part
     'Uniprot/Swiss-Prot Keywords' => 'ExternalMainXrefData.uniProtCategoryKeywords',
     # spot part
     'Spot ID' => 'SpotEntry.spotID'
    );


  return %labels_fields if ($call_for eq 'labels_fields');
  return %labels_output_fields if ($call_for eq 'labels_output_fields');



  # OTHERWISE, continue to proceed on query construction and execution #


  # Prepare for Output #

  my $fields_in_full_entries  = 21;
  my $fields_in_reunit_refs   = 7;
  my $fields_in_other_tables  = 2;
  my $fields_for_tissue = my $fields_for_keywords =  my $fields_for_spot = my $fields_for_EC = 1;

  # list here the rank of each field inside its corresponding table:
  my %labels_output_fields_rank_in_table =
    (
     # full_entries part
     'Accession number' => '3',
     'ID' => '1',
     'Description' => '8',
     'Gene names' => '9', 
     'Organism common name (entry)' => '10',
     'Organism classification (entry)' => '11',
     'Organism NCBI TaxID (entry)' => '12',
     'Map Names' => '14',
     'Free Comments' => '15',
     'Spot Identification Methods/Results' => '18+20', # '+'s are only treated for full_entries part
     'Spot related Comments' => '17+19',
     'Xref DBs' => '21',
     # references Part
     'Authors' => '#5',
     'Ref. Work' => '#2',
     'Ref. Title' =>  '#6',
     'Ref. Citation/Location' => '#7',
     'PubMed/Medline number' => '#3',
     # tissue
     'Tissue (Swiss-Prot)' => '##1',
     # keywords
     'Uniprot/Swiss-Prot Keywords' => '###1',
     # spots id
     'Spot ID' => '####1'
    );
  my %labels_output_fields_rank_in_table_copy =   %labels_output_fields_rank_in_table;


  # Procceed

  my $co_combined = ($Combined::co)? $Combined::co : new CGI;
  my ($command);

  my $field1        = $co_combined->param('combined_field1'); $field1 = $labels_fields_recall_digits{$field1};
  my $field2        = $co_combined->param('combined_field2'); $field2 = $labels_fields_recall_digits{$field2};
  my $field3        = $co_combined->param('combined_field3'); $field3 = $labels_fields_recall_digits{$field3};
  my $field4        = $co_combined->param('combined_field4'); $field4 = $labels_fields_recall_digits{$field4};
  my $include_f1    = $co_combined->param('include_field1');
  my $include_f2    = $co_combined->param('include_field2');
  my $include_f3    = $co_combined->param('include_field3');
  my $include_f4    = $co_combined->param('include_field4');
  my $exclude_f1    = $co_combined->param('exclude_field1');
  my $exclude_f2    = $co_combined->param('exclude_field2');
  my $exclude_f3    = $co_combined->param('exclude_field3');
  my $exclude_f4    = $co_combined->param('exclude_field4');
  my $inner_f1      = $co_combined->param('inner_and_or_field1');
  my $inner_f2      = $co_combined->param('inner_and_or_field2');
  my $inner_f3      = $co_combined->param('inner_and_or_field3');
  my $inner_f4      = $co_combined->param('inner_and_or_field4');
  my $outer_f12     = $co_combined->param('outer_and_or_field12');
  my $outer_f34     = $co_combined->param('outer_and_or_field34');
  my $outer_block12 = $co_combined->param('outer_and_or_block12');
  my $date_field    = $co_combined->param('combined_date');
  my $date_since    = $co_combined->param('date_since');
     $date_since    = "01-$1" if $date_since =~ /^\s*(\d+)\s*$/;
     $date_since    = "01-$1-$2" if $date_since =~ /^\s*(\w+)\W(\d+)\s*$/;
   # $date_since    = "$2-$1-$3" if $date_since =~ /^\s*(\d+)\W(\w+)\W(\d+)\s*$/;
  my $date_before   = $co_combined->param('date_before');
     $date_before   = "01-$1" if $date_before =~ /^\s*(\d+)\s*$/;
     $date_before   = "01-$1-$2" if $date_before =~ /^\s*(\w+)\W(\d+)\s*$/;
   # $date_before   = "$2-$1-$3" if $date_before =~ /^\s*(\d+)\W(\w+)\W(\d+)\s*$/;

  my @field         = ($field1, $field2, $field3, $field4);
  my @include       = ($include_f1, $include_f2, $include_f3, $include_f4);
  my @exclude       = ($exclude_f1, $exclude_f2, $exclude_f3, $exclude_f4);
  my @inner         = ($inner_f1, $inner_f2, $inner_f3, $inner_f4);
  my @outer_inside  = ($outer_f12, $outer_f34);
  my @outer_outside = ($outer_block12);

# sent only via POST interfaces since version 2.50.1
  my $exactQuery    = ($co_combined->param('Combined_exact'))? 1 : undef;

  if ($combinedFieldsMaxNumber) { # in the future, replace this hard code by automatic code generation using references!
    my $field5        = $co_combined->param('combined_field5'); $field5 = $labels_fields_recall_digits{$field5};
    my $field6        = $co_combined->param('combined_field6'); $field6 = $labels_fields_recall_digits{$field6};
    my $field7        = $co_combined->param('combined_field7'); $field7 = $labels_fields_recall_digits{$field7};
    my $field8        = $co_combined->param('combined_field8'); $field8 = $labels_fields_recall_digits{$field8};
    my $include_f5    = $co_combined->param('include_field5');
    my $include_f6    = $co_combined->param('include_field6');
    my $include_f7    = $co_combined->param('include_field7');
    my $include_f8    = $co_combined->param('include_field8');
    my $exclude_f5    = $co_combined->param('exclude_field5');
    my $exclude_f6    = $co_combined->param('exclude_field6');
    my $exclude_f7    = $co_combined->param('exclude_field7');
    my $exclude_f8    = $co_combined->param('exclude_field8');
    my $inner_f5      = $co_combined->param('inner_and_or_field5');
    my $inner_f6      = $co_combined->param('inner_and_or_field6');
    my $inner_f7      = $co_combined->param('inner_and_or_field7');
    my $inner_f8      = $co_combined->param('inner_and_or_field8');
    my $outer_f56     = $co_combined->param('outer_and_or_field56');
    my $outer_f78     = $co_combined->param('outer_and_or_field78');
    my $outer_block23 = $co_combined->param('outer_and_or_block23');
    my $outer_block34 = $co_combined->param('outer_and_or_block34');
    push @field, ($field5, $field6, $field7, $field8);
    push @include, ($include_f5, $include_f6, $include_f7, $include_f8);
    push @exclude, ($exclude_f5, $exclude_f6, $exclude_f7, $exclude_f8);
    push @inner, ($inner_f5, $inner_f6, $inner_f7, $inner_f8);
    push @outer_inside, ($outer_f56, $outer_f78);
    push @outer_outside, ($outer_block23, $outer_block34);
  }


  # Outout fields and implicit output fields
  my $output_fields = "Accession number__#__ID__#__".join("__#__", $co_combined->param('combined_output_fields'));
  my %impliedFieldOutput = (
    'Description +' => 'Description',
    'Gene Names +' => 'Gene names',
    'ID, Description & Genes +' => 'Description__#__Gene names',
    'Tissue (Swiss-Prot)' => 'Tissue (Swiss-Prot)',
    'Organism Name / Classification (Map)' => 'Organism common name (entry)__#__Organism classification (entry)',
    'Organism NCBI TaxId (Map)' => 'Organism NCBI TaxID (entry)',
    'UniProtKB/Swiss-Prot Keywords +' => 'Uniprot/Swiss-Prot Keywords',
    'Authors' => 'Authors',
    'PubMed/Medline number' => 'PubMed/Medline number',
    'Ref. Work' => 'Ref. Work',
    'Ref. Title' => 'Ref. Title',
    'Ref. Citation/Location' => 'Ref. Citation/Location',
    'Spot ID' => 'Spot ID',
    'Spot Identification Methods/Results' => 'Spot Identification Methods/Results',
    'Spot related Comments' => 'Spot related Comments',
    'Xref Database Name +' => 'Xref DBs',
    'Xref AC / Identifier +' => 'Xref DBs',
  );
  my @fieldCopy = @field;
  for (my $ii = 0; $ii < scalar(@fieldCopy); $ii++) {
    next unless $include[$ii];
    my $field = $fieldCopy[$ii];
    $field =~ s/^(\d+\.)+\s*//;
    foreach my $impliedFieldOutput (keys %impliedFieldOutput) {
      next unless $field eq $impliedFieldOutput;
      foreach my $impliedFieldOutputElement (split '__#__', $impliedFieldOutput{$impliedFieldOutput}) {
        my $impliedFieldOutputValue = $impliedFieldOutputElement;
        $output_fields .= '__#__'.$impliedFieldOutputValue if $output_fields !~ /__#__$impliedFieldOutputValue/;
      }
    }
  }
  undef(@fieldCopy);

  # Display the user query:
  my ($searchMessage, $subsearchMessage, $some_previous_block, $link_to_previous_field);
  for (my $ii = 0; $ii < scalar @field; $ii++) {
    if (($ii % 2) and ($include[($ii-1)] or $exclude[($ii-1)])) {
      $link_to_previous_field = $co_combined->br."&nbsp;&nbsp;&nbsp;".$outer_inside[int(($ii+1)/2)-1];
    }
    elsif ($ii and ($ii % 2 == 0) and $some_previous_block) {
      $link_to_previous_field = $co_combined->br.$co_combined->br."&nbsp;&nbsp;&nbsp;**&nbsp;".$outer_outside[int(($ii+1)/2)-1]."&nbsp;**".$co_combined->br;
    }
    next unless $include[$ii] or $exclude[$ii];
   (my $fieldMessage = $field[$ii]) =~ s/^\d+\.\d+\.\s+//;
    $subsearchMessage = $co_combined->br."\"".$co_combined->strong($fieldMessage)."\"";
    if ($include[$ii] =~ /\S/) {
      if ($link_to_previous_field) {
        $searchMessage .= $link_to_previous_field;
        undef($link_to_previous_field);
      }
     (my $includeMessage = $include[$ii]) =~ s/^\s+//;
      if ($inner[$ii] eq 'OR') {$includeMessage =~ s/\s+/\' or \'/g }
      elsif ($inner[$ii] eq 'AND') {$includeMessage =~ s/\s+/\' and \'/g }
      $subsearchMessage .= ($exactQuery? ' matching ' : ' containing ')."{'".$includeMessage."'}";
      $some_previous_block = 1;
    }
    if ($exclude[$ii] =~ /\S/) {
      if ($link_to_previous_field) {
        $searchMessage .= $link_to_previous_field;
        undef($link_to_previous_field);
      }
     (my $excludeMessage = $exclude[$ii]) =~ s/^\s+//;
      if ($inner[$ii] eq 'OR') {$excludeMessage =~ s/\s+/\' or \'/g }
      elsif ($inner[$ii] eq 'AND') {$excludeMessage =~ s/\s+/\' and \'/g }
      $subsearchMessage .= (($include[$ii])? " and" : '') . " not containing {'".$excludeMessage."'}";
      $some_previous_block = 1;
    }
    $searchMessage .= $subsearchMessage;
  }
  if ($date_since or $date_before) {$searchMessage .= $co_combined->br.$co_combined->span({class=>'underlined'}, $co_combined->param('combined_date'))}
  if ($date_since) { $searchMessage .= $co_combined->br.'since: '.$co_combined->param('date_since')};
  if ($date_before) { $searchMessage .= $co_combined->br.'before: '.$co_combined->param('date_before')};
  print IntoCenteredColoredCell($searchMessage, '#DDDDDD').$co_combined->p.$co_combined->br;



  my $blocks_number    = ($combinedFieldsMaxNumber)? 4 : ($main::blocks_number)? $main::blocks_number : 2;
  my $fields_per_block = ($main::fields_per_block)? $main::fields_per_block : 2; 
  my $number_of_fields = $#field;

  my @output_fields    = split "__#__", $output_fields;
  my ($output_flag_refs, $output_flag_keywords, $output_flag_tissue, $output_flag_spot);
  foreach (@output_fields) {
    $output_flag_refs     = 1 if /author|Ref\.|Medline|PubMed/i;
    $output_flag_tissue   = 1 if /tissue/i;
    $output_flag_keywords = 1 if /Keyword/i;
    $output_flag_spot     = 1 if /spot/i;
  }


  my (@include_query, @exclude_query, $includes_consolidate);


  for (my $ii = 0; $ii <= $number_of_fields; $ii++) {
    undef $include[$ii] unless $include[$ii] =~ /\w/;
    undef $exclude[$ii] unless $exclude[$ii] =~ /\w/;
    return -1 if ($include[$ii] and $include[$ii] !~ /\w\S*\w/ and $exclude[$ii] !~ /\w\S*\w/)
              or ($include[$ii] =~ /(?:^| )\w(?: |$)/ and $inner[$ii] =~ 'OR');
    return -2 if (($field[$ii] =~ /\bNCBI TaxId\b/i or $field[$ii] =~ /\bPubMed\/Medline number\b/i)
                   and ($include[$ii] =~ /[^\d|\s]/ or $exclude[$ii] =~ /[^\d|\s]/));
    $include[$ii] = '*' if !$include[$ii] and $exclude[$ii];
    $include_query[$ii] = $include[$ii];
    $exclude_query[$ii] = $exclude[$ii];
    $include_query[$ii] =~ s/(?:^\s+|\s+$)//g;
    $exclude_query[$ii] =~ s/(?:^\s+|\s+$)//g;
    $include_query[$ii] =~ s/\s+/__SEP__/mg unless $inner[$ii] eq 'adjacent'; 
    $exclude_query[$ii] =~ s/\s+/__SEP__/mg unless $inner[$ii] eq 'adjacent';  

    # Escape in/ex fields for postgreSQL regexp then restore '*'->'.*', '?'->'.' and spaces
   ($include_query[$ii] = quotemeta(quotemeta($include_query[$ii]))) =~ s/(?:\\\\\\\*)+/[a-z0-9]\*/g;  
   ($exclude_query[$ii] = quotemeta(quotemeta($exclude_query[$ii]))) =~ s/(?:\\\\\\\*)+/[a-z0-9]\*/g;
    $include_query[$ii] =~ s/(?:\\\\\\\?)/./g;
    $exclude_query[$ii] =~ s/(?:\\\\\\\?)/./g; 
    $include_query[$ii] =~ s/(?:\\\\\\\ )/ /g; # leave spaces unescaped for 'adjacent'
    $exclude_query[$ii] =~ s/(?:\\\\\\\ )/ /g;

    $includes_consolidate .= $include[$ii];
  }
  return -1 unless ($includes_consolidate or $date_since or $date_before);


  # The DATE query

  foreach my $date_string ($date_since, $date_before) {
    next unless $date_string;
    $command = "SET DATESTYLE TO 'POSTGRES, EUROPEAN'; select date_part('year', '$date_string'::DATE)";
    (my $verify_date) = EXECUTE_FAST_COMMAND($command);
    return -3 if $verify_date !~ /^\d+$/;
  }

  my $date_query;
  if ($date_since or $date_before) { # Filter ACs through the Date filter
    my ($creation, $date_entry_table, $date_field_column, $date_since_relversion, $date_before_relversion) = ();;

    if ($date_field eq 'Creation Date') {
      $creation = 1;
      $date_entry_table = 'Entry';
      $date_field_column = 'releaseCreation';
    }
    else {
      $date_entry_table = ($date_field eq 'Last 2D modification')? 'EntryVersion2D' : 'EntryVersionGeneral';
      $date_field_column = 'versionDate';
    }

    if ($date_since) {
      $command = ($creation)? "Select min(releaseNum) from Release where releaseDate >= '$date_since'::DATE"
                 :            "Select '\\''||min($date_field_column)||'\\'' from $date_entry_table where $date_field_column >= '$date_since'::DATE";
      ($date_since_relversion) = EXECUTE_FAST_COMMAND($command);
      return -4 unless defined($date_since_relversion);
    }
    if ($date_before) {
      $command = ($creation)? "Select max(releaseNum) from Release where releaseDate < '$date_before'::DATE"
                 :            "Select '\\''||max($date_field_column)||'\\'' from $date_entry_table where $date_field_column < '$date_before'::DATE";
      ($date_before_relversion) = EXECUTE_FAST_COMMAND($command);
      return -5 unless defined($date_before_relversion);
    }

    $date_query  = " (SELECT $date_entry_table.AC FROM $date_entry_table WHERE";
    $date_query .= " $date_field_column >= $date_since_relversion" if defined($date_since_relversion);
    $date_query .= " AND" if defined($date_since_relversion) and defined($date_before_relversion);
    $date_query .= " $date_field_column <= $date_before_relversion" if defined($date_before_relversion);
    $date_query .= ")";

    $date_query .= " INTERSECT " if $includes_consolidate;
  }


  # Prepare whole query

  my (@include_exclude_query_new, @analyze_label_fields, @inner_include_and, @inner_exclude_and, $analyze_label_field, $analyze_connectivity_flag, $expression);

  for (my $ii = 0; $ii <= $number_of_fields; $ii++) {

    if ($inner[$ii] eq 'OR')     { $include_query[$ii] =~ s/__SEP__/\|/g; $exclude_query[$ii] =~ s/__SEP__/\|/g }
    elsif ($inner[$ii] eq 'AND') { @inner_include_and = split ("__SEP__", $include_query[$ii]); @inner_exclude_and = split ("__SEP__", $exclude_query[$ii]) }

    $include[$ii] = '' if $include[$ii] !~ /\S+/;
    $exclude[$ii] = '' if $exclude[$ii] !~ /\S+/;
    next if (!$include[$ii] and !$exclude[$ii]);

    @analyze_label_fields = split ("__SEP__", $labels_fields{"$field[$ii]"});
    undef($analyze_connectivity_flag);
    $include_exclude_query_new[$ii] = ' (';

    foreach $analyze_label_field (@analyze_label_fields) {

      $include_exclude_query_new[$ii].= " OR (" if $analyze_connectivity_flag;

     # we need another pattern for field separation to replace by 'AND' for exclusion rather than 'OR'
     (my $analyze_label_field_exclude = $analyze_label_field) =~ s/__\#__/__\!\#__/mg;

      if ($inner[$ii] eq 'OR') { 
        $include_exclude_query_new[$ii] =~ s/__SEP__/\|/g;
        $include_exclude_query_new[$ii].= " ($analyze_label_field  ~* '$include_query[$ii]')" if $include[$ii];
        $include_exclude_query_new[$ii].= " AND" if $include[$ii] and $exclude[$ii];
        $include_exclude_query_new[$ii].= " ($analyze_label_field_exclude !~* '$exclude_query[$ii]')" if $exclude[$ii];
      }
      elsif ($inner[$ii] eq 'AND') {
        my @inner_include_and = split ("__SEP__", $include_query[$ii]);
        foreach (@inner_include_and) {
          $include_exclude_query_new[$ii].= " ($analyze_label_field  ~* '$_') AND";
        }
        foreach (@inner_exclude_and) {
          $include_exclude_query_new[$ii].= " ($analyze_label_field_exclude !~* '$_') AND";
        }
        $include_exclude_query_new[$ii] =~ s/AND$//;
      }
      else {
        $include_exclude_query_new[$ii].= " ($analyze_label_field  ~* '$include_query[$ii]')" if $include[$ii];
        $include_exclude_query_new[$ii].= " AND" if $include[$ii] and $exclude[$ii];
        $include_exclude_query_new[$ii].= " ($analyze_label_field_exclude !~* '$exclude_query[$ii]')" if $exclude[$ii];
      }

      $include_exclude_query_new[$ii] .= " ) ";
      $analyze_connectivity_flag = 1;
      $include_exclude_query_new[$ii] =~ s/OR$//;

      while ($include_exclude_query_new[$ii] =~ /__\#__[^~]+[^!](\!?\~\*\s*\'[^\']+\')/) {
        $expression = $1;
        $include_exclude_query_new[$ii] =~ s/__\#__/ $expression OR /;
      }
      while ($include_exclude_query_new[$ii] =~ /__\!\#__[^~]+[^!](\!?\~\*\s*\'[^\']+\')/) {
        $expression = $1;
        $include_exclude_query_new[$ii] =~ s/__\!\#__/ $expression AND /;
      }

    }

  } # end for $ii


  my @field_statement;
  my ($include_exclude_query_new_copy, $additional_first_statement_table, $additional_statement_table, $additional_statement, $additional_statement_initial);  
  for (my $ii = 0; $ii <= $number_of_fields; $ii++) {
    next if (!$include[$ii] and !$exclude[$ii]);
    # extra conditions:    
    undef($additional_statement);    
    $include_exclude_query_new_copy = $include_exclude_query_new[$ii];
    $include_exclude_query_new_copy =~ s/^\W+(\w+)\.\w+//;
    $additional_first_statement_table = $1;
    $additional_statement_initial = " $additional_first_statement_table.AC =";  
    while ($include_exclude_query_new_copy =~ s/(?:OR|AND)\s+(\w+)\.\w+//) {
      $additional_statement_table = $1;
      $additional_statement .= $additional_statement_initial." $additional_statement_table.AC AND"
        if ($additional_first_statement_table ne $additional_statement_table and 
                $additional_statement !~ /$additional_statement_table\.AC/);
    }
    $additional_statement =~ s/AND\s*$//;
    $include_exclude_query_new[$ii] =~ s/^(\s*\(\s*)/($additional_statement AND / if $additional_statement;

    my $fromExT =  ($include_exclude_query_new[$ii] =~ /\bExternalMainXrefData\./i)?
       ', ExternalMainXrefData' : '';

    if ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*Entry\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT Entry.AC FROM Entry $fromExT WHERE (".$include_exclude_query_new[$ii]."))";
      if ($include_exclude_query_new[$ii] =~ /\bAC\b\s*\~\*?/) {
      (my $not_first_ACs_query = $include_exclude_query_new[$ii]) =~ s/\bEntry.AC\b/SecondaryAC.secondaryAC/g;
       $field_statement[$ii] =~ s/\)$//;
       $field_statement[$ii]  .= " UNION SELECT AC from SecondaryAC WHERE (".$not_first_ACs_query."))";
      }
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*Organism\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT EntryGelImage.AC FROM EntryGelImage, Gel, Organism $fromExT WHERE (".$include_exclude_query_new[$ii].")".
                              " AND (EntryGelImage.gelID = Gel.gelID) AND (Gel.organismID = Organism.organismID) )";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*XrefDBParent\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT Entry.AC FROM Entry, EntryXrefDBParent, XrefDBParent $fromExT WHERE ".
                              "(".$include_exclude_query_new[$ii].") AND (Entry.AC = EntryXrefDBPArent.AC) AND ".
                              "(EntryXrefDBPArent.XrefDBCode = XrefDBParent.XrefDBCode) AND ".
                              "(EntryXrefDBParent.activated IS TRUE) )";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*EntryXrefDBParent\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT Entry.AC FROM Entry, $fromExT EntryXrefDBParent WHERE (".$include_exclude_query_new[$ii].")".
                              " AND (Entry.AC = EntryXrefDBParent.AC) AND (EntryXrefDBParent.activated IS TRUE))";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*ExternalMainXrefData\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT Entry.AC FROM Entry, ExternalMainXrefData WHERE (".$include_exclude_query_new[$ii].")".
                              " AND (Entry.AC = ExternalMainXrefData.AC) )";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*GelTissueSP\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT  EntryGelImage.AC FROM EntryGelImage, Gel, GelTissueSP $fromExT".
                              " WHERE (".$include_exclude_query_new[$ii].") AND (EntryGelImage.gelID = Gel.gelID) ".
                              " AND (Gel.gelID = GelTissueSP.gelID) )";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*ViewRef\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT ReferencedEntry.AC FROM ReferencedEntry, ViewRef $fromExT WHERE (".$include_exclude_query_new[$ii].")".
                              " AND (ReferencedEntry.referenceID = ViewRef.referenceID) )";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*SpotEntry\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT SpotEntry.AC FROM SpotEntry $fromExT WHERE (".$include_exclude_query_new[$ii]."))";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*EnzymeNomenclature\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT EnzymeNomenclature.AC FROM EnzymeNomenclature $fromExT WHERE (".$include_exclude_query_new[$ii]."))";
    }
    elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*ViewEntry\./i) {
      $field_statement[$ii] = " (SELECT DISTINCT ViewEntry.accession_number FROM ViewEntry $fromExT WHERE (".$include_exclude_query_new[$ii]."))";
    }
    #elsif ($include_exclude_query_new[$ii] =~ /^\s*(?:\(*\s*)*Author\./i) {
    #  $field_statement[$ii] = " (SELECT DISTINCT ReferencedEntry.AC FROM ReferencedEntry, Author WHERE (".$include_exclude_query_new[$ii].")".
    #                          " AND (ReferencedEntry.referenceID = Author.article) )";
    #} # Excluding an author will be resource consumming (independant author/AC lines)
  }

  my ($relation);
  my $in_fields_counter = my $block_counter = 0; 
  my $all_fields_statement  = "(";

  for (my $ii = 0; $ii <= $number_of_fields; $ii++) {

    if ($include[$ii] or $exclude[$ii]) {
      $all_fields_statement .= $field_statement[$ii];
    }
    else {
      next if (($ii+1) % $fields_per_block) == 1;
      $all_fields_statement =~ s/(?:UNION|INTERSECT)\s*$//i;
    }
    if (($ii+1) % $fields_per_block) {
      $relation = ($outer_inside[$in_fields_counter] eq 'OR')? "UNION":($outer_inside[$in_fields_counter] eq 'AND')? "INTERSECT":"";
      $all_fields_statement .= " ".$relation." ";
      $in_fields_counter++;
    }
    else {
      if ($ii == $number_of_fields) {
        $all_fields_statement .= ")";
        last;
      }
      $all_fields_statement =~ s/\)?\s*(?:UNION|INTERSECT)\s*\(\s*$//i;
      $relation = ($outer_outside[$block_counter] eq 'OR')? "UNION":($outer_outside[$block_counter] eq 'AND')? "INTERSECT":"";
      $all_fields_statement .= ") ".$relation." (";
      $block_counter++;
    }
  }
  $all_fields_statement =~ s/\(\s*\)//g;
  $all_fields_statement =~ s/(?:(?:UNION|INTERSECT|\()?\s*)+$//gi;
  $all_fields_statement =~ s/^\s*(?:UNION|INTERSECT|\))?\s*//gi;

  $command = $date_query.$all_fields_statement;
# sent only via POST interfaces since version 2.50.1
  if ($exactQuery) {$command =~ s/ \~\* / = /g}
  my $cost = EXPLAIN_FAST_COMMAND($command);
  if ($cost == -1 and length($command) > 5000) { # PostgreSQL < 7.3 do not send back rows on EXPLAIN commands -> $cost = -1 
                                                  # we can then replace the length control by another EXPLAIN query (and catch it via a tmp file with $conn->trace(debug_port).
                                                  # Another problem: sent query are limited in length, they are ignored when they exceed 2K characters.
    print "<br><br><strong class=Warn><center>Process Halted!<br><br>Constructed querry too long. Please, make your query shorter!".
          "</strong></center><br><br>" unless $EXECUTE_FAST_COMMAND::query_too_long;
    $EXECUTE_FAST_COMMAND::query_too_long = 1;
    return 0;
  }
  elsif ($cost > $main::max_postgreSQL_query_cost) {
    print "<br><br><strong class=Warn><center>Process Halted!<br><br>Constructed querry highly time-consuming (cost = $cost). Please, make your query lighter!".
          "</strong></center><br><br>" unless $EXECUTE_FAST_COMMAND::query_too_long;
    $EXECUTE_FAST_COMMAND::query_too_long = 1;
    return 0;
  }

  my (@selected_ACs) = EXECUTE_FAST_COMMAND($command);
  if (!$main::show_hidden_entries and scalar @selected_ACs > 0) {
    undef($command);
    foreach my $selected_ac (@selected_ACs) {
      $command .= "AC = '$selected_ac' OR "
    }
    $command =~ s/\s+OR\s*$//i;
    $command = "SELECT AC FROM Entry WHERE $SERVER::showFlagSQLEntry AND ($command)";
    @selected_ACs = EXECUTE_FAST_COMMAND("$command");
  }



  # DISPALY / OUTPUT FIELDS

  my (@head, @bold_head, @full_entry, @last_row, $fields_in_selected_tables, $row_bgcolor, $row_counter, @textfile_output, $textfile_output);
  my $separator = "**";
  my $main_cell_align = "center";

  for (my $ii=0; $ii < @output_fields; $ii++) {
    $head[$ii]      = $output_fields[$ii];
    $bold_head[$ii] = $co_combined->strong($output_fields[$ii]);
  }

  # evaluate/redefine fields rank
  $fields_in_selected_tables =  $fields_in_full_entries;
  if ($output_flag_refs) {
    foreach my $key (keys  %labels_output_fields_rank_in_table) {
      $labels_output_fields_rank_in_table{$key} = $1 + $fields_in_selected_tables if ($labels_output_fields_rank_in_table{$key} =~ /^\#(\d+)/)
    }
    $fields_in_selected_tables += $fields_in_reunit_refs;   
  }

  if ($output_flag_tissue) {
    foreach my $key (keys  %labels_output_fields_rank_in_table) {
      $labels_output_fields_rank_in_table{$key} = $1 + $fields_in_selected_tables if ($labels_output_fields_rank_in_table{$key} =~ /^\#\#(\d+)/)
    }
    $fields_in_selected_tables += $fields_for_tissue;
  }
  if ($output_flag_keywords) {
    foreach my $key (keys  %labels_output_fields_rank_in_table) {
      $labels_output_fields_rank_in_table{$key} = $1 + $fields_in_selected_tables if ($labels_output_fields_rank_in_table{$key} =~ /^\#\#\#(\d+)/);
    }
    $fields_in_selected_tables += $fields_for_keywords;   
  }
  if ($output_flag_spot) {
    foreach my $key (keys  %labels_output_fields_rank_in_table) {
      $labels_output_fields_rank_in_table{$key} = $1 + $fields_in_selected_tables if ($labels_output_fields_rank_in_table{$key} =~ /^\#\#\#\#(\d+)/);
    }
    $fields_in_selected_tables += $fields_for_spot;   
  }


  my @rows = th({ -bgcolor => 'lightblue', -class=>'center' }, \@bold_head)."\n";
  @selected_ACs = sort @selected_ACs;

  for (my $ii = 0; $ii < @selected_ACs; $ii++) {

    $row_bgcolor = ($row_counter % 2)? '#f0e68c':'#fffafa';
    $row_counter++;

    # full entries part
    $command = " SELECT * FROM ViewEntry WHERE ViewEntry.accession_number = '$selected_ACs[$ii]' LIMIT 1";
    (@full_entry) = EXECUTE_FAST_COMMAND($command);

    foreach my $head (@head) { # consolidate '+'s
      if ($labels_output_fields_rank_in_table{$head} =~ /^\d+\+/) {
        my $to_restore_labels_output_fields_rank_in_table = $labels_output_fields_rank_in_table{$head};
        $labels_output_fields_rank_in_table{$head} =~ s/^(\d+)(\+)/$2/;
        my $main_head = $1;
        my @global_cell = ();
        my $buffer;
        $buffer = $full_entry[$main_head-1] if $full_entry[$main_head-1] and $full_entry[$main_head-1] !~ /^\-\-/;
        while ( $labels_output_fields_rank_in_table{$head} =~ s/\+(\d+)//) {
          $buffer .= "<BR>$separator<BR>".$full_entry[$1-1] if $full_entry[$1-1] and $full_entry[$1-1] !~ /^\-\-/;
        }
        $labels_output_fields_rank_in_table{$head} = $main_head;
       ($full_entry[$main_head-1] = $buffer) =~ s/\-\!\-/<BR>/g;
        $full_entry[$main_head-1] =~ s/<BR>\s*MASTER\:(\s*\S+)/<BR>$separator<BR><SPAN class=\"small\">MASTER\:$1<\/SPAN>/mgi;
        $full_entry[$main_head-1] =~ s/((?:[^<|\n][^B|\n][^R|\n][^>|\n]){20,30})\s([^<|\n][^B|\n][^R|\n][^>|\n])/$1<BR>\n$2/mg;
        $full_entry[$main_head-1] =~ s/^([^\w]*<BR>)*//;
        $full_entry[$main_head-1] = "<DIV CLASS=\"smallComment\"".$full_entry[$main_head-1]."</DIV>" if $full_entry[$main_head-1];
        push (@global_cell, td([($full_entry[$main_head-1])]));
        $full_entry[$main_head-1] =  $co_combined->table({-border=>$main::table_border, -align=>'left'},
                                                         TR(\@global_cell)
                                                        ) if @global_cell;
        $labels_output_fields_rank_in_table{$head} = $to_restore_labels_output_fields_rank_in_table; # restore ranks for next entries
      }
    }

    # references part
    if ($output_flag_refs) {
      my (@full_refs, @full_last_refs);
      $command = " SELECT referenceID FROM ReferencedEntry WHERE AC = '$selected_ACs[$ii]'";
      my @ref_list = EXECUTE_FAST_COMMAND($command);

      foreach my $ref_id (@ref_list) {
        $command = " SELECT * FROM ViewRef WHERE referenceID = $ref_id";
        @full_last_refs = EXECUTE_FAST_COMMAND($command);
        for (my $jj = 0; $jj < @full_last_refs; $jj++) {
          $full_refs[$jj].= "<BR>$separator<BR>" if $full_refs[$jj];
          $full_last_refs[$jj] = '--' if !$full_last_refs[$jj];
          $full_refs[$jj].= $full_last_refs[$jj];
        }
      }

     (@full_entry) = (@full_entry, @full_refs);
    }

    # tissue part
    if ($output_flag_tissue) {
      my ($full_tissue);
      $command = " SELECT TissueSP.tissueSPDisplayedName FROM GelTissueSP, Gel, EntryGelImage, TissueSP".
                 " WHERE EntryGelImage.AC = '$selected_ACs[$ii]'".
                 " AND EntryGelImage.gelID = Gel.gelID AND Gel.gelID = GelTissueSP.gelID".
                 " AND GelTissueSP.tissueSPName IS Not NULL AND GelTissueSP.tissueSPname = TissueSP.tissueSPName".
                 " ORDER BY TissueSP.tissueSPDisplayedName";
      my @tissue_list  = EXECUTE_FAST_COMMAND($command);
      $full_tissue = join "<BR>$separator<BR>", @tissue_list;
      (@full_entry) = (@full_entry, $full_tissue);
    }

    # keywords part
    if ($output_flag_keywords) {
      my ($full_keywords);
      $command = " SELECT uniProtCategoryKeywords FROM ExternalMainXrefData WHERE ExternalMainXrefData.AC = '$selected_ACs[$ii]'";
      my @keywords_list  = EXECUTE_FAST_COMMAND($command);
      $full_keywords = join "<BR>$separator<BR>", @keywords_list;
      (@full_entry) = (@full_entry, $full_keywords);    
    }

    # spot id part
    if ($output_flag_spot) {
      my ($full_spots);
      $command = " SELECT spotID FROM SpotEntry WHERE SpotEntry.AC = '$selected_ACs[$ii]'";
      my @spots_list  = EXECUTE_FAST_COMMAND($command);
      $full_spots = join "<BR>$separator<BR>", @spots_list;
      (@full_entry) = (@full_entry, $full_spots);    
    }

    $full_entry[2] = $co_combined->a({href=>"$main::script_name$main::script_name_qmark"."ac=$full_entry[2]$DB::ArgumentCurrentDatabaseNameURI"}, $full_entry[2]);
    $full_entry[0] = $co_combined->a({href=>"$main::script_name$main::script_name_qmark"."ac=$full_entry[0]$DB::ArgumentCurrentDatabaseNameURI"}, $full_entry[0]);
    
    map { $_ = '--' if !$_ } @full_entry;

    my $fileOutputSeparator = ($co_combined->param('file_tabulated'))? "\t" : "\n";

    foreach my $head (@head) {
      push (@last_row, $full_entry[($labels_output_fields_rank_in_table{$head}-1)]."\n");
    }
    if ($co_combined->param('save_file')) {
      foreach my $head (@head) {
        $textfile_output.= $full_entry[($labels_output_fields_rank_in_table{$head}-1)].$fileOutputSeparator;
      }
      $textfile_output =~ s/$fileOutputSeparator$// if $co_combined->param('file_tabulated');
      push (@textfile_output, $textfile_output); 
      undef($textfile_output);
    }

    push (@rows, td({-bgcolor=>"$row_bgcolor"},[@last_row]));
    @last_row = ();


  # %labels_output_fields_rank_in_table =   %labels_output_fields_rank_in_table_copy;

  }

  return 1, print $co_combined->p({class=>"center"}, $co_combined->strong(i("Sorry! No entry has matched your criteria. Please, try again.\n"))) if $#selected_ACs == -1;

  my $caption_number_message = ($#selected_ACs+1);
  $caption_number_message .= ($#selected_ACs)? " entries ":" entry ";
  my $caption = caption({class=>'left'}, $co_combined->span({-class=>'H3Font underlined'}, 'Query result:'),
                  $co_combined->span({-class=>'Result bold'}, " ".$caption_number_message."matching criteria").$co_combined->br.$co_combined->br)."\n";
  print $co_combined->table({-border=>$main::table_border, -width=>"90%", -align=>'center', -bgcolor=>'#fffafa'},
                      $caption,
                      TR({-align=>$main_cell_align, -valign=>'middle'},\@rows)
                     );

  if ($co_combined->param('save_file') and $textfile_output[0]) {
    grep {$_ =~ s/\s*\n\s*/ ++ /g} @textfile_output if $co_combined->param('file_tabulated');
    my $data = join "\n", @textfile_output;
    &SAVE_FILE_DATA($data);
  }

  undef($co_combined);

  return 1;

}


##======================================================================##

##======================================================================##


sub _html_valid_icon_ {

  my $validatorIcone = $main::icons."/valid-html401.png";
  # source: http://www.w3.org/Icons/valid-html401, original height:31, width:88

  return

    "  <a href=\"http://validator.w3.org/check?uri=referer\">\n".
    "    <img src=\"$validatorIcone\" align=\"middle\"\n".
    "     alt=\"Valid HTML 4.01 Transitional\" height=\"31\" width=\"88\">\n".
    "  </a>\n";

}

##======================================================================##

##======================================================================##


sub _ignore_used_only_once_
{

  return;

  () if (
    $SERVER::core_selector or
    $SERVER::interface or
    $SERVER::viewer or
    $SERVER::set_select_user or
    $SERVER::set_select_user_password or
    $SERVER::hidePrivate or
    $SERVER::extractAllRemoteDatabaseNames or
    $SERVER::remoteInterfaceDefined or
    $DB::CurrentDatabaseName or
    $main::main_script or
    $main::set_dbname or
    $main::default_select_user or
    $main::default_select_user_password or
    $main::default_host or
    $main::default_tty or
    $main::default_options or
    $main::basic_font_color or
    $main::icons or
    $main::menu_selected_button_color or
    $main::default_port or
    $main::links or
    $main::menu_button_color or
    $main::bkgrd or
    $main::server_path or
    $EXTERNAL::include_links_to_compute_map
  )
  
}

#================================================================================================#
1;








#================================================================================================#
#================================================================================================#
#================================================================================================#
#================================================================================================#


