# PERL Package DbCrossRefs.pm -> database cross-reference manager
# KHM - Khaled Mostaguir - 9/2003

=head1 NAME
 
DbCrossRefs - supply object methods for database cross-reference handling
 
=head1 SYNOPSIS

   use DbCrossRefs;

   $db_object = new DbCrossRefs;

   $db_object->open("database_file.db", "db");

   $url_link = $db_object->get("SOME_DATABASE", "$ac", "$second_id")
     if $db_object->check("SOME_DATABASE");

   $db_object->add("NEW_DATABASE", "http://www.somewhere.org/displayer?ac={0}&sp={1}")
     unless $db_object->exists("NEW_DATABASE");

   $db_object->commit;

   $db_object->export_file("database.txt", "txt");

   %db_refs = $db_object->get_all();

   $db_object- = undef;

=head1 DESCRIPTION
 
   see DbCrossRefs.html

=head1 AUTHOR

   Khaled Mostaguir (khaled.mostaguir@isb-sib.ch)

=cut


package DbCrossRefs;

require 5.005; 

use Carp; 
use Symbol;
use IO::File;
use strict;
use Fcntl;
use AnyDBM_File;

if ($] >= 5.005) {use LWP::UserAgent}

# use DB_File;
# $DB_BTREE->{'compare'} = sub { shift cmp shift };

# use warnings  - not with perl 5.005;

$DbCrossRefs::ExPASy = $ENV{GL_expasy}? 1 : 0;
eval ("use Expasy") if $DbCrossRefs::ExPASy;

$DbCrossRefs::CaseInsensitive = 0;


BEGIN {

  use vars qw(@ISA $VERSION @EXPORT $AUTOLOAD);
  use Exporter();
  @ISA = qw(Exporter);
  $VERSION = "0.04.1"; 
 #@EXPORT = qw(&AUTOLOAD);

}


sub new {

  @_  >= 1 && @_ <= 3 or confess 'usage: $object = DbCrossRefs->new([file_name] [, file_format = {"txt" | "db"}])';

  if (@_ == 2 and $_[1] =~ /^(?:case_?)?insensitive$/i) {
    # e.g: $object->new DbCrossRefs("case_insensitive"), then open file with the open method
    $DbCrossRefs::CaseInsensitive = 1;
  }
  
  my $self = {};
  my $class = shift;
  my $file_name = shift;
  my $file_format = shift;
  my $object_gensym = gensym;

  $self->{PACKAGE} = $class;
  $self->{RETURN_CALLER} = sub { return caller(0) };

  bless $object_gensym, $class;
  bless ($self);

  if ($file_name and !$DbCrossRefs::CaseInsensitive) {
    $self->open($file_name, $file_format)
      or return undef;
  }

  return $self;

}


sub DESTROY {

  my ($object) = @_;
  return undef unless defined($object); 
  # my $not_hash_type = 1 if $object !~/HASH/i; # first call -> GLOB type (poor code!)
  my $not_hash_type = 1 if ref($object) ne 'HASH';
  unless ($not_hash_type) {
    my $file_name = $object->{FILE_NAME};
    my $file_format = $object->{FILE_FORMAT};
    $object->{OPENED_FILE}->IO::File::close if $object->{OPENED_FILE}   
  }

}


sub AUTOLOAD () {

  my $subroutine  = $AUTOLOAD;
  croak "You called a non existing method ($subroutine) with these arguments: (", join (", ", @_[1..$#_]).")"  if $subroutine !~ /\:\:undef$/;
  undef;

}


sub _has_a_file_been_read {

  my ($object, $method) = @_;

  my $package = &{$object->{RETURN_CALLER}};
  unless (defined($object->{FILE_NAME})) {
    carp $package.'->'.$method.': before using this method, you should have opened a file by $object->open([FILE_NAME])';
    return undef;
  }

  return 1;

}


sub open {

  my ($object, $file_name, $file_format, $import) = @_;
  my (%original_hash, %modified_hash, $comment_block);
  my (%counter, $file_name_copy, $file_format_copy);

  my $package = &{$object->{RETURN_CALLER}};
  my $method = ($import)? 'import_file' : 'file';
  (@_ >= 1 and @_ <= 4) or $import or confess $package.'->'.$method.': use $object->'.$method.'([file_name] [, file_format = {"txt" | "db"}])';

  if ($import) {
    %original_hash = %{$object->{NOW_HASH}};
    $comment_block = $object->{NOW_COMMENT};
  }

  $file_format = lc($file_format);
  $file_format = 'txt' if $file_format eq 'text' or !$file_format;
  confess $package.'->'.$method.': use $object->'.$method.'([file_name] [, file_format = {"txt" | "db"}])' unless ($file_format eq 'txt' or $file_format eq 'db' );

  $file_name_copy = $object->{FILE_NAME} if $import; 
  $file_format_copy = $object->{FILE_FORMAT} if $import;
  $object->{FILE_FORMAT} = $file_format;

  if ($file_name) {
    $object->{FILE_NAME} = $file_name;
    if (-e "$file_name") {
      if ($file_format eq 'txt') {
        $object->{OPENED_FILE} = new IO::File;
        $object->{OPENED_FILE}->open("<$file_name") or confess $package."->$method: cannot open a text file ($file_name)";
        my $opened_file = $object->{OPENED_FILE};
        local $/="\n";
        while (my $line = <$opened_file>) {
          chomp($line);
          $line =~ s/\r?$//;
          if ($line =~ /^\s*\#/) {
            $comment_block .= $line."\n";
            next;
          }
          next if $line =~ /^\s*$/;
          if ($line =~ /^(\S+)\s+(\S+)/) {
            my ($key, $value) = ($1, $2);
            $key = lc($key) if $DbCrossRefs::CaseInsensitive;
            if ($original_hash{$key}) {
              carp $package.'->'.$method.': duplicate key ('.$key.') in '.$file_name.' / '.$key.' will be re-written as '.$key.'_'.++$counter{$key};
              $key .= '_'.$counter{$key};
            }
            $original_hash{$key} = $value;
          }
          else {
            carp $package."->$method: a line is not conform to the expected syntax in $file_name:\n$line"."The line has been commented!"; 
            $comment_block .= "# ".$line."\n";
          }

        }
        $object->{OPENED_FILE}->close;
        undef $object->{OPENED_FILE};

      }
      elsif ($file_format eq 'db') {
        my %db_hash;
        # &{$DB_BTREE->{'compare'}};
        unless (tie %db_hash, "AnyDBM_File", "$file_name", O_RDWR, 0644) {
          confess $package."->$method: could not read the specified database file ($file_name)".
                        " as its' structure may have been built with a different default (Any)DB_File module";
        }
        while (my ($key, $value) = each %db_hash) {
          $key = lc($key) if $DbCrossRefs::CaseInsensitive;
          if ($original_hash{$key}) {
            carp $package.'->'.$method.': duplicate key ('.$key.') in '.$file_name.' / '.$key.' will be re-written as '.$key.'_'.++$counter{$key};
            $key .= '_'.$counter{$key};
          }
          $original_hash{$key} = $value unless $value eq '_comment_'; # (do not undef as it's referenced)
        }
        $comment_block = $db_hash{_comment_};
        untie %db_hash;
      }
      else { return undef } # not reached
    }
    else { # file name defined but not existant - but is kept in $object->{FILE_NAME} (for 'commit' method)
    }
  }

  else {
    # No file name: no special treatment for the moment
    $object->{OPENED_FILE} = IO::File->new_tmpfile() or carp $package."->open: could not open a temporary file";
    $object->{OPENED_FILE}->close;
    undef ($object->{OPENED_FILE});
    $object->{FILE_NAME} = defined;
  }

  $object->{FILE_NAME} = $file_name_copy if $import;
  $object->{FILE_FORMAT} = $file_format_copy if $import;

  $object->{ORIGINAL_HASH} = \%original_hash unless $import;
  $object->{ORIGINAL_COMMENT} = $comment_block unless $import;
  $object->{NOW_HASH} = \%original_hash;
  $object->{NOW_COMMENT} = $comment_block;

  return 1;

}


sub add {

  my ($object, $db,  $url, $comment, $check, $method) = @_;
  unless (defined($check)) {
    ($object, $db,  $url, $check, $method) = @_;
  }
  return undef unless &_has_a_file_been_read($object, 'add');

  $db = lc($db) if $DbCrossRefs::CaseInsensitive;
  $method = 'add' unless $method eq 'alter';

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 3 and @_ <= 6 or confess $package.'->'.$method.': usage $object->.'.$method.'(database, url [, comment] [, check = 1])';
  if ($comment) {
    $comment =~ s/^\s*\#?//;
    $comment =~ s/\n/ /mg;
    $comment = "#($db): $comment";
  }

  my $file_name = $object->{FILE_NAME};
  my %dbs_all = %{$object->{NOW_HASH}};
  my $dbs_original = $object->{ORIGINAL_HASH};
  my $comment_block = $object->{NOW_COMMENT};


  if ($db =~ /\s/ or $url =~ /\s/)  {
    carp "$package->add: you cannot ".$method." a database name ($db) that contains spaces" if $db =~ /\s/;
    carp "$package->add: you cannot ".$method." a URL ($url) that contains spaces" if $url =~ /\s/;
    return undef;
  }

  if (${$dbs_original}{$db} and $method eq 'add') {
    carp "$package->add: you tried to add an already existing database ($db), use the 'alter' method instead!";
    return undef;
  }
  if ($dbs_all{$db} and $method eq 'add') {
    carp "$package->add: you tried to add a previously added database ($db), use the 'alter' method instead!";
    return undef;
  }


  $dbs_all{$db} = $url;
  $comment_block .= "$comment\n" if $comment;

  $object->{NOW_HASH} = \%dbs_all;

  while ($check) {
    last if !&check($object, $db);
    &delete($object, $db);
    carp  $package.'->'.$method.': could not connect to the provided URL of the database you tried to '.$method;
    return undef;
  }

  $object->{NOW_COMMENT} = $comment_block;

  return 1;

}


sub alter {

  my ($object, $db,  $url, $comment, $check) = @_;

  $db = lc($db) if $DbCrossRefs::CaseInsensitive;
  $check = 0 unless $check;

  return undef unless &_has_a_file_been_read($object, 'alter');
  return &add($object, $db, $url, $comment, $check, 'alter');

}


sub alter_name {

  my ($object, $old_db, $new_db) = @_;
  return undef unless &_has_a_file_been_read($object, 'alter_name');

  my $package = &{$object->{RETURN_CALLER}};
  @_ == 3 or confess $package.'->alter_name: usage $object->alter_name(old_database_name, new_database_name)'; 

  $old_db = lc($old_db) if $DbCrossRefs::CaseInsensitive;
  $new_db = lc($new_db) if $DbCrossRefs::CaseInsensitive;

  my $url = ${$object->{NOW_HASH}}{$old_db};
  unless ($url) {
    carp $package."->alter_name: could not find a corresponding URL for the database name to be altered ($old_db)";
    return undef;    
  }
  unless (&add($object, $new_db, $url, '', '', 0)) {
    carp $package."->alter_name: could not add the new database name ($new_db)";
    return undef;
  }
  unless (&delete($object, $old_db)) {
    carp $package."->alter_name: could not delete the old database name ($old_db)";
    unless (&delete($object, $new_db)) {
      carp $package."->alter_name: a problem occured, both ($old_db) and ($new_db) exist now and do refer to the same URL";
    }
    return undef;
  }

  return 1;

}


sub delete {

  my ($object, $db) = @_;
  return undef unless &_has_a_file_been_read($object, 'delete');

  my $package = &{$object->{RETURN_CALLER}};
  @_ == 2 or confess $package.'->delete: usage $object->delete(database)';

  $db = lc($db) if $DbCrossRefs::CaseInsensitive;

  my $comment = $object->{NOW_COMMENT};
  $comment =~ s/^\s*\#\($db\)[^\n]*\n//mg;
  my %new_db_hash;
  if (${$object->{NOW_HASH}}{$db}) {
    while (my ($read_db, $read_url) = each %{$object->{NOW_HASH}}) {
      next if $read_db eq $db;
      next if $DbCrossRefs::CaseInsensitive and lc($read_db) eq lc($db);
      $new_db_hash{$read_db} = $read_url;
    }
   $object->{NOW_HASH} = \%new_db_hash;
   return 1;

  }

  return 0;

}


sub exists {

  my ($object, @db) =  @_;
  return undef unless &_has_a_file_been_read($object, 'exists');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 2 or confess $package.'->exists: usage $object->exists(database 1 [, database 2], ... [, database n])';

  foreach my $db (@db) {
    $db = lc($db) if $DbCrossRefs::CaseInsensitive;
    return 0 unless &get($object, $db);
  }

  return 1;

}


sub get {

  my ($object, $db, @identifiers) =  @_;
  return undef unless &_has_a_file_been_read($object, 'get');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 2 or confess $package.'->get: usage $object->get(database [, identifier 1] [, identifier 2] ... [, identifier n])';

  $db = lc($db) if $DbCrossRefs::CaseInsensitive;

  my $url = ${$object->{NOW_HASH}} {$db};
  return undef unless defined $url;

  my $counter = 0;

  foreach my $id (@identifiers) {
    $url =~ s/\{$counter(\:[^}]+)?\}/$id/g;
    $counter++;
  }
  # $url =~ s!^http\://(?:\w+\.)?expasy\.org/!/!i if exists $ENV{GL_expasy}; # Keep absolute URL at this level

  return $url;
}


sub get_all {

  # with no argument: returns a hash, with 'text' argument: returns a string

  my ($object, $format, $expression) =  @_;
  return undef unless &_has_a_file_been_read($object, 'get_all');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 1 and @_<=3 or confess $package.'->get_all: usage $object->get_all([format = {"txt"}], [expression])';
  
  my %nowHashNoIDs = %{$object->{NOW_HASH}};
  
  $expression = quotemeta($expression) if $expression;

  undef $format unless $format =~ /^te?xt$/i;

  if ($format) {

    my $list;
    foreach my $db_list (sort keys %{$object->{NOW_HASH}}) {
      next if $expression and $db_list !~ /$expression/i;
      $list .= "$db_list ${$object->{NOW_HASH}}{$db_list}\n";
    }
    $list = _no_ID_examples($list);
    return $list;
  }

  if (%{$object->{NOW_HASH}}) {
  
    my %hash_list = %{$object->{NOW_HASH}};
           
    if ($expression) {
      foreach my $key (keys %hash_list) {
        undef($hash_list{$key}) unless $key =~ /$expression/i;
        $hash_list{$key} = _no_ID_examples($hash_list{$key}) if $hash_list{$key};
      }
    }
    else {
      foreach my $hashURLs (keys %hash_list) {
        $hash_list{$hashURLs} = _no_ID_examples($hash_list{$hashURLs})
      }
    }
    return %hash_list;
  }
  
  sub _no_ID_examples {
   (my $_no_id_examples = $_[0]) =~ s/\{(\d+)\:[^}]*\}/{$1}/g;
    return $_no_id_examples;
  }
  
  return ();

}


sub get_comment {

  my  ($object, @db) =  @_;
  return undef unless &_has_a_file_been_read($object, 'comment');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 1 or confess $package.'->get_comment: usage $object->get_comment([expression 1] ... [, expression n])';

  map {$_ = lc($_)} @db if $DbCrossRefs::CaseInsensitive;

  my $comment = $object->{NOW_COMMENT};
  my $exp = join "|", @db;
  
  if (@db and grep {$_ =~ /^[a-z0-9\|]+$/} @db ) {
    $exp =~ s/\|+/\|/g;
    my $comment_limited;
    while ($comment =~ s/^([^\n]*\n)$//) {
      my $comment_line = $1;
      $comment_limited .= $comment_line if $comment_line =~ /$exp/;
    }
    $comment = $comment_limited; 
  }

  elsif (@db) {
      $exp =~ s/\|/, /g;
      carp  $package."->get_comment: only alphanumerical characters are allowed (serarched expressions: $exp)";
      return undef;    
  }

  return $comment;

}


sub check {

  # return 'undef' if no checking, a defined '' if all is ok, or a warning string (with details) if an URL fails

  return undef if $] < 5.005;
  my $result = undef;

  my  ($object, @db) =  @_;
  return undef unless &_has_a_file_been_read($object, 'check');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 1 or confess $package.'->check: usage $object->check({"Database"}, {"Database 2"},..,{"Database N"})';

  map {$_ = lc($_)} @db  if $DbCrossRefs::CaseInsensitive;

  @db = sort keys %{$object->{NOW_HASH}} unless @db;

  my $conn = LWP::UserAgent->new;
  $conn->timeout(60);
  my ($db, $url, $url_original);

  foreach my $db (@db) {
    $url = $url_original = ${$object->{NOW_HASH}}{$db};
    $url =~ s/\{\d+\:([^}]+)\}/$1/g;
    $url =~ s!^http\://(?:\w+\.)?expasy\.org/!/!i if exists $ENV{GL_expasy};
    $url =~ s/\{(\d+)\}/_$1_/g; # { & } cause sometime errors on some servers
    my $request = new HTTP::Request('GET', "$url");
    my $response = $conn->request($request);
    my $response_status = $response->status_line;
    my $check_error = $package."->check: could not properly connect to a database URL:\n$db => $url_original (".$response_status." / no arguments-> OK)\n\n";
    if($response->is_error()) {
      my $url_no_arg = $url;
      $url_no_arg =~ s/\?\S+$//;
      $request = new HTTP::Request('GET', "$url_no_arg");
      $response = $conn->request($request);
      if($response->is_error()) {
        my $check_error_2 = $package."->check: could not properly connect to a database URL:\n$db => $url_original (".$response_status.
                                     " / no arguments-> ".$response->status_line.")\n\n";
        carp $check_error_2;
        $result .= $check_error_2;
      }
      else {
        carp $check_error;
        $result .= $check_error;
      }
    }
  }

  return $result;

}


sub import_file {

  my  ($object, $file_name, $file_format) =  @_;
  return undef unless &_has_a_file_been_read($object, 'import');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 2 and @_ <= 3 or confess $package.'->import: use $object->import_file(file_name [, file_format = {"txt"|"db"}])';

  if (!-e "$file_name") {
    carp $package."->import_file: could not found a file ($file_name) to import data";
    return undef;
  }

  unless (&open($object, $file_name, $file_format, 1)) {
    carp  $package."->import_file: could not open a file ($file_name) to import data";
    return undef;
  }

  return 1;

}


sub export_file {

  my  ($object, $file_name, $file_format, $commit) =  @_;
  return undef unless &_has_a_file_been_read($object, 'import');

  my $package = &{$object->{RETURN_CALLER}};
  my $method = ($commit)? 'commit' : 'export_file';
  @_ >= 2 and @_ <= 4 or confess $package.'->'.$method.': use $object->export_file(file_name [, file_format = {"txt"|"db"}])';

  my $file_name_copy = ($file_name)? $file_name : "_TEMPORARY_FILE_";

  if (-s "$file_name" and !$commit) {
    carp $package.'->'.$method.": file already exists ($file_name), the file name has been changed to $file_name.$$";
    $file_name .= ".$$";
  }
  if (-e "$file_name" and $commit) {
    use File::Copy;
    copy("$file_name", "$file_name.old");
  }

  $file_format = lc($file_format);
  $file_format = 'txt' if $file_format eq 'text' or !$file_format;
  confess $package.'->'.$method.': use $object->'.$method.'(file_name [, file_format = {"txt"|"db"}])' unless ($file_format eq 'txt' or $file_format eq 'db' );

  if ($file_format eq 'txt') {
    my $export_file;
    my $comment_block = $object->{NOW_COMMENT};
    my %export_hash = %{$object->{NOW_HASH}};
    $export_file = new IO::File;
    $export_file->open(">$file_name") or confess $package.'->'.$method.": cannot open a text file ($file_name_copy)";
    $export_file->print($comment_block);
    foreach my $db_export (sort keys %export_hash) {
      $export_file->print("$db_export $export_hash{$db_export}\n");
    }
    $export_file->close;
  }
  elsif ($file_format eq 'db') {
    my %export_hash;
    # &{$DB_BTREE->{'compare'}};
    unless (tie %export_hash, "AnyDBM_File", $file_name, O_RDWR|O_CREAT|O_EXCL, 0644) {
      carp $package.'->'.$method.": could not prepare the database file to export data ($file_name_copy)";
      return undef;
    }
    %export_hash = %{$object->{NOW_HASH}};
    $export_hash{_comment_} = $object->{NOW_COMMENT};
    untie %export_hash;  
  } 
  else { return undef } # not reached

  return 1;

}


sub reset {

  my ($object) = @_;
  return undef unless &_has_a_file_been_read($object, 'reset');

  my $package = &{$object->{RETURN_CALLER}};
  unless (@_ == 1) {
    carp $package.'->commit: use $object->reset() with no arguments';
    return undef;
  }

  $object->{NOW_HASH} =  $object->{ORIGINAL_HASH};
  $object->{NOW_COMMENT} = $object->{ORIGINAL_COMMENT};

  return 1;

}


sub commit {

  my ($object, $file_name, $file_format) = @_;
  return undef unless &_has_a_file_been_read($object, 'commit');

  my $package = &{$object->{RETURN_CALLER}};
  @_ >= 1 and @_ <= 3 or confess $package.'->commit: use $object->commit([file_name [, file_format = {"txt"|"db"}]])';

  $file_name = $object->{FILE_NAME} unless $file_name;
  $file_format = 'txt' if !$file_format and $file_name;
  $file_format = $object->{FILE_FORMAT} unless $file_format;

  my $commit = 1;
  unless (defined($file_name) and &export_file($object, $file_name, $file_format, $commit)) {
    carp  $package."->commit: could not commit to ($file_name) to update data";
    return undef;
  }

  return 1;

}

#require was true
1;

END { };


