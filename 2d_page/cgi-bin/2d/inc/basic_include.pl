#@(#)basic_include.pl
#  version 2.50
#  Make2D-DB II tool
#  Basic configuration file for both the database constuction and the server scripts
#===================================================================================#

# /*  This script is part of the Make-2D DB II Tool                               */
# /*  Copyright to the SWISS INSTITUTE OF BIOINFORMATICS                          */
# /*  You can freely use this script without moving those copyright lines         */
# /*  Modifications should comply to the instructions stated in the licence terms */


# The definitions on this file are integrated with to the generated configuratrion files !!

# You may modify or not the proposed values in the 'USER CONFIGURABLE PART' section.

# An origianl copy of this file is to be found in lib2d/basicIncludeInitial.pl
# If this file is deleted, it will be automatically generated from the original copy


#-------------------------------------------------------------------------------#
#-------------------------------------------------------------------------------#


# USER CONFIGURABLE PART:


#-------------------------------------------------------------------------------#
# General Behaviour (you can edit this section at any time to change behaviour) #
#-------------------------------------------------------------------------------#

# Deactivate the Web interface acces:
# Set to 1 to temporarily deactivate any access (an appropriate message will be displayed)
$SERVER::deactivated = 0;


# If external update is switched on, include:
#
# from UniProtKB/Swiss-Prot and UniProtKB/TrEMBL (protein data)
$EXTERNAL::update_from_SwissProt = 0;
# from similar 2-DE databases
$EXTERNAL::update_from_Make2DDB  = 1;
# from Newt (taxonomy)
$EXTERNAL::update_from_Newt      = 0;
# link to various compute map programs on other servers
$EXTERNAL::include_links_to_compute_map = 1;
# replace protein internal data by the extracted data
$EXTERNAL::update_internal_data  = 1;

# During first installation, the external data may replace the inner data on 3 levels:
# 1 -> low, 2 -> partial, 3 -> full (default)
$EXTERNAL::replacementLevel = 3;

# Since all Swiss-Prot entries have been recently demerged, a user choice is sometime necessary to link
# his old accession numbers to new ones.
# If this value is set to 1, the tool will automatically assign the new accession number
# that shares the same Taxonomy ID with the old one.
# If this value is set to 0, then for all replacements the user will explicitly be asked for a choice
$EXTERNAL::acceptDemergedSwissProtEntryWithSameTaxID = 1;


# Is there any login ID and a password required to access the WEB server interface?
#
# login ID
$SERVER::localHostID = '';
# login password
$SERVER::localHostPass = '';

# The home page sub-header title
$SERVER::homeSubHeader = "Two-dimensional polyacrylamide gel electrophoresis database";

# Display general comments on the tool from the interface home page
# (2 for all comments, 1 for minimum comments, 0 for no comment display)
$SERVER::displayToolComments = 2;

# Include various Java applets (set to 0 if you want to deactivate java applets)
$SERVER::javaApplets = 1;

# Deactivate use of DBI placeholders:
# 0 for standard behaviour, modify only if you have problems updating external data
# (due to a DBD::Pg compilation problem)
# Change if DBD::Pg has been compiled with the POSTGRES_LIB variable pointing to
# an incorrect version of postgreSQL librairies
$SERVER::noDBIplaceholders = 0;


# When extracting annotations from a Melanie XML file,
# we may replace spot IDs label by any other annotation label if found.
# Define here this annotation label (e.g. 'SerialNumber')
$MELANIE::spotLabel = 'SerialNumber';

# When providing gif files for the maps that are of different size than the reference maps,
# set 'extractGifDimension' to 1.
# You can also define any border size (in pixels) if the images to be displayed contain some
# extra borders (e.g. legends/axes).
# e.g: SWISS-2DPAGE works with the following values: 1, 60, 60
$MAPIMAGE::extractGifDimension = 0;
$MAPIMAGE::verticalBorder      = 0;
$MAPIMAGE::horizontalBorder    = 0;

# To compare entry versions between updates, 2 levels are avaialble:
# 0: compare differences based on data length (default)
# 1 (or > 0): compare differences by performing a full checksum over data
$updateVersionHigherStrategy = 1;


# Debugging mode: (0) for no, (1) for normal debug, (2) for debug with no DB backup
$_DEBUG_::ON = 1;


#-------------------------------------------------------#
# Some Global Definitions (generally not to be changed) #
#-------------------------------------------------------#


# Main Configuration Definitions:


# Is this the SWISS-2DPAGE Database (0 for no, 1 for yes)?
# To be changed only if you are installing SWISS-2DPAGE locally on your system
# or wishing to host an interface that queries *only* SWISS-2DPAGE
# (you will need to contact us for both options).

$swiss_2d_page = 0;


# The main index for the database

$main_index = "SWISS-PROT";


# The name of the map viewer script

$map_viewer = "2d_view_map.cgi";

# The name of the GDchart script

$gd_chart_viewer = "GDchart.cgi";

# A tissue list is being developed at the Swiss-Prot Group (SIB).
# This tool needs to know which initial version is being used.

$tissue_list_initial_version = '25-Jul-2006';


# Some URLs (relative or absolute):

$tissue_list_URL = '/cgi-bin/lists?tisslist.txt';


# Spot clustering parameters. Used internally when reading spot positions directly from a MelanieII image file
# or when accession numbers are given instead of spots ID's in the spots reports.
# Those two parameters have effect only if you have not provided spots ID's on your reports
# (if you replaced them by accession numbers) or if you are using MelanieII maps to extract the spots positions.
# Modify the default values only if you happen to encounter some problems in the assignment of ID's to the different spots.
# Note that the MelanieII extraction process is likely to become completely abandoned!
# Set a value for clustering spots over the y axis (given in pixel)
$y_min_cluster = 5;
# Set a value for clustering spots by their mass weight (given in dalton)
$mw_min_cluster = 200;


# ! DEPRECATED !
# DEFAULT values for MISSING REQUIRED LINES: NO GIVEN SPOT NAME #
# 2D spots and 1D bands given with no name will be automatically assigned a sequential decimal value #
# Has effect with the MelanieII annotations extraction, or when providing only ACs in the spots reports
# Otherwise the spots IDs should be explicitly present in the flat file
# for 2D maps, the attributed name should have the prefix:#
# If no spots names (IDs) are given in the flat file (not recommanded), the tool will automatically assign
# sequential decimal values to the spots (1, 2,...).
# This has sense only with the direct annotations extraction from the MelanieII maps, or when no spots IDs have been
# given in the spots reports. Otherwise, an error will be raised if a spot name can't be attributed.
$unamed_spots_prefix_2D = "2D-X";
# for SDS bands, the attributed name should have the prefix:
$unamed_bands_prefix_1D = "1D-X";
# ! DEPRECATED !


# Force protein species read from the default values to change
# to the species of the corresponding maps if needed.

$forceProteinSpecies = 1;

# For non identified spots management, define a pseudo protein name
# as a container for all such spots.
# By convention, call it 'UNIDENTIFIED_SPOTS' - To be defined only once!

$unidentifiedProteinNickname = 'UNIDENTIFIED';

# For 'core' management and private users, include *non identified spots*
# annotations as an entry in all search operations 

$privateSearchUnidentifiedProtein = 1;

# This is the keyword used to indicate that a specific annotation is
# to be made private (not visible to public users).
# Do not change this value!

$privateKeyword = ' {private}';

# Name of the file that contains a list of entries (list of accession numbers)
# to be hidden in public access
$hiddenEntriesFile = 'hiddenEntries.txt';
# Name of the file that contains a list of gels (list of short names)
# to be hidden in public access
$hiddenGelsFile = 'hiddenGels.txt';
# Name of the file that contains spots associated to entries and
# their identification to be hidden in public access
$hiddenSpotsFile = 'hiddenSpots.txt';

# Name of the file to contain bibliogrpahic references (citations)
$main::referenceFile = 'reference.txt';


# A generated flat file begins with the following pattern
$generatedFlatFilePattern = '__GENERATED_FLAT_FILE__';
# A spot data section within a flat file begins with the following pattern
# lines have the syntax: Master Spot X Y pI Mw %Od %Vol
$spotDataSectionPattern = '__SPOT_DATA__';
# the sectin ends with
$spotDataSectionEndPattern = '__END_SPOT_DATA__';
# A generated maps_file begins with the following pattern
$generatedMapsFilePattern = '__GENERATED_MAPS_FILE__';


# Your provided data files (e.g. external MS files, external gel annotation documents)
# may or not be physically integrated within the database itself (1 for yes, 0 for not)
# $includeLargeObjectDocuments = 0; # keep this option deactivated!

# For a Mass Spectrometry data file: maximum number of characters to be directly displayed
# in the WEB server pages, min. 400, max. 100000 (the original data file is not affected).
$msFileMaxLineLength = 20000;


# URL address of the text documents reader on the ExPASy server
# This is the URL address to extract some useful text files from the ExPASy server

$expasy_GL_txt_reader_relative = "/cgi-bin/txt_on_web.cgi";

# URL address of the SRS and the DB extractor / mapper on the ExPASy server
# This is the URL address to extract external data from the ExPASy server
# via SRS, HTTP or database direct connections 

$expasy_getz_syntax_mapper_relative = "/cgi-bin/getz_syntax_mapper.cgi";

# Prevent the interface to execute generated long SQL queries when their cost is higher than

$max_postgreSQL_query_cost = 150000;

# If the origin of the map itself on the displayed image is not located exactly at
# the top left corner, define here the shifting between the two origins (in pixels).
# Please, note that those general 2 values will be overidden by any given value (including 0)
# found on the specific database definition blocks (from the 2d_include.pl configuration file).

$map_shift_left = 0;
$map_shift_down = 0;


# The next prefix will be attached to the extension of the cloned interface that gives
# access to the core schema (e.g. 2d.core.cgi and 2d_view_map.core.cgi)

$core_interface_prefix = 'core';



# MAPPING IDENTIFICATION METHODS #

# Within the generated configuration file you will find the model used with SWISS-2DPAGE.
# Write very simple Perl regular expressions to express the strings associated with each key.
# !DO NOT USE BACKSLASHES!
# Keys should be 8 characters maximum.
#
# use the detailed mapping identification method (yes | no)
#
$explore_mapping_method = 'yes';

# HERE WE DEFINE THE MAPPING METHODS KEYWORDS

%mapping_methods_description =    (
                                      "MS/MS"    =>  "Tandem mass spectrometry",
                                      "PMF"      =>  "Peptide mass fingerprinting",
                                      "Mi"       =>  "Microsequencing",
                                      "Aa"       =>  "Amino acid composition",
                                      "PeptSeq"  =>  "Peptide sequencing",
                                      "Gm"       =>  "Gel matching",
                                      "Im"       =>  "Immunobloting",
                                      "Co"       =>  "Comigration",
                                  );


%mapping_methods_containing  =    (
                                      "MS/MS"    =>  "Tandem mass spectrometry",
                                      "PMF"      =>  "Mass fingerprinting|Mass spectrometry|PMF",
                                      "Mi"       =>  "Microseq|Internal sequence|Sequence tag|Tagging",
                                      "Aa"       =>  "Amino acid composition",
                                      "PeptSeq"  =>  "Peptide sequencing",
                                      "Gm"       =>  "Matching|Identified on [1-2]-D",
                                      "Im"       =>  "Immuno",
                                      "Co"       =>  "Comigration",
                                  );


%mapping_methods_not_containing = ( 
                                       "PMF"     =>  "Tandem",
                                  );


# Entries main topics for nice display on the Web server (use perl regexp)
$SERVER::two_d_MAIN_TOPICS =
  "MAPPING|NORMAL LEVEL|PATHOLOGICAL LEVEL|(NORMAL |DISEASE )*POSITIONAL VARIANTS|".
  "EXPRESSION|(?:TANDEM )?MASS SPECTROMETRY|PEPTIDE MASSES|PEPTIDE SEQUENCES|AMINO ACID COMPOSITION";

# The interface search section 'identification method' should read the available mapping methods
# as defined in "%mapping_methods_description" (1), or use the default list instead (0)
$SERVER::read_mapping_methods_description_list = 0;

# Show a login button to the administrator interface on the public home page (1 to show, 0 to hide)
$SERVER::showAdminLoginButton = 1;

#----------------------------------------------------------------#
# Let the server interface query other similar remote interfaces:
#----------------------------------------------------------------#

# Contact only remote interfaces and ignore any local or remote postgreSQL direct connection (0 or 1)

$SERVER::onlyInterfaces = 0;

# Define the remote interfaces' parameters
#  -> 'URL' is required!
#  -> 'URI' is optioanl, and has effect only if the scripts' URL differs from the site home page
#  -> 'database' is optional and has effect only if the remote interface hosts several local databases
#     ('All' would select them all)
#  -> 'activated' => 1 is required to make the remote interface visible for the user (deactivate with 0)
# An example of definition is provided - Note that adding too many interfaces would slow down your queries

# Define here your remote interfaces' paramaters:

$SERVER::remoteInterfaceMaster = {

  'SWISS-2DPAGE' => {
     URL => 'http://www.expasy.org/swiss-2dpage',
     database => ['swiss-2dpage'],
     activated => 1
  },

  'World-2DPAGE' => {
    URL => 'http://www.expasy.org/world-2dpage',
    database => ['All'],
    activated => 1
  },

};

$SERVER::remoteInterfaceDefault = '';

#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#


# END OF USER CONFIGURABLE PART!

# DO NOT EDIT OR CHANGE ANYTHING STARTING FROM HERE!!

$BasicInclude::version = 2.50;

#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#

# Consider only the remote interfaces activated by user:

undef($SERVER::remoteInterfaceDefault) if $SERVER::remoteInterfaceDefault and
  !$SERVER::remoteInterfaceMaster->{$SERVER::remoteInterfaceDefault}->{activated};
foreach my $remoteDatabase (sort keys %{$SERVER::remoteInterfaceMaster}) {
  if ($SERVER::remoteInterfaceMaster->{$remoteDatabase}->{activated}) {
    $SERVER::remoteInterface->{$remoteDatabase} = $SERVER::remoteInterfaceMaster->{$remoteDatabase};
    $SERVER::remoteInterfaceDefault = $remoteDatabase if $SERVER::onlyInterfaces and
      !$SERVER::remoteInterfaceDefault;
  }
}

#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#

# Now, reload the editable configuration files if we have not done so yet (require):
# the installation config.cfg file is only read if we are running the installtion program

if ($Make2D::namespace) {
  require "$Make2D::default_configuration_dir/include.cfg" if -e "$Make2D::default_configuration_dir/include.cfg";
  require "$Make2D::default_configuration_dir/2d_include.pl" if -e "$Make2D::default_configuration_dir/2d_include.pl";
}
else {
  require "./inc/2d_include.pl" or die "no 2d_include.pl configuration file associated with basic_include.pl";
}

#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#

# Redefine some other global variables based on the two editable configuration files:

$main_script = "$main::cgi_2ddb/$main::main_script_file";

$expasy_GL_txt_reader = $main::expasy.$main::expasy_GL_txt_reader_relative;

$expasy_getz_syntax_mapper = $main::expasy.$main::expasy_getz_syntax_mapper_relative; 

#$main::icons = "/".$main::icons if $main::icons !~ /^\//;
$db_server_logo = ($main::db_server_logo_name and $main::db_server_logo_name !~ /\//)? "$main::icons/$main::db_server_logo_name" : $db_server_logo_name;

$unidentifiedProteinNickname =~ s/\W/_/g;

$MAPIMAGE::extractGifDimension = 1, $MAPIMAGE::verticalBorder = 60, $MAPIMAGE::horizontalBorder = 60 if $swiss_2d_page;

$email = undef if $email eq 'your.name\@somewhere.org';
$email =~ s/\\//g;

$MAPIMAGE::xRatio = 1.0;

$MAPIMAGE::yRatio = 1.0;

$add_database_name_to_title = 1 if scalar @DATABASES_Included > 1;

$SERVER::apacheModeRewriteOn = ($main::modeRewriteOn && $ENV{SERVER_SOFTWARE} =~ /Apache/i)? 1 : 0;

$SERVER::ExPASy = 0;

# If this is a portal, is it allowed to contact its own remote interfaces when it is remotely
# contacted via a WebService? (cyclic situations are managed and prevented)
$SERVER::PortalWebService = 0;

#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#
1;
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#


