

# (Do not remove any of the following '#' characters)

# To use the Apache mod_rewrite module edit the Apache httpd config file:
# (A copy of the instructions to be added to the Apache config file is to be found in the file 'mod_rewrite.txt')

# make sure the instruction "LoadModule rewrite_module modules/mod_rewrite.so" or equivalent is loaded,
# then make sure to activate the module by the following command (omit the '#' character to activate instructions):

# RewriteEngine on


# add the following commands:

# for debug purpose, you can include the following 2 commands to generate some log file: 
# RewriteLog "[any_path]/mod_rewrite.log"
# RewriteLogLevel 2

# define this map path:
# RewriteMap make2d-db_II_2d txt:/home/yonezawa/public_html/2d_page/cgi-bin/2d/inc/make2db_map.txt

# add the rules:
# RewriteRule {rules}...


#========================================================#

# Functions (do not include in the Apache config file):

#main /~yonezawa/2d_page/cgi-bin/2d/2d.cgi
main 2d.cgi

#viewer /~yonezawa/2d_page/cgi-bin/2d/2d_view_map.cgi
viewer 2d_view_map.cgi


