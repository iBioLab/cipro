## @class 
# 画像ファイルを処理するクラス。
#

package CIPRO::Image;

use lib "../../lib";
use strict;
use warnings;
use Image::Magick;


sub new {
    my $class = shift;
    bless {},$class;
}

## @method string miniMize ()
#
# 指定最大サイズと現サイズから、縮小画像サイズを得る。\n
# 縦・横の大きい方の値で、縦横比を変えずに縮小するサイズを求める。
#
# @return 縮小横・縦サイズ
#
sub miniMize{
    my $x     = shift;
    my $y     = shift;
    my $max_x = shift;
    my $max_y = shift;
    my $retx = $max_x;
    my $rety = $max_y;
    my $hi = 0;
    if($x > $y){
        $hi = $max_x / $x;
        $rety = $y * $hi;
    }else{
        $hi = $max_y / $y;
        $retx = $x * $hi;
    }
    return($retx,$rety);
}

## @method string resizeImage ()
#
# 画像を指定のサイズで縦横比を変えずに縮小する。\n
# 指定サイズが0の場合、指定サイズより基の画像\n
# サイズが小さい場合はサイズ変更しない。
#
# @return リサイズ後の画像データ
#
sub resizeImage{

    my $self = shift;
    my $data = shift;
    my $max_width = shift;
    my $max_height = shift;
    # 指定サイズ0ならリサイズしない
    if(($max_width != 0)&&($max_height != 0)){
        my $image = Image::Magick->new;
        $image->BlobToImage($data);

        my ($now_width, $now_height) = $image->Get('width', 'height');
        if(($now_width > $max_width)||($now_height > $max_height)){
            my ($width, $height) 
                                = miniMize(
                                      $now_width     #現在の横幅
                                    , $now_height    #現在の高さ
                                    , $max_width     #最大の横幅
                                    , $max_height ); #最大の高さ
            $image->Resize(
                      width  => $width
                    , height => $height
                    , blur   => 1
            );
        }
        $data = $image->ImageToBlob();
    }
    return $data;
}


## @method string miniMizeForPrint ()
#
# 指定最大サイズと現サイズから、縮小画像サイズを得る。\n
# 縦・横の大きい方の値で、縦横比を変えずに縮小するサイズを求める。
#
# @return 縮小横・縦サイズ
#
sub miniMizeForPrint{
    my $x     = shift;
    my $y     = shift;
    my $max_x = shift;
    my $max_y = shift;
    # 比率計算
    my $x_hi  = min($x, $max_x) / max($x, $max_x);
    my $y_hi  = min($y, $max_y) / max($y, $max_y);

    # 比率の小さな方で再計算
    if( $x_hi > $y_hi ){
        # 比率大でも最大値に収まるなら大きい方で再計算
        if( ($x * $x_hi <= $max_x) && ($y * $x_hi <= $max_y) ){
            return( (($x * $x_hi), ($y * $x_hi)) );
        }
        return( (($x * $y_hi), ($y * $y_hi)) );
    }else{
        # 比率大でも最大値に収まるなら大きい方で再計算
        if( ($x * $y_hi <= $max_x) && ($y * $y_hi <= $max_y) ){
            return( (($x * $y_hi), ($y * $y_hi)) );
        }
        return( (($x * $x_hi), ($y * $x_hi)) );
    }
}

## @method string max ()
#
# 2値を比較し、大きい値を取得する。
#
# @return 大きい値
#
sub max{
    my $a = shift;
    my $b = shift;
    return(($a > $b)?  $a:$b);
}

## @method string min ()
#
# 2値を比較し、小さい値を取得する。
#
# @return 小さい値
#
sub min{
    my $a = shift;
    my $b = shift;
    return(($a < $b)?  $a:$b);
}

## @method string resizeImageForPrint ()
#
# 画像をA4縦サイズに収まるように縦横比を変えずに縮小する。\n
# 指定サイズが0の場合、指定サイズより基の画像\n
# サイズが小さい場合はサイズ変更しない。
#
# @return リサイズ後の画像データ
#
sub resizeImageForPrint{

    my $self = shift;
    my $data = shift;
    my $max_width = shift;
    my $max_height = shift;
    # 指定サイズ0ならリサイズしない
    if(($max_width != 0)&&($max_height != 0)){
        my $image = Image::Magick->new;
        $image->BlobToImage($data);

        my ($now_width, $now_height) = $image->Get('width', 'height');
        if(($now_width > $max_width)||($now_height > $max_height)){
            my ($width, $height) 
                                = miniMizeForPrint(
                                      $now_width     #現在の横幅
                                    , $now_height    #現在の高さ
                                    , $max_width     #最大の横幅
                                    , $max_height ); #最大の高さ
            $image->Resize(
                      width  => $width
                    , height => $height
                    , blur   => 1
            );
        }
        $data = $image->ImageToBlob();
    }
    return $data;
}

1;
