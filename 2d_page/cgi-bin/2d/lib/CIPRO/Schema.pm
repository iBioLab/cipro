package CIPRO::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1vsVHzYDDShcNczziUHv0A


# You can replace this text with custom content, and it will be preserved on regeneration
1;
