## @class
# basic_infoのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::BasicInfo;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("basic_info");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('basic_info_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "cipro",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "eflag",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("basic_info_pkey", ["id"]);
__PACKAGE__->has_many(
  "basic_info_reference_db_links",
  "CIPRO::Schema::BasicInfoReferenceDbLink",
  { "foreign.basic_info_id" => "self.id" },
);
__PACKAGE__->has_many(
  "results",
  "CIPRO::Schema::Results",
  { "foreign.basic_info_id" => "self.id" },
);
__PACKAGE__->has_many(
  "seq_infoes",
  "CIPRO::Schema::SeqInfo",
  { "foreign.basic_info_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:twyM/86ZmcZp0DUJ2cTopg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
