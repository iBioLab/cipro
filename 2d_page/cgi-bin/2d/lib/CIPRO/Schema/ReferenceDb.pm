## @class
# reference_dbのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::ReferenceDb;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("reference_db");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "bigint",
    default_value => "nextval('reference_db_id_seq'::regclass)",
    is_nullable => 0,
    size => 8,
  },
  "external_db_id",
  { data_type => "integer", default_value => undef, is_nullable => 0, size => 4 },
  "xid",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "data",
  { data_type => "xml", default_value => undef, is_nullable => 1, size => undef },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("reference_db_pkey", ["id"]);
__PACKAGE__->has_many(
  "basic_info_reference_db_links",
  "CIPRO::Schema::BasicInfoReferenceDbLink",
  { "foreign.reference_db_id" => "self.id" },
);
__PACKAGE__->belongs_to(
  "external_db_id",
  "CIPRO::Schema::ExternalDb",
  { id => "external_db_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hWuJaUxoxNQJUX8pAvgkCQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
