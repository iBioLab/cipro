## @class 
# DBアクセス接続を実施するクラス。
#
package CIPRO::Service::Base;

use lib "../../../lib";
use CIPRO::Config;
use CIPRO::Schema;

sub new {
    my $class = shift;
    bless {},$class;
}

## @method string connect ()
#
# DBアクセス情報を取得し、DBへ接続するメソッド    
#
# @return DB接続結果
#
sub connect{
    my $self = shift;
    my $conf = CIPRO::Config->new();
    my $source = $conf->get_database_source_name();
    my $user = $conf->get_database_user();
    my $pass = $conf->get_database_pass();
    return CIPRO::Schema->connect($source, $user, $pass, {AutoCommit => 1});
}

1;
