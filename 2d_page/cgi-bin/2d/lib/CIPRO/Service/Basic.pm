## @class 
#  basic_infoテーブルを検索するクラス。
#
package CIPRO::Service::Basic;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;
use CIPRO::Service::Type;

@CIPRO::Service::Basic::ISA = qw(CIPRO::Service::Base);


## @method long getIdByCipro ()
#
# basic_info.idの取得    
#
# @param basic_info.cipro
# @param schema(optional)　schemaの指定が無い場合はメソッド内でconnect()
#
# @return 取得ID、存在しないbasic_info.ciproの場合は0
#
sub getIdByCipro{
    my $self = shift;
    my $cipro = shift;
    my $schema = shift || $self->connect;
    my $rs = $schema->resultset('BasicInfo');
    my $rows = 
            $rs->search({cipro => $cipro, deletedate => \'is null'},{columns => [qw/ id /]});
    if($rows->count == 0) {
        return 0;
    } else {
        return $rows->single->id;
    }
}

## @method string searchBySeqLength ()
#
# basic_infoテーブルアクセスを実施し、
# seq_infoテーブルの seqフィールドレングスを指定して条件に一致するbasic_infoデータを取得する
#
# @param[in] lf seqフィールドレングス最小値
# @param[in] lt seqフィールドレングス最大値
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchBySeqLength{
    my $self = shift;
    my $schema = $self->connect;
    my $lf = shift;
    my $lt = shift;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;
    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM seq_info WHERE length(seq) BETWEEN ".$lf." AND ".$lt." AND deletedate is null) AND deletedate is null" ,{ page => $page , rows => $limit });

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}
## @method string countBySeqLength ()
#
# basic_infoテーブルアクセスを実施し、
# seq_infoテーブルの seqフィールドレングスを指定して条件に一致するbasic_infoデータ件数を取得する
# @param[in] lf seqフィールドレングス最小値
# @param[in] lt seqフィールドレングス最大値
# @return データ件数
#
sub countBySeqLength{
    my $self = shift;
    my $schema = $self->connect;
    my $lf = shift;
    my $lt = shift;
    if(($lf eq '')||($lt eq '')){
        return 0;
    }
    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM seq_info WHERE length(seq) BETWEEN ".$lf." AND ".$lt." AND deletedate is null) AND deletedate is null" );
    return $basicInfo->count;
}

## @method string searchByMwAndPi ()
#
# basic_infoテーブルアクセスを実施し、
# seq_infoテーブルの mw、piを指定して条件に一致するbasic_infoデータを取得する
# @param[in] mwf mw最小値
# @param[in] mwt mw最大値
# @param[in] pif pi最小値
# @param[in] pit pi最大値
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchByMwAndPi{
    my $self = shift;
    my $schema = $self->connect;
    my $mwf = shift;
    my $mwt = shift;
    my $pif = shift;
    my $pit = shift;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;
    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM seq_info WHERE mw > ".$mwf." AND mw < ".$mwt." AND pi > ".$pif." AND pi < ".$pit." AND deletedate is null) AND deletedate is null" ,{ page => $page , rows => $limit });

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}
## @method string countByMwAndPi ()
#
# basic_infoテーブルアクセスを実施し、
# seq_infoテーブルの mw、piを指定して条件に一致するbasic_infoデータ件数を取得する
# @param[in] mwf mw最小値
# @param[in] mwt mw最大値
# @param[in] pif pi最小値
# @param[in] pit pi最大値
# @return データ件数
#
sub countByMwAndPi{
    my $self = shift;
    my $schema = $self->connect;
    my $mwf = shift;
    my $mwt = shift;
    my $pif = shift;
    my $pit = shift;
    if(($mwf eq '')||($mwt eq '')||($pif eq '')||($pit eq '')){
        return 0;
    }
    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM seq_info WHERE mw > ".$mwf." AND mw < ".$mwt." AND pi > ".$pif." AND pi < ".$pit." AND deletedate is null) AND deletedate is null");
    return $basicInfo->count;
}

## @method string search ()
#
# basic_infoテーブルデータを取得する
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub search{
    my $self = shift;
    my $schema = $self->connect;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;
    my $basicInfo = $schema->resultset('BasicInfo')->search( " deletedate is null" ,{ page => $page , rows => $limit});

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}

## @method string count ()
#
# basic_infoテーブルデータ件数を取得する
# @return データ件数
#
sub count{
    my $self = shift;
    my $schema = $self->connect;
    my $basicInfo = $schema->resultset('BasicInfo')->search( " deletedate is null" );
    return $basicInfo->count;
}

## @method string searchEstByTag ()
#
# resultsテーブルの sub_type_info=est 内フィールドを指定し、
# 条件に一致するbasic_infoテーブルデータを取得する
# @param[in] est est内の任意フィールド名
# @param[in] EST est内の全フィールド名
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchEstByTag{
    my ($self,$est,$EST,$limit,$offset) = @_;
    my $schema = $self->connect;
    my $page = $offset / $limit + 1;
    my @est = split /\+/, $est;
    my @EST = split /\+/, $EST;

    my $results_sub_query 
            = "IN(".makeSubQuery4EstByTag($schema, @est).")";

    my $basicInfo 
            = $schema->resultset('BasicInfo')->search({
                                                          id => \$results_sub_query,
                                                          deletedate => \'is null'
                                                        },
                                                        {
                                                          page => $page, 
                                                          rows => $limit,
                                                          order_by => 'cipro',
                                                        });
    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}

## @method string countEstByTag ()
#
# resultsテーブルの sub_type_info=est 内フィールドを指定し、条件に一致するデータの件数を取得する
# @param[in] est est内の任意フィールド名
# @param[in] EST est内の全フィールド名
# @return basic_infoテーブルの条件に一致したデータの件数
#
sub countEstByTag{
    my ($self,$est,$EST) = @_; 
    my $schema = $self->connect;
    my @est = split /\+/, $est;
    my @EST = split /\+/, $EST;

    my $results_sub_query 
            = "IN(".makeSubQuery4EstByTag($schema, @est).")";

    my $basicInfo 
            = $schema->resultset('BasicInfo')->search({
                                                          id => \$results_sub_query,
                                                          deletedate => \'is null'
                                                        },
                                                        {
                                                          columns => [qw/ id /]
                                                        });
    return $basicInfo->count;
}

## @method string makeSubQuery4EstByTag ()
#
# searchEstByTag()、countEstByTag()で使用するresults検索用のサブクエリを生成する。
# @param[in] $schema connect済みCIPRO::Schema
# @return サブクエリ
#
sub makeSubQuery4EstByTag {
    my($schema, @est) = @_;

    my $service = CIPRO::Service::Type->new;

    my $programtype_id 
            = $service->getProgramTypeIdByType("Expression Profile", $schema);

    my $subtypeinfo_id 
            = $service->getSubTypeInfoIdByType("est", $schema);

    my $selest = join "+", map{"cast(((xpath('/res/detail/$_/text()', res))[1]::text) as double precision)"} @est;
    my $totest = "cast(((xpath('/res/sum/text()', res))[1]::text) as double precision)";
    my $specify = join " AND ", map{"(cast(((xpath('/res/detail/$_/text()', res))[1]::text) as double precision) > 0)"} @est;

    return " SELECT basic_info_id".
            " FROM results".
            " WHERE TRUE".
            " AND program_type_id = ".$programtype_id.
            " AND sub_type_info_id = ".$subtypeinfo_id.
            " AND ".$specify.
            " AND (".$selest.")/(".$totest.") > 0.9".
            " AND (".$totest." != 0)".
            " AND deletedate is null";
}

## @method string searchSeqByMwAndPi ()
#
# basic_infoテーブルアクセスを実施し、
# seq_infoテーブルの mw、piの条件を指定し、一致するbasic_infoを取得する
# @param[in] pi0 任意のpi条件値
# @param[in] mw0 任意のmw条件値
# @param[in] r2 任意のpi,mw MAX条件値
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchSeqByMwAndPi{
    my ($self,$pi0,$mw0,$r2,$limit,$offset) = @_; 
    my $schema = $self->connect;
    my $page = $offset / $limit + 1;

    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM seq_info WHERE 
                     (pow((pi-".$pi0."),2)+pow((mw-".$mw0."),2) < ".$r2.")  AND deletedate is null)  AND deletedate is null" ,
                     { page => $page , rows => $limit });

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}


## @method string countSeqByMwAndPi ()
#
# basic_infoテーブルアクセスを実施し、
# seq_infoテーブルの mw、piの条件を指定し、条件に一致するデータの件数を取得する
# @param[in] pi0 任意のpi条件値
# @param[in] mw0 任意のmw条件値
# @param[in] r2 任意のpi,mw MAX条件値
# @return basic_infoテーブルの配列
#
sub countSeqByMwAndPi{
    my ($self,$pi0,$mw0,$r2) = @_; 
    my $schema = $self->connect;

    if(($pi0 eq '')||($mw0 eq '')||($r2 eq '')){
        return 0;
    }
    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM seq_info WHERE 
                     (pow((pi-".$pi0."),2)+pow((mw-".$mw0."),2) < ".$r2.")  AND deletedate is null)  AND deletedate is null");
    return $basicInfo->count;
}

## @method string searchByAcc ()
#
# basic_infoテーブルデータを取得する
# @param[in] acc Hit_accessionの文字列
# @param[in] evalue Hsp_evalueの最大値
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchByAcc{
    my $self = shift;
    my $schema = $self->connect;
    my $acc = shift;
    my $evalue = shift;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;

    my $type = 'BLAST';
    my $sub  = 'nr';
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $blastp_subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    $sub  = 'nt';
    my $tblastn_subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;

    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM results WHERE ".
    " deletedate IS NULL AND (sub_type_info_id = ".$blastp_subtypeinfo_id." OR sub_type_info_id = ".$tblastn_subtypeinfo_id." ) AND program_type_id = ".$programtype_id.
    " AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < ".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])" ,
                     { page => $page , rows => $limit });

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}

## @method string countByAcc ()
#
# @param[in] acc Hit_accessionの文字列
# @param[in] evalue Hsp_evalueの最小値
# basic_infoテーブルデータ件数を取得する
# @return データ件数
#
sub countByAcc{
    my $self = shift;
    my $schema = $self->connect;
    my $acc = shift;
    my $evalue = shift;
    my $type = 'BLAST';
    my $sub  = 'nr';
    if(($acc eq '')||($evalue eq '')){
        return 0;
    }
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $blastp_subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    $sub  = 'nt';
    my $tblastn_subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    my $basicInfo = $schema->resultset('BasicInfo')->search( "id IN (SELECT basic_info_id FROM results WHERE ".
    " deletedate IS NULL AND (sub_type_info_id = ".$blastp_subtypeinfo_id." OR sub_type_info_id = ".$tblastn_subtypeinfo_id." ) AND program_type_id = ".$programtype_id.
    " AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < ".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])");
    return $basicInfo->count;
}

## @method string searchByDevelopmentalHPF ()
#
# basic_infoテーブルデータを取得する
# @param[in] start DevelopmentalHPFStartの値
# @param[in] end DevelopmentalHPFEndの値
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchByDevelopmentalHPF{
    my $self   = shift;
    my $schema = $self->connect;
    my $type   = "3DPL";
    my $start  = shift;
    my $end    = shift;
    my $limit  = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $resultsres = $schema->resultset('Results')->search(
                  " program_type_id = ".$programtype_id.
                  " AND cast((xpath('/res/DevelopmentalHPFStart/text()' , res))[1]::text as double precision ) = ".$start.
                  " AND cast((xpath('/res/DevelopmentalHPFEnd/text()' , res))[1]::text as double precision ) = ".$end.
                  " AND deletedate IS NULL" );
    my @ret = ();
    if($resultsres->count > 0){
        my $basicinfo_ids = join "," , map{ my $id = $_->get_column('basic_info_id'); "$id";} $resultsres->all;
        my $basicInfo = $schema->resultset('BasicInfo')->search("id IN (".$basicinfo_ids.") AND deletedate is null" , { page => $page , rows => $limit});
        map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    }
    return @ret;
}

## @method string countByDevelopmentalHPF ()
#
# @param[in] start DevelopmentalHPFStartの値
# @param[in] end DevelopmentalHPFEndの値
# basic_infoテーブルデータ件数を取得する
# @return データ件数
#
sub countByDevelopmentalHPF{
    my $self   = shift;
    my $schema = $self->connect;
    my $type   = "3DPL";
    my $start  = shift;
    my $end    = shift;
    if(($start eq '')||($end eq '')){
        return 0;
    }
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $resultsres = $schema->resultset('Results')->search(
                  " program_type_id = ".$programtype_id.
                  " AND cast((xpath('/res/DevelopmentalHPFStart/text()' , res))[1]::text as double precision ) = ".$start.
                  " AND cast((xpath('/res/DevelopmentalHPFEnd/text()' , res))[1]::text as double precision ) = ".$end.
                  " AND deletedate IS NULL" );
    if($resultsres->count > 0){
        my $basicinfo_ids = join "," , map{my $id = $_->get_column('basic_info_id'); "$id";} $resultsres->all;
        my $basicInfo = $schema->resultset('BasicInfo')->search("id IN (".$basicinfo_ids.") AND deletedate is null");
        return $basicInfo->count;
    }else{
        return 0;
    }
}

## @method string searchByCiproIds ()
#
# basic_infoテーブルデータを取得する
# @param[in] cipro CIPRO IDのカンマ区切り文字列
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $cipro = shift;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;
    my $basicInfo = $schema->resultset('BasicInfo')->search( "cipro IN (".$cipro.") AND deletedate is null" ,{ page => $page , rows => $limit});

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}

## @method string countByCiproIds ()
#
# basic_infoテーブルデータ件数を取得する
# @param[in] cipro CIPRO IDのカンマ区切り文字列
# @return データ件数
#
sub countByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $cipro = shift;
    if($cipro eq ''){
        return 0;
    }
    my $basicInfo = $schema->resultset('BasicInfo')->search( "cipro IN (".$cipro.") AND deletedate is null" );
    return $basicInfo->count;
}



## @method string searchByAccAndOmim ()
#
# external_dbテーブルから、omimのidを取得し、
# reference_dbテーブルから、任意の値を含むdataをもつxidを取得する。
# 次に、resultsテーブルから、program_type=BLAST、sub_type_info=nr,nt、
# Hit_accession=xidに該当するbasic_info_idを取得し、
# basic_infoテーブルから、ciproフィールドを取得する
# @param[in] evalue Hsp_evalueの最大値
# @param[in] ID reference_dbテーブルのdataフィールドに含まれる任意の値
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルのciproフィールド
#
sub searchByAccAndOmim{
    my $self = shift;
    my $schema = $self->connect;
    my $evalue = shift;
    my $ID = shift;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;

    my $externalDbId = $schema->resultset('ExternalDb')->search({ name => 'omim' , deletedate => \'is null'})->single->id;

    my $basicInfo = $schema->resultset('BasicInfo')->search("id IN
                                 (SELECT basic_info_id FROM basic_info_reference_db_link
                                 WHERE reference_db_id IN
                                 (SELECT id FROM reference_db WHERE external_db_id =".$externalDbId." AND xid = '".$ID."' AND 
                                 cast((xpath('/omim/evalue/text()' ,data))[1]::text as double precision) < ".$evalue." AND deletedate is null)
                                  AND deletedate is null)",{ page => $page , rows => $limit } );

    my @ret = ();
    map { my $id = $_->cipro; my $row = [$id];push @ret, $row;} $basicInfo->all;
    return @ret;
}




## @method string countByAccAndOmim ()
#
# external_dbテーブルから、omimのidを取得し、
# reference_dbテーブルから、任意の値を含むdataをもつxidを取得する。
# 次に、resultsテーブルから、program_type=BLAST、sub_type_info=nr,nt、
# Hit_accession=xidに該当するbasic_info_idを取得し、
# basic_infoテーブルから、ciproフィールドを取得する
# @param[in] evalue Hsp_evalueの最大値
# @param[in] ID reference_dbテーブルのdataフィールドに含まれる任意の値
# @return basic_infoテーブルのデータ件数
#
sub countByAccAndOmim{
    my $self = shift;
    my $schema = $self->connect;
    my $evalue = shift;
    my $ID = shift;

    if(($evalue eq '')||($ID eq '')){
        return 0;
    }
    my $externalDbId = $schema->resultset('ExternalDb')->search({ name => 'omim' , deletedate => \'is null'})->single->id;

    my $basicInfo = $schema->resultset('BasicInfo')->search("id IN
                                 (SELECT basic_info_id FROM basic_info_reference_db_link
                                 WHERE reference_db_id IN
                                 (SELECT id FROM reference_db WHERE external_db_id =".$externalDbId." AND xid = '".$ID."' AND 
                                 cast((xpath('/omim/evalue/text()' ,data))[1]::text as double precision) < ".$evalue." AND deletedate is null)
                                  AND deletedate is null)");

    return $basicInfo->count;
}


## @method string searchByText ()
#
# basic_infoテーブルデータを取得する
# @param[in] cipro CIPRO IDのカンマ区切り文字列
# @param[in] limit 取得データ数
# @param[in] offset 取得データオフセット
# @return basic_infoテーブルの配列
#
sub searchByText{
    my $self = shift;
    my $schema = $self->connect;
    my $text = shift;
    my $limit = shift;
    my $offset = shift;
    my $page = $offset / $limit + 1;
    #urldecode
    $text = url_decode($text);
    #トリミング
    $text = trim($text);
    #連続スペース文字を1つに
    $text =~s/\s+/ /g;
    #スペースを"&"に置換
    $text =~s/\s/\&/g;
    
    $text =~ s/'/''''/g;         # ←「'」を「''」に置換
    $text =~ s/\\/\\\\\\\\/g;    # ←「\」を「\\」に置換
    my $docs      = $schema->resultset('Docs')->search( "vector @@ to_tsquery('''$text''')" 
                                                       , { select => ["id","rank(vector, to_tsquery('''$text'''))"],
                                                           as     => [qw/cipro rank/] ,
                                                           order_by=>"rank(vector, to_tsquery('''$text''')) DESC",
                                                           page   => $page ,
                                                           rows   => $limit});
    my @ret = ();
    map { my $id = $_->get_column('cipro'); my $rank = $_->get_column('rank');my $row = [$id,$rank];push @ret, $row;} $docs->all;
    return @ret;
}

## @method string countByText ()
#
# basic_infoテーブルデータ件数を取得する
# @param[in] cipro CIPRO IDのカンマ区切り文字列
# @return データ件数
#
sub countByText{
    my $self = shift;
    my $schema = $self->connect;
    my $text = shift;
    #urldecode
    $text = url_decode($text);
    #トリミング
    $text = trim($text);
    if($text eq ''){
        return 0;
    }
    #連続スペース文字を1つに
    $text =~s/\s+/ /g;
    #スペースを"&"に置換
    $text =~s/\s/\&/g;
    $text =~ s/'/''''/g;         # ←「'」を「''」に置換
    $text =~ s/\\/\\\\\\\\/g;    # ←「\」を「\\」に置換
    my $docs      = $schema->resultset('Docs')->search( "vector @@ to_tsquery('''$text''')");
    return $docs->count;
}

## @method string trim()
#
# 文字列前後の空白を除去
# @param[in] 文字列
# @return 空白を除去した文字列
#
sub trim {
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
}

## @method string url_decode()
#
# urlencodeされた文字列をurldecodeする
# @param[in] urlencode文字列
# @return urldecodeした文字列
#
sub url_decode {
  my $str = shift;
  $str =~ tr/+/ /;
  $str =~ s/%([0-9A-Fa-f][0-9A-Fa-f])/pack('H2', $1)/eg;
  return $str;
}

1;
