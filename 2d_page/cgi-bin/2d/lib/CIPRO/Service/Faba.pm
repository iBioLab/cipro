## @class 
#  3DPL用のデータを検索するクラス。
#
package CIPRO::Service::Faba;

use lib "../../../lib";
use CIPRO::Config;
use CIPRO::Service::Base;
use CIPRO::View::Faba;
use XML::TreePP;

@CIPRO::Service::Faba::ISA = qw(CIPRO::Service::Base);


## @method string searchByCiproId ()
#
# Resultsテーブルアクセスを実施し、
# CIPRO IDと一致するデータをresフィールドのXMLを解析したデータを取得する
# @param[in] ciproId CIPRO ID
# @return ResultsテーブルのresデータをXML解析したデータ
#
sub searchByCiproId{
    my $faba = CIPRO::View::Faba->new;
    my $self = shift;
    my $ciproId = shift;
    my $type = '3DPL';
    my $schema = $self->connect;
    my $basicinfo_dt    = $schema->resultset('BasicInfo')->search({cipro => $ciproId , deletedate => \'is null'});
    if($basicinfo_dt->count > 0){
        my $basicinfo_id   = $basicinfo_dt->next;
        my $programtype_dt = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'});
        if($programtype_dt->count > 0){
            my $programtype_id = $programtype_dt->next;
            my $data = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id->id
                        , program_type_id => $programtype_id->id , deletedate => \'is null'});

            if($data->count > 0){
                my $tpp  = XML::TreePP->new();
                my $tree = $tpp->parse( $data->next->res );
                $faba->set_ExpressedTissue(         $tree->{'res'}->{'ExpressedTissue'} );
                $faba->set_SubcellularLocalization( $tree->{'res'}->{'SubcellularLocalization'} );
                $faba->set_Description(             $tree->{'res'}->{'Description'} );
                $faba->set_ExperimentalConditions(  $tree->{'res'}->{'ExperimentalConditions'} );
                $faba->set_Channel(                 $tree->{'res'}->{'Channel'} );
                $faba->set_MagnificationScale(      $tree->{'res'}->{'MagnificationScale'} );
                return $faba;
            }
        }
    }
    return undef($faba);
}


## @method string searchFilenameByCiproId ()
#
# movieテーブルアクセスを実施し、
# filenameフィールドの値を取得する
# @param[in] id CIPRO ID
# @return movieテーブルのfilenameフィールド
#
sub searchFilenameByCiproId{
    my $self = shift;
    my ($id,$seq) = @_;
    my $type = "3DPL";
    my $schema = $self->connect;
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $results_id = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id
          , program_type_id => $programtype_id , deletedate => \'is null'})->single->id;
    my $filename;
    my $result = $schema->resultset('Movie')->search({results_id => $results_id , seqno => $seq , deletedate => \'is null'});
    if($result->count > 0){
        my $conf = CIPRO::Config->new();
        my $moviepath = $conf->get_3dpl_movie_root_path();
        if(substr($moviepath, -1) != '/'){
            $moviepath = $moviepath . '/';
        }
        $filename = $moviepath."CIPRO".$id."/".$result->single->filename;
    }
    return $filename;
}

## @method string searchAllPictureByCiproId ()
#
# Pictureテーブルアクセスを実施し、
# CIPRO IDと一致するデータを全て取得する
# @param[in] id CIPRO ID
# @return Pictureテーブルの配列
#
sub searchAllPictureByCiproId{
    my $self = shift;
    my ($id) = @_;
    my $type = "3DPL";
    my $schema = $self->connect;
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $results_id = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id
          , program_type_id => $programtype_id , deletedate => \'is null'})->single->id;
    return $schema->resultset('Picture')->search({results_id => $results_id , deletedate => \'is null'});
}

1;
