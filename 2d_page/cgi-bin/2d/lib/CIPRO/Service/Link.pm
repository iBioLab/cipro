## @class 
# ciprolinkテーブルのデータ追加、削除、更新を実行するクラス。
#
package CIPRO::Service::Link;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;

@CIPRO::Service::Link::ISA = qw(CIPRO::Service::Base);


## @method　string search()
#
# ciprolinkの有効なデータを取得するメソッド
#
# @param なし
# @return \@ciprolink
#
sub search{
    my $self = shift;
    my $schema = $self->connect;
    my $ret = $schema->resultset('Ciprolink')->search({deletedate => \'is null'});
    if($ret->count > 0){
        return $ret;
    }else{
        return undef($ret);
    }
}

## @method \@ciprolink selectAll()
#
# ciprolinkの有効なデータ全てを取得するメソッド
#
# @param なし
# @return \@ciprolink
#
sub selectAll {
    my ($self) = @_;
    my $schema = $self->connect;
    my $link_data = $schema->resultset('Ciprolink')->search({deletedate => \'is null'},{order_by => 'id ASC'});
    return $link_data;
}

## @method $ciprolink selectById ()
#
# 指定IDのciprolinkのデータを取得するメソッド
#
# @param $id    ID
# @return $ciprolink
#
sub selectById {
    my ($self, $id) = @_;
    my $schema = $self->connect;
    my $link_data = $schema->resultset('Ciprolink')->find({id => $id});
    return $link_data;
}

## @method $ciprolink selectById ()
#
# 指定IDのciprolinkのデータを更新するメソッド
#
# @param $id    ID
# @param $name  URL名
# @param $url  URL
# @param $category  カテゴリー名（"Related links" or "Ciona links"）
# @return $ciprolink
#
sub updateById {
    my ($self, $id, $name, $url, $category) = @_;
    my $schema = $self->connect;
    my $link_data = selectById($self,$id);
    if ($link_data) {
        $link_data->update({name => $name,url => $url,category => $category})
    }
    return $link_data;
}

## @method $ciprolink add ()
#
# ciprolinkのデータを新規追加するメソッド
#
# @param $name  URL名
# @param $url  URL
# @param $category  カテゴリー名（"Related links" or "Ciona links"）
# @return $ciprolink
#
sub add {
    my ($self, $name, $url, $category) = @_;
    my $schema = $self->connect;
    my $link_data = $schema->resultset('Ciprolink')->create({name => $name,url => $url,category => $category});
    return $link_data;
}

## @method $ciprolink deleteById ()
#
# ciprolinkのデータを削除するメソッド
#
# @param $id  ID
# @return $ciprolink
#
sub deleteById {
    my ($self, $id) = @_;
    my $schema = $self->connect;
    my $link_data = selectById($self,$id);
    if ($link_data) {
        $link_data->delete();
    }
    return $link_data;
}


1;


















1;
