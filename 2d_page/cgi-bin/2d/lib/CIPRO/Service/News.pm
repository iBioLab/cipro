## @class 
# cipronewsテーブルの一番新しい更新データを取得するクラス。
#
package CIPRO::Service::News;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;

@CIPRO::Service::News::ISA = qw(CIPRO::Service::Base);

my $schema = CIPRO::Service::Base->connect;

## @method string selectNews ()
#
# cipronewsテーブルアクセスを実施し、
# 一番新しい更新データ取得するメソッド
#
# @return cipronewsテーブルの データ
#
sub selectNews{
    my $data = $schema->resultset('Cipronews')->search({deletedate => \'is null', show => 'true'},{order_by => 'createdate DESC LIMIT 10'});

    return $data;
}

## @method \@cipronews selectAll()
#
# cipronewsの有効なデータ全てを取得するメソッド
#
# @param なし
# @return \@cipronews
#
sub selectAll {
    my ($self) = @_;
    my $schema = $self->connect;
    my $news_data = $schema->resultset('Cipronews')->search({deletedate => \'is null'},{order_by => 'id ASC'});
    return $news_data;
}

## @method $cipronews selectById ()
#
# 指定IDのcipronewsのデータを取得するメソッド
#
# @param $id    ID
# @return $cipronews
#
sub selectById {
    my ($self, $id) = @_;
    my $schema = $self->connect;
    my $news_data = $schema->resultset('Cipronews')->find({id => $id});
    return $news_data;
}

## @method $cipronews selectById ()
#
# 指定IDのcipronewsのデータを更新するメソッド
#
# @param $id    ID
# @param $news  ニューステキスト
# @return $cipronews
#
sub updateById {
    my ($self, $id, $news, $show) = @_;
    my $schema = $self->connect;
    my $news_data = selectById($self, $id);
    if ($news_data) {
        $news_data->update({news => $news, show => $show})
    }
    return $news_data;
}

## @method $cipronews add ()
#
# cipronewsのデータを新規追加するメソッド
#
# @param $news  ニューステキスト
# @return $cipronews
#
sub add {
    my ($self, $news) = @_;
    my $schema = $self->connect;
    my $news_data = 
                $schema->resultset('Cipronews')->create({news => $news});
    return $news_data;
}

## @method $cipronews deleteById ()
#
# cipronewsのデータを削除するメソッド
#
# @param $id  ID
# @return $cipronews
#
sub deleteById {
    my ($self, $id) = @_;
    my $schema = $self->connect;
    my $news_data = selectById($self,$id);
    if ($news_data) {
        $news_data->delete();
    }
    return $news_data;
}

1;
