## @class 
# reference_dbテーブルの xid, dataフィールドを取得するクラス。
# external_dbテーブルの resフィールドを取得するクラス。
#
package CIPRO::Service::Omim;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;

@CIPRO::Service::Omim::ISA = qw(CIPRO::Service::Base);


## @method string searchByExternalDbNameAndXids ()
#
# DB接続→external_dbテーブルアクセス→reference_dbテーブルアクセスを実施し、
# データを取得するメソッド
#
# @return reference_dbテーブル
#
sub searchByExternalDbNameAndXids{
    my $self = shift;
    my $name = shift;
    my $xid = shift;
    my $schema = $self->connect;
    my $externaldb_id    = $schema->resultset('ExternalDb')->search({name => $name , deletedate => \'is null'})->single->id;
    my $list = $schema->resultset('ReferenceDb')->search("xid IN (".$xid.") AND external_db_id = ".$externaldb_id." AND deletedate IS NULL");
    my @ret = ();
    map { my $data = $_->data; my $xid = $_->xid; my $row = [$data,$xid];push @ret, $row;} $list->all;
    return @ret;
}

## @method string searchXidByCiproIdAndExternalDbName ()
#
# DB接続→external_dbテーブルアクセス→reference_dbテーブルアクセスを実施し、
# xidを取得するメソッド
#
# @return reference_dbテーブルのxidフィールド
#
sub searchXidByCiproIdAndExternalDbName{
    my $self = shift;
    my $id = shift;
    my $name = shift;
    my $schema = $self->connect;
    my @ret = ();
    my $basicinfo_id = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $externaldb_id = $schema->resultset('ExternalDb')->search({name => $name , deletedate => \'is null'})->single->id;
    my $results = $schema->resultset('BasicInfoReferenceDbLink')->search({basic_info_id => $basicinfo_id , deletedate => \'is null'});

    if($results->count > 0){
        my $referencedb_ids = join "," , map{$id = $_->get_column('reference_db_id'); "$id";} $results->all;
        my $list = $schema->resultset('ReferenceDb')->search("id IN (".$referencedb_ids.") AND external_db_id = ".$externaldb_id." AND deletedate IS NULL");
        map { my $xid = $_->xid; my $row = [$xid];push @ret, $row;} $list->all;
    }
    return @ret;
}

## @method string searchByCiproIdAndExternalDbName ()
#
# DB接続→external_dbテーブルアクセス→reference_dbテーブルアクセスを実施し、
# xid、dataを取得するメソッド
#
# @return reference_dbテーブルのxidフィールド
#
sub searchByCiproIdAndExternalDbName{
    my $self = shift;
    my $id = shift;
    my $name = shift;
    my $schema = $self->connect;
    my @ret = ();
    my $basicinfo_id = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $externaldb_id = $schema->resultset('ExternalDb')->search({name => $name , deletedate => \'is null'})->single->id;
    my $results = $schema->resultset('BasicInfoReferenceDbLink')->search({basic_info_id => $basicinfo_id , deletedate => \'is null'});

    if($results->count > 0){
        my $referencedb_ids = join "," , map{$id = $_->get_column('reference_db_id'); "$id";} $results->all;
        my $list = $schema->resultset('ReferenceDb')->search("id IN (".$referencedb_ids.") AND external_db_id = ".$externaldb_id." AND deletedate IS NULL");
        map { my $xid = $_->xid; my $data = $_->data; my $row = [$xid,$data];push @ret, $row;} $list->all;
    }
    return @ret;
}

1;
