## @class 
#  permsテーブルを検索するクラス。
#
package CIPRO::Service::Perms;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;

@CIPRO::Service::Perms::ISA = qw(CIPRO::Service::Base);

## @method string searchByMw ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# 任意のCIPRO IDのseq_infoテーブルのデータを取得するメソッド。 
#
# @param[in] w mwの最大最小値を決定するための任意データ
# @param[in] acc mwの最大最小値を決定するための任意データ
# @return basic_infoテーブルのciproフィールド と permsテーブルのmwフィールドの配列
#

sub searchByBetweenMw{
    my $self = shift;
    my ($w, $acc) = @_;
    my $x = $w - $acc;
    my $y = $w + $acc;
    my $schema = $self->connect;

    my $permsResult = $schema->resultset('Perms')->search({mw => {-BETWEEN => ["$x","$y",]} , deletedate => \'is null'});
    my @ret = ();
    my $count = 0;
    if($permsResult->count > 0){
        while( my $row = $permsResult->next){
            my $perms_id  = $row->id;
            my $perms_mod = $row->mod;
            my $perms_mw  = $row->mw;
            my $results = $schema->resultset('PermsSeqInfoLink')->search({perms_id => $perms_id , deletedate => \'is null'});
            if($results->count > 0){
                while( my $seqrow = $results->next ){
                    my $seqinfo = $schema->resultset('SeqInfo')->search({id => $seqrow->get_column('seq_info_id'), deletedate => \'is null'});
                    my $cipro = $seqinfo->single->basic_info_id->cipro;
                    $ret[$count] = [$perms_mw,$cipro,$perms_mod];
                    $count++;
                }
            }
        }
    }
    return @ret;
}

## @method string searchByCiproIdAndMw ()
#
# basic_info, seq_info, perms_seq_info_link, permsテーブルアクセスを実施し、
# 任意のCIPRO IDとpermsテーブルのmwの値を条件にデータを取得するメソッド。 
#
# @param[in] id CIPRO ID
# @param[in] mw mw比較値
# @return basic_infoテーブルのciproフィールド と permsテーブルのseqフィールドの配列
#
sub searchByCiproIdAndMw{
    my $self = shift;
    my ($id,$mw) = @_;
    my $schema = $self->connect;
    my @ret = ();
    my $result = $schema->resultset('BasicInfo')->search({cipro => $id, deletedate => \'is null'});
    $result = $schema->resultset('SeqInfo')->search({basic_info_id => $result->single->id , deletedate => \'is null'});
    $result = $schema->resultset('PermsSeqInfoLink')->search({seq_info_id => $result->single->id , deletedate => \'is null'});
    if($result->count > 0){
        my $permsIds = join "," , map{ my $id = $_->get_column('perms_id'); "$id";} $result->all;
        $result = $schema->resultset('Perms')->search("id IN (".$permsIds.") AND mw::numeric = ".$mw."::numeric AND deletedate IS NULL");
        map { my $seq = $_->seq; my $row = [$id,$seq];push @ret, $row;} $result->all;
    }
    return @ret;
}

1;


