## @class 
# resultsテーブルの resフィールドを取得するクラス。
#
package CIPRO::Service::Res;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;
use CIPRO::Service::Type;
use CIPRO::Service::Basic;

@CIPRO::Service::Res::ISA = qw(CIPRO::Service::Base);

## @method string searchByCiproId ()
#
# DB接続→basic_infoテーブルアクセス→program_typeテーブルアクセス
# sub_type_infoテーブルアクセス→resultsテーブルアクセスを実施し、
# resフィールドを取得するメソッド
#
# @return resultsテーブルの resフィールド
#
sub searchByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my ($id, $type,$sub) = @_;
    my $res = '';
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $result;
    if(length($sub) > 0){
        my $subtypeinfo_id = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                      , sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
    }else{
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                       , deletedate => \'is null'});
    }
    if($result->count > 0){
        $res = $result->single->res;
    }
    return $res;
}

## @method string searchReserveByCiproId ()
#
# DB接続→basic_infoテーブルアクセス→program_typeテーブルアクセス
# sub_type_infoテーブルアクセス→resultsテーブルアクセスを実施し、
# reserveフィールドを取得するメソッド
#
# @return resultsテーブルの reserveフィールド
#
sub searchReserveByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my ($id, $type,$sub) = @_;
    my $reserve = '';
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $result;
    if(length($sub) > 0){
        my $subtypeinfo_id = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                        , sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
    }else{
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                       , deletedate => \'is null'});
    }
    if($result->count > 0){
        $reserve = $result->single->reserve;
    }
    return $reserve;
}

## @method string searchRandomByProgramAndSubTypeId ()
#
# DB接続→program_typeテーブルアクセス
# sub_type_infoテーブルアクセス→resultsテーブルアクセスを実施し、
# ランダムに1件basic_info_idフィールドを取得→basic_infoテーブル
# からciproフィールドを取得する
# @return basic_infoテーブルの ciproフィールド
#
sub searchRandomByProgramAndSubTypeId{
    my $self = shift;
    my $schema = $self->connect;
    my ($type,$sub) = @_;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    my $basicinfo_id    = $schema->resultset('Results')->search({ program_type_id => $programtype_id , sub_type_info_id => $subtypeinfo_id
                                                                , deletedate => \'is null'} ,
                                                                { order_by => 'random()' , page => 1 , rows => 1 } )->first->basic_info_id->id;
    return $schema->resultset('BasicInfo')->search({id => $basicinfo_id , deletedate => \'is null'})->single->cipro;
}

## @method string searchPdbByCiproId ()
#
# cphのresフィールドを取得する。
# pdbファイルのデータが<res>タグで括られているので
# <res>タグのテキスト部のみ取得。
# @return resultsテーブルの resフィールド
#
sub searchPdbByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id   = shift;
    my $type = shift;
    my $sub  = shift;
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    my $result          = $schema->resultset('Results')->search({ basic_info_id => $basicinfo_id ,
                                                                  program_type_id => $programtype_id ,
                                                                  sub_type_info_id => $subtypeinfo_id ,
                                                                  deletedate => \'is null'} ,
                                                                { select => ["(xpath('/res/text()' , res))[1]::text"],
                                                                  as     => ["pdb"] } );
    return $result->single->get_column('pdb');
}

## @method string searchEstByCiproId ()
#
# resultsテーブルのestデータを取得する
# @param[in] cipro CIPRO ID
# @return resultsテーブルのestデータ
#
sub searchEstByCiproId{
    my @EST = qw/eg cl gn tb lv em jv jm ad gd ts es nc ht bd dg ma/;
    my @selest = ();
    push @selest , map{"( xpath('/res/detail/$_/text()' , res ) )[1]::text"} @EST;
    my ($self,$cipro) = @_;
    my $schema = $self->connect;

    my $basicService = CIPRO::Service::Basic->new;
    my $basicinfo_id 
            = $basicService->getIdByCipro($cipro, $schema);

    my $typeService = CIPRO::Service::Type->new;
    my $programtype_id 
            = $typeService->getProgramTypeIdByType("Expression Profile", $schema);

    my $subtypeinfo_id 
            = $typeService->getSubTypeInfoIdByType("est", $schema);
=pod
    print "basicinfo_id   = ".$basicinfo_id,"\n";
    print "programtype_id = ".$programtype_id,"\n";
    print "subtypeinfo_id = ".$subtypeinfo_id,"\n";
=cut
    my $results 
            = $schema->resultset('Results')->search({
                                                        basic_info_id    => $basicinfo_id,
                                                        program_type_id  => $programtype_id, 
                                                        sub_type_info_id => $subtypeinfo_id,
                                                        deletedate       => \'is null',
                                                      },
                                                      { 
                                                        select => [@selest],
                                                        as     => [@EST]
                                                      });
    my @ret = ();
    map {
        @ret = ($_->get_column($EST[0]),
                $_->get_column($EST[1]),
                $_->get_column($EST[2]),
                $_->get_column($EST[3]),
                $_->get_column($EST[4]),
                $_->get_column($EST[5]),
                $_->get_column($EST[6]),
                $_->get_column($EST[7]),
                $_->get_column($EST[8]),
                $_->get_column($EST[9]),
                $_->get_column($EST[10]),
                $_->get_column($EST[11]),
                $_->get_column($EST[12]),
                $_->get_column($EST[13]),
                $_->get_column($EST[14]),
                $_->get_column($EST[15]),
                $_->get_column($EST[16]) );
    } $results->all;
    return @ret;
}

1;
