## @class 
#  seq_infoテーブルを検索するクラス。
#
package CIPRO::Service::Seq;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;

@CIPRO::Service::Seq::ISA = qw(CIPRO::Service::Base);

## @method string searchByCiproId ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# 任意のCIPRO IDのseq_infoテーブルのデータを取得するメソッド。 
#
# @return seq_infoテーブルデータ
#

sub searchByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $basicInfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->next;
    my $seq_info = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next;
    my $seq_info_seq = $seq_info->seq;
    my $seq_info_mw = $seq_info->mw;
    my $seq_info_pi = $seq_info->pi;
    my @seq_info = ([$seq_info_seq,$seq_info_mw,$seq_info_pi]);
    return @seq_info;
}

## @method string searchLengthByCiproId ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# seq_infoテーブルの seqフィールドレングスとidフィールドを取得するメソッド
#
# @return seq_infoテーブルの seqフィールドレングスとidフィールドの配列
#                                         
sub searchLengthByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $basicInfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->next;
    my $seq_info_seq = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next->seq;
    my $seq_info_id = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next->id;
    my $seq_length = length($seq_info_seq);
    my @seq_info = ([$id,$seq_length]);
    return @seq_info;
}

## @method string searchEflagByCiproId ()
#
# basic_info, seq_infoテーブルアクセスを実施。
# basic_infoテーブルの eflagフィールドが'1'か'null'かを判定し、
# それぞれ、't'、'f'を出力する。
# seq_infoテーブルの seqフィールドがMから始まるデータは、't'を
# それ以外は、'f'出力する。
# 
# @return seq_infoテーブルid、eflag、seq判定結果の配列
#
sub searchEflagByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $basicInfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->next;
    my $seq_info_id = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next->id;
    my $seq_info = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next->seq;
    my $basicInfo_eflag;
    my $seq_info_sel;

    if($basicInfo->eflag){
        $basicInfo_eflag = '1';
    }else{
        $basicInfo_eflag = '0';
    }

    if($seq_info=~ /^M/){
        $seq_info_sel = '1';
    }else{
        $seq_info_sel = '0';
    }

    my @seq = ([$id,$seq_info_sel,$basicInfo_eflag]);
    return @seq;
}

## @method string searchMwByCiproId ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# seq_infoテーブルの mw,piフィールドとbasic_infoテーブルの ciproフィールドを取得するメソッド
#
# @return seq_infoテーブルの mw,piフィールドとbasic_infoテーブルの ciproフィールドの配列
#                                         
sub searchMwByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $basicInfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->next;
    my $seq_info  = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next;
    my $seq_info_mw = $seq_info->mw;
    my $seq_info_pi = $seq_info->pi;
    my @seq_info = ([$id,$seq_info_mw,$seq_info_pi]);
    return @seq_info;
}

sub searchXseqByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $basicInfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->next;
    my $seq_info  = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next;
    my $seq_info_sha256 = $seq_info->sha256;
    my $basic_info_ids = $schema->resultset('SeqInfo')->search({sha256 => $seq_info_sha256,deletedate => \'is null'});
    my @cipro_ids = ();
    my $count = 0;
    while(my $row = $basic_info_ids->next){
      my $cipro_id = $row->basic_info_id->cipro;
      next if($cipro_id eq $id);
      $cipro_ids[$count] = $cipro_id;
      $count++;
    }
    return @cipro_ids;
}

## @method string searchLengthByCiproIds ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# seq_infoテーブルの seqフィールドレングスとidフィールドを取得するメソッド
#
# @return seq_infoテーブルの seqフィールドレングスとbasic_infoテーブルの ciproフィールドの配列
#                                         
sub searchLengthByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $cipro = shift;
    my $count = 0;
    my @seq_info = ();
    my $basicInfo = $schema->resultset('BasicInfo')->search("cipro IN (".$cipro.") AND deletedate is null");
    while(my $row = $basicInfo->next){
        my $seq_info_seq = $row->seq_infoes->search({deletedate => \'is null'})->single->seq;
        my $seq_length = length($seq_info_seq);
        $seq_info[$count] = [$row->cipro,$seq_length];
        $count++;
    }
    return @seq_info;
}

## @method string searchEflagByCiproIds ()
#
# basic_info, seq_infoテーブルアクセスを実施。
# basic_infoテーブルの eflagフィールドが'1'か'null'かを判定し、
# それぞれ、't'、'f'を出力する。
# seq_infoテーブルの seqフィールドがMから始まるデータは、't'を
# それ以外は、'f'出力する。
#
# @return seq_infoテーブルの eflag、seq判定結果とbasic_infoテーブルの ciproフィールドの配列
#                                         
sub searchEflagByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $cipro = shift;
    my $count = 0;
    my @seq_info = ();
    my $basicInfo = $schema->resultset('BasicInfo')->search("cipro IN (".$cipro.") AND deletedate is null");
    while(my $row = $basicInfo->next){
        my $seq_info_seq = $row->seq_infoes->search({deletedate => \'is null'})->single->seq;
        my $basicInfo_eflag = '0';
        my $seq_info_sel    = '0';
        if($row->eflag){
            $basicInfo_eflag = '1';
        }
        if($seq_info_seq=~ /^M/){
            $seq_info_sel    = '1';
        }
        $seq_info[$count] = [$row->cipro,$seq_info_sel,$basicInfo_eflag];
        $count++;
    }
    return @seq_info;
}

## @method string searchMwByCiproIds ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# seq_infoテーブルの mw,piフィールドとbasic_infoテーブルの ciproフィールドを取得するメソッド
#
# @return seq_infoテーブルの mw,piフィールドとbasic_infoテーブルの ciproフィールドの配列
#                                         
sub searchMwByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $cipro = shift;
    my $count = 0;
    my @seq_info = ();
    my $basicInfo = $schema->resultset('BasicInfo')->search("cipro IN (".$cipro.") AND deletedate is null");
    while(my $row = $basicInfo->next){
        my $seq_info_seq = $row->seq_infoes->search({deletedate => \'is null'})->single;
        my $seq_info_mw = $seq_info_seq->mw;
        my $seq_info_pi = $seq_info_seq->pi;
        $seq_info[$count] = [$row->cipro,$seq_info_mw,$seq_info_pi];
        $count++;
    }
    return @seq_info;
}

## @method string searchBlastpByCiproId ()
#
# resultsテーブルから、BLASTPのデータでHit_defタグに"Ciona intestinalis"文字列を持つ
# データを取得し、acc,source,protein,score,evalueを取得するメソッド
#
# @return resultsテーブルの resフィールドを解析した結果のacc,source,protein,score,evalueの配列
#
sub searchBlastpByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my @blastp_info = ();
    my $type = 'BLAST';
    my $sub  = 'nr';
    my $basicinfo_id  = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;

    my $basicInfo = $schema->resultset('Results')->search( "basic_info_id = ".$basicinfo_id.
                                                      " AND program_type_id = ".$programtype_id.
                                                      " AND sub_type_info_id = ".$subtypeinfo_id.
                                                      " AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"Ciona intestinalis\")]/Hit_def/text()' , res)::text[] > ARRAY['']".
                                                      " AND deletedate IS NULL",
                                                      { select => ["(xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"Ciona intestinalis\")]/Hit_accession/text()' , res))[1]::text",
                                                                   "(xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"Ciona intestinalis\")]/Hit_def/text()' , res))[1]::text",
                                                                   "(xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"Ciona intestinalis\")]/Hit_hsps/Hsp/Hsp_bit-score/text()' , res))[1]::text",
                                                                   "(xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"Ciona intestinalis\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text"
                                                                   ],
                                                        as     => [qw/acc def score evalue/] });
    if($basicInfo->count > 0){
        my $acc = $basicInfo->single->get_column('acc');
        my $def = $basicInfo->single->get_column('def');
        my $st = index($def, "[");
        my $en = index($def, "]");
        my $protein = substr($def , 0 , $st);
        my $source = substr($def , $st , $en - $st + 1);
        my $score = $basicInfo->single->get_column('score');
        my $evalue = $basicInfo->single->get_column('evalue');
        $blastp_info[0] = [$id,$acc,$source,$protein,$score,$evalue];
    }
    return @blastp_info;
}

## @method string searchBlastpByCiproIds ()
#
# 複数のCIPRO ID を対象としてresultsテーブルから、BLASTPのデータでHit_defタグに"Ciona intestinalis"文字列を持つ
# データを取得し、acc,source,protein,score,evalueを取得するメソッド
#
# @return resultsテーブルの resフィールドを解析した結果のacc,source,protein,score,evalueの配列
#
sub searchBlastpByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $ciproId = shift;
    $ciproId =~ s/'//g;
    my @cipro = split /,/, $ciproId;
    my @blastp_info = ();
    my $count = 0;
    foreach my $value ( @cipro ){
        my @tmp = searchBlastpByCiproId($self,$value);
        if(@tmp){
            $blastp_info[$count] = $tmp[0];
            $count++;
        }
    }

    return @blastp_info;
}


## @method string searchEstByCiproId ()
#
# resultsテーブルから、estのデータを取得する。
#
# @return basic_infoテーブルの ciproフィールドの配列
#
sub searchEstByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $type = 'Expression Profile';
    my $sub = 'est';
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;

    my @cipro = ();
    my $results = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
            , sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
    if($results->count > 0){
        $cipro[0] = [$id];
    }
    return @cipro;
}


## @method string searchEstByCiproIds ()
#
# 複数のCIPRO ID を対象としてresultsテーブルから、estのデータを取得する。
#
# @return basic_infoテーブルの ciproフィールドの配列
#
sub searchEstByCiproIds{
    my $self = shift;
    my $schema = $self->connect;
    my $ciproId = shift;
    $ciproId =~ s/'//g;
    my @cipro = split /,/, $ciproId;
    my @blastp_info = ();
    my $count = 0;
    foreach my $value ( @cipro ){
        my @tmp = searchEstByCiproId($self,$value);
        if(@tmp){
            $blastp_info[$count] = $tmp[0];
            $count++;
        }
    }
    return @blastp_info;

}

## @method string searchSeqByCiproId ()
#
# basic_info, seq_infoテーブルアクセスを実施し、
# 任意のCIPRO IDのseq_infoテーブルのseqを取得するメソッド。 
#
# @return seq_infoテーブルデータ
#

sub searchSeqByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $basicInfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single;
    my $seq_info = $basicInfo->seq_infoes->search({deletedate => \'is null'})->next;
    my $seq_info_seq = $seq_info->seq;
    my @seq_info = ([$seq_info_seq]);
    return @seq_info;
}

1;


