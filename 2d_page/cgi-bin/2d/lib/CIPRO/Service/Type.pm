## @class 
# program_type,sub_type_infoのサービスクラス
#
package CIPRO::Service::Type;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;

@CIPRO::Service::Type::ISA = qw(CIPRO::Service::Base);

## @method long getProgramTypeIdByType ()
#
# program_type.idの取得    
#
# @param program_type.type
# @param schema(optional)　schemaの指定が無い場合はメソッド内でconnect()
#
# @return 取得ID、存在しないprogram_type.typeの場合は0
#
sub getProgramTypeIdByType{
    my $self = shift;
    my $type = shift;
    my $schema = shift || $self->connect;
    my $rs = $schema->resultset('ProgramType');
    my $rows = 
            $rs->search({type=> $type, deletedate => \'is null'},{columns => [qw/ id /]});
    if($rows->count == 0) {
        return 0;
    } else {
        return $rows->single->id;
    }
}

## @method long getSubTypeInfoIdByType ()
#
# sub_type_info.idの取得    
#
# @param sub_type_info.type
# @param schema(optional)　schemaの指定が無い場合はメソッド内でconnect()
#
# @return 取得ID、存在しないsub_type_info.typeの場合は0
#
sub getSubTypeInfoIdByType{
    my $self = shift;
    my $type = shift;
    my $schema = shift || $self->connect;
    my $rs = $schema->resultset('SubTypeInfo');
    my $rows = 
            $rs->search({type=> $type, deletedate => \'is null'},{columns => [qw/ id /]});
    if($rows->count == 0) {
        return 0;
    } else {
        return $rows->single->id;
    }
}

1;
