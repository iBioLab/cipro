## @class 
#  ユーザー関連のサービスを提供するクラス
#
package CIPRO::Service::User;

use strict;
use warnings;
use lib "../../../lib";

use CIPRO::Service::Base;
use CIPRO::Schema::Users;

@CIPRO::Service::User::ISA = qw(CIPRO::Service::Base);

sub new {
    my $class = shift;
    bless {},$class;
}

## @method long isAuthUser ()
#
# @param $userId　ユーザーID(ログインID)
# @param $passwd　パスワード(ログインパスワード)
#
# @return 1    ユーザー認証成功
#         0    ユーザー認証失敗
#
sub isAuthUser() {
    my ($self, $userId, $passwd) = @_;
    my $schema = $self->connect;
    my $userData = 
            $schema->resultset('Users')
                            ->search({login_id => $userId , deletedate => \'is null'})->single;
    if (!$userData) {
        return 0;
    }
    else {
        if(crypt($passwd, $userData->login_password) eq $userData->login_password) {
            return 1;
        }
        else {
           return 0;
        }
    }
}
1;
