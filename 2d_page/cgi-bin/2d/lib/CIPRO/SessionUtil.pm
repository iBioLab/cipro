## @class 
#  セッション管理ユーティリティクラス
#
#  セッションIDの取得・消去は本クラスの中で完結させて
#  セッションIDの文字列など、他のスクリプトでは一切意識
#  しないようにする.
#
package CIPRO::SessionUtil;

use strict;
use warnings;
use lib "../../lib";

use CGI::Session qw/-ip_match/;
use CGI::Cookie;
use CIPRO::Config;

my $config = CIPRO::Config->new();

sub new {
    my $class = shift;
    bless {},$class;
}

## @method CGI::Cookie createNewSession()
#
# @param なし
#
# @return セッションIDを含む CGI::Cookieオブジェクト
#
sub createNewSession() {
    my ($self) = @_;
    my $session = CGI::Session->new('driver:file',undef,{Directory => $config->get_session_out_dir()});
    $session->expire($config->get_session_expires());
    my $cookie = CGI::Cookie->new(
        -name    => $config->get_session_sid_name(),
        -value   => $session->id,
    );
    return $cookie;
}

## @method void clearSessionIdFromCGI ()
#
# @param $cgi  CGIオブジェクト
#
# @return なし
#
sub clearSessionIdFromCGI() {
    my ($self, $cgi) = @_;
    my $sid = $cgi->cookie($config->get_session_sid_name()) || $cgi->param($config->get_session_sid_name()) || undef;
    my $session = CGI::Session->load('driver:file', $sid, {Directory => $config->get_session_out_dir()});
    if ($session->is_expired) {
    } else {
        $session->close();
        $session->delete();
    }
    return;
}

## @method long isValidSessionId ()
#
# @param $cgi CGIオブジェクト
#
# @return 1    セッションIDは有効
#         0    セッションIDは無効
#
sub isValidSessionId() {
    my ($self, $cgi) = @_;
#    return 1;
    my $sid = $cgi->cookie($config->get_session_sid_name()) || $cgi->param($config->get_session_sid_name()) || undef;
    my $session = CGI::Session->new('driver:file', $sid, {Directory => $config->get_session_out_dir()});
    if(defined $sid && $sid eq $session->id){
        return 1;
    }
    elsif(defined $sid && $sid ne $session->id){
        $session->close;
        $session->delete;
        return 0;
    }
    else{
        return 0;
    }
}

## @method void getSessionIdIdFromCGI ()
#
# @param $cgi  CGIオブジェクト
#
# @return なし
#
sub getSessionIdIdFromCGI() {
    my ($self, $cgi) = @_;
    my $sid = $cgi->cookie($config->get_session_sid_name()) || $cgi->param($config->get_session_sid_name()) || undef;
    return $sid;
}

1;
