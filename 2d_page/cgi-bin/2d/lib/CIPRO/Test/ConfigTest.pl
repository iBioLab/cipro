use strict;
use warnings;
use lib "../../../lib";

use CIPRO::Config;

my $config = CIPRO::Config->new();

#[database]
#source_name=dbi:Pg:dbname=cipro;host=192.168.11.250
#user=cipro
#pass=cipro

#[2dpage]
#icon_dir_path=./images/icon/2d_img

#[3dpl]
#icon_dir_path=./images/icon/3dpl

#[est]
#icon_dir_path=./images/icon/est_img/png

#[cphmodels]
#icon_dir_path=./images/icon/pdb/img

#[smart]
#icon_dir_path=./images/icon/smart_img

#[sosui]
#icon_dir_path=./images/icon/sosui

#[wolfpsort]
#icon_dir_path=./images/icon/wolfpsort/png

#[session]
#sid_name=CGISESSID
#out_dir=/tmp
#expires=+1m

print "get_database_source_name()    = ".$config->get_database_source_name(),"\n";
print "get_database_user()           = ".$config->get_database_user(),"\n";
print "get_database_pass()           = ".$config->get_database_pass(),"\n";

print "get_2dpage_icon_dir_path()    = ".$config->get_2dpage_icon_dir_path(),"\n";
print "get_3dpl_icon_dir_path()      = ".$config->get_3dpl_icon_dir_path(),"\n";
print "get_est_icon_dir_path()       = ".$config->get_est_icon_dir_path(),"\n";
print "get_cphmodels_icon_dir_path() = ".$config->get_cphmodels_icon_dir_path(),"\n";
print "get_sosui_icon_dir_path()     = ".$config->get_sosui_icon_dir_path(),"\n";
print "get_smart_icon_dir_path()     = ".$config->get_smart_icon_dir_path(),"\n";
print "get_wolfpsort_icon_dir_path() = ".$config->get_wolfpsort_icon_dir_path(),"\n";

print "get_session_sid_name()        = ".$config->get_session_sid_name(),"\n";
print "get_session_out_dir()         = ".$config->get_session_out_dir(),"\n";
print "get_session_expires()         = ".$config->get_session_expires(),"\n";

