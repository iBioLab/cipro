# Hidden Entries
# file name: hiddenEntries.txt

# List below all your entries' accession numbers to be hidden from public access.
# Any line beginning with a '#' character is ignored.

# You may list several accession numbers per line (separated by spaces or tabs).
#
# e.g.
# P12345	Q99TY2


P12345
Q99TY2 
