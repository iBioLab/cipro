# Hidden Spots
# file name: hiddenSpots.txt

# This file controls whether some spots, associated to specific entries, are to be hidden
# Data identification can also be hidden from within this file
# Any line beginning with a '#' character is ignored.

# The format to hide a spot association to an entry is (separated by spaces or tabs):
#
# Accession_number	Gel_shortname	Spot_ID
#
# To hide *all* spots associated to one entry in a particular gel, replace 'Spot_ID' by '*'
#
# e.g.
# P12345	PLASMA	397


# The format to hide a specific spot identification data is (separated by spaces or tabs):
#
# Accession_number	Gel_shortname	Spot_ID 	Identification_Method
#
# Where Identification_Method is one of: PMF, MS/MS or Aa (respectively, mass fingerprinting, tandem MS and amino acid)
#
# To hide a specific 'Identification Method' on all/one entries in a particular gel, replace 'Accession_number' by '*' and/or 'Spot_ID' by '*'
#
# e.g.
# P12345	PLASMA	397	MS/MS
# P12345	PLASMA	*  	MS/MS
# *     	PLASMA	*  	MS/MS
