/* $RCSfile$
 * $Author: hansonr $
 * $Date: 2007-07-06 00:49:41 +0900 (金, 06  7月 2007) $
 * $Revision: 7944 $
 *
 * Copyright (C) 2002-2005  The Jmol Development Team
 *
 * Contact: jmol-developers@lists.sf.net
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.jmol.modelset;

import java.util.BitSet;
import java.util.Vector;
import java.util.Hashtable;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;



public class Polymer {

  /*
   * this is a new class of "polymer" that does not necessarily have anything
   * created from it. Jmol can use it instead of any bioPolymer subclass, since
   * there are now no references to any bio-related classes in Viewer. 
   * 
   * 
   */
  protected Model model;

  // these arrays will be one longer than the polymerCount
  // we probably should have better names for these things
  // holds center points between alpha carbons or sugar phosphoruses
  protected Point3f[] leadMidpoints;
  protected Point3f[] leadPoints;
  protected Point3f[] tempPoints;
  // holds the vector that runs across the 'ribbon'
  protected Vector3f[] wingVectors;

  protected int[] leadAtomIndices;

  protected Polymer() {
  }

  public int getPolymerPointsAndVectors(int last, BitSet bs, Vector vList,
                                        boolean isTraceAlpha,
                                        float sheetSmoothing) {
    return 0;
  }

  public void addSecondaryStructure(byte type, char startChainID,
                                    int startSeqcode, char endChainID,
                                    int endSeqcode) {
  }

  public void calculateStructures() {
  }

  public String getSequence() {
    return "";
  }

  public Hashtable getPolymerInfo(BitSet bs) {
    return null;
  }

  public void setConformation(BitSet bsConformation, int nAltLocs) {
  }

  public void calcHydrogenBonds(BitSet bsA, BitSet bsB) {
  }

  public void calcSelectedMonomersCount(BitSet bsSelected) {
  }

  public void getPolymerSequenceAtoms(int iModel, int iPolymer, int group1,
                                      int nGroups, BitSet bsInclude,
                                      BitSet bsResult) {
  }

  public Point3f[] getLeadMidpoints() {
    return null;
  }
  
  public void getPdbData(char ctype, boolean isDerivative, BitSet bsAtoms, StringBuffer pdbATOM, StringBuffer pdbCONECT) {
    return;
  }

}
