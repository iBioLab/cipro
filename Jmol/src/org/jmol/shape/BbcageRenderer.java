/* $RCSfile$
 * $Author: hansonr $
 * $Date: 2007-11-07 14:15:03 +0900 (水, 07 11月 2007) $
 * $Revision: 8575 $
 *
 * Copyright (C) 2002-2006  Miguel, Jmol Development, www.jmol.org
 *
 * Contact: miguel@jmol.org
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.jmol.shape;

import org.jmol.viewer.StateManager;
import javax.vecmath.Point3i;

public class BbcageRenderer extends FontLineShapeRenderer {

  final Point3i[] screens = new Point3i[8];
  {
    for (int i = 8; --i >= 0; )
      screens[i] = new Point3i();
  }

  protected void render() {
    short mad = viewer.getObjectMad(StateManager.OBJ_BOUNDBOX);
    if (mad == 0 || !isGenerator && !g3d.checkTranslucent(false)
        || viewer.isJmolDataFrame())
      return;
    colix = viewer.getObjectColix(StateManager.OBJ_BOUNDBOX);
    render(mad, modelSet.getBboxVertices(), screens, null, 0);
  }
  
}
