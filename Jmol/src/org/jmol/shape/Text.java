/* $RCSfile$
 * $Author: egonw $
 * $Date: 2005-11-10 09:52:44 -0600 (Thu, 10 Nov 2005) $
 * $Revision: 4255 $
 *
 * Copyright (C) 2002-2005  The Jmol Development Team
 *
 * Contact: jmol-developers@lists.sf.net
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.jmol.shape;

import java.awt.FontMetrics;
import javax.vecmath.Point3f;

import org.jmol.api.JmolRendererInterface;
import org.jmol.g3d.Font3D;
import org.jmol.g3d.Graphics3D;
import org.jmol.util.Escape;
import org.jmol.viewer.JmolConstants;
import org.jmol.viewer.Viewer;
import org.jmol.util.TextFormat;

public class Text {

  public final static int POINTER_NONE = 0;
  public final static int POINTER_ON = 1;
  public final static int POINTER_BACKGROUND = 2;
  
  private final static String[] hAlignNames = {"", "left", "center", "right", ""};

  final static int XY = 0;
  final static int LEFT = 1;
  final static int CENTER = 2;
  final static int RIGHT = 3;
  final static int XYZ = 4;

  final static String[] vAlignNames = {"xy", "top", "bottom", "middle"};

  final static int TOP = 1;
  final static int BOTTOM = 2;
  final static int MIDDLE = 3;

  boolean atomBased;
  Viewer viewer;
  JmolRendererInterface g3d;
  Point3f xyz;
  String target;
  String text, textUnformatted;
  String script;
  
  boolean doFormatText;
  
  String[] lines;
  int align;
  int valign;
  int pointer;
  int movableX;
  int movableY;
  int movableXPercent = Integer.MAX_VALUE;
  int movableYPercent = Integer.MAX_VALUE;
  int offsetX;
  int offsetY;
  int z;
  int zSlab; // z for slabbing purposes -- may be near an atom

  int windowWidth;
  int windowHeight;
  boolean adjustForWindow;
  int boxX, boxY, boxWidth, boxHeight;
  
  int modelIndex = -1;
  boolean visible = true;
  
  Font3D font;
  FontMetrics fm;
  byte fid;
  int ascent;
  int descent;
  int lineHeight;

  short colix;
  short bgcolix;

  int[] widths;
  int textWidth;
  int textHeight;

  // for labels and hover
  Text(JmolRendererInterface g3d, Font3D font, String text, short colix,
      short bgcolix, int offsetX, int offsetY, int z, int zSlab, int textAlign) {
    this.viewer = null;
    this.g3d = g3d;
    atomBased = true;
    setText(text);
    this.colix = colix;
    this.bgcolix = bgcolix;
    setXYZs(offsetX, offsetY, z, zSlab);
    align = textAlign;
    setFont(font);
  }

  // for echo
  Text(Viewer viewer, Graphics3D g3d, Font3D font, String target, short colix, int valign, int align) {
    this.viewer = viewer;
    this.g3d = g3d;
    atomBased = false;
    this.target = target;
    if (target.equals("error"))
      valign = TOP; 
    this.align = align;
    this.valign = valign;
    this.font = font;
    this.colix = colix;
    this.z = 2;
    this.zSlab = Integer.MIN_VALUE;
    getFontMetrics();
  }

  void getFontMetrics() {
    fm = font.fontMetrics;
    descent = fm.getDescent();
    ascent = fm.getAscent();
    lineHeight = ascent + descent;
  }

  void setFid(byte fid) {
    if (this.fid == fid)
      return;
    this.fid = fid;
    recalc();
  }

  void setModel(int modelIndex) {
    this.modelIndex = modelIndex;
  }
 
  void setVisibility(boolean TF) {
    visible = TF;
  }
 
  void setXYZ(Point3f xyz) {
    valign = XYZ;
    this.xyz = xyz;
    setAdjustForWindow(false);
  }
  
  void setAdjustForWindow(boolean TF) {
    adjustForWindow = TF;
  }
  
  void setColix(short colix) {
    this.colix = colix;
  }

  void setColix(Object value) {
    colix = Graphics3D.getColix(value);
  }

  void setTranslucent(float level, boolean isBackground) {
    if (isBackground) {
      if (bgcolix != 0)
        bgcolix = Graphics3D.getColixTranslucent(bgcolix, !Float.isNaN(level), level);
    } else {
      colix = Graphics3D.getColixTranslucent(colix, !Float.isNaN(level), level);
    }
  }
  
  
  void setBgColix(short colix) {
    this.bgcolix = colix;
  }

  void setBgColix(Object value) {
    bgcolix = (value == null ? (short) 0 : Graphics3D.getColix(value));
  }

  void setMovableX(int x) {
    valign = (valign == XYZ ? XYZ : XY);
    movableX = x;
    movableXPercent = Integer.MAX_VALUE;
  }

  void setMovableY(int y) {
    valign = (valign == XYZ ? XYZ : XY);
    movableY = y;
    movableYPercent = Integer.MAX_VALUE;
  }
  
  void setMovableXPercent(int x) {
    valign = (valign == XYZ ? XYZ : XY);
    movableX = Integer.MAX_VALUE;
    movableXPercent = x;
  }

  void setMovableYPercent(int y) {
    valign = (valign == XYZ ? XYZ : XY);
    movableY = Integer.MAX_VALUE;
    movableYPercent = y;
  }
  
  void setXY(int x, int y) {
    setMovableX(x);
    setMovableY(y);
  }

  void setZs(int z, int zSlab) {
    this.z = z;
    this.zSlab = zSlab;
  }

  void setXYZs(int x, int y, int z, int zSlab) {
    setMovableX(x);
    setMovableY(y);
    setZs(z, zSlab);
  }

  void setScript(String script) {
    this.script = (script == null || script.length() == 0 ? null : script);
  }
  
  String getScript() {
    return script;
  }
  
  void setOffset(int offset) {
    //Labels only
    offsetX = getXOffset(offset);
    offsetY = getYOffset(offset);
    valign = XY;
  }

  final static int getXOffset(int offset) {
    switch (offset) {
    case 0:
      return JmolConstants.LABEL_DEFAULT_X_OFFSET;
    case Short.MAX_VALUE:
      return 0;
    default:
      return (byte) (offset >> 8);
    }
  }

  final static int getYOffset(int offset) {
    switch (offset) {
    case 0:
      return -JmolConstants.LABEL_DEFAULT_Y_OFFSET;
    case Short.MAX_VALUE:
      return 0;
    default:
      return -(int)((byte) (offset & 0xFF));
    }
  }

  void setText(String text) {
    text = fixText(text);
    if (this.text != null && this.text.equals(text))
      return;
    this.text = text;
    textUnformatted = text;
    doFormatText = (viewer != null && text != null 
        && (text.indexOf("%{") >= 0 || text.indexOf("@{") >= 0));
    if (!doFormatText)
      recalc();
  }

  void setFont(Font3D f3d) {
    font = f3d;
    getFontMetrics();
    recalc();
  }

  boolean setAlignment(String align) {
    if ("left".equals(align))
      return setAlignment(LEFT);
    if ("center".equals(align))
      return setAlignment(CENTER);
    if ("right".equals(align))
      return setAlignment(RIGHT);
    return false;
  }

  static String getAlignment(int align) {
    return hAlignNames[align & 3];
  }
  
  boolean setAlignment(int align) {
    this.align = align;
    recalc();
    return true;
  }

  void setPointer(int pointer) {
    this.pointer = pointer;
  }
  
  static String getPointer(int pointer) {
    return ((pointer & POINTER_ON) == 0 ? ""
        : (pointer & POINTER_BACKGROUND) > 0 ? "background" : "on");
  }
  
  String fixText(String text) {
    if (text == null || text.length() == 0)
      return null;
    int pt;
    while ((pt = text.indexOf("\n")) >= 0)
      text = text.substring(0, pt) + "|" + text.substring(pt + 1);
    return text;
  }
  
  void recalc() {
    if (text == null) {
      text = null;
      lines = null;
      widths = null;
      return;
    }
    if (fm == null)
      return;
    lines = TextFormat.split(text, '|');
    textWidth = 0;
    widths = new int[lines.length];
    for (int i = lines.length; --i >= 0;)
      textWidth = Math.max(textWidth, widths[i] = stringWidth(lines[i]));
    textHeight = lines.length * lineHeight;
    boxWidth = textWidth + 8;
    boxHeight = textHeight + 8;
  }

  private void formatText() {
    text = (viewer == null ? textUnformatted : 
      viewer.formatText(textUnformatted));
    recalc();
  }
  
  void render(JmolRendererInterface g3d, boolean antialias) {
    if (text == null)
      return;
    windowWidth = g3d.getRenderWidth();
    windowHeight = g3d.getRenderHeight();

    if (doFormatText)
      formatText();
    setPositions(antialias);

    // adjust positions if necessary

    setBoxOffsetsInWindow(antialias);


    // draw the box if necessary

    if (bgcolix != 0 && g3d.setColix(bgcolix))
      showBox(g3d, colix, bgcolix, boxX, boxY, z + 2, zSlab, 
        boxWidth + (antialias ? boxWidth : 0), 
        boxHeight + (antialias ? boxHeight : 0), antialias, 
        atomBased);
    if (g3d.setColix(colix)) {

      // now set x and y positions for text from (new?) box position

      int offset = (antialias ? boxWidth << 1 : boxWidth);
      int adj = (antialias ? 8 : 4);
      int x0 = boxX;
      switch (align) {
      case CENTER:
        x0 += offset / 2;
        break;
      case RIGHT:
        x0 += offset - adj;
        break;
      default:
        x0 += adj;
      }

      // now write properly aligned text

      int x = x0;
      int y = boxY + ascent + 4;
      offset = lineHeight;
      int nShift = 1;
      if (antialias) {
        y += ascent + 4;
        offset <<= 1;
        nShift = 2;
      }
      for (int i = 0; i < lines.length; i++) {
        switch (align) {
        case CENTER:
          x = x0 - (widths[i] * nShift / 2);
          break;
        case RIGHT:
          x = x0 - (widths[i] * nShift);
        }
        g3d.drawString(lines[i], font, x, y, z, zSlab);
        y += offset;
      }
    }

    
    // now draw the pointer, if requested

    if ((pointer & POINTER_ON) != 0) {
      g3d
          .setColix((pointer & POINTER_BACKGROUND) != 0 && bgcolix != 0 ? bgcolix
              : colix);
      if (boxX > movableX)
        g3d.drawLine(movableX, movableY, zSlab, boxX, boxY + boxHeight / 2,
            zSlab);
      else if (boxX + boxWidth < movableX)
        g3d.drawLine(movableX, movableY, zSlab, boxX + boxWidth, boxY
            + boxHeight / 2, zSlab);
    }
  }

  private void setPositions(boolean antialias) {
    int xLeft, xCenter, xRight;
    if (valign == XY || valign == XYZ) {
      int x = (movableXPercent == Integer.MAX_VALUE ?  movableX 
          : movableXPercent * windowWidth / 100);
      int offsetX = this.offsetX;
      if (antialias)
        offsetX <<= 1;
      xLeft = xRight = xCenter = x + offsetX;
    } else {
      xLeft = (antialias ? 10 : 5);
      xCenter = windowWidth / 2;
      xRight = windowWidth - xLeft;
    }
    
    // set box X from alignments
    
      boxX = xLeft;
      switch (align) {
      case CENTER:
        boxX = xCenter - (antialias ? boxWidth : boxWidth / 2); 
        break;
      case RIGHT:
        boxX = xRight - (antialias ? boxWidth * 2 : boxWidth);        
      }
    
    // set box Y from alignments
    
    boxY = 0;
    switch (valign) {
    case TOP:
      break;
    case MIDDLE:
      boxY = windowHeight / 2;
      break;
    case BOTTOM:
      boxY = windowHeight;
      break;
    default:
      int y = (movableYPercent == Integer.MAX_VALUE ?  movableY 
          : movableYPercent * windowHeight / 100);
      boxY = (atomBased|| xyz != null ? y : (windowHeight - y)) + offsetY;
      if (antialias)
        boxY += offsetY;
    }
  }
  
  void setBoxOffsetsInWindow(boolean antialias) {
    int xAdj = 0;
    int yAdj = 0;
    if (!adjustForWindow)
      yAdj -= (antialias ? lineHeight * 2 : lineHeight);
    if (atomBased && align == XY) {
      xAdj += JmolConstants.LABEL_DEFAULT_X_OFFSET;
      yAdj -= JmolConstants.LABEL_DEFAULT_Y_OFFSET + 4;
    }
    if (valign == XYZ) {
      yAdj += ascent / 2;
    }
    if (antialias) {
      xAdj <<= 1;
      yAdj <<= 1;
    }
    boxX += xAdj;
    boxY += yAdj;
    if (adjustForWindow) {
      // not labels

      // these coordinates are (0,0) in top left
      // (user coordinates are (0,0) in bottom left)
      int xMargin = (antialias ? 10 : 5);
      int bw = (antialias ? boxWidth << 1: boxWidth) + xMargin;
      int x = boxX;
      if (x + bw > windowWidth)
        x = windowWidth - bw;
      if (x < xMargin)
        x = xMargin;
      boxX = x;
      
      int y = boxY;
      y -= (antialias ? textHeight << 1 : textHeight);
      int bh = (antialias ? boxHeight << 1 : boxHeight);
      if (y + bh > windowHeight)
        y = windowHeight - bh;
      int y0 = (atomBased ? (16 + lineHeight) * (antialias ? 2 : 1) : 0);
      if (y < y0)
        y = y0;
      boxY = y;
    }
  }

  private static void showBox(JmolRendererInterface g3d,
                              short colix, short bgcolix, 
                       int x, int y, int z, int zSlab, 
                       int boxWidth, int boxHeight, 
                       boolean antialias, boolean atomBased) {
    //System.out.println("showBox bgcolix x y z " + bgcolix + " " + x + " " + y + " " + z);
    g3d.fillRect(x, y, z, zSlab, boxWidth, boxHeight);
    g3d.setColix(colix);
    if (!atomBased)
      return;
    if (antialias) {
      g3d.drawRect(x + 3, y + 3, z - 1, zSlab, 
          boxWidth - 6, boxHeight - 6);
      g3d.drawRect(x + 4, y + 4, z - 1, zSlab, 
          boxWidth - 8, boxHeight - 8);
    } else {
      g3d.drawRect(x + 1, y + 1, z - 1, zSlab, 
        boxWidth - 2, boxHeight - 2); 
    }
  }

  final static void renderSimple(JmolRendererInterface g3d, Font3D font,
                                 String strLabel, short colix, short bgcolix,
                                 int x, int y, int z, int zSlab, int xOffset,
                                 int yOffset, int ascent, int descent,
                                 boolean doPointer, short pointerColix,
                                 boolean antialias) {

    // old static style -- quick, simple, no line breaks, odd alignment?
    // LabelsRenderer only
    
    int x0 = x;
    int y0 = y;
    int boxWidth = font.fontMetrics.stringWidth(strLabel) + 8;
    int boxHeight = ascent + descent + 8;
    int xBoxOffset, yBoxOffset;
    
    // these are based on a standard |_ grid, so y is reversed.
    if (xOffset > 0) {
      xBoxOffset = xOffset;
    } else {
      xBoxOffset = -boxWidth;
      if (xOffset == 0)
        xBoxOffset /= 2;
      else
        xBoxOffset += xOffset;
    }

    if (yOffset > 0) {
      yBoxOffset = yOffset;
    } else {
      if (yOffset == 0)
        yBoxOffset = -boxHeight / 2 - 2;
      else
        yBoxOffset = -boxHeight + yOffset;
    }

    x += xBoxOffset;
    y += yBoxOffset;

    int thinSpace = 1;
    if (antialias) {
      x += xBoxOffset;
      y += yBoxOffset;
      boxWidth += boxWidth;
      boxHeight += boxHeight;
      thinSpace = 2;
    }
    
    if (bgcolix != 0 && g3d.setColix(bgcolix))
      showBox(g3d, colix, bgcolix, x, y, z, zSlab, 
          boxWidth, boxHeight, antialias, true);
    
    thinSpace <<= 2;
    if (antialias)
      ascent <<= 1;
    
    g3d.drawString(strLabel, font, x + thinSpace, 
        y + thinSpace + ascent, z - 1, zSlab);

    if (doPointer) {
      g3d.setColix(pointerColix);
      if (xOffset > 0)
        g3d.drawLine(x0, y0, zSlab, x, y + boxHeight / 2, zSlab);
      else if (xOffset < 0)
        g3d.drawLine(x0, y0, zSlab, x + boxWidth, y + boxHeight
            / 2, zSlab);
    }
  }

  public String getState(boolean isDefine) {
    StringBuffer s = new StringBuffer();
    if (text == null || atomBased || target.equals("error"))
      return "";
    //set echo top left
    //set echo myecho x y
    //echo .....

    if (isDefine) {
      String strOff = null;
      switch (valign) {
      case XY:
        strOff = (movableXPercent == Integer.MAX_VALUE ? movableX + " "
            : movableXPercent + "% ");
        strOff += (movableYPercent == Integer.MAX_VALUE ? movableY + ""
            : movableYPercent + "%");
      //fall through
      case XYZ:
        if (strOff == null)
          strOff = Escape.escape(xyz);
        s.append("  set echo ").append(target).append(" ").append(strOff);
        if (align != LEFT)
          s.append("  set echo ").append(target).append(" ").append(hAlignNames[align]);
        break;
      default:
        s.append("  set echo ").append(vAlignNames[valign]).append(" ").append(hAlignNames[align]);
      }
      s.append("; echo ").append(Escape.escape(textUnformatted)).append(";\n");
      if (script != null)
        s.append("  set echo ").append(target).append(" script ").append(Escape.escape(script)).append(";\n");
      if (modelIndex >= 0)
        s.append("  set echo ").append(target).append(" model ").append(viewer.getModelNumberDotted(modelIndex)).append(";\n");
    }
    //isDefine and target==top: do all
    //isDefine and target!=top: just start
    //!isDefine and target==top: do nothing
    //!isDefine and target!=top: do just this
    //fluke because top is defined with default font
    //in initShape(), so we MUST include its font def here
    if (isDefine != target.equals("top"))
      return s.toString();
    // these may not change much:
    s.append("  " + Shape.getFontCommand("echo", font)).append(";\n");
    s.append("  color echo");
    if (Graphics3D.isColixTranslucent(colix))
      s.append(" translucent " + Graphics3D.getColixTranslucencyLevel(colix));
    s.append(" [x").append(g3d.getHexColorFromIndex(colix)).append("]");
    if (bgcolix != 0) {
      s.append("; color echo background");
      if (Graphics3D.isColixTranslucent(bgcolix))
        s.append(" translucent " + Graphics3D.getColixTranslucencyLevel(bgcolix));
      s.append(" [x").append(g3d.getHexColorFromIndex(bgcolix)).append("]");
    }
    s.append(";\n");
    return s.toString();
  }
  
  public boolean checkObjectClicked(int x, int y) {
    return (script != null && 
        x >= boxX && x <= boxX + boxWidth && y >= boxY && y <= boxY + boxHeight);
  }
  
  private int stringWidth(String str) {
    int w = 0;
    int f = 1;
    int subscale = 1; //could be something less than that
    if (str == null)
      return 0;
    if (str.indexOf("<su") < 0)
      return fm.stringWidth(str);
    int len = str.length();
    String s;
    for (int i = 0; i < len; i++) {
      if (str.charAt(i) == '<') {
        if (i + 4 < len
            && ((s = str.substring(i, i + 5)).equals("<sub>") || s
                .equals("<sup>"))) {
          i += 4;
          f = subscale;
          continue;
        }
        if (i + 5 < len
            && ((s = str.substring(i, i + 6)).equals("</sub>") || s
                .equals("</sup>"))) {
          i += 5;
          f = 1;
          continue;
        }
      }
      w += fm.stringWidth(str.substring(i, i + 1)) * f;
    }
    return w;
  }
}
