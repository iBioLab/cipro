# Copyright (C) 2005
# This file is distributed under the same license as the Jmol package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Jmol\n"
"Report-Msgid-Bugs-To: jmol-developers@lists.sourceforge.net\n"
"POT-Creation-Date: 2007-10-24 06:47+0200\n"
"PO-Revision-Date: 2005-12-20 07:33-0000\n"
"Last-Translator: Metro <portugalfolding@gmail.com>\n"
"Language-Team: Portugal@Folding <Jmol-developers@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: Portugal\n"
"X-Poedit-Basepath: ../../../..\n"

#: org/openscience/jmol/app/AboutDialog.java:57
#: org/openscience/jmol/app/GuiMap.java:158
msgid "About Jmol"
msgstr "Acerca do Jmol"

#: org/openscience/jmol/app/AboutDialog.java:67
#: org/openscience/jmol/app/HelpDialog.java:77
#: org/openscience/jmol/app/WhatsNewDialog.java:68
#, java-format
msgid "Unable to find url \"{0}\"."
msgstr "Não consegue encontrar url \"{0}\"."

#: org/openscience/jmol/app/AboutDialog.java:95
#: org/openscience/jmol/app/HelpDialog.java:106
#: org/openscience/jmol/app/Jmol.java:452
#: org/openscience/jmol/app/PreferencesDialog.java:173
#: org/openscience/jmol/app/WhatsNewDialog.java:96
msgid "OK"
msgstr "OK"

#: org/openscience/jmol/app/AtomSetChooser.java:163
msgid "AtomSetChooser"
msgstr "Selector do conjunto de átomos"

#: org/openscience/jmol/app/AtomSetChooser.java:168
msgid "No AtomSets"
msgstr "Nenhum conjunto de átomos"

#: org/openscience/jmol/app/AtomSetChooser.java:197
msgid "Properties"
msgstr "Propriedades"

#: org/openscience/jmol/app/AtomSetChooser.java:205
msgid "Atom Set Collection"
msgstr "Colecção de conjuntos de átomos"

#: org/openscience/jmol/app/AtomSetChooser.java:226
msgid "Collection"
msgstr "Colecção"

#: org/openscience/jmol/app/AtomSetChooser.java:231
msgid "Info"
msgstr "Info"

#: org/openscience/jmol/app/AtomSetChooser.java:238
#: org/openscience/jmol/app/GuiMap.java:70
#: org/openscience/jmol/app/PovrayDialog.java:175
#: org/openscience/jmol/app/PovrayDialog.java:458
#: org/openscience/jmol/app/PovrayDialog.java:605
#: org/openscience/jmol/app/PovrayDialog.java:626
msgid "Select"
msgstr "Seleccionar"

#: org/openscience/jmol/app/AtomSetChooser.java:253
msgid "Repeat"
msgstr "Repetir"

#: org/openscience/jmol/app/AtomSetChooser.java:265
msgid "FPS"
msgstr "FPS"

#: org/openscience/jmol/app/AtomSetChooser.java:281
#: org/openscience/jmol/app/GuiMap.java:103
msgid "Vector"
msgstr "Vector"

#: org/openscience/jmol/app/AtomSetChooser.java:288
msgid "Radius"
msgstr "Raio"

#: org/openscience/jmol/app/AtomSetChooser.java:301
msgid "Scale"
msgstr "Escala"

#: org/openscience/jmol/app/AtomSetChooser.java:315
msgid "Amplitude"
msgstr "Amplitude"

#: org/openscience/jmol/app/AtomSetChooser.java:325
msgid "Period"
msgstr "Período"

#: org/openscience/jmol/app/AtomSetChooser.java:354
msgid "Controller"
msgstr "Controlador"

#: org/openscience/jmol/app/AtomSetChooser.java:361
msgid "atom set"
msgstr "Grupo de átomos"

#: org/openscience/jmol/app/AtomSetChooser.java:363
msgid "vector"
msgstr "vector"

#: org/openscience/jmol/app/AtomSetChooser.java:366
#, java-format
msgid "Go to first {0} in the collection"
msgstr "Ir para o primeiro {0} na colecção"

#: org/openscience/jmol/app/AtomSetChooser.java:367
#, java-format
msgid "Go to previous {0} in the collection"
msgstr "Ir para o anterior {0} na colecção"

#: org/openscience/jmol/app/AtomSetChooser.java:368
#, java-format
msgid "Play the whole collection of {0}'s"
msgstr "Correr toda a colecção de {0}'s"

#: org/openscience/jmol/app/AtomSetChooser.java:369
msgid "Pause playing"
msgstr "Pausa"

#: org/openscience/jmol/app/AtomSetChooser.java:370
#, java-format
msgid "Go to next {0} in the collection"
msgstr "Ir para a seguinte  {0} na colecção"

#: org/openscience/jmol/app/AtomSetChooser.java:371
#, java-format
msgid "Jump to last {0} in the collection"
msgstr "Saltar para o último {0} na selecção"

#: org/openscience/jmol/app/ConsoleTextArea.java:87
#, java-format
msgid "Error reading from BufferedReader: {0}"
msgstr "Erro de leitura em \"BufferedReader\": {0}"

#: org/openscience/jmol/app/DisplayPanel.java:156
msgid "Delete Atoms"
msgstr "Eliminar átomos"

#: org/openscience/jmol/app/DisplayPanel.java:157
msgid "Select Atoms"
msgstr "Escolha de átomos"

#: org/openscience/jmol/app/FilePreview.java:67
msgid "Preview"
msgstr "Prever"

#: org/openscience/jmol/app/FilePreview.java:87
msgid "Append models"
msgstr ""

#: org/openscience/jmol/app/GuiMap.java:45
msgid "&File"
msgstr "&Ficheiro"

#: org/openscience/jmol/app/GuiMap.java:46
msgid "New"
msgstr "Novo"

#: org/openscience/jmol/app/GuiMap.java:47
msgid "&Open"
msgstr "&Abrir"

#: org/openscience/jmol/app/GuiMap.java:48
msgid "Open &URL"
msgstr "Abrir &URL"

#: org/openscience/jmol/app/GuiMap.java:49
msgid "Scrip&t..."
msgstr "Scrip&t..."

#: org/openscience/jmol/app/GuiMap.java:50
msgid "AtomSetChooser..."
msgstr "Selector do conjunto de átomos..."

#: org/openscience/jmol/app/GuiMap.java:51
msgid "&Save As..."
msgstr "&Gravar como..."

#: org/openscience/jmol/app/GuiMap.java:52
msgid "&Export"
msgstr "&Exportar"

#: org/openscience/jmol/app/GuiMap.java:53
#, fuzzy
msgid "Export &Image or Script..."
msgstr "Exportar Imagem"

#: org/openscience/jmol/app/GuiMap.java:54
#, fuzzy
msgid "Export to &Web Page..."
msgstr "Exportar PDF..."

#: org/openscience/jmol/app/GuiMap.java:55
#, fuzzy
msgid "Render in POV-&Ray..."
msgstr "Renderizar em 'POV-Ray'"

#: org/openscience/jmol/app/GuiMap.java:56
#, fuzzy
msgid "Export &PDF..."
msgstr "Exportar PDF..."

#: org/openscience/jmol/app/GuiMap.java:57
msgid "&Print..."
msgstr "&Print..."

#: org/openscience/jmol/app/GuiMap.java:58
#: org/openscience/jmol/app/ScriptWindow.java:110
msgid "Close"
msgstr "Fechar"

#: org/openscience/jmol/app/GuiMap.java:59
msgid "E&xit"
msgstr "E&xit"

#: org/openscience/jmol/app/GuiMap.java:60
msgid "Recent Files..."
msgstr "Ficheiros recentes..."

#: org/openscience/jmol/app/GuiMap.java:61
msgid "&Edit"
msgstr "&Editar"

#: org/openscience/jmol/app/GuiMap.java:62
msgid "Make crystal..."
msgstr "Fazer cristal..."

#: org/openscience/jmol/app/GuiMap.java:63
#: org/openscience/jmol/app/GuiMap.java:69
msgid "Select All"
msgstr "Seleccionar todos"

#: org/openscience/jmol/app/GuiMap.java:64
msgid "Deselect All"
msgstr "Desseleccionar todos"

#: org/openscience/jmol/app/GuiMap.java:65
msgid "Copy Image"
msgstr ""

#: org/openscience/jmol/app/GuiMap.java:66
#, fuzzy
msgid "Copy Script"
msgstr "Rasmol Scripts"

#: org/openscience/jmol/app/GuiMap.java:67
msgid "&Preferences..."
msgstr "&Preferências..."

#: org/openscience/jmol/app/GuiMap.java:68
msgid "&Paste"
msgstr ""

#: org/openscience/jmol/app/GuiMap.java:71
msgid "All"
msgstr "Todos"

#: org/openscience/jmol/app/GuiMap.java:72
#: org/openscience/jmol/app/GuiMap.java:85
#: org/openscience/jmol/app/GuiMap.java:91
#: org/openscience/jmol/app/GuiMap.java:97
#: org/openscience/jmol/app/GuiMap.java:104
msgid "None"
msgstr "Nenhum"

#: org/openscience/jmol/app/GuiMap.java:73
msgid "Hydrogen"
msgstr "Hidrogénio"

#: org/openscience/jmol/app/GuiMap.java:74
msgid "Carbon"
msgstr "Cabono"

#: org/openscience/jmol/app/GuiMap.java:75
msgid "Nitrogen"
msgstr "Azoto"

#: org/openscience/jmol/app/GuiMap.java:76
msgid "Oxygen"
msgstr "Oxigénio"

#: org/openscience/jmol/app/GuiMap.java:77
msgid "Phosphorus"
msgstr "Fósforo"

#: org/openscience/jmol/app/GuiMap.java:78
msgid "Sulphur"
msgstr "Enxofre"

#: org/openscience/jmol/app/GuiMap.java:79
msgid "Amino"
msgstr "Amino"

#: org/openscience/jmol/app/GuiMap.java:80
msgid "Nucleic"
msgstr "Nucleico"

#: org/openscience/jmol/app/GuiMap.java:81
msgid "Water"
msgstr "Água"

#: org/openscience/jmol/app/GuiMap.java:82
msgid "Hetero"
msgstr "Hetero"

#: org/openscience/jmol/app/GuiMap.java:83
msgid "&Display"
msgstr "&Display"

#: org/openscience/jmol/app/GuiMap.java:84
msgid "Atom"
msgstr "Átomo"

#: org/openscience/jmol/app/GuiMap.java:86
#: org/openscience/jmol/app/GuiMap.java:87
#: org/openscience/jmol/app/GuiMap.java:88
#: org/openscience/jmol/app/GuiMap.java:89
#, fuzzy, java-format
msgid "{0}% vanderWaals"
msgstr "20% vanderWaals"

#: org/openscience/jmol/app/GuiMap.java:90
msgid "Bond"
msgstr "Ligação"

#: org/openscience/jmol/app/GuiMap.java:92
msgid "Wireframe"
msgstr "LigarFrame"

#: org/openscience/jmol/app/GuiMap.java:93
#: org/openscience/jmol/app/GuiMap.java:94
#: org/openscience/jmol/app/GuiMap.java:95
#: org/openscience/jmol/app/GuiMap.java:107
#: org/openscience/jmol/app/GuiMap.java:108
#, fuzzy, java-format
msgid "{0} Å"
msgstr "0.1 Å"

#: org/openscience/jmol/app/GuiMap.java:96
msgid "Label"
msgstr "Etiqueta"

#: org/openscience/jmol/app/GuiMap.java:98
msgid "Symbol"
msgstr "Simbolo"

#: org/openscience/jmol/app/GuiMap.java:99
#: org/openscience/jmol/app/Jmol.java:422
msgid "Name"
msgstr "Nome"

#: org/openscience/jmol/app/GuiMap.java:100
msgid "Number"
msgstr "Número"

#: org/openscience/jmol/app/GuiMap.java:101
msgid "Centered"
msgstr "Centrado"

#: org/openscience/jmol/app/GuiMap.java:102
msgid "Upper right"
msgstr "Superior direito"

#: org/openscience/jmol/app/GuiMap.java:105
msgid "On"
msgstr "On/Ligado"

#: org/openscience/jmol/app/GuiMap.java:106
#, fuzzy, java-format
msgid "{0} pixels"
msgstr "3 pixeis"

#: org/openscience/jmol/app/GuiMap.java:109
#: org/openscience/jmol/app/GuiMap.java:110
#: org/openscience/jmol/app/GuiMap.java:111
#: org/openscience/jmol/app/GuiMap.java:112
#: org/openscience/jmol/app/GuiMap.java:113
#, fuzzy, java-format
msgid "Scale {0}"
msgstr "Escala 0.2"

#: org/openscience/jmol/app/GuiMap.java:114
msgid "Zoom"
msgstr "Zoom"

#: org/openscience/jmol/app/GuiMap.java:115
#: org/openscience/jmol/app/GuiMap.java:116
#: org/openscience/jmol/app/GuiMap.java:117
#: org/openscience/jmol/app/GuiMap.java:118
#: org/openscience/jmol/app/GuiMap.java:119
#, java-format
msgid "{0}%"
msgstr ""

#: org/openscience/jmol/app/GuiMap.java:120
#: org/openscience/jmol/app/GuiMap.java:164
msgid "Perspective Depth"
msgstr "Profundidade da perpectiva"

#: org/openscience/jmol/app/GuiMap.java:121
#: org/openscience/jmol/app/GuiMap.java:165
msgid "Axes"
msgstr "Eixos"

#: org/openscience/jmol/app/GuiMap.java:122
#: org/openscience/jmol/app/GuiMap.java:166
msgid "Bounding Box"
msgstr "Caixa de ligação"

#: org/openscience/jmol/app/GuiMap.java:123
msgid "&Hydrogens"
msgstr "&Hidrogénios"

#: org/openscience/jmol/app/GuiMap.java:124
msgid "&Vectors"
msgstr "&Vectores"

#: org/openscience/jmol/app/GuiMap.java:125
msgid "&Measurements"
msgstr "&Medições"

#: org/openscience/jmol/app/GuiMap.java:126
msgid "&View"
msgstr "&Ver"

#: org/openscience/jmol/app/GuiMap.java:127
msgid "Front"
msgstr "Frente"

#: org/openscience/jmol/app/GuiMap.java:128
msgid "Top"
msgstr "Topo"

#: org/openscience/jmol/app/GuiMap.java:129
msgid "Bottom"
msgstr "Fundo"

#: org/openscience/jmol/app/GuiMap.java:130
msgid "Right"
msgstr "Direita"

#: org/openscience/jmol/app/GuiMap.java:131
msgid "Left"
msgstr "Esquerda"

#: org/openscience/jmol/app/GuiMap.java:132
msgid "Transform..."
msgstr "Transformar..."

#: org/openscience/jmol/app/GuiMap.java:133
msgid "Define Center"
msgstr "Definir centro"

#: org/openscience/jmol/app/GuiMap.java:134
msgid "&Tools"
msgstr "&Ferramentas"

#: org/openscience/jmol/app/GuiMap.java:135
#: org/openscience/jmol/app/MeasurementTable.java:65
msgid "Measurements..."
msgstr "Medições..."

#: org/openscience/jmol/app/GuiMap.java:136
msgid "Distance Units"
msgstr "Unidades de distância"

#: org/openscience/jmol/app/GuiMap.java:137
msgid "Nanometers 1E-9"
msgstr "Nanometros 1E-9"

#: org/openscience/jmol/app/GuiMap.java:138
msgid "Angstroms 1E-10"
msgstr "Angstroms 1E-10"

#: org/openscience/jmol/app/GuiMap.java:139
msgid "Picometers 1E-12"
msgstr "Picometros 1E-12"

#: org/openscience/jmol/app/GuiMap.java:140
msgid "Animate..."
msgstr "Animar..."

#: org/openscience/jmol/app/GuiMap.java:141
msgid "Vibrate..."
msgstr "Vibrar..."

#: org/openscience/jmol/app/GuiMap.java:142
msgid "&Graph..."
msgstr "&Gráfico"

#: org/openscience/jmol/app/GuiMap.java:143
msgid "Calculate chemical &shifts..."
msgstr "Calcular &shifts químicos"

#: org/openscience/jmol/app/GuiMap.java:144
msgid "&Crystal Properties"
msgstr "&Propriedades do Cristal"

#: org/openscience/jmol/app/GuiMap.java:145
msgid "Once"
msgstr "Uma vez"

#: org/openscience/jmol/app/GuiMap.java:146
msgid "Loop"
msgstr "Curva"

#: org/openscience/jmol/app/GuiMap.java:147
msgid "Palindrome"
msgstr "Palindroma"

#: org/openscience/jmol/app/GuiMap.java:148
msgid "Stop animation"
msgstr "Parar a animação"

#: org/openscience/jmol/app/GuiMap.java:149
#: org/openscience/jmol/app/GuiMap.java:181
msgid "Rewind to first frame"
msgstr "Rebobinar para o primeiro frame"

#: org/openscience/jmol/app/GuiMap.java:150
#: org/openscience/jmol/app/GuiMap.java:182
msgid "Go to next frame"
msgstr "Ir para o próximo frame"

#: org/openscience/jmol/app/GuiMap.java:151
#: org/openscience/jmol/app/GuiMap.java:183
msgid "Go to previous frame"
msgstr "Ir para o frame anterior"

#: org/openscience/jmol/app/GuiMap.java:152
msgid "Start vibration"
msgstr "Iniciar a vibração"

#: org/openscience/jmol/app/GuiMap.java:153
msgid "Stop vibration"
msgstr "Parar a vibração"

#: org/openscience/jmol/app/GuiMap.java:154
msgid "First frequency"
msgstr "Primeira frequência"

#: org/openscience/jmol/app/GuiMap.java:155
msgid "Next frequency"
msgstr "Próxima freqência"

#: org/openscience/jmol/app/GuiMap.java:156
msgid "Previous frequency"
msgstr "Frequência anterior"

#: org/openscience/jmol/app/GuiMap.java:157
msgid "&Help"
msgstr "&Ajuda"

#: org/openscience/jmol/app/GuiMap.java:159
msgid "User Guide"
msgstr "Manual"

#: org/openscience/jmol/app/GuiMap.java:160
msgid "What's New"
msgstr "O que há de novo"

#: org/openscience/jmol/app/GuiMap.java:161
#, fuzzy
msgid "Jmol Java &Console"
msgstr "Consola Jmol"

#: org/openscience/jmol/app/GuiMap.java:162
msgid "Hydrogens"
msgstr "Hidrogénios"

#: org/openscience/jmol/app/GuiMap.java:163
msgid "Measurements"
msgstr "Medições"

#: org/openscience/jmol/app/GuiMap.java:167
msgid "RasMol/Chime compatible axes orientation/rotations"
msgstr "Eixos compatíveis RasMol/Chimeorientação/rotação"

#: org/openscience/jmol/app/GuiMap.java:168
msgid "File Preview (needs restarting Jmol)"
msgstr "Pré-visualização do ficheiro (requer reiniciação do Jmol)"

#: org/openscience/jmol/app/GuiMap.java:169
#, fuzzy
msgid "Clear console button (needs restarting Jmol)"
msgstr "Pré-visualização do ficheiro (requer reiniciação do Jmol)"

#: org/openscience/jmol/app/GuiMap.java:170
#: org/openscience/jmol/app/GuiMap.java:171
msgid "Use Atom Color"
msgstr "Usar cor do átomo"

#: org/openscience/jmol/app/GuiMap.java:172
msgid "Open a file."
msgstr "Abrir ficheiro"

#: org/openscience/jmol/app/GuiMap.java:173
#, fuzzy
msgid "Export view to an image or script file."
msgstr "Exportar para um ficheiro de imagem"

#: org/openscience/jmol/app/GuiMap.java:174
msgid "Export one or more views to a web page."
msgstr ""

#: org/openscience/jmol/app/GuiMap.java:175
msgid "Render in pov-ray."
msgstr "Renderizar em \"pov-ray\""

#: org/openscience/jmol/app/GuiMap.java:176
msgid "Print view."
msgstr "Visualização de impressão"

#: org/openscience/jmol/app/GuiMap.java:177
msgid "Rotate molecule."
msgstr "Rodar molécula"

#: org/openscience/jmol/app/GuiMap.java:178
msgid "Select an atom or region."
msgstr "Escolher átomo ou região"

#: org/openscience/jmol/app/GuiMap.java:179
msgid "View measurement table."
msgstr "Ver tabela de medições"

#: org/openscience/jmol/app/GuiMap.java:180
msgid "Return molecule to home position."
msgstr "Recolocar a molécula na posição inicial"

#: org/openscience/jmol/app/HelpDialog.java:64
msgid "Jmol Help"
msgstr "Ajuda Jmol"

#: org/openscience/jmol/app/ImageCreator.java:104
msgid "IO Exception:"
msgstr "Excepção IO:"

#: org/openscience/jmol/app/ImageTyper.java:63
msgid "Image Type"
msgstr "Tipo de imagem"

#: org/openscience/jmol/app/ImageTyper.java:99
msgid "JPEG Quality"
msgstr "Qualidade JPEG"

#: org/openscience/jmol/app/Jmol.java:143
#: org/openscience/jmol/app/webexport/WebMaker.java:64
msgid "Error starting Jmol: the property 'user.home' is not defined."
msgstr "Erro ao iniciar o Jmol: a propriedade 'user.home' não está definida"

#: org/openscience/jmol/app/Jmol.java:188
msgid "Initializing 3D display..."
msgstr "Inicializando a visualização 3D..."

#: org/openscience/jmol/app/Jmol.java:213
msgid "Initializing Preferences..."
msgstr "Inicializando as Preferências..."

#: org/openscience/jmol/app/Jmol.java:215
msgid "Initializing Recent Files..."
msgstr "Inicializando os ficheiros recentes..."

#: org/openscience/jmol/app/Jmol.java:218
msgid "Initializing Script Window..."
msgstr "Inicializando a Janela de script..."

#: org/openscience/jmol/app/Jmol.java:220
msgid "Initializing AtomSetChooser Window..."
msgstr "Inicializando a Janeal 'AtomSetChooser'..."

#: org/openscience/jmol/app/Jmol.java:228
msgid "Initializing Measurements..."
msgstr "Inicializando medições..."

#: org/openscience/jmol/app/Jmol.java:252
msgid "Building Command Hooks..."
msgstr "Construindo 'Command Hooks'..."

#: org/openscience/jmol/app/Jmol.java:263
msgid "Building Menubar..."
msgstr "Construindo barra de menus..."

#: org/openscience/jmol/app/Jmol.java:279
msgid "Starting display..."
msgstr "Iniciando a visualização..."

#: org/openscience/jmol/app/Jmol.java:282
msgid "Setting up File Choosers..."
msgstr "Definir 'File Choosers'..."

#: org/openscience/jmol/app/Jmol.java:341
msgid "Setting up Drag-and-Drop..."
msgstr "Definir Drag-and-Drop..."

#: org/openscience/jmol/app/Jmol.java:362
msgid "Launching main frame..."
msgstr "Lançando o frame (quadro) principal..."

#: org/openscience/jmol/app/Jmol.java:380
msgid "Creating main window..."
msgstr "Criando janela principal..."

#: org/openscience/jmol/app/Jmol.java:381
msgid "Initializing Swing..."
msgstr "Inicializando 'swing' (balanço)..."

#: org/openscience/jmol/app/Jmol.java:393
msgid "Initializing Jmol..."
msgstr "Inicializando Jmol..."

#: org/openscience/jmol/app/Jmol.java:411
msgid "All Files"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:412
#: org/openscience/jmol/app/Jmol.java:450
#: org/openscience/jmol/app/PovrayDialog.java:500
#: org/openscience/jmol/app/PreferencesDialog.java:165
#: org/openscience/jmol/app/RecentFilesDialog.java:69
msgid "Cancel"
msgstr "Cancelar"

#: org/openscience/jmol/app/Jmol.java:413
msgid "Abort file chooser dialog"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:414
#: org/openscience/jmol/app/Jmol.java:415
msgid "Details"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:416
#, fuzzy
msgid "Directory"
msgstr "Directoria de trabalho"

#: org/openscience/jmol/app/Jmol.java:417
#: org/openscience/jmol/app/Jmol.java:437
#: org/openscience/jmol/app/Jmol.java:439
#: org/openscience/jmol/app/RecentFilesDialog.java:65
msgid "Open"
msgstr "Abrir"

#: org/openscience/jmol/app/Jmol.java:418
#, fuzzy
msgid "Open selected directory"
msgstr "Grupo de átomos"

#: org/openscience/jmol/app/Jmol.java:419
msgid "Attributes"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:420
msgid "Modified"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:421
msgid "Generic File"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:423
#, fuzzy
msgid "File Name:"
msgstr "Filename Stem"

#: org/openscience/jmol/app/Jmol.java:424
msgid "Size"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:425
msgid "Files of Type:"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:426
msgid "Type"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:427
#: org/openscience/jmol/app/ScriptWindow.java:106
msgid "Help"
msgstr "Ajuda"

#: org/openscience/jmol/app/Jmol.java:428
msgid "FileChooser help"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:429
#: org/openscience/jmol/app/Jmol.java:430
msgid "Home"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:431
#: org/openscience/jmol/app/Jmol.java:432
msgid "List"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:433
msgid "Look In:"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:434
msgid "Error creating new folder"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:435
msgid "New Folder"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:436
msgid "Create New Folder"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:438
#, fuzzy
msgid "Open selected file"
msgstr "Grupo de átomos"

#: org/openscience/jmol/app/Jmol.java:440
#: org/openscience/jmol/app/Jmol.java:442
#: org/openscience/jmol/app/PovrayDialog.java:757
msgid "Save"
msgstr "Gravar"

#: org/openscience/jmol/app/Jmol.java:441
#, fuzzy
msgid "Save selected file"
msgstr "Grupo de átomos"

#: org/openscience/jmol/app/Jmol.java:443
#, fuzzy
msgid "Save In:"
msgstr "Gravar"

#: org/openscience/jmol/app/Jmol.java:444
msgid "Update"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:445
msgid "Update directory listing"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:446
msgid "Up"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:447
msgid "Up One Level"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:451
#, fuzzy
msgid "No"
msgstr "Nenhum"

#: org/openscience/jmol/app/Jmol.java:453
msgid "Yes"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:475
msgid "give this help page"
msgstr "Dar esta página de ajuda"

#: org/openscience/jmol/app/Jmol.java:477
msgid "no display (and also exit when done)"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:478
msgid "check script syntax only"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:479
msgid "silent startup operation"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:481
msgid "no console -- all output to sysout"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:483
msgid "exit after script (implicit with -n)"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:497
msgid "property=value"
msgstr "propriedade=valor"

#: org/openscience/jmol/app/Jmol.java:500
msgid "supported options are given below"
msgstr "opções suportadas são referidas em baixo"

#: org/openscience/jmol/app/Jmol.java:505
#, fuzzy, java-format
msgid "window width x height, e.g. {0}"
msgstr "tamanho da janela 500x500"

#: org/openscience/jmol/app/Jmol.java:513
msgid ""
"JPG image quality (1-100; default 75) or PNG image compression (0-9; default "
"2, maximum compression 9"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:519
#, java-format
msgid "{0} or {1}:filename"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:541
msgid "For example:"
msgstr ""

#: org/openscience/jmol/app/Jmol.java:547
msgid "The -D options are as follows (defaults in parathesis):"
msgstr "As opções -D são as seguintes (pré-definição em parentesis):"

#: org/openscience/jmol/app/Jmol.java:703
msgid "Executing script..."
msgstr "Executando um script..."

#: org/openscience/jmol/app/Jmol.java:715
#, fuzzy
msgid "Jmol Java Console"
msgstr "Consola Jmol"

#: org/openscience/jmol/app/Jmol.java:723
#: org/openscience/jmol/app/ScriptWindow.java:94
msgid "Clear"
msgstr "Limpar"

#: org/openscience/jmol/app/Jmol.java:737
msgid "Could not create ConsoleTextArea: "
msgstr "Não foi possível criar 'ConsoleTextArea':"

#: org/openscience/jmol/app/Jmol.java:823
msgid "Closing Jmol..."
msgstr "Fechar Jmol..."

#: org/openscience/jmol/app/Jmol.java:1075
msgid "Macros"
msgstr "Macros"

#: org/openscience/jmol/app/Jmol.java:1444
msgid "Open URL"
msgstr "Abrir URL"

#: org/openscience/jmol/app/Jmol.java:1445
msgid "Enter URL of molecular model"
msgstr "Escrever URL de um modelo molecular"

#: org/openscience/jmol/app/MeasurementTable.java:123
msgid "Delete"
msgstr "Eliminar"

#: org/openscience/jmol/app/MeasurementTable.java:132
msgid "DeleteAll"
msgstr "Eliminar todos"

#: org/openscience/jmol/app/MeasurementTable.java:150
msgid "Dismiss"
msgstr "Rejeitar"

#: org/openscience/jmol/app/MeasurementTable.java:200
msgid "Value"
msgstr "Valor"

#: org/openscience/jmol/app/PovrayDialog.java:113
msgid "Render in POV-Ray"
msgstr "Renderizar em 'POV-Ray'"

#: org/openscience/jmol/app/PovrayDialog.java:151
msgid "Jmol-to-POV-Ray Conversion"
msgstr "Conversão Jmol para POV-Ray"

#: org/openscience/jmol/app/PovrayDialog.java:155
#, fuzzy
msgid "Filename"
msgstr "Filename Stem"

#: org/openscience/jmol/app/PovrayDialog.java:157
#, fuzzy
msgid ""
"'caffeine.pov' -> 'caffeine.pov', 'caffeine.pov.ini', 'caffeine.pov.spt'"
msgstr ""
"Frame único: eg 'caffine' -> 'caffine.pov';  frame múltiplo: eg 'caffine' -> "
"'caffine_1.pov', 'caffine_2.pov'"

#: org/openscience/jmol/app/PovrayDialog.java:167
msgid "Working Directory"
msgstr "Directoria de trabalho"

#: org/openscience/jmol/app/PovrayDialog.java:169
msgid "Where the .pov files will be saved"
msgstr "Onde os ficheiro .pov irão ser gravados"

#: org/openscience/jmol/app/PovrayDialog.java:189
msgid "POV-Ray Runtime Options"
msgstr "Opções de excecução POV-Ray"

#: org/openscience/jmol/app/PovrayDialog.java:194
msgid "Run POV-Ray directly"
msgstr "Executar POV-Ray directamente"

#: org/openscience/jmol/app/PovrayDialog.java:196
#, fuzzy
msgid "Launch POV-Ray from within Jmol"
msgstr "Lançar povray dentro do Jmol"

#: org/openscience/jmol/app/PovrayDialog.java:241
msgid "Display While Rendering"
msgstr "Visualizar enquanto renderiza"

#: org/openscience/jmol/app/PovrayDialog.java:243
msgid "Should povray attempt to display while rendering?"
msgstr "O povray pode tentar exibir enquanto renderiza?"

#: org/openscience/jmol/app/PovrayDialog.java:266
msgid "Width : "
msgstr "Largura:"

#: org/openscience/jmol/app/PovrayDialog.java:268
msgid "Image width"
msgstr "Largura da imagem"

#: org/openscience/jmol/app/PovrayDialog.java:283
msgid "Height : "
msgstr "Altura"

#: org/openscience/jmol/app/PovrayDialog.java:285
msgid "Image height"
msgstr "Altura da imagem"

#: org/openscience/jmol/app/PovrayDialog.java:302
msgid "Fixed ratio : "
msgstr "Razão fixa:"

#: org/openscience/jmol/app/PovrayDialog.java:304
msgid "Use a fixed ratio for width:height"
msgstr "Usar razão fixa para largura:altura"

#: org/openscience/jmol/app/PovrayDialog.java:315
msgid "User defined"
msgstr "Definido pelo utilizador"

#: org/openscience/jmol/app/PovrayDialog.java:317
msgid "Keep ratio of Jmol window"
msgstr "Manter razão da janela Jmol"

#: org/openscience/jmol/app/PovrayDialog.java:357
msgid "N - PNG"
msgstr "N - PNG"

#: org/openscience/jmol/app/PovrayDialog.java:360
msgid "P - PPM"
msgstr "N - PNG"

#: org/openscience/jmol/app/PovrayDialog.java:363
msgid "C - Compressed Targa-24"
msgstr "C - Compressed Targa-24"

#: org/openscience/jmol/app/PovrayDialog.java:366
msgid "T - Uncompressed Targa-24"
msgstr "T - Uncompressed Targa-24"

#: org/openscience/jmol/app/PovrayDialog.java:382
msgid "Alpha transparency"
msgstr "Alpha transparency"

#: org/openscience/jmol/app/PovrayDialog.java:384
msgid "Output Alpha transparency data"
msgstr "Dados de transparência Alpha de saída"

#: org/openscience/jmol/app/PovrayDialog.java:397
msgid "Mosaic preview"
msgstr "Pré-visualização em mosaico"

#: org/openscience/jmol/app/PovrayDialog.java:399
msgid "Render the image in several passes"
msgstr "Renderizar imagem em vários passos"

#: org/openscience/jmol/app/PovrayDialog.java:409
msgid "Start size : "
msgstr "Tamanho inicial:"

#: org/openscience/jmol/app/PovrayDialog.java:411
msgid "Inital size of the tiles"
msgstr "Tamanho inicial dos mosaicos"

#: org/openscience/jmol/app/PovrayDialog.java:427
msgid "End size : "
msgstr "Tamanho final:"

#: org/openscience/jmol/app/PovrayDialog.java:429
msgid "Final size of the tiles"
msgstr "Tamanho final dos mosaicos"

#: org/openscience/jmol/app/PovrayDialog.java:450
msgid "POV-Ray Executable Location"
msgstr "Localização do executável Pov-Ray"

#: org/openscience/jmol/app/PovrayDialog.java:452
msgid "Location of the povray Executable"
msgstr "Localização do executável povray"

#: org/openscience/jmol/app/PovrayDialog.java:489
#: org/openscience/jmol/app/PovrayDialog.java:755
msgid "Go!"
msgstr "Avançar"

#: org/openscience/jmol/app/PovrayDialog.java:491
msgid "Save file and possibly launch povray"
msgstr "Gravar ficheiro e possivelmente lançar povray"

#: org/openscience/jmol/app/PovrayDialog.java:502
msgid "Cancel this dialog without saving"
msgstr "Cancelar sem gravar"

#: org/openscience/jmol/app/PreferencesDialog.java:133
msgid "Preferences"
msgstr "Preferências"

#: org/openscience/jmol/app/PreferencesDialog.java:150
msgid "Display"
msgstr "Mostrar"

#: org/openscience/jmol/app/PreferencesDialog.java:151
msgid "Atoms"
msgstr "Átomos"

#: org/openscience/jmol/app/PreferencesDialog.java:152
msgid "Bonds"
msgstr "Ligações"

#: org/openscience/jmol/app/PreferencesDialog.java:157
msgid "Jmol Defaults"
msgstr "Pré-definições Jmol"

#: org/openscience/jmol/app/PreferencesDialog.java:161
msgid "RasMol Defaults"
msgstr "Pré-definições RasMol"

#: org/openscience/jmol/app/PreferencesDialog.java:169
msgid "Apply"
msgstr "Aplicar"

#: org/openscience/jmol/app/PreferencesDialog.java:197
msgid "Show All"
msgstr "Mostrar todos"

#: org/openscience/jmol/app/PreferencesDialog.java:296
msgid "Default atom size"
msgstr "Tamanho de átomo pré-definido"

#: org/openscience/jmol/app/PreferencesDialog.java:297
msgid "(percentage of vanDerWaals radius)"
msgstr "(percentagem do raio de vandDerWaals)"

#: org/openscience/jmol/app/PreferencesDialog.java:349
msgid "Compute Bonds"
msgstr "Calcular ligações"

#: org/openscience/jmol/app/PreferencesDialog.java:351
msgid "Automatically"
msgstr "Automaticamente"

#: org/openscience/jmol/app/PreferencesDialog.java:352
msgid "Don't Compute Bonds"
msgstr "Não calcular as ligações"

#: org/openscience/jmol/app/PreferencesDialog.java:379
msgid "Default Bond Radius"
msgstr "Raio das ligações pré-definido"

#: org/openscience/jmol/app/PreferencesDialog.java:380
#: org/openscience/jmol/app/PreferencesDialog.java:418
#: org/openscience/jmol/app/PreferencesDialog.java:469
msgid "(Angstroms)"
msgstr "(Angstroms)"

#: org/openscience/jmol/app/PreferencesDialog.java:417
msgid "Bond Tolerance - sum of two covalent radii + this value"
msgstr "Tolerância da ligação- somatório de dois raios covalentes+ este valor"

#: org/openscience/jmol/app/PreferencesDialog.java:468
msgid "Minimum Bonding Distance"
msgstr "Distância mínima de ligação"

#: org/openscience/jmol/app/RecentFilesDialog.java:60
msgid "Recent Files"
msgstr "Ficheiros recentes"

#: org/openscience/jmol/app/ScriptWindow.java:71
#, fuzzy
msgid "Jmol Script Console"
msgstr "Consola Jmol"

#: org/openscience/jmol/app/ScriptWindow.java:85
msgid "Run"
msgstr "Executar"

#: org/openscience/jmol/app/ScriptWindow.java:89
msgid "Halt"
msgstr "Parar"

#: org/openscience/jmol/app/ScriptWindow.java:98
msgid "History"
msgstr ""

#: org/openscience/jmol/app/ScriptWindow.java:102
#, fuzzy
msgid "State"
msgstr "Gravar"

#: org/openscience/jmol/app/ScriptWindow.java:114
msgid "Undo"
msgstr ""

#: org/openscience/jmol/app/ScriptWindow.java:118
msgid "Redo"
msgstr ""

#: org/openscience/jmol/app/Splash.java:45
msgid "Loading..."
msgstr "Carregando..."

#: org/openscience/jmol/app/WhatsNewDialog.java:57
msgid "What's New in Jmol"
msgstr "O que há de novo no Jmol"

#~ msgid "Image size"
#~ msgstr "Tamanho da imagem"

#~ msgid "Output format : "
#~ msgstr "Formato de saida"

#~ msgid "Select the file format of the output file"
#~ msgstr "Seleccionar o formato do ficheiro"

#~ msgid "Command Line to Execute"
#~ msgstr " linha de comandos para executar"

#~ msgid "The actual command which will be executed"
#~ msgstr "O comando actual irá ser executado"

#~ msgid "null component string"
#~ msgstr "Componente nulo do filamento"

#~ msgid "Filename Stem"
#~ msgstr "Filename Stem"

#~ msgid "Use .ini file"
#~ msgstr "Usar ficheiro .ini"

#~ msgid "Save options in a .ini file"
#~ msgstr "Gravar opções num ficheiro .ini"

#~ msgid "Render all frames"
#~ msgstr "Renderizar todos os frames"

#~ msgid "Render each model (not only the currently displayed one)"
#~ msgstr "Renderizar cada modelo (não só o que está a ser visualizado)"

#~ msgid "Turn on POV-Ray anti-aliasing"
#~ msgstr "Ligar 'anti-aliasing' do POV-Ray"

#~ msgid "Use povray's slower but higher quality anti-aliasing mode"
#~ msgstr ""
#~ "Usar modo 'anti-aliasing' mais lento (mas com melhor qualidade)  do povray"

#~ msgid "Render in pov-ray..."
#~ msgstr "Renderizar em pov-ray..."

#~ msgid "File not loaded"
#~ msgstr "Ficheiro não carregado"

#, fuzzy
#~ msgid "no display"
#~ msgstr "Mostrar"

#, fuzzy
#~ msgid "Jmol executing script ..."
#~ msgstr "Executando um script..."

#~ msgid "Wi&reframe Rotation"
#~ msgstr "Rotação de FrameLi&gado"

#~ msgid "Wireframe Rotation"
#~ msgstr "Rotação de frame"

#~ msgid "Colors"
#~ msgstr "Cores"

#~ msgid "Background"
#~ msgstr "Fundo"

#~ msgid "Set the Background Color"
#~ msgstr "Definir cor de fundo"

#~ msgid "Background Color"
#~ msgstr "Cores do fundo"

#~ msgid "Picked Atoms"
#~ msgstr "Átomos escolhidos"

#~ msgid "Set the Color for Picked Atoms"
#~ msgstr "Definir cor de átomos escolhidos"

#~ msgid "Picked Atom Color"
#~ msgstr "Cor do átomo escolhido"

#~ msgid "Text"
#~ msgstr "Texto"

#~ msgid "Set the Color for Text"
#~ msgstr "Definir cor do texto"

#~ msgid "Text Color"
#~ msgstr "Cor do texto"

#~ msgid "Set the Color for Bonds"
#~ msgstr "Definir cor para as ligações"

#~ msgid "Bond Color"
#~ msgstr "Cor da ligação"

#~ msgid "Vectors"
#~ msgstr "Vectores"

#~ msgid "Set the Vector Color"
#~ msgstr "Definir cor do Vector"

#~ msgid "Vector Color"
#~ msgstr "Cor do vector"

#~ msgid "Color for distance, angle, & torsion measurements"
#~ msgstr "Cor para a distância, ângulo, e medições de torção"

#~ msgid "Measurements Color"
#~ msgstr "Cor das meidções"

#~ msgid "Loading plugins..."
#~ msgstr "Carregamento de plugins..."

#~ msgid "15% vanderWaals"
#~ msgstr "15% vanderWaals"

#~ msgid "25% vanderWaals"
#~ msgstr "20% vanderWaals"

#~ msgid "100% vanderWaals"
#~ msgstr "20% vanderWaals"

#~ msgid "0.10 Å"
#~ msgstr "0.10 Å"

#~ msgid "0.15 Å"
#~ msgstr "0.15 Å"

#~ msgid "0.20 Å"
#~ msgstr "0.15 Å"

#~ msgid "0.05 Å"
#~ msgstr "0.05 Å"

#~ msgid "Scale 0.5"
#~ msgstr "Escala 0.5"

#~ msgid "Scale 1"
#~ msgstr "Escala 1"

#~ msgid "Scale 2"
#~ msgstr "Escala 2"

#~ msgid "Scale 5"
#~ msgstr "Escala 5"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "150%"
#~ msgstr "150%"

#~ msgid "200%"
#~ msgstr "200%"

#~ msgid "400%"
#~ msgstr "400%"

#~ msgid "800%"
#~ msgstr "800%"
