# Copyright (C) 2005
# This file is distributed under the same license as the Jmol package.
# Ivo Sarak <ivo@ra.vendomar.ee>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: Jmol\n"
"Report-Msgid-Bugs-To: jmol-developers@lists.sourceforge.net\n"
"POT-Creation-Date: 2007-11-03 09:46+0100\n"
"PO-Revision-Date: 2006-03-17 20:19+0100\n"
"Last-Translator: Ivo Sarak <ivo@ra.vendomar.ee>\n"
"Language-Team: Estonian <Jmol-developers@lists.sf.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Estonian\n"
"X-Poedit-Country: Estonia\n"
"X-Poedit-Basepath: ../../../..\n"

#: org/jmol/applet/Console.java:39
#, fuzzy
msgid "Jmol Script Console"
msgstr "Jmol skript sai täidetud"

#: org/jmol/applet/Console.java:41
msgid "Execute"
msgstr ""

#: org/jmol/applet/Console.java:42
msgid "Clear Output"
msgstr ""

#: org/jmol/applet/Console.java:43
#, fuzzy
msgid "Clear Input"
msgstr "Sulge"

#: org/jmol/applet/Console.java:44 org/jmol/popup/PopupResourceBundle.java:832
msgid "History"
msgstr ""

#: org/jmol/applet/Console.java:45
#, fuzzy
msgid "State"
msgstr "Määra Z Kiirus"

#: org/jmol/applet/Console.java:46
#, fuzzy
msgid "Load"
msgstr "Ligand"

#: org/jmol/applet/Console.java:91
msgid "press CTRL-ENTER for new line or paste model data and press Load"
msgstr ""

#: org/jmol/applet/Jmol.java:714
#, fuzzy, java-format
msgid ""
"Jmol Applet version {0} {1}.\n"
"\n"
"An OpenScience project.\n"
"\n"
"See http://www.jmol.org for more information"
msgstr ""
"Jmol Applet. On osa OpenScience projektist. Täpsemalt vaata http://www.jmol."
"org"

#: org/jmol/applet/Jmol.java:862
msgid "File Error:"
msgstr "Faili viga:"

#: org/jmol/appletwrapper/AppletWrapper.java:82
msgid "Loading Jmol applet ..."
msgstr ""

#: org/jmol/appletwrapper/AppletWrapper.java:143
#, java-format
msgid "  {0} seconds"
msgstr ""

#: org/jmol/i18n/GT.java:101
msgid "Catalan"
msgstr ""

#: org/jmol/i18n/GT.java:102
msgid "Czech"
msgstr ""

#: org/jmol/i18n/GT.java:103
msgid "Dutch"
msgstr ""

#: org/jmol/i18n/GT.java:104
msgid "English"
msgstr ""

#: org/jmol/i18n/GT.java:105
msgid "Estonian"
msgstr ""

#: org/jmol/i18n/GT.java:106
msgid "French"
msgstr ""

#: org/jmol/i18n/GT.java:107
#, fuzzy
msgid "German"
msgstr "Hall"

#: org/jmol/i18n/GT.java:108
msgid "Portuguese - Brazil"
msgstr ""

#: org/jmol/i18n/GT.java:109
msgid "Portuguese"
msgstr ""

#: org/jmol/i18n/GT.java:110
msgid "Spanish"
msgstr ""

#: org/jmol/i18n/GT.java:111
msgid "Turkish"
msgstr ""

#: org/jmol/popup/JmolPopup.java:413 org/jmol/popup/JmolPopup.java:453
#, fuzzy
msgid "all"
msgstr "Kõik"

#: org/jmol/popup/JmolPopup.java:537
msgid "Java memory usage"
msgstr ""

#: org/jmol/popup/JmolPopup.java:543
#, java-format
msgid "{0} MB total"
msgstr ""

#: org/jmol/popup/JmolPopup.java:545
#, java-format
msgid "{0} MB free"
msgstr ""

#: org/jmol/popup/JmolPopup.java:548
#, java-format
msgid "{0} MB maximum"
msgstr ""

#: org/jmol/popup/JmolPopup.java:551
msgid "unknown maximum"
msgstr ""

#: org/jmol/popup/JmolPopup.java:554
msgid "1 processor"
msgstr ""

#: org/jmol/popup/JmolPopup.java:555
#, java-format
msgid "{0} processors"
msgstr ""

#: org/jmol/popup/JmolPopup.java:557
msgid "unknown processor count"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:526
msgid "No atoms loaded"
msgstr "Ei ühtegi aatomit"

#: org/jmol/popup/PopupResourceBundle.java:528
#, fuzzy
msgid "Configurations"
msgstr "Tõlked"

#: org/jmol/popup/PopupResourceBundle.java:529
msgid "Element"
msgstr "Element"

#: org/jmol/popup/PopupResourceBundle.java:530
#, fuzzy
msgid "Model/Frame"
msgstr "Mudel"

#: org/jmol/popup/PopupResourceBundle.java:531
msgid "Language"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:532
#: org/jmol/popup/PopupResourceBundle.java:533
#: org/jmol/popup/PopupResourceBundle.java:534
msgid "By Residue Name"
msgstr "Jäänuse nime järgi"

#: org/jmol/popup/PopupResourceBundle.java:535
msgid "By HETATM"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:536
msgid "Molecular Orbitals"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:537
#: org/jmol/popup/PopupResourceBundle.java:839
#: org/jmol/popup/PopupResourceBundle.java:856
msgid "Symmetry"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:540
msgid "Model information"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:541
#, fuzzy, java-format
msgid "Select ({0})"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:542
#, fuzzy, java-format
msgid "All {0} models"
msgstr "Kõik Mudelid"

#: org/jmol/popup/PopupResourceBundle.java:543
#, java-format
msgid "Configurations ({0})"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:544
#, fuzzy, java-format
msgid "Collection of {0} models"
msgstr "Kõik Mudelid"

#: org/jmol/popup/PopupResourceBundle.java:545
#, java-format
msgid "atoms: {0}"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:546
#, java-format
msgid "bonds: {0}"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:547
#, java-format
msgid "groups: {0}"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:548
#, fuzzy, java-format
msgid "chains: {0}"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:549
#, java-format
msgid "polymers: {0}"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:550
#, fuzzy, java-format
msgid "model {0}"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:551
#, fuzzy, java-format
msgid "View {0}"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:552
msgid "Main Menu"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:556
#: org/jmol/popup/PopupResourceBundle.java:570
#: org/jmol/popup/PopupResourceBundle.java:579
#: org/jmol/popup/PopupResourceBundle.java:596
msgid "All"
msgstr "Kõik"

#: org/jmol/popup/PopupResourceBundle.java:557
#: org/jmol/popup/PopupResourceBundle.java:676
#: org/jmol/popup/PopupResourceBundle.java:685
msgid "None"
msgstr "Ei ühtegi"

#: org/jmol/popup/PopupResourceBundle.java:558
msgid "Display Selected Only"
msgstr "Kuva Ainult Valitud"

#: org/jmol/popup/PopupResourceBundle.java:559
msgid "Invert Selection"
msgstr "Vaheta Valik"

#: org/jmol/popup/PopupResourceBundle.java:561
#, fuzzy
msgid "View"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:562
msgid "Front"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:563
msgid "Left"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:564
#, fuzzy
msgid "Right"
msgstr "Ülemine Parem"

#: org/jmol/popup/PopupResourceBundle.java:565
msgid "Top"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:566
#, fuzzy
msgid "Bottom"
msgstr "Suurenda"

#: org/jmol/popup/PopupResourceBundle.java:567
#, fuzzy
msgid "Back"
msgstr "Must"

#: org/jmol/popup/PopupResourceBundle.java:569
msgid "Protein"
msgstr "Proteiin"

#: org/jmol/popup/PopupResourceBundle.java:571
#: org/jmol/popup/PopupResourceBundle.java:582
#: org/jmol/popup/PopupResourceBundle.java:652
#: org/jmol/popup/PopupResourceBundle.java:739
msgid "Backbone"
msgstr "Selgroog"

#: org/jmol/popup/PopupResourceBundle.java:572
msgid "Side Chains"
msgstr "Külgmised Ketid"

#: org/jmol/popup/PopupResourceBundle.java:573
msgid "Polar Residues"
msgstr "Polaarsed jäänused"

#: org/jmol/popup/PopupResourceBundle.java:574
msgid "Nonpolar Residues"
msgstr "Mittepolaarsed jäänused"

#: org/jmol/popup/PopupResourceBundle.java:575
msgid "Basic Residues (+)"
msgstr "Lihtsad jäänused (+)"

#: org/jmol/popup/PopupResourceBundle.java:576
msgid "Acidic Residues (-)"
msgstr "Happelised jäänused (-)"

#: org/jmol/popup/PopupResourceBundle.java:577
msgid "Uncharged Residues"
msgstr "Mittelaetud jäänused"

#: org/jmol/popup/PopupResourceBundle.java:578
msgid "Nucleic"
msgstr "Tuuma"

#: org/jmol/popup/PopupResourceBundle.java:580
msgid "DNA"
msgstr "DNA"

#: org/jmol/popup/PopupResourceBundle.java:581
#, fuzzy
msgid "RNA"
msgstr "ARN"

#: org/jmol/popup/PopupResourceBundle.java:583
msgid "Bases"
msgstr "Alused"

#: org/jmol/popup/PopupResourceBundle.java:584
msgid "AT pairs"
msgstr "AT paarid"

#: org/jmol/popup/PopupResourceBundle.java:585
msgid "GC pairs"
msgstr "GC paarid"

#: org/jmol/popup/PopupResourceBundle.java:586
msgid "AU pairs"
msgstr "AU paarid"

#: org/jmol/popup/PopupResourceBundle.java:587
#, fuzzy
msgid "Hetero"
msgstr "hetero"

#: org/jmol/popup/PopupResourceBundle.java:588
msgid "All PDB \"HETATM\""
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:589
#, fuzzy
msgid "All Solvent"
msgstr "Lahusti"

#: org/jmol/popup/PopupResourceBundle.java:590
#, fuzzy
msgid "All Water"
msgstr "Vesi"

#: org/jmol/popup/PopupResourceBundle.java:592
msgid "Nonaqueous Solvent"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:593
msgid "Nonaqueous HETATM"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:594
msgid "Ligand"
msgstr "Ligand"

#: org/jmol/popup/PopupResourceBundle.java:597
msgid "Carbohydrate"
msgstr "Süsinikhüdraat"

#: org/jmol/popup/PopupResourceBundle.java:598
msgid "None of the above"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:600
#, fuzzy
msgid "Style"
msgstr "Määra Z Kiirus"

#: org/jmol/popup/PopupResourceBundle.java:601
msgid "Scheme"
msgstr "Skeem"

#: org/jmol/popup/PopupResourceBundle.java:602
msgid "CPK Spacefill"
msgstr "CPK Spacefill"

#: org/jmol/popup/PopupResourceBundle.java:603
msgid "Ball and Stick"
msgstr "Pall ja Kepp"

#: org/jmol/popup/PopupResourceBundle.java:604
msgid "Sticks"
msgstr "Kepid"

#: org/jmol/popup/PopupResourceBundle.java:605
msgid "Wireframe"
msgstr "Sõrestik"

#: org/jmol/popup/PopupResourceBundle.java:606
#: org/jmol/popup/PopupResourceBundle.java:653
#: org/jmol/popup/PopupResourceBundle.java:741
msgid "Cartoon"
msgstr "Pildijada"

#: org/jmol/popup/PopupResourceBundle.java:607
#: org/jmol/popup/PopupResourceBundle.java:658
#: org/jmol/popup/PopupResourceBundle.java:740
msgid "Trace"
msgstr "Jälg"

#: org/jmol/popup/PopupResourceBundle.java:609
#: org/jmol/popup/PopupResourceBundle.java:698
msgid "Atoms"
msgstr "Aatomid"

#: org/jmol/popup/PopupResourceBundle.java:610
#: org/jmol/popup/PopupResourceBundle.java:619
#: org/jmol/popup/PopupResourceBundle.java:628
#: org/jmol/popup/PopupResourceBundle.java:640
#: org/jmol/popup/PopupResourceBundle.java:651
#: org/jmol/popup/PopupResourceBundle.java:661
#: org/jmol/popup/PopupResourceBundle.java:664
#: org/jmol/popup/PopupResourceBundle.java:765
#: org/jmol/popup/PopupResourceBundle.java:815
#: org/jmol/popup/PopupResourceBundle.java:854
msgid "Off"
msgstr "välja"

#: org/jmol/popup/PopupResourceBundle.java:611
#: org/jmol/popup/PopupResourceBundle.java:612
#: org/jmol/popup/PopupResourceBundle.java:613
#: org/jmol/popup/PopupResourceBundle.java:614
#: org/jmol/popup/PopupResourceBundle.java:615
#: org/jmol/popup/PopupResourceBundle.java:616
#, fuzzy, java-format
msgid "{0}% van der Waals"
msgstr "{0}% vanderWaals"

#: org/jmol/popup/PopupResourceBundle.java:618
#: org/jmol/popup/PopupResourceBundle.java:735
msgid "Bonds"
msgstr "Sidemed"

#: org/jmol/popup/PopupResourceBundle.java:620
#: org/jmol/popup/PopupResourceBundle.java:630
#: org/jmol/popup/PopupResourceBundle.java:641
#: org/jmol/popup/PopupResourceBundle.java:662
#: org/jmol/popup/PopupResourceBundle.java:665
#: org/jmol/popup/PopupResourceBundle.java:764
msgid "On"
msgstr "Sees"

#: org/jmol/popup/PopupResourceBundle.java:621
#: org/jmol/popup/PopupResourceBundle.java:622
#: org/jmol/popup/PopupResourceBundle.java:623
#: org/jmol/popup/PopupResourceBundle.java:624
#: org/jmol/popup/PopupResourceBundle.java:625
#: org/jmol/popup/PopupResourceBundle.java:633
#: org/jmol/popup/PopupResourceBundle.java:634
#: org/jmol/popup/PopupResourceBundle.java:635
#: org/jmol/popup/PopupResourceBundle.java:636
#: org/jmol/popup/PopupResourceBundle.java:637
#: org/jmol/popup/PopupResourceBundle.java:644
#: org/jmol/popup/PopupResourceBundle.java:645
#: org/jmol/popup/PopupResourceBundle.java:646
#: org/jmol/popup/PopupResourceBundle.java:647
#: org/jmol/popup/PopupResourceBundle.java:648
#: org/jmol/popup/PopupResourceBundle.java:667
#: org/jmol/popup/PopupResourceBundle.java:668
#: org/jmol/popup/PopupResourceBundle.java:877
#: org/jmol/popup/PopupResourceBundle.java:878
#: org/jmol/popup/PopupResourceBundle.java:879
#: org/jmol/popup/PopupResourceBundle.java:880
#: org/jmol/popup/PopupResourceBundle.java:881
#, fuzzy, java-format
msgid "{0} Å"
msgstr "{0} px"

#: org/jmol/popup/PopupResourceBundle.java:627
#: org/jmol/popup/PopupResourceBundle.java:736
msgid "Hydrogen Bonds"
msgstr "Vesiniku Sidemed"

#: org/jmol/popup/PopupResourceBundle.java:629
msgid "Calculate"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:631
msgid "Set H-Bonds Side Chain"
msgstr "Pane H-Sidemed Külgmisse Ketti"

#: org/jmol/popup/PopupResourceBundle.java:632
msgid "Set H-Bonds Backbone"
msgstr "Pane H-Sidemed Selgroogu"

#: org/jmol/popup/PopupResourceBundle.java:639
#: org/jmol/popup/PopupResourceBundle.java:737
msgid "Disulfide Bonds"
msgstr "Disulfiidi Sidemed"

#: org/jmol/popup/PopupResourceBundle.java:642
msgid "Set SS-Bonds Side Chain"
msgstr "Pane SS-Sidemed Külgmisse Ketti"

#: org/jmol/popup/PopupResourceBundle.java:643
msgid "Set SS-Bonds Backbone"
msgstr "Pane SS-Sidemed Selgroogu"

#: org/jmol/popup/PopupResourceBundle.java:650
msgid "Structures"
msgstr "Struktuurid"

#: org/jmol/popup/PopupResourceBundle.java:654
#, fuzzy
msgid "Cartoon Rockets"
msgstr "Pildijada"

#: org/jmol/popup/PopupResourceBundle.java:655
#: org/jmol/popup/PopupResourceBundle.java:742
msgid "Ribbons"
msgstr "Ribad"

#: org/jmol/popup/PopupResourceBundle.java:656
#: org/jmol/popup/PopupResourceBundle.java:743
#, fuzzy
msgid "Rockets"
msgstr "Pildijada"

#: org/jmol/popup/PopupResourceBundle.java:657
#: org/jmol/popup/PopupResourceBundle.java:744
msgid "Strands"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:660
#, fuzzy
msgid "Vibration"
msgstr "Tõlked"

#: org/jmol/popup/PopupResourceBundle.java:663
#: org/jmol/popup/PopupResourceBundle.java:748
msgid "Vectors"
msgstr "Vektorid"

#: org/jmol/popup/PopupResourceBundle.java:666
#, java-format
msgid "{0} pixels"
msgstr "{0} pikselit"

#: org/jmol/popup/PopupResourceBundle.java:669
#: org/jmol/popup/PopupResourceBundle.java:670
#: org/jmol/popup/PopupResourceBundle.java:671
#: org/jmol/popup/PopupResourceBundle.java:672
#: org/jmol/popup/PopupResourceBundle.java:673
#, java-format
msgid "Scale {0}"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:675
msgid "Stereographic"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:677
#, fuzzy
msgid "Red+Cyan glasses"
msgstr "Punane+Roheline klaasid"

#: org/jmol/popup/PopupResourceBundle.java:678
msgid "Red+Blue glasses"
msgstr "Punane+Sinine klaasid"

#: org/jmol/popup/PopupResourceBundle.java:679
msgid "Red+Green glasses"
msgstr "Punane+Roheline klaasid"

#: org/jmol/popup/PopupResourceBundle.java:680
msgid "Cross-eyed viewing"
msgstr "Rist-silmne vaatamine"

#: org/jmol/popup/PopupResourceBundle.java:681
msgid "Wall-eyed viewing"
msgstr "Otse vaatamine"

#: org/jmol/popup/PopupResourceBundle.java:683
#: org/jmol/popup/PopupResourceBundle.java:745
msgid "Labels"
msgstr "Sildid"

#: org/jmol/popup/PopupResourceBundle.java:686
msgid "With Element Symbol"
msgstr "Koos Elementide Sümbolitega"

#: org/jmol/popup/PopupResourceBundle.java:687
msgid "With Atom Name"
msgstr "Koos Aatomite Nimetustega"

#: org/jmol/popup/PopupResourceBundle.java:688
msgid "With Atom Number"
msgstr "Koos Aatomite Numbritega"

#: org/jmol/popup/PopupResourceBundle.java:690
msgid "Position Label on Atom"
msgstr "Pane Aatomile Silt"

#: org/jmol/popup/PopupResourceBundle.java:691
msgid "Centered"
msgstr "Tsentreeritud"

#: org/jmol/popup/PopupResourceBundle.java:692
msgid "Upper Right"
msgstr "Ülemine Parem"

#: org/jmol/popup/PopupResourceBundle.java:693
msgid "Lower Right"
msgstr "Alumine Vasak"

#: org/jmol/popup/PopupResourceBundle.java:694
msgid "Upper Left"
msgstr "Ülemine Vasak"

#: org/jmol/popup/PopupResourceBundle.java:695
msgid "Lower Left"
msgstr "Alumine Vasak"

#: org/jmol/popup/PopupResourceBundle.java:697
msgid "Color"
msgstr "Värv"

#: org/jmol/popup/PopupResourceBundle.java:700
msgid "By Scheme"
msgstr "Skeemi järgi"

#: org/jmol/popup/PopupResourceBundle.java:701
msgid "Element (CPK)"
msgstr "Element (CPK)"

#: org/jmol/popup/PopupResourceBundle.java:702
msgid "Alternative Location"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:703
#, fuzzy
msgid "Molecule"
msgstr "Mudel"

#: org/jmol/popup/PopupResourceBundle.java:704
msgid "Formal Charge"
msgstr "Formaalne Laeng"

#: org/jmol/popup/PopupResourceBundle.java:705
msgid "Partial Charge"
msgstr "Osaline Laeng"

#: org/jmol/popup/PopupResourceBundle.java:707
#, fuzzy
msgid "Amino Acid"
msgstr "Amiino"

#: org/jmol/popup/PopupResourceBundle.java:708
msgid "Secondary Structure"
msgstr "Teisene Struktuur"

#: org/jmol/popup/PopupResourceBundle.java:709
msgid "Chain"
msgstr "Kett"

#: org/jmol/popup/PopupResourceBundle.java:711
msgid "Inherit"
msgstr "Omane"

#: org/jmol/popup/PopupResourceBundle.java:712
msgid "Black"
msgstr "Must"

#: org/jmol/popup/PopupResourceBundle.java:713
msgid "White"
msgstr "Valge"

#: org/jmol/popup/PopupResourceBundle.java:714
msgid "Cyan"
msgstr "Rohekassinine"

#: org/jmol/popup/PopupResourceBundle.java:716
msgid "Red"
msgstr "Punane"

#: org/jmol/popup/PopupResourceBundle.java:717
msgid "Orange"
msgstr "Oraanz"

#: org/jmol/popup/PopupResourceBundle.java:718
msgid "Yellow"
msgstr "Kollane"

#: org/jmol/popup/PopupResourceBundle.java:719
msgid "Green"
msgstr "Roheline"

#: org/jmol/popup/PopupResourceBundle.java:720
msgid "Blue"
msgstr "Sinine"

#: org/jmol/popup/PopupResourceBundle.java:721
msgid "Indigo"
msgstr "Indigo"

#: org/jmol/popup/PopupResourceBundle.java:722
msgid "Violet"
msgstr "Violet"

#: org/jmol/popup/PopupResourceBundle.java:724
msgid "Salmon"
msgstr "Kollane"

#: org/jmol/popup/PopupResourceBundle.java:725
msgid "Olive"
msgstr "Oliivroheline"

#: org/jmol/popup/PopupResourceBundle.java:726
msgid "Maroon"
msgstr "Punapruun"

#: org/jmol/popup/PopupResourceBundle.java:727
#, fuzzy
msgid "Gray"
msgstr "Hall"

#: org/jmol/popup/PopupResourceBundle.java:728
msgid "Slate Blue"
msgstr "Sinine"

#: org/jmol/popup/PopupResourceBundle.java:729
msgid "Gold"
msgstr "Kuldne"

#: org/jmol/popup/PopupResourceBundle.java:730
msgid "Orchid"
msgstr "Orchid"

#: org/jmol/popup/PopupResourceBundle.java:732
#: org/jmol/popup/PopupResourceBundle.java:852
msgid "Make Opaque"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:733
#: org/jmol/popup/PopupResourceBundle.java:853
msgid "Make Translucent"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:738
#, fuzzy
msgid "Structure"
msgstr "Struktuurid"

#: org/jmol/popup/PopupResourceBundle.java:746
msgid "Background"
msgstr "Taust"

#: org/jmol/popup/PopupResourceBundle.java:747
#, fuzzy
msgid "Surface"
msgstr "Jälg"

#: org/jmol/popup/PopupResourceBundle.java:749
#: org/jmol/popup/PopupResourceBundle.java:863
#: org/jmol/popup/PopupResourceBundle.java:889
msgid "Axes"
msgstr "Teljed"

#: org/jmol/popup/PopupResourceBundle.java:750
#: org/jmol/popup/PopupResourceBundle.java:864
msgid "Boundbox"
msgstr "Boundbox"

#: org/jmol/popup/PopupResourceBundle.java:751
#: org/jmol/popup/PopupResourceBundle.java:865
msgid "Unitcell"
msgstr "Unitcell"

#: org/jmol/popup/PopupResourceBundle.java:753
msgid "Zoom"
msgstr "Suurenda"

#: org/jmol/popup/PopupResourceBundle.java:760
msgid "Zoom In"
msgstr "Suurenda sisse"

#: org/jmol/popup/PopupResourceBundle.java:761
msgid "Zoom Out"
msgstr "Vähenda"

#: org/jmol/popup/PopupResourceBundle.java:763
#: org/jmol/popup/PopupResourceBundle.java:826
msgid "Spin"
msgstr "Spinn"

#: org/jmol/popup/PopupResourceBundle.java:767
msgid "Set X Rate"
msgstr "Määra X Kiirus"

#: org/jmol/popup/PopupResourceBundle.java:768
msgid "Set Y Rate"
msgstr "Määra Y Kiirus"

#: org/jmol/popup/PopupResourceBundle.java:769
msgid "Set Z Rate"
msgstr "Määra Z Kiirus"

#: org/jmol/popup/PopupResourceBundle.java:770
#: org/jmol/popup/PopupResourceBundle.java:796
msgid "Set FPS"
msgstr "Määra FPS"

#: org/jmol/popup/PopupResourceBundle.java:780
#, fuzzy
msgid "Animation"
msgstr "Animatsiooni Reziim"

#: org/jmol/popup/PopupResourceBundle.java:781
msgid "Animation Mode"
msgstr "Animatsiooni Reziim"

#: org/jmol/popup/PopupResourceBundle.java:782
msgid "Play Once"
msgstr "Mängi Üks Kord"

#: org/jmol/popup/PopupResourceBundle.java:783
msgid "Palindrome"
msgstr "Palindroom"

#: org/jmol/popup/PopupResourceBundle.java:784
msgid "Loop"
msgstr "Tagasiside"

#: org/jmol/popup/PopupResourceBundle.java:786
msgid "Play"
msgstr "Mängi"

#: org/jmol/popup/PopupResourceBundle.java:787
#, fuzzy
msgid "Pause"
msgstr "Alused"

#: org/jmol/popup/PopupResourceBundle.java:788
#, fuzzy
msgid "Resume"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:789
msgid "Stop"
msgstr "Seiska"

#: org/jmol/popup/PopupResourceBundle.java:790
msgid "Next Frame"
msgstr "Järgmine Kaader"

#: org/jmol/popup/PopupResourceBundle.java:791
msgid "Previous Frame"
msgstr "Eelmine Kaader"

#: org/jmol/popup/PopupResourceBundle.java:792
msgid "Rewind"
msgstr "Keri Tagasi"

#: org/jmol/popup/PopupResourceBundle.java:793
#, fuzzy
msgid "Reverse"
msgstr "Mängi Tagurpidi"

#: org/jmol/popup/PopupResourceBundle.java:794
msgid "Restart"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:803
#, fuzzy
msgid "Measurement"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:804
msgid "Double-Click begins and ends all measurements"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:805
#, fuzzy
msgid "Click for distance measurement"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:806
#, fuzzy
msgid "Click for angle measurement"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:807
msgid "Click for torsion (dihedral) measurement"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:808
#, fuzzy
msgid "Delete measurements"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:809
#, fuzzy
msgid "List measurements"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:810
msgid "Distance units nanometers"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:811
msgid "Distance units Angstroms"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:812
msgid "Distance units picometers"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:814
#, fuzzy
msgid "Set picking"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:816
#, fuzzy
msgid "Center"
msgstr "Tsentreeritud"

#: org/jmol/popup/PopupResourceBundle.java:818
msgid "Identity"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:819
#, fuzzy
msgid "Label"
msgstr "Sildid"

#: org/jmol/popup/PopupResourceBundle.java:820
#, fuzzy
msgid "Select atom"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:821
#, fuzzy
msgid "Select chain"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:822
#, fuzzy
msgid "Select element"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:823
#, fuzzy
msgid "Select group"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:824
#, fuzzy
msgid "Select molecule"
msgstr "Määra Valiku Reziim"

#: org/jmol/popup/PopupResourceBundle.java:825
#, fuzzy
msgid "Select site"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:828
msgid "Show"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:829
#, fuzzy
msgid "Console"
msgstr "Konsool..."

#: org/jmol/popup/PopupResourceBundle.java:830
msgid "File Contents"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:831
msgid "File Header"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:833
msgid "Isosurface JVXL data"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:834
#, fuzzy
msgid "Measure"
msgstr "Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:835
msgid "Molecular orbital JVXL data"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:836
msgid "Model"
msgstr "Mudel"

#: org/jmol/popup/PopupResourceBundle.java:837
#, fuzzy
msgid "Orientation"
msgstr "Tõlked"

#: org/jmol/popup/PopupResourceBundle.java:838
#, fuzzy
msgid "Space group"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:840
msgid "Current state"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:841
#, fuzzy
msgid "Unit cell"
msgstr "Unitcell"

#: org/jmol/popup/PopupResourceBundle.java:842
msgid "Extract MOL data"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:844
#, fuzzy
msgid "Surfaces"
msgstr "Jälg"

#: org/jmol/popup/PopupResourceBundle.java:845
#, fuzzy
msgid "Dot Surface"
msgstr "Jälg"

#: org/jmol/popup/PopupResourceBundle.java:846
#, fuzzy
msgid "van der Waals Surface"
msgstr "{0}% vanderWaals"

#: org/jmol/popup/PopupResourceBundle.java:847
#, fuzzy
msgid "Molecular Surface"
msgstr "Mudel"

#: org/jmol/popup/PopupResourceBundle.java:848
#, java-format
msgid "Solvent Surface ({0}-Angstrom probe)"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:850
#, java-format
msgid "Solvent-Accessible Surface (VDW + {0} Angstrom)"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:851
msgid "Molecular Electrostatic Potential"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:857
#: org/jmol/popup/PopupResourceBundle.java:858
#: org/jmol/popup/PopupResourceBundle.java:859
#, fuzzy, java-format
msgid "Reload {0}"
msgstr "Skaala {0}"

#: org/jmol/popup/PopupResourceBundle.java:860
msgid "Reload + Polyhedra"
msgstr ""

#: org/jmol/popup/PopupResourceBundle.java:867
msgid "Hide"
msgstr "Peida"

#: org/jmol/popup/PopupResourceBundle.java:868
msgid "Dotted"
msgstr "Punktiir"

#: org/jmol/popup/PopupResourceBundle.java:870
msgid "Pixel Width"
msgstr "Pikseli Laius"

#: org/jmol/popup/PopupResourceBundle.java:871
#: org/jmol/popup/PopupResourceBundle.java:872
#: org/jmol/popup/PopupResourceBundle.java:873
#: org/jmol/popup/PopupResourceBundle.java:874
#, java-format
msgid "{0} px"
msgstr "{0} px"

#: org/jmol/popup/PopupResourceBundle.java:876
msgid "Angstrom Width"
msgstr "Angstromi Laius"

#: org/jmol/popup/PopupResourceBundle.java:884
#, fuzzy
msgid "Selection Halos"
msgstr "Vali"

#: org/jmol/popup/PopupResourceBundle.java:885
msgid "Show Hydrogens"
msgstr "Näita Vesinikke"

#: org/jmol/popup/PopupResourceBundle.java:886
msgid "Show Measurements"
msgstr "Näita Mõõted"

#: org/jmol/popup/PopupResourceBundle.java:887
msgid "Perspective Depth"
msgstr "Perspetkiivi Sügavus"

#: org/jmol/popup/PopupResourceBundle.java:888
#, fuzzy
msgid "Bound Box"
msgstr "Boundbox"

#: org/jmol/popup/PopupResourceBundle.java:890
#, fuzzy
msgid "Unit Cell"
msgstr "Unitcell"

#: org/jmol/popup/PopupResourceBundle.java:891
msgid "RasMol Colors"
msgstr "RasMol Värvid"

#: org/jmol/popup/PopupResourceBundle.java:893
msgid "About Jmol"
msgstr "Jmol'ist"

#: org/jmol/popup/PopupResourceBundle.java:898
msgid "Mouse Manual"
msgstr "Hiire Õpetus"

#: org/jmol/popup/PopupResourceBundle.java:899
msgid "Translations"
msgstr "Tõlked"

#: org/jmol/viewer/Compiler.java:850
#, java-format
msgid "missing END for {0}"
msgstr ""

#: org/jmol/viewer/Compiler.java:2399
#, java-format
msgid "invalid context for {0}"
msgstr ""

#: org/jmol/viewer/Compiler.java:2404 org/jmol/viewer/Eval.java:4783
msgid "command expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2408
#, java-format
msgid "invalid expression token: {0}"
msgstr ""

#: org/jmol/viewer/Compiler.java:2412
#, java-format
msgid "unrecognized token: {0}"
msgstr ""

#: org/jmol/viewer/Compiler.java:2416 org/jmol/viewer/Eval.java:10005
msgid "unexpected end of script command"
msgstr ""

#: org/jmol/viewer/Compiler.java:2420 org/jmol/viewer/Eval.java:10009
msgid "bad argument count"
msgstr ""

#: org/jmol/viewer/Compiler.java:2424
msgid "end of expression expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2428
msgid "right parenthesis expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2432
msgid "left parenthesis expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2436
msgid "right brace expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2440
msgid "right bracket expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2444
msgid "{ number number number } expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2448
#, java-format
msgid "unrecognized expression token: {0}"
msgstr ""

#: org/jmol/viewer/Compiler.java:2452
msgid "comparison operator expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2456
msgid "equal sign expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2460 org/jmol/viewer/Eval.java:9971
msgid "number expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2464
msgid "number or variable name expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2468 org/jmol/viewer/Eval.java:10017
#, java-format
msgid "unrecognized {0} parameter"
msgstr ""

#: org/jmol/viewer/Compiler.java:2472
msgid "identifier or residue specification expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2476
msgid "residue specification (ALA, AL?, A*) expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2480
msgid "invalid chain specification"
msgstr ""

#: org/jmol/viewer/Compiler.java:2484
msgid "invalid model specification"
msgstr ""

#: org/jmol/viewer/Compiler.java:2488
msgid "invalid atom specification"
msgstr ""

#: org/jmol/viewer/Compiler.java:2497 org/jmol/viewer/Eval.java:9976
msgid "quoted string expected"
msgstr ""

#: org/jmol/viewer/Compiler.java:2501
msgid "comma or right parenthesis expected"
msgstr ""

#: org/jmol/viewer/Eval.java:280 org/jmol/viewer/Eval.java:4764
msgid "script ERROR: "
msgstr ""

#: org/jmol/viewer/Eval.java:308
msgid "too many script levels"
msgstr ""

#: org/jmol/viewer/Eval.java:2301
#, java-format
msgid ""
"plane expected -- either three points or atom expressions or {0} or {1} or "
"{2}"
msgstr ""

#: org/jmol/viewer/Eval.java:2314
#, fuzzy
msgid "No unit cell"
msgstr "Unitcell"

#: org/jmol/viewer/Eval.java:2324
msgid "Miller indices cannot all be zero."
msgstr ""

#: org/jmol/viewer/Eval.java:2481
msgid "a color or palette name (Jmol, Rasmol) is required"
msgstr ""

#: org/jmol/viewer/Eval.java:3336
#, java-format
msgid "{0} connections deleted"
msgstr ""

#: org/jmol/viewer/Eval.java:3357
#, java-format
msgid "{0} connections modified or created"
msgstr ""

#: org/jmol/viewer/Eval.java:4047
#, java-format
msgid "space group {0} was not found."
msgstr ""

#: org/jmol/viewer/Eval.java:4334
#, fuzzy
msgid "bad atom number"
msgstr "Koos Aatomite Numbritega"

#: org/jmol/viewer/Eval.java:4633
msgid "too many rotation points were specified"
msgstr ""

#: org/jmol/viewer/Eval.java:4669
msgid "rotation points cannot be identical"
msgstr ""

#: org/jmol/viewer/Eval.java:5424
#, fuzzy, java-format
msgid "{0} hydrogen bonds"
msgstr "Vesiniku Sidemed"

#: org/jmol/viewer/Eval.java:5436
#, java-format
msgid "{0} not allowed with background model displayed"
msgstr ""

#: org/jmol/viewer/Eval.java:5741
msgid "Calculate what?"
msgstr ""

#: org/jmol/viewer/Eval.java:5971
msgid "trajectory not applicable in this context"
msgstr ""

#: org/jmol/viewer/Eval.java:6105
#, java-format
msgid "invalid {0} control keyword"
msgstr ""

#: org/jmol/viewer/Eval.java:7740
msgid "save what?"
msgstr ""

#: org/jmol/viewer/Eval.java:7787
msgid "restore what?"
msgstr ""

#: org/jmol/viewer/Eval.java:7960
#, java-format
msgid "write what? {0} or {1} \"filename\""
msgstr ""

#: org/jmol/viewer/Eval.java:8008
msgid "No data available"
msgstr ""

#: org/jmol/viewer/Eval.java:8406 org/jmol/viewer/Eval.java:9215
msgid "no MO basis/coefficient data available for this frame"
msgstr ""

#: org/jmol/viewer/Eval.java:9219
msgid "no MO coefficient data available"
msgstr ""

#: org/jmol/viewer/Eval.java:9221
msgid "Only one molecular orbital is available in this file"
msgstr ""

#: org/jmol/viewer/Eval.java:9231
msgid "no MO occupancy data available"
msgstr ""

#: org/jmol/viewer/Eval.java:9241
#, java-format
msgid "An MO index from 1 to {0} is required"
msgstr ""

#: org/jmol/viewer/Eval.java:9732
msgid ""
"No partial charges were read from the file; Jmol needs these to render the "
"MEP data."
msgstr ""

#: org/jmol/viewer/Eval.java:9922
#, java-format
msgid "{0} require that only one model be displayed"
msgstr ""

#: org/jmol/viewer/Eval.java:9926
msgid "unrecognized command"
msgstr ""

#: org/jmol/viewer/Eval.java:9930
msgid "unrecognized atom property"
msgstr ""

#: org/jmol/viewer/Eval.java:9934
msgid "unrecognized bond property"
msgstr ""

#: org/jmol/viewer/Eval.java:9938
msgid "filename expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9942
msgid "boolean expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9946
msgid "boolean or number expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9950
#, java-format
msgid "boolean, number, or {0} expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9954
msgid "(atom expression) or integer expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9958
msgid "valid (atom expression) expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9962
msgid "bad [R,G,B] color"
msgstr ""

#: org/jmol/viewer/Eval.java:9966
msgid "integer expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9981
msgid "quoted string or identifier expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9985
msgid "property name expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9989
msgid "x y z axis expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9993
msgid "color expected"
msgstr ""

#: org/jmol/viewer/Eval.java:9997
msgid "unrecognized object"
msgstr ""

#: org/jmol/viewer/Eval.java:10001
msgid "runtime unrecognized expression"
msgstr ""

#: org/jmol/viewer/Eval.java:10013
msgid "invalid argument"
msgstr ""

#: org/jmol/viewer/Eval.java:10021
#, java-format
msgid "unrecognized SHOW parameter --  use {0}"
msgstr ""

#: org/jmol/viewer/Eval.java:10025
#, java-format
msgid "integer out of range ({0} - {1})"
msgstr ""

#: org/jmol/viewer/Eval.java:10030
#, java-format
msgid "decimal number out of range ({0} - {1})"
msgstr ""

#: org/jmol/viewer/Eval.java:10035
#, java-format
msgid "number must be ({0} or {1})"
msgstr ""

#: org/jmol/viewer/Eval.java:10040
msgid "file not found"
msgstr ""

#: org/jmol/viewer/Eval.java:10044
msgid "draw object not defined"
msgstr ""

#: org/jmol/viewer/Eval.java:10048
msgid "object name expected after '$'"
msgstr ""

#: org/jmol/viewer/Eval.java:10053
msgid " {x y z} or $name or (atom expression) required"
msgstr ""

#: org/jmol/viewer/Eval.java:10057
msgid "keyword expected"
msgstr ""

#: org/jmol/viewer/Eval.java:10061
msgid "invalid parameter order"
msgstr ""

#: org/jmol/viewer/Eval.java:10065
msgid "incompatible arguments"
msgstr ""

#: org/jmol/viewer/Eval.java:10069
msgid "insufficient arguments"
msgstr ""

#: org/jmol/viewer/Eval.java:11617
msgid "too many parentheses"
msgstr ""

#: org/jmol/viewer/PickingManager.java:169
msgid "pick one more atom in order to spin the model around an axis"
msgstr ""

#: org/jmol/viewer/PickingManager.java:170
msgid "pick two atoms in order to spin the model around an axis"
msgstr ""

#: org/jmol/viewer/SelectionManager.java:72
#: org/jmol/viewer/SelectionManager.java:88
#, fuzzy, java-format
msgid "{0} atoms hidden"
msgstr "Nimi peidetud"

#: org/jmol/viewer/SelectionManager.java:178
#, fuzzy, java-format
msgid "{0} atoms selected"
msgstr "Ei ühtegi aatomit"

#: org/jmol/viewer/Viewer.java:4010
msgid "ERROR: cannot set boolean flag to string value"
msgstr ""

#: org/jmol/viewer/Viewer.java:4138
msgid "ERROR: cannot set boolean flag to numeric value"
msgstr ""

#: org/jmol/viewer/Viewer.java:4642
msgid "ERROR: Cannot set value of this variable to a boolean."
msgstr ""

#~ msgid "Select"
#~ msgstr "Vali"

#, fuzzy
#~ msgid "Compatibility"
#~ msgstr "RasMol/Chime Ühilduvus"

#, fuzzy
#~ msgid "RasMol/Chime Settings"
#~ msgstr "RasMol/Chime Ühilduvus"

#~ msgid "Axes RasMol/Chime"
#~ msgstr "Teljed RasMol/Chime"

#, fuzzy
#~ msgid "Zero-Based Xyz Rasmol"
#~ msgstr "Zero Based Xyz Rasmol"

#~ msgid "Jmol executing script ..."
#~ msgstr "Jmol käivitab skripti ..."

#, fuzzy
#~ msgid "use {0}"
#~ msgstr "Skaala {0}"

#~ msgid "Animate"
#~ msgstr "Animeeri"

#~ msgid "Frame"
#~ msgstr "Raamistik"

#~ msgid "All Frames"
#~ msgstr "Kõik Raamistikud"

#~ msgid "Set Select Mode"
#~ msgstr "Määra Valiku Reziim"

#~ msgid "Replace Selection"
#~ msgstr "Asenda Valik"

#~ msgid "Add to Selection (OR)"
#~ msgstr "Lisa Valikusse (OR)"

#~ msgid "Narrow Selection (AND)"
#~ msgstr "Kitsenda Valikut (AND)"

#~ msgid "Render"
#~ msgstr "Renderda"

#~ msgid "Crimson"
#~ msgstr "Lilla"

#~ msgid "Dark Red"
#~ msgstr "Tumepunane"

#~ msgid "Firebrick"
#~ msgstr "Helepunane"

#~ msgid "Indian Red"
#~ msgstr "Indiaanipunane"

#~ msgid "Dark Magenta"
#~ msgstr "Tumelilla"

#~ msgid "Dark Salmon"
#~ msgstr "Tumekollane"

#~ msgid "Light Salmon"
#~ msgstr "Helekollane"

#~ msgid "Deep Pink"
#~ msgstr "Tumeroosa"

#~ msgid "Light Pink"
#~ msgstr "Heleroosa"

#~ msgid "Goldenrod"
#~ msgstr "Kuldkollane"

#~ msgid "Lemon Chiffon"
#~ msgstr "Sidrunkollane"

#~ msgid "Yellow-Green"
#~ msgstr "Kollaroheline"

#~ msgid "Lime"
#~ msgstr "Laimiroheline"

#~ msgid "Seagreen"
#~ msgstr "Mereroheline"

#~ msgid "Green-Blue"
#~ msgstr "Rohesinine"

#~ msgid "Spring Green"
#~ msgstr "Sügisroheline"

#~ msgid "Aqua"
#~ msgstr "Aqua"

#~ msgid "Azure"
#~ msgstr "Taevasinine"

#~ msgid "Carolina Blue"
#~ msgstr "Caroliina-sinine"

#~ msgid "Cadet Blue"
#~ msgstr "Mundrisinine"

#~ msgid "Cornflower"
#~ msgstr "Sinililla"

#~ msgid "Dark Slate Blue"
#~ msgstr "Tumesinine"

#~ msgid "Light Steel Blue"
#~ msgstr "Sinakashall"

#~ msgid "Charge"
#~ msgstr "Laeng"

#~ msgid "Crystal"
#~ msgstr "Kristall"

#~ msgid "Aquamarine"
#~ msgstr "Akvamariin"

#~ msgid "Forest Green"
#~ msgstr "Metsaroheline"

#~ msgid "Hot Pink"
#~ msgstr "Kuumroosa"

#~ msgid "Options"
#~ msgstr "Valikud"

#, fuzzy
#~ msgid "Show Selected Atoms"
#~ msgstr "Näita Valikuvarje"

#~ msgid "Jmol Colors"
#~ msgstr "Jmol Värvid"

#, fuzzy
#~ msgid "or dotted"
#~ msgstr "Punktiir"

#~ msgid "Except Solvent"
#~ msgstr "Välista Lahustit"

#~ msgid "Except Water"
#~ msgstr "Välista Vett"

#~ msgid "Other"
#~ msgstr "Muu"

#~ msgid "Lipid"
#~ msgstr "Lipiid"

#~ msgid "Nanometers"
#~ msgstr "Nanomeetrid"

#~ msgid "Angstroms"
#~ msgstr "Angsromid"

#~ msgid "Picometers"
#~ msgstr "Picomeetrid"

#~ msgid "Open"
#~ msgstr "Ava"

#~ msgid "Wireframe Rotation"
#~ msgstr "Sõrestiku Keeramine"

#~ msgid ""
#~ "Jmol Applet.  Part of the OpenScience project. See http://www.jmol.org "
#~ "for more information"
#~ msgstr ""
#~ "Jmol Applet. On osa OpenScience projektist. Täpsemalt vaata http://www."
#~ "jmol.org"
