EBUILDS := CGI Config-Simple Class-Std DBIx-Class namespace-clean \
	   Package-Stash DBIx-Class Context-Preserve Devel-GlobalDestruction \
	   Module-Find CGI-Session DBD-Pg

update:
	sudo perl-cleaner --allmodules
	sudo emerge $(EBUILDS)
	sudo perl -MCPAN -e'install Class::C3::Componentised'
	sudo perl -MCPAN -e'install B::Hooks::EndOfScope'
