# README #
### What is this repository for? ###

* Quick summary
This is CIPRO database system UI.  CIPRO is Integrated Ciona intestinalis Protein Database.
The database includes comprehensive information for the tunicate Ciona intestinalis proteome, 
as well as related information including gene, transcrippts, annotation and homology.

* Version
The final version of the database was 2.5 as of the project end in 2013.
Database body will be put in repository separately.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
#### Database setup ####
Setup postgresql >=9.0.

    $ git clone _cipro-database_
    $ createuser -U postgres -d cipro
    $ pg_restore -U cipro cipro_v2.5

#### Web UI setup ####
    $ cd _webdir_
    $ git clone _cipro_

* Configuration
* Dependencies
Setup postgresql >=9.0.

* Database configuration
Edit conf/cipro/cipro.ini for database setting.
The default port is set as 5433, which differs from normal postgresql setup.

* How to run tests
Access the web directory.

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact