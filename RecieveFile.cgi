#!/usr/bin/perl
use DBI;
use JSON;
use CGI qw/:all/;
use XML::DOM;
use XML::Twig;
use XML::TreePP;
use Text::ParseWords;

my $dbh=DBI->connect("dbi:Pg:dbname=cipro_v2.5;host=cipro6.ibio.jp;", "postgres", "", {AutoCommit => 0});
my $sid=cookie('at-ciprojp');
my $insert = $dbh->prepare("insert into annotname (basic_info_id,cname,sid,uname,evidence) values ((select id from basic_info where cipro = ? AND deletedate is null limit 1),?,?,?,?)");
my $update = $dbh->prepare("update annotname set cname = ?,uname = ?,evidence = ?, createdate = now() where basic_info_id in (select distinct basic_info.id from basic_info left join seq_info on basic_info_id = basic_info.id WHERE sha256 in (select sha256 from basic_info left join seq_info on basic_info_id = basic_info.id where  basic_info.cipro = ?) AND basic_info.deletedate IS NULL) and sid = ?");

my $LOG;
my $fh = upload('inputFile');
while(<$fh>){
    my($id,$cname,$uname,$date,$evidence,$length,$mw,$pi) = quotewords( ',', 0, $_);
    next if($id =~ /ID/);
    next unless($id);
    #$LOG .= "$evidence\n";
    $evidence =~ s/,\s+/,/g;
    $evidence =~ s/(2D-PAGE|MS\/MS|3DPL|EST|Microarray|etc)/experimental,$1/g;
    $evidence =~ s/(KEGG OC|Phylogeny|InterProScan|TMHMM|BLASTP|MODELLER|PSIPRED|NetPhos|OMIM|WoLF PSORT|etc)/theoretical,$1/g;
    $evidence =~ s/theoretical,theoretical/theoretical/g;
    $evidence =~ s/experimental,experimental/experimental/g;
    $evidence .= ",";
    #$LOG .= "$evidence\n";
    my @pairs = ($evidence =~ /(?:ciona|human|mouse|hagfish|lamprey|pacific_transparent_sea_squirt|common_sea_squirt|amphioxus|sea_urchin|nematode|fly|jgi1|mim|aniseed|interpro|PIR|TIGRFAMs|SEG|HAMAP|GENE3D|PFAM|SMART|PANTHER|PRINTS|PRODOM|SUPERFAMILY|COIL|PROFILE|PROSITE|go|literature|homology|url|experimental|theoretical|noevidence),.+?,/g);
    #$LOG .= "@pairs\n";
    my @evidence;
    map {my($name,$id) = split /,/,$_;push @evidence,{-name => $name,-id=>$id}} @pairs;
    my $treepp = XML::TreePP->new();
    my $tree = { types => {type => [@evidence]}};
    my $xml = $treepp->write( $tree );
    my $twig= new XML::Twig;
    $twig->set_indent(" "x4);
    $twig->parse($xml);
    $twig->set_pretty_print("indented");
    $xml = $twig->sprint;
    #$LOG .= "$xml\n";
    my $select = $dbh->prepare("select * from annotname where basic_info_id = (select id from basic_info where cipro = ? AND deletedate is null limit 1) and sid = ?");
    my @xseq = map {$_->[0]} @{$dbh->selectall_arrayref("select distinct basic_info.cipro from basic_info left join seq_info on basic_info_id = basic_info.id WHERE sha256 in (select sha256 from basic_info left join seq_info on basic_info_id = basic_info.id where  basic_info.cipro = '$id') AND basic_info.deletedate IS NULL")};
    foreach my $i (@xseq){
	$select->execute($i,$sid);
	#$LOG .= $select->rows,"\n";
	if($select->rows){
	    $update->execute($cname,$uname,$xml,$i,$sid) or warn $update->errstr;
	}else{
	    $insert->execute($i,$cname,$sid,$uname,$xml) or warn $insert->errstr;
	}
    }       
}
$dbh->commit or die $dbh->errstr;
$dbh->disconnect;

my $obj;
if(param('inputFile')){
    $obj = {'status' => "success",
	    'name' => "default",
	    'request' => [],
	    'postvars' => [],
	    #'content' => $content,
	    #'log' => $LOG,
	    'details' => []
    };
}else{
    $obj = {'status' => "failed",
	    'details' => ""};
}

print start_html;
print '<textarea>',to_json($obj),'</textarea>',"\n";
print end_html;
