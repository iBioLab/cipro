#!/usr/bin/perl
use strict;
use warnings;
use lib "../lib";

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp;
use CIPRO::Service::User;
use CIPRO::SessionUtil;

my $userId = param('userId');
my $passwd = param('passwd');

my $cgi = CGI->new();
my $userService = CIPRO::Service::User->new();
my $sessionUtil = CIPRO::SessionUtil->new();

if(!$userService->isAuthUser($userId, $passwd)) {
    $cgi->delete_all();
    print $cgi->redirect(-uri => './index.cgi?error=login');
}
else {
    $cgi->delete_all();
    my $cookie = $sessionUtil->createNewSession();
    print $cgi->redirect(-uri => './menu.cgi', -cookie => $cookie);
}

