#!/usr/bin/perl

use lib "../lib";
use strict;
use warnings;

use DBI;
use CGI qw/:all/;
use CGI::Carp;

use CIPRO::Service::Link;
use CIPRO::SessionUtil;

use constant NAME => "ciprolink";
use constant UPDATE_BUTTON_NAME => " 更新 ";
use constant DELETE_BUTTON_NAME => " 削除 ";
use constant CREATE_BUTTON_NAME => " 追加 ";


my $cgi = CGI->new();
my $linkService = CIPRO::Service::Link->new();
my $sessionUtil = CIPRO::SessionUtil->new();

if(!$sessionUtil->isValidSessionId($cgi)) {
    $cgi->delete_all();
    print $cgi->redirect('./index.cgi?error=session');
}



###--各パラメータ受け取り--###
my $id = param('box1');
my $name = param('box2');
my $url = param('box3');
my $category = param('box4');
my $up = param('upb');
my $del = param('delb');
my $add = param('addb');
my $Name_a = param('name');
my $URL_a = param('url');
my $Category_a = param('category');

my $message = "";

print header(-expires=>'now', charset=>'utf-8'), start_html(-title=>uc NAME);


###--データ編集--###
if ($up eq UPDATE_BUTTON_NAME){

    my $data = $linkService->updateById($id, $name, $url, $category);
    if ($data){
        $message = "id=".$id."　を更新しました";
    }
    else{
        $message = "該当データがありません";
    }

###--データ削除--###
}elsif ($del eq DELETE_BUTTON_NAME){
    my $data = $linkService->deleteById($id);

     if ($data){
        $message = "id=".$id."　を削除しました";
    }
    else{
        $message = "該当データがありません";
    }

###--データ追加--###
}elsif ($add eq CREATE_BUTTON_NAME){
    my $data = $linkService->add($Name_a, $URL_a, $Category_a);

    $message = "name= ".$Name_a.", url= ".$URL_a."　を追加しました";
} 


###--テーブル情報取得--###
my $seq_rs = $linkService->selectAll();

print '<h3>ニュースのメンテナンス画面</h3>';
print '<a href="./menu.cgi">管理者用メニュー</a> > ニュースのメンテナンス';
print '<br/>';
print '<br/>';


###--テーブル作成--###
print "<table border=2 align=center>
       <caption><h4>【ciprolinkテーブル一覧】</h4></caption><tr>
       <th>id</th>
       <th>name</th>
       <th>url</th>
       <th>category</th>
       <th>　update　</th>
       <th>　delete　</th></tr>";

my $inc = 0;

while(my $row = $seq_rs->next)
{
   my $inc++ ;
   my $formid = "form$inc";
  print '
   <tr><form name = "form$inc" id = "form$inc" method="post" action = "ciprolink.cgi">
   <td><input type = "hidden" name="box1" value= "'.$row->id.'" >'.$row->id.'</td>
   <td><input type = "text" name="box2" value= "'.$row->name.'" ></td>
   <td><input type = "text" name="box3" value= "'.$row->url.'" size="60"></td>
   <td><select name="box4" >';
   
    if ($row->category eq "Related Links"){
      print '<option>Ciona Links</option>';
      print '<option selected ="selected">Related Links</option>'}
    else{
      print '<option selected ="selected">Ciona Links</option>';
      print '<option>Related Links</option>'}

  print ' 
   </select></td>
   <td align=center><input type = "submit" name="upb" value = "'.UPDATE_BUTTON_NAME.'"></td>
   <td align=center><input type = "submit" name="delb" value = "'.DELETE_BUTTON_NAME.'" onClick="return check$inc(this);"></form></td></tr>

   <script Language="JavaScript">
       function check$inc(){
         if(!confirm("本当に削除しますか？")){
             return false;
         }
         var fm = document.$formid;
         fm.submit();
       }
   </script>';

}

###--データ追加フォーム作成--###
print '
   </table></br><br></br><br></br>
   <div align="center"> <h4>【データ追加フォーム】</h4> </div> 
   <table cellpadding="25" border="2" align="center"> 
   <tr> <td bgcolor="#eeeeff"> 
   <form method="post" >
       name:　　&nbsp;<input type="text" name="name" size="30"><br/>
       url:　 　&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="url" size="50" value="http://"><br/>
       category:&nbsp;<select name="category">
       <option>Ciona Links</option>
       <option>Related Links</option>
       </select>
     <p><input type="submit" name="addb" value="'.CREATE_BUTTON_NAME.'"></p>
   </form>
   </td></tr></table> <br>
';

if( $message ne "") {
    print '<font color="red"><h4>'.$message.'</h4></font>';
}

###--例外発生時のエラーメッセージ出力--###
if( $@ ){  
    print "ERROR!!<br> $@\n";
}


print end_html;








