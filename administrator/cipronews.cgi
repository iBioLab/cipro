#!/usr/bin/perl
use strict;
use warnings;
use lib "../lib";

use DBI;
use CGI qw/:all/;
use CGI::Carp;
use CIPRO::Service::News;
use CIPRO::SessionUtil;

use constant NAME => "cipronews";
use constant UPDATE_BUTTON_NAME => " 更新 ";
use constant DELETE_BUTTON_NAME => " 削除 ";
use constant CREATE_BUTTON_NAME => " 追加 ";

my $cgi = CGI->new();
my $newsService = CIPRO::Service::News->new();
my $sessionUtil = CIPRO::SessionUtil->new();

if(!$sessionUtil->isValidSessionId($cgi)) {
    $cgi->delete_all();
    print $cgi->redirect('./index.cgi?error=session');
}

###--各パラメータ受け取り--###
my $id     = param('box1');
my $news   = param('box2');
my $show   = param('show') eq 'on' ? 'TRUE': 'FALSE';
my $up     = param('upb');
my $del    = param('delb');
my $add    = param('addb');
my $news_a = param('news');

my $message = "";

print header(-expires=>'now', charset=>'utf-8'), 
                            start_html(-title=>uc NAME);

###--データ更新--###
if ($up eq UPDATE_BUTTON_NAME) {
    my $data = $newsService->updateById($id, $news, $show);
    if ($data){
        $message = "id=".$id."　を更新しました";
    }
    else{
        $message = "該当データがありません: ". $id;
    }

###--データ削除--###
}
elsif ($del eq DELETE_BUTTON_NAME) {
    my $data = $newsService->deleteById($id);
    if ($data){
        $message = "id=".$id."　を削除しました";
    }
    else{
        $message = "該当データがありません";
    }


###--データ追加--###
}
elsif ($add eq CREATE_BUTTON_NAME) {
    my $data = $newsService->add($news_a);
    $message = "news= ".$news_a."　を追加しました";
} 


###--テーブル情報取得--###
my $seq_rs = $newsService->selectAll();

print '<h3>ニュースのメンテナンス画面</h3>';
print '<a href="./menu.cgi">管理者用メニュー</a> > ニュースのメンテナンス';
print '<br/>';
print '<br/>';

###--テーブル作成--###
   
print '<div align="left"> <h4>【データ一覧】</h4></div> 
       <table border="1">
       <tr>
       <th>id</th>
       <th>show</th>
       <th>news</th>
       <th>&nbsp;&nbsp;update&nbsp;&nbsp;</th>
       <th>&nbsp;&nbsp;delete&nbsp;&nbsp;</th></tr>';

my $inc = 0;

while(my $row = $seq_rs->next)
{
   $inc++ ;
   my $formid = "form$inc";
  print '
   <tr><form name = "form$inc" id = "form$inc" method="post" action = "cipronews.cgi">
   <td><input type = "hidden" name="box1" value= "'.$row->id.'" >'.$row->id.'</td>
   <td><input type = "checkbox" name = "show"'.($row->show?'checked':'').'></td>
   <td><textarea name="box2" cols="60" rows="1">'.$row->news.'</textarea></td>
   <td align=center><input type = "submit" name="upb" value = "'.UPDATE_BUTTON_NAME.'"></td>
   <td align=center><input type = "submit" name="delb" value = "'.DELETE_BUTTON_NAME.'" onClick="return check$inc(this);"></form></td></tr>

   <script Language="JavaScript">
       function check$inc(){
         if(!confirm("本当に削除しますか？")){
             return false;
         }
         var fm = document.$formid;
         fm.submit();
       }
   </script>';

}

###--データ追加フォーム作成--###
print '
   </table><br /><br /><br />
   <div align="left"> <h4>【データ追加】</h4></div> 
   <table cellpadding="5" border="2"> 
   <tr> <td bgcolor="#eeeeff"> 
   <form method="post" >
       news　<br><textarea name="news" cols="60" rows="1"></textarea>
     <p><input type="submit" name="addb" value="'.CREATE_BUTTON_NAME.'"></p>
   </form>
   </td></tr></table> <br>
';

if( $message ne "") {
    print '<font color="red"><h4>'.$message.'</h4></font>';
}

###--例外発生時のエラーメッセージ出力--###
if( $@ ){  
    print "ERROR!!<br> $@\n";
}

print end_html;

