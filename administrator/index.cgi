#!/usr/bin/perl
use strict;
use warnings;
use lib "../lib";

use CGI qw/:all/;
use CIPRO::SessionUtil;

my $cgi = new CGI;
my $error = param('error') || "";

print $cgi->header(-charset=>'utf-8'),
      $cgi->start_html(-lang=>'ja', -encoding=>'utf-8', -title=>'管理者用ログイン画面');

print <<"HTML_VIEW";
<h3>管理者用ログイン画面</h3>
<br/>
<form name="loginForm" method="post" action="./authentication.cgi">
<table>
    <tr> 
        <th>ログインID:</th>
        <td>
            <input type="text" name="userId" value="" style="ime-mode: disabled;" />
        </td>
    </tr>
    <tr> 
        <th>パスワード:</th>
        <td>
            <input type="password" name="passwd" />
        </td>
    </tr>
</table>
<br/>
<input type="submit" value="ログイン" />
</form>
HTML_VIEW

if ($error eq "login") {
    print '<font color="red"><h4>ログインID またはパスワードが不正です.</h4></font>';
}
elsif ($error eq "session") {
    print '<font color="red"><h4>セッションが無効か、タイムアウトになりました.</h4></font>';
}
$cgi->delete_all();

print $cgi->end_html;
exit;
__END__

