#!/usr/bin/perl
use lib "../lib";

use strict;
use CGI;
use CIPRO::SessionUtil;

my $cgi = CGI->new;

print $cgi->header(-charset=>'utf-8'),
      $cgi->start_html(-lang=>'ja', -encoding=>'utf-8', -title=>'ログアウト画面');
print "<h3>ログアウト画面</h3>";

my $sessionUtil = CIPRO::SessionUtil->new();
$sessionUtil->clearSessionIdFromCGI($cgi);

print <<"HTML_VIEW";
<p>ログアウトしました。</p>
<p><a href="./index.cgi">ログイン画面へ</a></p>
HTML_VIEW

print $cgi->end_html;
exit;

__END__

