#!/usr/bin/perl
use strict;
use warnings;
use lib "../lib";

use CGI qw/:all/;
use CGI::Carp;
use CIPRO::Service::User;
use CIPRO::SessionUtil;

my $cgi = CGI->new();
my $sessionUtil = CIPRO::SessionUtil->new();

if(!$sessionUtil->isValidSessionId($cgi)) {
    $cgi->delete_all();
    print $cgi->redirect('./index.cgi?error=session');
}

print $cgi->header(-charset=>'utf-8'),
      $cgi->start_html(-lang=>'ja', -encoding=>'utf-8', -title=>'管理者用メニュー');

print <<"HTML_VIEW";
<h3>管理者用メニュー</h3>
<br/>
<table>
    <tr> 
        <td>
            <a href="./ciprolink.cgi">リンクのメンテナンス</a>
        </td>
    </tr>
    <tr> 
        <td>
            <a href="./cipronews.cgi">ニュースのメンテナンス</a>
        </td>
    </tr>
</table>
<br/>
<br/>
<table>
    <tr> 
        <td>
            <a href="./logout.cgi">ログアウト</a>
        </td>
    </tr>
</table>
<br/>

HTML_VIEW

print $cgi->end_html;
exit;
__END__

