<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Annual_report extends CI_Controller {

  function Annual_report ()
  {
    parent::__construct();

    $this->load->library('parser');
    $this->load->model("mannotname");
  }

  function total($rec)
  {
    $cnt=0;
    foreach ($rec as $r) {
      $cnt += $r->count;
    }
    return $cnt;
  }

  public function index ()
  {
    $rec = $this->mannotname->annual_report();
    $total = $this->total($rec);
    $param = array("rec"=>$rec, "total"=>$total);
    $this->load->view('vannual_report', $param);
  }
}

/* End of file annual_report.php */
/* Location: ./application/controllers/annual_report.php */
