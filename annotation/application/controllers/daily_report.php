<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daily_report extends CI_Controller {

  function Daily_report ()
  {
    parent::__construct();

    $this->load->library('parser');
    $this->load->model("mannotname");
  }

  public function view()
  {
    if ($this->input->post('daily')) {
      $y = $this->input->post('year');
      $m = $this->input->post('month');
      $this->summary($y, $m);
    }

    $period = $this->input->post('term');
    if ($period == 'last_month') {

    } else {
      $y = $this->input->post('year');
      $m = $this->input->post('month');
    }
    $a = $this->input->post('annotator');
    $this->index($y, $m, $a);
  }

  public function report($y="", $m="", $a="")
  {
    $param["rec"] = $this->mannotname->monthly_report($y, $m, $a);
    if (count($param["rec"])>0)
      $this->parser->parse('vmonthly', $param);
    else
      echo "No data for $y-$m";
  }

  public function summary($y="", $m="") {
    $param["rec"] = $this->mannotname->summary($y,$m);
    if (count($param["rec"])>0)
      $this->parser->parse('vsummary', $param);
    else
      echo "No data for $y-$m";
  }

  public function summary_today() {
    $param["rec"] = $this->mannotname->summary_today($y,$m);
    if (count($param["rec"])>0)
      $this->parser->parse('vsummary', $param);
    else
      echo "No data for $y-$m";
  }

  public function index ($y="", $m="", $a="")
  {
    $param['annotator_list'] = $this->mannotname->annotator_list();
    $this->parser->parse('vform', $param);
    $this->report($y, $m, $a);
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
