<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dbstat extends CI_Controller {

  public function index()
  {
    $this->load->model('Mstat');
    $param['stat'] = $this->Mstat->dbstat_all();
    $this->load->view('vstat',$param);
  }
}

/* End of file dbstat.php */
/* Location: ./application/controllers/dbstat.php */
