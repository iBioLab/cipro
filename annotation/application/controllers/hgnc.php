<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hgnc extends CI_Controller {

  public function index($query="")
  {
    $this->show();
    $this->load->view('vhgnc_form');
  }

  public function show($query="")
  {
    $definition_file = 'hgnc.txt';
    if ($query === "") 
      $query = $this->input->get_post('query', TRUE);

    function tab_split($s) { return explode("\t", $s); }

    if ( $query != NULL and $query !== "" ) {
      $exclude = '/'.implode('|',
      	         preg_grep("/^$/", explode("\n", `cat EXCLUDE.txt`),
		 	   PREG_GREP_INVERT)
		 ).'/i';
     // var_dump($exclude);
      $queries = preg_grep($exclude,
      		 	   preg_split("/[ :;,.-]+/", $query),
			   PREG_GREP_INVERT);
      $q = implode('|', preg_grep("/.{5}/", $queries));
      $rows=explode("\n", `/bin/egrep -i '$q' $definition_file`);
      $param["list"]=array_map("tab_split", $rows);
      $param["head"]=explode("\t", `/bin/head -1 $definition_file`);
      $this->load->view('vhgnc_table', $param);
      echo "q=", $q;
    }
  }
}

/* End of file Hgnc.php */
/* Location: ./application/controllers/Hgnc.php */
