<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hgnc extends CI_Controller {

  public function index($query="")
  {
    $this->show();
    $this->load->view('vhgnc_form');
  }


  public function show($query="")
  {
    $definition_file = 'hgnc.txt';
    if ($query === "") 
      $query = $this->input->get_post('query', TRUE);
    function tab_split($s) { return explode("\t", $s); }
    if ( $query != NULL and $query !== "" ) {
      $param["head"]=explode("\t", `/bin/head -1 $definition_file`);
      $rows=explode("\n", `/bin/grep -i '$query' $definition_file`);
      $param["list"]=array_map("tab_split",$rows);
      $this->load->view('vhgnc_table', $param);
    }
  }
}

/* End of file Hgnc.php */
/* Location: ./application/controllers/Hgnc.php */
