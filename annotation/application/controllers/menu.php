<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {

  public function index()
  {
    $param["list"] = array('dbstat',
    			'daily_report',
    			'monthly_report',
			'annual_report',
			'ruptime');
    $this->load->view('vmenu', $param);
  }
}

/* End of file list.php */
/* Location: ./application/controllers/list.php */
