<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruptime extends CI_Controller {

  public function index()
  {
    header("Refresh:60");
    echo standard_date("DATE_ATOM");
    echo '<pre>', `ruptime`, '</pre>';
    echo safe_mailto('ciproadmin@ibio.jp');
  }

  public function test()
  {
    echo 'test';
  }

  public function ajax()
  {
    $this->load->library('javascript');
    $this->load->jquery->event('', $this->test());
    $this->load->view()
  }
}

/* End of file list.php */
/* Location: ./application/controllers/list.php */
