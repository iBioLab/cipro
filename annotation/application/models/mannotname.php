<?php

class Mannotname extends CI_Model {
  function Mannotname ()
  {
    parent::__construct();
    $this->load->database();
    //$this->output->enable_profile();
  }

  function annotator_list ()
  {
    $this->db->from('annotname a')
      ->select('uname AS annotator')
      ->distinct()
      ->order_by('annotator', 'ASC');
    return $this->db->get()->result();
  }

  function get_year_month($y="", $m="")
  {
    if ($y == "" and $m == "") {
      $y = date('Y'); // this year
      $m = date('n')-1; // last month
      if ($m==0) {
	$y -= 1;
	$m = 12;
      }
    } else if ($y == "") {
      $y = date('Y'); // this year
    } else if ($m == "") {
      $m = date('n'); // this month
    }
    return array("year"=> $y, "month" => $m);
  }

  // params: year, month, annotator
  function monthly_report ($yr="", $mo="", $a="")
  {
    $ym = $this->get_year_month($yr, $mo);
    $y = $ym['year'];
    $m = $ym['month'];

    if ($a != "") {
      $this->db->where('a.uname', $a);
    }
    $this->db->from("annotname a")
      ->select("a.id")
      ->select("SUBSTRING(a.createdate::TEXT FROM 1 FOR 10) AS date")
      ->select("b.cipro AS seqid")
      ->select("a.sid AS hash")
      ->select("a.uname AS annotator")
      ->select("a.cname AS annotation")
      ->select("TEXT(a.evidence) AS evidence")
      ->select("DATE('$y-$m-1') as start")
      ->select("SUBSTRING((DATE('$y-$m-1')+INTERVAL'1month'-INTERVAL'1day')"
	       ."::TEXT from 1 for 10) as end")
      ->join('basic_info b', 'b.id=a.basic_info_id')
              ->where("a.createdate BETWEEN '$y-$m-1'"
		    ." AND DATE('$y-$m-1')+INTERVAL '1month'-INTERVAL '1day'")
      //      ->where("date_trunc('month', a.createdate)=date('$y-$m-1');" )
      ->order_by("annotator,a.createdate", "ASC");
 
    return $this->db->get()->result();
  }

  function annual_report() {
     $this->db->from("annotname")
       ->select("to_char(createdate, 'YYYY') AS year", FALSE)
       ->select("to_char(createdate, 'MM') AS month", FALSE)
       ->select("uname")
       ->select("count(*)")
       ->group_by('year,month,uname')
       ->order_by('year DESC, month DESC, uname');
    return $this->db->get()->result();
  }

  function summary($y="", $m="")
  {
    $ym = $this->get_year_month($y, $m);
    $y = $ym['year'];
    $m = $ym['month'];
  }

  function today()
  {
    $this->db->from("annotname a")
      ->select("a.id")
      ->select("SUBSTRING(a.createdate::TEXT FROM 1 FOR 10) AS date")
      ->select("b.cipro AS seqid")
      ->select("a.sid AS hash")
      ->select("a.uname AS annotator")
      ->select("a.cname AS annotation")
      ->select("TEXT(a.evidence) AS evidence")
      ->select("date('today') as start")
      ->select("date('today') as end")
      ->join('basic_info b', 'b.id=a.basic_info_id')
      ->where("a.createdate >", 'today')
      ->order_by("annotator,a.createdate", "ASC");
    return $this->db->get()->result();
  }

}
