<?php

class Mstat extends CI_Model {
  function Mstat ()
  {
    parent::__construct();
    $this->load->database();
    //$this->output->enable_profile();
  }

  function dbstat_common()
  {
    $this->db->from('pg_class')
	     ->select('relname')
    	     ->select('reltuples as rows')
	     ->select('(relpages*8) as kbytes');
    return $this->db->get()->result();
  }

  public function dbstat($table)
  {
    $this->db->where('relname', $table);
    return $this->dbstat_common();
  }

  public function dbstat_all()
  {
    $tables = $this->db->list_tables();
    $this->db->where_in('relname', $tables)
             ->order_by('kbytes DESC, rows DESC');
    return $this->dbstat_common();
  }
}
