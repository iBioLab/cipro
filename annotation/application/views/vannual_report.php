<html>
<head>
<title>
  Annotation annual report
</title>
<link href="<?=base_url()?>css/style.css" type=text/css rel=stylesheet />
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js></script>
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js></script>
</head>

<?php
$url0="http://cipro.ibio.jp/noannotation/#";
$url1="http://cipro.ibio.jp/current/#";
?>

<body>
<img src=http://cipro.ibio.jp/current/images/cipro_logo.png />
<div align=center>
<h1>CIPRO annotation report</h1>
<?=validation_errors()?>
Total: <?=$total?>
</div>
<div align=right><?=date('Y-m-d')?></div>
<div id=table_body align=center>
<table>
<caption> </caption>
<tr>
 <th>Year</th>
 <th>Month</th>
 <th>Annotator</th>
 <th>#of Annotation</th>
</tr>
<?foreach ($rec as $r):?>
 <tr>
  <td><?=$r->year?></td>
  <td><?=$r->month?></td>
  <td><?=$r->uname?></td>
  <td style="text-align:right"><?=$r->count?></td>
 </tr>
<?endforeach?>
</table>
</div>
<p/>
</body>
</html?
