<html>
<head>
<title>
 Simple HGNC query
</title>
<link href="<?=base_url()?>css/style.css" type=text/css rel=stylesheet />
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js></script>
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js></script>
</head>

<body>
<div align=center>
<h1>HGNC simple query</h1>
<?=validation_errors()?>
</div>
<div>
<table>
<tr>
<?foreach ($head as $h):?>
  <th><?=$h?></th>
<?endforeach?>
</tr>
<?foreach ($list as $row):?>
<tr>
  <?foreach ($row as $col):?>
    <td><?=$col?></td>
  <?endforeach?>
  </tr>
<?endforeach?>
</tr>
</table>
</div>
<p/>
</body>
</html>
