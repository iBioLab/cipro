<html>
<head>
<title>
  Annotation monthly report (<?=$rec[0]->start?>-<?=$rec[0]->end?>)
</title>
<link href="<?=base_url()?>css/style.css" type=text/css rel=stylesheet />
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js></script>
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js></script>
</head>

<?php
$url0="http://cipro.ibio.jp/noannotation/#";
$url1="http://cipro.ibio.jp/current/#";
?>

<body>
<img src=http://cipro.ibio.jp/current/images/cipro_logo.png />
<div align=center>
<h1>CIPRO annotation report</h1>
<?=validation_errors()?>
</div>
<div align=right><?=date('Y-m-d')?></div>
<div id=table_body align=center>
<table>
<caption>
<?=$rec[0]->start?> - <?=$rec[0]->end?> : <?=count($rec)?> records
</caption>
<tr>
 <th>ID</th>
 <th>Timestamp</th>
 <th>SeqID</th>
 <th>Annotator</th>
 <th>Annotation</th>
 <th>Evidence</th>
 <th colspan=2>Screenshot</th>
</tr>
<?foreach ($rec as $r):?>
 <tr>
  <td><?=$r->id?></td>
  <td><?=$r->date?></td>
  <td><?=$r->seqid?></td>
  <td><?=$r->annotator?></td>
  <td><?=$r->annotation?></td>
  <td><?=htmlspecialchars($r->evidence)?></td>
  <td><a href=<?=$url0?><?=$r->seqid?> target=w0>scrn0</a></td>
  <td><a href=<?=$url1?><?=$r->seqid?> target=w1>scrn1</a></td>
 </tr>
<?endforeach?>
</table>
</div>
<p/>
</body>
</html?
