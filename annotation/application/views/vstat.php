<html>
<head>
<title>
  CIPRO table Statistics
</title>
<link href="<?=base_url()?>css/style.css" type=text/css rel=stylesheet />
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js></script>
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js></script>
</head>

<body>

<div align=center>
<h1>CIPRO stats</h1>
<?=validation_errors()?>
</div>
<table>
<caption>
</caption>
<tr>
 <th>Table</th>
 <th>Rows</th>
 <th>kBytes</th>
</tr>
<?foreach ($stat as $r):?>
 <tr>
  <td><?=$r->relname?></td>
  <td class='number'><?=number_format($r->rows)?></td>
  <td class='number'><?=number_format($r->kbytes)?></td>
 </tr>
<?endforeach?>
</table>
</div>
<p/>
</body>
</html?
