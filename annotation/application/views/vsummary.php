<html>
<head>
<title>
  Annotation monthly report (<?=$rec[0]->start?>-<?=$rec[0]->end?>)
</title>
<link href="<?=base_url()?>css/style.css" type=text/css rel=stylesheet />
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js></script>
<script type=text/javascript src=https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js></script>
</head>

<body>
<div align=center>
<h1>CIPRO annotation daily summary</h1>
<?=validation_errors()?>
</div>
<div align=right><?=date('Y-m-d')?></div>
<div id=table_body align=center>
<table>
<caption>
<?=$rec[0]->start?> - <?=$rec[0]->end?> : <?=count($rec)?> records
</caption>
<tr>
 <th>Date</th>
 <th>Annotator</th>
 <th>Count</th>
</tr>
<?foreach ($rec as $r):?>
 <tr>
  <td><?=$r->year_month?></td>
  <td><?=$r->annotator?></td>
  <td><?=$r->count?></td>
 </tr>
<?endforeach?>
</table>
</div>
<p/>
</body>
</html?
