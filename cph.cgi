#!/usr/bin/perl
# cph.cgi

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::Res;

my $id = param('id') || param('keywords');
my ($cipro) = split "_",$id;
my $serv = CIPRO::Service::Res->new();
my $res  = $serv->searchByCiproId($cipro,'Structural Model','modeller');

$DOJOSCRIPT=<<END;
<script src="http://ajax.googleapis.com/ajax/libs/dojo/1.4.3/dojo/dojo.xd.js" djConfig="parseOnLoad: true, locale:'en-us'" type="text/javascript"></script>
END

$JSCRIPT=<<END;
function resizeIframe() {    
    var iframe = parent.document.getElementById("jmoli$id");
    var height = document.documentElement.scrollHeight || document.getElementById("jmoli$id").offsetHeight;
    iframe.style.height = height + 10 + "px";
}
END
#    var height = document.documentElement.scrollHeight || document.getElementById("jmoli$id").offsetHeight;
#var height = document.documentElement.scrollHeight || window.innerHeight;
#var height = document.getElementById("jmoli$id").offsetHeight;
#var height = document.documentElement.scrollHeight;
#alert(document.write(document.getElementById("jmoli$id").clientHeight);
print header, start_html(-title=>uc $NAME,
                         -script=>[{-type=>"text/javascript",
                                    -src=>"Jmol/appletweb/Jmol.js"},
				   $JSCRIPT
			 ],
			 #head=>$DOJOSCRIPT
			 -onload=>"resizeIframe();"
    );
if(length($res) > 0){
    print script({type=>"text/javascript"},'jmolInitialize("Jmol/jars","JmolApplet.jar");');
    print script({type=>"text/javascript"},
                 'jmolApplet(300, "load pdbdata.cgi?id='.$cipro.';'.
                 ' background grey; select all;spacefill off;'.
                 'wireframe off;cartoon;color structure;")');
}else{
    print h3("No hits reported");
}
print end_html;

