#!/usr/bin/perl
# f_randimg.cgi  - display random image.

use CGI qw/:all/;
use lib "./lib";
use CIPRO::Service::Res;

my $prg = "Structural Model";
my $sub = "modeller";
my $serv = CIPRO::Service::Res->new();
my $id = $serv->searchRandomByProgramAndSubTypeId($prg,$sub);

sub contents() {
  print table(Tr([td([
        a({href=>"#$id"},
          img({src=>"viewicon.cgi?id=$id&c=$prg", width=>240, height=>240, border=>0})
            .br.center("$id")
      )
    ]) ]) );
}
print "Content-type:text/html\n\n";
contents();

