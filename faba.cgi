#!/bin/perl

use strict;
use CGI::Carp;
use CGI qw/:all/;

#require "imgSz.pl";

my @period=("Zygote","Cleavage","Gastrula","Neurula","Tailbud","Swimming larva","Adhension",
			"Tail absorption","Body axis rotation","Juvenile");
my @abbreviation=("zygote","cleavage","gastrula","neurula","tailbud","swimming","adhension",
			"absorption","rotation","juvenile");
my @hours=("0-0.9","0.9-4.5","4.5-6.3","6.3-8.5","8.5-17.5","17.5-23","24-26","27-29","30-57","60-168");

my %stage=(
	"Zygote"=>"1",
	"Cleavage"=>"2,3,4,5-a,5-b,6-a,6-b,7,8,9",
	"Gastrula"=>"10,11,12,13",
	"Neurula"=>"14,15,16",
	"Tailbud"=>"17,18,19,20,21,22,23,24,25",
	"Swimming larva"=>"26,27,28,29,30-a,30-b",
	"Adhension"=>"31,32-a,32-b",
	"Tail absorption"=>"33,34,35",
	"Body axis rotation"=>"36-a,36-b,37-a,37-b,38,39-a,39-b,39-c,39-d,40",
	"Juvenile"=>"41,42-a,42-b,42-c,43,44,45,46,47"
);

my %img=(
	1=>"0.4,0.8",
	2=>0.9,
	3=>"1.45,1.7",
	4=>1.9,
	"5-a"=>2.35,
	"5-b"=>2.65,
	"6-a"=>3,
	"6-b"=>3.2,
	7=>3.35,
	8=>4,
	9=>4.2,
	10=>"4.5,4.6",
	11=>4.9,
	12=>5.65,
	13=>5.9,
	14=>6.35,
	15=>6.8,
	16=>7.4,
	17=>8.45,
	18=>8.8,
	19=>9.3,
	20=>9.5,
	21=>10,
	22=>10.9,
	23=>"11.9,12",
	24=>13.5,
	25=>"15.9,16.5,17",
	26=>17.5,
	27=>19,
	28=>20,
	29=>21,
	"30-a"=>22,
	"30-b"=>23,
	31=>24,
	"32-a"=>25,
	"32-b"=>26,
	33=>27,
	34=>28,
	35=>29,
	"36-a"=>30,
	"36-b"=>33,
	"37-a"=>36,
	"37-b"=>39,
	38=>42,
	"39-a"=>45,
	"39-b"=>48,
	"39-c"=>51,
	"39-d"=>54,
	40=>57,
	41=>60,
	"42-a"=>63,
	"42-b"=>66,
	"42-c"=>69,
	43=>72,
	44=>96,
	45=>120,
	46=>144,
	47=>168
);

my $CGIURL="http://chordate.bpni.bio.keio.ac.jp/faba/";

sub main_frame{
	my $frame=h3("A standard web-based resource called 
		Four-dimensional Ascidian Body Atlas (FABA).");
	$frame.=b("Original FABA").br.a({href=>"http://chordate.bpni.bio.keio.ac.jp/faba/1.4/top.html"},
			img({src=>"http://chordate.bpni.bio.keio.ac.jp/faba2/2.2/img/faba.png",alt=>"FABA",height=>"41px",style=>"border-color:#000000; border-width:1px;"}))."&nbsp;".
		a({href=>"http://chordate.bpni.bio.keio.ac.jp/faba2/2.2/top.html"},
			
img({src=>$CGIURL."1.4/img/faba2.png",alt=>"FABA2",style=>"border-color:#000000; border-width:1px;"})).br.br;
	$frame.="When you would like to see the detail, ".a({href=>"fabatab.cgi?fabatab=detail"},"click here").".".br.br;

	for my $i (0..$#period){
#		$frame.=a({href=>"lwplist.cgi?stage=$abbreviation[$i]"},b($period[$i]." period (".$hours[$i]."hr)"));
		$frame.=b($period[$i]." period (".$hours[$i]."hr)").table({border=>1},createTable($period[$i],$i)).br;			
	}
	
	return $frame;
}

sub createTable($$){
	my $row;
	my @stages=split(/\,/,$stage{$_[0]});
	my $URL1=$CGIURL."1.4/";
	my $URL2="http://chordate.bpni.bio.keio.ac.jp/faba2/2.2/";
	my $stageColumns=th({align=>"center"},"Stage");
	my $hrColumns=th({align=>"center"},"Time after fertilization");
	my $imgColumns=th({align=>"center"},"Image");

	for my $i (0..$#stages){
		my ($imgURL,$cgiURL);
		my @hpr=split(/\,/,$img{$stages[$i]});
		my $division=$#hpr+1;
	
		# add HPR columns
		for my $j (@hpr){
			$hrColumns.=td({align=>"center"},$j);
		}
		
		
		# add Image columns
		for my $j (@hpr){
			# divide URLs
			if($_[1]<=4){
				$imgURL=$URL1."img/dev_3D/dev3D-".$j.".jpg";
#				$cgiURL=$URL1."faba_contents.cgi?url=3D.html?".$j;
				$cgiURL=$URL1."3D.html?".$j;
			}else{
				$imgURL=$URL2."img/dev_3D/".$j."hpf.jpg";
#				$cgiURL=$URL2."faba_contents.cgi?url=3D.html?".$j;
				$cgiURL=$URL2."3D.html?".$j;
			}
=pod			
			my ($width,$height)=&imgSz::getSize($imgURL);
			my $scale;
			if($width>$height){
				$scale=50/$width;
			}else{
				$scale=50/$height;
			}

			$imgColumns.=td({align=>"center"},a({href=>$cgiURL,target=>"_blank"},img({src=>$imgURL,alt=>$j."hpr",
				height=>$scale*$height,width=>$scale*$width})));
=cut			
			$imgColumns.=td({align=>"center", style=>"background-color: black;"},a({href=>$cgiURL,target=>"_blank"},img({src=>$imgURL,alt=>$j."hpr",
				width=>50, style=>"border-style: none;"})));
		}		

		# add STAGE columns
		if($division==1){
			$stageColumns.=td({align=>"center"},$stages[$i]);				
		}else{
			$stageColumns.=td({colspan=>$division,align=>"center"},$stages[$i]);
		}		
	}
	$row.=Tr($stageColumns).Tr($hrColumns).Tr($imgColumns);		
	
	return $row;
}

main:{
	print header,
			start_html({title=>"CIPRO FABA"}),
			main_frame(),
			end_html;	
}
