#!/bin/perl

use strict;
use CGI::Carp;
use CGI qw/:all/;

my $expression=param('expression');
my $faba=param('faba');
my @abbreviation=("Zygote","Cleavage","Gastrula","Neurula","Tailbud","Swimming","Adhension",
			"Absorption","Rotation","Juvenile");
my @period=("Zygote","Cleavage","Gastrula","Neurula","Tailbud","Swimming larva","Adhension",
			"Tail absorption","Body axis rotation","Juvenile");
my @hours=("0-0.9","0.9-4.5","4.5-6.3","6.3-8.5","8.5-17.5","17.5-23","24-26","27-29","30-57","60-168");
my %periods=(
	"eg"=>0,
	"cl"=>1,
	"gn"=>"2,3",
	"tb"=>4,
	"lv"=>5,
	"em"=>"6,7,8",
	"jv"=>9,
	"jm"=>9
);
my %stage=(
	"Zygote"=>"1",
	"Cleavage"=>"2,3,4,5-a,5-b,6-a,6-b,7,8,9",
	"Gastrula"=>"10,11,12,13",
	"Neurula"=>"14,15,16",
	"Tailbud"=>"17,18,19,20,21,22,23,24,25",
	"Swimming larva"=>"26,27,28,29,30-a,30-b",
	"Adhension"=>"31,32-a,32-b",
	"Tail absorption"=>"33,34,35",
	"Body axis rotation"=>"36-a,36-b,37-a,37-b,38,39-a,39-b,39-c,39-d,40",
	"Juvenile"=>"41,42-a,42-b,42-c,43,44,45,46,47"
);
my %img=(
	1=>"0.4,0.8",
	2=>0.9,
	3=>"1.45,1.7",
	4=>1.9,
	"5-a"=>2.35,
	"5-b"=>2.65,
	"6-a"=>3,
	"6-b"=>3.2,
	7=>3.35,
	8=>4,
	9=>4.2,
	10=>"4.5,4.6",
	11=>4.9,
	12=>5.65,
	13=>5.9,
	14=>6.35,
	15=>6.8,
	16=>7.4,
	17=>8.45,
	18=>8.8,
	19=>9.3,
	20=>9.5,
	21=>10,
	22=>10.9,
	23=>"11.9,12",
	24=>13.5,
	25=>"15.9,16.5,17",
	26=>17.5,
	27=>19,
	28=>20,
	29=>21,
	"30-a"=>22,
	"30-b"=>23,
	31=>24,
	"32-a"=>25,
	"32-b"=>26,
	33=>27,
	34=>28,
	35=>29,
	"36-a"=>30,
	"36-b"=>33,
	"37-a"=>36,
	"37-b"=>39,
	38=>42,
	"39-a"=>45,
	"39-b"=>48,
	"39-c"=>51,
	"39-d"=>54,
	40=>57,
	41=>60,
	"42-a"=>63,
	"42-b"=>66,
	"42-c"=>69,
	43=>72,
	44=>96,
	45=>120,
	46=>144,
	47=>168
);

my %descriptionWidth=(
	0.4=>2,
	0.9=>1,
	1.45=>2,
	1.9=>1,
	2.35=>1,
	2.65=>1,
	3=>1,
	3.2=>1,
	3.35=>1,
	4=>1,
	4.2=>1,
	4.5=>2,
	4.9=>1,
	5.65=>1,
	5.9=>1,
	6.35=>1,
	6.8=>1,
	7.4=>1,
	8.45=>1,
	8.8=>1,
	9.3=>1,
	9.5=>1,
	10=>1,
	10.9=>1,
	11.9=>2,
	13.5=>1,
	15.9=>3,
	17.5=>1,
	19=>1,
	20=>1,
	21=>1,
	22=>2,
	24=>1,
	25=>2,
	27=>1,
	28=>1,
	29=>1,
	30=>2,
	36=>2,
	42=>1,
	45=>4,
	57=>1,
	60=>1,
	63=>3,
	72=>1,
	96=>1,
	120=>1,
	144=>1,
	168=>1
);

my %descriptions=(
	0.4=>"Zygote, fertilized egg",
	0.9=>"Two cell-stage embryo",
	1.45=>"Four cell-stage embryo",
	1.9=>"Eight cell-stage embryo",
	2.35=>"Early sixteen-cell stage embryo",
	2.65=>"Late sixteen-cell stage embryo",
	3=>"Early thirty two-cell stage embryo",
	3.2=>"Late thirty two-cell stage embryo",
	3.35=>"Fourty four-cell stage embryo. The vegetal side of the embryo is very round.",
	4=>"Sixty four-cell stage embryo. Embryo has a square shape seen form the top, with bulging B7.4 cells.",
	4.2=>"Seventy six cell stage embryo. The vegetal side of the embryo is very flat",
	4.5=>"Gastrulation starts with the apical constriction of A7.1 blastomeres.",
	4.9=>"The notochord has invaginated. The vegetal side of the embryo has a horseshoe shape.",
	5.65=>"Six-row neural plate stage. The blastopore is still central and open.",
	5.9=>"The blastopore is in posterior position and nearly closed. The embryo elongates anteriorly. 
	The neural plate has more than 6 rows and the A-line neural rows (I and II) start to curve 
	(neurulation begin). The large b6.5 progeny are coming together at the midline.",
	6.35=>"A-line neural plate forms a gutter lined by b6.5 descendants. 
	The embryo has a diamond shape. The gutter is not closed.",
	6.8=>"The neural tube has formed on most of its length. The embryo has an oval shape. 
	The a-line neural plate also forms a gutter.",
	7.4=>"The neural tube starts to form in the posterior territories. The embryo elongates. ",
	8.45=>"First indication of a separation between tail and trunk territories. 
	The tail is not bent and has the same length as the trunk. Any notochord cells not finished intercalation.",
	8.8=>"The tail is clearly separated from the trunk. Tail and trunk have same length. 
	Neuropore still open, a-line neurulation. ",
	9.3=>"The tail bends about 40&#176; and is slightly longer than the trunk. 
	A few anterior most notochord cells begin to intercalate and linear.",
	9.5=>"Neuropore closed, tail bent by 60&#176;, neurulation complete.",
	10=>"Tail 1 1/2 times longer than trunk and curve ventrally (90&#176;). 
	Intercalation of notochord cells just finished. ",
	10.9=>"The body adopts a half circle shape. Tail twice as long as trunk.",
	11.9=>"Initiation of the pigmentation of the otolith. 
	Tail strongly curved with tip close to the anterior end of the trunk.",
	13.5=>"Notochord vacuolation begins, palps start to be visible at the front end of the embryo. 
	Tail straightens.",
	15.9=>"Ocellus melanization. All notochord cells have vacuoles. Tail bent dorsally.",
	17.5=>"Immatured papilla, head shape is spherical.",
	19=>"Spindle-like head shape.",
	20=>"-",
	21=>"Expansion of basal part of papilla and elongate. Square shaped head. 
	Spherical shape of test cells. Primordial endstyle observed.",
	22=>"Tail muscle well diversified. Longest length of larva.",
	24=>"Papilla change the shape and curved. Primordial gut observed.",
	25=>"Reversal of papilla.",
	27=>"Start of tail absorption.",
	28=>"50% of tail absorbed to trunk part. Thickening of tail.",
	29=>"End of tail absorption. Degeneration of papilla.",
	30=>"Disruption of head tunic. Papilla utilized, observation of both oral and atrial siphon. 
	Start of body axis rotation (0&#176;).",
	36=>"Body axis rotaion at 30&#176; (35.2hpf).",
	42=>"Body axis rotaion at 60&#176; (44.5hpf), one pair of gill-slit open.",
	45=>"Two pairs of gill-slit open, amplae start to diverge.",
	57=>"Body axis rotaion at 90&#176; (55hpf).",
	60=>"Two pairs of gill-slit, oral siphon open and start to eat food.",
	63=>"Longitudinal muscle observed.",
	72=>"Endstyle and amplae straight line.",
	96=>"Disappearance of absorbed larval tail.",
	120=>"Appearance of stomach, gut and neural grand.",
	144=>"-",
	168=>"-"
);

sub createTable(){
	my $tables;
	my @correspondingPeriods;
	if(defined $expression){
		@correspondingPeriods=split(/\,/,$periods{$expression});
	}else{
		for my $i (0..$#abbreviation){
			if($abbreviation[$i] eq $faba){
				push(@correspondingPeriods,$i);				
			}
		}
	}
	
	for my $i (@correspondingPeriods){
		$tables.=b($period[$i]." period "."($hours[$i]hr)");
		my $content=Tr(th("Image"),th("Link"),th("Stage"),th("Time after fertilization"),th("Description"));
		my @correspondingStages=split(/\,/,$stage{$period[$i]});
		for my $s (@correspondingStages){
			my $URL="http://chordate.bpni.bio.keio.ac.jp/";
			if($i<=4){
				$URL.="faba/1.4/";
			}else{
				$URL.="faba2/2.2/";
			}
			my @images=split(/\,/,$img{$s});
			for my $j (0..$#images){
				# add images
				my $column;
				if($i<=4){
					$column=td({bgcolor=>"#000000",,align=>"center",width=>100},
						a{href=>$URL."dev_3D/".$images[$j]."hpf.html"},img({src=>$URL."img/dev_3D/dev3D-".$images[$j].".jpg",width=>80}));					
				}else{
					$column=td({bgcolor=>"#000000",,align=>"center"},
						a{href=>$URL."dev_3D/".$images[$j]."hpf.html"},img({src=>$URL."img/dev_3D/t-".$images[$j]."hpf.jpg",width=>80}));										
				}
				# add links
				if($i<=4){
					$column.=td({align=>"center",width=>80},
								a({href=>$URL."3D.html?".$images[$j]},img({src=>$URL."img/3D_btn.png",alt=>"3D",border=>0})),br,
								a({href=>$URL."slice.html?".$images[$j]},img({src=>$URL."img/slice_btn.png",alt=>"slice",border=>0})),br,
								a({href=>$URL."body_atlas.html?".$images[$j]},img({src=>$URL."img/cellLineages_btn.png",alt=>"cell lineages",border=>0})),br,
								a({href=>$URL."timelapse.html?".$images[$j]},img({src=>$URL."img/timelapse_btn.png",alt=>"timelapse",border=>0})));
				}else{
					$column.=td({align=>"center",width=>80},
								a({href=>$URL."3D.html?".$images[$j]},img({src=>$URL."img/3D_btn.png",alt=>"3D",border=>0})),br,
								a({href=>$URL."slice.html?".$images[$j]},img({src=>$URL."img/slice_btn.png",alt=>"slice",border=>0})),br,
								a({href=>$URL."developmental_time.html?".$images[$j]},img({src=>$URL."img/developmentalTime_btn.png",alt=>"developmental time",border=>0})),br,
								a({href=>$URL."timelapse.html?".$images[$j]},img({src=>$URL."img/timelapse_btn.png",alt=>"timelapse",border=>0})));					
				}
				# add stages
				if($#images>0 and $j==0){
					$column.=td({rowspan=>$#images+1,align=>"center",style=>"font-size: 15pt;",width=>"60"},$s);
				}elsif($#images==0){
					$column.=td({align=>"center",style=>"font-size: 15pt;",width=>"60"},$s);
				}
				# add times after felitization
				$column.=td({align=>"center",style=>"font-size: 12pt;",width=>"80"},$images[$j]."hr");
				# add descriptions
				if(defined $descriptions{$images[$j]}){
					$column.=td({rowspan=>$descriptionWidth{$images[$j]},align=>"center",style=>"font-size: 12pt;"},
						$descriptions{$images[$j]});
				}
				$content.=Tr($column);
			}
		}
		$tables.=table({width=>700,border=>1},$content).br;
	}
	return $tables;
}

sub main_frame(){
	my $frame=h4(i('Ciona intestinalis')," early embryoic development");
	if(param('fabatab') eq "detail"){
		for my $key (@abbreviation){
			$faba=$key;
			$frame.=createTable();
		}
	}else{
		$frame.=createTable();
	}
	return $frame;
}

main:{
	print header,
		start_html({title=>"CIPRO FABA Detail",style=>"css/full.css"}),
		main_frame(),
		end_html;	
}
