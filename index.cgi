#!/usr/bin/perl
# index.cgi

use CGI qw/:all/;
use lib "./lib";
use CIPRO::Config;
use CIPRO::Service::Link;
#use CGI::Session qw/-ip_match/;
use CGI::Session;

my $sid=cookie('at-ciprojp')||undef;
$SESSION_ID;
unless($sid){
    my $session=CGI::Session->new(undef,$sid,{Directory=>'/tmp'});
    $session->expire('+1y');
    $SESSION_ID = $session->id;
}


# 3DPL Search
$startTime = param('start_time');
$endTime   = param('end_time');

# Given ID
$ID = param('id') || param('locus');

($homeTabSelectFlag, $searchTabSelectFlag) = ("true","false");
if($startTime && $endTime){
    ($homeTabSelectFlag, $searchTabSelectFlag) = ("false", "true");
} elsif($ID){
    ($homeTabSelectFlag, $searchTabSelectFlag) = ("false", "true");
}


$DOJO_VER='1.6.1';

$DOJOSCRIPT=<<END;
<script src="http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojo/dojo.xd.js" djConfig="parseOnLoad: true, locale:'en-us',isDebug: true" type="text/javascript"></script>
END

#dojo.addOnLoad(indexinit);
$JSCRIPT=<<END;
dojo.addOnLoad(function(){indexinit("$SESSION_ID");});
END

$PMF = join '\n',(528.13, 565.33, 775.36, 846.31,
			874.46, 933.48, 972.5, 1006.51,
			1038.51, 1130.52, 1135.58, 1450.69,
			1606.71, 1797.91, 1801.81, 1970.77,
			2127.14, 2022.98, 2460.38, 3114.35);

my @prog_radio   = radio_group(-dojoType=>"dijit.form.RadioButton", -name => "prog", -values => ["blastp", "blastx"], -class=>"blastprog",
                                -default => "blastp", -labels => { "blastp" => "Protein", "blastx" => "DNA" } );

my @filter_radio = radio_group(-dojoType=>"dijit.form.RadioButton", -name => "filter", -values => ["F", "T"], -class=>"blastfilter",
                                -default => "F", -labels => { "F" => "Off", "T" => "On" } );

my @mask_lc_radio = radio_group(-dojoType=>"dijit.form.RadioButton", -name => "mask_lc", -values => ["F", "T"], -class=>"blastmask_lc",
                                -default => "F", -labels => { "F" => "Off", "T" => "On" } );

my @cmod_radio = radio_group(-dojoType=>"dijit.form.RadioButton", -name => "sh", -tooltiptext=>"Select one fixed modification.", -class=>"sh",#-id=>"permscmod", 
			      -values => ["None","c","m","e"],
                              -default => "None");
my @trypsin_radio = radio_group(-dojoType=>"dijit.form.RadioButton", -name => "enzyme", -class=>"enzyme",#-id=>"permstrypsin",
				-tooltiptext=>"Select the kind of trypsin if the contamination is suspected.", 
				-values => ["None","Porcine(Major)","Porcine(All)","Bovine"],
				-default => "None");

my %est = ( "eg"=>"eggs", "cl"=>"cleaving embryos",
	    "gn"=>"gastrulae neurulae", "tb"=>"tailbud embryos",
	    "lv"=>"larvae", "em"=>"embryo mix",
	    "jv"=>"juvenile 1", "jm"=>"juvenile 2",
	    "ad"=>"young adults", "gd"=>"gonad",
	    "ts"=>"testis", "es"=>"endostyle",
	    "nc"=>"neural complex", "ht"=>"heart",
	    "bd"=>"blood cell", "dg"=>"digestive gland",
	    "ma"=>"mature adult" );
#my @mod = ("N-terminal Acetylation (n)","Oxidation of M (o)",
#	   "Phosphorylation of S, T and Y (p)","Acrylamidation of C (a)" ); 
my @mod = ("N-terminal Acetylation","Oxidation of M",
	   "Phosphorylation of S, T and Y","Acrylamidation of C" ); 
my %mod =  ("N-terminal Acetylation" => "n","Oxidation of M" => "o",
	   "Phosphorylation of S, T and Y" => "p","Acrylamidation of C" => "a" );

sub get_link{
    $serv = CIPRO::Service::Link->new;
    $linklist = $serv->search();
}

my %cionalinkicon = ('Ciona intestinalis Genome'=>"",'CiAID'=>"",'PerMS (Tsukuba Univ.)'=>"",
		     'Tunicate Portal'=>"tunicateIcon",'GHOST'=>"",'FABA'=>"",
		     'ANISEED'=>"",'JGI'=>"");

sub set_ciona_link{
    if(defined($linklist)){
        while ( my $row = $linklist->next ) {
            if($row->category =~ /Ciona Links/){
                push @cionalinks,
                li({ -dojoType=>"dijit.MenuItem", -iconClass=>$cionalinkicon{$row->name}, -onclick=>"window.open('".$row->url."','_blank'); return false;"},
		    $row->name);
            }
        }
    }
}
sub print_ciona_link{
    Button({ -dojoType=>"dijit.form.DropDownButton"},
	span("Ciona Links"),
	ul({ -dojoType=>"dijit.Menu"},@cionalinks)),
}

sub set_related_link{
    if(defined($linklist)){
        $linklist->first;
        while ( my $row = $linklist->next ) {
            if($row->category =~ /Related Links/){
                push @relatedlinks,
                div({ -dojoType=>"dijit.MenuItem", -onclick=>"window.open('".$row->url."','_blank'); return false;"},
		    $row->name);
            }
        }
    }
}
sub print_related_link{
    div({ -dojoType=>"dijit.form.DropDownButton"},
	div("Related Links"),
	div({ -dojoType=>"dijit.Menu"},@relatedlinks)),
}

sub print_searchresult{
    div({ -id=>"searchresult", -dojotype=>"dijit.layout.ContentPane", -region=>"center", -splitter=>"true", -sizeShare=>"55",
	  -style=>"margin : 0px 0px 0px 0px ;"},
	table({-id=>"grid",-class=>"tundra",-jsId=>"taskGrid", -dojoType=>"dojox.grid.DataGrid",rowsPerPage=>"25",#rowCount=>"5",# 
	       #structure=>"resultStructure",
	       -style=>"width: 100%; height: 100%;"},
	      thead(Tr(th({field=>"ID",width=>"auto",formatter=>"ciproIDFormatter"},"ID"),
		       th({field=>"Length",width=>"auto"},"Length [AA]"),
		       th({field=>"MW(calc)",width=>"auto"},"MW(calc) [Da]"),
		       th({field=>"pI(calc)",width=>"auto"
			       ,formatter=>"fmtWidget"
			  },"pI(calc)")
		    ))))
}

=note
sub print_searchresult{
    ($startTime, $endTime, $ID) = @_;
    if($startTime && $endTime){
	div({ -id=>"searchresult", -dojotype=>"dijit.layout.ContentPane", -region=>"center", -splitter=>"true", -sizeShare=>"55",
	      -style=>"margin : 0px 0px 0px 0px ;", -href=>"searchresult.cgi?start_time=$startTime&end_time=$endTime"},"")
    }elsif($ID){
	div({ -id=>"searchresult", -dojotype=>"dijit.layout.ContentPane", -region=>"center", -splitter=>"true", -sizeShare=>"55",
	      -style=>"margin : 0px 0px 0px 0px ;", -href=>"searchresult.cgi?id=$ID"},"")   	
    }else{
        div({ -id=>"searchresult", -dojotype=>"dijit.layout.ContentPane", -region=>"center", -splitter=>"true", -sizeShare=>"55",
	      -style=>"margin : 0px 0px 0px 0px ;"},"")
    }
}
=cut
get_link();
set_ciona_link();
set_related_link();

my %icon = ('HOME'=>"homeIcon", 'Overview'=>"overviewIcon", 'Help|FAQ'=>"helpIcon",
	    'Member'=>"memberIcon", '2D PAGE'=>"", 'FABA'=>"", 'SiteMap'=>"sitemapIcon");


sub Toolbar{
    div({-dojoType=>"dijit.Toolbar", -class=>"menuBar"},
	Button({-dojoType=>"dijit.form.DropDownButton", -iconClass=>"ciprodbIcon"} ,
	    span("CIPRO"),
	       ul({-dojoType=>"dijit.Menu"} ,
		map( {$_ =~ /,'(.*)'\)/;li({-dojoType=>"dijit.MenuItem", -onclick=>"$_ return false;", -iconClass=>$icon{$1}} ,$1)}
		     "addNewTab('home.html','HOME');",
		     "addNewTab('overview.html','Overview');",
		     "addNewTab('helpfaq.html','Help|FAQ');",
		     "addNewTab('member.html','Member');",
		     "addNewTabOuterPage('2d_page/cgi-bin/2d/2d.cgi','2D PAGE');",
		     "addNewTab('faba.cgi','FABA');",
		     "addNewTab('sitemap.html','SiteMap');"))),
	span({-dojoType=>"dijit.ToolbarSeparator"},""),
	div({ -dojoType=>"dijit.form.DropDownButton"} ,
	    div("Service"),
	    div({ -dojoType=>"dijit.Menu"},
		div({ -dojoType=>"dijit.MenuItem", -onclick=>"addNewTab('annotation.html','Annotation_Jumboree');return false;"},"Annotation Jumboree"),
		div({ -dojoType=>"dijit.MenuItem", -onclick=>"window.open('./download','_blank'); return false;"},"Download"),
		div({ -dojoType=>"dijit.MenuItem", -onclick=>"window.open('extra.html','_blank'); return false;"},"Extra"))),
	span({-dojoType=>"dijit.ToolbarSeparator"},""),
	print_ciona_link,
	span({ -dojoType=>"dijit.ToolbarSeparator"},""),
	print_related_link)
}


sub Top{
    div(table({ -width=>"99%"},
	      Tr(td({ -width=>"120px",-height=>"50px"},img({ -src=>"./images/cipro_logo.png"} )),
		 td({ -align=>"center"},
		    span({ -style=>"color: #292929; font-family: Myriad, Lucida Grande, Bitstream Vera Sans, Arial, Helvetica, sans-serif"},
			 "<b><i><font color='gray'><span><font color='orangered'>C</font></span>iona".
			 " <span><font color='orangered'>i</font></span>ntestinalis ".
			 "<span><font color='orangered'>Pro</font></span>tein Database</font></i></b>")),
		 td({ -align=>"right"},
                    #start_form( -method=>"post" , -onsubmit=>"searchFreeText();return false;"),
		    #"Full text Search: ", 
		    "ID: ", 
		    textfield(-dojoType=>"dijit.form.TextBox", -name=>"keywords", -id=>"freetextsearchtext", -onKeyPress=>"if (event.keyCode==13){document.location='#'+dojo.query('#freetextsearchtext').attr('value');}"),
		    #textfield(-dojoType=>"dijit.form.TextBox", -name=>"keywords", -class=>"freetextsearchtext"),
		    #Button({-dojoType=>"dijit.form.Button", -name=>" go ", -value=>" go ",-onclick=>"searchFreeText()"},"go"),
		    Button({-dojoType=>"dijit.form.Button", -name=>" go ", -value=>" go ",
			    #-onclick=>"var id = dijit.byId('freetextsearchtext').getValue(false);document.location = '#'+id;"},"go"),
			   -onclick=>"document.location='#'+dojo.query('#freetextsearchtext').attr('value');"},"go"),
		    #end_form
		 ))))
}

sub Button{
    my $Att = shift @_;
    my $att = join " ",map{(my $i = $_)=~ s/-//;"$i=\"${$Att}{$_}\""} keys %$Att;
    my @contents = @_;
    return "<button $att>@contents</button>";
}

sub GeneralSearch{
    #start_form(-dojoType=>"dijit.form.Form", -method=>"post" , -onsubmit=>"searchFullText();return false;"),
    start_form(-dojoType=>"dijit.form.Form", -method=>"post" , -onsubmit=>"changeResultGrid();return false;"),
    table({-style=>"border: 1px solid #9f9f9f;", -cellspacing=>"10"},
	  #thead(Tr(th("Description"),th(""))),
	  tbody(Tr(td("Full text search"),
		   td(textfield(-dojoType=>"dijit.form.TextBox", -name=>"keyword", -class=>"fulltextsearchtext", -id=>"fulltextsearchtext",
				#-value=>"Alzheimer",
				#onFocus=>"dijit.byId('fulltextsearchtext').attr('value', '');",
				#onBlur=>"dijit.byId('fulltextsearchtext').attr('value', 'Alzheimer');",
				-tooltiptext=>"Type in keyword (e.g. cellulase) in this box"))),
		Tr(td("Annotator name"),
		   td(textfield(-dojoType=>"dijit.form.TextBox", -name=>"annotuname", -id=>"annotuname"))),
		Tr(td("#of Annotations"),td({nowrap},label({for=>"annotf"},"min: "),
			       textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"annotf", -id=>"annotf",style=>"width: 58px;"),
			       label({for=>"annott"},"max: "),
			       textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"annott", -id=>"annott",style=>"width: 58px;"))),
		Tr(td(""),td(checkbox(-dojoType=>"dijit.form.CheckBox", -id=>"order", -value=>"random",label=>"random order"))),
		Tr(td(a({href=>"http://bit.ly/pNAmYP", target=>'help'}, "H-inv autoannotation")),
		   td(textfield(-dojoType=>"dijit.form.TextBox", -name=>"hinvdesc", -id=>"hinvdesc"))),
		Tr(td(""),td({nowrap},label({for=>"hinvcategory"},"category"),
			       textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"hinvcategory", -id=>"hinvcategory",style=>"width: 58px;"))),
		Tr(td("Protein Length [AA]"),td({nowrap},label({for=>"lf"},"min: "),
					textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"lf", -id=>"lf",style=>"width: 58px;"),
					label({for=>"lt"},"max: "),
					textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"lt", -id=>"lt",style=>"width: 58px;"))),
		Tr(td("Molecular Weight [Da]"),td({nowrap},label({for=>"mwf"},"min: "),
				    textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"mwf", -id=>"mwf",style=>"width: 58px;"),
				    label({for=>"mwt"},"max: "),
				    textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"mwt", -id=>"mwt",style=>"width: 58px;"))),
		Tr(td("Isoelectric point (pI)"),td({nowrap},label({for=>"pif"},"min: "),
			       textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"pif", -id=>"pif",style=>"width: 58px;"),
			       label({for=>"pit"},"max: "),
			       textfield(-dojoType=>"dijit.form.NumberTextBox", -name=>"pit", -id=>"pit",style=>"width: 58px;"))),
		Tr(td("Indirect Search via Homolog"),td(textfield(-dojoType=>"dijit.form.TextBox", -name=>"id", -id=>"homologid" ))),
		Tr(td(radio_group(-dojoType=>"dijit.form.RadioButton", -class=>"homolog" ,
				       -values=>[qw/mim acc/],
				       -labels=>{'mim'=>'MIM ID', 'acc'=>'Accession Number'},
				       -default=>'mim')),
		   td(label({for=>"homologevalue"},"E value: 1.0E"),input({-dojoType=>"dijit.form.NumberSpinner",
				    -onChange=>"dojo.byId('oc1').value=arguments[0]",
				    -value=>"-10",
				    -class=>"bigFont",
				    -constraints=>"{max:1550,places:0}",
				    -name=>"evalue",
				    -labels=>"1.0E^",
				    -id=>"homologevalue",style=>"width: 58px;"}))),
		Tr(td("Expressed Tissue / Stage"),
		   td(Select({-dojoType=>"dijit.form.MultiSelect", -method=>"post", -name=>"est", -id=>"est",multiple=>"true",
			      #-onsubmit=>"return false;",-style=>"height:100px; width:175px; border:5px solid #ededed;"},
			      -onsubmit=>"return false;",-style=>"height:100px; width: 100%; border:5px solid #ededed;"},
			     map({option({value=>"$_"},$est{$_})} qw/eg cl gn tb lv em jv jm ad gd ts es nc ht bd dg ma/)))),
		Tr(td("Data availability"),
		   td(Select({-dojoType=>"dijit.form.MultiSelect", -method=>"post", -name=>"content", -id=>"content",multiple=>"true",
			      -onsubmit=>"return false;",-style=>"height:100px; width: 100%; border:5px solid #ededed;"},
			     map({option({value=>"$_"},$_)} "2D-PAGE","MS/MS","3DPL","EST","Microarray","InterProScan",
				 "TMHMM","BLASTP","MODELLER","PSIPRED","NetPhos","OMIM","WoLF PSORT"))))
	      
	  )),
    div({align=>"center",nowrap},Button({-dojoType=>"dijit.form.Button", -type=>"submit", -value=>"Submit"},"Search"),
	#Button({-dojoType=>"dijit.form.Button", -type=>"button",onClick=>"location.href='php/csvresult.php'"},"CSV"),
	Button({-dojoType=>"dijit.form.Button", -type=>"button",onClick=>"changeResultGrid('csv');"},"Get CSV"),
	Button({-dojoType=>"dijit.form.Button", -type=>"reset"},"Clear")),
    end_form
}

sub BLAST{
    #start_form( -method=>"post" , -name=>"blastsearchform", -id=>"blastsearchform" , -onsubmit=>"searchBlast();return false;"),
    start_form(-dojoType=>"dijit.form.Form", -method=>"post" , -name=>"blastsearchform", -id=>"blastsearchform" , -onsubmit=>"changeResultGrid();return false;"),
    table({ -class=>"searchAccordion"},
	  Tr(td({ -colspan=>"3"}, "Sequence: ",br,
		textarea(-dojoType=>"dijit.form.SimpleTextarea", -name=>"seq", -rows=>"10", -cols=>"50",
			 -tooltiptext=>"Type in sequence in this box", -id=>"blastseq"),
		div({ -dojoType=>"dijit.Tooltip", -connectId=>"blastseq", -position=>"below"},"Type in sequence in this box"))),
	  Tr(td("Type"), td([@prog_radio])),
	  Tr(td("Repeat filter"), td([@filter_radio])),
	  #Tr(td("Mask lowercase"), td([@mask_lc_radio])),
	  Tr(td(a({ -target=>"_help",
		    -href=>"http://www.ncbi.nlm.nih.gov/Education/BLASTinfo/Scoring2.html"},"Distance Matrix")),
	     td({-colspan=>"2"},Select({-dojoType=>"dijit.form.ComboBox",-id=>"blastmatrix"},
				       map({$_ eq "BLOSUM62" ? option({-value=>$_,-selected=>"true"},$_):option({-value=>$_},$_)}
					   qw/BLOSUM45 BLOSUM50 BLOSUM62 BLOSUM80 BLOSUM90 PAM250 PAM70 PAM30/))),
	     #td(img({ -width=>"60%", -src=>"http://www.ncbi.nlm.nih.gov/Education/BLASTinfo/Matrices.gif", -height=>"60%"} )),
	  ),
	  Tr(td([qw/BLOSUM80 BLOSUM62 BLOSUM45/])),
	  Tr(td([qw/PAM1 PAM120 PAM250/])),
	  Tr(td([i("Less divergent"),"&hArr;",i("More divergent")]))),
    div({align=>"center",nowrap},Button({-dojoType=>"dijit.form.Button", -type=>"submit", -value=>"Submit"},"Submit"),
	Button({-dojoType=>"dijit.form.Button", -type=>"button",onClick=>"location.href='php/csvresult.php'"},"CSV"),
	Button({-dojoType=>"dijit.form.Button", -type=>"reset"},"Clear")),
    end_form
}

sub Peptide{
    start_form(-dojoType=>"dijit.form.Form", -method=>"post", -onsubmit=>"changeResultGrid();return false;"),
    table({-style=>"border: 1px solid #9f9f9f;", -cellspacing=>"10"},
	  tbody(Tr(td("Fragment Sequence"),
		   td(textfield(-dojoType=>"dijit.form.TextBox", -id=>"grep", -size=>"30"))),   
		Tr(td("PMF"),
		   td(textarea(-dojoType=>"dijit.form.SimpleTextarea", -id=>"pmf", -rows=>"5", -cols=>"30",
			       -tooltiptext=>"Input the mass weights (Da), which should be splitted by linebreaks."),br,
		      span(a({href=>"javascript:void(0)",onclick=>"dijit.byId('pmf').attr('value',\"$PMF\");"},
			img({src=>"images/icon/add.png",title=>"Add Sample Data",style=>"vertical-align:middle;"}),"Example")),
		      div({ -dojoType=>"dijit.Tooltip", -connectId=>"pmf", -position=>"right"},
			  "Input the mass weights (Da), which should be splitted by linebreaks."))),
		Tr(td("Modifications"),
		   td(Select({-dojoType=>"dijit.form.MultiSelect", -method=>"post", -id=>"ptm",multiple=>"true",
			      -onsubmit=>"return false;",-style=>"height:50px; width:200px; border:5px solid #ededed;"},
			     map({option({value=>"$mod{$_}",label=>"$_"},$_)} @mod)),
		      div({ -dojoType=>"dijit.Tooltip", -connectId=>"ptm", -position=>"below"},"Check any known or suspected variable modifications."))),
		Tr(td("S-alkyl reagent"),
		   td(@cmod_radio,
		      div({ -dojoType=>"dijit.Tooltip", -connectId=>"sh", -position=>"below"},"Select one fixed modification."))),
		Tr(td(""),td("c:Carbamidomethylation, m:Carboxymethylation, e:Pyridylethylation")),
		Tr(td("Threshold"),
		   td(textfield(-dojoType=>"dijit.form.NumberTextBox",-id=>"accuracy", -value=>"0.2", -size=>"3",
				-tooltiptext=>"Input the error window for MS/MS fragment ion mass values."),
		      div({ -dojoType=>"dijit.Tooltip", -connectId=>"accuracy", -position=>"below"},
			  "Input the error window for MS/MS fragment ion mass values."))),
		Tr(td("Contaminants"),td("")),
		Tr(td("Keratin"),td(checkbox(-dojoType=>"dijit.form.CheckBox", -id=>"contami", -value=>"Keratin"
					     -tooltiptext=>"Check this box if contamination of keratin is suspected."),
				    div({ -dojoType=>"dijit.Tooltip", -connectId=>"contami", -position=>"below"},
					"Check this box if contamination of keratin is suspected."))),
		Tr(td("Trypsin"),
		   td(@trypsin_radio,
		      div({ -dojoType=>"dijit.Tooltip", -connectId=>"trypsin", -position=>"below"},
			  "Select the kind of trypsin if the contamination is suspected."))),

#The peptide masses are with cysteines treated with:
# nothing (in reduced form)
# Iodoacetic acid
# Iodoacetamide
# 4-vinyl pyridene
#with acrylamide adducts
#with methionines oxidized
# [M+H]+ or  [M] or  [M-H]- .
# average or   monoisotopic.
#Select an enzyme :
#Allow for ? missed cleavages.


#		Tr(td("Display"),
#		   td("top",
#		      Select({-dojoType=>"dijit.form.ComboBox",-name=>"appear",-tooltiptext=>"Select the number of candidates to be appeared among 10, 20, and 50."},
#			     map({$_ == 20 ? option({-value=>$_,-selected=>"true"},$_):option({-value=>$_},$_)} 10,20,50)),
#		      div({ -dojoType=>"dijit.Tooltip", -connectId=>"permsappear", -position=>"above"},
#			  "Select the number of candidates to be appeared among 10, 20, and 50.")))

	  )),
    div({align=>"center",nowrap},Button({-dojoType=>"dijit.form.Button", -type=>"submit", -value=>"Submit"},"Submit"),
	Button({-dojoType=>"dijit.form.Button", -type=>"button",onClick=>"location.href='php/csvresult.php'"},"CSV"),
	Button({-dojoType=>"dijit.form.Button", -type=>"reset"},"Clear")),
    end_form
}

sub SearchBox{
    div({ -dojotype=>"dijit.layout.AccordionContainer",-sizeMin=>"20",-sizeShare=>"45"},
	div({ -title=>"General Search", -id=>'general', -selected=>"false", -dojotype=>"dijit.layout.ContentPane"},GeneralSearch),
	div({ -title=>"CIPRO BLAST", -id=>'blast', -selected=>"false", -dojotype=>"dijit.layout.ContentPane"},BLAST),
	div({ -title=>"PerMS", -id=>'perms', -selected=>"false", -dojotype=>"dijit.layout.ContentPane"},Peptide))
}

print header(-expires=>'-1'), 
    start_html(-title=>'CIPRO',
	       -style=>["css/cipro.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/layout/resources/ScrollPane.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/editor/plugins/resources/css/Save.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/grid/resources/Grid.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/grid/resources/soriaGrid.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/grid/resources/tundraGrid.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/grid/resources/nihiloGrid.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/form/resources/Rating.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dijit/themes/dijit.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojox/form/resources/FileInput.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dojo/resources/dojo.css",
			#"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dijit/themes/tundra/tundra.css",
			"http://ajax.googleapis.com/ajax/libs/dojo/$DOJO_VER/dijit/themes/soria/soria.css"
	       ],
	       #-head =>[meta({-http_equiv => 'Pragma',-content => 'no-cache'}),
		#	meta({-http_equiv => 'Cache-Control',-content => 'no-store'}),
		#	meta({-http_equiv => 'Cache-Control',-content => 'no-cache'}),
		#	meta({-http_equiv => 'Expires',-content => '-1'})
	       #],
	       -script=>[{-type=>"text/javascript", -src=>"./js/cipro.js"},
			 $JSCRIPT],
	       -class=>"soria",
	       -head=>$DOJOSCRIPT);
print  div({-dojoType=>"dijit.layout.LayoutContainer",
	    -style=>"width: 100%; height: 100%; padding: 0; margin: 0; border: 0;"},
	   div({-dojoType=>"dijit.layout.ContentPane", -layoutAlign=>"top"}, Toolbar, Top),
	   div({-dojotype=>"dijit.layout.TabContainer",-tabStrip=>"true", -layoutAlign=>"client",
		-style=>"overflow:hidden;height: 100%; width: 99%;", -id=>"maindiv"},
	       div({-href=>"home.html", -title=>"HOME", -id=>"hometab", -selected=>"$homeTabSelectFlag",
	      	    -dojotype=>"dijit.layout.ContentPane"},""),
	       div({-title=>"SEARCH",-id=>"searchtab", -selected=>"$searchTabSelectFlag" ,
	       	     -dojotype=>"dijit.layout.SplitContainer",-orientation=>"horizontal",
		     -sizerWidth=>"5" ,-activeSizing=>"0"},
		   SearchBox,
		   print_searchresult($startTime,$endTime,$ID)
		   ))); 
print end_html;

