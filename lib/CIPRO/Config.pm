## @class 
# CIPROの設定を取得するクラス。
#
package CIPRO::Config;
use Config::Simple;
use strict;
use warnings;
use constant CONFIG_FILE => 'conf/cipro.ini';
use Class::Std;

## @method string get_database_source_name ()
#
# DB接続データソースネームを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return DB接続データソースネーム
#
my %database_source_name_of  : ATTR( :get<database_source_name> );


## @method string get_database_user ()
#
# DB接続ユーザー名を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return DB接続ユーザー名
my %database_user_of          : ATTR( :get<database_user> );


## @method string get_database_pass ()
#
# DB接続パスワードを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return DB接続パスワード
#
my %database_pass_of          : ATTR( :get<database_pass> ); 


## @method string get_towdpage_icon_dir_path ()
#
# 2d page用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 2d page用の画像ファイルディレクトリパス
#
my %twodpage_icon_dir_path_of          : ATTR( :get<2dpage_icon_dir_path> );

## @method string get_threedpl_icon_dir_path ()
#
# 3dpl用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 3dpl用の画像ファイルディレクトリパス
#
my %threedpl_icon_dir_path_of          : ATTR( :get<3dpl_icon_dir_path> );

## @method string get_threedpl_movie_root_path ()
#
# 3dpl用の動画ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 3dpl用の動画ファイルディレクトリパス
#
my %threedpl_movie_root_path_of          : ATTR( :get<3dpl_movie_root_path> );

## @method string get_est_icon_dir_path ()
#
# est用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return est用の画像ファイルディレクトリパス
#
my %est_icon_dir_path_of               : ATTR( :get<est_icon_dir_path> );

## @method string get_cphmodels_icon_dir_path ()
#
# cphmodels用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return cphmodels用の画像ファイルディレクトリパス
#
my %cphmodels_icon_dir_path_of         : ATTR( :get<cphmodels_icon_dir_path> );

## @method string get_sosui_icon_dir_path ()
#
# sosui用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return sosui用の画像ファイルディレクトリパス
#
my %sosui_icon_dir_path_of             : ATTR( :get<sosui_icon_dir_path> );

## @method string get_smart_icon_dir_path ()
#
# smart用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return smart用の画像ファイルディレクトリパス
#
my %smart_icon_dir_path_of             : ATTR( :get<smart_icon_dir_path> );

## @method string get_wolfpsort_icon_dir_path ()
#
# wolfpsort用の画像ファイルディレクトリパスを取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return wolfpsort用の画像ファイルディレクトリパス
#
my %wolfpsort_icon_dir_path_of         : ATTR( :get<wolfpsort_icon_dir_path> );


## @method string get_session_sid_name ()
#
# セッション管理用のcookie名を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return cookie名
#
my %session_sid_name_of                 : ATTR( :get<session_sid_name> );

## @method string get_session_out_dir ()
#
# セッション管理用ファイルの出力先を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return セッション管理用ファイルの出力先
#
my %session_out_dir_of                  : ATTR( :get<session_out_dir> );

## @method string get_session_expires ()
#
# セッションの有効期限を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return セッションの有効期限
#
my %session_expires_of                  : ATTR( :get<session_expires> );

## @method string get_view_image_width ()
#
# 画像表示横幅サイズ(Pixel)を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 画像表示横幅サイズ
#
my %view_image_width_of                  : ATTR( :get<view_image_width> );

## @method string get_view_image_height ()
#
# 画像表示縦幅サイズ(Pixel)を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 画像表示縦幅サイズ
#
my %view_image_height_of                  : ATTR( :get<view_image_height> );

## @method string get_print_image_width ()
#
# 印刷用画像表示横幅サイズ(Pixel)を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 画像表示横幅サイズ
#
my %print_image_width_of                  : ATTR( :get<print_image_width> );

## @method string get_print_image_height ()
#
# 印刷用画像表示縦幅サイズ(Pixel)を取得する。\n
# ATTRにより定義されたメソッドです。
#
# @return 画像表示縦幅サイズ
#
my %print_image_height_of                  : ATTR( :get<print_image_height> );

## @cond
sub BUILD {
    my ($self, $ident, $arg_ref) = @_;
    my $cfg = new Config::Simple(CONFIG_FILE)->vars();

    #[database]
    $database_source_name_of{$ident} = $cfg->{'database.source_name'};
    $database_user_of{$ident}         = $cfg->{'database.user'};
    $database_pass_of{$ident}         = $cfg->{'database.pass'};

    #[2dpage]
    $twodpage_icon_dir_path_of{$ident}  = $cfg->{'2dpage.icon_dir_path'};

    #[3dpl]
    $threedpl_icon_dir_path_of{$ident}  = $cfg->{'3dpl.icon_dir_path'};
    $threedpl_movie_root_path_of{$ident}  = $cfg->{'3dpl.movie_root_path'};

    #[est]
    $est_icon_dir_path_of{$ident}       = $cfg->{'est.icon_dir_path'};

    #[cphmodels]
    $cphmodels_icon_dir_path_of{$ident} = $cfg->{'cphmodels.icon_dir_path'};

    #[sosui]
    $sosui_icon_dir_path_of{$ident}     = $cfg->{'sosui.icon_dir_path'};

    #[smart]
    $smart_icon_dir_path_of{$ident}     = $cfg->{'smart.icon_dir_path'};

    #[wolfpsort]
    $wolfpsort_icon_dir_path_of{$ident} = $cfg->{'wolfpsort.icon_dir_path'};

    #[session]
    $session_sid_name_of{$ident}        = $cfg->{'session.sid_name'};
    $session_out_dir_of{$ident}         = $cfg->{'session.out_dir'};
    $session_expires_of{$ident}         = $cfg->{'session.expires'};

    #[image]
    $view_image_width_of{$ident}        = $cfg->{'image.view_image_width'};
    $view_image_height_of{$ident}       = $cfg->{'image.view_image_height'};
    $print_image_width_of{$ident}       = $cfg->{'image.print_image_width'};
    $print_image_height_of{$ident}      = $cfg->{'image.print_image_height'};
}
## @endcond

1;

