## @class
# basic_info_reference_db_linkのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::BasicInfoReferenceDbLink;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("basic_info_reference_db_link");
__PACKAGE__->add_columns(
  "basic_info_id",
  { data_type => "integer", default_value => undef, is_nullable => 0, size => 4 },
  "reference_db_id",
  { data_type => "integer", default_value => undef, is_nullable => 0, size => 4 },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("basic_info_id", "reference_db_id");
__PACKAGE__->add_unique_constraint(
  "basic_info_reference_db_link_pkey",
  ["basic_info_id", "reference_db_id"],
);
__PACKAGE__->belongs_to(
  "reference_db_id",
  "CIPRO::Schema::ReferenceDb",
  { id => "reference_db_id" },
);
__PACKAGE__->belongs_to(
  "basic_info_id",
  "CIPRO::Schema::BasicInfo",
  { id => "basic_info_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NBPSudWa9Skr4HyFwR52FQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
