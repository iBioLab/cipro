## @class
# docsのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::Docs;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("docs");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 0,
    size => undef,
  },
  "doc",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "vector",
  {
    data_type => "tsvector",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("docs_pkey", ["id"]);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nlQkBdhf2iFt6YWWXyraDg


# You can replace this text with custom content, and it will be preserved on regeneration
1;
