## @class
# external_dbのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::ExternalDb;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("external_db");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('external_db_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "type",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "name",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "url",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("external_db_pkey", ["id"]);
__PACKAGE__->has_many(
  "reference_dbs",
  "CIPRO::Schema::ReferenceDb",
  { "foreign.external_db_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Wfwi5CVg6siGc8oQ+01vWQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
