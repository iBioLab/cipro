## @class
# perms_seq_info_linkのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::PermsSeqInfoLink;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("perms_seq_info_link");
__PACKAGE__->add_columns(
  "seq_info_id",
  { data_type => "integer", default_value => undef, is_nullable => 0, size => 4 },
  "perms_id",
  { data_type => "integer", default_value => undef, is_nullable => 0, size => 4 },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("seq_info_id", "perms_id");
__PACKAGE__->add_unique_constraint("perms_seq_info_link_pkey", ["seq_info_id", "perms_id"]);
__PACKAGE__->belongs_to(
  "seq_info_id",
  "CIPRO::Schema::SeqInfo",
  { id => "seq_info_id" },
);
__PACKAGE__->belongs_to("perms_id", "CIPRO::Schema::Perms", { id => "perms_id" });


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:a30TQzwghNM+vTGzc/7CcA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
