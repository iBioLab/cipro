## @class
# resultsのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::Results;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("results");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "bigint",
    default_value => "nextval('results_id_seq'::regclass)",
    is_nullable => 0,
    size => 8,
  },
  "basic_info_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "program_type_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "sub_type_info_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "res",
  { data_type => "xml", default_value => undef, is_nullable => 1, size => undef },
  "reserve",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("results_pkey", ["id"]);
__PACKAGE__->has_many(
  "movies",
  "CIPRO::Schema::Movie",
  { "foreign.results_id" => "self.id" },
);
__PACKAGE__->has_many(
  "pictures",
  "CIPRO::Schema::Picture",
  { "foreign.results_id" => "self.id" },
);
__PACKAGE__->belongs_to(
  "program_type_id",
  "CIPRO::Schema::ProgramType",
  { id => "program_type_id" },
);
__PACKAGE__->belongs_to(
  "basic_info_id",
  "CIPRO::Schema::BasicInfo",
  { id => "basic_info_id" },
);
__PACKAGE__->belongs_to(
  "sub_type_info_id",
  "CIPRO::Schema::SubTypeInfo",
  { id => "sub_type_info_id" },
);
__PACKAGE__->has_many(
  "thumbnails",
  "CIPRO::Schema::Thumbnail",
  { "foreign.results_id" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:T6OFsa+nqSTXl/0V6DgiIQ


# You can replace this text with custom content, and it will be preserved on regeneration
1;
