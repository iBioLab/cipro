## @class
# seq_infoのスキーマクラス。\n 
# DBIx::Class::Schema::Loaderで自動生成されたファイルです。
#
package CIPRO::Schema::SeqInfo;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("seq_info");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('seq_info_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "basic_info_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "seq",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "sha256",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "mw",
  { data_type => "real", default_value => undef, is_nullable => 1, size => 4 },
  "pi",
  { data_type => "real", default_value => undef, is_nullable => 1, size => 4 },
  "createdate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "createuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "updatedate",
  { data_type => "date", default_value => "now()", is_nullable => 0, size => 4 },
  "updateuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "deletedate",
  { data_type => "date", default_value => undef, is_nullable => 1, size => 4 },
  "deleteuser",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("seq_info_pkey", ["id"]);
__PACKAGE__->has_many(
  "perms_seq_info_links",
  "CIPRO::Schema::PermsSeqInfoLink",
  { "foreign.seq_info_id" => "self.id" },
);
__PACKAGE__->belongs_to(
  "basic_info_id",
  "CIPRO::Schema::BasicInfo",
  { id => "basic_info_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04005 @ 2009-02-05 17:37:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hr+b4HF4ezuwaqgMYKkPPA


# You can replace this text with custom content, and it will be preserved on regeneration
1;
