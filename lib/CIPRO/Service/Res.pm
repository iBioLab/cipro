## @class 
# resultsテーブルの resフィールドを取得するクラス。
#
package CIPRO::Service::Res;

use lib "../../../lib";
use strict;
use warnings;
use CIPRO::Service::Base;
use CIPRO::Service::Type;
use CIPRO::Service::Basic;

@CIPRO::Service::Res::ISA = qw(CIPRO::Service::Base);

## @method string searchByCiproId ()
#
# DB接続→basic_infoテーブルアクセス→program_typeテーブルアクセス
# sub_type_infoテーブルアクセス→resultsテーブルアクセスを実施し、
# resフィールドを取得するメソッド
#
# @return resultsテーブルの resフィールド
#
sub searchByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my ($id, $type,$sub) = @_;
    my $res = '';
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $result;
    if(length($sub) > 0){
        my $subtypeinfo_id = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                      , sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
    }else{
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                       , deletedate => \'is null'});
    }
    if($result->count > 0){
        $res = $result->single->res;
    }
    return $res;
}

## @method string searchReserveByCiproId ()
#
# DB接続→basic_infoテーブルアクセス→program_typeテーブルアクセス
# sub_type_infoテーブルアクセス→resultsテーブルアクセスを実施し、
# reserveフィールドを取得するメソッド
#
# @return resultsテーブルの reserveフィールド
#
sub searchReserveByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my ($id, $type,$sub) = @_;
    my $reserve = '';
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $result;
    if(length($sub) > 0){
        my $subtypeinfo_id = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                        , sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
    }else{
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                       , deletedate => \'is null'});
    }
    if($result->count > 0){
        $reserve = $result->single->reserve;
    }
    return $reserve;
}

## @method string searchRandomByProgramAndSubTypeId ()
#
# DB接続→program_typeテーブルアクセス
# sub_type_infoテーブルアクセス→resultsテーブルアクセスを実施し、
# ランダムに1件basic_info_idフィールドを取得→basic_infoテーブル
# からciproフィールドを取得する
# @return basic_infoテーブルの ciproフィールド
#
sub searchRandomByProgramAndSubTypeId{
    my $self = shift;
    my $schema = $self->connect;
    my ($type,$sub) = @_;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    my $basicinfo_id    = $schema->resultset('Results')->search("id IN (SELECT picture.results_id FROM picture WHERE picture.deletedate is null)".
								" AND program_type_id = $programtype_id AND sub_type_info_id = $subtypeinfo_id".
								" AND deletedate is null",
                                                                { order_by => 'random()' , page => 1 , rows => 1 } )->first->basic_info_id->id; 
    return $schema->resultset('BasicInfo')->search({id => $basicinfo_id , deletedate => \'is null'})->single->cipro;
}

## @method string searchPdbByCiproId ()
#
# cphのresフィールドを取得する。
# pdbファイルのデータが<res>タグで括られているので
# <res>タグのテキスト部のみ取得。
# @return resultsテーブルの resフィールド
#
sub searchPdbByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my $id   = shift;
    my $type = shift;
    my $sub  = shift;
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $subtypeinfo_id  = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
    my $result          = $schema->resultset('Results')->search({ basic_info_id => $basicinfo_id ,
                                                                  program_type_id => $programtype_id ,
                                                                  sub_type_info_id => $subtypeinfo_id ,
                                                                  deletedate => \'is null'} ,
                                                                { select => ["(xpath('/res/text()' , res))[1]::text"],
                                                                  as     => ["pdb"] } );
    return $result->single->get_column('pdb');
}

## @method string searchEstByCiproId ()
#
# resultsテーブルのestデータを取得する
# @param[in] cipro CIPRO ID
# @return resultsテーブルのestデータ
#
sub searchEstByCiproId{
    my @EST = qw/eg cl gn tb lv em jv jm ad gd ts es nc ht bd dg ma/;
    my @selest = ();
    push @selest , map{"( xpath('/res/detail/$_/text()' , res ) )[1]::text"} @EST;
    my ($self,$cipro) = @_;
    my $schema = $self->connect;

    my $basicService = CIPRO::Service::Basic->new;
    my $basicinfo_id 
            = $basicService->getIdByCipro($cipro, $schema);

    my $typeService = CIPRO::Service::Type->new;
    my $programtype_id 
            = $typeService->getProgramTypeIdByType("Expression Profile", $schema);

    my $subtypeinfo_id 
            = $typeService->getSubTypeInfoIdByType("est", $schema);
=pod
    print "basicinfo_id   = ".$basicinfo_id,"\n";
    print "programtype_id = ".$programtype_id,"\n";
    print "subtypeinfo_id = ".$subtypeinfo_id,"\n";
=cut
    my $results 
            = $schema->resultset('Results')->search({
                                                        basic_info_id    => $basicinfo_id,
                                                        program_type_id  => $programtype_id, 
                                                        sub_type_info_id => $subtypeinfo_id,
                                                        deletedate       => \'is null',
                                                      },
                                                      { 
                                                        select => [@selest],
                                                        as     => [@EST]
                                                      });
    my @ret = ();
    map {
        @ret = ($_->get_column($EST[0]),
                $_->get_column($EST[1]),
                $_->get_column($EST[2]),
                $_->get_column($EST[3]),
                $_->get_column($EST[4]),
                $_->get_column($EST[5]),
                $_->get_column($EST[6]),
                $_->get_column($EST[7]),
                $_->get_column($EST[8]),
                $_->get_column($EST[9]),
                $_->get_column($EST[10]),
                $_->get_column($EST[11]),
                $_->get_column($EST[12]),
                $_->get_column($EST[13]),
                $_->get_column($EST[14]),
                $_->get_column($EST[15]),
                $_->get_column($EST[16]) );
    } $results->all;
    return @ret;
}

## @method string searchMicroarrayByCiproId ()
#
# resultsテーブルのestデータを取得する
# @param[in] cipro CIPRO ID
# @return resultsテーブルのmicroarrayデータ
#
sub searchMicroarrayByCiproId{
    my @microarray = qw/_2cell _4cell _8cell _16cell _32cell _64cell _EG _LG _EN _ITB _ETB _MTB _LTB _LV _JN _1__5M _2__5M _4__0M/;
    my $n = 0;
    my $flag = 1;
    my @ret = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    while ($flag == 1) {
        $n++;
        my @selest = ();
        push @selest , map{"( xpath('/res/data/$_/text()' , res ) )[$n]::text"} @microarray;
        my ($self,$cipro) = @_;
        my $schema = $self->connect;

        my $basicService = CIPRO::Service::Basic->new;
        my $basicinfo_id 
                = $basicService->getIdByCipro($cipro, $schema);

        my $typeService = CIPRO::Service::Type->new;
        my $programtype_id 
                = $typeService->getProgramTypeIdByType("Expression Profile", $schema);

        my $subtypeinfo_id 
                = $typeService->getSubTypeInfoIdByType("microarray", $schema);

        my $results 
                = $schema->resultset('Results')->search({
                                                        basic_info_id    => $basicinfo_id,
                                                        program_type_id  => $programtype_id, 
                                                        sub_type_info_id => $subtypeinfo_id,
                                                        deletedate       => \'is null',
                                                      },
                                                      { 
                                                        select => [@selest],
                                                        as     => [@microarray]
                                                      });
	if(defined $results->first) {
	    for my $i (0..$#microarray) {
	        $ret[$i] += $results->first->get_column($microarray[$i]);
		if($i < $#microarray) {
		    $results->next;
		}
	    }
	} else {
	    $flag = 0;
	}
    }

    for my $i (@ret) {
      $i /= $n;
    }

    return @ret;
}

## @method string search2DPageQuantityByCiproId ()
#
# resultsテーブルのestデータを取得する
# @param[in] cipro CIPRO ID
# @return resultsテーブルの2D-PAGE quantityデータ
#
sub search2DPageQuantityByCiproId{
    my @stage = qw/eg ga ne lv ov nc/;
    my @selest = ();
    push @selest , map{"( xpath('/res/quantity/$_/text()' , res ) )[1]::text"} @stage;
    my ($self,$cipro) = @_;
    my $schema = $self->connect;

    my $basicService = CIPRO::Service::Basic->new;
    my $basicinfo_id 
            = $basicService->getIdByCipro($cipro, $schema);

    my $typeService = CIPRO::Service::Type->new;
    my $programtype_id 
            = $typeService->getProgramTypeIdByType("Expression Profile", $schema);

    my $subtypeinfo_id 
            = $typeService->getSubTypeInfoIdByType("2D-PAGE Quantity", $schema);
    
    my $results 
            = $schema->resultset('Results')->search({
                                                        basic_info_id    => $basicinfo_id,
                                                        program_type_id  => $programtype_id, 
                                                        sub_type_info_id => $subtypeinfo_id,
                                                        deletedate       => \'is null',
                                                      },
                                                      { 
                                                        select => [@selest],
                                                        as     => [@stage]
                                                      });
    my @ret = ();
    map {
        @ret = ($_->get_column($stage[0]),
                $_->get_column($stage[1]),
                $_->get_column($stage[2]),
                $_->get_column($stage[3]),
                $_->get_column($stage[4]),
                $_->get_column($stage[5]));
    } $results->all;
    return @ret;
}

## @method string search2DPageSpotsByCiproId ()
#
# resultsテーブルのestデータを取得する
# @param[in] cipro CIPRO ID
# @return resultsテーブルの2D-PAGE quantityデータ
#
sub search2DPageSpotsByCiproId{
    my @stage = qw/eg ga ne lv ov nc/;
    my @selest = ();
    push @selest , map{"( xpath('/res/spots/$_/text()' , res ) )[1]::text"} @stage;
    my ($self,$cipro) = @_;
    my $schema = $self->connect;

    my $basicService = CIPRO::Service::Basic->new;
    my $basicinfo_id 
            = $basicService->getIdByCipro($cipro, $schema);

    my $typeService = CIPRO::Service::Type->new;
    my $programtype_id 
            = $typeService->getProgramTypeIdByType("Expression Profile", $schema);

    my $subtypeinfo_id 
            = $typeService->getSubTypeInfoIdByType("2D-PAGE Quantity", $schema);
    
    my $results 
            = $schema->resultset('Results')->search({
                                                        basic_info_id    => $basicinfo_id,
                                                        program_type_id  => $programtype_id, 
                                                        sub_type_info_id => $subtypeinfo_id,
                                                        deletedate       => \'is null',
                                                      },
                                                      { 
                                                        select => [@selest],
                                                        as     => [@stage]
                                                      });
    my @ret = ();
    map {
        @ret = ($_->get_column($stage[0]),
                $_->get_column($stage[1]),
                $_->get_column($stage[2]),
                $_->get_column($stage[3]),
                $_->get_column($stage[4]),
                $_->get_column($stage[5]));
    } $results->all;
    return @ret;
}


## @method string searchMassByCiproId ()
#
# resultsテーブルのestデータを取得する
# @param[in] cipro CIPRO ID
# @return resultsテーブルのmassデータ
#
sub searchMassByCiproId{
    my @stage = qw/_OR _GI _ST _IN _EN _NC _HT _BD _TS _OV _OE/;
    my @selest = ();
    push @selest , map{"( xpath('/res/$_/text()' , res ) )[1]::text"} @stage;
    my ($self,$cipro) = @_;
    my $schema = $self->connect;

    my $basicService = CIPRO::Service::Basic->new;
    my $basicinfo_id 
            = $basicService->getIdByCipro($cipro, $schema);

    my $typeService = CIPRO::Service::Type->new;
    my $programtype_id 
            = $typeService->getProgramTypeIdByType("Expression Profile", $schema);

    my $subtypeinfo_id 
            = $typeService->getSubTypeInfoIdByType("Spectral Count", $schema);
    
    my $results 
            = $schema->resultset('Results')->search({
                                                        basic_info_id    => $basicinfo_id,
                                                        program_type_id  => $programtype_id, 
                                                        sub_type_info_id => $subtypeinfo_id,
                                                        deletedate       => \'is null',
                                                      },
                                                      { 
                                                        select => [@selest],
                                                        as     => [@stage]
                                                      });
    my @ret = ();
    map {
        @ret = ($_->get_column($stage[0]),
                $_->get_column($stage[1]),
                $_->get_column($stage[2]),
                $_->get_column($stage[3]),
                $_->get_column($stage[4]),
                $_->get_column($stage[5]),
                $_->get_column($stage[6]),
                $_->get_column($stage[7]),
                $_->get_column($stage[8]),
                $_->get_column($stage[9]),
                $_->get_column($stage[10]));
    } $results->all;
    return @ret;
}

1;
