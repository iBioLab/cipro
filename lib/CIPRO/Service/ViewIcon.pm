## @class 
# viewicon.cgiのDBアクセス処理を実施するクラス。
#

package CIPRO::Service::ViewIcon;

use lib "../../../lib";
use strict;
use warnings;
#use CIPRO::Image;
use CIPRO::Service::Base;

@CIPRO::Service::ViewIcon::ISA = qw(CIPRO::Service::Base);


## @method string searchByThumbnail ()
#
# thumbnailテーブルアクセスを実施し、任意のresults_idの
# fileフィールドを取得するメソッド
#
# @return thumbnailテーブルの fileフィールド
#
sub searchByThumbnail{
    my $self = shift;
    my $schema = $self->connect;
    my $results_id = shift;
    my $seqno = shift;
    my $file;
    my $thumbnail = $schema->resultset('Thumbnail')->search({results_id => $results_id , seqno => $seqno , deletedate => \'is null'});
    if($thumbnail->count > 0){
        $file = $thumbnail->single->file;
    }
    return $file;
}

## @method string searchByPicture ()
#
# pictureテーブルアクセスを実施し、任意のresults_idの
# fileフィールドを取得するメソッド
#
# @return pictureテーブルの fileフィールド
#
sub searchByPicture{
    my $self = shift;
    my $schema = $self->connect;
    my $results_id = shift;
    my $seqno = shift;
    my $file;
    my $picture = $schema->resultset('Picture')->search({results_id => $results_id , seqno => $seqno , deletedate => \'is null'});
    if($picture->count > 0){
        $file = $picture->single->file;
    }
    return $file;
}

## @method string searchByGenPicture ()
#
# program_typeテーブルアクセスを実施し、
# →general_pectureテーブルアクセスを実施し、
# 任意のtypeのfileフィールドを取得するメソッド
#
# @return general_pictureテーブルの fileフィールド
#
sub searchByGenPicture{
    my $self = shift;
    my $schema = $self->connect;
    my $type = shift;
    my $programtype_id = 0;
    my $file;
    if(!$type eq ''){
        $programtype_id = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    }
    my $general = $schema->resultset('GeneralPicture')->search({program_type_id => $programtype_id , deletedate => \'is null'});
    if($general->count > 0){
        $file = $general->single->file;
    }
    return $file;
}

## @method string searchByIcon ()
#
# 該当するテーブルのfileフィールドを取得するメソッド
#
# @return general_picture または picture または thumbnailテーブルの fileフィールド
#
sub searchByIcon{
    my $self = shift;
    my $schema = $self->connect;
    my ($id, $type, $sub , $seq , $thumb) = @_;

    my $nodata_flag = 1;
    my $results_id;
    my $basicinfo = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'});
    if($basicinfo->count > 0){
        my $basicinfo_id = $basicinfo->single->id;
        my $programtype_id = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
        my $results = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id
                                                            , program_type_id => $programtype_id , deletedate => \'is null'});
        if($results->count > 0){
            $nodata_flag = 0;
            $results_id = $results->single->id;

            if(!$sub eq ''){
                my $subtypeinfo_id  =$schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
                my $results = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id , program_type_id => $programtype_id ,
                                                                     sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
                if($results->count > 0){
                    $nodata_flag = 0;
                    $results_id = $results->single->id;
                }
            }
        }
    }

    my $FILE = '';

    if($nodata_flag){
        #no data
        if($type eq 'Sequence and Motif'){
            #Sequence and Motifでは、seq_infoテーブルのseqデータを表示するが、
            #seq_infoのデータは、CIPRO ID毎に必ず存在するので「No Data」で無く、
            #general_picture画像を表示する。
            $FILE = searchByGenPicture($self,$type);
        }else{
            $FILE = searchByGenPicture($self,'');
        }
    }else{
        if($thumb eq 'thumb'){
            #thumbnail
            $FILE = searchByThumbnail($self,$results_id,$seq);
        }else{
            #picture
            $FILE = searchByPicture($self,$results_id,$seq);
            my $config = CIPRO::Config->new();
            my $max_width  = $config->get_view_image_width();
            my $max_height = $config->get_view_image_height();
            #my $serv = CIPRO::Image->new();
            #$FILE = $serv->resizeImage($FILE,$max_width,$max_height);
        }
        if(!$FILE){
            #thumbnail nothing -> general_picture
            $FILE = searchByGenPicture($self,$type);
        }
    }
    return $FILE;
}

## @method string searchIdsByCiproId ()
#
# pictureテーブルアクセスを実施し、任意のresults_idの
# idフィールドを個数分取得するメソッド
#
# @return pictureテーブルの idフィールド
#
sub searchIdsByCiproId{
    my $self = shift;
    my $schema = $self->connect;
    my ($id, $type,$sub) = @_;
    my $results_id = '';
    my $basicinfo_id    = $schema->resultset('BasicInfo')->search({cipro => $id , deletedate => \'is null'})->single->id;
    my $programtype_id  = $schema->resultset('ProgramType')->search({type => $type , deletedate => \'is null'})->single->id;
    my $result;
    my $picture;
    my @ret = ();
    if(length($sub) > 0){
        my $subtypeinfo_id = $schema->resultset('SubTypeInfo')->search({type => $sub , deletedate => \'is null'})->single->id;
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                      , sub_type_info_id => $subtypeinfo_id ,  deletedate => \'is null'});
    }else{
        $result = $schema->resultset('Results')->search({basic_info_id => $basicinfo_id, program_type_id => $programtype_id
                                                       , deletedate => \'is null'});
    }
    if($result->count > 0){
        $results_id = $result->single->id;
        $picture = $schema->resultset('Picture')->search({results_id => $results_id , deletedate => \'is null'});
        if($picture->count > 0){
            map { my $id = $_->id; my $row = [$id];push @ret, $row;} $picture->all;
        }
    }
    return @ret;
}

## @method string searchByPictureId ()
#
# pictureテーブルアクセスを実施し、任意のidの
# fileフィールドを取得するメソッド
# 印刷画面表示用にリサイズが可能
# 
# @return pictureテーブルの fileフィールド
#
sub searchByPictureId{
    my $self = shift;
    my $schema = $self->connect;
    my $id = shift;
    my $printFlag = shift;
    my $file;
    my $picture = $schema->resultset('Picture')->search({id => $id , deletedate => \'is null'});
    if($picture->count > 0){
        $file = $picture->single->file;
        # 印刷用にリサイズする
        if(1 == $printFlag){
            # 印刷用の画像表示サイズを取得してリサイズ
            my $config = CIPRO::Config->new();
            my $max_width  = $config->get_print_image_width();
            my $max_height = $config->get_print_image_height();
            #my $serv = CIPRO::Image->new();
            #$file = $serv->resizeImageForPrint($file,$max_width,$max_height);
        }
    }
    return $file;
}

1;
