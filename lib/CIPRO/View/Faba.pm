## @class 
# CIPROの設定を取得するクラス。
#
package CIPRO::View::Faba;
use strict;
use warnings;
use Class::Std;

my %ExpressedTissue_of           : ATTR( :name<ExpressedTissue> );
my %SubcellularLocalization_of   : ATTR( :name<SubcellularLocalization> );
my %Description_of               : ATTR( :name<Description> ); 
my %ExperimentalConditions_of    : ATTR( :name<ExperimentalConditions> ); 
my %Channel_of                   : ATTR( :name<Channel> ); 
my %MagnificationScale_of        : ATTR( :name<MagnificationScale> ); 

sub BUILD {
    my ($self, $ident, $arg_ref) = @_;
    $ExpressedTissue_of{$ident} = "";
    $SubcellularLocalization_of{$ident} = "";
    $Description_of{$ident} = "";
    $ExperimentalConditions_of{$ident} = "";
    $Channel_of{$ident} = "";
    $MagnificationScale_of{$ident} = "";
}


1;

