#!/usr/bin/perl
# news.cgi

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::News;

my $serv = CIPRO::Service::News->new();
my $list = $serv->selectNews();
#my $news = "";
#if($list->count > 0){
#    $news = $list->next->news;
#}

print header, start_html(-title=>uc $NAME);
if($list->count > 0){
    while(my $news = $list->next){
	print $news->createdate."&nbsp;".$news->news, br;
    }
}
print end_html;
