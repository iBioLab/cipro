#!/usr/bin/perl
# pdbdata.cgi

use strict;
use warnings;
use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::Res;

my $id = param('id');
my $type = 'Structural Model';
my $sub  = 'modeller';

my $serv = CIPRO::Service::Res->new();
my $pdb  = $serv->searchPdbByCiproId($id,$type,$sub);

print "Content-type:text/plain\n\n";
print $pdb;

