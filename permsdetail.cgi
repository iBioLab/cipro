#!/usr/bin/perl
# permsdetail.cgi

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::Perms;
use CIPRO::Service::Seq;

my $postdata;
read(STDIN,$postdata,$ENV{'CONTENT_LENGTH'});
my @q=split /\s+/,$postdata;
my ($ID,@weight,@arg,@mods,$pi,$mw,$score,$acc,@contam,@contflag);
for $i (0..$#q-1){
    if($q[$i]=~m/"id"/){
        $ID=$q[$i+1];
    }elsif($q[$i]=~m/weight/){
        @weight=split /\,/,$q[$i+1];
    }elsif($q[$i]=~m/arg/){
        @arg=split /\,/,$q[$i+1];
    }elsif($q[$i]=~m/mods/){
        @mods=split /\,/,$q[$i+1];
    }elsif($q[$i]=~m/pi/){
        $pi=$q[$i+1];
    }elsif($q[$i]=~m/score/){
        $score=$q[$i+1];
    }elsif($q[$i]=~m/acc/){
        $acc=$q[$i+1];
    }elsif($q[$i]=~m/mw/){
        $mw=$q[$i+1];
    }elsif($q[$i]=~m/contam/){
        @contam=split /,/,$q[$i+1];
    }elsif($q[$i]=~m/contflag/){
        @contflag=split /,/,$q[$i+1];
    }
}
my ($dif,$frame,$content,$modContent,$modFrame,$seq,$originalID,@c,$contamDetail);
my @mkind=('n','o','p','a','c','m','e');

my ($sth,$sth2,$proteinSeq,@tmpSeq);
our @keratin=qw/ 517.2986 533.2571 547.3026 550.2405 559.3567 576.3103 579.256 603.3716
    604.3052 629.3045 639.2482 698.3294 712.374 811.4353 906.4684 949.4598 954.4352
    968.4952 972.4028 973.4952 974.5157 997.4599 1011.4534 1016.5011 1024.4949 
    1071.6047 1080.5324 1127.5042 1165.4793 1192.6212 1263.6946 1377.6757 1421.678
    1538.7168 1593.7725 1828.9441 2043.9943 2075.0657 2244.0314 2415.302 3752.8575
    6428.7799/;
our @porcineAll=qw/ 451.1941 462.22 515.3306 569.2684 633.2844 650.315 664.367 694.39 
    711.3136 732.433 748.4279 759.3889 792.462 802.4311 820.4933 842.51 870.5412 906.5049 
    920.4842 934.5362 955.5001 964.558 968.4511 980.4875 1006.4879 1006.5209 1008.5188 
    1023.4747 1044.5478 1045.5642 1126.5645 1153.575 1169.5699 1175.6346 1178.5264 
    1191.6295 1309.6388 1335.4946 1337.5102 1378.6578 1390.6854 1420.7225 1469.731 
    1515.8456 1531.8405 1537.6416 1539.6572 1543.8768 1559.8718 1649.8221 1694.8325 
    1698.8737 1726.9049 1736.843 1766.7842 1768.7998 1808.8754 1836.9066 1940.9354 
    1955.9537 1959.0473 1975.0422 1987.0785 2003.0734 2010.9443 2038.9755 2158.0313 
    2166.029 2174.0262 2211.1046 2225.1202 2239.1358 2283.1807 2299.1756 2435.1587 
    2455.1849 2457.2005 2470.0512 2472.0668 2483.2161 2485.2317 2486.0461 2488.0617 
    2536.1502 2538.1658 2624.3295 2634.3561 2650.351 2652.3607 2662.3873 2678.3822 
    2707.4168 2807.3145 2914.5062 3013.3243 3094.6246 3110.6195 3143.4852 3159.4801 
    3161.4957 3171.5164 3173.532 3187.5113 3189.5269 3217.4968 3219.5125 3245.5281 
    3247.5437 3309.7265 3325.7214 3337.7577 3353.7526 3900.8108 3928.842 4475.1011 
    4475.2669 4491.2618 4503.2981 4519.293 4718.2343 4746.2655 5152.3372 5168.3321 
    5180.3684 5196.3633 5557.8751 5573.87 /;
our @porcineMajor=qw/515.3306 842.51 870.5412 1045.5642 1126.5645 1420.7225 1531.8405 
    1940.9354 2003.0734 2211.1046 2225.1202 2239.1358 2283.1807 2299.1756 2678.3822 
    2807.3145 2914.5062 3094.6246 3337.7577 3353.7526/;
our @bovine=qw/259.19 362.2 632.31 658.38 804.41 905.5 1019.5 1110.55 1152.57 1432.71 
    1494.61 2162.05 2192.99 2272.15 2551.24 4550.12/;

for my $i (0..$#weight){
    my $nmods;
    my $modColumn=td({align=>'right'},$weight[$i]);
    my $detail;
    my $serv = CIPRO::Service::Perms->new;
    my @arrays = $serv->searchByCiproIdAndMw($ID,$weight[$i]);
    foreach my $array (@arrays){
        $array->[0]=~s/\\\./\./g;
        if($array->[0]=~$ID){
            $seq=$array->[1];
            last;
        }
    }
    $dif=sprintf("%.4f",$weight[$i]-$arg[$i]);
    for my $j (@mkind){
        $nmods=$mods[$i]=~s/$j//g;
	if($nmods>0){
            $modColumn.=td($nmods);
	}else{
	    $modColumn.=td('&nbsp;');
	}
    }
    $modColumn.=td($seq);
    if(@contam>0){
        my (@c,@w);
        $contamDetail='none';
        for $k (@contflag){
            if($k=~m/Keratin/){
                for $l (@keratin){
                    if(abs($l-$arg[$i])<=$acc){
                        push @c,'keratin';
                        push @w,$l;
                        last;
                    }
                }
            }
            if($k=~m/Major/){
                for $l (@porcineMajor){
                    if(abs($l-$arg[$i])<=$acc){
                        push @c,'trypsin (porcine)';
                        push @w,$l;
                        last;
                    }
                }
            }elsif($k=~m/All/){
                for $l (@porcineAll){
                    if(abs($l-$arg[$i])<=$acc){
                        push @c,'trypsin (porcine)';
                        push @w,$l;
                        last;
                    }
                }                        
            }elsif($k=~m/Bovine/){
                for $l (@bovine){
                    if(abs($l-$arg[$i])<=$acc){
                        push @c,'trypsin (bovine)';
                        push @w,$l;
                        last;
                    }
                }
            }
        }
        if(@c>0){
            $detail=$c[0].' MW: '.$w[0];
            for $k (1..$#c){
                $detail.=', '.$c[$k].' MW: '.$w[$k];
            }
            $contamDetail=font({color=>'red'},$detail);
        }
        $content.=Tr(td({align=>'right'},$weight[$i]),
                    td({align=>'right'},$arg[$i]),
                    td({align=>'right'},sprintf("%d",calcScore($weight[$i],$arg[$i]))),
                    td({align=>'right'},$dif),
                    td({align=>'center'},$contamDetail));
    }else{
        $content.=Tr(td({align=>'right'},$weight[$i]),
                    td({align=>'right'},$arg[$i]),
                    td({align=>'right'},sprintf("%d",calcScore($weight[$i],$arg[$i]))),
                    td({align=>'right'},$dif));    
    }
    $modContent.=Tr($modColumn);
}

if(@contam>0){
    $frame=table({-bgcolor=>'#e0f0ff',-border=>1},Tr(th('Weight'),th('Query'),th('Score'),th('Difference'),th('Suspicious contaminants')),$content);
}else{
    $frame=table({-bgcolor=>'#e0f0ff',-border=>1},Tr(th('Weight'),th('Query'),th('Score'),th('Difference')),$content);
}
$modFrame=table({-bgcolor=>'#e0f0ff',-border=>1},
    Tr(th('Weight'),th('n'),th('o'),th('p'),th('a'),th('c'),th('m'),th('e'),th('Peptide sequence')),$modContent);

$originalID=$ID;
$ID=~s/CIPRO//;
$ID=~s/x//g;
my $serv = CIPRO::Service::Seq->new;
my @list = $serv->searchByCiproId($ID);

@tmpSeq=split / */,$list[0][0];
for $i (0..$#tmpSeq){
    $proteinSeq.=$tmpSeq[$i];
    if(($i+1)%60==0){
        $proteinSeq.=br;
    }
}

$summary.='#matched fragments: '.@weight.br;
$summary.='pI: '.$pi.br;
$summary.='score: '.sprintf("%.4f",$score).br;
$summary.='molecular weight: '.$mw.' kDa'.br;

sub calcScore($$){
    my ($w,$arg)=@_;
    my $score;
    
    if($w==$arg){
        $score=1000000;
    }else{
        $score=100/abs($w-$arg);
    }
    return $score;
}


print start_html(-title=>'PerMS Result',-style=>'./css/full.css'),
h2("Result: CIPRO$originalID"),
h4('Summary'),
$summary,hr,
h4('Mass Difference'),
$frame,hr,
h4('Modifications'),
$modFrame,hr,
h4("CIPRO$originalID Protein Sequence"),
$proteinSeq,hr,
end_html;
