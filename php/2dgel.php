<?php
require("dbinfo.php");
$id = $_GET['id'];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$sds = "select results_id,seqno,filename from picture where results_id in (select id from results where program_type_id = 5 AND basic_info_id = (select id from basic_info where cipro='$id' AND deletedate IS NULL limit 1) AND deletedate IS NULL) AND deletedate IS NULL ORDER BY seqno";

$gel = pg_query($sds);

$all_items = array();
while ($row = @pg_fetch_assoc($gel)){
  //  $item = array ( "results_id" => $row["results_id"],
  //		  "seqno" => $row["seqno"],
  //		  "filename" => $row["filename"]);
  //$url = 'http://cipro.ibio.jp/~ueno/2.5/thumbnail.cgi?results_id='.$row["results_id"].'&seqno='.$row["seqno"];
  $url = 'thumbnail.cgi?results_id='.$row["results_id"].'&seqno='.$row["seqno"];
  $item = array ( "id" => preg_replace('/.+\|(\w+)(\d+)-(\d+).png/', '$1 [pH${2}-$3]', $row["filename"]),
		  "acc" => preg_replace('/(.+)\|(\w+)(\d+)-(\d+).png/', '$1', $row["filename"]),
		  "gel" => preg_replace('/.+\|(\w+)(\d+)-(\d+).png/', '$1${2}-$3', $row["filename"]),
		  "url" => $url);
  array_push($all_items, $item);
}

$tasks = array( "identifier" => 'id',
		"items" => $all_items);
print(json_encode($tasks));
?>

