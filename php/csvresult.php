<?php
require("dbinfo.php");
//$ids = stripslashes($_GET['ids']);
$keyword = $_GET['keyword'];
$lf = $_GET['lf'];
$lt = $_GET['lt'];
$mwf = $_GET['mwf'];
$mwt = $_GET['mwt'];
$pif = $_GET['pif'];
$pit = $_GET['pit'];
$est = explode(',', $_GET['est']);
$evalue = $_GET['evalue'];
$acc = $_GET['acc'];
$mim = $_GET['mim'];
$contents = explode(',', $_GET['content']);

$seq = preg_replace('/\s/','',$_GET['seq']);
$prog = $_GET['prog'];
$filter = $_GET['filter'];
$matrix = $_GET['matrix'];

$pattern = $_GET['grep'];
$peptidemass = explode(',',$_GET['pmf']);
$accuracy = $_GET['accuracy'];
$ptm = $_GET['ptm'];
$sh = $_GET['sh'];
$miss = $_GET['miss'];
$co = $_GET['contami'];
$enzyme = $_GET['enzyme'];

$annotf = $_GET['annotf'];
$annott = $_GET['annott'];
$annotuser = $_GET['annotuname'];

$hinvdesc = $_GET['hinvdesc'];
$hinvcategory = $_GET['hinvcategory'];

function quote($id){
  return "'".$id."'";
}

function suf($str){
  return $str.":*";
}

if($seq){
  $xml = `echo $seq | blastall -d ../cipro -a2 -m7 -p $prog -F $filter -M $matrix`;
  $xmldom = new DOMDocument();
  $xmldom->loadXML( $xml );
  $hits = $xmldom->getElementsByTagName("Hit");
  $blast = array();
  foreach($hits as $hit){
    $hitacc = $hit->getElementsByTagName("Hit_accession");
    $hitevalue = $hit->getElementsByTagName("Hsp_evalue");
    $blast[$hitacc->item(0)->nodeValue] = $hitevalue->item(0)->nodeValue;
  }
  $ids = implode(",",array_map("quote",array_keys($blast)));
}

$connection=pg_connect("host=$hostspec dbname=$database user=$username password=$password");

$annotation = "array(select cname || ':::' || uname || ':::' || date(createdate) || ':::' || coalesce(xpath('//@id|//@name',evidence)::text,'') from annotname where basic_info_id = basic_info.id";
if($annotuser){
  $annotation .= " AND uname = '$annotuser'";
}
$query = "select basic_info.cipro,basic_info.eflag,unnest($annotation)) as annot,array(select program_type_id || ':' || COALESCE(sub_type_info_id,0) from results where basic_info_id = basic_info.id) as content,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL";

$ciproids = " AND basic_info.cipro in ($ids)";

$keyword = preg_replace('/\s\s/','\s',$keyword);
$keyword = implode(" & ", array_map("suf",preg_split('/\s+/', $keyword)));
$keyword = preg_replace('/& OR:\* &/',' | ',$keyword);
//$keyword = preg_replace("/'/","''''",$keyword);
//$keyword = preg_replace('/\/','\\\\\\\\',$keyword);
$keyword = preg_replace('/ \-/',' \!',$keyword);

$text = " AND basic_info.cipro in (select id from docs where vector @@ to_tsquery('$keyword') order by rank_cd(vector,to_tsquery('$keyword')))";
$length = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE length(seq) BETWEEN ".$lf." AND ".$lt." AND deletedate is null)";
$mw = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE mw BETWEEN ".$mwf." AND ".$mwt." AND deletedate is null)";
$pi = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE pi BETWEEN ".$pif." AND ".$pit." AND deletedate is null)";

$annot;
if($annotf == 0 && $annott == 0){
  $annot = " AND basic_info.id not in (SELECT basic_info_id FROM annotname)";
}else{
  $annot = " AND basic_info.id in (SELECT basic_info_id FROM (SELECT basic_info_id,count(basic_info_id) FROM annotname GROUP BY basic_info_id) AS annot WHERE count BETWEEN ".$annotf." AND ".$annott.")";
}
$uname = " AND basic_info.id in (SELECT basic_info_id FROM annotname WHERE uname = '$annotuser')";

$hinvc = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 10 AND (xpath('/HinvAutoannotation/category/text()' ,data))[1]::text = '$hinvcategory' AND deletedate is null))";
$hinvd = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 10 AND (xpath('/HinvAutoannotation/description/text()' ,data))[1]::text ~ '$hinvdesc' AND deletedate is null))";

$selest = array();
$specify = array();
foreach($est as $i){
  array_push($selest, "cast(((xpath('/res/detail/$i/text()', res))[1]::text) as double precision)");
  array_push($specify,"cast(((xpath('/res/detail/$i/text()', res))[1]::text) as double precision) > 0");
}
$totest = "cast(((xpath('/res/sum/text()', res))[1]::text) as double precision)";
$sp = implode(" AND ",$specify);
$sel = implode("+",$selest);
$estselect = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE TRUE AND program_type_id = 2 AND sub_type_info_id = 8 AND ".$sp." AND (".$sel.")/(".$totest.") > 0.9 AND (".$totest." != 0) AND deletedate is null)";

//$acc = "NP_001030292";
$homolog = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE deletedate IS NULL AND sub_type_info_id = 4 AND program_type_id = 1 AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < 1.0E".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])";
//$mim = "602054";
//$evalue = -1;
$omim = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 1 AND xid = '".$mim."' AND cast((xpath('/omim/evalue/text()' ,data))[1]::text as double precision) < 1.0E".$evalue." AND deletedate is null))";

//support ^$[].+*
$grep = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE seq ~ '$pattern' AND deletedate is null)";
$overlap = array();
for($i = 0;$i < sizeof($peptidemass);$i++){
  $lower = $peptidemass[$i] - $accuracy;
  $upper = $peptidemass[$i] + $accuracy; 
  array_push($overlap,"x between $lower and $upper");
}
$overlaps = implode(" OR ",$overlap);
//$pmf = "select basic_info_id, count(x) from (select basic_info_id,unnest(s || o) as x from pmf) as t where $overlaps  group by basic_info_id order by count desc"; 
$pmf = " AND basic_info.id in (select basic_info_id from (select basic_info_id,unnest(s || o) as x from pmf) as t where $overlaps group by basic_info_id)"; 


for($i = 0; $i < sizeof($contents); $i++){
  switch ($contents[$i]){
  case 'BLASTP':
    $content_blastp = " AND basic_info.id in (select basic_info_id from results where program_type_id = 1 AND sub_type_info_id = 1 AND deletedate is null)";
    break;
  case 'EST':
    $content_est = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 8 AND deletedate is null)";
    break;
  case '2D-PAGE':
    $content_sds = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 9 AND deletedate is null)";
    break;
  case 'Microarray':
    $content_array = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 10 AND deletedate is null)";
    break;
  case 'MS/MS':
    $content_msms = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 22 AND deletedate is null)";
    break;
  case 'MODELLER':
    $content_modeller = " AND basic_info.id in (select basic_info_id from results where program_type_id = 3 AND sub_type_info_id = 12 AND deletedate is null)";
    break;
  case 'WoLF PSORT':
    $content_wolfpsort = " AND basic_info.id in (select basic_info_id from results where program_type_id = 4 AND sub_type_info_id = 14 AND deletedate is null)";
    break;
  case '3DPL':
    $content_3dpl = " AND basic_info.id in (select basic_info_id from results where program_type_id = 6  AND deletedate is null)";
    break;
  case 'TMHMM':
    $content_tmhmm = " AND basic_info.id in (select basic_info_id from results where program_type_id = 7 AND sub_type_info_id = 21 AND deletedate is null)";
    break;
  case 'PSIPRED':
    $content_psipred = " AND basic_info.id in (select basic_info_id from results where program_type_id = 9 AND sub_type_info_id = 18 AND deletedate is null)";
    break;
  case 'NetPhos':
    $content_netphos = " AND basic_info.id in (select basic_info_id from results where program_type_id = 9 AND sub_type_info_id = 19 AND deletedate is null)";
    break;
  case 'InterProScan':
    $content_iprscan = " AND basic_info.id in (select basic_info_id from results where program_type_id = 9 AND sub_type_info_id = 24 AND deletedate is null)";
    break;
  case 'OMIM':
    $content_omim = " AND basic_info.id in (select basic_info_id from results where program_type_id = 10 AND deletedate is null)";
    break;
  }
}

if($seq && count($blast)){
  $query .= $ciproids;
}
if($_GET['keyword']){
  $query .= $text;
}
if(isset($lf) && isset($lt)){
  $query .= $length;
}
if(isset($mwf) && isset($mwt)){
  $query .= $mw;
}
if(isset($pif) && isset($pit)){
  $query .= $pi;
}
if($annotuser){
  $query .= $uname;
}
if(isset($annotf) && isset($annott)){
  $query .= $annot;
}
if($hinvcategory){
  $query .= $hinvc;
}
if($hinvdesc){
  $query .= $hinvd;
}

if($_GET['est']){
  $query .= $estselect;
}
if($acc){
  $query .= $homolog;
}
if($mim){
  $query .= $omim;
}
if($pattern){
  $query .= $grep;
}
if($_GET['pmf']){
  $query .= $pmf;
}

if($content_blastp){
  $query .= $content_blastp;
}
if($content_est){
  $query .= $content_est;
}
if($content_sds){
  $query .= $content_sds;
}
if($content_array){
  $query .= $content_array;
}
if($content_msms){
  $query .= $content_msms;
}
if($content_modeller){
  $query .= $content_modeller;
}
if($content_wolfpsort){
  $query .= $content_wolfpsort;
}
if($content_3dpl){
  $query .= $content_3dpl;
}
if($content_tmhmm){
  $query .= $content_tmhmm;
}
if($content_psipred){
  $query .= $content_psipred;
}
if($content_netphos){
  $query .= $content_netphos;
}
if($content_iprscan){
  $query .= $content_iprscan;
}
if($content_omim){
  $query .= $content_omim;
}

if($_GET["random"]){
  $query .= " order by random()";
}

$query .= ";";
$base = pg_query($query);

$hpf = "(SELECT basic_info_id FROM results WHERE program_type_id = ".$programtype_id." AND cast((xpath('/res/DevelopmentalHPFStart/text()' , res))[1]::text as double precision ) = ".$start." AND cast((xpath('/res/DevelopmentalHPFEnd/text()' , res))[1]::text as double precision ) = ".$end." AND deletedate IS NULL)";



header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=searchresult.csv');
if($seq && count($blast)){
  print "#ID,Protein Name,Annotator,Date,Evidence,Length,MW,pI,E-value\n";
}else{
  print "#ID,Protein Name,Annotator,Date,Evidence,Length,MW,pI\n";
}
while ($row = @pg_fetch_assoc($base)){  
  if($row["cipro"] != "1"){
    $annot = preg_split('/:::/', $row["annot"]);
    //$annot = preg_replace('/^{|}$/','', preg_replace('/\\\/','',preg_replace('/\\\"/','',$row["annot"])));
    if($seq && count($blast)){
      echo $row["cipro"],",\"",$annot[0],"\",",$annot[1],",",$annot[2],",",preg_replace('/{|}/','"',$annot[3]),",",intval($row["length"]),",",floatval($row["mw"]),",",floatval($row["pi"]),",",$blast[$row["cipro"]],"\n";
      //echo $row["cipro"],",",preg_replace('/","/',' AND ',$annot),",",intval($row["length"]),",",floatval($row["mw"]),",",floatval($row["pi"]),",",$blast[$row["cipro"]],"\n";
    }else{
      echo $row["cipro"],",\"",$annot[0],"\",",$annot[1],",",$annot[2],",",preg_replace('/{|}/','"',preg_replace('/"/','',$annot[3])),",",intval($row["length"]),",",floatval($row["mw"]),",",floatval($row["pi"]),"\n";
      //echo $row["cipro"],",",preg_replace('/","/',' AND ',$annot),",",intval($row["length"]),",",floatval($row["mw"]),",",floatval($row["pi"]),"\n";
    }
  }
}

?>

