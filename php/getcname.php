<?php
require("dbinfo.php");

$query = "SELECT id,sid,uname,cname,DATE(createdate) FROM annotname WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$cipro' AND deletedate IS NULL LIMIT 1)";
if($sid){
  $query .= " AND sid = '$sid' "; 
}
//$query .= "order by rating";
$comment = pg_query($query);

$all_items = array();
while ($row = @pg_fetch_assoc($comment)){
  $item = array ( "id" => $row["id"],
		  "sid" => $row["sid"],
		  "uname" => $row["uname"],
                  "cname" => $row["cname"],
		  "label" => $row["cname"].' by '.$row["uname"].' at '.$row["date"],
		  "rating"=>intval(isset($row["rating"])?$row['rating']:NULL),
		  // can be severe bug
		  "date" => $row["date"]);
  array_push($all_items, $item);
}

function datecmp($a, $b) {
  return ($a['date']==$b['date'])? 0:
  	 ($a['date']> $b['date'])? -1 : 1;
}
usort ($all_items, "datecmp");

$tasks = array(//"identifier" => 'sid',
	       "identifier" => 'id',
	       "label" => "label",
	       "items" => $all_items);
print(json_encode($tasks));
?>
