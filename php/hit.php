<?php
$img = imagecreatetruecolor(101, 5);
$white = imagecolorallocate($img, 255, 255, 255);
$red = imagecolorallocate($img, 255, 0, 0);
$bred = imagecolorallocate($img, 255, 100, 100);
$black = imagecolorallocate($img, 0, 0, 0);
$grey = imagecolorallocate($img, 211, 211, 211);

ImageColorTransparent($img, $white);
imagefill($img, 0, 0, $white);
imageline($img, 0, 2, 100, 2, $grey);
imagesetthickness($img, 1);

$range = explode(',',$_GET['range']);
for($i = 0; $i < sizeof($range); $i++){
  $pos = explode(':',$range[$i]);
  imagerectangle($img,intval($pos[0]), 0, intval($pos[1]), 4, $red);
  imagefilledrectangle($img,intval($pos[0]), 1, intval($pos[1]), 3, $bred);
}

//imagerectangle($img,intval($range[0]), 0, intval($range[1]), 8, $red);
//imagefilledrectangle($img,intval($range[0]), 1, intval($range[1]), 7, $bred);

/*
$from = explode(',',$_GET['from']);
$to = explode(',',$_GET['to']);
for($i = 0; $i < sizeof($from); $i++){
  imagerectangle($img,intval($from[$i]), 0, intval($to[$i]), 8, $red);
  imagefilledrectangle($img,intval($from[$i]), 1, intval($to[$i]), 7, $bred);
}
*/
//imageantialias($img, true);
header("Content-type: image/png");
imagepng($img);
imagedestroy($img);
?>
