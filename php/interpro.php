<?php
require("dbinfo.php");
$id = $_GET["id"];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$iprscan = pg_query("select res from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 9 AND sub_type_info_id = 24  AND deletedate is null;");


$all_items = array();
$i = 0;
$xmldom = new DOMDocument();
while ($row = @pg_fetch_assoc($iprscan)){
  $xmldom->loadXML( $row["res"] );
  $hits = $xmldom->getElementsByTagName("interpro");
  $proteins = $xmldom->getElementsByTagName("protein");
  $length;
  foreach($proteins as $protein){ 
    $length = $protein->getAttribute("length");
  }
  foreach($hits as $hit){
    $interpro_name = $hit->getAttribute("name");
    $interpro_id = $hit->getAttribute("id");
    $interpro_type = $hit->getAttribute("type");
    
    $children = array();

    $goes = $hit->getElementsByTagName("classification");
    $ga = array();
    foreach($goes as $go){
      $go_id = $go->getAttribute("id");
      $go_desc = $go->getElementsByTagName("description");
      $desc = $go_desc->item(0)->nodeValue;
      $g = array ( "type" => "go","id" => $go_id,"name" => $desc,"db"=> "GO","no" => ++$i);
      array_push($ga, $g);
      //array_push($children, array('_reference' => $go_id));
      array_push($children, array('_reference' =>$i));
      array_push($all_items, $g);    
    }
    $matches = $hit->getElementsByTagName("match");
    $ma = array();
    foreach($matches as $match){
      $match_id = $match->getAttribute("id");
      $match_name = $match->getAttribute("name");
      $match_dbname = $match->getAttribute("dbname");
      $locations = $match->getElementsByTagName("location");
      $range = array();
      $score = array();
      foreach($locations as $location){
	$start = intval($location->getAttribute("start")*100/$length);
	$end = intval($location->getAttribute("end")*100/$length);
	array_push($range, $start.':'.$end);
	array_push($score,$location->getAttribute("score"));
      //$evidence = $location->getAttribute("evidence");
      }
      $m = array ( "type" => "match","id" => $match_id,"name" => $match_name, "no" => ++$i,
		   //"range" => intval($location_start*100/$length).",".intval($location_end*100/$length),//"check" => true,
		   "range" => implode(",",$range),
		   "db" => $match_dbname,
		   "score" => implode(",",$score));
      array_push($ma, $m);
      //array_push($children, array('_reference' => $match_id));
      array_push($children, array('_reference' => $i));
      array_push($all_items, $m);
    }
    $interpro = array ( "type" => "interpro","id" => $interpro_id, "no" => ++$i,
			"name" => $interpro_name,"db"=>"InterProScan",
			"children" => $children
			);
    array_push($all_items, $interpro);
  }
}

$tasks = array( //"identifier" => 'id',
	       "identifier" => 'no',
		"label" => 'db',
	       "items" => $all_items);
print(json_encode($tasks));

?>

