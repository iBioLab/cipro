<?php
require("dbinfo.php");
$id = $_GET["id"];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$omim = pg_query("select xid,unnest(xpath('/omim/disorder/text()' , data))::text as disorder,unnest(xpath('/omim/evalue/text()' , data))::text as evalue,unnest(xpath('/omim/location/text()' , data))::text as location,unnest(xpath('/omim/symbols/text()' , data))::text as symbols from reference_db where external_db_id = 1 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null;");

/*
$omim  disorder, evalue, location, symbols
*/

$all_items = array();
$count = 1;
while ($row = @pg_fetch_assoc($omim)){
  $item = array ( "id"=>$count++,"type" => "mim","xid" => $row["xid"],"disorder" => $row["disorder"],"evalue" => $row["evalue"],
		  "location" => $row["location"],"symbols" => $row["symbols"]);
  array_push($all_items, $item);
}
$tasks = array( "identifier" => 'id',
                "items" => $all_items);
print(json_encode($tasks));

?>

