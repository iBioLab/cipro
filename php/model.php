<?php
//$img = imagecreatetruecolor(101, 5);
$img = imagecreatetruecolor(111, 5);
$white = imagecolorallocate($img, 255, 255, 255);
$red = imagecolorallocate($img, 255, 0, 0);
$bred = imagecolorallocate($img, 255, 100, 100);
$black = imagecolorallocate($img, 0, 0, 0);
//$grey = imagecolorallocate($img, 211, 211, 211);
$grey = imagecolorallocate($img, 111, 111, 111);

ImageColorTransparent($img, $white);
imagefill($img, 0, 0, $white);
//imageline($img, 0, 2, 100, 2, $grey);
imagesetthickness($img, 1);

$range = explode(',',$_GET['range']);
$posi = array();
for($i = 0; $i < sizeof($range); $i++){
  $pos = explode(':',$range[$i]);
  $pos[0] += 5;
  $pos[1] += 5;
  array_push($posi,$pos[0]);
  array_push($posi,$pos[1]);
  //imagerectangle($img,$pos[0], 0, $pos[1], 4, $red);
  //imagefilledrectangle($img,$pos[0], 1, $pos[1], 3, $bred);
}

sort($posi); 
$last = sizeof($posi) - 1;
if($_GET['direction'] == 'f'){
  imagepolygon($img, array($posi[$last]+5,2,$posi[$last],0,$posi[$last],4), 3, $red);
  imagefilledpolygon($img, array($posi[$last]+5,2,$posi[$last],0,$posi[$last],4), 3, $bred);
}else if($_GET['direction'] == 'r'){
  imagepolygon($img, array($posi[0]-5,2,$posi[0],0,$posi[0],4), 3, $red);
  imagefilledpolygon($img, array($posi[0]-5,2,$posi[0],0,$posi[0],4), 3, $bred);
}

for($i = 0; $i < sizeof($posi) - 2; $i++){
  if($i %  2){
    if($_GET['direction'] == 'f'){
      $y = 0;
    }else if($_GET['direction'] == 'r'){
      //$y = 4;
      $y = 0;
    }
    imageline($img, $posi[$i], 2, 0.5*($posi[$i+1]-$posi[$i])+$posi[$i], $y, $grey);
    imageline($img, 0.5*($posi[$i+1]-$posi[$i])+$posi[$i], $y, $posi[$i+1], 2, $grey);
  }
}

for($i = 0; $i < sizeof($range); $i++){
  $pos = explode(':',$range[$i]);
  $pos[0] += 5;
  $pos[1] += 5;
  array_push($posi,$pos[0]);
  array_push($posi,$pos[1]);
  imagerectangle($img,$pos[0], 0, $pos[1], 4, $red);
  imagefilledrectangle($img,$pos[0], 1, $pos[1], 3, $bred);
}



//imagefilledpolygon($img, array(x1,2,x2,0,x2,4), 4, $bred);


//imagerectangle($img,intval($range[0]), 0, intval($range[1]), 8, $red);
//imagefilledrectangle($img,intval($range[0]), 1, intval($range[1]), 7, $bred);

/*
$from = explode(',',$_GET['from']);
$to = explode(',',$_GET['to']);
for($i = 0; $i < sizeof($from); $i++){
  imagerectangle($img,intval($from[$i]), 0, intval($to[$i]), 8, $red);
  imagefilledrectangle($img,intval($from[$i]), 1, intval($to[$i]), 7, $bred);
}
*/
//imageantialias($img, true);
header("Content-type: image/png");
imagepng($img);
imagedestroy($img);
?>
