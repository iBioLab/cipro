<?php
require("dbinfo.php");
$id = $_GET["id"];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$ciona = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_def/text()' , res)::text[] > ARRAY[''] AND deletedate is null;");

$human = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1 AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");

$mouse = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Mus musculus]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Mus musculus]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");

$hagfish = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Eptatretus burgeri]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Eptatretus burgeri]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
$lamprey = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Petromyzon marinus]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Petromyzon marinus]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
$pacific_transparent_sea_squirt = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Ciona savignyi]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Ciona savignyi]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
$common_sea_squirt = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Halocynthia roretzi]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Halocynthia roretzi]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
$amphioxus = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Branchiostoma floridae]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Branchiostoma floridae]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
$sea_urchin = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Strongylocentrotus purpuratus]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Strongylocentrotus purpuratus]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
$nematode = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Caenorhabditis elegans]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Caenorhabditis elegans]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");

$fly = pg_query("select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Drosophila melanogaster]\")]/Hit_def/text()' , res))[1]::text as def from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND cast((xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[contains(Hit_def/text(),\"[Drosophila melanogaster]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");

$faba = pg_query("select unnest(xpath('/res/data/gene_name/text()' , res))::text as name from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 6 AND deletedate is null group by name;");

//$xseq = pg_query("select distinct basic_info.cipro from basic_info left join seq_info on basic_info_id = basic_info.id WHERE sha256 in (select sha256 from basic_info left join seq_info on basic_info_id = basic_info.id where  basic_info.cipro = '$id') AND basic_info.cipro !='$id' AND basic_info.deletedate IS NULL;");

$omim = pg_query("select unnest(xpath('/omim/symbols/text()' , data))::text as symbols from reference_db where external_db_id = 1 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

//$pubmed = pg_query("select xid as pmid, array(select lastname || ' ' || firstname from (select unnest(xpath('//Author/Initials/text()',data))::text as lastname,unnest(xpath('//Author/LastName/text()',data))::text as firstname from reference_db where external_db_id = 2 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) ) as temp) as name,(xpath('//PubDate/Year/text()',data))[1]::text as year,(xpath('//ArticleTitle/text()',data))[1]::text as title,(xpath('//ISOAbbreviation/text()',data))[1]::text as journal,(xpath('//JournalIssue/Volume/text()',data))[1]::text as volume,(xpath('//MedlinePgn/text()',data))[1]::text as page from reference_db where external_db_id = 2 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

//$aniseed = pg_query("select (xpath('/aniseed/Gene_name_by_manual_annotation/text()' , data))[1]::text as mname,(xpath('/aniseed/Gene_name_inferred/text()' , data))[1]::text as iname from reference_db where external_db_id = 3 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

$aniseed = pg_query("select unnest(array[(xpath('/aniseed/Gene_name_by_manual_annotation/text()' , data))[1]::text, (xpath('/aniseed/Gene_name_inferred/text()' , data))[1]::text]) as name from reference_db where external_db_id = 3 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

$jgi1 = pg_query("select unnest(xpath('/jgiv1/jgiv1_gene_name/text()' , data))::text as name from reference_db where external_db_id = 6 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

$ensembl = pg_query("select unnest(xpath('/ensembl/ensembl_gene_name/text()' , data))::text as name from reference_db where external_db_id = 8 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

//$kg = pg_query("select (xpath('/kyotograil/kyotograil_gene_name/text()' , data))[1]::text as name from reference_db where external_db_id = 5 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1));");

$iprscan = pg_query("select res from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 9 AND sub_type_info_id = 24  AND deletedate is null;");

$all_items = array();
$names = array();

/*
while ($row = @pg_fetch_assoc($pubmed)){
  $names = explode(',',preg_replace('/\{|\}|\"/','', $row["name"]));
  $authors = array();
  foreach($names as $i){
    list($lastname,$fastname) = preg_split('/\s+/', $i);
    array_push($authors,implode(".",str_split($lastname)).'. '.$fastname);
  }
  $item = array ( "type" => "pubmed","pmid" => $row["pmid"],"year" => intval($row["year"]),
		  "volume" => $row["volume"],"page" => $row["page"],
		  "journal" => $row["journal"],"title" => $row["title"],"name" => $authors);
  array_push($all_items, $item);
}
*/
/*
while ($row = @pg_fetch_assoc($aniseed)){
  $item = array ( "type" => "aniseed","mname" => $row["mname"],"iname" => $row["iname"]);
  array_push($all_items, $item);
}
*/
while ($row = @pg_fetch_assoc($aniseed)){
  array_push($names, $row["name"]);
}

while ($row = @pg_fetch_assoc($jgi1)){
  array_push($names, $row["name"]);
}

while ($row = @pg_fetch_assoc($ensembl)){
  array_push($names, $row["name"]);
}

while ($row = @pg_fetch_assoc($kg)){
  array_push($names, $row["name"]);
}

/*
$fabaitem = array();
while ($row = @pg_fetch_assoc($faba)){
  $item = array ( "image" => $row["image"],"thumbnail" => $row["thumbnail"],
		  "record" => $row["record"],"link" => $row["link"],
		  "slink" => $row["slink"],"id" => $row["id"],
		  "name" => $row["name"],"hpf" => $row["hpf"],
		  "tissue" => $row["tissue"],"locate" => $row["locate"]);
  array_push($fabaitem, $item);
}
array_push($all_items, array ( "type" => "faba","fabaitem" => $fabaitem));
*/
while ($row = @pg_fetch_assoc($faba)){
  array_push($names, $row["name"]);
}
/*
while ($row = @pg_fetch_assoc($xseq)){
  $item = array ( "type" => "xseq","name" => $row["cipro"]);
  array_push($all_items, $item);
}
*/

while ($row = @pg_fetch_assoc($ciona)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}

while ($row = @pg_fetch_assoc($human)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}

while ($row = @pg_fetch_assoc($mouse)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}

while ($row = @pg_fetch_assoc($hagfish)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}
while ($row = @pg_fetch_assoc($lamprey)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}
while ($row = @pg_fetch_assoc($pacific_transparent_sea_squirt)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}
while ($row = @pg_fetch_assoc($common_sea_squirt)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}
while ($row = @pg_fetch_assoc($amphioxus)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}
while ($row = @pg_fetch_assoc($sea_urchin)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}
while ($row = @pg_fetch_assoc($nematode)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}

while ($row = @pg_fetch_assoc($fly)){
  $name = preg_replace("/ \[.*/","",$row["def"]);
  array_push($names, preg_replace("/>.*/","",$name));
}

while ($row = @pg_fetch_assoc($omim)){
  array_push($names, $row["symbols"]);
}


$xmldom = new DOMDocument();
while ($row = @pg_fetch_assoc($iprscan)){
  $xmldom->loadXML( $row["res"] );
  $hits = $xmldom->getElementsByTagName("interpro");
  foreach($hits as $hit){
    $interpro_name = $hit->getAttribute("name");
    $interpro_id = $hit->getAttribute("id");
    $goes = $hit->getElementsByTagName("classification");
    $ga = array();
    foreach($goes as $go){
      $go_id = $go->getAttribute("id");
      $go_desc = $go->getElementsByTagName("description");
      $desc = $go_desc->item(0)->nodeValue;
      $g = array ( "type" => "go","id" => $go_id,"desc" => $desc);
      array_push($ga, $g);    
    }
    $matches = $hit->getElementsByTagName("match");
    $ma = array();
    foreach($matches as $match){
      $match_id = $match->getAttribute("id");
      $match_name = $match->getAttribute("name");
      $match_dbname = $match->getAttribute("dbname");
      $location = $match->getElementsByTagName("location");
      $location_start = $location->item(0)->getAttribute("start");
      $location_end = $location->item(0)->getAttribute("end");
      $location_score = $location->item(0)->getAttribute("score");
      $location_evidence = $location->item(0)->getAttribute("evidence");
      if($match_name != 'seg' && $match_name != 'no description'){
	array_push($names, $match_name);
      }
    }
    if($interpro_name != 'unintegrated'){
      array_push($names, $interpro_name);
    }
  }
}

sort($names);
foreach(array_unique($names) as $i){ 
  array_push($all_items,array("name" => $i));
}

$tasks = array( "label" => 'name',
		"items" => $all_items);
print(json_encode($tasks));

?>

