<?php
include 'php-ofc-library/open-flash-chart.php';
require("dbinfo.php");

$width = $_GET["width"];
$height = $_GET["height"];
$date = $_GET["date"];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

if($date){
  $annot = pg_query("select uname,count(uname),date(createdate) from annotname group by uname,date(createdate)");
}else{
  $annot = pg_query("select uname,count(uname) from annotname group by uname");
}
$N = count($annot);
$i = 0;
$annotdate = array();
$user = array();
while ($row = @pg_fetch_assoc($annot)){
  //$annotdata[$i] = 'null';
  $annotdata[$i] = $row["count"];
  $user[$i] = $row["uname"];
  $i++;
}

$chart = new graph();
//$chart->title('Expression Profile','{font-size: 10px; color: #778877; text-align: center}');
$chart->title(' ','{font-size: 10px; color: #778877; text-align: center}');

$chart->bg_colour = '#FFFFFF';

$xlabel = $user;

$chart->set_x_max( $N );
$chart->set_x_labels($xlabel);
$chart->set_x_label_style( 8, '0x000000',2 );

$chart->set_x_legend('Annotator',12,'#778877');

//$chart->set_data($estdata);

$maxval = array();
if($N){
  $annotmax = max($annotdata);
  array_push($maxval,$annotmax);
  $chart->set_data( $annotdata );
  //$chart->bar( 50, '#9933CC', 'Annotaion', 10 );
  $chart->bar( 50, '#CC3333', 'achievement', 10 );
}

$max = max($maxval);
rsort($annotdata);
if($max){
  //$chart->set_y_max($max);
  $chart->set_y_max($annotdata[1]);
}

$chart->set_y_legend('#Annotation',12,'#778877');

echo $chart->render();

?>
