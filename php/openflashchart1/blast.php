<?php
include 'php-ofc-library/open-flash-chart.php';
//#$path = '/current/conf';
//#set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require("../dbinfo.php");

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$evalue = pg_query("select case when a[i] = 0 or -log(10.0,a[i]) > 100 then '> 100' when -log(10.0,a[i]) > 90 then '> 90' when -log(10.0,a[i]) > 80 then '> 80' when -log(10.0,a[i]) > 70 then '> 70' when -log(10.0,a[i]) > 60 then '> 60' when -log(10.0,a[i]) > 50 then '> 50' when -log(10.0,a[i]) > 40 then '> 40' when -log(10.0,a[i]) > 30 then '> 30' when -log(10.0,a[i]) > 20 then '> 20' when -log(10.0,a[i]) > 10 then '> 10' else '<< 10' END as category, count (*) as qty from 
(select s1.a, generate_series(array_lower(s1.a,1),array_upper(s1.a,1)) as i from (select ((select (xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit/Hit_hsps/Hsp/Hsp_evalue/text()', res))::text[] from results WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL) AND program_type_id=(SELECT id FROM program_type WHERE type='BLAST' AND deletedate IS NULL) AND sub_type_info_id = (SELECT id FROM sub_type_info WHERE type='nr' AND deletedate IS NULL) AND deletedate IS NULL)::numeric[]) as a) as s1) as s2 group by category");


//$chart = new open_flash_chart();
$chart = new graph();
//$chart->title('BLASTP: Hits vs. -log(E-value)','{font-size: 10px; color: #778877; text-align: center}');
$chart->title(' ','{font-size: 10px; color: #778877; text-align: center}');

$labels = array( "> 100" => 10, "> 90" => 9,
                 "> 80" => 8, "> 70" => 7,
                 "> 60" => 6,"> 50" => 5,
                 "> 40" => 4, "> 30" => 3,
                 "> 20" => 2, "> 10" => 1,
                 "<< 10" => 0);
$xlabel = array( "<< 10", "> 10", "> 20",
                 "> 30", "> 40",
                 "> 50","> 60", 
                 "> 70","> 80",
		 "> 90","> 100");

$hits = array();
for ($i=0; $i <= 10; $i++) {
  $hits[] = 0;
}

while ($row = @pg_fetch_assoc($evalue)){
  $xlabel[$labels[$row["category"]]] = $row["category"];
  $hits[$labels[$row["category"]]] = intval($row["qty"]);
}


$chart->bg_colour = '#FFFFFF';
//$chart->x_axis_colour( '#808080', '#A0A0A0' );
//$chart->y_axis_colour( '#808080', '#A0A0A0' );
//$chart->line( 2, '0x80a033', '#Hits', 10 );
//$chart->area_hollow( 2, 3, 25, '0x80a033' );
$chart->area_hollow( 2, 3, 25, '#9933CC' );

//$chart->x_label_steps(1);
$chart->set_x_max( 11 );
//$x_axis = new x_axis();
//$x_axis->set_range( 0, 10, 1);

$chart->set_x_labels($xlabel);
//$chart->set_x_label_style( 8, '#9933CC' );
$chart->set_x_label_style( 8, '0x000000' );
//$x_labels = new x_axis_labels();
//$x_labels->set_labels( $xlabel );
//$x_axis->set_labels( $x_labels );
//$chart->set_x_axis( $x_axis );

$chart->set_x_legend('-log(E-value)',12,'#778877');
//$x_legend = new x_legend( '-log(E-value)' );
//$x_legend->set_style( '{font-size: 20px; color: #778877}' );
//$chart->set_x_legend( $x_legend );

//while ($row = @pg_fetch_assoc($evalue)){
//  $hits[$labels[$row["category"]]] = $row["qty"];
//  //  $links[] = "javascript:alert('hits: " . $random_hits[$i] . "')";
//}

$chart->set_data($hits);
//$line_dot = new line();
//$line_dot->set_values($hits);
//$chart->add_element( $line_dot );

$max = max($hits);
if($max){
  $chart->set_y_max( $max);
}else{
  $chart->set_y_max( 100 );
}
//$chart->y_label_steps( 10 );
//$y_axis = new y_axis();
//$y_axis->set_range( 0, 100 + $max, 10);
//$chart->set_y_axis( $y_axis );

$chart->set_y_legend('#Hits',12,'#778877');
//$y_legend = new y_legend( '#Hits' );
//$y_legend->set_style( '{font-size: 20px; color: #778877}' );
//$chart->set_y_legend( $y_legend );


//while ($row = @pg_fetch_assoc($evalue)){
//echo $row["category"];
//echo $row["qty"];
//}

//$x = new x_axis();
//$x->set_steps(1);

//echo $chart->toPrettyString(); 
echo $chart->render();

/*
$data = array();
$max = 0;
while($row = pg_fetch_array($evalue)){
    //intval関数でdownloadsの値を数値属性に変換
    $data[] = intval($row['downloads']);
}

//open_flash_chartオブジェクト作成
$chart = new open_flash_chart();
//タイトルの設定
$chart->set_title( new title( 'BLASTP: Hits vs. -log(E-value)' ) );
*/

/****
//dotオブジェクト作成
$d = new dot();
//グラフ上の点の色とサイズを設定
$d->colour('#9C0E57')->size(4);

//areaオブジェクト作成
$area = new area();
$area->set_width( 2 );
$area->set_default_dot_style($d);
$area->set_colour( '#C4B86A' );
$area->set_fill_colour( '#C4B86A' );
$area->set_fill_alpha( 0.7 );
$area->set_values( $data );

//チャートにareaオブジェクトをセット
$chart->add_element( $area );
****/

/*
//y_axisオブジェクト作成
$y_axis = new y_axis();
//Y軸のメモリ幅と最大値を設定
$y_axis->set_range( 0, 16000, 1000 );

//チャートにy_axisオブジェクトをセット
$chart->add_y_axis( $y_axis );

//JSONデータ作成
echo $chart->toPrettyString(); 
*/

/*
$flashChart = new graph();
// Sets height and width
//$flashChart->begin(400, 250);
// x Label
$labels = array( "> 100" => 0, "> 90" => 1,
                 "> 80" => 2, "> 70" => 3,
                 "> 60" => 4,"> 50" => 5,
                 "> 40" => 6, "> 30" => 7,
                 "> 20" => 8, "> 10" => 9,
                 "<< 10" => 10);

$xlabel = array( "> 100", "> 90",
                 "> 80", "> 70",
                 "> 60","> 50",
                 "> 40", "> 30",
                 "> 20", "> 10",
                 "<< 10");

foreach ($blasts as $evalue):
        $xlabel[$labels[$blast[0]["category"]]] = __($blast[0]["category"],true);
endforeach;
$flashChart->labels($xlabel);
// Title
$flashChart->title('BLASTP: Hits vs. -log(E-value)');

// Configure Grid style and legends
$flashChart->configureGrid(
        array(
                'x_axis' => array(
                        'step' => 1,
                        'legend' => '-log(E-value)',
                        ),
                'y_axis' => array(
                        'legend' => '#Hits',
                )
        )
);



$hits = array();
for ($i=0; $i < 10; $i++) {
    $hits[] = 0;
}

foreach ($blasts as $blast):
        $hits[$labels[$blast[0]["category"]]] = $blast[0]["qty"];
//      $links[] = "javascript:alert('hits: " . $random_hits[$i] . "')";
endforeach;

// Register each data set with its information.
$data = array(
        'Hits' => array(
                'color' => '#afe342',
                'font_size' => 11,
                'data' => $hits,
                'graph_style' => 'lines',
//              'links' => $links,
        )
);


$flashChart->setData($data);

// Set Ranges in the chart
$flashChart->setRange('y', 0, 100);
$flashChart->setRange('x', 0, 10);

// Show the graph
echo $flashChart->render();
*/
?>
