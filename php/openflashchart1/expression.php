<?php
include 'php-ofc-library/open-flash-chart.php';
require("../dbinfo.php");

//$est = pg_query("select xpath('/res/detail/*/text()', res)::text[] from results WHERE program_type_id = 2 AND sub_type_info_id = 8 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL)");
$est = pg_query("select t2.scores[i],xpath('//text()',t2.clones[i])::text as clones from (select t1.scores,t1.clones, generate_series(array_lower(t1.scores,1),array_upper(t1.scores,1)) as i from (select xpath('//clones//*',res) as clones,xpath('/res/detail/*/text()', res)::text[] as scores from results WHERE program_type_id = 2 AND sub_type_info_id = 8 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL) AND deletedate IS NULL) as t1) as t2;");

$array = pg_query("select xpath('/res/data/*/text()', res)::text[] from results WHERE program_type_id = 2 AND sub_type_info_id = 10 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL)");

$sds = pg_query("select xpath('/res/quantity/*/text()', res)::text[] from results WHERE program_type_id = 2 AND sub_type_info_id = 9 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL)");

$msms = pg_query("select xpath('/res/*/text()', res)::text[] from results WHERE program_type_id = 2 AND sub_type_info_id = 22 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL)"); 

$kh = array(34284, 33137, 25287, 33900, 23724, 20993, 22373, 3173, 26918, 35914, 15292, 5340, 1994, 10568, 13108, 33503, 19368);
$estN = array("KH" => $kh);
$model = "KH";
/*
my %total_spots_stage=qw/
  eg 154
  ga 91
  ne 100
  lv 339
  /;

my %total_spots_tissue=qw/
  nc 477
  ov 224
  /;
*/

/*
while ($row = @pg_fetch_assoc($est)){
  $col = explode(',',preg_replace('/\{|\}/','',$row["xpath"]));
  //print $col[16];
  $resest = count($col);
  for ($i=0; $i <= 16; $i++) {
    if($i == 12){
      $estdata[$i] = 'null';
    }
    if($i >= 12){
      $estdata[$i+1] = ($col[$i]*1000)/$estN[$model][$i];
    }else{
      $estdata[$i] = ($col[$i]*1000)/$estN[$model][$i];
    }
  }
}
*/
$count = 0;
$resest = pg_num_rows($est);
$estlinks = array();
while ($row = @pg_fetch_assoc($est)){
  //$col = explode(',',preg_replace('/\{|\}|\"/','',$row["clones"]));
  $clone = preg_replace('/,/','#',preg_replace('/\{|\}|\"/','',$row["clones"]));
  //print $col[16];
  //for ($i=0; $i <= 16; $i++) {
  if($count == 12){
    $estdata[$count] = 'null';
    array_push($estlinks,"javascript:clone('')");
  }
  if($count >= 12){
    $estdata[$count+1] = ($row["scores"]*1000)/$estN[$model][$count];
    array_push($estlinks,"javascript:clone('$clone')");
  }else{
    $estdata[$count] = ($row["scores"]*1000)/$estN[$model][$count];
    array_push($estlinks,"javascript:clone('$clone')");
  }
  //}
  $count++;
}

while ($row = @pg_fetch_assoc($array)){
  $col = explode(',',preg_replace('/\{|\}/','',$row["xpath"]));
  $resarray = count($col);
  for ($i=0; $i <= 17; $i++) {
      $arraydata[$i] = 'null';
  }
  $arraydata[0] = 0;
  $arraydata[1] = ($col[0]+$col[1]+$col[2]+$col[3]+$col[4]+$col[5])/6;
  $arraydata[2] = ($col[6]+$col[7]+$col[8])/3;
  $arraydata[3] = ($col[9]+$col[10]+$col[11]+$col[12])/4;
  $arraydata[4] = intval($col[13]);
  $arraydata[6] = intval($col[14]);
  $arraydata[9] = ($col[15]+$col[16]+$col[17])/3;
}

while ($row = @pg_fetch_assoc($sds)){
  $col = explode(',',preg_replace('/\{|\}/','',$row["xpath"]));
  $ressds = count($col);
  for ($i=0; $i <= 17; $i++) {
      $sdsdata[$i] = 'null';
  }
  $sdsdata[0] = $col[0]/1000;
  $sdsdata[1] = $col[1]/1000;
  $sdsdata[2] = ($col[2]+$col[3])/2000;
  $sdsdata[4] = $col[4]/1000;
  $sdsdata[12] = $col[5]/1000;
  $sdsdata[14] = $col[6]/1000;
}

while($row = @pg_fetch_assoc($msms)) {
  $col = explode(',', preg_replace('/\{|\}/', '', $row["xpath"]));
  $resmsms = count($col);
  for($i = 0; $i <= 17; $i++) {
    $msmsdata[$i] = 'null';
  }
  for($i = 1; $i <= 10; $i++) {
    $msmsvalue[$i-1] = $col[$i];
  }

  if(max($msmsvalue) >= 100) {
    $msmsdata[17] = ($msmsvalue[2]+$msmsvalue[3]) / 200;
    $msmsdata[13] = $msmsvalue[4]/100;
    $msmsdata[14] = $msmsvalue[5]/100;
    $msmsdata[15] = $msmsvalue[6]/100;
    $msmsdata[16] = $msmsvalue[7]/100;
    $msmsdata[11] = $msmsvalue[8]/100;
    $msmsdata[12] = $msmsvalue[9]/100;
  } else if (max($msmvalue) >= 10) {
    $msmsdata[17] = ($msmsvalue[2]+$msmsvalue[3]) / 20;
    $msmsdata[13] = $msmsvalue[4]/10;
    $msmsdata[14] = $msmsvalue[5]/10;
    $msmsdata[15] = $msmsvalue[6]/10;
    $msmsdata[16] = $msmsvalue[7]/10;
    $msmsdata[11] = $msmsvalue[8]/10;
    $msmsdata[12] = $msmsvalue[9]/10;
  } else {
    $msmsdata[17] = ($msmsvalue[2]+$msmsvalue[3]) / 2;
    $msmsdata[13] = $msmsvalue[4];
    $msmsdata[14] = $msmsvalue[5];
    $msmsdata[15] = $msmsvalue[6];
    $msmsdata[16] = $msmsvalue[7];
    $msmsdata[11] = $msmsvalue[8];
    $msmsdata[12] = $msmsvalue[9];
  }
}

$chart = new graph();
//$chart->title('Expression Profile','{font-size: 10px; color: #778877; text-align: center}');
$chart->title(' ','{font-size: 10px; color: #778877; text-align: center}');

$chart->bg_colour = '#FFFFFF';

$xlabel = array( 'eggs','cleaving embryos','gastrulae & neurulae','tailbud embryos',
		 'larvae','embryo mix','juvenile','juvenile 2','young adults','mature adult','gonad','testis',
		 'ovary','endostyle','neural complex','heart','blood cell','digestive gland');

//"eg","cl","gn","tb","lv","em","jv","jm","ad","ma","gd","ts","ov","es","nc","ht","bd","dg"

/*
$hits = array();
for ($i=0; $i <= 10; $i++) {
  $hits[] = 0;
}

while ($row = @pg_fetch_assoc($evalue)){
  $xlabel[$labels[$row["category"]]] = $row["category"];
  $hits[$labels[$row["category"]]] = intval($row["qty"]);
}
*/

//$chart->area_hollow( 2, 3, 25, '0x80a033' );

$chart->set_x_max( 18 );
$chart->set_x_labels($xlabel);
$chart->set_x_label_style( 8, '0x000000',2 );

$chart->set_x_legend('Developmental Stages & Adult Tissues',12,'#778877');

//$chart->set_data($estdata);

$maxval = array();
if($resest){
  $estmax = max($estdata);
  array_push($maxval,$estmax);
  $chart->set_data( $estdata );
  //$chart->bar( 50, '#9933CC', 'EST', 10 );
  $chart->bar( 50, '#CC3333', 'EST', 10 );
}
if($resarray){
  $arraymax = max($arraydata);
  $arraymin = min($arraydata);
  if(! $resest && ! $ressds && ! $resmsms){
    $chart->set_y_max($arraymax);
    $chart->set_y_min($arraymin);
    $chart->y_axis_colour( '#164166' );
    //$chart->set_y_legend( 'Microarray log([intensity]/[egg stage intensity])' ,12 , '#164166' );
    $chart->set_y_legend( 'Microarray (log ratio to egg)' ,12 , '#164166' );
  }else{
    //$chart->attach_to_y_right_axis(2);
    if(! $resest){
      $chart->attach_to_y_right_axis(1);
    }else{
      $chart->attach_to_y_right_axis(2);
    }
    $chart->set_y_right_max($arraymax);
    $chart->set_y_right_min($arraymin);
    $chart->y_right_axis_colour( '#164166' );
    //$chart->set_y_right_legend( 'Microarray log([intensity]/[egg stage intensity])' ,12 , '#164166' );
    $chart->set_y_right_legend( 'Microarray (log ratio to egg)' ,12 , '#164166' );
  }
  $chart->set_data( $arraydata );
  //$chart->line_dot( 3, 5, '#CC3399', 'Microarray', 10 );  
  $chart->line_dot( 3, 4, '#CC9933', 'Microarray', 10 );  
}
if(isset($ressds) and $ressds>0){
  $sdsmax = max($sdsdata);
  array_push($maxval,$sdsmax);
  $chart->set_data( $sdsdata );
  $chart->bar( 50, '#336699', '2D-PAGE', 10 );
}
if(isset($resmsms) and $resmsms>0) {
  $msmsmax = max($msmsdata);
  array_push($maxval, $msmsmax);
  $chart->set_data($msmsdata);
  if(max($msmsvalue) >= 100) {
    $chart->bar(50, '#339966', 'MSMS (x100)', 10);
  } else if(max($msmsvalue) >= 10) {
    $chart->bar(50, '#339966', 'MSMS (x10)', 10);
  } else {
    $chart->bar(50, '#339966', 'MSMS', 10);
  }
}


$max = max($maxval);
if($max){
  $chart->set_y_max($max);
}/*else{
  $chart->set_y_max(1000);
  }*/
$chart->set_y_legend('Expression Level',12,'#778877');

$chart->set_tool_tip('#x_label#[#key#]:<br>#val#');
//$chart->set_tool_tip('Test2:#x_label#:<br>#val#:#key#');
//$chart->set_links("javascript:alert('15%25'),javascript:alert('25%25')");
//plural setting is possible 
$chart->set_links($estlinks);
//$chart->set_links(array("javascript:test()","javascript:alert('1%25')","javascript:alert('2%25')","javascript:alert('3%25')","javascript:alert('4%25')","javascript:alert('5%25')"));
//$chart->set_links(array("javascript:alert('21%25')","javascript:alert('22%25')","javascript:alert('23%25')","javascript:alert('24%25')","javascript:alert('25%25')"));

echo $chart->render();

?>
