<?php
require("dbinfo.php");
$keyword = $_GET['keyword'];
$lf = $_GET['lf'];
$lt = $_GET['lt'];
$mwf = $_GET['mwf'];
$mwt = $_GET['mwt'];
$pif = $_GET['pif'];
$pit = $_GET['pit'];
$est = explode('+', $_GET['est']);
$evalue = $_GET['evalue'];
$acc = $_GET['acc'];
$mim = $_GET['mim'];

/*
$contents = param('contents');
*/

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

//$base = pg_query("select * from results WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL) AND program_type_id=(SELECT id FROM program_type WHERE type='BLAST' AND deletedate IS NULL) AND sub_type_info_id = (SELECT id FROM sub_type_info WHERE type='nr' AND deletedate IS NULL)");

//$base = pg_query("select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.cipro='CIPRO100.11.1' AND basic_info.deletedate IS NULL;");

//$base = pg_query("select length(seq),mw,pi from seq_info WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL)");

$query = "select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL";
$text = " AND basic_info.cipro in (select id from docs where vector @@ to_tsquery('$keyword:*') order by rank_cd(vector,to_tsquery('$keyword:*')))";
$length = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE length(seq) BETWEEN ".$lf." AND ".$lt." AND deletedate is null)";
$mw = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE mw BETWEEN ".$mwf." AND ".$mwt." AND deletedate is null)";
$pi = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE pi BETWEEN ".$pif." AND ".$pit." AND deletedate is null)";

$selest = array();
$specify = array();
foreach($est as $i){
  array_push($selest, "cast(((xpath('/res/detail/$i/text()', res))[1]::text) as double precision)");
  array_push($specify,"cast(((xpath('/res/detail/$i/text()', res))[1]::text) as double precision) > 0");
}
$totest = "cast(((xpath('/res/sum/text()', res))[1]::text) as double precision)";
$sp = implode(" AND ",$specify);
$sel = implode("+",$selest);
$estselect = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE TRUE AND program_type_id = 2 AND sub_type_info_id = 8 AND ".$sp." AND (".$sel.")/(".$totest.") > 0.9 AND (".$totest." != 0) AND deletedate is null)";

//$acc = "NP_001030292";
$homolog = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE deletedate IS NULL AND sub_type_info_id = 4 AND program_type_id = 1 AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < 1.0E".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])";
//$mim = "602054";
//$evalue = -1;
$omim = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 1 AND xid = '".$mim."' AND cast((xpath('/omim/mim/evalue/text()' ,data))[1]::text as double precision) < 1.0E".$evalue." AND deletedate is null))";

if($keyword){
  $query .= $text;
}
if($lf && $lt){
  $query .= $length;
}
if($mwf && $mwt){
  $query .= $mw;
}
if($pif && $pit){
  $query .= $pi;
}
if($_GET['est']){
  $query .= $estselect;
}
if($acc){
  $query .= $homolog;
}
if($mim){
  $query .= $omim;
}

//$query = "select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 1 AND xid = '602054' AND cast((xpath('/omim/mim/evalue/text()' ,data))[1]::text as double precision) < 1.0E-1 AND deletedate is null))";
$query .= ";";
$base = pg_query($query);


///$text = pg_query("select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.cipro in (select id from docs where vector @@ to_tsquery('$keyword:*') order by rank_cd(vector,to_tsquery('$keyword:*'))) AND basic_info.deletedate IS NULL;");

//$homolog = "(SELECT basic_info_id FROM results WHERE deletedate IS NULL AND (sub_type_info_id = ".$blastp_subtypeinfo_id." OR sub_type_info_id = ".$tblastn_subtypeinfo_id." ) AND program_type_id = ".$programtype_id." AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < ".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])";

$hpf = "(SELECT basic_info_id FROM results WHERE program_type_id = ".$programtype_id." AND cast((xpath('/res/DevelopmentalHPFStart/text()' , res))[1]::text as double precision ) = ".$start." AND cast((xpath('/res/DevelopmentalHPFEnd/text()' , res))[1]::text as double precision ) = ".$end." AND deletedate IS NULL)";

$all_items = array();
while ($row = @pg_fetch_assoc($base)){
  $item = array ( "ID" => $row["cipro"],
		  "Length" => $row["length"],
		  "MW(calc)" => $row["mw"],
		  "pI(calc)" => $row["pi"]);
  array_push($all_items, $item);
}

$tasks = array( "identifier" => 'ID',
		"items" => $all_items);
print(json_encode($tasks));
/*
        $countlist = $serv->countByAcc($ID,$evalue);
    }elsif(param('query') eq "mim" && defined param('id')&& defined param('evalue')){
        $countlist = $serv->countByAccAndOmim($evalue,$ID);
    }elsif(defined param('id') || defined param('locus')){
        $countlist = $serv->countByCiproIds($SQLID);
    }elsif(defined param('start_time') && defined param('end_time')){
        $countlist = $serv->countByDevelopmentalHPF($startTime,$endTime);
    }elsif(defined param('fragment')){
        $countlist = $serv->countByFragment(param('fragment'));    	
    }else{
        $countlist = $serv->count();
    }

sub seqicon {
    if($_[0] eq "1.1"){
        img({src=>"./images/full_seq.gif",alt=>"N,C-terminal",border=>"0"})
    }elsif($_[0] eq "1.0"){
        img({src=>"./images/nter_seq.gif",alt=>"N-terminal",border=>"0"})
    }elsif($_[0] eq "0.1"){
        img({src=>"./images/cter_seq.gif",alt=>"C-terminal",border=>"0"})
    }
}

sub esticon {
    if($_[0] eq "1"){
        img({src=>"./images/est.gif",border=>"0",id=>"est$_[1]"}),
        div( {dojoType=>"dijit.Tooltip",connectId=>"est$_[1]"},
             img( {src=>"$CGIURL/viewicon.cgi?c=Expression Profile&id=$_[1]"})
           )
    }
}

sub return_protein_name{
	my $data = shift;
    my $tpp  = XML::TreePP->new();
    if(!defined $$data[1]){
    	return "";
    }
    my $tree = $tpp->parse( $$data[1] );
    my $name; 
    if(defined $tree->{'kyotograil'}->{'kyotograil_gene_name'}){
 	   $name=$tree->{'kyotograil'}->{'kyotograil_gene_name'};    	
    }
	return $name;
}

sub brief {
    my $blast = $_[0];
    my($acc,$source,$protein,$score,$evalue) = @$blast;
    my $pro_name=return_protein_name($_[1]);    

    if(defined $pro_name and $pro_name!~m/^\s*$/){
    	return str("Protein",$pro_name);
    }elsif(defined $_[0]){
	    return str("Accession Number",$acc),br,str("Source",$source),br,
   	 	str("Protein",$protein),br,str("Score",$score),br,str("E value",$evalue);
    }
}

sub main_frame {
        if(param('prog') eq "blastp"){
            my $SEQ = param('seq');
            my $PROG = param('prog');
#            push @results,a({href=>"$CGIURL/blast.cgi?prog=$PROG&seq=$SEQ&type=report"},"CIRPO BLAST Results");
        }

        if($#cipro_id >= 0){
            push @results, select_p_form , br;
            if(defined param('keyword')){
                push @results,table({cellspacing=>"10"},
                        map {
                        Tr(td($start+1+$_,":"),
                           #td(c("lwp",$cipro_id[$_]))),
			   td(a({href=>"#$cipro_id[$_]"},$cipro_id[$_]=~m/KH/? $cipro_id[$_]:"$cipro_id[$_]").
                           ##td(a({href=>"#$cipro_id[$_]",onclick=>"addTab('$cipro_id[$_]');return false;"},$cipro_id[$_]=~m/KH/? $cipro_id[$_]:"$cipro_id[$_]").
                           " [score: ".$headline{$cipro_id[$_]}."]")),
                        Tr(td({valign=>"top"},
                              seqicon($seq{$cipro_id[$_]}),br,
                              esticon($est{$cipro_id[$_]},$cipro_id[$_])),
                           td(abst($length{$cipro_id[$_]},
                               $mw{$cipro_id[$_]}),
                              brief($blastp{$cipro_id[$_]},$name{$cipro_id[$_]}))
                           ),
                        } 0..$#cipro_id );
            }else{
                push @results,table({cellspacing=>"10"},
                    map {
                        Tr(td($start+1+$_,":"),
                           #td(c("lwp",$cipro_id[$_]))),
                           td(a({href=>"javascript:void(0)",onclick=>"addTab('$cipro_id[$_]');return false;"},$cipro_id[$_]=~m/KH/? $cipro_id[$_]:"$cipro_id[$_]"))),
                        Tr(td({valign=>"top"},
                          seqicon($seq{$cipro_id[$_]}),br,
                          esticon($est{$cipro_id[$_]},$cipro_id[$_])),
                           td(abst($length{$cipro_id[$_]},
                               $mw{$cipro_id[$_]}),
                          brief($blastp{$cipro_id[$_]},$name{$cipro_id[$_]}))
                           )
                        } 0..$#cipro_id );
            }

        }
}

*/


?>

