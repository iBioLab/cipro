<?php
require("dbinfo.php");
$id = $_GET["id"];
 
$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

//$ciona = pg_query("select (xpath('//Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_accession/text()' , res))[1]::text as acc,(xpath('//Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_def/text()' , res))[1]::text as def,(xpath('//Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_hsps/Hsp/Hsp_bit-score/text()' , res))[1]::text as score,(xpath('//Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as evalue from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1  AND xpath('//Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\")]/Hit_def/text()' , res)::text[] > ARRAY[''] AND deletedate is null;");

$blastp = pg_query("select res from results where program_type_id = 1 AND sub_type_info_id  = 1 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate is null limit 1);");

$ortholog = pg_query("select unnest(xpath('//Hit/Hit_accession/text()',t2.hit[i])) as acc,unnest(xpath('//Hit/Hit_def/text()',t2.hit[i])) as def,unnest(xpath('//Hit/Hit_len/text()',t2.hit[i])) as length,(select (xpath('//Iteration_query-len/text()',res))[1]::text from results where program_type_id = 1 AND sub_type_info_id  = 1 and basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND deletedate is null limit 1) as qlen,unnest(xpath('//Hsp_query-from/text()',t2.hit[i])) as start,unnest(xpath('//Hsp_query-to/text()',t2.hit[i])) as end,unnest(xpath('//Hsp_align-len/text()',t2.hit[i])) as align,unnest(xpath('//Hsp_identity/text()',t2.hit[i])) as idt,unnest(xpath('//Hsp_positive/text()',t2.hit[i])) as posi,unnest(xpath('//Hsp_score/text()',t2.hit[i])) as score,unnest(xpath('//Hsp_evalue/text()',t2.hit[i])) as evalue from (select t1.hit,generate_series(array_lower(t1.hit,1),array_upper(t1.hit,1)) as i from (select xpath('//Hit[contains(Hit_def/text(),\"[Ciona intestinalis]\") or contains(Hit_def/text(),\"[Homo sapiens]\") or contains(Hit_def/text(),\"[Mus musculus]\") or contains(Hit_def/text(),\"[Eptatretus burgeri]\") or contains(Hit_def/text(),\"[Petromyzon marinus]\") or contains(Hit_def/text(),\"[Ciona savignyi]\") or contains(Hit_def/text(),\"[Halocynthia roretzi]\") or contains(Hit_def/text(),\"[Branchiostoma floridae]\") or contains(Hit_def/text(),\"[Strongylocentrotus purpuratus]\") or contains(Hit_def/text(),\"[Caenorhabditis elegans]\") or contains(Hit_def/text(),\"[Drosophila melanogaster]\")]',res) as hit from results where program_type_id = 1 AND sub_type_info_id  = 1 and basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND deletedate is null limit 1) as t1) as t2;");

$blastp25 = pg_query("select unnest(xpath('//Hit/Hit_accession/text()',t2.hit[i])) as acc,unnest(xpath('//Hit/Hit_def/text()',t2.hit[i])) as def,unnest(xpath('//Hit/Hit_len/text()',t2.hit[i])) as length,(select (xpath('//Iteration_query-len/text()',res))[1]::text from results where program_type_id = 1 AND sub_type_info_id  = 1 and basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND deletedate is null limit 1) as qlen,unnest(xpath('//Hsp_query-from/text()',t2.hit[i])) as start,unnest(xpath('//Hsp_query-to/text()',t2.hit[i])) as end,unnest(xpath('//Hsp_align-len/text()',t2.hit[i])) as align,unnest(xpath('//Hsp_identity/text()',t2.hit[i])) as idt,unnest(xpath('//Hsp_positive/text()',t2.hit[i])) as posi,unnest(xpath('//Hsp_score/text()',t2.hit[i])) as score,unnest(xpath('//Hsp_evalue/text()',t2.hit[i])) as evalue from (select t1.hit,generate_series(array_lower(t1.hit,1),array_upper(t1.hit,1)) as i from (select xpath('//Hit',res) as hit from results where program_type_id = 1 AND sub_type_info_id  = 1 and basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND deletedate is null limit 1) as t1) as t2;");

//$human = pg_query("select (xpath('//Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_accession/text()' , res))[1]::text as acc,(xpath('//Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_def/text()' , res))[1]::text as def,(xpath('//Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_hsps/Hsp/Hsp_bit-score/text()' , res))[1]::text as score,(xpath('//Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as evalue from results where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' limit 1) AND program_type_id = 1 AND sub_type_info_id = 1 AND cast((xpath('//Hit[contains(Hit_def/text(),\"[Homo sapiens]\")]/Hit_hsps/Hsp/Hsp_evalue/text()' , res))[1]::text as double precision) < 1.0E-20 AND deletedate is null;");
/*
$human;
$mouse;
$hagfish;
$lamprey;
$pacific_transparent_sea_squirt;
$common_sea_squirt;
$amphioxus;
$sea_urchin;
$nematode;
$fly;

$ortholog = array("Homo sapiens" => "human","Mus musculus" => "mouse","Eptatretus burgeri" => "hagfish",
		 "Petromyzon marinus" => "lamprey","Ciona savignyi" => "pacific_transparent_sea_squirt",
		 "Halocynthia roretzi" => "common_sea_squirt","Branchiostoma floridae" => "amphioxus",
		 "Strongylocentrotus purpuratus" => "sea_urchin","Caenorhabditis elegans" => "nematode",
		 "Drosophila melanogaster" => "fly",);
*/
/*
$basic  cipro, sflag, eflag,length,mw,pi,seq
$ciona  acc, def, score, evalue
$human acc, def, score, evalue
$mouse acc, def, score, evalue
$fly acc, def, score, evalue
*/

//length | qlen | start | end | align | idt | posi 

$all_items = array();


while ($row = @pg_fetch_assoc($ortholog)){
  $item = array ();
  $def = preg_split('/\[|\]/',$row["def"]);
  $protein = preg_replace("/>.*/","",$def[0]);
  $source = $def[1];
  //integrate start & end
  if($source == 'Ciona intestinalis'){//if match Ciona intestinalis
    $item = array ( "type" => "ortholog","acc" => $row["acc"],"name"=>$protein,"source"=>$source,//"def" => $row["def"]
    		    "score" => $row["score"],"evalue" => $row["evalue"]);
  }else if(floatval($row["evalue"]) < 1E-20 ){// if evalue < 1E-20 and 1st Hit
    $item = array ( "type" => "ortholog","acc" => $row["acc"],"name"=>$protein,"source"=>$source,//"def" => $row["def"]
		    "score" => $row["score"],"evalue" => $row["evalue"]);
  }
  //array_push($all_items, $item);
}


$flag = array();
$xmldom = new DOMDocument();
while ($row = @pg_fetch_assoc($blastp)){
  $xmldom->loadXML( $row["res"] );
  $qlen = $xmldom->getElementsByTagName("Iteration_query-len")->item(0)->nodeValue;  
  $hits = $xmldom->getElementsByTagName("Hit");
  foreach($hits as $hit){
    $acc = $hit->getElementsByTagName("Hit_accession")->item(0)->nodeValue;
    $def = $hit->getElementsByTagName("Hit_def")->item(0)->nodeValue;
    $tmp = preg_split('/\[|\]/',$def);
    if (count($tmp)<1) continue;
    $protein = preg_replace("/>.*/","",$tmp[0]);
    $source = isset($tmp[1])?$tmp[1]:NULL;
    if($source != 'Ciona intestinalis' && $source != 'Homo sapiens' && $source != 'Mus musculus' && 
       $source != 'Eptatretus burgeri' && $source != 'Petromyzon marinus' && $source != 'Ciona savignyi' &&
       $source != 'Halocynthia roretzi' && $source != 'Branchiostoma floridae' && $source != 'Strongylocentrotus purpuratus' && 
       $source != 'Caenorhabditis elegans' && $source != 'Drosophila melanogaster'){
      continue;
    }
    if(isset($flag[$source]) and $flag[$source] == 1){
      continue;
    }
    $blen = $hit->getElementsByTagName("Hit_len")->item(0)->nodeValue;
    $hsps = $hit->getElementsByTagName("Hsp");
    $score = array();
    $evalue  = array();
    $idt  = array();
    $posi  = array();
    $cov = array();
    $range = array();

    foreach($hsps as $hsp){
      if(intval($hsp->getElementsByTagName("Hsp_score")->item(0)->nodeValue) >= 80){
	array_push($score,$hsp->getElementsByTagName("Hsp_score")->item(0)->nodeValue);
	array_push($evalue,$hsp->getElementsByTagName("Hsp_evalue")->item(0)->nodeValue);
	array_push($idt,intval($hsp->getElementsByTagName("Hsp_identity")->item(0)->nodeValue * 100
			       /$hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue));
	array_push($posi,intval($hsp->getElementsByTagName("Hsp_positive")->item(0)->nodeValue * 100
				/$hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue));
	//array_push($cov,intval($hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue * 100 / $qlen));
	$f = $hsp->getElementsByTagName("Hsp_query-from")->item(0)->nodeValue;
	$t = $hsp->getElementsByTagName("Hsp_query-to")->item(0)->nodeValue;
	$cov = array_merge($cov,range($f,$t));	
        $from = intval($f * 100 / $qlen);
	$to = intval($t * 100 / $qlen);
	array_push($range,$from.':'.$to);
      }
    }
    if($source == 'Ciona intestinalis' || floatval(min($evalue)) < 1E-20){
      $item = array ( "type" => "ortholog","acc" => $acc,"name"=>$protein,"source"=>$source,
		      "score" => implode(",",$score),"evalue" => implode(",",$evalue),"idt" => implode(",",$idt),//"check" => true,
		      "posi" => implode(",",$posi),"range" => implode(",",$range),"coverage" => intval(count(array_unique($cov)) * 100 / $qlen)//"cov" => implode(",",$cov)
		      );
      array_push($all_items, $item);
      $flag[$source] = 1;
    }
  }
}

$tasks = array( "identifier" => 'acc',
                "items" => $all_items);
print(json_encode($tasks));

?>

