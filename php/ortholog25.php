<?php
require("dbinfo.php");
$id = $_GET["id"];
 
$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$blastp = pg_query("select res from results where program_type_id = 1 AND sub_type_info_id  = 1 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate is null limit 1);");


$all_items = array();

$xmldom = new DOMDocument();
while ($row = @pg_fetch_assoc($blastp)){
  $xmldom->loadXML( $row["res"] );
  $qlen = $xmldom->getElementsByTagName("Iteration_query-len")->item(0)->nodeValue;  
  $hits = $xmldom->getElementsByTagName("Hit");
  foreach($hits as $hit){
    $acc = $hit->getElementsByTagName("Hit_accession")->item(0)->nodeValue;
    $def = $hit->getElementsByTagName("Hit_def")->item(0)->nodeValue;
    $tmp = preg_split('/\[|\]/',$def);
    $protein = preg_replace("/>.*/","",$tmp[0]);
    $source = $tmp[1];
    $blen = $hit->getElementsByTagName("Hit_len")->item(0)->nodeValue;
    $hsps = $hit->getElementsByTagName("Hsp");
    $score = array();
    $evalue  = array();
    $idt  = array();
    $posi  = array();
    $cov = array();
    $range = array();

    foreach($hsps as $hsp){
      if(intval($hsp->getElementsByTagName("Hsp_score")->item(0)->nodeValue) >= 80){
	array_push($score,$hsp->getElementsByTagName("Hsp_score")->item(0)->nodeValue);
	array_push($evalue,$hsp->getElementsByTagName("Hsp_evalue")->item(0)->nodeValue);
	array_push($idt,intval($hsp->getElementsByTagName("Hsp_identity")->item(0)->nodeValue * 100
			       /$hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue));
	array_push($posi,intval($hsp->getElementsByTagName("Hsp_positive")->item(0)->nodeValue * 100
				/$hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue));
	$f = $hsp->getElementsByTagName("Hsp_query-from")->item(0)->nodeValue;
	$t = $hsp->getElementsByTagName("Hsp_query-to")->item(0)->nodeValue;
	$cov = array_merge($cov,range($f,$t));	
        $from = intval($f * 100 / $qlen);
	$to = intval($t * 100 / $qlen);
	array_push($range,$from.':'.$to);
      }
    }
    if(max($idt) >= 25){
      $item = array ( "type" => "ortholog","acc" => $acc,"name"=>$protein,"source"=>$source,
		      "score" => implode(",",$score),"evalue" => implode(",",$evalue),"idt" => implode(",",$idt),
		      "posi" => implode(",",$posi),"range" => implode(",",$range),"coverage" => intval(count(array_unique($cov)) * 100 / $qlen)
		      );
      array_push($all_items, $item);
    }
  }
}

$tasks = array( "identifier" => 'acc',
                "items" => $all_items);
print(json_encode($tasks));

?>

