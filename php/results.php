<?php
require("dbinfo.php");
$id = $_GET["id"];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$basic = pg_query("select basic_info.cipro,seq ~ '^M' as sflag, basic_info.eflag,length(seq),mw,pi,seq,array(select distinct program_type_id || ':' || COALESCE(sub_type_info_id,0) from results where basic_info_id = basic_info.id AND deletedate is null) as content from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.cipro='$id' AND basic_info.deletedate IS NULL limit 1;");

$mass = pg_query("select unnest(xpath('/res/locus_id/text()', res))::text as locus_id from results where basic_info_id = (select id from basic_info where cipro = '$id') and program_type_id = 2 and sub_type_info_id = 22 and deletedate is null limit 1;");

$faba = pg_query("select unnest(xpath('//image/text()' , t2.faba[i]))::text as image,unnest(xpath('//thumbnail/text()' , t2.faba[i]))::text as thumbnail,unnest(xpath('//full_record_link/text()' , t2.faba[i]))::text as record,unnest(xpath('//_3D_link/text()' , t2.faba[i]))::text as link,unnest(xpath('//slice_link/text()' , t2.faba[i]))::text as slink,unnest(xpath('//link_id/text()' , t2.faba[i]))::text as id,unnest(xpath('//gene_name/text()' , t2.faba[i]))::text as name,unnest(xpath('//hpf/text()' , t2.faba[i]))::text::numeric as hpf,unnest(xpath('//expressed_tissue/text()' , t2.faba[i]))::text as tissue,unnest(xpath('//subcellular_localization/text()' , t2.faba[i]))::text as locate from (select t1.faba,generate_series(array_lower(t1.faba,1),array_upper(t1.faba,1)) as i from (select xpath('/res/data',res) as faba from results where program_type_id = 6 AND basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate is null limit 1) AND deletedate is null) as t1) as t2 order by hpf;");

$xseq = pg_query("select distinct basic_info.cipro from basic_info left join seq_info on basic_info_id = basic_info.id WHERE sha256 in (select sha256 from basic_info left join seq_info on basic_info_id = basic_info.id where  basic_info.cipro = '$id') AND basic_info.cipro !='$id' AND basic_info.deletedate IS NULL;");

$pubmed = pg_query("select pmid,array_agg(lastname || ' ' || firstname) as name,year,title,journal,volume,page from (select xid as pmid,unnest(xpath('//Author/Initials/text()',data))::text as lastname, unnest(xpath('//Author/LastName/text()',data))::text as firstname,(xpath('//PubDate/Year/text()',data))[1]::text as year,(xpath('//ArticleTitle/text()',data))[1]::text as title,(xpath('//ISOAbbreviation/text()',data))[1]::text as journal,(xpath('//JournalIssue/Volume/text()',data))[1]::text as volume,(xpath('//MedlinePgn/text()',data))[1]::text as page from reference_db where external_db_id = 2 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null) as tmp group by pmid,year,title,journal,volume,page;");

$antibody = pg_query("select (xpath('//antibody_ortholog/text()' , data))[1]::text as ortholog,(xpath('//antibody_peptide/text()' , data))[1]::text as peptide,(xpath('//antibody_recombinant/text()' , data))[1]::text as recombinant,(xpath('//antibody_desc/text()' , data))[1]::text as desc from reference_db where external_db_id = 9 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null;");

$hinv = pg_query("select xid,COALESCE((xpath('/HinvAutoannotation/description/text()' , data))[1]::text,'') as description,COALESCE((xpath('/HinvAutoannotation/category/text()' , data))[1]::text,'') as category,COALESCE((xpath('/HinvAutoannotation/evidence/text()' , data))[1]::text,'') as evidence,COALESCE((xpath('/HinvAutoannotation/Identity/text()' , data))[1]::text,'') as identity,COALESCE((xpath('/HinvAutoannotation/Coverage/text()' , data))[1]::text,'') as coverage,COALESCE((xpath('/HinvAutoannotation/Species/text()' , data))[1]::text,'') as species from reference_db where external_db_id = 10 AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null limit 1;");

$xref = pg_num_rows(pg_query("select xid from reference_db where external_db_id in (3,5,6,8) AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null;"));

/*
$basic  cipro, sflag, eflag,length,mw,pi,seq
$faba image, thumbnail, record, link, slink, id, name, hpf, tissue, locate
$xseq cipro
$go acc, name
*/

$all_items = array();
$content2name = array(
    "3:12" => 'modeller',
    "10:0" => 'omim',
    "9:18" => 'psipred',
    "9:24" => 'interpro',
    "7:21" => 'tmhmm',
    "4:14" => 'wolfpsort',
    "2:8"  => 'est',
    "2:9"  => 'sds',
    "2:10" => 'array',
    "2:22" => 'msms',
    "1:1"  => 'blastp',
    "1:32" => 'savignyi',
    "1:35" => 'phylogeny',
    "1:4" =>  'ghost',
    '2:23' => 'integrated_graph',
    "1:25" => 'hinv',
    '9:19' => 'netphos',
);

while ($row = @pg_fetch_assoc($basic)){
  $contents = explode(',',preg_replace('/\{|\}/','', $row["content"]));
  $c = array();
  $expression = "";  
  foreach($contents as $i){
    if($i == "2:8" || $i == "2:10"){
      $expression = "expression";
    }else if($i == "9:24"){
      array_push($all_items, array("type" => 'interpro'));
    }else if($i == "10:0"){
      array_push($all_items, array("type" => 'mim'));
      array_push($c,$content2name[$i]);
    }else{
      array_push($c,$content2name[$i]);
    }
  }
  if($expression){
    array_push($c,$expression);
  }
  $item = array ( "type" => "basic","sflag" => $row["sflag"],"eflag" => intval($row["eflag"]),
		  "length" => $row["length"],"mw" => $row["mw"],
		  "pi" => $row["pi"],"seq" => $row["seq"],"content" => $c);
  array_push($all_items, $item);
}

while ($row = @pg_fetch_assoc($mass)) {
  array_push($all_items, array("type" => "msms", "locus_id" => $row["locus_id"]));
}

$fabaitem = array();
while ($row = @pg_fetch_assoc($faba)){
  $item = array ( "image" => $row["image"],"thumbnail" => $row["thumbnail"],
		  "record" => $row["record"],"link" => $row["link"],
		  "slink" => $row["slink"],"id" => $row["id"],
		  "name" => $row["name"],"hpf" => $row["hpf"],
		  "tissue" => $row["tissue"],"locate" => $row["locate"]);
  array_push($fabaitem, $item);
}
array_push($all_items, array ( "type" => "faba","fabaitem" => $fabaitem));

while ($row = @pg_fetch_assoc($xseq)){
  $item = array ( "type" => "xseq","cipro" => $row["cipro"]);
  array_push($all_items, $item);
}

while ($row = @pg_fetch_assoc($pubmed)){
  $names = explode(',',preg_replace('/\{|\}|\"/','', $row["name"]));
  $authors = array();
  foreach($names as $i){
    list($lastname,$fastname) = preg_split('/\s+/', $i);
    array_push($authors,implode(".",str_split($lastname)).'. '.$fastname);
  }
  $item = array ( "type" => "pubmed","pmid" => $row["pmid"],"year" => intval($row["year"]),
		  "volume" => $row["volume"],"page" => $row["page"],
		  "journal" => $row["journal"],"title" => $row["title"],"name" => $authors);
  array_push($all_items, $item);
}

while ($row = @pg_fetch_assoc($antibody)){
  $item = array ( "type" => "antibody","ortholog"=>$row["ortholog"],
		  "desc" => $row["desc"],"peptide" => $row["peptide"],
		  "recombinant" => $row["recombinant"]);
  array_push($all_items, $item);
}

while ($row = @pg_fetch_assoc($hinv)){
  $item = array ( "type" => "hinv","description" => $row["description"],"category" => $row["category"],
		  "evidence" => $row["evidence"],"identity" => $row["identity"],"coverage" => $row["coverage"],"species" => $row["species"]);
  array_push($all_items, $item);
}

if($xref){
  $item = array ( "type" => "xref");
  array_push($all_items, $item);
}

$item = array ( "type" => "literature");
array_push($all_items, $item);
$item = array ( "type" => "experimental");
array_push($all_items, $item);
$item = array ( "type" => "theoretical");
array_push($all_items, $item);
$item = array ( "type" => "noevidence");
array_push($all_items, $item);

$tasks = array( "identifier" => 'type',
                "items" => $all_items);
print(json_encode($tasks));

?>

