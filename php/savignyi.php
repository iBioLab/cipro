<?php
require("dbinfo.php");
$id = $_GET["id"];
 
$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$blastp = pg_query("select res from results where program_type_id = 1 and sub_type_info_id = 32 and basic_info_id = (select id from basic_info where cipro = '$id') and deletedate is null limit 1");

//length | qlen | start | end | align | idt | posi 

function round_exponent ($number) {
  $num = preg_replace("/e-.*?$/", "", $number);
  $pow = preg_replace("/^.*?e-/", "", $number);
  
  if($num == 0) {
    return 0;
  } else {
    return round($num, 5)."e-".$pow;
  }
}

$all_items = array();

$xmldom = new DOMDocument();
while ($row = @pg_fetch_assoc($blastp)){
  $xmldom->loadXML($row["res"]);
  $qlen = $xmldom->getElementsByTagName("Iteration_query-len")->item(0)->nodeValue;
  $hits = $xmldom->getElementsByTagName("Hit");
  $flag = true;
  foreach($hits as $hit) {
    $def = $hit->getElementsByTagName("Hit_def")->item(0)->nodeValue;
    $acc = preg_replace("/\s.*$/", "", $def);
    $name = $hit->getElementsByTagName("Hit_id")->item(0)->nodeValue;

    $blen = $hit->getElementsByTagName("Hit_len")->item(0)->nodeValue;
    $hsps = $hit->getElementsByTagName("Hsp");
    
    $score = array();
    $evalue = array();
    $idt = array();
    $posi = array();
    $cov = array();
    $range = array();

    foreach($hsps as $hsp) {
      if(intval($hsp->getElementsByTagName("Hsp_score")->item(0)->nodeValue) >= 80) {
        array_push($score, $hsp->getElementsByTagName("Hsp_score")->item(0)->nodeValue);
        array_push($evalue, round_exponent($hsp->getElementsByTagName("Hsp_evalue")->item(0)->nodeValue));
        array_push($idt, intval($hsp->getElementsByTagName("Hsp_identity")->item(0)->nodeValue * 100 / $hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue));
        array_push($posi, intval($hsp->getElementsByTagName("Hsp_positive")->item(0)->nodeValue * 100 / $hsp->getElementsByTagName("Hsp_align-len")->item(0)->nodeValue));
        $f = $hsp->getElementsByTagName("Hsp_query-from")->item(0)->nodeValue;
        $t = $hsp->getElementsByTagName("Hsp_query-to")->item(0)->nodeValue;
	$cov = array_merge($cov, range($f, $t));
	$from = intval($f * 100 / $qlen);
	$to = intval($t * 100 / $qlen);
	array_push($range, $from.":".$to);
      }
    }
    
    if($flag) {
      $savignyi_desc = pg_query("select unnest(xpath('/ensembl/ensembl_gene_name/text()', data)::text[]) as desc from reference_db where external_db_id = (select id from external_db where name = 'enscsavp') and xid = '$acc' and deletedate is null");
      $item = array("acc" => $acc, "source" => 'C. savignyi', "desc" => "-",
     	  "score" => implode(",", $score), "evalue" => implode(",", $evalue),
	  "idt" => implode(",", $idt), "posi" => implode(",", $posi),
	  "range" => implode(",", $range), 
	  "coverage" => intval(count(array_unique($cov)) * 100 / $qlen));
      if(preg_match("/ENSCSAVP/", $acc)) {
        while($row_savignyi = @pg_fetch_assoc($savignyi_desc)) {
          $item = array("acc" => $acc, "source" => 'C. savignyi', "desc" => $row_savignyi["desc"],
    	    	    "score" => implode(",", $score), "evalue" => implode(",", $evalue),
		    "idt" => implode(",", $idt), "posi" => implode(",", $posi),
		    "range" => implode(",", $range), 
		    "coverage" => intval(count(array_unique($cov)) * 100 / $qlen));
        }
      }
      array_push($all_items, $item);
      $flag = false;
    }
  }
}

$tasks = array( "identifier" => 'acc',
                "items" => $all_items);
print(json_encode($tasks));

?>

