<?php
require("dbinfo.php");
//$ids = stripslashes($_GET['ids']);
$keyword = $_GET['keyword'];
$lf = $_GET['lf'];
$lt = $_GET['lt'];
$mwf = $_GET['mwf'];
$mwt = $_GET['mwt'];
$pif = $_GET['pif'];
$pit = $_GET['pit'];
$est = explode(',', $_GET['est']);
$evalue = $_GET['evalue'];
$acc = $_GET['acc'];
$mim = $_GET['mim'];
$contents = explode(',', $_GET['content']);

$seq = preg_replace('/\s/','',$_GET['seq']);
$prog = $_GET['prog'];
$filter = $_GET['filter'];
$matrix = $_GET['matrix'];

$pattern = $_GET['grep'];
$peptidemass = explode(',',$_GET['pmf']);
$accuracy = $_GET['accuracy'];
$ptm = $_GET['ptm'];
$sh = $_GET['sh'];
$miss = $_GET['miss'];
$co = $_GET['contami'];
$enzyme = $_GET['enzyme'];

$annotf = $_GET['annotf'];
$annott = $_GET['annott'];
$annotuser = $_GET['annotuname'];

$hinvdesc = $_GET['hinvdesc'];
$hinvcategory = $_GET['hinvcategory'];

function quote($id){
  return "'".$id."'";
}

function suf($str){
  return $str.":*";
}

if($seq){
  $xml = `echo $seq | blastall -d ../cipro -a2 -m7 -p $prog -F $filter -M $matrix`;
  $xmldom = new DOMDocument();
  $xmldom->loadXML( $xml );
  $hits = $xmldom->getElementsByTagName("Hit");
  $blast = array();
  foreach($hits as $hit){
    $hitacc = $hit->getElementsByTagName("Hit_accession");
    $hitevalue = $hit->getElementsByTagName("Hsp_evalue");
    $blast[$hitacc->item(0)->nodeValue] = $hitevalue->item(0)->nodeValue;
  }
  $ids = implode(",",array_map("quote",array_keys($blast)));
}

$connection=pg_connect("host=$hostspec dbname=$database user=$username password=$password");

//$base = pg_query("select * from results WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL) AND program_type_id=(SELECT id FROM program_type WHERE type='BLAST' AND deletedate IS NULL) AND sub_type_info_id = (SELECT id FROM sub_type_info WHERE type='nr' AND deletedate IS NULL)");

//$base = pg_query("select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.cipro='CIPRO100.11.1' AND basic_info.deletedate IS NULL;");

//$base = pg_query("select length(seq),mw,pi from seq_info WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro='$id' AND deletedate IS NULL)");

//$query = "select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL";

$query = "select basic_info.id,basic_info.cipro,basic_info.eflag,array(select program_type_id || ':' || COALESCE(sub_type_info_id,0) from results where basic_info_id = basic_info.id) as content,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL";

$ciproids = " AND basic_info.cipro in ($ids)";

$keyword = preg_replace('/\s\s/','\s',$keyword);
$keyword = implode(" & ", array_map("suf",preg_split('/\s+/', $keyword)));
$keyword = preg_replace('/& OR:\* &/',' | ',$keyword);
//$keyword = preg_replace("/'/","''''",$keyword);
//$keyword = preg_replace('/\/','\\\\\\\\',$keyword);
$keyword = preg_replace('/ \-/',' \!',$keyword);

$text = " AND basic_info.cipro in (select id from docs where vector @@ to_tsquery('$keyword') order by rank_cd(vector,to_tsquery('$keyword')))";
$length = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE length(seq) BETWEEN ".$lf." AND ".$lt." AND deletedate is null)";
$mw = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE mw BETWEEN ".$mwf." AND ".$mwt." AND deletedate is null)";
$pi = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE pi BETWEEN ".$pif." AND ".$pit." AND deletedate is null)";

$annot;
if($annotf == 0 && $annott == 0){
  $annot = " AND basic_info.id not in (SELECT basic_info_id FROM annotname)";
}else{
  $annot = " AND basic_info.id in (SELECT basic_info_id FROM (SELECT basic_info_id,count(basic_info_id) FROM annotname GROUP BY basic_info_id) AS annot WHERE count BETWEEN ".$annotf." AND ".$annott.")";
}
$uname = " AND basic_info.id in (SELECT basic_info_id FROM annotname WHERE uname = '$annotuser')";

$hinvc = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 10 AND (xpath('/HinvAutoannotation/category/text()' ,data))[1]::text = '$hinvcategory' AND deletedate is null))";
$hinvd = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 10 AND (xpath('/HinvAutoannotation/description/text()' ,data))[1]::text ~ '$hinvdesc' AND deletedate is null))";

$selest = array();
$specify = array();
foreach($est as $i){
  array_push($selest, "cast(((xpath('/res/detail/$i/text()', res))[1]::text) as double precision)");
  array_push($specify,"cast(((xpath('/res/detail/$i/text()', res))[1]::text) as double precision) > 0");
}
$totest = "cast(((xpath('/res/sum/text()', res))[1]::text) as double precision)";
$sp = implode(" AND ",$specify);
$sel = implode("+",$selest);
$estselect = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE TRUE AND program_type_id = 2 AND sub_type_info_id = 8 AND ".$sp." AND (".$sel.")/(".$totest.") > 0.9 AND (".$totest." != 0) AND deletedate is null)";

//$acc = "NP_001030292";
$homolog = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE deletedate IS NULL AND sub_type_info_id = 4 AND program_type_id = 1 AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < 1.0E".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])";
//$mim = "602054";
//$evalue = -1;
$omim = " AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 1 AND xid = '".$mim."' AND cast((xpath('/omim/evalue/text()' ,data))[1]::text as double precision) < 1.0E".$evalue." AND deletedate is null))";

$gelspot = " AND basic_info.id in (SELECT basic_info_id FROM results WHERE program_type_id = 9 AND (xpath('/res/gel/spot[@id=\"$spot\"]/mw/text()', res))[1]::text is not null AND deletedate is null)";

//support ^$[].+*
$grep = " AND basic_info.id in (SELECT basic_info_id FROM seq_info WHERE seq ~ '$pattern' AND deletedate is null)";

$keratin = array(517.2986, 533.2571, 547.3026, 550.2405, 559.3567, 576.3103, 579.256, 603.3716,
		 604.3052, 629.3045, 639.2482, 698.3294, 712.374, 811.4353, 906.4684, 949.4598, 954.4352,
		 968.4952, 972.4028, 973.4952, 974.5157, 997.4599, 1011.4534, 1016.5011, 1024.4949,
		 1071.6047, 1080.5324, 1127.5042, 1165.4793, 1192.6212, 1263.6946, 1377.6757, 1421.678,
		 1538.7168, 1593.7725, 1828.9441, 2043.9943, 2075.0657, 2244.0314, 2415.302, 3752.8575,
		 6428.7799);
$pig_trypsinA = array(451.1941, 462.22, 515.3306, 569.2684, 633.2844, 650.315, 664.367, 694.39,
		      711.3136, 732.433, 748.4279, 759.3889, 792.462, 802.4311, 820.4933, 842.51, 870.5412, 906.5049,
		      920.4842, 934.5362, 955.5001, 964.558, 968.4511, 980.4875, 1006.4879, 1006.5209, 1008.5188,
		      1023.4747, 1044.5478, 1045.5642, 1126.5645, 1153.575, 1169.5699, 1175.6346, 1178.5264,
		      1191.6295, 1309.6388, 1335.4946, 1337.5102, 1378.6578, 1390.6854, 1420.7225, 1469.731,
		      1515.8456, 1531.8405, 1537.6416, 1539.6572, 1543.8768, 1559.8718, 1649.8221, 1694.8325,
		      1698.8737, 1726.9049, 1736.843, 1766.7842, 1768.7998, 1808.8754, 1836.9066, 1940.9354,
		      1955.9537, 1959.0473, 1975.0422, 1987.0785, 2003.0734, 2010.9443, 2038.9755, 2158.0313,
		      2166.029, 2174.0262, 2211.1046, 2225.1202, 2239.1358, 2283.1807, 2299.1756, 2435.1587,
		      2455.1849, 2457.2005, 2470.0512, 2472.0668, 2483.2161, 2485.2317, 2486.0461, 2488.0617,
		      2536.1502, 2538.1658, 2624.3295, 2634.3561, 2650.351, 2652.3607, 2662.3873, 2678.3822,
		      2707.4168, 2807.3145, 2914.5062, 3013.3243, 3094.6246, 3110.6195, 3143.4852, 3159.4801,
		      3161.4957, 3171.5164, 3173.532, 3187.5113, 3189.5269, 3217.4968, 3219.5125, 3245.5281,
		      3247.5437, 3309.7265, 3325.7214, 3337.7577, 3353.7526, 3900.8108, 3928.842, 4475.1011,
		      4475.2669, 4491.2618, 4503.2981, 4519.293, 4718.2343, 4746.2655, 5152.3372, 5168.3321,
		      5180.3684, 5196.3633, 5557.8751, 5573.87);

$pig_trypsinB = array(515.3306, 842.51, 870.5412, 1045.5642, 1126.5645, 1420.7225, 1531.8405,
		      1940.9354, 2003.0734, 2211.1046, 2225.1202, 2239.1358, 2283.1807, 2299.1756, 2678.3822,
		      2807.3145, 2914.5062, 3094.6246, 3337.7577, 3353.7526);
$bovine_trypsin = array(259.19, 362.2, 632.31, 658.38, 804.41, 905.5, 1019.5, 1110.55, 1152.57, 1432.71,
			1494.61, 2162.05, 2192.99, 2272.15, 2551.24, 4550.12);

$contamimass = array();
if($co){
  $contamimass = array_merge($contamimass,$keratin);
}
if($enzyme == "Porcine(All)"){
  $contamimass = array_merge($contamimass,$pig_trypsinA);
}else if($enzyme == "Porcine(Major)"){
  $contamimass = array_merge($contamimass,$pig_trypsinB);
}else if($enzyme == "Bovine"){
  $contamimass = array_merge($contamimass,$bovine_trypsin);
}
$filter = array();
for($i = 0;$i < sizeof($contamimass);$i++){
  $lower = $contamimass[$i] - $accuracy;
  $upper = $contamimass[$i] + $accuracy; 
  array_push($filter,"x between $lower and $upper");
}
$filters = implode(" AND ",$filter);

$ptmarray = array();
for($i = 0; $i < 16; $i++){
  $j = str_split(decbin($i));
  $m = "";
  if($j[0] && preg_match("/n/",$ptm)){
    $m .= "n";
  }
  if($j[1] && preg_match("/o/",$ptm)){
    $m .= "o";
  }
  if($j[2] && preg_match("/p/",$ptm)){
    $m .= "p";
  }
  if($j[3] && preg_match("/a/",$ptm)){
    $m .= "a";
  }
  if(! $m){
    $m = "s";
  }
  if($sh && $m != "s"){
    array_push($ptmarray,$m.$sh);
  }
  array_push($ptmarray,$m);
}
$ptms = implode(" || ",array_unique($ptmarray));
$ptm_arg = implode(",", array_unique($ptmarray));
//$pmf = " AND basic_info.id in (select basic_info_id from (select basic_info_id,unnest(s || o) as x from pmf) as t where $overlaps group by basic_info_id)"; 
//$pmf = " AND basic_info.id in (select basic_info_id from (select basic_info_id,unnest($ptms) as x from pmf) as t where $overlaps group by basic_info_id)";

$overlap = array();
for($i = 0;$i < sizeof($peptidemass);$i++){
  $lower = $peptidemass[$i] - $accuracy;
  $upper = $peptidemass[$i] + $accuracy; 
//  array_push($overlap,"x between $lower and $upper");
  array_push($overlap, "select basic_info_id from (select basic_info_id, unnest($ptms) as x from pmf) as t where x between $lower and $upper group by basic_info_id");
}
//$overlaps = implode(" OR ",$overlap);
$overlaps = implode(" UNION ALL ", $overlap);

$pmfids;
$pmfcount = array();
if($_GET['pmf']){
  //$pmfquery = pg_query("select basic_info_id,count(x) from (select basic_info_id,unnest($ptms) as x from pmf) as t where $overlaps group by basic_info_id order by count desc;"); 
//  $pmfquery = pg_query("select basic_info_id,count(x) from (select basic_info_id,unnest($ptms) as x from pmf) as t where $overlaps group by basic_info_id;");
  $pmfquery = pg_query("select basic_info_id, count(basic_info_id) from ($overlaps) as w group by basic_info_id");
  $pmfid = array();
  while ($row = @pg_fetch_assoc($pmfquery)){
    $pmfcount[$row["basic_info_id"]] = $row["count"];
    array_push($pmfid,$row["basic_info_id"]);
  }
  $pmfs = implode(",",$pmfid);
  if(count($pmfid)){
    $pmfids = " AND basic_info.id in ($pmfs)";
  }
}


for($i = 0; $i < sizeof($contents); $i++){
  switch ($contents[$i]){
  case 'BLASTP':
    $content_blastp = " AND basic_info.id in (select basic_info_id from results where program_type_id = 1 AND sub_type_info_id = 1 AND deletedate is null)";
    break;
  case 'EST':
    $content_est = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 8 AND deletedate is null)";
    break;
  case '2D-PAGE':
    $content_sds = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 9 AND deletedate is null)";
    break;
  case 'Microarray':
    $content_array = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 10 AND deletedate is null)";
    break;
  case 'MS/MS':
    $content_msms = " AND basic_info.id in (select basic_info_id from results where program_type_id = 2 AND sub_type_info_id = 22 AND deletedate is null)";
    break;
  case 'MODELLER':
    $content_modeller = " AND basic_info.id in (select basic_info_id from results where program_type_id = 3 AND sub_type_info_id = 12 AND deletedate is null)";
    break;
  case 'WoLF PSORT':
    $content_wolfpsort = " AND basic_info.id in (select basic_info_id from results where program_type_id = 4 AND sub_type_info_id = 14 AND deletedate is null)";
    break;
  case '3DPL':
    $content_3dpl = " AND basic_info.id in (select basic_info_id from results where program_type_id = 6  AND deletedate is null)";
    break;
  case 'TMHMM':
    $content_tmhmm = " AND basic_info.id in (select basic_info_id from results where program_type_id = 7 AND sub_type_info_id = 21 AND deletedate is null)";
    break;
  case 'PSIPRED':
    $content_psipred = " AND basic_info.id in (select basic_info_id from results where program_type_id = 9 AND sub_type_info_id = 18 AND deletedate is null)";
    break;
  case 'NetPhos':
    $content_netphos = " AND basic_info.id in (select basic_info_id from results where program_type_id = 9 AND sub_type_info_id = 19 AND deletedate is null)";
    break;
  case 'InterProScan':
    $content_iprscan = " AND basic_info.id in (select basic_info_id from results where program_type_id = 9 AND sub_type_info_id = 24 AND deletedate is null)";
    break;
  case 'OMIM':
    $content_omim = " AND basic_info.id in (select basic_info_id from results where program_type_id = 10 AND deletedate is null)";
    break;
  }
}
//select distinct id,program_type_id,sub_type_info_id from results where basic_info_id = (select id from basic_info where cipro = 'CIPRO100.11.1');
//select ARRAY(select distinct program_type_id || ',' || COALESCE(sub_type_info_id,0) from results where basic_info_id = (select id from basic_info where cipro = 'CIPRO100.11.1'));
//select basic_info.cipro,basic_info.eflag,results.program_type_id,results.sub_type_info_id,length(seq),mw,pi from (basic_info left join seq_info on basic_info_id = basic_info.id) join results on seq_info.basic_info_id = results.basic_info_id WHERE basic_info.cipro = 'CIPRO100.11.1' AND basic_info.deletedate IS NULL;

//select distinct basic_info.cipro,basic_info.eflag,(select ARRAY(select distinct program_type_id || ',' || COALESCE(sub_type_info_id,0) from results where basic_info_id = (select id from basic_info where cipro = 'CIPRO100.11.1'))) as content,length(seq),mw,pi from (basic_info left join seq_info on basic_info_id = basic_info.id) join results on seq_info.basic_info_id = results.basic_info_id WHERE basic_info.cipro = 'CIPRO100.11.1' AND basic_info.deletedate IS NULL;

//select cipro,array(select program_type_id || ',' || sub_type_info_id from results where basic_info_id = basic_info.id) as content from basic_info;

//successful
//select basic_info.cipro,basic_info.eflag,array(select program_type_id || ',' || COALESCE(sub_type_info_id,0) from results where basic_info_id = basic_info.id) as content,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL;

//if($ids){
//  $query .= $ciproids;
//}
if($seq && count($blast)){
  $query .= $ciproids;
}
if($_GET['keyword']){
  $query .= $text;
}
if(isset($lf) && isset($lt)){
  $query .= $length;
}
if(isset($mwf) && isset($mwt)){
  $query .= $mw;
}
if(isset($pif) && isset($pit)){
  $query .= $pi;
}
if($annotuser){
  $query .= $uname;
}
if(isset($annotf) && isset($annott)){
  $query .= $annot;
}
if($hinvcategory){
  $query .= $hinvc;
}
if($hinvdesc){
  $query .= $hinvd;
}

if($_GET['est']){
  $query .= $estselect;
}
if($acc){
  $query .= $homolog;
}
if($mim){
  $query .= $omim;
}
if($pattern){
  $query .= $grep;
}
if($_GET['pmf']){
  //$query .= $pmf;
  $query .= $pmfids;
}

if($content_blastp){
  $query .= $content_blastp;
}
if($content_est){
  $query .= $content_est;
}
if($content_sds){
  $query .= $content_sds;
}
if($content_array){
  $query .= $content_array;
}
if($content_msms){
  $query .= $content_msms;
}
if($content_modeller){
  $query .= $content_modeller;
}
if($content_wolfpsort){
  $query .= $content_wolfpsort;
}
if($content_3dpl){
  $query .= $content_3dpl;
}
if($content_tmhmm){
  $query .= $content_tmhmm;
}
if($content_psipred){
  $query .= $content_psipred;
}
if($content_netphos){
  $query .= $content_netphos;
}
if($content_iprscan){
  $query .= $content_iprscan;
}
if($content_omim){
  $query .= $content_omim;
}

if($_GET["random"]){
  $query .= " order by random()";
}

//$query = "select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.deletedate IS NULL AND basic_info.id in (SELECT basic_info_id FROM basic_info_reference_db_link WHERE reference_db_id IN (SELECT id FROM reference_db WHERE external_db_id = 1 AND xid = '602054' AND cast((xpath('/omim/mim/evalue/text()' ,data))[1]::text as double precision) < 1.0E-1 AND deletedate is null))";
$query .= ";";
$base = pg_query($query);

///$text = pg_query("select basic_info.cipro,basic_info.eflag,length(seq),mw,pi from basic_info left join seq_info on basic_info_id = basic_info.id WHERE basic_info.cipro in (select id from docs where vector @@ to_tsquery('$keyword:*') order by rank_cd(vector,to_tsquery('$keyword:*'))) AND basic_info.deletedate IS NULL;");

//$homolog = "(SELECT basic_info_id FROM results WHERE deletedate IS NULL AND (sub_type_info_id = ".$blastp_subtypeinfo_id." OR sub_type_info_id = ".$tblastn_subtypeinfo_id." ) AND program_type_id = ".$programtype_id." AND xpath('/BlastOutput/BlastOutput_iterations/Iteration/Iteration_hits/Hit[Hit_hsps/Hsp/Hsp_evalue < ".$evalue." ]/Hit_accession/text()' , res)::text[] @> ARRAY['".$acc."'])";

$hpf = "(SELECT basic_info_id FROM results WHERE program_type_id = ".$programtype_id." AND cast((xpath('/res/DevelopmentalHPFStart/text()' , res))[1]::text as double precision ) = ".$start." AND cast((xpath('/res/DevelopmentalHPFEnd/text()' , res))[1]::text as double precision ) = ".$end." AND deletedate IS NULL)";

// $ptms, $co, $accuracy
$all_items = array();
while ($row = @pg_fetch_assoc($base)){
  if($row["cipro"] != "1"){
    if($seq && count($blast) && $_GET["pmf"]){
      $item = array ( "ID" => $row["cipro"]."|".$_GET["pmf"]."|".$accuracy."|".$ptm_arg."|".$co."|".$enzyme,
		      "Length" => intval($row["length"]),
		      "MW(calc)" => floatval($row["mw"]),
		      "pI(calc)" => floatval($row["pi"]),
		      "evalue" => floatval($blast[$row["cipro"]]),
		      "count" => intval($pmfcount[$row["id"]]));
    }else if($seq && count($blast)){
      $item = array ( "ID" => $row["cipro"],
		      "Length" => intval($row["length"]),
		      "MW(calc)" => floatval($row["mw"]),
		      "pI(calc)" => floatval($row["pi"]),
		      "evalue" => floatval($blast[$row["cipro"]]));
    }else if($_GET["pmf"]){
      $item = array ( "ID" => $row["cipro"]."|".$_GET["pmf"]."|".$accuracy."|".$ptm_arg."|".$co."|".$enzyme,
		      "Length" => intval($row["length"]),
		      "MW(calc)" => floatval($row["mw"]),
		      "pI(calc)" => floatval($row["pi"]),
		      "count" => intval($pmfcount[$row["id"]]));
    }else{
      $item = array ( "ID" => $row["cipro"],
		      "Length" => intval($row["length"]),
		      "MW(calc)" => floatval($row["mw"]),
		      "pI(calc)" => floatval($row["pi"]));
    }
    //$content = explode(',',preg_replace('/\{|\}/','', $row["content"]));
    array_push($all_items, $item);
  }
}

$tasks = array( "identifier" => 'ID',
		"items" => $all_items);
print(json_encode($tasks));
/*
    }elsif(defined param('start_time') && defined param('end_time')){
        $countlist = $serv->countByDevelopmentalHPF($startTime,$endTime);
*/

?>

