CREATE VIEW randtype AS
     SELECT program_type.id, program_type.type
     FROM program_type
     ORDER BY random()
     LIMIT 1
;
CREATE VIEW randimg2 AS
WITH s as (
     SELECT r.id AS rid,t.type AS ptype,r.basic_info_id,r.sub_type_info_id
     FROM randtype t
     JOIN results r ON t.id=r.program_type_id
     JOIN picture p ON r.id=p.results_id
     WHERE r.deletedate IS NULL
     AND p.deletedate IS NULL
     ORDER BY random() LIMIT 1
)
SELECT b.cipro AS cipro_id, rid, ptype, sub.type AS stype
   FROM s
   LEFT JOIN basic_info b ON b.id = s.basic_info_id
   LEFT JOIN sub_type_info sub ON sub.id = s.sub_type_info_id
;
