<?php
require("dbinfo.php");
$id = $_GET["id"];

$connection=pg_connect ("host=$hostspec dbname=$database user=$username password=$password");

$xref = pg_query("select t2.xid,t2.ename,t2.kname,t2.j1name,t2.amname,t2.ainame,t2.scaffold,t2.direction,array_agg(cast(((t2.start[i]-t3[1])/t3[2])*100 as integer) || ':' || cast(((t2.end[i]-t3[1])/t3[2])*100 as integer)) as range from (select (select array[min(t4.i::numeric),max(t4.i::numeric)-min(t4.i::numeric)] from (select unnest(xpath('//CDS_start/text()',data)::text[] || xpath('//CDS_end/text()',data)::text[]) as i from reference_db where external_db_id in (3,5,6,8) AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null) as t4) as t3,t1.start,t1.end,t1.xid,t1.ename,t1.kname,t1.j1name,t1.amname,t1.ainame,t1.scaffold,t1.direction,generate_series(array_lower(t1.start,1),array_upper(t1.start,1)) as i from (select xid, (xpath('//ensembl_gene_name/text()' , data))[1]::text as ename, (xpath('//kyotograil_id/text()',data))[1]::text as kname,(xpath('//jgiv1_gene_name/text()',data))[1]::text as j1name,(xpath('//Gene_name_by_manual_annotation/text()',data))[1]::text as amname,(xpath('//Gene_name_inferred/text()',data))[1]::text as ainame,(xpath('//scaffold/text()',data))[1]::text as scaffold,(xpath('//CDS/@direction',data))[1]::text as direction,cast(xpath('//CDS_start/text()',data)::text as numeric[]) as start,cast(xpath('//CDS_end/text()',data)::text as numeric[]) as end from reference_db where external_db_id in (3,5,6,8) AND id in (select reference_db_id from basic_info_reference_db_link where basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$id' AND deletedate IS NULL limit 1)) AND deletedate is null) as t1) as t2 group by xid,start,kname,ename,j1name,amname,ainame,scaffold,direction order by start;");


/*
$aniseed  mname, iname, pubmed
$jgi1 name
$ensembl name
$kg name
*/

$all_items = array();
$count = 1;
while ($row = @pg_fetch_assoc($xref)){
  $type;
  $db;
  $direction;
  $range;
  $name;
  if(preg_match("/ci/",$row["xid"])){
    $type = "jgi1";
    $db = "JGIv1";
    $name = $row["j1name"];
  }else if(preg_match("/ENSCINT/",$row["xid"])){
    $type = "ensembl";
    $db = "Ensembl";
    $name = $row["ename"];
  }else if(preg_match("/aniseed/",$row["xid"])){
    $type = "aniseed";
    $db = "Aniseed";
    $name = '';
    if($row["amname"] != false){
      $name = $row["amname"];
    }
    if($row["ainame"] != false){
      $name .= ' ('.$row["ainame"].')';
    }
  }else{
    $type = "kg";
    $name = $row["kname"];
    if(preg_match("/KH/",$row["xid"])){
      $db = "KH";
    }else{
      $db = "KG";
      $row["xid"] = "KYOTOGRAIL2005.".$row["xid"];
    }
  }
  if($row["direction"] == "+"){
    $direction = "f";
  }else if($row["direction"] == "-"){
    $direction = "r";
  }else{
    $direction = "n";
  }
  $item = array ("id" => $count++,"type" => $type,"db" => $db,"xid"=>$row["xid"],"name" => $name,
		 "range" => preg_replace('/\{|\}/','', $row["range"]),"direction"=>$direction);
  array_push($all_items, $item);
}

$tasks = array( "identifier" => 'id',
                "items" => $all_items);
print(json_encode($tasks));

?>

