#!/usr/bin/perl
# q.cgi - query and print info for an $ID in table $NAME

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::Res;

$SUB  = param 's';
$NAME = param 't';
$ID  = param 'id';
$HTML = param 'html';

$serv = CIPRO::Service::Res->new();
$list = $serv->searchByCiproId($ID,$NAME,$SUB);
if(length($list) == 0){
    $list = $serv->searchReserveByCiproId($ID,$NAME,$SUB);
    print header, start_html(-title=>"CIPRO$id - $NAME - $SUB");
    print h3($NAME);
    print $list;
    print end_html;
}else{
#    print header({-type=>'text/xml'});
    print '<?xml version="1.0" encoding="Shift_JIS" ?>';
    print '<?xml-stylesheet type="text/xsl" href="blast.xsl"?>';
    print $list;
}

