#!/usr/bin/perl

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::Seq;
use CIPRO::Service::Perms;

our %MW = qw/
	G   57.0214 A   71.0371 V   99.0684 L  113.0840 I  113.0841
	P   97.0528 M  131.0405 F  147.0684 W  186.0793 S   87.0320
	T  101.0477 N  114.0429 Q  128.0586 C  103.0092 D  115.0269
	E  129.0426 K  128.0950 H  137.0589 R  156.1011 Y  163.0633
/;
our @MW=sort keys %MW;
our @AA = (
  [qw/G P T E/], [qw/A M N K/], [qw/V F Q H/], [qw/L W C R/], [qw/I S D Y/],
);
our %mMW = qw/
	n   42.0106 o   15.9949 p   79.9663 a   71.0370 c   57.0214
	m   58.0054 e  105.0578
/;
our $START=&now;
our $start = time;
our $Nproc=0;
my (@f,@f2,@line,%seq,@id,%IDoccurrence,%includedWeight,%score,%pweight,%pi,$q,$acc,%mod);
my (@keratinFound,%keratinSuspicious,%trypsin);
my ($dbh,$sth,$k,$SEQ,$dbh2,$sth2,@presult,%argWeight,%resultMod);
my $defaultFilter=['n','o','p','a','c','m','e'];
my @filter,@exclusion;
my $mod=['Protein N-terminus Acetylated (n)',
		'Oxidation of M (o)',
		'Phosphorylation of S, T and Y (p)',
		'Acrylamide Modified Cys (a)'];
my $cmod=['None',
		'Carbamidomethylation of C (c)',
		'Carboxymethylation of C (m)',
		'Pyridylethylation of C (e)'];
my $contam=['Keratin'];
my $tryp=['None','Porcine(Major)','Porcine(All)','Bovine'];
my $app=['10','20','50'];
our @keratinWeight=qw/ 517.2986 533.2571 547.3026 550.2405 559.3567 576.3103 579.256 603.3716
	604.3052 629.3045 639.2482 698.3294 712.374 811.4353 906.4684 949.4598 954.4352
	968.4952 972.4028 973.4952 974.5157 997.4599 1011.4534 1016.5011 1024.4949 
	1071.6047 1080.5324 1127.5042 1165.4793 1192.6212 1263.6946 1377.6757 1421.678
	1538.7168 1593.7725 1828.9441 2043.9943 2075.0657 2244.0314 2415.302 3752.8575
	6428.7799/;
our @porcineAll=qw/ 451.1941 462.22 515.3306 569.2684 633.2844 650.315 664.367 694.39 
	711.3136 732.433 748.4279 759.3889 792.462 802.4311 820.4933 842.51 870.5412 906.5049 
	920.4842 934.5362 955.5001 964.558 968.4511 980.4875 1006.4879 1006.5209 1008.5188 
	1023.4747 1044.5478 1045.5642 1126.5645 1153.575 1169.5699 1175.6346 1178.5264 
	1191.6295 1309.6388 1335.4946 1337.5102 1378.6578 1390.6854 1420.7225 1469.731 
	1515.8456 1531.8405 1537.6416 1539.6572 1543.8768 1559.8718 1649.8221 1694.8325 
	1698.8737 1726.9049 1736.843 1766.7842 1768.7998 1808.8754 1836.9066 1940.9354 
	1955.9537 1959.0473 1975.0422 1987.0785 2003.0734 2010.9443 2038.9755 2158.0313 
	2166.029 2174.0262 2211.1046 2225.1202 2239.1358 2283.1807 2299.1756 2435.1587 
	2455.1849 2457.2005 2470.0512 2472.0668 2483.2161 2485.2317 2486.0461 2488.0617 
	2536.1502 2538.1658 2624.3295 2634.3561 2650.351 2652.3607 2662.3873 2678.3822 
	2707.4168 2807.3145 2914.5062 3013.3243 3094.6246 3110.6195 3143.4852 3159.4801 
	3161.4957 3171.5164 3173.532 3187.5113 3189.5269 3217.4968 3219.5125 3245.5281 
	3247.5437 3309.7265 3325.7214 3337.7577 3353.7526 3900.8108 3928.842 4475.1011 
	4475.2669 4491.2618 4503.2981 4519.293 4718.2343 4746.2655 5152.3372 5168.3321 
	5180.3684 5196.3633 5557.8751 5573.87 /;
our @porcineMajor=qw/515.3306 842.51 870.5412 1045.5642 1126.5645 1420.7225 1531.8405 
	1940.9354 2003.0734 2211.1046 2225.1202 2239.1358 2283.1807 2299.1756 2678.3822 
	2807.3145 2914.5062 3094.6246 3337.7577 3353.7526/;
our @bovine=qw/259.19 362.2 632.31 658.38 804.41 905.5 1019.5 1110.55 1152.57 1432.71 
	1494.61 2162.05 2192.99 2272.15 2551.24 4550.12/;

sub now { sprintf '%02d:%02d:%02d', (localtime)[2,1,0] }
sub TESTSEQ	{ my $a=join "", <DATA>; $a=~s/\s//g; $a }
sub NBSP ($)	{ "&nbsp;" x $_[0] }
sub LMinMW ()	{ [ qw/0 100 200 300 400 500 600 800 1000/ ] }
sub LMaxMW ()	{ [ qw/1000 2000 3000 4000 5000 6000 8000 10000/ ] }
sub DMinMW ()	{ 500 }
sub DMaxMW ()	{ 8000 }
sub RA()	{[qw/2 3 4/]}
sub dRA()	{0.2}
sub selected() {'No modification'}
sub defTrypsin() {'None'}
sub sample() {"528.1317\n565.3296\n775.3584\n846.312\n874.4621\n933.4781\n972.5010
1006.5109\n1038.5129\n1130.5192\n1135.5838\n1450.6871
1606.7079\n1797.9095\n1801.81\n1970.77\n2127.1361\n2022.9766
2460.3838\n3114.346"}
sub min ($$)    { $_[0] > $_[1]? $_[1] : $_[0] }
sub MISNUM ()	{ min param('misnum'), 1 }
#sub MISNUM ()	{0}
sub H2O()	{ 18.0105 }
sub H  ()	{  1.0078 }
sub C_MOD()	{ qw/c m e/ }


# Direct search
sub directSearch (\@$){
	my ($arg,$acc)=@_;
	my $diff=$acc;
	my @resultArray;
	my @mods=param('mod');
	my $cmod=param('cmod');
	my @contam=param('contam');
	my $tryp=param('trypsin');
	my @trypsin;
	my $flag2,$modflag,$keratin=0;
	
	if($cmod ne 'No modification'){
		push @mods,$cmod;
	}
	
	for $i (@mods){
		push @exclusion,$i;
	}
	
	if($tryp ne 'None'){
		push @contam,$tryp;
	}
	
	for $i (@$defaultFilter){
		$flag2=1;
		for $j (@exclusion){
			if($j=~/$i\)$/){
				$flag2=0;
			}
		}
		if($flag2==1){
			push @filter,$i;
		}
	}
	
	if(@contam>0){
		for $i (@contam){
			if($i=~m/Keratin/){
				$keratin=1;
			}
			if($i=~m/Major/){
				@trypsin=@porcineMajor;
			}elsif($i=~m/All/){
				@trypsin=@porcineAll;
			}elsif($i=~m/Bovine/){
				@trypsin=@bovine;
			}
		}
		
		for my $w (@$arg){
			for $i (keys %flag){
				$flag{$i}=1;
			}
			
			if($keratin==1){
				for $j (@keratinWeight){
					if(abs($j-$w)<=$acc){
						push @keratinFound,$j;
					}
				}
			}
			
			for $j (@trypsin){
				if(abs($j-$w)<=$acc){
					push @keratinFound,$j;
				}
			}
		}
	}

	for my $w (@$arg){
		for $i (keys %flag){
			$flag{$i}=1;
		}
		my $keratinFlag=0;
		for $i (@keratinFound){
			if(abs($i-$w)<$acc){
				$keratinFlag=1;
			}
		}
		my $serv = CIPRO::Service::Perms->new();
		my @list = $serv->searchByBetweenMw($w,$acc);
		foreach my $array (@list){
			$modflag=1;
			@resultArray=($array->[0],$array->[1],$array->[2]);
			$resultArray[0]=~s/\\//g;
			$resultArray[1]=~s/\\//g;
			$resultArray[2]=~s/\\//g;
			
			for $i (@filter){
				if($resultArray[2]=~/$i/){
					$modflag=0;
				}
			}

			if(defined $IDoccurrence{$resultArray[1]} and $flag{$resultArray[1]}==1 and $modflag==1){
				$IDoccurrence{$resultArray[1]}++;
				$score{$resultArray[1]}+=calcScore($resultArray[0],$w);
				$includedWeight{$resultArray[1]}.=','.$resultArray[0];
				$argWeight{$resultArray[1]}.=','.$w;
				if ($resultArray[2]=~/[a-z]+/){
					$resultMod{$resultArray[1]}.=','.$resultArray[2];
				}else{
					$resultMod{$resultArray[1]}.=',0';					
				}
				$flag{$resultArray[1]}=-1;
				if($keratinFlag==1){
					if(defined $keratinSuspicious{$resultArray[1]}){
						$keratinSuspicious{$resultArray[1]}++;
					}else{
						$keratinSuspicious{$resultArray[1]}=1;
					}
				}
			}elsif($flag{$resultArray[1]}==0 and $modflag==1 ){
				$IDoccurrence{$resultArray[1]}=1;
				$score{$resultArray[1]}=calcScore($resultArray[0],$w);
				$includedWeight{$resultArray[1]}=$resultArray[0];
				$argWeight{$resultArray[1]}=$w;
				if ($resultArray[2]=~/[a-z]+/){
					$resultMod{$resultArray[1]}=$resultArray[2];
				}else{
					$resultMod{$resultArray[1]}='0';					
				}
				$flag{$resultArray[1]}=-1;
				if($keratinFlag==1){
					if(defined $keratinSuspicious{$resultArray[1]}){
						$keratinSuspicious{$resultArray[1]}++;
					}else{
						$keratinSuspicious{$resultArray[1]}=1;
					}
				}
			}
			$seq{$resultArray[1]}=$resultArray[3];
		}
	}
}

sub calcScore($$){
	my ($w,$arg)=@_;
	my $score;
	
	if($w==$arg){
		$score=1000000;
	}else{
		$score=100/abs($w-$arg);
	}
	return $score;
}

## output

sub msgbox($){ 
	my $ID=shift;
	my $str="CIPRO".$ID;
	
	a({href=>"javascript:void(0)",onclick=>"addTab('$ID');return false;"},$str)
}

sub fragmentList(\@){
	my $ID=$_[0];
	my $MWFrame,$MWContent;
	my $weight,$s;
	my %iscore;
	my $nID=@$ID;
	my $f;
	my @scoreKey;
	my @contam=param('contam');
	my $tryp=param('trypsin');
	my $nca=param('appear');

	for my $i (@$ID){
		for my $j (1..@line){
			if($IDoccurrence{$i}==$j){
				$iscore{$i}=$score{$i};
			}
		}
	}
	
	@scoreKey=sort {$iscore{$b}<=>$iscore{$a}} keys %iscore;
	if(@contam>0){
		$con=$contam[0];
	}
	if($tryp ne 'None'){
		$con.=",".$tryp;
	}
	$con=~s/^,//;
	
	for my $i (0..$nca-1){
		my ($resultWeight,$resultArg,$modSeq);
		my @modArray=split /\,/,$resultMod{$$ID[$i]};
		my @argArray=split /\,/,$argWeight{$$ID[$i]};
		my @result=split /\,/,$includedWeight{$$ID[$i]};
		
		for my $j (0..$#result){
			if ($j==$#result){
				$resultWeight.=$result[$j];
			}else{
				$resultWeight.=$result[$j].',';
			}
		}
		for my $j (0..$#argArray){
			if ($j==$#argArray){
				$resultArg.=$argArray[$j];
			}else{
				$resultArg.=$argArray[$j].',';
			}
		}
		for my $j (0..$#modArray){
			if($j==$#modArray){
				$modSeq.=$modArray[$j];
			}else{
				$modSeq.=$modArray[$j].',';				
			}
		}
		
		$weight=sprintf("%.2f",$pweight{$$ID[$i]}/1000);
		$s=sprintf("%.2f",$iscore{$scoreKey[$i]});
		my $j=$$ID[$i];
		$j=~s/x+/x/;
		if($j=~m/x$/){
			$weight.='+';
		}
		$MWContent.=start_form({-method=>'POST',-id=>"permsdetailform$i"});
		$MWContent.=($i+1).'. '.msgbox($j).br;
		if(defined $keratinSuspicious{$$ID[$i]}){
			$MWContent.=NBSP(10).'#matched fragments: '.$IDoccurrence{$$ID[$i]};
			if($keratinSuspicious{$$ID[$i]}==1){
				$MWContent.=font({color=>'red'}, ' (including 1 suspicious fragment)').br;				
			}else{
				$MWContent.=font({color=>'red'}, ' (including '.$keratinSuspicious{$$ID[$i]}.' suspicious fragments)').br;
			}
		}else{
			$MWContent.=NBSP(10).'#matched fragments: '.$IDoccurrence{$$ID[$i]}.br;
		}
		$MWContent.=NBSP(10).'score: '.$s.br;
		$MWContent.=NBSP(10).'pI: '.$pi{$$ID[$i]}.br;
		$MWContent.=NBSP(10).'MW: '.$weight.' kDa';
		$MWContent.=hidden('id',$$ID[$i]);
		$MWContent.=hidden('weight',$resultWeight);			
		$MWContent.=hidden('arg',$resultArg);
		$MWContent.=hidden('mods',$modSeq);
		$MWContent.=hidden('pi',$pi{$$ID[$i]});
		$MWContent.=hidden('score',$score{$$ID[$i]});
		$MWContent.=hidden('mw',$weight);
		if(defined $keratinSuspicious{$$ID[$i]}){
			$contamWeight=$keratinFound[0];
			for $j (1..$#keratinFound){
				$contamWeight.=','.$keratinFound[$j];
			}
			$MWContent.=hidden('contam',$contamWeight);
			$MWContent.=hidden('acc',$acc);
			$MWContent.=hidden('contflag',$con);
		}
#		$MWContent.=NBSP(10).submit('Detail');
		$MWContent.=NBSP(10).button(-value=>'Detail',-id=>'detailbutton',-onclick=>"showPermsDetail($i)");
		$MWContent.=end_form.p;
	}
	$MWFrame=$MWContent;
	
	for $i (@filter){
		$f.=$i;
	}
	b('Filter: without [',$f,']'),p,
	$MWFrame,hr,
	"$nID proteins are found."	
}

sub find(){
	my @array,$pI;
		
	$q=param('query');
	$q=~s/[\n]+/\,/g;
	$q=~s/\s+/\,/g;
	$q=~s/\,+/\,/g;
	$q=~s/[a-zA-Z]//g;
	@line=split(/\,/,$q);	
	$acc=param('accuracy');
	directSearch(@line,$acc);
	$msf=param('msf');
	$mst=param('mst');
	$pif=param('pif');
	$pit=param('pit');
	
	foreach $k (sort {$IDoccurrence{$b}<=>$IDoccurrence{$a}} keys %IDoccurrence){
		@f=split /\,/,$includedWeight{$k};
		
		foreach $f (@f){
			@f2=split /\&/,$f;
			$mod{$k}{$f2[0]}=$f2[1];
		}
   	 	
   	 	$flag=1;
   	 	my $query=$k;
   	 	$query=~s/CIPRO//;
   	 	$query=~s/x+//;
   	 	my $serv = CIPRO::Service::Seq->new();
   	 	my @list = $serv->searchByCiproId($query);
		$weight = $list[0][1];
		$pI = $list[0][2];

		if($weight<$msf*1000 or $weight>$mst*1000){
			$flag=0;
		}else{
			$pweight{$k}=$weight;
		}
		
		if($pI<$pif or $pI>$pit){
			$flag=0;
		}else{
			$pi{$k}=$pI;
		}
		
   	 	if($flag==1){
   	 		push @array,$k;
   	 	}
	}

  if(@array > 0){
	div({-id=>'content'},
	&fragmentList(\@array)
		),
  }else{
	div( "No hits found"),
  }
}




##==== Cystein modifications
# (a) Acrylamidation
# (c) Carbamidomethylation, (m) Carboxymethylation, (e) Pyridylethylation
#   c, m, e are exclusive each others
#   a can be combinatorial with other three in any numbers
#   when modified a+[cme] need to be modified in full

sub cMod(@) { map c_mod($_), @_ }
sub c_mod($) {
  my $f = shift;
  my $c = $f =~ tr/C//;
  my @r=(''); # non-modified
  if ($c) {
    for my $m (C_MOD) {
      push @r, map {('a' x $_).($m x ($c-$_))} 0..$c
    }
  }
  @r
}


## N-terminal acetylation
sub nMod(@) { $_[0]=~/n/ and 'n' }

##################################################
##
##  Main
##
##################################################
main:{
    print start_html(-title=>'PerMS Server',
                     -script=>[
                               {-type=>"text/javascript",
                                -src=>"http://ajax.googleapis.com/ajax/libs/dojo/1.4.0/dojo/dojo.xd.js"},
                               {-type=>"text/javascript",
                                -src=>"./js/cipro.js"}]),
          table( {valign=>"top",width=>"100%",height=>"100%"},
                 Tr({-valign=>"top"}, td({-valign=>"top"},find) )
                ),
# DEBUG ---------------------------------------------
#          table( 
#                 Tr(td('others'),td(param('others'))),
#                 Tr(td('mod'),td(param('mod'))),
#                 Tr(td('cmod'),td(param('cmod'))),
#                 Tr(td('contam'),td(param('contam'))),
#                 Tr(td('trypsin'),td(param('trypsin'))),
#                 Tr(td('appear'),td(param('appear'))),
#                 Tr(td('query'),td(param('query'))),
#                 Tr(td('accuracy'),td(param('accuracy'))),
#                 Tr(td('msf'),td(param('msf'))),
#                 Tr(td('mst'),td(param('mst'))),
#                 Tr(td('pif'),td(param('pif'))),
#                 Tr(td('pit'),td(param('pit')))),
# DEBUG ---------------------------------------------
          end_html;
}

__DATA__
MSVETLMRSKNLLPVQVIASVPSNPMGSSQSGQKAAFEEALHNSIESSLRSVAVAPSSVFSTLFLS
LRPASVVNKDETADIKPNSEQLNEELNKQNASRQESTNEPQNSLLAAKFPHTNQSPLNSSNPSILSLQSGTSPTPVCRPP
ALIPASHVRHTNFPHNVATSTSLPPVPRPPMTLMAQQQQQLEARVEQLKQLQRLHAWKQLHKDKPLHVMPGSEMSASGNN
QQSNLPAVLGGSGMSVQPPAPIVLPSNSINGMYSNGNFHGTRITGSKLPKGFCKSGKDIRLTTLENDSSFMVPCPNEYVL
LALQTSASSSSDNLVIIAINLKSLPPPNSSSQALSVQCVGCGEKAIHTFIQLVEHVGLDYSEVANAHLKTNSSHLIGAHA
RFYLVRDRNNARHLIRGPALMWNKQLEAGRVRSSIPVPRFSVMQNPMFRNQNTMLPTPTHFTSPKSSHPPLVQSLSGPTY
SRGIYPLKRKASKSSIEIFEDEPRRYTLNKEDASSDIVIPEDLALTLQRKLIIMLLHPLNQQPRLSGDVASVVVTSSHHP
LHTSLVACELIIQHYITALIIAFSEESERRKLRMDLTSVYSALTERIVEVNLRMAFEDGNRLTRSKLEMELSLAASVCHP
NLQTKTFKSDPDDDGNNLHVIESDDAQSDKDGEGKTSGWESKEETQFDSRLEPLKRKRPRMDCVESDFISAAMRDANLQT
TDNGKETKCNPVAPKVVLPKRIYDILKSNNEGTTDVTSLLPCQLSSWSETLHPILDSRTTNPDQVAAYYRKWTKPLPGHP
DYNDSSDTLDENENQISQIDPYRVLLTTPPNSNPVFICCNFLKQLHRSLMKMLEIEVYDDVEDRPMISREATFHTLLGNP
VSTERSQITNSCYDLQQLHVRKPSVVGNNRKVVTISSNINHAFNVRRGKENVTLDDAENSSSSTYEVNLAPSSSWSYKGS
LNLRLIIPVKSRSLFNFIKCEDGSHKLRSIRLREQSYDGLKTPIVSFGNNGAAEGLYNLRGTLDTNNDDVNHLHMVVVKE
CDEFSYTKLLPNHVLLVLPQDFDVLGRGAMKFAAQALFTHFYDVSIGDEASDTVWPFVMMMNDDCVAWSSWKPSEESISN
LADEWSDISLKELLENVESSLTSEVAAVGFRPWSTDLTRPNSAENLVDKNSKFFGAFILILLKKQDAEICYYAYMFNWED
LDWSLRLAAAGLPVQRFENIALCTKVMPAVKSHEILNSEESVLVTAADSEFVPFSFHPSLLLEKHFNKNAASVFPLSQSN
PLNPVLCIDCYISLGTDILVEFTSSDPSAPDWALDYNKLYGGLLLYCPASTVTFDFLLKYQFAEGATVCVIGHDRSSLRQ
TVVKLNLESEWHFRLTDEFLTANHTVLSGSNVRPLYFLTGMHKNP
