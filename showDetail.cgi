#!/usr/bin/perl

use DBI;
use CGI qw/:all/;
use CGI::Carp;

my @queries = split(/\,/, param('query'));
my $ID = param('id');
my @mods = split(/\,/, param('ptms'));
my @mod_cands = qw/n o p a c m e/;
my $ker = param('contami');
my $enzyme = param('enzyme');
my $accuracy = param('accuracy');
my $dbh = DBI->connect("dbi:Pg:dbname = cipro_v2.5; host = cipro6.ibio.jp", "postgres", "", {AutoCommit => 0});
our @keratin = qw/ 517.2986 533.2571 547.3026 550.2405 559.3567 576.3103 579.256 603.3716
	604.3052 629.3045 639.2482 698.3294 712.374 811.4353 906.4684 949.4598 954.4352
	968.4952 972.4028 973.4952 974.5157 997.4599 1011.4534 1016.5011 1024.4949 
	1071.6047 1080.5324 1127.5042 1165.4793 1192.6212 1263.6946 1377.6757 1421.678
	1538.7168 1593.7725 1828.9441 2043.9943 2075.0657 2244.0314 2415.302 3752.8575
	6428.7799/;
our @porcineAll = qw/ 451.1941 462.22 515.3306 569.2684 633.2844 650.315 664.367 694.39 
	711.3136 732.433 748.4279 759.3889 792.462 802.4311 820.4933 842.51 870.5412 906.5049 
	920.4842 934.5362 955.5001 964.558 968.4511 980.4875 1006.4879 1006.5209 1008.5188 
	1023.4747 1044.5478 1045.5642 1126.5645 1153.575 1169.5699 1175.6346 1178.5264 
	1191.6295 1309.6388 1335.4946 1337.5102 1378.6578 1390.6854 1420.7225 1469.731 
	1515.8456 1531.8405 1537.6416 1539.6572 1543.8768 1559.8718 1649.8221 1694.8325 
	1698.8737 1726.9049 1736.843 1766.7842 1768.7998 1808.8754 1836.9066 1940.9354 
	1955.9537 1959.0473 1975.0422 1987.0785 2003.0734 2010.9443 2038.9755 2158.0313 
	2166.029 2174.0262 2211.1046 2225.1202 2239.1358 2283.1807 2299.1756 2435.1587 
	2455.1849 2457.2005 2470.0512 2472.0668 2483.2161 2485.2317 2486.0461 2488.0617 
	2536.1502 2538.1658 2624.3295 2634.3561 2650.351 2652.3607 2662.3873 2678.3822 
	2707.4168 2807.3145 2914.5062 3013.3243 3094.6246 3110.6195 3143.4852 3159.4801 
	3161.4957 3171.5164 3173.532 3187.5113 3189.5269 3217.4968 3219.5125 3245.5281 
	3247.5437 3309.7265 3325.7214 3337.7577 3353.7526 3900.8108 3928.842 4475.1011 
	4475.2669 4491.2618 4503.2981 4519.293 4718.2343 4746.2655 5152.3372 5168.3321 
	5180.3684 5196.3633 5557.8751 5573.87 /;
our @porcineMajor = qw/515.3306 842.51 870.5412 1045.5642 1126.5645 1420.7225 1531.8405 
	1940.9354 2003.0734 2211.1046 2225.1202 2239.1358 2283.1807 2299.1756 2678.3822 
	2807.3145 2914.5062 3094.6246 3337.7577 3353.7526/;
our @bovine = qw/259.19 362.2 632.31 658.38 804.41 905.5 1019.5 1110.55 1152.57 1432.71 
	1494.61 2162.05 2192.99 2272.15 2551.24 4550.12/;
my %MW = qw/
	G   57.02 A   71.04 V   99.07 L  113.08 I  113.08
	P   97.05 M  131.04 F  147.07 W  186.08 S   87.03
	T  101.05 N  114.04 Q  128.06 C  103.01 D  115.03
	E  129.04 K  128.10 H  137.06 R  156.10 Y  163.06
/;
my %mMW = qw/
	n   42.01 o   15.99 p   79.97 a   71.04 c   57.02
	m   58.01 e  105.06
/;	

my @overlaps;

my $sth = $dbh->prepare("SELECT seq FROM seq_info WHERE basic_info_id = (SELECT id FROM basic_info WHERE cipro = '$ID')");
$sth->execute;
my $seq = $sth->fetch;

my @peptides = degradation($$seq[0]);
my $column;
for my $query (@queries) {
	my $res = matchWeight($query, @peptides);
	if($res =~ m/\|/) {
		my @elm = split(/\|/, $res);
		my (%nmods, $mod_elm, $c, @contams, $contam_flag);
		if(defined $elm[2]) {
			for my $m (@mod_cands) {
				for my $mm (split(/ */, $elm[2])) {
					if($m eq $mm) {
						$nmods{$m}++;
					}
				}
				if(defined $nmods{$m} and $nmods{$m} > 0) {
					$mod_elm .= td($nmods{$m});
				} else {
					$mod_elm .= td('&nbsp;');
				}
			}			
		} else {
			$mod_elm = (td('&nbsp;'))x7;
		}
		
		if(defined $ker) {
			push(@contams, @keratin);
		}
		if(defined $enzyme and $enzyme eq 'Porcine(All)') {
			push(@contams, @porcineAll);
		} elsif(defined $enzyme and $enzyme eq 'Porcine(Major)') {
			push(@contams, @porcineMajor);
		} elsif(defined $enzyme and $enzyme eq 'Bovine') {
			push(@contams, @bovine);
		}
		
		for my $contam (@contams) {
			if(abs($query-$contam) <= $accuracy) {
				$contam_flag = 1;
				last;
			}
		}
		
		if(defined $contam_flag) {
			$column .= Tr(td($query), td($elm[0]), td({align => "center"}, $elm[1]), $mod_elm, td(calcScore($query, $elm[0])), td('suspicious'));			
		} else {
			$column .= Tr(td($query), td($elm[0]), td({align => "center"}, $elm[1]), $mod_elm, td(calcScore($query, $elm[0])), td('&nbsp;'));						
		}
	} 
}

sub H2O()	{ 18.01 }

sub H  ()	{  1.01 }

sub degradation ($) {
	my @peptides;
	my @aas = split(/ */, $_[0]);
	my $last = 0; 
	
	for my $i (0..$#aas) {
		if((($aas[$i] eq 'K' or $aas[$i] eq 'R') and $aas[$i+1] ne 'P') or $i == $#aas) {
			push(@peptides, substr($_[0], $last, $i-$last+1));
			$last = $i + 1;
		}
	}
	
	return @peptides;
}

sub modification ($) {
	my $seq = $_[0];
	my (@mod_weights, @res_mods);
	for my $mod (@mods) {
		if($mod =~ m/a/) {
			my $actylamidation = $seq =~ tr/C/C/;
			if($actylamidation > 0) {
				if($mod =~ m/c/) {
					for my $i (1..$actylamidation) {
						for my $j (0..$i) {
							push(@mod_weights, $mMW{'a'} * $j + $mMW{'c'} * ($i-$j));
							push(@res_mods, ('a'x$j).('c'x($i-$j)));
						}
					}
				} elsif($mod =~ m/m/) {
					for my $i (1..$actylamidation) {
						for my $j (0..$i) {
							push(@mod_weights, $mMW{'a'} * $j + $mMW{'m'} * ($i-$j));
							push(@res_mods, ('a'x$j).('m'x($i-$j)));
						}
					}					
				} elsif($mod =~ m/e/) {
					for my $i (1..$actylamidation) {
						for my $j (0..$i) {
							push(@mod_weights, $mMW{'a'} * $j + $mMW{'e'} * ($i-$j));
							push(@res_mods, ('a'x$j).('e'x($i-$j)));
						}
					}					
				}
			}
		} else {
			my $reagent = $seq =~ tr/C/C/;
			if($mod =~ m/c/) {
				for my $i (1..$reagent) {
					push(@mod_weights, $mMW{'c'} * $i);
					push(@res_mods, 'c'x$i);
				}
			} elsif($mod =~ m/m/) {
				for my $i (1..$reagent) {
					push(@mod_weights, $mMW{'m'} * $i);
					push(@res_mods, 'm'x$i);
				}
			} elsif($mod =~ m/e/) {
				for my $i (1..$reagent) {
					push(@mod_weights, $mMW{'e'} * $i);
					push(@res_mods, 'e'x$i);
				}				
			}
		}

		if($mod =~ m/n/) {
			my $current_weights = $#mod_weight;
			for my $i (0..$current_weights) {
				$mod_weights[$current_weights+$i] = $mod_weight[$i] + $mMW{'n'};
				$res_mods[$current_weights+$i] = 'n'.$res_mods[$i];
			}
			push(@mod_weights, $mMW{'n'});
		} 
		if($mod =~ m/o/) {
			my $oxidization = $seq =~ tr/M/M/;
			my $current_weights = $#mod_weight;
			for my $i (0..$current_weights) {
				for my $j (1..$oxidization) {
					$mod_weights[$current_weights+$i*$oxidization+$j] = $mod_weights[$i] + $mMW{'o'} * $j;
					$res_mods[$current_weights+$i*$oxidization+$j] = $res_mods[$i].('o'x$j);
				}
			}
			
			for my $i (1..$oxidization) {
				push(@mod_weights, $mMW{'o'} * $i);
				push(@res_mods, 'o'x$i);
			}
		} 
		if($mod =~ m/p/) {
			my $phosphorylation = 0;
			my $current_weights = $#mod_weight;
			my @aas = split(/ */, $seq);
			for my $aa (@aas) {
				if($aa eq 'S' or $aa eq 'T' or $aa eq 'Y') {
					$phosphorylation++;
				}
			}
			
			for my $i (0..$current_weights) {
				for my $j (1..$phosphorylation) {
					$mod_weights[$current_weights+$i*$phosphorylation+$j] = $mod_weights[$i] + $mMW{'p'} * $j;
					$res_mods[$current_weights+$i*$phosphorylation+$j] = $res_mods[$i].('p'x$j);
				}
			}

			for my $i (1..$phosphorylation) {
				push(@mod_weights, $mMW{'p'} * $i);
				push(@res_mods, 'p'x$i);
			}
		}
		
	}
	
	return (\@mod_weights, \@res_mods);
}

sub matchWeight ($@) {
	my ($query, @peptides) = @_;
	my $res;
	my $error;
	
	for my $i (0..$#peptides) {
		my ($mod_peptides, $res_mods) = modification($peptides[$i]);
		my $peptide_weight = H2O() + H();
		
		for my $aa (split(/ */, $peptides[$i])) {
			$peptide_weight += $MW{$aa};
		}
		
		if(abs($peptide_weight-$query) <= $accuracy) {
			$res = $peptide_weight."|".$peptides[$i]."|";
			$error = abs($peptide_weight-$query);
		} elsif($#mods > 0) {
			my @mps = @$mod_peptides;
			for my $j (0..$#mps) {
				if(abs($peptide_weight+$mps[$j]-$query) <= $accuracy) {
					if(!defined $error or $peptide_weight+$mps[$j]-$query < $error) {
						$res = ($peptide_weight+$mps[$j])."|".$peptides[$i]."|".$$res_mods[$j];
						$error = abs($peptide_weight+$mps[$j]-$query);
					}
				}
			}
		}
	}
	
	if(defined $res) {
		return $res;
	} else {
		return 0;			
	}
}

sub matchSeq($$) {
	my ($peptide, $protein) = @_;
	my $start = index($protein, $peptide);
	
	return $start;
}

sub calcScore($$) {
	my ($w, $arg) = @_;
	my $score;
	
	if($w == $arg){
		$score = 1000000;
	} else {
		$score = 100/abs($w-$arg);
	}
	
	return sprintf("%.2f", $score);
}

print start_html(-title => 'PerMS Result', -style => 'css/full.css'),
#h2("Result: $ID"),
h4('Summary'),
table(Tr(th({rowspan => 2}, 'Query'), th({rowspan => 2}, 'Entry'), th({rowspan => 2}, 'Sequence'), th({colspan => 7}, 'Modification'), th({rowspan => 2}, 'Score'), th({rowspan => 2}, 'Contaminants')), Tr(th('n'), th('o'), th('p'), th('a'), th('c'), th('m'), th('e')), $column),
#h4("$ID Protein Sequence"),
#table({style => "border-spacing:0px;", -bgcolor => '#e0f0ff'}, $proteinSeq),
end_html;

$sth->finish;
$dbh->disconnect;