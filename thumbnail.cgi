#!/usr/bin/perl

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::ViewIcon;

my $results_id   = param('results_id');
# print = 1 -> PRINT PAGE SIZE , print = 0 -> ORIGINAL SIZE
my $seqno   = param('seqno');

sub open_image{
    my $results_id  = shift;
    my $seqno  = shift;
    my $serv = CIPRO::Service::ViewIcon->new();
    my $file = $serv->searchByThumbnail($results_id,$seqno);
    binmode(STDOUT);
    print $file;
}

print header({-type=>'image/png'});
open_image($results_id,$seqno);

