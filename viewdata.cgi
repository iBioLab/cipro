#!/usr/bin/perl
# viewdata.cgi

use CGI qw/:all/;
use CGI::Carp;

$ID = param('id');

sub get_summary {
    my $len = $_[0];
    my($mw,$pi) = split ",",$_[1];
    my $name = $tree->{'kyotograil'}->{'kyotograil_gene_name'};;
    my($acc,$source,$protein,$score,$evalue) = @$list;
}

sub Button{
    my $Att = shift @_;
    my $att = join " ",map{(my $i = $_)=~ s/-//;"$i=\"${$Att}{$_}\""} keys %$Att;
    my @contents = @_;
    return "<button $att>@contents</button>";
}

sub newMsg{
    a({href=>"javascript:void(0)",
       style=>"margin-left: 2px;",
       onClick=>'var newTab = new Comment({id: "new"+paneId  }).container;'.
	   'var editor = dijit.byId("new"+paneId+"_content");'.
	   'var subject = dijit.byId("new"+paneId+"_subject");'.
	   'var to = dojo.query("#"+"new"+paneId+"_to");'.
	   'var title = dijit.byId("new"+paneId+"_title");'.
	   'var name = dijit.byId("new"+paneId+"_name");'.
	   "dojo.xhrGet({url: 'php/editcomment.php',content:{id: '$ID', sid: session},".
	   " handleAs: 'json',load: function(data) {editor.onLoadDeferred.addCallback(".
	   "function(){".
	   "subject.attr('value','$ID');".
	   "title.attr('value',data.items[0].title);".
	   "to.attr('value',data.items[0].rating);".
	   "name.attr('value',data.items[0].name);".
	   "editor.attr('value', data.items[0].content);})}});".
	   'dojo.mixin(newTab,{title: "New Comment #" + paneId++,closable: true});'.
	   ' var maindiv = dijit.byId("maindiv");maindiv.addChild(newTab);maindiv.selectChild(newTab);return false;'},
	   img({src=>"images/icon/comments.png",alt=>"COMMENTS", title=>"Leave a comment",style=>"vertical-align:middle;"}),"Comments")
}

sub editMsg{
    div({-dojoType=>"dijit.Declaration", -widgetClass=>"Comment"},
	div({-dojoType=>"dijit.layout.LayoutContainer", -dojoAttachPoint=>"container",-id=>'${id}_tab',
	     -title=>"Composing...", -closeable=>"true"},
	    div({-dojoType=>"dijit.layout.LayoutContainer",-layoutAlign=>"top",
		 -style=>"overflow: visible; z-index: 10; color:#666; "},
		table({-width=>"100%"},
		      Tr(td({-style=>"padding-left: 20px; padding-right:8px; text-align:right;",nowrap},
			    label({-for=>'${id}_subject'},"CIPRO ID: ")),
			 td(Select({-dojoType=>"dijit.form.ComboBox",
				    -id=>'${id}_subject', -hasDownArrow=>"false"})),
			 td({-style=>"padding-left: 20px; padding-right:8px; text-align:right;",nowrap},
			    label({-for=>'${id}_title'},"Comment Title: ")),
			 td({-width=>"100%"},
			    Select({-dojoType=>"dijit.form.ComboBox",
				    -id=>'${id}_title', -hasDownArrow=>"false"}))),
		      Tr({-style=>"padding-top:5px;"},
			 td({-style=>"padding-left:20px; padding-right: 8px; text-align:right;"},
			    label({-for=>'${id}_to'},"Rating:")),
			 td(div({-dojoType=>"dojox.form.Rating",numStars=>"5",value=>"0",
				 onChange=>"dojo.query('#\${id}_to').attr('value', this.value);"},""),
			    input({type=>"hidden",id=>'${id}_to',value=>"0"},"")
			 ),
			 td({-style=>"padding-left: 20px; padding-right:8px; text-align:right;",nowrap},
			    label({-for=>'${id}_name'},"Your Name: ")),
			 td({-width=>"100%"},
			    Select({-dojoType=>"dijit.form.ComboBox",
				    -id=>'${id}_name', -hasDownArrow=>"false"})))),
		hr({-size=>"1",-noshade})),
	    div({-dojoType=>"dijit.layout.LayoutContainer",
		 -layoutAlign=>"bottom", -align=>"center"},
		Button({dojoType=>"dijit.form.Button",
			iconClass=>"mailIconOk",
			onclick=>'sendComment(\'${id}\');'.
			    'var maindiv = dijit.byId(\'maindiv\');'.
			    'var child = dijit.byId(\'${id}_tab\');'.
			    'var target = dijit.byId(\'${id}_subject\').getValue(false);'.
			    'maindiv.removeChild(child);dojo.hash(target);'
		       },"Send")),
	    div({-dojoType=>"dijit.layout.LayoutContainer",-layoutAlign=>"client"},
		div({-dojoType=>"dijit.Editor", -id=>'${id}_content',
		     -plugins=>"['bold','italic','|','insertImage','createLink','foreColor','hiliteColor']",
		     -styleSheets=>"http://ajax.googleapis.com/ajax/libs/dojo/1.4.3/dojo/resources/dojo.css"},
		    ""))))
}

sub printpage{
    a({href=>"javascript:void(0)",onclick=>"window.print();",
       style=>"border-left-width: 1px;border-left-style: solid;border-left-color: #E0E0E0;margin-left: 2px;padding-left: 10px;"},
      img({src=>"./images/icon/printer.png" , alt=>"PRINT PAGE",style=>"vertical-align:middle;"}),"Print")
}

print header(-expires=>'-1'),start_html(-title=>uc $ID);
print div({style=>"width: 940px;background: white;border-left: 1px solid #DDD;border-right: 1px solid #DDD;margin: 0px auto;padding: 0px 10px;width: 940px;"},
	  div({style=>"width: 620px;float: left;margin: 0px 20px 0px 0px;margin-right: 20px;"},
	      div({style=>"width: 620px;float: left;"},
		  div({style=>"width: 300px;float: left;margin: 0px 20px 0px 0px;margin-right: 20px;"},
		      h1({style=>"color: #666;"},$ID),
		      span({id=>"header$ID"},"")),
		  div({style=>"width: 300px;float: left;"},
		      br,
		      label({for=>"annotname$ID"},"Protein Name:"),
		      input({id=>"annotname$ID"}))),
	      div({style=>"width: 450px;float: left;margin: 0px 20px 0px 0px;margin-right: 20px;"},
		  div({style=>"text-data-area;width: 450px;height: 13px;background: url(images/header.gif) repeat-x 0px 2px;padding-bottom: 7px;"},""),
		  div({-dojoType=>"dijit.layout.ContentPane",id=>"description$ID",
		       style=>"width: 450px;border-bottom-color: #CCC; border-bottom-style: dotted;border-bottom-width: 1px;margin-bottom: 7px;"},
		      div({id=>"${ID}_maintext"},""),
		      div({id=>"xref$ID"},""),
		      div({id=>"savignyi$ID"},""),
		      div({id=>"ncbi$ID"},""),
		      div({id=>"blastp25$ID",align=>"center"},""),
		      div({id=>"mim$ID"},""),
		      div({id=>"interpro$ID"},""),
		      div({id=>"pubmed$ID"},""),
		      div({id=>"go$ID"},""),
		      div({id=>"extra$ID"},"")),
		  newMsg,printpage,br,br),
	      div({style=>"width: 150px;float: left;"},
		  div({id=>"xseq$ID"},""),
		  div({style=>"text-data-area;width: 150px;height: 13px;background: url(images/header.gif) repeat-x 0px 2px;padding-bottom: 7px;"},""),
		  div({style=>"width: 150px;border-bottom-color: #CCC; border-bottom-style: dotted;border-bottom-width: 1px;padding-bottom: 7px;".
			   "margin-bottom: 7px;"},
		      label({for=>"uname$ID"},"Your Name:"),
		      input({id=>"uname$ID"}),
		      label({for=>"cname$ID"},"Recommended Name:"),
		      input({id=>"cname$ID"}),
		      a({href=>"javascript:void(0)",onclick=>"sendAnnotName('$ID');"},
			img({src=>"images/icon/submit.gif",style=>"margin-top: 7px;"})),
		      a({href=>"javascript:void(0)",onclick=>"deleteAnnotName('$ID');"},
			img({src=>"images/icon/delete.gif",style=>"margin-top: 7px;"}))),
		  div({style=>"padding-bottom: 7px;"},newMsg,printpage),
		  div({id=>"wolfpsort$ID"},""),
		  div({id=>"tmhmm$ID"},""),
		  div({id=>"modeller$ID"},""),
		  div({id=>"psipred$ID"},""),
		  div({id=>"omim$ID"},""),
		  div({id => "msms$ID"}, ""),
		  div({id => "phylogeny$ID"}, ""))),
	  div({style=>"width: 300px;float: left;"},
	      div({id=>"sds$ID"},""),
	      div({id=>"faba$ID"},""),
	      div({id=>"expression$ID"},""),
	      div({id=>"blastp$ID"},"")),
	  editMsg,
	  div({id=>"${ID}_related",style=>"width:940px;float: left;"},""),
	  div({id=>"${ID}_comment",style=>"width:940px;float: left;"},""),
	  div({id=>"${ID}_history",style=>"width:940px;float: left;"},""));
print end_html;
