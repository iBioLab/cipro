#!/usr/bin/perl

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::ViewIcon;

my $ID = param('id');
my $NAME = param('c');
my $SEQ = param('seq')||'1';
my $THUMBNAIL_FLAG = param('tf');
my $SUBTYPE = '';

sub open_image{
    my $id  = shift;
    my $prg = shift;
    my $sub = shift;
    my $seq = shift;
    my $thumb = shift;
    my $serv = CIPRO::Service::ViewIcon->new();
    my $file = $serv->searchByIcon($id,$prg,$sub,$seq,$thumb);

    binmode(STDOUT);
    print $file;
}

if($NAME eq 'Sequence and Motif'){
    $SUBTYPE = 'psipred';
}elsif($NAME eq 'Expression Profile'){
    $SUBTYPE = 'integrated graph';
}elsif($NAME eq 'Structural Model'){
    $SUBTYPE = 'modeller';
}elsif($NAME eq 'BLAST'){
    $SUBTYPE = 'nr';
}elsif($NAME eq 'TM Prediction'){
    $SUBTYPE = 'sosui';
}elsif($NAME eq 'Localization'){
    $SUBTYPE = 'wolfpsort';
}elsif($NAME eq '2D PAGE'){
    $SUBTYPE = '';
}elsif($NAME eq '3DPL'){
    $SUBTYPE = '';
}elsif($NAME eq 'SNP'){
    $SUBTYPE = '';
    $NAME='OMIM';
}elsif($NAME eq 'OMIM'){
    $SUBTYPE = '';
}else{
    $NAME = '';
    $SUBTYPE = '';
}

print header({-type=>'image/png'});
open_image($ID,$NAME,$SUBTYPE,$SEQ,$THUMBNAIL_FLAG);

