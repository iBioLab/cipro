#!/usr/bin/perl

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::ViewIcon;

my $PICTUREID   = param('id');
# print = 1 -> PRINT PAGE SIZE , print = 0 -> ORIGINAL SIZE
my $PRINT   = param('print')||0;

sub open_image{
    my $id  = shift;
    my $print  = shift;
    my $serv = CIPRO::Service::ViewIcon->new();
    my $file = $serv->searchByPictureId($id,$print);

    binmode(STDOUT);
    print pack "H*", substr $file, 1;

}

print header({-type=>'image/png'});
open_image($PICTUREID,$PRINT);

