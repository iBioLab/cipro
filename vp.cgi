#!/usr/bin/perl

use CGI qw/:all/;
use CGI::Carp;
use lib "./lib";
use CIPRO::Service::ViewIcon;

my $PICTUREID   = param('id') || 357318; # "SPKH.C14.10001.v1.D";
# print = 1 -> PRINT PAGE SIZE , print = 0 -> ORIGINAL SIZE
my $PRINT   = param('print')||0;

sub open_image{
    my $id  = shift;
    my $print  = shift;
    my $serv = CIPRO::Service::ViewIcon->new();
    my $file = $serv->searchByPictureId($id,$print);

    binmode(STDOUT);
    $file =~ s/^x//;
    print pack "H*", $file;
}

print header({-type=>'image/png'});
open_image($PICTUREID,$PRINT);

